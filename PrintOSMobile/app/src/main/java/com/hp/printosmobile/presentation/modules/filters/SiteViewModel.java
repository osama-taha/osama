package com.hp.printosmobile.presentation.modules.filters;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Osama Taha on 10/9/16.
 */
public class SiteViewModel implements FilterItem, Serializable {

    private String id;
    private String name;
    private String timeZone;
    private boolean isSelected;
    private List<FilterItem> groups;
    private List<FilterItem> devices;
    private boolean allSite;
    private boolean defaultSite;
    private List<String> serialNumbers;

    public SiteViewModel() {

    }

    public SiteViewModel(String id, String name, String timeZone) {
        this.id = id;
        this.name = name;
        this.timeZone = timeZone;
    }


    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setDevices(List<FilterItem> devices) {
        this.devices = devices;
    }

    public List<FilterItem> getDevices() {
        return devices;
    }

    public void setGroups(List<FilterItem> groups) {
        this.groups = groups;
    }

    public List<FilterItem> getGroups() {
        return groups;
    }

    @Override
    public List<FilterItem> getItems() {
        List<FilterItem> items = new ArrayList<>();
        items.addAll(devices);
        return items;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    @Override
    public boolean isSelected() {
        return false;
    }

    public boolean isDefaultSite() {
        return defaultSite;
    }

    public void setDefaultSite(boolean defaultSite) {
        this.defaultSite = defaultSite;
    }

    public boolean isAllSite() {
        return allSite;
    }

    public void setAllSite(boolean allSite) {
        this.allSite = allSite;
    }

    @Override
    public int compareTo(FilterItem filterItem) {
        return 0;
    }

    @Override
    public String toString() {
        return "SiteViewModel{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", timeZone='" + timeZone + '\'' +
                ", groups=" + groups +
                '}';
    }

    public void setSerialNumbers(List<String> serialNumbers) {
        this.serialNumbers = serialNumbers;
    }

    public List<String> getSerialNumbers() {
        return serialNumbers;
    }

}