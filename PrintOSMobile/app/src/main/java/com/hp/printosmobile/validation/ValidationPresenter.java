package com.hp.printosmobile.validation;

import android.content.Context;

import com.hp.printosmobile.presentation.Presenter;
import com.hp.printosmobilelib.core.communications.remote.APIException;
import com.hp.printosmobilelib.core.communications.remote.Preferences;
import com.hp.printosmobilelib.core.communications.remote.models.UserData;
import com.hp.printosmobilelib.core.logging.HPLogger;

import okhttp3.ResponseBody;
import retrofit2.Response;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Anwar Asbah on 6/7/2017.
 */

public class ValidationPresenter extends Presenter<ValidationView> {

    public static final String TAG = ValidationPresenter.class.getSimpleName();

    public void authenticate(final Context context, final boolean isFirstCreation) {

        HPLogger.d(TAG, "Start validate... checking cookie");
        Subscription subscription = ValidationManager.validateToken()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<ResponseBody>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        HPLogger.d(TAG, "Validation error... cookie not valid");
                        if(e instanceof APIException){
                            if (((APIException) e).getKind() == APIException.Kind.NETWORK){
                                if(mView != null) {
                                    mView.onValidationError();
                                }
                                return;
                            }
                        }
                        login(context, isFirstCreation);
                    }

                    @Override
                    public void onNext(ResponseBody responseBody) {
                        HPLogger.d(TAG, "Token Valid");
                        if(mView != null) {
                            mView.onValidationCompleted();
                        }
                    }
                });

        addSubscriber(subscription);
    }

    public void login(final Context context, boolean isFirstCreation) {

        HPLogger.d(TAG, "Start Autologin.");

        if(mView != null) {
            mView.onPreAutoLogin(isFirstCreation);
        }

        Subscription subscription = ValidationManager.performAutoLogin(context)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<Response<UserData>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (e instanceof APIException) {
                            if (((APIException) e).getKind() == APIException.Kind.UNAUTHORIZED) {
                                HPLogger.d(TAG, "AutoLogin Unauthorized");
                                if(mView != null) {
                                    mView.onValidationUnauthorized();
                                }
                                return;
                            }
                        }
                        HPLogger.d(TAG, "AutoLogin failed due to unknown error");
                        if(mView != null) {
                            mView.onValidationError();
                        }
                    }

                    @Override
                    public void onNext(Response<UserData> userData) {
                        HPLogger.d(TAG, "Autologin Success" + userData.toString());
                        if (userData != null && userData.isSuccessful()) {
                            updateContext(context, userData.body());
                        } else {
                            if (userData.code() >= 400 && userData.code() < 500) {
                                HPLogger.d(TAG, "AutoLogin fail: Unauthorized");
                                if(mView != null) {
                                    mView.onValidationUnauthorized();
                                }
                                return;
                            }
                            HPLogger.d(TAG, "AutoLogin failed: unknown error");
                            if(mView != null) {
                                mView.onValidationError();
                            }
                        }
                    }
                });

        addSubscriber(subscription);
    }


    private void updateContext(Context context, UserData userData) {

        if (Preferences.getInstance(context).getSavedOrganizationId() != null) {

            HPLogger.d(TAG, "Start Change context.");

            Subscription subscription = ValidationManager.changeContext(context)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(new Subscriber<ResponseBody>() {
                        @Override
                        public void onCompleted() {
                        }

                        @Override
                        public void onError(Throwable e) {
                            HPLogger.d(TAG, "Change context failed");
                            if(mView != null) {
                                mView.onValidationCompletedWithoutContextChange();
                            }
                        }

                        @Override
                        public void onNext(ResponseBody responseBody) {
                            HPLogger.d(TAG, "Change context success");
                            if(mView != null) {
                                mView.onValidationCompleted();
                            }
                        }
                    });

            addSubscriber(subscription);

        } else if (userData != null) {
            HPLogger.d(TAG, "no context saved");
            UserData.Context organization = userData.getContext();
            Preferences.getInstance(context).saveOrganization(organization);
            if(mView != null) {
                mView.onValidationCompletedWithoutContextChange();
            }
        }
    }

    @Override
    public void detachView() {
        stopSubscribers();
        mView = null;
    }
}
