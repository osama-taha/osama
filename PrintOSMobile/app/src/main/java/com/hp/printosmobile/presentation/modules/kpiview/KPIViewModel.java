package com.hp.printosmobile.presentation.modules.kpiview;

import com.hp.printosmobile.presentation.modules.week.kpiexplanationdialog.KpiExplanationEnum;
import com.hp.printosmobile.presentation.modules.week.kpiexplanationdialog.KpiExplanationViewModel;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Anwar Asbah on 4/27/2016.
 */
public class KPIViewModel implements Serializable{

    private String name;
    private String localizedName;
    private int value;
    private int maxScore;
    private int score;
    private KPIValueHandleEnum kpiValueHandle;
    private KPIScoreStateEnum scoreState;
    private KPIScoreTrendEnum scoreTrend;
    private String customText;
    private int kpiDrawable;
    private KpiExplanationEnum kpiEnum;
    private List<KpiExplanationViewModel> kpiExplanationViewModels;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getMaxScore() {
        return maxScore;
    }

    public void setMaxScore(int maxScore) {
        this.maxScore = maxScore;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public KPIValueHandleEnum getUnit() {
        return kpiValueHandle;
    }

    public void setUnit(KPIValueHandleEnum unit) {
        this.kpiValueHandle = unit;
    }

    public KPIScoreStateEnum getScoreState() {
        return scoreState;
    }

    public void setScoreState(KPIScoreStateEnum scoreState) {
        this.scoreState = scoreState;
    }

    public KPIScoreTrendEnum getScoreTrend() {
        return scoreTrend;
    }

    public void setScoreTrend(KPIScoreTrendEnum scoreTrend) {
        this.scoreTrend = scoreTrend;
    }

    public int getKpiDrawable() {
        return kpiDrawable;
    }

    public void setKpiDrawable(int kpiDrawable) {
        this.kpiDrawable = kpiDrawable;
    }

    public void setLocalizedName(String localizedName) {
        this.localizedName = localizedName;
    }

    public String getLocalizedName() {
        return localizedName;
    }

    public String getCustomText() {
        return customText;
    }

    public void setCustomText(String customText) {
        this.customText = customText;
    }

    public KpiExplanationEnum getKpiEnum() {
        return kpiEnum;
    }

    public void setKpiEnum(KpiExplanationEnum kpiEnum) {
        this.kpiEnum = kpiEnum;
    }

    public List<KpiExplanationViewModel> getKpiExplanationViewModels() {
        return kpiExplanationViewModels;
    }

    public void setKpiExplanationViewModels(List<KpiExplanationViewModel> kpiExplanationViewModels) {
        this.kpiExplanationViewModels = kpiExplanationViewModels;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("KPIViewModel{");
        sb.append("name='").append(name).append('\'');
        sb.append(", value=").append(value);
        sb.append(", maxScore=").append(maxScore);
        sb.append(", score=").append(score);
        sb.append(", unit='").append(kpiValueHandle).append('\'');
        sb.append(", scoreState=").append(scoreState);
        sb.append(", scoreTrend=").append(scoreTrend);
        sb.append(", kpiDrawableUrl=").append(kpiDrawable);
        sb.append('}');
        return sb.toString();
    }
}