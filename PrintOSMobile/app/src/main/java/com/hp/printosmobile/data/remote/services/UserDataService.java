package com.hp.printosmobile.data.remote.services;

import com.hp.printosmobile.data.remote.ApiConstants;
import com.hp.printosmobile.data.remote.models.LastSiteData;
import com.hp.printosmobile.data.remote.models.UserRoles;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.PUT;
import rx.Observable;

/**
 * Created by Osama Taha on 10/10/16.
 */

public interface UserDataService {

    @PUT(ApiConstants.LAST_SELECT_SITE_API)
    Observable<ResponseBody> saveLastSite(@Body LastSiteData.LastSite lastSite);

    @GET(ApiConstants.LAST_SELECT_SITE_API)
    Observable<Response<LastSiteData>> getLastSite();

    @GET(ApiConstants.USER_ROLES_API)
    Call<UserRoles> getUserRoles();

}
