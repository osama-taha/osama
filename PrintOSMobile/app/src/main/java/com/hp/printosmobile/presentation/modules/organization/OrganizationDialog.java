package com.hp.printosmobile.presentation.modules.organization;

import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.AppseeSdk;
import com.hp.printosmobile.IntercomSdk;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.modules.drawer.OrganizationViewModel;
import com.hp.printosmobile.utils.HPUIUtils;
import com.hp.printosmobilelib.ui.widgets.HPEditText;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by anwar asbah on 11/20/2016.
 */
public class OrganizationDialog extends DialogFragment {

    private static final String ORGANIZATION_LIST_ARG = "organizations_ist_arg";
    private static final String SELECTED_ORGANIZATION_ID_ARG = "organizations_ID_arg";
    private static final long SCROLL_TO_SELECTED_ORGANIZATION_DELAY = 200;

    @Bind(R.id.organization_input_field)
    HPEditText organizationEditText;
    @Bind(R.id.organization_list)
    RecyclerView organizationList;

    List<OrganizationViewModel> organizations;
    String selectedOrganizationId;
    ListAdapter adapter;
    OrganizationDialogCallback listener;

    public static OrganizationDialog getInstance(List<OrganizationViewModel> organizations, String selectedOrganizationID) {
        OrganizationDialog dialog = new OrganizationDialog();

        Bundle bundle = new Bundle();
        //bundle.putSerializable(ORGANIZATION_LIST_ARG, (ArrayList<OrganizationViewModel>) organizations);
        dialog.organizations = organizations;
        bundle.putString(SELECTED_ORGANIZATION_ID_ARG, selectedOrganizationID);
        dialog.setArguments(bundle);

        return dialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE,
                R.style.AccountDialogStyle);

        Bundle bundle = getArguments();
        selectedOrganizationId = "";
        //organizations = new ArrayList<>();

        //if (bundle != null && bundle.containsKey(ORGANIZATION_LIST_ARG)) {
        //    organizations = (List<OrganizationViewModel>) bundle.getSerializable(ORGANIZATION_LIST_ARG);
        //}

        if (bundle != null && bundle.containsKey(SELECTED_ORGANIZATION_ID_ARG)) {
            selectedOrganizationId = bundle.getString(SELECTED_ORGANIZATION_ID_ARG, "");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.organization_dialog, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        IntercomSdk.getInstance(PrintOSApplication.getAppContext()).logEvent(IntercomSdk.PBM_VIEW_ACCOUNT_EVENT);
        Analytics.sendEvent(Analytics.MOBILE_PRINT_BEAT_CATEGORY, Analytics.VIEW_CHANGE_ORGANIZATION_ACTION);
        AppseeSdk.getInstance(getActivity()).startScreen(AppseeSdk.SCREEN_CHANGE_ORGANIZATION);

        initView();
        getDialog().setCanceledOnTouchOutside(true);
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        }
    }

    private void initView() {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        adapter = new ListAdapter(organizations, selectedOrganizationId);
        organizationList.setLayoutManager(layoutManager);
        organizationList.setAdapter(adapter);

        organizationList.setHasFixedSize(true);

        organizationEditText.forceHideClearButton(true);

        organizationEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                adapter.getFilter().filter(s);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        organizationList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dx != 0 || dy != 0) {
                    HPUIUtils.hideSoftKeyboard(getActivity(), organizationEditText);
                }
            }
        });

        organizationList.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                organizationList.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        int itemsCount = adapter.getItemCount();
                        if (itemsCount > 0) {
                            int selectedIndex = adapter.getSelectedOrganizationPosition();
                            selectedIndex = selectedIndex < itemsCount - 1 ? selectedIndex + 1 : selectedIndex;
                            organizationList.scrollToPosition(selectedIndex);
                        }
                    }
                }, SCROLL_TO_SELECTED_ORGANIZATION_DELAY);
            }
        });
    }

    public void addOrganizationDialogCallback(OrganizationDialogCallback callback) {
        listener = callback;
    }

    @OnClick(R.id.cancel_button)
    public void onCancelClicked() {
        dismiss();
    }

    @OnClick(R.id.switch_button)
    public void onSwitchClicked() {
        if (listener != null) {
            listener.onOrganizationsSelected(adapter.getSelectedOrganization());
        }
        dismiss();
    }

    @OnClick(R.id.dialog_layout)
    public void onClickOutside(){
        dismiss();
    }

    @Override
    public void dismiss() {
        super.dismiss();
        if (listener != null) {
            listener.onDialogDismissed();
        }
    }

    public class ListAdapter extends RecyclerView.Adapter<ListAdapter.OrganizationViewHolder> implements Filterable {

        List<OrganizationViewModel> originalOrganizationList;
        List<OrganizationViewModel> filteredOrganizationList;
        OrganizationViewModel selectedOrganization = null;
        Filter filter;

        public ListAdapter(List<OrganizationViewModel> organizations, String initialSelectedOrganizationID) {
            originalOrganizationList = organizations == null ? new ArrayList<OrganizationViewModel>() : organizations;

            if (organizations != null) {
                for (OrganizationViewModel model : organizations) {
                    if (model.getOrganizationId().equals(initialSelectedOrganizationID)) {
                        selectedOrganization = model;
                        break;
                    }
                }
            }

            getFilter().filter("");
        }

        @Override
        public OrganizationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            ViewGroup view = (ViewGroup) inflater.inflate(R.layout.organization_dialog_item, parent, false);
            view.setBackgroundResource(R.drawable.list_item_bg_color);
            ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
            layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
            view.setLayoutParams(layoutParams);

            OrganizationViewHolder organizationViewHolder = new OrganizationViewHolder(view);
            return organizationViewHolder;
        }

        @Override
        public void onBindViewHolder(OrganizationViewHolder holder, int position) {
            OrganizationViewModel organizationViewModel = filteredOrganizationList.get(position);
            holder.viewModel = organizationViewModel;
            holder.organizationName.setText(organizationViewModel.getOrganizationName());
            holder.itemView.setSelected(false);
            holder.organizationName.setSelected(false);

            if (selectedOrganization != null && selectedOrganization.getOrganizationId() != null
                    && organizationViewModel.getOrganizationId() != null) {
                if (selectedOrganization.getOrganizationId().equals(organizationViewModel.getOrganizationId())) {

                    holder.itemView.setSelected(true);
                    holder.organizationName.setSelected(true);
                }
            }
        }

        @Override
        public int getItemCount() {
            return filteredOrganizationList == null ? 0 : filteredOrganizationList.size();
        }

        public OrganizationViewModel getSelectedOrganization() {
            return selectedOrganization;
        }

        @Override
        public Filter getFilter() {
            if (filter == null) {
                filter = new Filter() {
                    @Override
                    protected FilterResults performFiltering(CharSequence constraint) {
                        String filterString = constraint.toString().toLowerCase();

                        FilterResults results = new FilterResults();

                        final List<OrganizationViewModel> list = originalOrganizationList;

                        int count = list.size();
                        final ArrayList<OrganizationViewModel> filteredList = new ArrayList<>();

                        String filterableString;

                        for (int i = 0; i < count; i++) {
                            filterableString = list.get(i).getOrganizationName();
                            if (filterableString.toLowerCase().contains(filterString)) {
                                filteredList.add(list.get(i));
                            }
                        }

                        results.values = filteredList;
                        results.count = filteredList.size();

                        return results;
                    }

                    @Override
                    protected void publishResults(CharSequence constraint, FilterResults results) {
                        filteredOrganizationList = (List<OrganizationViewModel>) results.values;
                        notifyDataSetChanged();
                    }
                };
            }
            return filter;
        }

        public int getSelectedOrganizationPosition() {
            for (int i = 0; i < originalOrganizationList.size(); i++) {
                if (originalOrganizationList.get(i).getOrganizationId().equals(selectedOrganizationId)) {
                    return i;
                }
            }

            return 0;
        }

        public class OrganizationViewHolder extends RecyclerView.ViewHolder {

            @Bind(R.id.organization_name)
            TextView organizationName;

            View itemView;
            OrganizationViewModel viewModel;

            public OrganizationViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);

                this.itemView = itemView;

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        selectedOrganization = viewModel;
                        notifyDataSetChanged();
                        HPUIUtils.hideSoftKeyboard(PrintOSApplication.getAppContext(), organizationEditText);
                    }
                });
            }
        }
    }

    public interface OrganizationDialogCallback {
        void onOrganizationsSelected(OrganizationViewModel selectedOrganization);

        void onDialogDismissed();
    }
}
