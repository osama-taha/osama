package com.hp.printosmobile.presentation.modules.home;

import com.hp.printosmobile.presentation.modules.productionsnapshot.indigo.ProductionViewModel;
import com.hp.printosmobile.presentation.modules.today.TodayViewModel;

/**
 * Created by Osama Taha on 9/3/16.
 */
public class PrintBeatDashboardViewModel {

    private TodayViewModel todayViewModel;
    private ProductionViewModel productionViewModel;

    public void setTodayViewModel(TodayViewModel todayViewModel) {
        this.todayViewModel = todayViewModel;
    }

    public TodayViewModel getTodayViewModel() {
        return todayViewModel;
    }

    public void setProductionViewModel(ProductionViewModel productionViewModel) {
        this.productionViewModel = productionViewModel;
    }

    public ProductionViewModel getProductionViewModel() {
        return productionViewModel;
    }
}
