package com.hp.printosmobile.presentation.modules.week.kpiexplanationdialog;

import android.content.Context;
import android.text.Spannable;
import android.text.SpannableString;

import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.data.remote.models.PreferencesData;
import com.hp.printosmobile.data.remote.models.WeekData;
import com.hp.printosmobile.presentation.modules.kpiview.KPIScoreStateEnum;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;
import com.hp.printosmobile.utils.HPLocaleUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Anwar Asbah on 8/8/2017.
 */
public class KpiExplanationUtils {

    private static final String HOURS_FORMAT = "%1$d:%2$d";
    private static final int SECONDS_IN_HOURS = 3600;
    private static final int SECONDS_IN_MINS = 60;

    public static KpiExplanationViewModel parseKpiExplanationItemForIndigo(
            WeekData.KpiExplanationItem item, Context context) {

        if (item == null) {
            return null;
        }

        KpiExplanationViewModel explanationViewModel = new KpiExplanationViewModel();

        if (item.getMin() != null) {
            explanationViewModel.setMin(Math.round(item.getMin().getValue()));
        }
        if (item.getMax() != null) {
            explanationViewModel.setMax(Math.round(item.getMax().getValue()));
        }
        if (item.getScore() != null) {
            explanationViewModel.setScore(Math.round(item.getScore().getValue()));
        }
        if (item.getValue() != null) {
            explanationViewModel.setValue(Math.round(item.getValue().getValue()));
        }
        explanationViewModel.setLimit(item.getLimit());
        explanationViewModel.setStateEnum(KPIScoreStateEnum.from(item.getState()));
        explanationViewModel.setKpiExplanationEnum(KpiExplanationEnum.from(context,
                item.getName(), BusinessUnitEnum.INDIGO_PRESS));
        explanationViewModel.setDescriptionText(getHeader(context, explanationViewModel.getKpiExplanationEnum()));
        explanationViewModel.setProperties(getKpiProperties(item));

        return explanationViewModel;
    }

    public static KpiExplanationViewModel parseKpiExplanationItemForLatex(
            WeekData.Scorable scorable, Context context, int maxScore) {

        if (scorable == null) {
            return null;
        }

        KpiExplanationViewModel explanationViewModel = new KpiExplanationViewModel();

        explanationViewModel.setMin(-1);
        explanationViewModel.setMax(scorable.getTargetValue());
        explanationViewModel.setScore(scorable.getScore() == null ? 0 : scorable.getScore());
        explanationViewModel.setValue(scorable.getValue() == null ? 0 : scorable.getValue());
        explanationViewModel.setLimit(maxScore);
        explanationViewModel.setStateEnum(KPIScoreStateEnum.from(scorable.getKpiScoreState()));
        explanationViewModel.setKpiExplanationEnum(KpiExplanationEnum.from(context,
                scorable.getName(), BusinessUnitEnum.LATEX_PRINTER));
        explanationViewModel.setDescriptionText(getHeader(context, explanationViewModel.getKpiExplanationEnum()));

        if (explanationViewModel.getKpiExplanationEnum() == KpiExplanationEnum.MAINTENANCE_LATEX) {
            List<KpiExplanationViewModel.Property> properties = new ArrayList<>();
            KpiExplanationViewModel.Property property = new KpiExplanationViewModel.Property();
            property.setValue(String.valueOf(explanationViewModel.getValue()));
            property.setStringID(R.string.kpi_explanation_property_maintenance);
            properties.add(property);
            explanationViewModel.setProperties(properties);
        }

        return explanationViewModel;
    }

    private static List<KpiExplanationViewModel.Property> getKpiProperties(WeekData.KpiExplanationItem item) {

        List<WeekData.Property> properties = item.getProperties();
        if (properties == null) {
            return null;
        }

        List<KpiExplanationViewModel.Property> propertiesList = new ArrayList<>();
        for (WeekData.Property property : properties) {
            KpiExplanationViewModel.Property parseProperty = parseProperty(property);
            if (parseProperty != null) {
                propertiesList.add(parseProperty);
            }
        }

        return propertiesList;
    }

    private static KpiExplanationViewModel.Property parseProperty(WeekData.Property property) {
        if (property == null) {
            return null;
        }

        KpiExplanationViewModel.Property parsedProperty = new KpiExplanationViewModel.Property();

        KpiExplanationStringEnum stringEnum = KpiExplanationStringEnum.from(property.getName());
        if (stringEnum != KpiExplanationStringEnum.UNKNOWN) {

            KpiExplanationUnitEnum unitEnum = KpiExplanationUnitEnum.from(property.getUnit());
            parsedProperty.setStringID(stringEnum.getStringID());
            parsedProperty.setValue(getPropertyValue(property.getValue(), unitEnum));

            return parsedProperty;
        }

        return null;
    }

    private static String getPropertyValue(float value, KpiExplanationUnitEnum unitEnum) {
        switch (unitEnum) {
            case KILO:
                return HPLocaleUtils.getDecimalString(value, true);
            case SECONDS:
                int hours = (int) value / SECONDS_IN_HOURS;
                int mins = ((int) value % SECONDS_IN_HOURS) / SECONDS_IN_MINS;
                return String.format(HOURS_FORMAT, hours, mins);
            default:
                return HPLocaleUtils.getDecimalString((int) value, true);
        }
    }

    public static Spannable getPropertySpannable(Context context, KpiExplanationViewModel.Property property) {
        String propertyValue = property.getValue();
        String propertyString = String.format(context.getString(property.getStringID()), propertyValue);

        int start = propertyString.indexOf(propertyValue);
        int end = start + propertyValue.length();

        SpannableString spannable = new SpannableString(propertyString);
        spannable.setSpan(new android.text.style.StyleSpan(android.graphics.Typeface.BOLD), start, end, Spannable.SPAN_INCLUSIVE_INCLUSIVE);

        return spannable;
    }

    private static String getHeader(Context context, KpiExplanationEnum explanationEnum) {
        switch (explanationEnum) {
            case PRINT_VOLUME_LATEX:
                return context.getString(PrintOSPreferences.getInstance(context).getUnitSystem() == PreferencesData.UnitSystem.Metric ?
                        R.string.kpi_explanation_header_print_volume_latex_sqm :
                        R.string.kpi_explanation_header_print_volume_latex_ft);
            case MAINTENANCE_LATEX:
                return context.getString(R.string.kpi_explanation_header_maintenance);
            case UTILIZATION_LATEX:
                return context.getString(R.string.kpi_explanation_header_utilization);
            default:
                return null;
        }
    }

    public enum KpiExplanationStringEnum {

        AVAILABLE_TIME("available_time", R.string.kpi_explanation_property_available_time),
        PRINT_TIME("print_time", R.string.kpi_explanation_property_print_time),
        UP_TIME("up_time", R.string.kpi_explanation_property_up_time),

        FAILURES_COUNT("failures_count", R.string.kpi_explanation_property_failures_count),
        FAILURES_RATE("failures_rate", R.string.kpi_explanation_property_rate),
        FAILURES_TARGET_COUNT("failures_target_count", R.string.kpi_explanation_property_target_count),
        IMPRESSIONS("impressions", R.string.kpi_explanation_property_impressions),

        JAMS_COUNT("jams_count", R.string.kpi_explanation_property_jams_count),
        JAMS_RATE("jams_rate", R.string.kpi_explanation_property_rate),
        JAMS_TARGET_COUNT("jams_target_count", R.string.kpi_explanation_property_target_count),
        SHEETS("sheets", R.string.kpi_explanation_property_sheets),

        RESTARTS_COUNT("restarts_count", R.string.kpi_explanation_property_restarts_count),
        RESTARTS_RATE("restarts_rate", R.string.kpi_explanation_property_rate),
        RESTARTS_TARGET_COUNT("restarts_target_count", R.string.kpi_explanation_property_target_count),

        PIPS_REPLACEMENTS("pips_replacements", R.string.kpi_explanation_property_replacements),
        PIP_AVERAGE_LIFESPAN("pip_average_lifespan", R.string.kpi_explanation_property_average_life_span),

        BLANKETS_REPLACEMENTS("blankets_replacements", R.string.kpi_explanation_property_replacements),
        BLANKET_AVERAGE_LIFESPAN("blanket_average_lifespan", R.string.kpi_explanation_property_average_life_span),

        UNKNOWN("", 0);

        private String key;
        private int stringID;

        KpiExplanationStringEnum(String key, int stringID) {
            this.key = key;
            this.stringID = stringID;
        }

        public int getStringID() {
            return stringID;
        }

        public static KpiExplanationStringEnum from(String key) {
            if (key == null) {
                return UNKNOWN;
            }
            for (KpiExplanationStringEnum stringEnum : KpiExplanationStringEnum.values()) {
                if (stringEnum.key.equals(key))
                    return stringEnum;
            }
            return UNKNOWN;
        }
    }

    public enum KpiExplanationUnitEnum {

        KILO("Kilo"),
        NUMBER("Number"),
        SECONDS("Seconds");

        private String key;

        KpiExplanationUnitEnum(String key) {
            this.key = key;
        }

        public static KpiExplanationUnitEnum from(String key) {
            if (key == null) {
                return NUMBER;
            }
            for (KpiExplanationUnitEnum unitEnum : KpiExplanationUnitEnum.values()) {
                if (unitEnum.key.equals(key))
                    return unitEnum;
            }
            return NUMBER;
        }
    }
}