
package com.hp.printosmobile.data.remote.models.ExtendedStatistics;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class PrinterData {

    @JsonProperty("printer_info")
    private PrinterInfo printerInfo;
    @JsonProperty("notifications")
    private Notifications notifications;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * @return The printerInfo
     */
    @JsonProperty("printer_info")
    public PrinterInfo getPrinterInfo() {
        return printerInfo;
    }

    /**
     * @param printerInfo The printer_info
     */
    @JsonProperty("printer_info")
    public void setPrinterInfo(PrinterInfo printerInfo) {
        this.printerInfo = printerInfo;
    }

    /**
     * @return The notifications
     */
    @JsonProperty("notifications")
    public Notifications getNotifications() {
        return notifications;
    }

    /**
     * @param notifications The notifications
     */
    @JsonProperty("notifications")
    public void setNotifications(Notifications notifications) {
        this.notifications = notifications;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class PrinterInfo {

        @JsonProperty("last_update_at")
        private String lastUpdateAt;
        @JsonProperty("deviceStatus")
        private String deviceStatus;
        @JsonProperty("status")
        private String status;
        @JsonProperty("alerts_status")
        private String alertsStatus;
        @JsonProperty("status_message")
        private String statusMessage;
        @JsonProperty("printer_model")
        private String printerModel;
        @JsonProperty("printer_serial_number")
        private String printerSerialNumber;
        @JsonProperty("printer_product_number")
        private String printerProductNumber;
        @JsonProperty("printer_service_id")
        private Object printerServiceId;
        @JsonProperty("printer_firmware_version")
        private String printerFirmwareVersion;
        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<String, Object>();

        /**
         * @return The lastUpdateAt
         */
        @JsonProperty("last_update_at")
        public String getLastUpdateAt() {
            return lastUpdateAt;
        }

        /**
         * @param lastUpdateAt The last_update_at
         */
        @JsonProperty("last_update_at")
        public void setLastUpdateAt(String lastUpdateAt) {
            this.lastUpdateAt = lastUpdateAt;
        }


        /**
         * @return The device status
         */
        @JsonProperty("deviceStatus")
        public String getDeviceStatus() {
            return deviceStatus;
        }

        /**
         * @param deviceStatus The device status
         */
        @JsonProperty("deviceStatus")
        public void setDeviceStatus(String deviceStatus) {
            this.deviceStatus = deviceStatus;
        }

        /**
         * @return The status
         */
        @JsonProperty("status")
        public String getStatus() {
            return status;
        }

        /**
         * @param status The status
         */
        @JsonProperty("status")
        public void setStatus(String status) {
            this.status = status;
        }

        /**
         * @return The alertsStatus
         */
        @JsonProperty("alerts_status")
        public String getAlertsStatus() {
            return alertsStatus;
        }

        /**
         * @param alertsStatus The alerts_status
         */
        @JsonProperty("alerts_status")
        public void setAlertsStatus(String alertsStatus) {
            this.alertsStatus = alertsStatus;
        }

        /**
         * @return The statusMessage
         */
        @JsonProperty("status_message")
        public String getStatusMessage() {
            return statusMessage;
        }

        /**
         * @param statusMessage The status_message
         */
        @JsonProperty("status_message")
        public void setStatusMessage(String statusMessage) {
            this.statusMessage = statusMessage;
        }

        /**
         * @return The printerModel
         */
        @JsonProperty("printer_model")
        public String getPrinterModel() {
            return printerModel;
        }

        /**
         * @param printerModel The printer_model
         */
        @JsonProperty("printer_model")
        public void setPrinterModel(String printerModel) {
            this.printerModel = printerModel;
        }

        /**
         * @return The printerSerialNumber
         */
        @JsonProperty("printer_serial_number")
        public String getPrinterSerialNumber() {
            return printerSerialNumber;
        }

        /**
         * @param printerSerialNumber The printer_serial_number
         */
        @JsonProperty("printer_serial_number")
        public void setPrinterSerialNumber(String printerSerialNumber) {
            this.printerSerialNumber = printerSerialNumber;
        }

        /**
         * @return The printerProductNumber
         */
        @JsonProperty("printer_product_number")
        public String getPrinterProductNumber() {
            return printerProductNumber;
        }

        /**
         * @param printerProductNumber The printer_product_number
         */
        @JsonProperty("printer_product_number")
        public void setPrinterProductNumber(String printerProductNumber) {
            this.printerProductNumber = printerProductNumber;
        }

        /**
         * @return The printerServiceId
         */
        @JsonProperty("printer_service_id")
        public Object getPrinterServiceId() {
            return printerServiceId;
        }

        /**
         * @param printerServiceId The printer_service_id
         */
        @JsonProperty("printer_service_id")
        public void setPrinterServiceId(Object printerServiceId) {
            this.printerServiceId = printerServiceId;
        }

        /**
         * @return The printerFirmwareVersion
         */
        @JsonProperty("printer_firmware_version")
        public String getPrinterFirmwareVersion() {
            return printerFirmwareVersion;
        }

        /**
         * @param printerFirmwareVersion The printer_firmware_version
         */
        @JsonProperty("printer_firmware_version")
        public void setPrinterFirmwareVersion(String printerFirmwareVersion) {
            this.printerFirmwareVersion = printerFirmwareVersion;
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }

    }

}
