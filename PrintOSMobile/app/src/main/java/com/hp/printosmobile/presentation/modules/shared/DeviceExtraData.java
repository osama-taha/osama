package com.hp.printosmobile.presentation.modules.shared;

import com.hp.printosmobile.presentation.modules.devicedetails.JobModel;
import com.hp.printosmobile.presentation.modules.devicedetails.InkModel;
import com.hp.printosmobile.presentation.modules.devicedetails.SubstrateModel;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by Anwar Asbah on 5/4/2016.
 */
public class DeviceExtraData implements Serializable {

    private String name;
    private DeviceState printerState;
    private String printerImageUrl;
    private List<JobModel> futureJobs;
    private List<JobModel> printingJobs;
    private List<JobModel> printedJobs;
    private List<InkModel> inkList;
    private List<SubstrateModel> substrateList;
    private String message;
    private String frontPanelMessage;
    private String model;
    private Date lastUpdatedAt;
    private String serialNumber;
    private String firmwareVersion;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DeviceState getState() {
        return printerState;
    }

    public void setState(DeviceState state) {
        this.printerState = state;
    }

    public void setPrinterImageUrl(String printerImageUrl) {
        this.printerImageUrl = printerImageUrl;
    }

    public List<InkModel> getInkList() {
        return inkList;
    }

    public void setInkList(List<InkModel> inkList) {
        this.inkList = inkList;
    }

    public String getPrinterImageUrl() {
        return printerImageUrl;
    }

    public List<SubstrateModel> getSubstrateList() {
        return substrateList;
    }

    public void setSubstrateList(List<SubstrateModel> substrateList) {
        this.substrateList = substrateList;
    }

    public List<JobModel> getFutureJobs() {
        return futureJobs;
    }

    public void setFutureJobs(List<JobModel> futureJobs) {
        this.futureJobs = futureJobs;
    }

    public List<JobModel> getPrintingJobs() {
        return printingJobs;
    }

    public void setPrintingJobs(List<JobModel> printingJobs) {
        this.printingJobs = printingJobs;
    }

    public List<JobModel> getPrintedJobs() {
        return printedJobs;
    }

    public void setPrintedJobs(List<JobModel> printedJobs) {
        this.printedJobs = printedJobs;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getLastUpdatedAt() {
        return lastUpdatedAt;
    }

    public void setLastUpdatedAt(Date lastUpdatedAt) {
        this.lastUpdatedAt = lastUpdatedAt;
    }

    public String getFrontPanelMessage() {
        return frontPanelMessage;
    }

    public void setFrontPanelMessage(String frontPanelMessage) {
        this.frontPanelMessage = frontPanelMessage;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getFirmwareVersion() {
        return firmwareVersion;
    }

    public void setFirmwareVersion(String firmwareVersion) {
        this.firmwareVersion = firmwareVersion;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("DeviceExtraData{");
        sb.append("name='").append(name).append('\'');
        sb.append(", printerState=").append(printerState);
        sb.append(", printerImageUrl=").append(printerImageUrl);
        sb.append('}');
        return sb.toString();
    }
}
