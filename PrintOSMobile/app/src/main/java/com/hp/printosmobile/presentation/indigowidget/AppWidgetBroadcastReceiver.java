package com.hp.printosmobile.presentation.indigowidget;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.hp.printosmobilelib.core.logging.HPLogger;

public class AppWidgetBroadcastReceiver extends BroadcastReceiver {

    private static final String TAG = AppWidgetBroadcastReceiver.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {

        HPLogger.d(TAG, "Global receiver, action: " + intent.getAction());

        PrintOSAppWidgetProvider.updateAllWidgets(context);

    }

}