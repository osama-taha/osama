package com.hp.printosmobile.presentation.modules.devices;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.hp.printosmobile.AppseeSdk;
import com.hp.printosmobile.IntercomSdk;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.BaseFragment;
import com.hp.printosmobile.presentation.modules.devicedetails.IndigoDeviceDetailsFragment;
import com.hp.printosmobile.presentation.modules.devicedetails.LatexDeviceDetailsFragment;
import com.hp.printosmobile.presentation.modules.home.Panel;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitViewModel;
import com.hp.printosmobile.presentation.modules.main.IMainFragment;
import com.hp.printosmobile.presentation.modules.personaladvisor.AdviceViewModel;
import com.hp.printosmobile.presentation.modules.personaladvisor.PersonalAdvisorDialog;
import com.hp.printosmobile.presentation.modules.personaladvisor.PersonalAdvisorViewModel;
import com.hp.printosmobile.presentation.modules.personaladvisor.presenter.PersonalAdvisorFactory;
import com.hp.printosmobile.presentation.modules.shared.DeviceViewModel;
import com.hp.printosmobilelib.core.communications.remote.APIException;
import com.hp.printosmobilelib.core.logging.HPLogger;
import com.hp.printosmobilelib.ui.common.HPFragment;
import com.hp.printosmobilelib.ui.utils.DividerItemDecoration;
import com.hp.printosmobilelib.ui.widgets.HPSegmentedRecyclerView;

import java.util.List;
import java.util.Map;

import butterknife.Bind;

/**
 * @author Osama Taha
 */
public class DevicesFragment extends BaseFragment implements DevicesView, IMainFragment, DevicesAdapter.DevicesAdapterListener, IndigoDeviceDetailsFragment.IndigoDeviceDetailsFragmentCallbacks, PersonalAdvisorFactory.PersonalAdvisorObserver {

    private static final String TAG = DevicesFragment.class.getSimpleName();
    private static final String ADVICE_TAG = "advices";

    @Bind(R.id.recycler_view_devices)
    HPSegmentedRecyclerView devicesRecyclerView;
    @Bind(R.id.loading_view)
    ProgressBar loadingView;
    @Bind(R.id.error_msg_text_view)
    View errorMsg;

    private BusinessUnitViewModel businessUnitViewModel;
    private DevicesCallbacks listener;

    private DevicesPresenter presenter;
    private DevicesAdapter deviceAdapter;

    private HPFragment deviceDetails;
    private boolean fromDevices;
    private List<DeviceViewModel> deviceDataList;

    public DevicesFragment() {
        // Required empty public constructor
    }

    public static DevicesFragment newInstance() {
        DevicesFragment homeFragment = new DevicesFragment();
        return homeFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initView();
        initPresenter();

        PersonalAdvisorFactory.getInstance().addObserver(this);
    }

    @Override
    public void onResume() {
        super.onResume();

        try {
            listener = (DevicesCallbacks) getActivity();
        } catch (ClassCastException e) {
            throw new RuntimeException(getActivity().getClass().getSimpleName() + " must implement DevicesCallbacks.");
        }

    }

    public boolean isDeviceDetailsDisplayed() {
        return deviceDetails != null;
    }

    public void removeDeviceDetailsDrillDownViews() {

        if (deviceDetails != null) {
            FragmentTransaction trans = getFragmentManager()
                    .beginTransaction();
            trans.remove(deviceDetails);
            trans.commit();
            deviceDetails = null;
            listener.onDevicesDetailedClosed(this);
        }
    }

    private void initView() {

        devicesRecyclerView.setHasFixedSize(true);
        devicesRecyclerView.setContentLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));

    }

    private void initPresenter() {

        presenter = new DevicesPresenter();
        presenter.attachView(this);

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_devices;
    }

    @Override
    public void onBusinessUnitSelected(BusinessUnitViewModel businessUnitViewModel) {

        if(presenter == null) {
            return;
        }

        HPLogger.d(TAG, "Update " + TAG);

        this.businessUnitViewModel = businessUnitViewModel;
        presenter.setSelectedBusinessUnit(businessUnitViewModel);


        presenter.loadData(getActivity());

        errorMsg.setVisibility(View.GONE);
    }

    @Override
    public void onFiltersChanged(BusinessUnitViewModel businessUnitViewModel) {

        HPLogger.d(TAG, "Filter changed " + TAG);

        onBusinessUnitSelected(businessUnitViewModel);

    }

    @Override
    public void didFinishGettingBusinessUnits(Map<BusinessUnitEnum, BusinessUnitViewModel> businessUnitsMap) {
    }

    @Override
    public void hideLoading() {
        loadingView.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showLoading() {
        loadingView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onGettingDevicesCompleted(List<DeviceViewModel> deviceDataList) {

        if (deviceDataList == null || businessUnitViewModel == null) {
            devicesRecyclerView.setVisibility(View.INVISIBLE);
            errorMsg.setVisibility(View.VISIBLE);
            return;
        }

        this.deviceDataList = deviceDataList;

        updatePersonalAdvisorObserver();

        deviceAdapter = new DevicesAdapter(getContext(), deviceDataList, businessUnitViewModel.getBusinessUnit(), this);
        devicesRecyclerView.setAdapter(deviceAdapter);
        devicesRecyclerView.addItemDecoration(new DividerItemDecoration(getContext(), 1, deviceDataList.size(), false));
        deviceAdapter.notifyDataSetChanged();

        devicesRecyclerView.setSegmentsVisible(businessUnitViewModel.getBusinessUnit() == BusinessUnitEnum.LATEX_PRINTER);
        devicesRecyclerView.setVisibility(View.VISIBLE);

    }

    @Override
    public void onError() {
        devicesRecyclerView.setVisibility(View.INVISIBLE);
        errorMsg.setVisibility(View.VISIBLE);
        onGettingDevicesCompleted(null);
        if (listener != null) {
            listener.moveToHomeFromTab(getString(R.string.fragment_devices_name_key));
        }
    }

    @Override
    public void onMainError(String msg, boolean triggerSignOut){
        errorMsg.setVisibility(View.VISIBLE);
        devicesRecyclerView.setVisibility(View.INVISIBLE);
    }

    @Override
    public boolean respondToValidationPeriodExceedance() {
        return deviceDetails != null;
    }

    @Override
    public void onEmptyDevicesList() {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.detachView();
        PersonalAdvisorFactory.getInstance().removeObserver(this);
    }

    @Override
    public void onDeviceClicked(DeviceViewModel model, boolean fromDevices) {

        if(getActivity() == null || !isAdded()){
            return;
        }
        
        this.fromDevices = fromDevices;

        attachDeviceDetailsScreen(model);
    }

    @Override
    public void onAdvicesIconClicked(DeviceViewModel model) {

        if (model.hasUnsuppressedAdvices()) {

            PersonalAdvisorDialog dialog = PersonalAdvisorDialog.newInstance(model.getPersonalAdvisorViewModel(),
                    new PersonalAdvisorDialog.PersonalAdvisorDialogCallback() {
                        @Override
                        public void goToPersonalAdvisor(AdviceViewModel adviceViewModel) {
                            onGoToPersonalAdvisorClicked(adviceViewModel);
                        }

                        @Override
                        public void onAdvicePopupDismiss() {

                        }

                        @Override
                        public void onScreenshotCreated(Panel personalAdvisor, Uri storageUri) {
                            shareImage(storageUri);
                        }
                    });

            dialog.show(getFragmentManager(), ADVICE_TAG);
        }
    }

    @Override
    public void onFilterSelected() {

        //Get back to the first index.
        devicesRecyclerView.getContentRecyclerView().smoothScrollToPosition(0);

    }

    private void attachDeviceDetailsScreen(DeviceViewModel model) {

        FragmentTransaction trans = getFragmentManager()
                .beginTransaction();

        switch (businessUnitViewModel.getBusinessUnit()) {
            case INDIGO_PRESS:
                deviceDetails = IndigoDeviceDetailsFragment.newInstance(model);
                ((IndigoDeviceDetailsFragment) deviceDetails).attachListener(this);
                break;
            case LATEX_PRINTER:
                deviceDetails = LatexDeviceDetailsFragment.newInstance(model);
                break;
            default:
                return;
        }

        trans.replace(R.id.root_frame, deviceDetails);
        trans.commit();

        listener.onDevicesDetailedClicked(deviceDetails);
    }

    @Override
    public boolean onBackPressed() {

        if (deviceDetails != null) {

            if (!deviceDetails.onBackPressed()) {
                FragmentTransaction trans = getFragmentManager()
                        .beginTransaction();
                trans.remove(deviceDetails);
                trans.commit();
                deviceDetails = null;
                listener.onDevicesDetailedClosed(this);

                if (!fromDevices) {
                    listener.moveFromDevicesToHome();
                } else {
                    AppseeSdk.getInstance(getActivity()).startScreen(AppseeSdk.SCREEN_DEVICES);
                }
            }

            return true;
        }

        listener.moveFromDevicesToHome();

        return true;
    }

    @Override
    public boolean isFragmentNameToBeDisplayed() {
        return true;
    }

    @Override
    public boolean isFilteringAvailable() {

        if (deviceDetails != null) {
            return deviceDetails.isFilteringAvailable();
        }

        return true;
    }

    @Override
    public boolean isIntercomAccessible() {
        if (deviceDetails != null) {
            return deviceDetails.isIntercomAccessible();
        }

        return true;
    }

    @Override
    public int getToolbarDisplayName() {

        if (deviceDetails != null) {
            return deviceDetails.getToolbarDisplayName();
        }

        if (businessUnitViewModel != null) {
            return businessUnitViewModel.getBusinessUnit() != BusinessUnitEnum.LATEX_PRINTER ? R.string.devices : R.string.printers;
        }

        return R.string.devices;
    }

    public void onRefresh() {
        presenter.onRefresh();
    }

    @Override
    public void onNext10JobsClosed(HPFragment fragment) {
        listener.onDevicesDetailedClicked(fragment);
    }

    @Override
    public void onNext10JobsClicked(HPFragment fragment) {
        listener.onDetailsLevelChanged(fragment);
    }

    @Override
    public void onGoToPersonalAdvisorClicked(AdviceViewModel model) {
        listener.onGoToPersonalAdvisorClicked(model);
    }

    public void onShiftSelected(boolean isShiftSupport) {
        presenter.setShiftSupport(isShiftSupport);
        presenter.loadData(PrintOSApplication.getAppContext());
    }

    @Override
    public void updatePersonalAdvisorObserver() {
        HPLogger.d(TAG, "updating Personal Advisor Observer");
        if(deviceDataList != null){
            PersonalAdvisorFactory personalAdvisorFactory = PersonalAdvisorFactory.getInstance();
            for (DeviceViewModel deviceViewModel : deviceDataList){
                PersonalAdvisorViewModel personalAdvisorViewModel = personalAdvisorFactory
                        .getDeviceAdvices(deviceViewModel.getSerialNumber());
                deviceViewModel.setPersonalAdvisorViewModel(personalAdvisorViewModel);
            }
            if(deviceAdapter != null){
                deviceAdapter.setDeviceData(deviceDataList);
            }
        }
    }

    @Override
    public void onPersonalAdvisorError(APIException e, String tag) {
        //Nothing to do
    }

    public interface DevicesCallbacks {

        void onDevicesDetailedClicked(HPFragment hpFragment);

        void onDetailsLevelChanged(HPFragment hpFragment);

        void onDevicesDetailedClosed(HPFragment hpFragment);

        void moveFromDevicesToHome();

        void onGoToPersonalAdvisorClicked(AdviceViewModel model);

        void moveToHomeFromTab(String string);
    }
}
