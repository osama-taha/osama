package com.hp.printosmobile.presentation.modules.personaladvisor.presenter;

import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.presentation.Presenter;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitViewModel;
import com.hp.printosmobile.presentation.modules.personaladvisor.PersonalAdvisorViewModel;
import com.hp.printosmobilelib.core.communications.remote.APIException;
import com.hp.printosmobilelib.core.logging.HPLogger;

import okhttp3.ResponseBody;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Anwar Asbah on 05/10/2017.
 */
public class PersonalAdvisorPresenter extends Presenter<PersonalAdvisorView> {

    private static final String TAG = PersonalAdvisorPresenter.class.getSimpleName();

    private BusinessUnitViewModel selectedBusinessUnit;

    public void getAdvices() {
        Subscription subscription = PersonalAdvisorDataManager.getAdvices(
                PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).getLanguage(
                        PrintOSApplication.getAppContext().getString(R.string.default_language)), selectedBusinessUnit)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<PersonalAdvisorViewModel>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        HPLogger.d(TAG, "Error while fetching the personal advisor data " + e);
                        onRequestError(e, PersonalAdvisorFactory.TAG);
                    }

                    @Override
                    public void onNext(PersonalAdvisorViewModel personalAdvisorViewModel) {
                        mView.onPersonalAdvisorDataRetrieved(personalAdvisorViewModel);
                    }
                });
        addSubscriber(subscription);
    }

    public void likeAdvice(int adviceId, String device, boolean isLiked) {

        Subscription subscription = PersonalAdvisorDataManager.likeAdvice(adviceId, device, isLiked).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new Subscriber<ResponseBody>() {
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        HPLogger.e(TAG, "Error while like/unlike an advice");
                    }

                    @Override
                    public void onNext(ResponseBody responseBody) {
                    }
                });
        addSubscriber(subscription);
    }

    public void suppressAdvice(int adviceId, String device) {

        Subscription subscription = PersonalAdvisorDataManager.suppressAdvice(adviceId, device).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new Subscriber<ResponseBody>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        HPLogger.d(TAG, "Error while suppressing an advice");
                    }

                    @Override
                    public void onNext(ResponseBody responseBody) {
                    }
                });
        addSubscriber(subscription);
    }

    public void unhideAllAdvices() {

        Subscription subscription = PersonalAdvisorDataManager.unhideAdvices(selectedBusinessUnit.getFiltersViewModel().getSerialNumbers()).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new Subscriber<ResponseBody>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        HPLogger.d(TAG, "Error while unhiding all advices");
                    }

                    @Override
                    public void onNext(ResponseBody responseBody) {
                    }
                });
        addSubscriber(subscription);

    }

    private void onRequestError(Throwable e, String tag) {

        APIException exception = APIException.create(APIException.Kind.UNEXPECTED);
        if (e instanceof APIException) {
            exception = (APIException) e;
        }

        mView.onError(exception, tag);
    }

    public void setSelectedBusinessUnit(BusinessUnitViewModel selectedBusinessUnit) {
        this.selectedBusinessUnit = selectedBusinessUnit;
    }

    public void refresh() {
        stopSubscribers();
    }

    @Override
    public void detachView() {
        stopSubscribers();
        mView = null;
    }

}