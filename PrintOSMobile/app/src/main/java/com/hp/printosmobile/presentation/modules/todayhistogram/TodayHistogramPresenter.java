package com.hp.printosmobile.presentation.modules.todayhistogram;

import android.content.Context;

import com.hp.printosmobile.presentation.Presenter;
import com.hp.printosmobile.presentation.modules.home.HomeDataManager;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitViewModel;
import com.hp.printosmobilelib.core.communications.remote.APIException;
import com.hp.printosmobilelib.core.logging.HPLogger;

import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Anwar Asbah on 05/03/2017.
 */
public class TodayHistogramPresenter extends Presenter<HistogramPresenterView> {

    private static final String TAG = TodayHistogramPresenter.class.getSimpleName();

    public synchronized void getActualVsTargetData(Context context, BusinessUnitViewModel selectedBusinessUnit,
                                                   boolean isShiftSupport) {


        Subscription subscription = HomeDataManager.getActualVsTarget(context, selectedBusinessUnit, isShiftSupport)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<TodayHistogramViewModel>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        HPLogger.d(TAG, "Error while fetching actual vs target data " + e);
                        onRequestError(e);
                    }

                    @Override
                    public void onNext(TodayHistogramViewModel actualVsTargetViewModel) {
                        mView.updateHistogram(actualVsTargetViewModel);
                    }
                });
        addSubscriber(subscription);
    }

    private void onRequestError(Throwable e) {
        APIException exception = APIException.create(APIException.Kind.UNEXPECTED);
        if (e instanceof APIException) {
            exception = (APIException) e;
        }

        mView.onRequestError(exception);
    }

    @Override
    public void detachView() {
        stopSubscribers();
        mView = null;
    }

}