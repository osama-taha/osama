package com.hp.printosmobile.presentation.modules.main;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.support.v4.content.res.ResourcesCompat;

import com.hp.printosmobile.R;
import com.hp.printosmobile.utils.HPUIUtils;

/**
 * Created by Anwar Asbah on 10/6/2016.
 */
public enum ReportKpiEnum {

    BLANKET(R.string.indigo_kpi_blanket_key,
            BusinessUnitEnum.INDIGO_PRESS,
            R.drawable.workweek_bid_kpi_selected,
            R.drawable.workweek_bid_kpi,
            R.color.c52,
            R.string.indigo_kpi_week_blanket,
            R.string.report_y_axis_guidelines,
            false, R.drawable.blanket),
    PIP(R.string.indigo_kpi_pip_key,
            BusinessUnitEnum.INDIGO_PRESS,
            R.drawable.workweek_pip_kpi_selected,
            R.drawable.workweek_pip_kpi,
            R.color.c51,
            R.string.indigo_kpi_week_pip,
            R.string.report_y_axis_guidelines,
            false, R.drawable.pip),
    SUPPLIES_LIFE_SPAN(R.string.indigo_kpi_supplies_lifespan_key,
            BusinessUnitEnum.INDIGO_PRESS,
            R.drawable.workweek_supplied_yield_kpi_selected,
            R.drawable.workweek_supplied_yield_kpi,
            R.color.c20,
            R.string.indigo_kpi_week_parameter_supplies,
            R.string.report_y_axis_guidelines,
            false, R.drawable.supplied_yield),
    RESTARTS(R.string.indigo_kpi_restarts_key,
            BusinessUnitEnum.INDIGO_PRESS,
            R.drawable.workweek_restart_rates_kpi_selected,
            R.drawable.workweek_restart_rates_kpi,
            R.color.c19,
            R.string.indigo_kpi_week_restarts,
            R.string.report_y_axis_guidelines,
            false, R.drawable.restart),
    JAMS(R.string.indigo_kpi_jams_key,
            BusinessUnitEnum.INDIGO_PRESS,
            R.drawable.workweek_jum_rates_kpi_selected,
            R.drawable.workweek_jum_rates_kpi,
            R.color.c17,
            R.string.indigo_kpi_week_jams,
            R.string.report_y_axis_guidelines,
            false, R.drawable.jam),
    FAILURES(R.string.indigo_kpi_failures_key,
            BusinessUnitEnum.INDIGO_PRESS,
            R.drawable.workweek_failur_rates_kpi_selected,
            R.drawable.workweek_failur_rates_kpi,
            R.color.c16,
            R.string.indigo_kpi_week_failures,
            R.string.report_y_axis_guidelines,
            false, R.drawable.failur),
    PRINT_VOLUME_INDIGO(R.string.indigo_kpi_print_volume_key,
            BusinessUnitEnum.INDIGO_PRESS,
            R.drawable.workweek_printing_volume_kpi_selected,
            R.drawable.workweek_printing_volume_kpi,
            R.color.c18,
            R.string.indigo_kpi_week_parameter_volume,
            R.string.report_y_axis_impressions,
            false, R.drawable.printing_volume),
    AVAILABILITY(R.string.indigo_kpi_availability_key,
            BusinessUnitEnum.INDIGO_PRESS,
            R.drawable.workweek_availability_kpi_selected,
            R.drawable.workweek_availability_kpi,
            R.color.c50,
            R.string.indigo_kpi_week_parameter_availability,
            R.string.report_y_axis_up_time,
            true, R.drawable.workweek_availability),
    TECHNICAL(R.string.indigo_kpi_technical_key,
            BusinessUnitEnum.INDIGO_PRESS,
            R.drawable.workweek_technical_kpi_selected,
            R.drawable.workweek_technical_kpi,
            R.color.c53,
            R.string.indigo_kpi_week_parameter_technical,
            R.string.report_y_axis_guidelines,
            false, R.drawable.workweek_technical),
    EFFICIENCY(R.string.pwp_kpi_efficiency_key,
            BusinessUnitEnum.IHPS_PRESS,
            R.drawable.operating_efficiency_kpi_selected,
            R.drawable.operating_efficiency_kpi,
            R.color.c60c,
            R.string.pwp_kpi_week_operating_efficiency,
            R.string.report_y_axis_percents,
            false, R.drawable.operating_efficiency),
    HEALTH(R.string.pwp_kpi_health_key,
            BusinessUnitEnum.IHPS_PRESS,
            R.drawable.system_health_score_kpi_selected,
            R.drawable.system_health_score_kpi,
            R.color.c60b,
            R.string.pwp_kpi_week_system_health,
            R.string.report_y_axis_percents,
            false, R.drawable.system_health_score),
    MAINTENANCE(R.string.pwp_kpi_maintenance_key,
            BusinessUnitEnum.IHPS_PRESS,
            R.drawable.maintenance_score_kpi_selected,
            R.drawable.maintenance_score_kpi,
            R.color.c60b,
            R.string.pwp_kpi_week_parameter_maintenance,
            R.string.report_y_axis_percents,
            false, R.drawable.maintenance_score),
    PAGES(R.string.pwp_kpi_pages_key,
            BusinessUnitEnum.IHPS_PRESS,
            R.drawable.pages_per_run_kpi_selected,
            R.drawable.pages_per_run_kpi,
            R.color.c60d,
            R.string.pwp_kpi_week_pages_perrestart,
            R.string.report_y_axis_count,
            false, R.drawable.pages_per_run),
    PRINT_VOLUME_PWP(R.string.pwp_kpi_print_volume_key,
            BusinessUnitEnum.IHPS_PRESS,
            R.drawable.print_volume_kpi_selected,
            R.drawable.print_volume_kpi,
            R.color.c60e,
            R.string.pwp_kpi_week_parameter_print_volume,
            R.string.report_y_axis_count,
            false, R.drawable.pwp_print_volume),

    AVAILABILITY_SCITEX(R.string.scitex_kpi_availability_key,
            BusinessUnitEnum.SCITEX_PRESS,
            R.drawable.workweek_availability_kpi_selected,
            R.drawable.workweek_availability_kpi,
            R.color.c50,
            R.string.indigo_kpi_week_parameter_availability,
            R.string.report_y_axis_count,
            true, R.drawable.workweek_availability),

    PAPER_JAMS_SCITEX(R.string.scitex_kpi_paper_jams_key,
            BusinessUnitEnum.SCITEX_PRESS,
            R.drawable.workweek_technical_kpi_selected,
            R.drawable.workweek_technical_kpi,
            R.color.c53,
            R.string.scitex_kpi_week_paper_jams,
            R.string.report_y_axis_count,
            false, R.drawable.workweek_technical),

    PRINT_VOLUME_SCITEX(R.string.scitex_kpi_print_volume_key,
            BusinessUnitEnum.SCITEX_PRESS,
            R.drawable.workweek_printing_volume_kpi_selected,
            R.drawable.workweek_printing_volume_kpi,
            R.color.c18,
            R.string.indigo_kpi_week_parameter_volume,
            R.string.report_y_axis_count,
            false, R.drawable.printing_volume),

    PRINT_VOLUME_LATEX(R.string.latex_kpi_print_volume_key,
            BusinessUnitEnum.LATEX_PRINTER,
            R.drawable.workweek_printing_volume_kpi_selected,
            R.drawable.workweek_printing_volume_kpi,
            R.color.c18,
            R.string.indigo_kpi_week_parameter_volume,
            R.string.report_y_axis_count,
            false, R.drawable.printing_volume),

    FAILURES_LATEX(R.string.latex_kpi_failures_key,
            BusinessUnitEnum.LATEX_PRINTER,
            R.drawable.workweek_failur_rates_kpi_selected,
            R.drawable.workweek_failur_rates_kpi,
            R.color.c16,
            R.string.indigo_kpi_week_failures,
            R.string.report_y_axis_count,
            false, R.drawable.failur),

    MAINTENANCE_LATEX(R.string.latex_kpi_maintenance_key,
            BusinessUnitEnum.LATEX_PRINTER,
            R.drawable.workweek_maintenance_kpi_selected,
            R.drawable.workweek_maintenance_kpi,
            R.color.c92,
            R.string.indigo_kpi_week_parameter_maitanance,
            R.string.report_y_axis_count,
            false, R.drawable.workweek_maintenance),

    UTILIZATION_LATEX(R.string.latex_kpi_utilization_key,
            BusinessUnitEnum.LATEX_PRINTER,
            R.drawable.maintenance_score_kpi_selected,
            R.drawable.maintenance_score_kpi,
            R.color.c60b,
            R.string.latex_kpi_week_utilization,
            R.string.report_y_axis_percents,
            false, R.drawable.maintenance_score),

    UNKNOWN(R.string.unknown_value,
            BusinessUnitEnum.UNKNOWN,
            -1,
            -1,
            -1,
            -1,
            R.string.report_y_axis_points,
            false
            , R.drawable.generic);

    private int keyNameStringID;
    private int localizedNameStringID;
    private int selectedIconDrawableID;
    private int iconDrawableID;
    private int color;
    private boolean isBarChar;
    private int yAxisStringID;
    private final int weekPanelKpiIcon;
    private BusinessUnitEnum businessUnitEnum;
    private static final int UNKNOWN_ID = -1;

    ReportKpiEnum(int keyNameStringID, BusinessUnitEnum businessUnitEnum, int selectedIconDrawableID,
                  int iconDrawableID, int color, int localizedNameStringID, int yAxisStringID, boolean isBarChar, int weekPanelKpiIcon) {
        this.keyNameStringID = keyNameStringID;
        this.localizedNameStringID = localizedNameStringID;
        this.selectedIconDrawableID = selectedIconDrawableID;
        this.iconDrawableID = iconDrawableID;
        this.color = color;
        this.businessUnitEnum = businessUnitEnum;
        this.isBarChar = isBarChar;
        this.yAxisStringID = yAxisStringID;
        this.weekPanelKpiIcon = weekPanelKpiIcon;
    }

    public String getKeyName(Context context) {
        return context.getString(keyNameStringID);
    }

    public String getLocalizedNameString(Context context, String defaultValue) {
        return localizedNameStringID != UNKNOWN_ID ?
                context.getString(localizedNameStringID) : defaultValue;
    }

    public Drawable getSelectedIconDrawable(Resources resources) {
        return ResourcesCompat.getDrawable(resources,
                selectedIconDrawableID == UNKNOWN_ID ?
                        R.drawable.workweek_generic_kpi_selected : selectedIconDrawableID, null);
    }

    public Drawable getIconDrawable(Resources resources) {
        return ResourcesCompat.getDrawable(resources,
                iconDrawableID == UNKNOWN_ID ?
                        R.drawable.workweek_generic_kpi : iconDrawableID, null);
    }

    public int getColor(Resources resources) {
        if (color == UNKNOWN_ID) {
            return HPUIUtils.getRandomColor();
        }
        return ResourcesCompat.getColor(resources, color, null);
    }

    public String getYAxisLabel(Context context) {
        return context.getString(yAxisStringID);
    }

    public BusinessUnitEnum getBusinessUnitEnum() {
        return businessUnitEnum;
    }

    public boolean isBarChar() {
        return isBarChar;
    }

    public int getWeekPanelKpiIcon() {
        return weekPanelKpiIcon;
    }

    public static ReportKpiEnum from(Context context, String name, BusinessUnitEnum businessUnitEnum) {
        for (ReportKpiEnum kpi : ReportKpiEnum.values()) {
            if (businessUnitEnum == kpi.getBusinessUnitEnum() &&
                    kpi.getKeyName(context).toLowerCase().equals(name.toLowerCase()))
                return kpi;
        }
        return ReportKpiEnum.UNKNOWN;
    }
}
