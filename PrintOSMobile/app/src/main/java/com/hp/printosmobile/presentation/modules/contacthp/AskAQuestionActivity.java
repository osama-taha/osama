package com.hp.printosmobile.presentation.modules.contacthp;

import android.net.Uri;

import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.AppseeSdk;
import com.hp.printosmobile.IntercomSdk;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.modules.contacthp.shared.AttachImageView;
import com.hp.printosmobile.presentation.modules.contacthp.shared.ContactHpContactThroughView;
import com.hp.printosmobile.presentation.modules.contacthp.shared.ContactHpTextField;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;
import com.hp.printosmobile.utils.HPUIUtils;

import java.util.List;

import butterknife.Bind;

/**
 * Created be Anwar Asbah 10/11/2016
 */
public class AskAQuestionActivity extends ContactHPBaseActivity {

    private static final String TAG = AskAQuestionActivity.class.getSimpleName();

    @Bind(R.id.attach_image_view)
    AttachImageView attachImageView;
    @Bind(R.id.question_text_view)
    ContactHpTextField questionTextView;
    @Bind(R.id.subject_text_view)
    ContactHpTextField subjectTextView;
    @Bind(R.id.contact_through_view)
    ContactHpContactThroughView contactThroughView;

    @Override
    protected int getLayoutResource() {
        return R.layout.contact_hp_ask_a_question;
    }

    @Override
    protected void init() {
        IntercomSdk.getInstance(this).logEvent(IntercomSdk.PBM_VIEW_ASK_EVENT);

        attachImageView.addAttachImageClickListener(this);
        questionTextView.setTextFieldRightDrawable(null);
    }

    @Override
    protected void hideSoftKeyboard() {
        HPUIUtils.hideSoftKeyboard(this,
                subjectTextView.getAutoCompleteTextView(),
                questionTextView.getAutoCompleteTextView());
    }

    @Override
    public int getTitleResourceID() {
        return R.string.hp_contact_ask_a_question_title;
    }

    @Override
    public void sendRequest() {
        List<Uri> uriList = attachImageView.getAttachedImagesUri();

        if (uriList == null || uriList.size() == 0) {
            Analytics.sendEvent(Analytics.MOBILE_PRINT_BEAT_CATEGORY, Analytics.ASK_THE_EXPERT_SEND_ACTION);
        } else {
            Analytics.sendEvent(Analytics.MOBILE_PRINT_BEAT_CATEGORY, Analytics.ASK_THE_EXPERT_SEND_ACTION, Analytics.CONTACT_HP_SCREENSHOT_LABEL);
        }


        presenter.askQuestion(this,
                subjectTextView.getText(),
                questionTextView.getText(),
                uriList,
                contactThroughView.getContactOption());

        IntercomSdk.getInstance(this).logEvent(IntercomSdk.PBM_SEND_QUESTION_EVENT);
        AppseeSdk.getInstance(this).sendEvent(AppseeSdk.EVENT_SEND_QUESTION);
    }

    @Override
    public void setFormEnable(boolean isEnabled) {
        attachImageView.setEnabled(isEnabled);
        questionTextView.setEnabled(isEnabled);
        subjectTextView.setEnabled(isEnabled);
        contactThroughView.setEnabled(isEnabled);
    }

    @Override
    public void setHasPhone(boolean hasPhone) {
        contactThroughView.setHasPhone(hasPhone);
    }

    @Override
    public boolean isFormFilled() {
        String subject = subjectTextView.getText();
        String question = questionTextView.getText();

        return displayWarningMessage(subject, R.string.hp_contact_fill_subject_warning)
                && displayWarningMessage(question, R.string.hp_contact_fill_question_warning);
    }
}
