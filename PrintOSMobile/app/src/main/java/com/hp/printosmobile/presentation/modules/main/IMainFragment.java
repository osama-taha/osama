package com.hp.printosmobile.presentation.modules.main;

import java.util.Map;

/**
 * This interface must be implemented by the {@link MainActivity } fragments
 * in order to handle different operations.
 *
 * @author Osama Taha
 */
public interface IMainFragment {

    void onBusinessUnitSelected(BusinessUnitViewModel businessUnitViewModel);

    void onFiltersChanged(BusinessUnitViewModel businessUnitViewModel);

    void didFinishGettingBusinessUnits(Map<BusinessUnitEnum, BusinessUnitViewModel> businessUnitsMap);

    void onMainError (String msg, boolean triggerSignOut);

    boolean respondToValidationPeriodExceedance ();
}
