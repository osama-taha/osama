package com.hp.printosmobile.presentation.modules.personaladvisor.presenter;

import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.IntercomSdk;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitViewModel;
import com.hp.printosmobile.presentation.modules.personaladvisor.AdviceViewModel;
import com.hp.printosmobile.presentation.modules.personaladvisor.PersonalAdvisorViewModel;
import com.hp.printosmobilelib.core.communications.remote.APIException;
import com.hp.printosmobilelib.core.logging.HPLogger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Anwar Asbah on 5/10/2017.
 */

public class PersonalAdvisorFactory implements PersonalAdvisorView {

    public static final String TAG = "PersonalAdvisorFactory";
    public static final String TAG_LIKE = "LIKE";
    public static final String TAG_SUPPRESS = "suppress";

    private static PersonalAdvisorFactory instance;

    private List<PersonalAdvisorObserver> observers;
    private PersonalAdvisorViewModel personalAdvisorViewModel;
    private PersonalAdvisorViewModel filteredAdviceViewModel;
    private Map<String, PersonalAdvisorViewModel> advicesDevicesMap;
    private PersonalAdvisorPresenter personalAdvisorPresenter;
    private BusinessUnitViewModel businessUnitViewModel;

    public static PersonalAdvisorFactory getInstance() {
        if (instance == null) {
            instance = new PersonalAdvisorFactory();
        }
        return instance;
    }

    private PersonalAdvisorFactory() {
        observers = new ArrayList<>();
    }

    public void loadData(BusinessUnitViewModel businessUnitViewModel) {
        this.businessUnitViewModel = businessUnitViewModel;
        if (businessUnitViewModel != null) {
            if (personalAdvisorPresenter == null) {
                personalAdvisorPresenter = new PersonalAdvisorPresenter();
                personalAdvisorPresenter.attachView(this);
            }
            personalAdvisorPresenter.setSelectedBusinessUnit(businessUnitViewModel);
            personalAdvisorPresenter.getAdvices();
        }
    }

    public void addObserver(PersonalAdvisorObserver observer) {
        if (observers == null) {
            observers = new ArrayList<>();
        }

        if (!observers.contains(observer) && observer != null) {
            HPLogger.d(TAG, "adding observer: " + observer.getClass().getSimpleName());
            observers.add(observer);
            observer.updatePersonalAdvisorObserver();
        }
    }

    public PersonalAdvisorViewModel getFilteredAdvises() {
        return filteredAdviceViewModel;
    }

    public PersonalAdvisorViewModel getDeviceAdvices(String deviceID) {
        if (advicesDevicesMap != null && advicesDevicesMap.containsKey(deviceID)) {
            return advicesDevicesMap.get(deviceID);
        }
        return null;
    }

    public void likeAdvice(AdviceViewModel adviceViewModel, boolean isLiked) {
        Analytics.sendEvent(Analytics.MOBILE_PRINT_BEAT_CATEGORY, Analytics.PERSONAL_ADVISOR_ACTION,
                isLiked ? Analytics.PERSONAL_ADVISOR_LIKE_LABEL : Analytics.PERSONAL_ADVISOR_UNLIKE_LABEL);

        if (isLiked) {
            IntercomSdk.getInstance(PrintOSApplication.getAppContext()).logEvent(IntercomSdk.PBM_PA_LIKE_EVENT);
        }

        if (adviceViewModel != null) {
            HPLogger.d(TAG, "likeAdvice id: " + adviceViewModel.getAdviseId() + " deviceSerialNum: " +
                    adviceViewModel.getDeviceSerialNumber() + " liked: " + isLiked);

            adviceViewModel.setLiked(isLiked);
            if (personalAdvisorPresenter != null) {
                personalAdvisorPresenter.likeAdvice(adviceViewModel.getAdviseId(), adviceViewModel.getDeviceSerialNumber(), isLiked);
            }
            notifyAllObservers();
        }
    }

    public void suppressAdvice(AdviceViewModel adviceViewModel) {
        IntercomSdk.getInstance(PrintOSApplication.getAppContext()).logEvent(IntercomSdk.PBM_PA_SUPPRESS_EVENT);
        Analytics.sendEvent(Analytics.MOBILE_PRINT_BEAT_CATEGORY, Analytics.PERSONAL_ADVISOR_ACTION, Analytics.PERSONAL_ADVISOR_DELETE_LABEL);

        if (adviceViewModel != null) {

            HPLogger.d(TAG, "suppressAdvice id: " + adviceViewModel.getAdviseId() + " deviceSerialNum: " +
                    adviceViewModel.getDeviceSerialNumber());

            adviceViewModel.setSuppressed(true);
            if (personalAdvisorPresenter != null) {
                personalAdvisorPresenter.suppressAdvice(adviceViewModel.getAdviseId(), adviceViewModel.getDeviceSerialNumber());
            }
            notifyAllObservers();
        }
    }

    public void unhideAllAdvices() {

        Analytics.sendEvent(Analytics.MOBILE_PRINT_BEAT_CATEGORY, Analytics.PERSONAL_ADVISOR_ACTION,
                Analytics.PERSONAL_ADVISOR_UNHIDE_ALL);
        if (filteredAdviceViewModel != null) {
            HPLogger.d(TAG, "Unhide All Advices");
            if (personalAdvisorPresenter != null) {
                personalAdvisorPresenter.unhideAllAdvices();
            }
            filteredAdviceViewModel.unSuppressAll();
            notifyAllObservers();
        }
    }

    private void notifyAllObservers() {
        if (observers != null) {
            HPLogger.d(TAG, "notify All Observers");
            for (PersonalAdvisorObserver observer : observers) {
                if (observer != null) {
                    observer.updatePersonalAdvisorObserver();
                }
            }
        }
    }

    @Override
    public void onPersonalAdvisorDataRetrieved(PersonalAdvisorViewModel personalAdvisorViewModel) {
        this.personalAdvisorViewModel = personalAdvisorViewModel;
        sortAdvicesByDevices();
        setFilteredAdvices();
        if (observers != null) {
            for (PersonalAdvisorObserver observer : observers) {
                if (observer != null) {
                    observer.updatePersonalAdvisorObserver();
                }
            }
        }
    }

    @Override
    public void onError(APIException exception, String tag) {
        if (observers != null) {
            for (PersonalAdvisorObserver observer : observers) {
                if (observer != null) {
                    observer.onPersonalAdvisorError(exception, tag);
                }
            }
        }
    }

    private void setFilteredAdvices() {
        List<String> serialNumbers = businessUnitViewModel.getFiltersViewModel().getSerialNumbers();
        if (serialNumbers != null) {
            filteredAdviceViewModel = new PersonalAdvisorViewModel();
            List<AdviceViewModel> adviceViewModels = new ArrayList<>();
            for (String serialNumber : serialNumbers) {
                if (advicesDevicesMap.containsKey(serialNumber)) {
                    PersonalAdvisorViewModel model = advicesDevicesMap.get(serialNumber);
                    if (model != null && model.getAllAdvices() != null) {
                        adviceViewModels.addAll(model.getAllAdvices());
                    }
                }
            }
            filteredAdviceViewModel.setAdvices(adviceViewModels);
        }
    }

    private void sortAdvicesByDevices() {
        if (personalAdvisorViewModel != null && personalAdvisorViewModel.getAllAdvices() != null) {

            Map<String, List<AdviceViewModel>> advicesMap = new HashMap<>();
            for (AdviceViewModel adviceViewModel : personalAdvisorViewModel.getAllAdvices()) {
                List<AdviceViewModel> adviceList;
                if (!advicesMap.containsKey(adviceViewModel.getDeviceSerialNumber())) {
                    adviceList = new ArrayList<>();
                    advicesMap.put(adviceViewModel.getDeviceSerialNumber(), adviceList);
                } else {
                    adviceList = advicesMap.get(adviceViewModel.getDeviceSerialNumber());
                }
                adviceList.add(adviceViewModel);
            }

            advicesDevicesMap = new HashMap<>();
            for (String key : advicesMap.keySet()) {
                PersonalAdvisorViewModel pressAdvices = new PersonalAdvisorViewModel();
                pressAdvices.setAdvices(advicesMap.get(key));
                advicesDevicesMap.put(key, pressAdvices);
            }
        }
    }

    public void removeObserver(PersonalAdvisorObserver observer) {
        if (observers != null && observers.contains(observer)) {
            HPLogger.d(TAG, "remove Observer: " + observer.getClass().getSimpleName());
            observers.remove(observer);
        }
    }

    public interface PersonalAdvisorObserver {
        void updatePersonalAdvisorObserver();

        void onPersonalAdvisorError(APIException e, String tag);
    }
}
