package com.hp.printosmobile.presentation.modules.todayhistogram;

import com.hp.printosmobile.presentation.modules.home.ActiveShiftsViewModel;
import com.hp.printosmobile.presentation.modules.home.ShiftViewModel;
import com.hp.printosmobile.presentation.modules.today.TodayViewModel;
import com.hp.printosmobile.presentation.modules.todayhistogram.TodayHistogramViewModel.TodayHistogramItem;

import java.util.Calendar;
import java.util.List;

/**
 * Created by Anwar Asbah on 5/7/2017.
 */

public class HistogramUtility {

    public static void appendTodayDataToHistogramData(TodayHistogramViewModel todayHistogramViewModel,
                                                      TodayViewModel todayViewModel,
                                                      ActiveShiftsViewModel activeShiftsViewModel) {

        if (todayHistogramViewModel != null) {

            List<TodayHistogramItem> histogramViewModels = todayHistogramViewModel.getData();

            TodayHistogramItem todayHistogramItem = new TodayHistogramItem();
            todayHistogramItem.setDayOrdinal(Calendar.getInstance().get(Calendar.DAY_OF_WEEK));
            todayHistogramItem.setActual((int) todayViewModel.getPrintVolumeValue());
            todayHistogramItem.setDate(Calendar.getInstance().getTime());
            todayHistogramItem.setMeters(todayViewModel.getMeters());
            todayHistogramItem.setMetersSquare(todayViewModel.getSquareMeters());
            todayHistogramItem.setSheets(todayViewModel.getSheets());
            todayHistogramItem.setTarget((int) todayViewModel.getPrintVolumeIntraDailyTargetValue());
            todayHistogramItem.setStatus(TodayHistogramItem.TodayHistogramItemStatus.getStatus(
                    todayHistogramItem.getActual(),
                    todayHistogramItem.getTarget(),
                    todayHistogramViewModel.getLowerThreshold(),
                    todayHistogramViewModel.getHigherThreshold()
            ));

            if (todayHistogramViewModel.isShiftSupport()) {
                if (activeShiftsViewModel != null && activeShiftsViewModel.getShiftViewModels() != null) {
                    for (ShiftViewModel shiftViewModel : activeShiftsViewModel.getShiftViewModels()) {
                        if (shiftViewModel.getShiftType() == ShiftViewModel.ShiftType.CURRENT) {
                            ShiftViewModel todayShiftModel = new ShiftViewModel();

                            todayShiftModel.setShiftId(shiftViewModel.getShiftId());
                            todayShiftModel.setShiftName(shiftViewModel.getShiftName());
                            todayShiftModel.setShiftType(shiftViewModel.getShiftType());
                            todayShiftModel.setStartDay(shiftViewModel.getStartDay());
                            todayShiftModel.setStartTime(shiftViewModel.getStartTime());
                            todayShiftModel.setStartHour(shiftViewModel.getStartHour());
                            todayShiftModel.setEndDay(shiftViewModel.getEndDay());
                            todayShiftModel.setEndHour(shiftViewModel.getEndHour());
                            todayShiftModel.setPresentedDay(shiftViewModel.getPresentedDay());

                            todayHistogramItem.setShift(todayShiftModel);
                            break;
                        }
                    }
                }

                if (todayHistogramItem.getShift() == null) {
                    return;
                }
            }

            histogramViewModels.add(todayHistogramItem);
        }
    }
}
