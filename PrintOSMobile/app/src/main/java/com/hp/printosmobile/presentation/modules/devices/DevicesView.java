package com.hp.printosmobile.presentation.modules.devices;

import com.hp.printosmobile.presentation.modules.shared.BaseDeviceViewModel;
import com.hp.printosmobile.presentation.modules.shared.DeviceViewModel;

import java.util.List;

/**
 * Created by Osama Taha on 5/23/16.
 */
public interface DevicesView {

    void hideLoading();

    void showLoading();

    void onGettingDevicesCompleted(List<DeviceViewModel> deviceDataList);

    void onError();

    void onEmptyDevicesList();
}
