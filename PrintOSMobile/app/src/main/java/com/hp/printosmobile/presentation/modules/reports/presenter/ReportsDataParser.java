package com.hp.printosmobile.presentation.modules.reports.presenter;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.support.v4.content.res.ResourcesCompat;

import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.remote.models.ReportData;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;
import com.hp.printosmobile.presentation.modules.main.ReportKpiEnum;
import com.hp.printosmobile.presentation.modules.main.ReportTypeEnum;
import com.hp.printosmobile.presentation.modules.reports.ReportKpiViewModel;
import com.hp.printosmobile.utils.HPUIUtils;
import com.hp.printosmobilelib.core.utils.HPDateUtils;
import com.hp.printosmobilelib.ui.widgets.TypefaceManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A class responsible for converting and building the KPIs for performance chart.
 * Created by Osama Taha on 6/18/2015.
 */
public class ReportsDataParser {

    private static final String UP_TIME_KEY = "up time";
    private static final String PRINTING_TIME_KEY = "print time";
    private static final String AVAILABLE_TIME_KEY = "available time";

    public static ReportKpiViewModel initKPI(Context context, String kpiName, BusinessUnitEnum businessUnitEnum) {
        if (kpiName == null) {
            return null;
        }

        ReportKpiEnum kpiEnum = ReportKpiEnum.from(context, kpiName, businessUnitEnum);
        ReportKpiViewModel kpi = new ReportKpiViewModel();
        Resources resources = context.getResources();

        kpi.setName(kpiName);
        kpi.setLocalizedName(kpiEnum.getLocalizedNameString(context, kpiName));
        kpi.setIconUp(kpiEnum.getIconDrawable(resources));
        kpi.setIconDown(kpiEnum.getSelectedIconDrawable(resources));
        kpi.setColor(kpiEnum.getColor(resources));
        kpi.setFillArea(true);
        kpi.setCircleColor(ResourcesCompat.getColor(resources, R.color.reports_value_circle_color, null));
        kpi.setCircleHoleColor(ResourcesCompat.getColor(resources, R.color.reports_value_circle_hole_color, null));
        kpi.setLineWidth(resources.getDimension(R.dimen.reports_line_width));
        kpi.setDrawCircleHole(true);
        kpi.setHighLightColor(Color.WHITE);
        kpi.setValueTextColor(ResourcesCompat.getColor(resources, R.color.reports_value_text_color, null));
        kpi.setValueTextSize(resources.getInteger(R.integer.kpi_text_size));
        kpi.setValueTypeFace(TypefaceManager.getTypeface(context, TypefaceManager.HPTypeface.HP_REGULAR));
        kpi.setYAxisLabel(kpiEnum.getYAxisLabel(context));

        if (kpiEnum.isBarChar()) {
            kpi.setDefaultView(ReportKpiViewModel.ReportView.BAR);
        }

        return kpi;
    }

    /**
     * Build a list of kpi models to be used for populating the chart.
     */
    public static ArrayList<ReportKpiViewModel> parseReportData(Context context, ReportData reportData, String dateFormat, boolean isSingleKpi, BusinessUnitEnum businessUnitEnum, ReportTypeEnum reportTypeEnum) {
        HPUIUtils.resetRandomColorIndex();
        if (reportData == null || reportData.getUnitEvents() == null || reportData.getUnitEvents().size() == 0) {
            return null;
        }

        ArrayList<ReportKpiViewModel> kpiList = new ArrayList<>();
        List<Entry> prevEntries = null;

        for (ReportData.UnitEvent kpiData : reportData.getUnitEvents()) {
            if (kpiData.getEvents() == null || kpiData.getEvents().size() == 0) {
                continue;
            }

            List<Entry> accumulativeEntries = new ArrayList<>();
            List<Entry> entries = new ArrayList<>();
            List<BarEntry> barEntries = new ArrayList<>();

            for (ReportData.UnitEvent.Event event : kpiData.getEvents()) {
                int index = kpiData.getEvents().indexOf(event);
                entries.add(new Entry(
                        ReportTypeEnum.COUNT == reportTypeEnum ? event.getCount()
                                : ReportTypeEnum.RATE == reportTypeEnum ? event.getRate()
                                : ReportTypeEnum.VALUE == reportTypeEnum ? event.getValue()
                                : ReportTypeEnum.WEIGHT == reportTypeEnum ? event.getWeight()
                                : ReportTypeEnum.GUIDELINES_PERCENT == reportTypeEnum ? event.getGuidelinesPercent()
                                : event.getValue(), index));
                barEntries.add(new BarEntry(getBarEntries(event), index));

                if (prevEntries == null) {
                    accumulativeEntries.add(new Entry(entries.get(index).getVal(), index));
                } else {
                    accumulativeEntries.add(new Entry(
                            (index < prevEntries.size() ? prevEntries.get(index).getVal() : 0)
                                    + entries.get(index).getVal(), index)
                    );
                }
            }

            prevEntries = accumulativeEntries;
            List<String> weeks = getWeeksInRange(kpiData.getEvents(), dateFormat);

            ReportKpiViewModel kpi = initKPI(context, isSingleKpi ? reportData.getKpiName() : kpiData.getUnitName(), businessUnitEnum);
            kpi.setAccumulativeValues(accumulativeEntries);
            kpi.setBarEntries(barEntries);
            kpi.setOverallValues(entries);
            kpi.setXAxisValues(weeks);
            kpi.setUnitEvents(kpiData.getEvents());

            kpiList.add(kpi);
        }

        return kpiList;
    }

    private static float[] getBarEntries(ReportData.UnitEvent.Event event) {
        if (event.getValues() == null) return new float[0];

        Map<String, Double> valuesMap = new HashMap<>();
        for (ReportData.UnitEvent.Event.Value value : event.getValues()) {
            valuesMap.put(value.getName(), value.getValue());
        }

        if (valuesMap.size() == 0) {
            return new float[]{event.getValue()};

        } else if (!valuesMap.containsKey(UP_TIME_KEY) || !valuesMap.containsKey(PRINTING_TIME_KEY)
                || !valuesMap.containsKey(AVAILABLE_TIME_KEY)) {
            return new float[]{event.getValue()};
        } else {
            double upTime = valuesMap.get(UP_TIME_KEY);
            upTime = upTime == 0 ? 1 : upTime;

            float printTimeValue = (float) ((valuesMap.get(PRINTING_TIME_KEY) / upTime) * 100);
            float availableTimeValue = (float) ((valuesMap.get(AVAILABLE_TIME_KEY) / upTime) * 100);

            float minValue = Math.min(printTimeValue, availableTimeValue);
            float maxValue = Math.max(printTimeValue, availableTimeValue);

            return new float[]{minValue, maxValue - minValue};
        }
    }

    public static void getWeekPoints(ReportKpiViewModel kpi) {
        if (kpi.isAggregate()) {
            if (kpi.getSubKPIs() != null) {
                float sum = 0;
                for (ReportKpiViewModel kpiModel : kpi.getSubKPIs()) {
                    List<Entry> entries = kpiModel.getKpiValues();
                    if (entries != null && entries.size() > 0) {
                        sum += entries.get(entries.size() - 1).getVal();
                    }
                }

                kpi.setWeekPoints((int) sum);
            }
        } else {
            List<Entry> entries = kpi.getKpiValues();
            if (entries != null && entries.size() > 0) {
                kpi.setWeekPoints((int) entries.get(entries.size() - 1).getVal());
            }
        }

    }

    public static List<String> getWeeksInRange(List<ReportData.UnitEvent.Event> events, String format) {

        List<String> weeks = new ArrayList<>();

        for (ReportData.UnitEvent.Event event : events) {

            int week;
            //Handle three different values might come from the server.
            try {
                //Check if the init data is integer.
                week = Integer.parseInt(event.getHappenedOn());
            } catch (NumberFormatException e) {
                try {
                    //If failed then parse the event name.
                    week = Integer.parseInt(event.getEventName());
                } catch (NumberFormatException ex) {
                    //Or parse the HappenedOn property as date to get the week number.
                    week = HPDateUtils.getWeekOfYear(event.getHappenedOn(), format);
                }
            }
            weeks.add(String.valueOf(week));
        }
        return weeks;
    }
}