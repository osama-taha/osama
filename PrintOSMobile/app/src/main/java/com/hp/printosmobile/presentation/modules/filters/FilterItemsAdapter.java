package com.hp.printosmobile.presentation.modules.filters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;

import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hp.printosmobile.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Osama Taha on 10/9/16.
 */
public class FilterItemsAdapter extends RecyclerView.Adapter<FilterItemsAdapter.ViewHolder> {

    private List<FilterItem> mItems;
    private SparseBooleanArray mSelectedItems;
    private final FilterItemsAdapterCallbacks mListener;
    private Context context;

    public FilterItemsAdapter(Context context, List<FilterItem> mItems, FilterItemsAdapterCallbacks filterItemsAdapterCallbacks) {
        this.context = context;
        this.mSelectedItems = new SparseBooleanArray();
        this.mItems = mItems;
        this.mListener = filterItemsAdapterCallbacks;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.filter_item_layout, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.mItem = mItems.get(position);

        holder.itemNameTextView.setText(holder.mItem.getName());

        if (!(holder.mItem instanceof DeviceFilterViewModel)) {
            holder.itemView.setBackgroundResource(R.drawable.list_item_bg_color);
            holder.itemNameTextView.setTextColor(ContextCompat.getColorStateList(context, R.color.filter_list_group_site_item_text_color));
        } else {
            holder.itemNameTextView.setTextColor(ContextCompat.getColorStateList(context, R.color.filter_device_item_text_color_selector));
        }

        holder.setSelected(mSelectedItems.get(position, holder.mItem.isSelected()));

        holder.itemNameTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    holder.setSelected(!holder.getSelected());
                    mListener.onItemSelected(holder.getSelected(), holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mItems == null ? 0 : mItems.size();
    }

    public void addItems(List<FilterItem> items) {

        if (mItems == null) {
            mItems = new ArrayList<>();
        }

        mItems.clear();
        mSelectedItems.clear();
        mItems.addAll(items);
        notifyDataSetChanged();

    }

    public void selectAll() {

        for (FilterItem filterItem : mItems) {
            mSelectedItems.put(mItems.indexOf(filterItem), true);
        }
        notifyDataSetChanged();
    }

    public void clearSelections() {
        if (mSelectedItems != null) {
            mSelectedItems.clear();
            notifyDataSetChanged();
        }
    }

    public void selectItem(FilterItem filterItem, boolean select) {

        if (mItems != null && filterItem != null) {
            int index = mItems.indexOf(filterItem);
            if (index > -1) {
                mSelectedItems.put(index, select);
                notifyDataSetChanged();
            }
        }
    }

    public void selectRange(List<FilterItem> items) {

        if (items != null && mSelectedItems != null) {
            mSelectedItems.clear();
            for (int i = 0; i < items.size(); i++) {
                int index = mItems.indexOf(items.get(i));
                if (index > -1) {
                    mSelectedItems.put(index, true);
                }
            }
        }
        notifyDataSetChanged();
    }

    public List<FilterItem> getSelectedItems() {

        List<FilterItem> items = new ArrayList();
        for (int i = 0; i < mSelectedItems.size(); i++) {
            if (mSelectedItems.valueAt(i)) {
                FilterItem element = mItems.get(mSelectedItems.keyAt(i));
                items.add(element);
            }
        }

        return items;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements IFilterViewHolder {

        public final View mView;
        public final TextView itemNameTextView;
        private final ImageView checkMarkImageView;
        public FilterItem mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            itemNameTextView = (TextView) view.findViewById(R.id.item_name_text_view);
            checkMarkImageView = (ImageView) view.findViewById(R.id.checkmark);
        }

        @Override
        public void setText(String v) {
            itemNameTextView.setText(v);
        }

        @Override
        public void setSelected(boolean selected) {

            mView.setSelected(selected);
            itemNameTextView.setSelected(selected);
            if (mItem instanceof DeviceFilterViewModel) {
                checkMarkImageView.setVisibility(selected ? View.VISIBLE : View.INVISIBLE);
            } else {
                checkMarkImageView.setVisibility(View.INVISIBLE);
            }

        }

        @Override
        public boolean getSelected() {
            return itemNameTextView.isSelected();
        }

        @Override
        public void updateView(int position) {
            setSelected(mSelectedItems.get(position, false));
        }

    }

    public interface FilterItemsAdapterCallbacks {

        void onItemSelected(boolean selected, FilterItem mItem);
    }


    public interface IFilterViewHolder {

        void setText(String text);

        void setSelected(boolean selected);

        boolean getSelected();

        void updateView(int position);
    }

}
