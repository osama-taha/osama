package com.hp.printosmobile.data.remote.models;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class NotificationSubscription {

    @JsonProperty("userSubscriptionList")
    private ArrayList<Subscription> subscriptions;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();


    @JsonProperty("userSubscriptionList")
    public ArrayList<Subscription> getSubscriptions() {
        return subscriptions;
    }

    @JsonProperty("userSubscriptionList")
    public void setSubscriptions(ArrayList<Subscription> subscriptions) {
        this.subscriptions = subscriptions;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public static class Subscription {
        @JsonProperty("eventDescriptionId")
        private String eventDescriptionId;
        @JsonProperty("userSubscriptionId")
        private String userSubscriptionId;
        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<String, Object>();

        @JsonProperty("eventDescriptionId")
        public String getEventDescriptionId() {
            return eventDescriptionId;
        }

        @JsonProperty("eventDescriptionId")
        public void setEventDescriptionId(String eventDescriptionId) {
            this.eventDescriptionId = eventDescriptionId;
        }

        @JsonProperty("userSubscriptionId")
        public String getUserSubscriptionId() {
            return userSubscriptionId;
        }

        @JsonProperty("userSubscriptionId")
        public void setUserSubscriptionId(String userSubscriptionId) {
            this.userSubscriptionId = userSubscriptionId;
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }
    }

}
