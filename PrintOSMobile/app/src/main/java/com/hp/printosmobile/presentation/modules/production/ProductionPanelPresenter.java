package com.hp.printosmobile.presentation.modules.production;

import android.content.Context;
import com.hp.printosmobilelib.core.logging.HPLogger;

import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.data.remote.models.ProductionData;
import com.hp.printosmobile.data.remote.services.ProductionFetchDataHelper;
import com.hp.printosmobile.presentation.Presenter;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by leviasaf on 07/05/2016.
 */
public class ProductionPanelPresenter extends Presenter<IProductionView> {

    private static String TAG = ProductionPanelPresenter.class.getSimpleName();
    private CompositeSubscription mCompositeSubscription = new CompositeSubscription();

    ProductionFetchDataHelper mController;

    public ProductionPanelPresenter(Context context) {
        mController= ProductionFetchDataHelper.getInstance(context, PrintOSApplication.getApiServicesProvider());
        mCompositeSubscription.add(mController.getFetchDataRxSubject().observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<ProductionData>() {
                    @Override
                    public void onCompleted() {}
                    @Override
                    public void onError(Throwable e) {}
                    @Override
                    public void onNext(ProductionData productionDataResponse) {
                        HPLogger.d(TAG,"Fetch complete ProductionPanelPresenter");

                        mView.setNumberOfJobs(productionDataResponse.getPrintedJobs());
                    }
                }));
    }

    @Override
    public void detachView() {
        mView = null;
        mCompositeSubscription.unsubscribe();

    }

    public void refresh() {
        refreshFromNetwork();
    }


    private void refreshFromNetwork() {
        mCompositeSubscription.add(mController.getForceRefreshRxObservable().observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<ProductionData>() {
                    @Override
                    public void onCompleted() {
                        HPLogger.d(TAG,"Refresh complete");
                    }
                    @Override
                    public void onError(Throwable e) {}

                    @Override
                    public void onNext(ProductionData productionDataResponse) {
                    }
                }));
    }
}
