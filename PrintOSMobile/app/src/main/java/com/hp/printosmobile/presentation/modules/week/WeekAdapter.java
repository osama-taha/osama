package com.hp.printosmobile.presentation.modules.week;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.modules.kpiview.KPIScoreStateEnum;
import com.hp.printosmobile.presentation.modules.kpiview.KPIViewModel;
import com.hp.printosmobile.presentation.modules.week.kpiexplanationdialog.KpiExplanationDialog;
import com.hp.printosmobile.utils.HPLocaleUtils;
import com.hp.printosmobile.utils.HPUIUtils;
import com.hp.printosmobilelib.ui.widgets.HPProgressArc;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Anwar Asbah on 4/25/2016.
 */
public class WeekAdapter extends PagerAdapter implements KPIAdapter.KPICallbacks {

    private static final float KPI_ITEM_BOTTOM_PADDING = 2;

    private Context context;
    private WeekViewCallback listener;
    private WeekCollectionViewModel weekCollectionViewModel;
    private WeekViewBuilder viewBuilder;

    private Map<Integer, View> viewsList;

    public WeekAdapter(Context context, WeekCollectionViewModel weekCollectionViewModel, WeekViewCallback listener) {
        this.context = context;
        this.listener = listener;
        this.weekCollectionViewModel = weekCollectionViewModel;
        this.viewBuilder = new WeekViewBuilder();
        this.viewsList = new HashMap<>();
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        if (viewsList.containsKey(position)) {
            return viewsList.get(position);
        }

        LayoutInflater inflater = LayoutInflater.from(context);
        View layout = inflater.inflate(R.layout.view_week, container, false);
        viewBuilder.build(layout, weekCollectionViewModel.isShowRanking(), weekCollectionViewModel.getWeeks().get(position), position);
        container.addView(layout);

        viewsList.put(position, layout);

        layout.setTag(position);
        return layout;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

    }

    @Override
    public int getCount() {
        if (weekCollectionViewModel != null && weekCollectionViewModel.getWeeks() != null) {
            return weekCollectionViewModel.getWeeks().size();
        }

        return 0;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    public void bind(WeekCollectionViewModel viewModels) {
        clearViews();
        this.weekCollectionViewModel = viewModels;
        notifyDataSetChanged();
    }

    private void clearViews() {
        viewsList.clear();
    }

    @Override
    public void onKpiSelected(String s) {
        if (listener != null) {
            listener.onKPISelected(s);
        }
    }

    @Override
    public void showKpiExplanationDialog(ArrayList<KPIViewModel> kpiList, int position) {
        if (listener != null) {
            listener.showKpiExplanationDialog(kpiList, position);
        }
    }

    class WeekViewBuilder {

        @Bind(R.id.week_header)
        TextView weekHeader;
        @Bind(R.id.week_performance_text)
        TextView weekPerformanceText;
        @Bind(R.id.kpis_layout)
        RecyclerView kpisContainer;
        @Bind(R.id.week_performance_image)
        ImageView weekPerformanceImage;
        @Bind(R.id.progress_arc)
        HPProgressArc progressArc;
        @Bind(R.id.week_score_value)
        TextView weekScoreText;
        @Bind(R.id.week_max_score_value)
        TextView weekScoreMaxText;
        @Bind(R.id.week_press_status_warning_indicator)
        View weekPressStatusWarningIndicator;

        @Bind(R.id.ranking_layout)
        View rankingLayout;
        @Bind(R.id.no_ranking_data_text_view)
        TextView noRankingDataTextView;
        @Bind(R.id.trophy_layout)
        View trophyLayout;
        @Bind(R.id.trophy_image_view)
        ImageView trophyImageView;
        @Bind(R.id.top_5_label)
        TextView top5TextView;
        @Bind(R.id.top_5_in_region_label)
        TextView top5RegionMessageTextView;
        @Bind(R.id.rank_area_layout)
        LinearLayout rankAreaLayout;
        private int width;

        private void build(View view, boolean showRanking, final WeekViewModel viewModel, int index) {

            ButterKnife.bind(this, view);
            if (viewModel == null || viewModel.getKpis() == null) {
                return;
            }

            weekPerformanceImage.setImageDrawable(ResourcesCompat.getDrawable(context.getResources(),
                    getPerformanceDrawable(viewModel.getPerformance()), null));
            setWeeklyPointStatus(weekPerformanceText, viewModel.getPerformance());
            weekHeader.setText(getWeekHeader(index, viewModel.getFromDate(), viewModel.getToDate(), viewModel.getWeekNumber()));
            buildKpiView(viewModel.getKpis());

            updateProgressArc(viewModel.getWeekScore(), viewModel.getWeekMaxScore(), viewModel.getPerformance());

            if (showRanking) {
                setRankingLayout(viewModel);
            } else {
                rankingLayout.setVisibility(View.GONE);
            }

            progressArc.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (v.getId() != R.id.kpis_layout) {
                        listener.onWeekSelected();
                    }
                }
            });

            weekPressStatusWarningIndicator.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        listener.onPartialDataWarningClicked(viewModel.weekPressStatuses);
                    }
                }
            });

            List<WeekViewModel.WeekPressStatus> weekPressStatuses = viewModel.getWeekPressStatuses();
            boolean hasMissing = false;
            if (weekPressStatuses != null) {
                for (WeekViewModel.WeekPressStatus status : weekPressStatuses) {
                    if (status.getWeeklyPressStatusEnum() != WeekViewModel.WeekPressStatus.WeeklyPressStatusEnum.READY) {
                        hasMissing = true;
                        break;
                    }
                }

            }
            weekPressStatusWarningIndicator.setVisibility(hasMissing ? View.VISIBLE : View.INVISIBLE);

        }

        private int getPerformanceDrawable(KPIScoreStateEnum state) {
            if (state == null) {
                return R.drawable.week_below_average;
            }
            switch (state) {
                case GREAT:
                    return R.drawable.week_great;
                case GOOD:
                    return R.drawable.week_good;
                case AVERAGE:
                    return R.drawable.week_average;
                default:
                    return R.drawable.week_below_average;
            }
        }

        private void setWeeklyPointStatus(TextView textView, KPIScoreStateEnum state) {
            if (state == null) {
                return;
            }

            switch (state) {
                case GREAT:
                    textView.setText(context.getString(R.string.week_performance_great));
                    textView.setTextColor(ResourcesCompat.getColor(context.getResources(), R.color.week_performance_great_color, null));
                    return;
                case GOOD:
                    textView.setText(context.getString(R.string.week_performance_good));
                    textView.setTextColor(ResourcesCompat.getColor(context.getResources(), R.color.week_performance_good_color, null));
                    return;
                case AVERAGE:
                    textView.setText(context.getString(R.string.week_performance_average));
                    textView.setTextColor(ResourcesCompat.getColor(context.getResources(), R.color.week_performance_average_color, null));
                    return;
                default:
                    textView.setText(context.getString(R.string.week_performance_below_average));
                    textView.setTextColor(ResourcesCompat.getColor(context.getResources(), R.color.week_performance_below_average_color, null));
                    return;
            }
        }

        private void updateProgressArc(int score, int maxScore, KPIScoreStateEnum state) {
            int progressColor;
            int trackColor;

            switch (state) {
                case GREAT:
                    progressColor = ResourcesCompat.getColor(context.getResources(), R.color.week_performance_great_color, null);
                    trackColor = ResourcesCompat.getColor(context.getResources(), R.color.week_performance_great_tack_color, null);
                    break;
                case GOOD:
                    progressColor = ResourcesCompat.getColor(context.getResources(), R.color.week_performance_good_color, null);
                    trackColor = ResourcesCompat.getColor(context.getResources(), R.color.week_performance_good_tack_color, null);
                    break;
                case AVERAGE:
                    progressColor = ResourcesCompat.getColor(context.getResources(), R.color.week_performance_average_color, null);
                    trackColor = ResourcesCompat.getColor(context.getResources(), R.color.week_performance_average_tack_color, null);
                    break;
                default:
                    progressColor = ResourcesCompat.getColor(context.getResources(), R.color.week_performance_below_average_color, null);
                    trackColor = ResourcesCompat.getColor(context.getResources(), R.color.week_performance_below_average_tack_color, null);
                    break;
            }

            final int[] sweepColors = {progressColor, progressColor};
            final float[] sweepPosition = {0.5f, 1f};
            progressArc.setProgressSweepGradient(sweepColors, sweepPosition);
            progressArc.setTrackColor(trackColor);
            progressArc.setRoundedEdges(false);

            float percentageFill = maxScore == 0 ? 0 : (float) score / (float) maxScore;
            percentageFill = Math.max(0, percentageFill);

            progressArc.setProgress((int) (percentageFill * 100));

            weekScoreText.setTextColor(progressColor);
            weekScoreText.setText(String.valueOf(score));

            //for now value is set to 100 s there is a bug in the server
            weekScoreMaxText.setText(String.format(context.getString(R.string.week_max_score_dinomiator_expression), 100));//maxScore));
        }

        private String getWeekHeader(int index, Date fromDate, Date toDate, int weekNumber) {

            if (fromDate == null || toDate == null) return "";

            int indexDif = getCount() - index;
            String weekAgo = indexDif == 1 ? context.getString(R.string.last_week)
                    : String.format(context.getString(R.string.weeks_ago), indexDif);

            String dateRange = HPLocaleUtils.getDateRangeString(context, fromDate, toDate);

            return String.format(context.getString(R.string.week_header_format), weekAgo, dateRange, String.valueOf(weekNumber));
        }

        private void buildKpiView(List<KPIViewModel> kpis) {
            RecyclerView.LayoutManager manager = new LinearLayoutManager(context);
            kpisContainer.setLayoutManager(manager);
            kpisContainer.addItemDecoration(new VerticalSpaceItemDecoration(
                    (int) (KPI_ITEM_BOTTOM_PADDING * Resources.getSystem().getDisplayMetrics().density)));
            kpisContainer.setAdapter(new KPIAdapter(kpis, context, WeekAdapter.this, weekCollectionViewModel.getBusinessUnitEnum()));
            kpisContainer.setHasFixedSize(true);
            kpisContainer.setNestedScrollingEnabled(false);
        }

        private void setRankingLayout(WeekViewModel viewModel) {

            if (viewModel.hasRankingData()) {

                noRankingDataTextView.setVisibility(View.GONE);
                rankAreaLayout.setVisibility(View.VISIBLE);

                final RankingViewModel rankingViewModel = viewModel.getRankingViewModel();

                HPUIUtils.setVisibilityForViews(rankingViewModel.isShowTrophy(), trophyLayout);

                if (rankingViewModel.isShowTrophy()) {
                    top5RegionMessageTextView.setText(rankingViewModel.getTop5RankingRegionMessage());
                    trophyLayout.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (rankingViewModel != null && !rankingViewModel.isWorldWideTop5()) {
                                HPUIUtils.displayToast(context, rankingViewModel.getTop5RankingRegionMessage(), false);
                            }
                        }
                    });
                }

                rankAreaLayout.removeAllViews();

                createAreaRankLayoutIfExists(rankAreaLayout, rankingViewModel.getSubRegionViewModel());
                createAreaRankLayoutIfExists(rankAreaLayout, rankingViewModel.getRegionViewModel());
                int count = createAreaRankLayoutIfExists(rankAreaLayout, rankingViewModel.getWorldWideViewModel());

                if (count > 0) {
                    rankAreaLayout.getChildAt(count - 1)
                            .findViewById(R.id.separator_view).setVisibility(View.GONE);
                }

            } else {

                rankAreaLayout.setVisibility(View.GONE);
                noRankingDataTextView.setVisibility(View.VISIBLE);
                trophyLayout.setVisibility(View.INVISIBLE);

            }

        }

        private int createAreaRankLayoutIfExists(LinearLayout rankAreaLayout, final RankingViewModel.AreaViewModel areaViewModel) {

            if (areaViewModel != null) {

                LayoutInflater inflater = LayoutInflater.from(context);
                View rankLayout = inflater.inflate(R.layout.area_rank_layout, rankAreaLayout, false);

                TextView titleTextView = (TextView) rankLayout.findViewById(R.id.weekly_printbeat_rank_area_title_text_view);
                TextView rankTextView = (TextView) rankLayout.findViewById(R.id.weekly_printbeat_rank_text_view);
                TextView rankTotalDividerTextView = (TextView) rankLayout.findViewById(R.id.weekly_printbeat_divider_text_view);
                TextView totalTextView = (TextView) rankLayout.findViewById(R.id.weekly_printbeat_rank_total_text_view);
                View trendLayout = rankLayout.findViewById(R.id.weekly_printbeat_rank_trend_layout);
                TextView offsetTextView = (TextView) rankLayout.findViewById(R.id.weekly_printbeat_rank_offset);
                ImageView trendImageView = (ImageView) rankLayout.findViewById(R.id.weekly_printbeat_rank_trend_icon);
                View separatorView = rankLayout.findViewById(R.id.separator_view);

                separatorView.setVisibility(View.VISIBLE);

                titleTextView.setText(areaViewModel.getName() == null ? "" : areaViewModel.getName());

                if (areaViewModel.isLowerThird()) {
                    rankTextView.setText(areaViewModel.getMessage());
                    HPUIUtils.setVisibility(false, rankTotalDividerTextView, totalTextView);
                } else {
                    rankTextView.setText(String.valueOf(areaViewModel.getActualRank()));
                    totalTextView.setText(String.valueOf(areaViewModel.getTotal()));
                }

                trendLayout.setBackgroundResource(areaViewModel.getTrend().getBackgroundIconResId());

                if (areaViewModel.getTrend() == RankingViewModel.Trend.EQUALS) {
                    offsetTextView.setText("");
                    trendImageView.setImageResource(0);
                } else {
                    offsetTextView.setText(String.valueOf(Math.abs(areaViewModel.getOffset())));
                    trendImageView.setImageResource(areaViewModel.getTrend().getDirectionIconResId());
                }

                View.OnClickListener rankingLayoutClickListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (areaViewModel != null) {
                            if (areaViewModel.getRankAreaType() != RankingViewModel.RankAreaType.WORLD_WIDE) {
                                HPUIUtils.displayToast(context, areaViewModel.getName(), false);
                            }
                        }
                    }
                };

                rankLayout.setOnClickListener(rankingLayoutClickListener);

                LinearLayout.LayoutParams layoutParams = new LinearLayout
                        .LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT);
                layoutParams.weight = 1;

                rankAreaLayout.addView(rankLayout, layoutParams);
            }

            return rankAreaLayout.getChildCount();

        }

    }

    class VerticalSpaceItemDecoration extends RecyclerView.ItemDecoration {

        private final int verticalSpaceHeight;

        public VerticalSpaceItemDecoration(int verticalSpaceHeight) {
            this.verticalSpaceHeight = verticalSpaceHeight;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                                   RecyclerView.State state) {
            outRect.bottom = verticalSpaceHeight;
        }
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    public interface WeekViewCallback {
        void onKPISelected(String kpiName);

        void onWeekSelected();

        void onPartialDataWarningClicked(List<WeekViewModel.WeekPressStatus> weekPressStatuses);

        void showKpiExplanationDialog(ArrayList<KPIViewModel> kpiList, int position);
    }
}