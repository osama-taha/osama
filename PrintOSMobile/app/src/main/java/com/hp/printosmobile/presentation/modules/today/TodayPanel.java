package com.hp.printosmobile.presentation.modules.today;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.IntercomSdk;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.data.remote.models.PreferencesData;
import com.hp.printosmobile.presentation.modules.home.ActiveShiftsViewModel;
import com.hp.printosmobile.presentation.modules.home.ShiftViewModel;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;
import com.hp.printosmobile.presentation.modules.personaladvisor.presenter.PersonalAdvisorFactory;
import com.hp.printosmobile.presentation.modules.personaladvisor.presenter.PersonalAdvisorFactory.PersonalAdvisorObserver;
import com.hp.printosmobile.presentation.modules.shared.DeviceViewModel;
import com.hp.printosmobile.presentation.modules.todayhistogram.TodayHistogramViewModel;
import com.hp.printosmobile.utils.HPUIUtils;
import com.hp.printosmobilelib.core.communications.remote.APIException;
import com.hp.printosmobilelib.core.logging.HPLogger;
import com.hp.printosmobilelib.ui.widgets.HPViewPager;
import com.hp.printosmobilelib.ui.widgets.panel.PanelView;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Osama Taha on 5/15/2016.
 */
public class TodayPanel extends PanelView<TodayViewModel> implements TodayAdapter.TodayAdapterCallbacks, PersonalAdvisorObserver {

    public final static String TAG = TodayPanel.class.getSimpleName();
    public final static String TAG_ACTUAL_VS_TARGET = "ACTUAL_VS_TARGET";
    private static final long FADE_DURATION = 500;
    private static final int MIN_ITEMS_TO_SHOW_VIEW_ALL_DEVICES_BUTTON = 2;

    @Bind(R.id.today_pager)
    HPViewPager todayPager;
    @Bind(R.id.today_pager_indicator)
    CirclePageIndicator todayPagerIndicator;
    @Bind(R.id.button_view_all)
    TextView buttonViewAll;
    @Bind(R.id.today_mode_buttons)
    RadioGroup shiftsDaysPicker;
    @Bind(R.id.view_separator)
    View separatorView;
    @Bind(R.id.today_panel_content)
    View todayPanelContent;

    private TodayPanelCallbacks callbacks;
    private TodayAdapter todayAdapter;
    private int selectedPagePosition;
    private TodayHistogramViewModel todayHistogramViewModel;
    private ActiveShiftsViewModel activeShiftsViewModel;
    private ShiftViewModel selectedShift;
    private boolean initialLoad = true;
    private ImageView shiftsDaysArrowImage;

    private boolean automaticSwipeEnabled = true;
    private boolean pageScrolled;
    private boolean pagerScrolledOnFirstLoad;
    private boolean todayDataLoaded;

    public TodayPanel(Context context) {
        super(context);
        bindViews();
    }

    public TodayPanel(Context context, AttributeSet attrs) {
        super(context, attrs);
        bindViews();
    }

    public TodayPanel(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        bindViews();
    }

    private void bindViews() {
        ButterKnife.bind(this, getView());
        initView();
        initHeaderIcons();
        PersonalAdvisorFactory.getInstance().addObserver(this);
    }

    private void initHeaderIcons() {

        View headerLeftView = inflate(getContext(), R.layout.today_panel_header_left_view, getHeaderLeftView());
        shiftsDaysArrowImage = (ImageView) headerLeftView.findViewById(R.id.today_panel_arrow);

        shiftsDaysArrowImage.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                closeShiftsDaysPicker(shiftsDaysPicker.getVisibility() == GONE ? false : true);
            }
        });

    }

    private void initView() {

        todayPanelContent.setVisibility(GONE);

        todayAdapter = new TodayAdapter(getContext(), this);
        todayPager.setAdapter(todayAdapter);

        todayPagerIndicator.setViewPager(todayPager);

        todayPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                onPageChanged(position);
            }

            @Override
            public void onPageSelected(int position) {
                selectedPagePosition = position;
                pageScrolled = !pagerScrolledOnFirstLoad;
                pagerScrolledOnFirstLoad = false;
                setTitle(getTitleSpannable());
                buttonViewAll.setText(
                        getContext().getString(position == TodayAdapter.TODAY_VIEW_POSITION ?
                                R.string.today_panel_view_all :
                                R.string.today_panel_rotate_for_details));

                if (position == TodayAdapter.HISTOGRAM_VIEW_POSITION) {
                    if (todayHistogramViewModel == null && callbacks != null) {
                        showLoading();
                        callbacks.loadHistogramData();
                    }
                    IntercomSdk.getInstance(PrintOSApplication.getAppContext()).logEvent(IntercomSdk.PBM_VIEW_7DAY_EVENT);
                }

                Analytics.sendEvent(Analytics.MOBILE_PRINT_BEAT_CATEGORY, Analytics.TODAY_VIEW_ACTION,
                        position == TodayAdapter.TODAY_VIEW_POSITION ? Analytics.TODAY_VIEW_SHOW_SCORE_VIEW : Analytics.TODAY_VIEW_SHOW_HISTOGRAM_VIEW);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                callbacks.enableSwipeToRefresh(state == ViewPager.SCROLL_STATE_IDLE);

                if (pageScrolled) {
                    callbacks.onTodayViewSelected();
                    pageScrolled = false;
                }

            }
        });

    }

    private void onPageChanged(int position) {

        if (position == TodayAdapter.HISTOGRAM_VIEW_POSITION) {
            hideViewAllButton(false);
        } else {
            hideViewAllButton(getViewModel().getSiteViewModel().getDevices().size() < MIN_ITEMS_TO_SHOW_VIEW_ALL_DEVICES_BUTTON);
        }

    }

    public void setAutomaticSwipeEnabled(boolean automaticSwipeEnabled) {
        this.automaticSwipeEnabled = automaticSwipeEnabled;
    }

    public void onFilterSelected() {
        selectedShift = null;
        onRefresh();
    }

    @Override
    public Spannable getTitleSpannable() {

        SpannableStringBuilder builder;

        if (selectedPagePosition == TodayAdapter.HISTOGRAM_VIEW_POSITION && todayDataLoaded) {

            builder = new SpannableStringBuilder(getHistogramCardTitle());

        } else {

            if (getViewModel() == null || getViewModel().getDeviceViewModels() == null) {
                builder = new SpannableStringBuilder(getContext().getString(R.string.today_panel_title_press_view));
            } else {


                int selectedDevicesCount = getViewModel().getDeviceViewModels().size();
                int siteDevicesCount = getViewModel().getSiteViewModel().getDevices().size();

                String devicesCountTitle = String.valueOf(siteDevicesCount);

                if (selectedDevicesCount != siteDevicesCount) {
                    devicesCountTitle = String.format("%d/%d", selectedDevicesCount, siteDevicesCount);
                }

                builder = new SpannableStringBuilder(String.format(
                        getContext().getString(R.string.today_panel_panel_title_press_view_with_extra), devicesCountTitle));
            }

            if (selectedShift != null && selectedShift.getShiftType() == ShiftViewModel.ShiftType.CURRENT) {
                builder = new SpannableStringBuilder(getContext().getString(R.string.today_panel_title_current_shift));
            }
        }

        return builder;

    }

    private CharSequence getHistogramCardTitle() {

        if (todayHistogramViewModel == null) {

            return getContext().getString(R.string.today_panel_title_last_7_days_view);
        } else {

            boolean isShiftSupport = todayHistogramViewModel.isShiftSupport();
            if (todayHistogramViewModel.getBusinessUnit() == BusinessUnitEnum.INDIGO_PRESS) {
                return getContext().getString(isShiftSupport ? R.string.today_panel_last_shifts : R.string.today_panel_last_days);
            } else {
                PreferencesData.UnitSystem unitSystem = PrintOSPreferences.getInstance(getContext()).getUnitSystem();
                if (unitSystem == PreferencesData.UnitSystem.Metric) {
                    return getContext().getString(isShiftSupport ? R.string.today_panel_last_shifts_sqm : R.string.today_panel_last_days_sqm);
                } else {
                    return getContext().getString(isShiftSupport ?
                            R.string.today_panel_last_shifts_ft_square : R.string.today_panel_last_days_ft_square);
                }
            }
        }
    }


    @Override
    public int getContentView() {
        return R.layout.today_panel_content;
    }

    @Override
    public void updateViewModel(TodayViewModel viewModel) {

        setViewModel(viewModel);
        todayDataLoaded = true;

        updatePersonalAdvisorObserver();

        if (showEmptyCard()) {
            return;
        }

        onPageChanged(todayPager.getCurrentItem());

        setTitle(getTitleSpannable());
        todayAdapter.setTodayViewModel(viewModel);

        todayPagerIndicator.setVisibility(GONE);
        todayPager.setScrollingEnabled(false);

        hideShiftsDaysArrow(true);
        closeShiftsDaysPicker(true);

        if (callbacks != null) {
            callbacks.setHasRTDevices(false);
        }
        boolean shouldSwipeToDays = true;

        if (viewModel != null && viewModel.getDeviceViewModels() != null) {
            for (DeviceViewModel deviceViewModel : viewModel.getDeviceViewModels()) {
                if (deviceViewModel.isRTSupported()) {
                    todayPagerIndicator.setVisibility(VISIBLE);
                    todayPager.setScrollingEnabled(true);
                    hideShiftsDaysArrow(activeShiftsViewModel.getShiftViewModels() != null && activeShiftsViewModel.getShiftViewModels().size() >= 0 ?
                            false : true);
                    shouldSwipeToDays = false;
                    if (callbacks != null) {
                        callbacks.setHasRTDevices(true);
                    }
                    break;
                }
            }
        }

        if ((shouldSwipeToDays || initialLoad) && automaticSwipeEnabled) {
            initialLoad = false;
            pagerScrolledOnFirstLoad = true;
            todayPager.setCurrentItem(TodayAdapter.TODAY_VIEW_POSITION, true);
            buttonViewAll.setText(getContext().getString(R.string.today_panel_view_all));
        } else if (initialLoad) {
            buttonViewAll.setText(getContext().getString(R.string.today_panel_rotate_for_details));
            if (callbacks != null) {
                callbacks.loadHistogramData();
            }
        }

        todayPanelContent.setVisibility(VISIBLE);
        automaticSwipeEnabled = true;
    }

    public void updateActualVsTarget(TodayHistogramViewModel viewModel) {
        todayHistogramViewModel = viewModel;
        todayAdapter.setTodayHistogramViewModel(viewModel);
        setTitle(getTitleSpannable());
    }

    public ActiveShiftsViewModel getActiveShiftsViewModel() {
        return this.activeShiftsViewModel;
    }

    public void updateActiveShifts(ActiveShiftsViewModel activeShiftsViewModel) {

        this.activeShiftsViewModel = activeShiftsViewModel;

        if (activeShiftsViewModel.getShiftViewModels() == null || activeShiftsViewModel.getShiftViewModels().size() == 0) {
            hideShiftsDaysArrow(true);
            return;
        }

        shiftsDaysPicker.removeAllViews();

        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        for (int i = 0; i < activeShiftsViewModel.getShiftViewModels().size(); i++) {
            final ShiftViewModel shiftViewModel = activeShiftsViewModel.getShiftViewModels().get(i);

            String shiftName = getContext().getString(shiftViewModel.getShiftType().getDisplayNameResource());
            String selectShiftName = selectedShift == null ?
                    getContext().getString(R.string.day_default_shift_display_name)
                    : getContext().getString(selectedShift.getShiftType().getDisplayNameResource());

            View shiftModeView = inflater.inflate(R.layout.today_panel_content_radio, null);
            RadioButton shiftModeButton = (RadioButton) shiftModeView.findViewById(R.id.radio_button);
            shiftModeButton.setId(i);
            shiftModeButton.setText(shiftName);
            shiftModeButton.setChecked(shiftName.equals(selectShiftName));

            shiftModeButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    closeShiftsDaysPicker(true);
                }
            });
            shiftModeButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                ShiftViewModel model = shiftViewModel;

                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    onRefresh();

                    if (isChecked) {
                        selectedShift = model;
                        callbacks.onShiftSelected(selectedShift);
                        showLoading();
                    }
                }
            });

            shiftsDaysPicker.addView(shiftModeButton);
        }
    }

    @Override
    protected boolean isValidViewModel() {
        return !(getViewModel() == null
                || getViewModel().getDeviceViewModels() == null
                || getViewModel().getDeviceViewModels().size() == 0);
    }

    @Override
    protected String getEmptyCardText() {
        return getContext().getString(R.string.no_information_to_display);
    }

    private void hideShiftsDaysArrow(boolean hide) {
        shiftsDaysArrowImage.setVisibility(hide ? GONE : VISIBLE);
    }

    private void hideViewAllButton(boolean hide) {

        HPUIUtils.setVisibility(!hide, separatorView, buttonViewAll);

    }

    private void closeShiftsDaysPicker(boolean hide) {

        if (hide) {
            //Fade out animation
            shiftsDaysPicker.animate().alpha(0.0f).setDuration(FADE_DURATION);
            shiftsDaysPicker.setVisibility(GONE);
            //Arrow direction is DOWN (original state).
            shiftsDaysArrowImage.setRotation(0);
        } else {
            //Fade in animation
            shiftsDaysPicker.animate().alpha(1.0f).setDuration(FADE_DURATION);
            shiftsDaysPicker.setVisibility(VISIBLE);
            //Arrow direction is UP.
            shiftsDaysArrowImage.setRotation(-180);
        }

    }

    @Override
    public void showLoading() {
        super.showLoading();
        shiftsDaysPicker.setEnabled(false);
    }

    @Override
    public void hideLoading() {
        super.hideLoading();
        shiftsDaysPicker.setEnabled(true);
    }

    public void addTodayPanelCallbacks(TodayPanelCallbacks callbacks) {
        this.callbacks = callbacks;
    }

    @OnClick(R.id.button_view_all)
    void onViewAllButtonClicked() {
        if (callbacks != null) {
            if (selectedPagePosition == TodayAdapter.HISTOGRAM_VIEW_POSITION) {
                callbacks.onHistogramViewAllClicked(todayHistogramViewModel);
            } else {
                callbacks.onTodayPanelViewAllClicked();
            }
        }

    }

    @Override
    public void onDeviceClicked(DeviceViewModel model) {
        if (callbacks != null) {
            callbacks.onDeviceClicked(model);
        }
    }

    @Override
    public void onHasAdviceIconClicked(DeviceViewModel model) {
        if (callbacks != null) {
            callbacks.onHasAdviceIconClicked(model);
        }
    }

    @Override
    public void onTodayViewInitialized() {
        if (callbacks != null) {
            callbacks.onTodayViewInitialized();
        }
    }

    @Override
    public void updatePersonalAdvisorObserver() {
        HPLogger.d(TAG, "updating Personal Advisor Observer");
        TodayViewModel todayViewModel = getViewModel();
        if (todayViewModel != null) {
            List<DeviceViewModel> deviceViewModels = todayViewModel.getDeviceViewModels();
            PersonalAdvisorFactory factory = PersonalAdvisorFactory.getInstance();
            if (deviceViewModels != null) {
                for (DeviceViewModel model : deviceViewModels) {
                    model.setPersonalAdvisorViewModel(factory.getDeviceAdvices(model.getSerialNumber()));
                }
            }
            if (todayAdapter != null) {
                todayAdapter.notifyDevicesList();
            }
        }
    }

    @Override
    public void onPersonalAdvisorError(APIException e, String tag) {
        //Do Nothing
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        PersonalAdvisorFactory.getInstance().removeObserver(this);
    }

    @Override
    public void onRefresh() {
        super.onRefresh();
        todayHistogramViewModel = null;
        todayPager.setCurrentItem(TodayAdapter.TODAY_VIEW_POSITION, true);
        setTitle(getTitleSpannable());
        if (callbacks != null) {
            callbacks.resetHistogramData();
        }
    }

    public interface TodayPanelCallbacks {

        void onTodayPanelViewAllClicked();

        void onHistogramViewAllClicked(TodayHistogramViewModel model);

        void onDeviceClicked(DeviceViewModel model);

        void onHasAdviceIconClicked(DeviceViewModel model);

        void onShiftSelected(ShiftViewModel shiftViewModel);

        void enableSwipeToRefresh(boolean enable);

        void onTodayViewInitialized();

        void onTodayViewSelected();

        void setHasRTDevices(boolean hasRTDevices);

        void loadHistogramData();

        void resetHistogramData();
    }
}
