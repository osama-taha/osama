package com.hp.printosmobile.data.remote.models;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.hp.printosmobile.data.remote.services.RealtimeTrackingService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A model representing the response of {@link RealtimeTrackingService#getTargetData(String, List, String, String, boolean)} endpoint.
 *
 * @author Osama Taha
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TargetData {

    @JsonProperty("intraDailyTarget")
    private IntraDailyTarget intraDailyTarget;
    @JsonProperty("shiftTarget")
    private ShiftTarget shiftTarget;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    /**
     * @return The intraDailyTarget
     */
    @JsonProperty("intraDailyTarget")
    public IntraDailyTarget getIntraDailyTarget() {
        return intraDailyTarget;
    }

    /**
     * @param intraDailyTarget The intraDailyTarget
     */
    @JsonProperty("intraDailyTarget")
    public void setIntraDailyTarget(IntraDailyTarget intraDailyTarget) {
        this.intraDailyTarget = intraDailyTarget;
    }

    /**
     * @return The shiftTarget
     */
    @JsonProperty("shiftTarget")
    public ShiftTarget getShiftTarget() {
        return shiftTarget;
    }

    /**
     * @param shiftTarget The shiftTarget
     */
    @JsonProperty("shiftTarget")
    public void setShiftTarget(ShiftTarget shiftTarget) {
        this.shiftTarget = shiftTarget;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("TargetData{");
        sb.append("intraDailyTarget=").append(intraDailyTarget);
        sb.append(", shiftTarget=").append(shiftTarget);
        sb.append(", additionalProperties=").append(additionalProperties);
        sb.append('}');
        return sb.toString();
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public class ShiftTarget {

        @JsonProperty("value")
        private Double value;
        @JsonProperty("day")
        private Integer day;
        @JsonProperty("hour")
        private Integer hour;
        @JsonProperty("device")
        private String device;
        @JsonProperty("date")
        private String date;
        @JsonProperty("calculatedValue")
        private Double calculatedValue;
        @JsonProperty("offsetFromToday")
        private Integer offsetFromToday;
        @JsonProperty("shiftDataRespond")
        private ShiftData shiftData;

        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<>();

        /**
         * @return The value
         */
        @JsonProperty("value")
        public Double getValue() {
            return value;
        }

        /**
         * @param value The value
         */
        @JsonProperty("value")
        public void setValue(Double value) {
            this.value = value;
        }

        /**
         * @return The day
         */
        @JsonProperty("day")
        public Integer getDay() {
            return day;
        }

        /**
         * @param day The day
         */
        @JsonProperty("day")
        public void setDay(Integer day) {
            this.day = day;
        }

        /**
         * @return The hour
         */
        @JsonProperty("hour")
        public Integer getHour() {
            return hour;
        }

        /**
         * @param hour The hour
         */
        @JsonProperty("hour")
        public void setHour(Integer hour) {
            this.hour = hour;
        }

        /**
         * @return The device
         */
        @JsonProperty("device")
        public String getDevice() {
            return device;
        }

        /**
         * @param device The device
         */
        @JsonProperty("device")
        public void setDevice(String device) {
            this.device = device;
        }

        /**
         * @return The date
         */
        @JsonProperty("date")
        public String getDate() {
            return date;
        }

        /**
         * @param date The date
         */
        @JsonProperty("date")
        public void setDate(String date) {
            this.date = date;
        }

        /**
         * @return The calculatedValue
         */
        @JsonProperty("calculatedValue")
        public Double getCalculatedValue() {
            return calculatedValue;
        }

        /**
         * @param calculatedValue The calculatedValue
         */
        @JsonProperty("calculatedValue")
        public void setCalculatedValue(Double calculatedValue) {
            this.calculatedValue = calculatedValue;
        }

        /**
         * @return The offsetFromToday
         */
        @JsonProperty("offsetFromToday")
        public Integer getOffsetFromToday() {
            return offsetFromToday;
        }

        /**
         * @param offsetFromToday The offsetFromToday
         */
        @JsonProperty("offsetFromToday")
        public void setOffsetFromToday(Integer offsetFromToday) {
            this.offsetFromToday = offsetFromToday;
        }

        @JsonProperty("shiftDataRespond")
        public ShiftData getShiftData() {
            return shiftData;
        }

        @JsonProperty("shiftDataRespond")
        public void setShiftData(ShiftData shiftData) {
            this.shiftData = shiftData;
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }

        @Override
        public String toString() {
            final StringBuffer sb = new StringBuffer("ShiftTarget{");
            sb.append("value=").append(value);
            sb.append(", day=").append(day);
            sb.append(", hour=").append(hour);
            sb.append(", device='").append(device).append('\'');
            sb.append(", date='").append(date).append('\'');
            sb.append(", calculatedValue=").append(calculatedValue);
            sb.append(", offsetFromToday=").append(offsetFromToday);
            sb.append(", additionalProperties=").append(additionalProperties);
            sb.append('}');
            return sb.toString();
        }
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public class IntraDailyTarget {

        @JsonProperty("value")
        private Double value;
        @JsonProperty("day")
        private Integer day;
        @JsonProperty("hour")
        private Integer hour;
        @JsonProperty("device")
        private String device;
        @JsonProperty("date")
        private Object date;
        @JsonProperty("calculatedValue")
        private Double calculatedValue;
        @JsonProperty("offsetFromToday")
        private Integer offsetFromToday;
        @JsonProperty("shiftDataRespond")
        private ShiftData shiftData;

        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<>();

        /**
         * @return The value
         */
        @JsonProperty("value")
        public Double getValue() {
            return value;
        }

        /**
         * @param value The value
         */
        @JsonProperty("value")
        public void setValue(Double value) {
            this.value = value;
        }

        /**
         * @return The day
         */
        @JsonProperty("day")
        public Integer getDay() {
            return day;
        }

        /**
         * @param day The day
         */
        @JsonProperty("day")
        public void setDay(Integer day) {
            this.day = day;
        }

        /**
         * @return The hour
         */
        @JsonProperty("hour")
        public Integer getHour() {
            return hour;
        }

        /**
         * @param hour The hour
         */
        @JsonProperty("hour")
        public void setHour(Integer hour) {
            this.hour = hour;
        }

        /**
         * @return The device
         */
        @JsonProperty("device")
        public String getDevice() {
            return device;
        }

        /**
         * @param device The device
         */
        @JsonProperty("device")
        public void setDevice(String device) {
            this.device = device;
        }

        /**
         * @return The date
         */
        @JsonProperty("date")
        public Object getDate() {
            return date;
        }

        /**
         * @param date The date
         */
        @JsonProperty("date")
        public void setDate(Object date) {
            this.date = date;
        }

        /**
         * @return The calculatedValue
         */
        @JsonProperty("calculatedValue")
        public Double getCalculatedValue() {
            return calculatedValue;
        }

        /**
         * @param calculatedValue The calculatedValue
         */
        @JsonProperty("calculatedValue")
        public void setCalculatedValue(Double calculatedValue) {
            this.calculatedValue = calculatedValue;
        }

        /**
         * @return The offsetFromToday
         */
        @JsonProperty("offsetFromToday")
        public Integer getOffsetFromToday() {
            return offsetFromToday;
        }

        /**
         * @param offsetFromToday The offsetFromToday
         */
        @JsonProperty("offsetFromToday")
        public void setOffsetFromToday(Integer offsetFromToday) {
            this.offsetFromToday = offsetFromToday;
        }

        @JsonProperty("shiftDataRespond")
        public ShiftData getShiftData() {
            return shiftData;
        }

        @JsonProperty("shiftDataRespond")
        public void setShiftData(ShiftData shiftData) {
            this.shiftData = shiftData;
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }

        @Override
        public String toString() {
            final StringBuffer sb = new StringBuffer("IntraDailyTarget{");
            sb.append("value=").append(value);
            sb.append(", day=").append(day);
            sb.append(", hour=").append(hour);
            sb.append(", device='").append(device).append('\'');
            sb.append(", date=").append(date);
            sb.append(", calculatedValue=").append(calculatedValue);
            sb.append(", offsetFromToday=").append(offsetFromToday);
            sb.append(", additionalProperties=").append(additionalProperties);
            sb.append('}');
            return sb.toString();
        }
    }
}
