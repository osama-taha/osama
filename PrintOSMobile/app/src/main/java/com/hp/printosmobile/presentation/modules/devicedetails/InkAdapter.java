package com.hp.printosmobile.presentation.modules.devicedetails;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hp.printosmobile.R;
import com.hp.printosmobilelib.ui.widgets.HPVerticalProgressBar;
import com.hp.printosmobilelib.ui.widgets.TypefaceManager;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Anwar Asbah on 8/16/2016.
 */
public class InkAdapter extends RecyclerView.Adapter<InkAdapter.InkViewHolder> {


    private InkAdapterListener listener;
    private Context context;
    private List<InkModel> inks;
    private int selectedIndex;
    private View selectedView;
    private final float textFontSize;
    private final Typeface textTypeFace;

    public InkAdapter(Context context, List<InkModel> inks, InkAdapterListener listener) {
        this.context = context;
        this.inks = inks;
        this.listener = listener;
        selectedIndex = 0;
        textFontSize = context.getResources().getDimension(R.dimen.app_default_font_size);
        textTypeFace = TypefaceManager.getTypeface(context, TypefaceManager.HPTypeface.HP_LIGHT);
    }

    @Override
    public InkViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ViewGroup deviceView = (ViewGroup) inflater.inflate(R.layout.ink_item, parent, false);
        InkViewHolder inkViewHolder = new InkViewHolder(deviceView);
        return inkViewHolder;
    }

    @Override
    public void onBindViewHolder(InkViewHolder holder, int position) {

        InkModel model = inks.get(position);

        holder.inkBar.setLabel(context.getString(model.getInkEnum().getLocalizedColorCodeResId()));
        holder.inkBar.setValues(model.getLevel(), model.getCapacity());
        holder.inkBar.setValueText(model.getLevel());

        holder.inkBar.setLabelFontSize(textFontSize);
        holder.inkBar.setLabelTypeFace(textTypeFace);
        holder.inkBar.setValueFontSize(textFontSize);
        holder.inkBar.setValueTypeFace(textTypeFace);

        holder.position = position;

        int color1 = model.getInkEnum().getColor();
        int color2 = android.R.color.transparent;

        holder.inkBar.setColors(ResourcesCompat.getColor(context.getResources(), color1, null),
                ResourcesCompat.getColor(context.getResources(), color2, null));

        if (model.getInkState() != InkModel.InkState.OK) {
            holder.inkBar.setStatusImage(ResourcesCompat.getDrawable(context.getResources(), R.drawable.warning_triangle, null));
        } else {
            holder.inkBar.setStatusImageGone();
        }
        holder.view.setBackgroundResource(position == selectedIndex ?
                R.drawable.ink_item_border : android.R.color.transparent);

        if (selectedIndex == position) {
            listener.onInkSelected(model);
            selectedView = holder.view;
        }
    }

    @Override
    public int getItemCount() {
        return inks == null ? 0 : inks.size();
    }

    public class InkViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.ink_progress)
        HPVerticalProgressBar inkBar;
        View view;
        int position;

        public InkViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            view = itemView;
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectedIndex = position;
                    listener.onInkSelected(inks.get(position));
                    if (selectedView != null) {
                        selectedView.setBackgroundResource(android.R.color.transparent);
                    }
                    view.setBackgroundResource(R.drawable.ink_item_border);
                    selectedView = view;
                }
            });
        }
    }

    public interface InkAdapterListener {
        void onInkSelected(InkModel model);
    }
}
