package com.hp.printosmobile.presentation.modules.tooltip;

import android.content.Context;

import android.graphics.Point;
import android.graphics.Rect;
import android.os.Handler;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.PopupWindow;

import com.hp.printosmobile.utils.HPUIUtils;

/**
 * Created by Osama Taha on 12/17/16.
 */
public abstract class Tooltip<T> {

    private static final int MSG_DISMISS_TOOLTIP = 100;
    private static final int DEFAULT_WIDTH_IN_DP = 200;
    protected Context mContext;
    private PopupWindow tooltipWindow;
    protected View contentView;
    private LayoutInflater inflater;

    public Tooltip(Context context) {

        mContext = context;
        tooltipWindow = new PopupWindow(context);

        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        contentView = inflater.inflate(getLayoutResourceId(), null);
        initContentView();
    }

    protected abstract void initContentView();

    public abstract int getLayoutResourceId();

    public abstract void updateView(T viewModel);

    public void showToolTip(View anchor) {

        tooltipWindow.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        tooltipWindow.setWidth(HPUIUtils.dpToPx(mContext, DEFAULT_WIDTH_IN_DP));

        tooltipWindow.setOutsideTouchable(true);
        tooltipWindow.setTouchable(true);
        tooltipWindow.setFocusable(true);

        tooltipWindow.setContentView(contentView);

        int screenPosition[] = new int[2];
        // Get location of anchor view on screen
        anchor.getLocationOnScreen(screenPosition);

        // Get rect for anchor view
        Rect anchorRect = new Rect(screenPosition[0], screenPosition[1], screenPosition[0]
                + anchor.getWidth(), screenPosition[1] + anchor.getHeight());

        // Call view measure to calculate how big your view should be.
        contentView.measure(HPUIUtils.dpToPx(mContext, DEFAULT_WIDTH_IN_DP), ViewGroup.LayoutParams.WRAP_CONTENT);

        int positionX = anchorRect.centerX();
        int positionY = anchorRect.bottom + HPUIUtils.dpToPx(mContext, 5);

        tooltipWindow.showAtLocation(anchor, Gravity.NO_GRAVITY, positionX,
                positionY);

        //handler.sendEmptyMessageDelayed(MSG_DISMISS_TOOLTIP, 4000);
    }

    public void showToolTipToLeftSide(View anchor, float screenLeftMarginPX, float anchorTopMarginPX) {

        tooltipWindow.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        tooltipWindow.setWidth(ViewGroup.LayoutParams.WRAP_CONTENT);

        tooltipWindow.setOutsideTouchable(true);
        tooltipWindow.setTouchable(true);
        tooltipWindow.setFocusable(true);

        tooltipWindow.setContentView(contentView);

        int screenPosition[] = new int[2];
        // Get location of anchor view on screen
        anchor.getLocationOnScreen(screenPosition);

        // Get rect for anchor view
        Rect anchorRect = new Rect(screenPosition[0], screenPosition[1], screenPosition[0]
                + anchor.getWidth(), screenPosition[1] + anchor.getHeight());

        // Call view measure to calculate how big your view should be.
        contentView.measure(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        WindowManager windowManager = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
        Display display = windowManager.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int positionX = (int) (size.x - screenLeftMarginPX - contentView.getMeasuredWidth());
        int positionY = (int) (anchorRect.bottom + anchorTopMarginPX);

        tooltipWindow.showAtLocation(anchor, Gravity.NO_GRAVITY, positionX,
                positionY);

        //handler.sendEmptyMessageDelayed(MSG_DISMISS_TOOLTIP, 4000);
    }

    public void showToolTip(View anchor, int centerX, int centerY, int gravity) {

        int toolTipWidth = HPUIUtils.dpToPx(mContext, DEFAULT_WIDTH_IN_DP);

        tooltipWindow.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        tooltipWindow.setWidth(toolTipWidth);

        tooltipWindow.setOutsideTouchable(true);
        tooltipWindow.setTouchable(true);
        tooltipWindow.setFocusable(true);

        tooltipWindow.setContentView(contentView);

        // Call view measure to calculate how big your view should be.
        contentView.measure(HPUIUtils.dpToPx(mContext, DEFAULT_WIDTH_IN_DP), ViewGroup.LayoutParams.WRAP_CONTENT);

        int positionX = gravity == Gravity.LEFT ? centerX - toolTipWidth : centerX;
        int positionY = centerY + HPUIUtils.dpToPx(mContext, 5);

        tooltipWindow.showAtLocation(anchor, Gravity.NO_GRAVITY, positionX,
                positionY);
    }


    boolean isTooltipShown() {
        return (tooltipWindow != null &&
                tooltipWindow.isShowing());
    }

    void dismissTooltip() {
        if (isTooltipShown()) {
            tooltipWindow.dismiss();
        }
    }

    Handler handler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
                case MSG_DISMISS_TOOLTIP:
                    dismissTooltip();
                    break;
            }
        }

    };

}