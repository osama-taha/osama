package com.hp.printosmobile.utils;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.Environment;
import android.view.View;

import java.io.File;
import java.io.FileOutputStream;

/**
 * Created by Anwar Asbah on 12/12/15.
 */
public class FileUtils {

    private static final String DIRECTORY_NAME = "/Screenshots";
    private static final String FILE_NAME = "_%d.png";

    public static Bitmap getScreenShot(View view) {
        Bitmap bitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
        return bitmap;
    }

    public static Uri store(Bitmap bm, String fileName) {
        final String dirPath = Environment.getExternalStorageDirectory().getAbsolutePath() + DIRECTORY_NAME;
        File dir = new File(dirPath);
        if (!dir.exists())
            dir.mkdirs();
        File file = new File(dirPath, fileName + String.format(FILE_NAME, System.currentTimeMillis()));
        FileOutputStream fOut = null;
        try {
            file.createNewFile();

            fOut = new FileOutputStream(file);
            bm.compress(Bitmap.CompressFormat.PNG, 100, fOut);
            fOut.flush();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (fOut != null) {
                    fOut.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return Uri.fromFile(file);
    }
}