package com.hp.printosmobile.presentation.modules.productionsnapshot.latex;

import android.content.Context;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.util.AttributeSet;
import android.widget.TextView;

import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.modules.productionsnapshot.indigo.ProductionViewModel;
import com.hp.printosmobile.utils.HPLocaleUtils;
import com.hp.printosmobilelib.ui.widgets.panel.PanelView;

import butterknife.Bind;

/**
 * Created by Anwar Asbah on 4/12/2016.
 */
public class LatexProductionPanel extends PanelView<ProductionViewModel> {

    public final static String TAG = LatexProductionPanel.class.getSimpleName();

    @Bind(R.id.text_queued_value)
    TextView queuedValueTextView;
    @Bind(R.id.text_printing_value)
    TextView printingValueTextView;
    @Bind(R.id.text_printed_value)
    TextView printedValueTextView;
    @Bind(R.id.text_failed_printed_value)
    TextView failedPrintedTextView;

    public LatexProductionPanel(Context context) {
        super(context);
    }

    public LatexProductionPanel(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public LatexProductionPanel(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public Spannable getTitleSpannable() {
        SpannableStringBuilder builder = new SpannableStringBuilder(getContext().getString(R.string.production_panel_title));
        return builder;
    }

    @Override
    public int getContentView() {
        return R.layout.production_snapshot_content_latext;
    }

    @Override
    public void updateViewModel(ProductionViewModel viewModel) {

        setViewModel(viewModel);

        if (showEmptyCard()) {
            return;
        }

        int queued = viewModel.getJobsInQueue();
        int printing = viewModel.getNumberOfPrintingJobs();
        int printed = viewModel.getNumberOfTodayPrintedJobs();

        queuedValueTextView.setText(queued == LatexProductionSnapshotViewModel.ERROR_VALUE ?
                getContext().getString(R.string.unknown_value) : getLocalizedValue(viewModel.getJobsInQueue()));

        printingValueTextView.setText(printing == LatexProductionSnapshotViewModel.ERROR_VALUE ?
                getContext().getString(R.string.unknown_value) : getLocalizedValue(viewModel.getNumberOfPrintingJobs()));

        printedValueTextView.setText(printed == LatexProductionSnapshotViewModel.ERROR_VALUE ?
                getContext().getString(R.string.unknown_value) : getLocalizedValue(viewModel.getNumberOfTodayPrintedJobs()));

        failedPrintedTextView.setText(getLocalizedValue(viewModel.getNumberOfTodayFailedPrintedJobs()));
        failedPrintedTextView.setVisibility(viewModel.getNumberOfTodayFailedPrintedJobs() > 0 ? VISIBLE : GONE);
    }

    @Override
    protected boolean isValidViewModel() {
        return getViewModel() != null;
    }

    protected String getEmptyCardText() {
        return getContext().getString(R.string.no_information_to_display);
    }

    private String getLocalizedValue(int value) {
        return HPLocaleUtils.getLocalizedValue(value);
    }

}
