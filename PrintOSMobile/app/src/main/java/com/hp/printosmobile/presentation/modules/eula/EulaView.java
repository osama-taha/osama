package com.hp.printosmobile.presentation.modules.eula;

import com.hp.printosmobile.presentation.MVPView;

/**
 * Created by Anwar Asbah on 9/1/16.
 */
public interface EulaView extends MVPView {

    void displayEula(EULAViewModel eulaViewModel);

}
