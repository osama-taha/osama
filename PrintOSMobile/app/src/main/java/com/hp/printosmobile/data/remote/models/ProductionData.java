package com.hp.printosmobile.data.remote.models;

/**
 * Created by leviasaf on 21/04/2016.
 */

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "activeJobs",
        "printedJobs",
        "jobs"
})
public class ProductionData {

    @JsonProperty("activeJobs")
    private String activeJobs;
    @JsonProperty("printedJobs")
    private String printedJobs;
    @JsonProperty("jobs")
    private List<JobData> jobs = new ArrayList<JobData>();
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     *
     * @return
     * The activeJobs
     */
    @JsonProperty("activeJobs")
    public String getActiveJobs() {
        return activeJobs;
    }

    /**
     *
     * @param activeJobs
     * The activeJobs
     */
    @JsonProperty("activeJobs")
    public void setActiveJobs(String activeJobs) {
        this.activeJobs = activeJobs;
    }

    /**
     *
     * @return
     * The printedJobs
     */
    @JsonProperty("printedJobs")
    public String getPrintedJobs() {
        return printedJobs;
    }

    /**
     *
     * @param printedJobs
     * The printedJobs
     */
    @JsonProperty("printedJobs")
    public void setPrintedJobs(String printedJobs) {
        this.printedJobs = printedJobs;
    }

    /**
     *
     * @return
     * The jobs
     */
    @JsonProperty("jobs")
    public List<JobData> getJobs() {
        return jobs;
    }

    /**
     *
     * @param jobs
     * The jobs
     */
    @JsonProperty("jobs")
    public void setJobs(List<JobData> jobs) {
        this.jobs = jobs;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}