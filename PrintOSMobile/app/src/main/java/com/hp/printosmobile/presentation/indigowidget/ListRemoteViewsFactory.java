package com.hp.printosmobile.presentation.indigowidget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.presentation.modules.filters.FilterItem;
import com.hp.printosmobile.presentation.modules.home.HomeDataManager;
import com.hp.printosmobile.presentation.modules.home.PrintBeatDashboardViewModel;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitViewModel;
import com.hp.printosmobile.presentation.modules.shared.DeviceViewModel;
import com.hp.printosmobilelib.core.logging.HPLogger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.functions.Func1;
import rx.functions.FuncN;
import rx.schedulers.Schedulers;

/**
 * Created by Osama Taha on 2/10/17.
 */
public class ListRemoteViewsFactory implements RemoteViewsService.RemoteViewsFactory {

    private static final String TAG = ListRemoteViewsFactory.class.getSimpleName();

    private static final int VIEW_TYPE_COUNT = 1;
    private final WidgetType mWidgetType;

    private Context mContext;
    private boolean mFinishWithError;

    private List<PrintBeatDashboardViewModel> mPrintBeatViewModels = new ArrayList<>();

    public ListRemoteViewsFactory(Context context, Intent intent, WidgetType widgetType) {
        this.mContext = context;
        this.mWidgetType = widgetType;
    }

    @Override
    public void onCreate() {

    }

    @Override
    public int getCount() {
        return mPrintBeatViewModels == null ? 0 : mPrintBeatViewModels.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private PrintBeatDashboardViewModel getPrintBeatDashboardViewModel(int index) {
        return mPrintBeatViewModels.get(index);
    }

    @Override
    public RemoteViews getLoadingView() {
        return null;
    }

    @SuppressLint("NewApi")
    @Override
    public RemoteViews getViewAt(int position) {

        if (getCount() == 0) {
            return null;
        }

        IndigoWidgetRemoteViews indigoWidgetRemoteViews = new IndigoWidgetRemoteViews(mContext, mWidgetType);

        PrintBeatDashboardViewModel printBeatDashboardViewModel = getPrintBeatDashboardViewModel(position);

        return indigoWidgetRemoteViews.applyViewModel(printBeatDashboardViewModel);
    }

    @Override
    public int getViewTypeCount() {
        return VIEW_TYPE_COUNT;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public void onDataSetChanged() {

        HPLogger.d(TAG, "widget DataSetChanged " + mWidgetType.name());

        Analytics.sendEvent(Analytics.APP_WIDGET_UPDATE_ACTION);

        IndigoWidgetRemoteViews remoteViews = new IndigoWidgetRemoteViews(mContext, mWidgetType);

        remoteViews.loading();

        boolean isLoggedIn = PrintOSPreferences.getInstance(mContext).isLoggedIn();

        if (isLoggedIn) {
            loadData();
        } else {
            //Not logged in.
            mFinishWithError = false;
            mPrintBeatViewModels = null;
        }

        if (!isLoggedIn) {
            HPLogger.d(TAG, "User not logged in, unable to update the widget.");
            remoteViews.showUserNotLoggedInView();
            remoteViews.setShowNextPrevButton(false);
        } else if (mFinishWithError) {
            HPLogger.d(TAG, "show widget error layout.");
            remoteViews.showErrorView(mContext.getString(R.string.unexpected_error));
            remoteViews.setShowNextPrevButton(false);
        } else if (getCount() == 0) {
            HPLogger.d(TAG, "No information to display.");
            remoteViews.showErrorView(mContext.getString(R.string.no_information_to_display));
            remoteViews.setShowNextPrevButton(false);
        } else {
            HPLogger.d(TAG, "Refresh widget flipper.");
            remoteViews.showWidgetFlipper();
            remoteViews.setDisplayedChild(0);
            remoteViews.setShowNextPrevButton(getCount() > 1);
        }

        remoteViews.loadComplete();

    }

    private void loadData() {

        Subscription observable = HomeDataManager.initBusinessUnits(mContext)
                .subscribeOn(Schedulers.newThread())
                .observeOn(Schedulers.newThread())
                .flatMap(new Func1<Map<BusinessUnitEnum, BusinessUnitViewModel>, Observable<List<PrintBeatDashboardViewModel>>>() {
                    @Override
                    public Observable<List<PrintBeatDashboardViewModel>> call(Map<BusinessUnitEnum, BusinessUnitViewModel> businessUnitEnumBusinessUnitViewModelMap) {

                        if (businessUnitEnumBusinessUnitViewModelMap.containsKey(BusinessUnitEnum.INDIGO_PRESS)) {
                            BusinessUnitViewModel indigoBusinessUnit = businessUnitEnumBusinessUnitViewModelMap.get(BusinessUnitEnum.INDIGO_PRESS);

                            List<Observable<PrintBeatDashboardViewModel>> observables = new ArrayList<>();

                            for (FilterItem siteViewModel : indigoBusinessUnit.getFiltersViewModel().getSites()) {
                                indigoBusinessUnit.getFiltersViewModel().setSelectedSite(siteViewModel);
                                observables.add(HomeDataManager.getPrintBeatViewModel(indigoBusinessUnit, false, false));
                            }

                            return Observable.combineLatest(observables, new FuncN<List<PrintBeatDashboardViewModel>>() {
                                @Override
                                public List<PrintBeatDashboardViewModel> call(Object... args) {

                                    List<PrintBeatDashboardViewModel> printBeatDashboardViewModels = new ArrayList<>();
                                    for (Object object : args) {
                                        if (object instanceof PrintBeatDashboardViewModel) {
                                            PrintBeatDashboardViewModel printBeatDashboardViewModel = (PrintBeatDashboardViewModel) object;
                                            List<DeviceViewModel> deviceViewModels = printBeatDashboardViewModel.getTodayViewModel().getDeviceViewModels();
                                            Collections.sort(deviceViewModels, DeviceViewModel.RT_DEVICES_FIRST_COMPARATOR);
                                            printBeatDashboardViewModels.add(printBeatDashboardViewModel);
                                        }
                                    }
                                    return printBeatDashboardViewModels;
                                }
                            });

                        } else {

                            mPrintBeatViewModels = new ArrayList<>();
                            mFinishWithError = false;

                            return Observable.empty();

                        }

                    }
                }).subscribe(new Subscriber<List<PrintBeatDashboardViewModel>>() {
                    @Override
                    public void onCompleted() {

                        HPLogger.e(TAG, "widget on complete ");

                    }

                    @Override
                    public void onError(Throwable e) {

                        HPLogger.e(TAG, "Finish getting data for widget with error ", e);

                        mPrintBeatViewModels = null;
                        mFinishWithError = true;
                        unsubscribe();

                    }

                    @Override
                    public void onNext(List<PrintBeatDashboardViewModel> printBeatDashboardViewModels) {

                        HPLogger.d(TAG, "Finish getting data for widget.");

                        mPrintBeatViewModels = printBeatDashboardViewModels;

                        mFinishWithError = false;

                        unsubscribe();
                    }
                });

        while (!observable.isUnsubscribed()) {
        }


    }

    @Override
    public void onDestroy() {

        if (mPrintBeatViewModels != null) {
            mPrintBeatViewModels.clear();
        }

    }

}