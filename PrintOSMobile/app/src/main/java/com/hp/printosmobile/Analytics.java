package com.hp.printosmobile;

import android.app.Application;
import android.os.Bundle;
import android.text.TextUtils;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.presentation.modules.shared.UserViewModel;
import com.hp.printosmobilelib.core.logging.HPLogger;
import com.hp.printosmobilelib.core.utils.DeviceUtils;

/**
 * Helps with logging custom events and errors to Google Analytics.
 * <p/>
 * Created by Osama on 1/3/2016.
 */
public class Analytics {

    private static final String TAG = Analytics.class.getSimpleName();

    //SCREEN NAMES
    public static final String SPLASH_SCREEN_NAME = "splash";
    public static final String LOGIN_SCREEN_NAME = "login";
    public static final String MAIN_SCREEN_NAME = "main";
    public static final String REPORTS_SCREEN_NAME = "reports";
    public static final String ABOUT_SCREEN_NAME = "about";
    public static final String SETTINGS_SCREEN_NAME = "settings";
    public static final String ASK_THE_EXPERT_SCREEN_NAME = "ask the expert";

    private static final String USER_ID_PARAMETER = "&uid";

    //MAIN CATEGORY FOR EVENTS
    public static final String MOBILE_PRINT_BEAT_CATEGORY = "Mobile Print Beat";

    //LOGIN EVENTS:
    public static final String LOGIN_EVENT_LOGIN_ACTION = "login";
    public static final String LOGIN_EVENT_LOGOUT_ACTION = "logout";
    public static final String LOGIN_EVENT_AUTO_LOGIN_ACTION = "Automatic Login";
    public static final String LOGIN_EVENT_AUTO_LOGIN_FAILED_ACTION = "Automatic Login Failed";
    public static final String LOGIN_EVENT_EULA_LABEL = "EULA";
    public static final String LOGIN_EVENT_LOCKED_LABEL = "LOCKED";
    public static final String LOGIN_EVENT_EXPIRED_LABEL = "EXPIRED";
    public static final String LOGIN_EVENT_CREDENTIALS_LABLE = "CREDENTIALS";
    public static final String LOGIN_EVENT_LOGIN_OTHER_LABEL = "OTHER<%s>";

    //CHART CLICKED
    public static final String CHART_CLICKED_ACTION = "chart clicked";

    //SETTINGS EVENTS:
    public static final String SETTINGS_CLICKED_ACTION = "settings clicked";
    public static final String ABOUT_CLICKED_ACTION = "about clicked";
    public static final String REPORT_CLICKED_ACTION = "chart clicked";
    public static final String ASK_THE_EXPERT_CLICKED_ACTION = "ask the expert clicked";
    public static final String OPEN_SOURCE_CLICKED_ACTION = "open sources license clicked";

    //FILTER EVENTS:
    public static final String PRESS_FILTER_ACTION = "select press filter";
    public static final String PRESS_FILTER_SELECT_ALL_LABEL = "all";

    //PERSONAL ADVISOR EVENTS:
    public static final String PERSONAL_ADVISOR_ACTION = "personal adviser";

    public static final String PERSONAL_ADVISOR_SHOW_LABEL = "show";
    public static final String PERSONAL_ADVISOR_SWIPE_LEFT_LABEL = "swipe left";
    public static final String PERSONAL_ADVISOR_SWIPE_RIGHT_LABEL = "swipe right";
    public static final String PERSONAL_ADVISOR_DELETE_LABEL = "delete";
    public static final String PERSONAL_ADVISOR_LIKE_LABEL = "like";
    public static final String PERSONAL_ADVISOR_UNLIKE_LABEL = "unlike";
    public static final String PERSONAL_ADVISOR_UNHIDE_ALL = "unhide all";


    //WEEK PANEL EVENTS:
    public static final String SWITCH_TO_WEEK_VIEW_ACTION = "switch to week view";

    public static final String WEEK_VIEW_ACTION = "week view";
    public static final String WEEK_VIEW_SHOW_LABEL = "Week-%d";

    //TODAY PANEL EVENTS:
    public static final String SWITCH_TO_TODAY_VIEW_ACTION = "switch to today's view";
    public static final String TODAY_VIEW_ACTION = "today's view";
    public static final String TODAY_VIEW_SHOW_SCORE_VIEW = "score ";
    public static final String TODAY_VIEW_SHOW_HISTOGRAM_VIEW = "histogram";

    public static final String BROWSE_MACHINE_VIEW_ACTION = "browse machine views";
    public static final String MACHINE_VIEW_ACTION = "machine view";
    public static final String MACHINE_VIEW_SWIPE_LEFT = "swipe left";
    public static final String MACHINE_VIEW_SWIPE_RIGHT = "swipe right";

    //REPORTS EVENTS:
    public static final String REPORT_ACTION = "report";
    public static final String REPORT_SET_REPORT_TYPE_ACTION = "set report type";
    public static final String REPORT_SHOW_VALUES_LABEL = "show values";
    public static final String REPORT_HIDE_VALUES_LABEL = "hide values";
    public static final String REPORT_ZOOM_IN_LABEL = "zoom in";
    public static final String REPORT_ZOOM_OUT_LABEL = "zoom out";

    //LANGUAGE EVENTS:
    public static final String SETTINGS_CHANGE_LANGUAGE_ACTION = "change language";
    public static final String SETTINGS_CHANGE_UNIT_SYSTEM_ACTION = "change unit system";

    //CONTACT HP
    public static final String CONTACT_HP_SCREENSHOT_LABEL = "Screenshot";

    //ASK THE EXPERT EVENTS:
    public static final String ASK_THE_EXPERT_SEND_ACTION = "send ask the expert";
    public static final String ASK_THE_EXPERT_ADD_SCREENSHOT_LABEL = "Screenshot";

    //OPEN SOURCE LICENSES EVENTS:
    public static final String OPEN_SOURCE_SELECT_ACTION = "select open source library";

    //REFRESH EVENTS:
    public static final String REFRESH_DATE_EVENT = "refresh";

    // New events
    public static final String SWITCH_EQUIPMENT_ACTION = "switch equipment";
    public static final String VIEW_DEVICE_DETAILS_ACTION = "view device details";
    public static final String REPORT_A_PROBLEM_CLICKED_ACTION =
            "report a problem Clicked";
    public static final String REPORT_A_PROBLEM_SEND_ACTION = "send report a problem";
    public static final String FEEDBACK_TO_HP_CLICKED_ACTION =
            "feedback to HP clicked";
    public static final String FEEDBACK_TO_HP_SEND_ACTION = "send feedback to HP";
    public static final String VIEW_NEXT_10_JOBS_ACTION = "view next 10 jobs";
    public static final String VIEW_SERVICE_CALLS_ACTION = "view service calls";
    public static final String SWITCH_ACCOUNT_ACTION = "switch account";
    public static final String FORGOT_PASSWORD_CLICKED_ACTION =
            "Forgot Password clicked";
    public static final String FORGOT_PASSWORD_REQUEST_SENT_ACTION =
            "Forgot Password request sent";
    public static final String LOGIN_FAILED_ACTION = "Login failed";

    public static final String VIEW_NOTIFICATIONS_LIST_ACTION = "view notifications list";
    public static final String VIEW_NOTIFICATIONS_SETTINGS_ACTION = "view notifications settings";
    public static final String VIEW_CHANGE_ORGANIZATION_ACTION = "view change organization";
    public static final String GOOGLE_PLAY_SERVICES_STATUS_ACTION = "Google Play Services Status";

    public static final String VIEW_LANDSCAPE_HISTOGRAM_ACTION = "view histogram in landscape";


    public static final String SHOW_NPS_ACTION = "nps popup shown";
    public static final String SUBMIT_NPS_ACTION = "submit nps";
    public static final String SHOW_BEAT_COINS_ACTION = "beat coins popup shown";
    public static final String COLLECT_COINS_CLICKED_ACTION = "collect coins clicked";

    public static final String APP_WIDGET_UPDATE_ACTION = "app widget update";
    public static final String APP_OPENED_ACTION = "app opened";

    public static final String SHOW_NOTIFICATION_ACTION = "show notification";
    public static final String CLICK_NOTIFICATION_ACTION = "click notification";
    public static final String UNSUBSCRIBE_NOTIFICATION_ACTION = "unsubscribe notification";
    public static final String SUBSCRIBE_NOTIFICATION_ACTION = "subscribe notification";
    public static final String NOTIFICATION_DELETED_ACTION = "notification deleted";
    public static final String NOTIFICATION_MARKED_READ_ACTION = "notification marked read";

    public static final String ACCEPT_LOCATION_SERVICES = "accept location services";
    public static final String DENY_LOCATION_SERVICES = "deny location services";
    public static final String OPEN_APP_SETTINGS = "click go to settings";

    private static final String FIREBASE_ANALYTICS_PUSH_NOTIFICATION_EVENT = "pb_push_notification";
    private static final String FIREBASE_ANALYTICS_PUSH_NOTIFICATION_ARRIVE_EVENT = "pb_push_notification_arrived";
    private static final String FIREBASE_ANALYTICS_PUSH_NOTIFICATION_TYPE_KEY = "pb_type";
    private static final String FIREBASE_CURRENT_STACK = "current_stack";
    private static final String FIREBASE_USER_ID = "printos_user";

    /**
     * The index of the user id dimension.
     */
    private static final int USER_ID_DIMENSION_INDEX = 1;

    /**
     * The index of the organization id dimension.
     */
    private static final int ORGANIZATION_ID_DIMENSION_INDEX = 2;

    private static final int HP_INTERNAL_ORGANIZATION_DIMENSION_INDEX = 3;

    private static final int USER_ROLES_DIMENSION_INDEX = 4;

    private static final int ORGANIZATION_TYPE_INDEX = 5;

    private static final int SELF_PROVISION_ORGANIZATION_INDEX = 6;

    private static final int ENTITY_TYPE_DIMENSION_INDEX = 7;

    private static final int DEVICE_ID_CUSTOM_DIMENSION_INDEX = 11;

    private static final String ENTITY_TYPE_MOBILE_USER = "Mobile User";

    private static final String HP_INTERNAL_ORGANIZATION_PREFIX = "HP Inc.";

    private static Tracker tracker;

    /**
     * An object holds the currently logged in user
     */
    private static UserViewModel signedInUser;

    /**
     * Enum used to identify the tracker that needs to be used for tracking.
     */
    public enum TrackerName {
        TESTING_TRACKER, // Tracker used only at development time.
        STG_TRACKER, // Tracker used only for Staging server usually at alpha time.
        RELEASE_TRACKER, // Tracker used only at FT and release time.
    }

    /**
     * Init the default {@link Tracker} for this {@link Application}.
     *
     * @return tracker
     */
    synchronized public static void init(TrackerName trackerName, Application application) {

        GoogleAnalytics analytics = GoogleAnalytics.getInstance(application);

        signedInUser = PrintOSPreferences.getInstance(application).getUserInfo();

        String trackerId = (trackerName == TrackerName.TESTING_TRACKER) ? application.getString(R.string.ga_testing_tracker_id)
                : (trackerName == TrackerName.STG_TRACKER) ? application.getString(R.string.ga_staging_tracker_id)
                : application.getString((R.string.ga_release_tracker_id));

        HPLogger.d(TAG, "init google analytics, tracker name: " + trackerName.name());

        tracker = analytics.newTracker(trackerId);
        //By setting the user ID on the tracker, the ID will be sent with all subsequent hits.
        tracker.set(USER_ID_PARAMETER, signedInUser.getUserId());

        if (!TextUtils.isEmpty(signedInUser.getUserId())) {
            FirebaseAnalytics.getInstance(application).setUserId(signedInUser.getUserId());
        }

    }

    /**
     * Sends a screen name event.
     *
     * @param screenName - name of the screen.
     */
    public static void sendScreenName(String screenName) {

        if (tracker == null) {
            HPLogger.e(TAG, "The global tracker ID should be initialized by the derived application class");
            // do not throw exception and not return so null object exception will occur later
        }

        tracker.setScreenName(screenName);
        tracker.send(getScreenBuilderWithDimensions().build());

    }

    public static void sendEvent(String action) {
        sendEvent(MOBILE_PRINT_BEAT_CATEGORY, action);
    }

    /**
     * Logs an event for analytics.
     *
     * @param category - name of the event's category.
     * @param action   - name of the event's action.
     */
    public static void sendEvent(String category, String action) {

        tracker.send(getEventBuilderWithDimensions()
                .setCategory(category)
                .setAction(action)
                .build());

        HPLogger.d(TAG, String.format("Google analytics event sent: %s", action));
    }

    /**
     * Logs an event for analytics.
     *
     * @param category - name of the event's category.
     * @param action   - name of the event's action.
     * @param label    - name of the event's label.
     */
    public static void sendEvent(String category, String action, String label) {

        tracker.send(getEventBuilderWithDimensions()
                .setCategory(category)
                .setAction(action)
                .setLabel(label)
                .build());

        HPLogger.d(TAG, String.format("Google analytics event sent: %s|%s", action, TextUtils.isEmpty(label) ? "" : label));

    }

    /**
     * Creates an event builder object and Attaches custom dimensions.
     *
     * @return {@link com.google.android.gms.analytics.HitBuilders.EventBuilder}
     */
    private static HitBuilders.EventBuilder getEventBuilderWithDimensions() {

        HitBuilders.EventBuilder builder = new HitBuilders.EventBuilder()
                .setCustomDimension(USER_ID_DIMENSION_INDEX, signedInUser.getUserId())
                .setCustomDimension(ORGANIZATION_ID_DIMENSION_INDEX, signedInUser.getUserOrganization().getOrganizationId());

        String organizationName = signedInUser.getUserOrganization().getOrganizationName();

        if (organizationName != null && !organizationName.isEmpty()) {
            builder.setCustomDimension(HP_INTERNAL_ORGANIZATION_DIMENSION_INDEX, getHPInternalOrganizationDimenValue(organizationName));
            builder.setCustomDimension(ORGANIZATION_TYPE_INDEX, signedInUser.getUserOrganization().getOrganizationTypeDisplayName());
        }

        builder.setCustomDimension(USER_ROLES_DIMENSION_INDEX, signedInUser.getUserRoles());

        String provision = signedInUser.getSelfProvisionOrg();
        if (provision != null && !provision.isEmpty()) {
            builder.setCustomDimension(SELF_PROVISION_ORGANIZATION_INDEX, signedInUser.getSelfProvisionOrg());
        }

        builder.setCustomDimension(ENTITY_TYPE_DIMENSION_INDEX, ENTITY_TYPE_MOBILE_USER);
        builder.setCustomDimension(DEVICE_ID_CUSTOM_DIMENSION_INDEX, DeviceUtils.getDeviceIdMD5(PrintOSApplication.getAppContext()));

        return builder;
    }

    /**
     * Creates a screen builder object and Attaches custom dimensions.
     *
     * @return {@link com.google.android.gms.analytics.HitBuilders.ScreenViewBuilder}
     */
    private static HitBuilders.ScreenViewBuilder getScreenBuilderWithDimensions() {

        HitBuilders.ScreenViewBuilder builder = new HitBuilders.ScreenViewBuilder()
                .setCustomDimension(USER_ID_DIMENSION_INDEX, signedInUser.getUserId())
                .setCustomDimension(ORGANIZATION_ID_DIMENSION_INDEX, signedInUser.getUserOrganization().getOrganizationId());

        String organizationName = signedInUser.getUserOrganization().getOrganizationName();

        if (organizationName != null && !organizationName.isEmpty()) {
            builder.setCustomDimension(HP_INTERNAL_ORGANIZATION_DIMENSION_INDEX, getHPInternalOrganizationDimenValue(organizationName));
            builder.setCustomDimension(ORGANIZATION_TYPE_INDEX, signedInUser.getUserOrganization().getOrganizationTypeDisplayName());
        }

        builder.setCustomDimension(USER_ROLES_DIMENSION_INDEX, signedInUser.getUserRoles());

        builder.setCustomDimension(ENTITY_TYPE_DIMENSION_INDEX, ENTITY_TYPE_MOBILE_USER);

        builder.setCustomDimension(DEVICE_ID_CUSTOM_DIMENSION_INDEX, DeviceUtils.getDeviceIdMD5(PrintOSApplication.getAppContext()));

        return builder;
    }

    private static String getHPInternalOrganizationDimenValue(String organization) {

        //if an organization name init with “HP Inc.” return "true", otherwise return "false".
        return organization.startsWith(HP_INTERNAL_ORGANIZATION_PREFIX) ? "true" : "false";

    }

    public static void sendNotificationClickedFirebaseEvent(String notificationType) {
        sendNotificationTypeFirebaseAnalytics(FIREBASE_ANALYTICS_PUSH_NOTIFICATION_EVENT, notificationType);
    }


    public static void sendNotificationArriveFirebaseEvent(String notificationType) {
        sendNotificationTypeFirebaseAnalytics(FIREBASE_ANALYTICS_PUSH_NOTIFICATION_ARRIVE_EVENT, notificationType);
    }


    public static void sendNotificationTypeFirebaseAnalytics(String eventName, String notificationType) {

        HPLogger.d(TAG, "Firebase analytics event sent " + eventName);

        Bundle bundle = new Bundle();
        bundle.putString(FIREBASE_ANALYTICS_PUSH_NOTIFICATION_TYPE_KEY, notificationType);

        String serverName = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).getServerName();
        bundle.putString(FIREBASE_CURRENT_STACK, serverName);

        String userId = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).getUserInfo().getUserId();

        if (!TextUtils.isEmpty(userId)) {
            bundle.putString(FIREBASE_USER_ID, userId);
        }

        FirebaseAnalytics.getInstance(PrintOSApplication.getAppContext()).logEvent(eventName, bundle);
    }

}