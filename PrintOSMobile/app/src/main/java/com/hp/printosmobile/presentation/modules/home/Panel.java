package com.hp.printosmobile.presentation.modules.home;

import com.hp.printosmobile.presentation.modules.insights.InsightsViewModel;
import com.hp.printosmobile.presentation.modules.personaladvisor.PersonalAdvisorPanel;
import com.hp.printosmobile.presentation.modules.printersnapshot.PrinterSnapshotPanel;
import com.hp.printosmobile.presentation.modules.productionsnapshot.indigo.IndigoProductionPanel;
import com.hp.printosmobile.presentation.modules.productionsnapshot.latex.LatexProductionPanel;
import com.hp.printosmobile.presentation.modules.servicecallpanel.ServiceCallPanel;
import com.hp.printosmobile.presentation.modules.today.TodayPanel;
import com.hp.printosmobile.presentation.modules.week.WeekPanel;

/**
 * Created by Osama Taha on 5/18/16.
 */
public enum Panel {

    LATEX_PRODUCTION(1, LatexProductionPanel.TAG),
    TODAY(2, TodayPanel.TAG),
    PRINTERS(3, PrinterSnapshotPanel.TAG),
    PERFORMANCE(4, WeekPanel.TAG),
    PERSONAL_ADVISOR(5, PersonalAdvisorPanel.TAG),
    INDIGO_PRODUCTION(6, IndigoProductionPanel.TAG),
    PANEL_SERVICE_CALL(7, ServiceCallPanel.TAG),
    JAM_CHART(8, InsightsViewModel.InsightKpiEnum.JAM.getTAG()),
    FAILURE_CHART(9, InsightsViewModel.InsightKpiEnum.FAILURE.getTAG()),
    UNKNOWN(-1, "");

    private int panelOrder;
    private String panelClass;

    Panel(int panelOrder, String panelClass) {
        this.panelOrder = panelOrder;
        this.panelClass = panelClass;
    }

    public int getPanelOrder() {
        return panelOrder;
    }

    public String getPanelTag() {
        return panelClass;
    }

    public static Panel from(String panelTag) {
        for (Panel panel : Panel.values()) {
            if (panel.getPanelTag().equals(panelTag))
                return panel;
        }
        return Panel.UNKNOWN;
    }
}
