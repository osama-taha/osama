package com.hp.printosmobile.presentation.modules.servicecallpanel;

import android.content.Context;
import android.support.v4.content.res.ResourcesCompat;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.util.AttributeSet;
import android.widget.TextView;

import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.modules.servicecallpanel.ServiceCallViewModel.ServiceCallStateEnum;
import com.hp.printosmobile.utils.HPLocaleUtils;
import com.hp.printosmobilelib.ui.widgets.panel.PanelView;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by Anwar Asbah on 3/9/2017.
 */
public class ServiceCallPanel extends PanelView<ServiceCallViewModel> {

    public final static String TAG = ServiceCallPanel.class.getSimpleName();

    @Bind(R.id.text_closed_call_label)
    TextView closedLabel;
    @Bind(R.id.text_open_calls_label)
    TextView openedLabel;
    @Bind(R.id.text_open_calls_value)
    TextView numberOfOpenCalls;
    @Bind(R.id.text_closed_call_value)
    TextView numberOfClosedCalls;

    private ServiceCallPanelCallback serviceCallPanelCallback;

    public ServiceCallPanel(Context context) {
        super(context);
    }

    public ServiceCallPanel(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ServiceCallPanel(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setServiceCallPanelCallback(ServiceCallPanelCallback serviceCallPanelCallback) {
        this.serviceCallPanelCallback = serviceCallPanelCallback;
    }

    @Override
    public Spannable getTitleSpannable() {
        SpannableStringBuilder builder = new SpannableStringBuilder(getContext().getString(R.string.service_call_panel_title));
        return builder;
    }

    @Override
    public int getContentView() {
        return R.layout.service_call_panel;
    }

    @Override
    public void updateViewModel(ServiceCallViewModel viewModel) {

        setViewModel(viewModel);

        if (showEmptyCard()) {
            return;
        }

        if (viewModel != null && viewModel.getCallViewModels() != null) {

            int closed = viewModel.getNumberOfClosedCalls();
            int opened = viewModel.getNumberOfOpenCalls();

            numberOfOpenCalls.setText(HPLocaleUtils.getLocalizedValue(opened));
            openedLabel.setText(getContext().getString(opened == 1 ? R.string.service_call_open
                    : R.string.service_calls_open));

            numberOfClosedCalls.setText(HPLocaleUtils.getLocalizedValue(closed));
            closedLabel.setText(getContext().getString(closed == 1 ? R.string.service_call_closed
                    : R.string.service_calls_closed));

            numberOfOpenCalls.setTextColor(ResourcesCompat.getColor(getResources(),
                    ServiceCallStateEnum.OPEN.getColor(), null));
            numberOfClosedCalls.setTextColor(ResourcesCompat.getColor(getResources(),
                    ServiceCallStateEnum.CLOSED.getColor(), null));
        }
    }

    @Override
    protected boolean isValidViewModel() {
        return !(getViewModel() == null
                || getViewModel().getCallViewModels() == null
                || getViewModel().getCallViewModels().size() == 0);
    }

    protected String getEmptyCardText() {
        return getContext().getString(R.string.no_information_to_display);
    }

    @OnClick(R.id.opened_layout)
    public void onOpenLayoutClicked() {
        if (getViewModel() != null) {
            if (serviceCallPanelCallback != null && getViewModel() != null) {
                serviceCallPanelCallback.onServiceCallPanelClicked(getViewModel(), ServiceCallStateEnum.OPEN);
            }
        }
    }

    @OnClick(R.id.closed_layout)
    public void onCloseLayoutClicked() {
        if (getViewModel() != null) {
            if (serviceCallPanelCallback != null && getViewModel() != null) {
                serviceCallPanelCallback.onServiceCallPanelClicked(getViewModel(), ServiceCallStateEnum.CLOSED);
            }
        }
    }

    public interface ServiceCallPanelCallback {
        void onServiceCallPanelClicked(ServiceCallViewModel serviceCallViewModel, ServiceCallStateEnum serviceCallStateEnum);
    }
}
