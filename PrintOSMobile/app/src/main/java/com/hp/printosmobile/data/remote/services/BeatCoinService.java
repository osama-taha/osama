package com.hp.printosmobile.data.remote.services;

import com.hp.printosmobile.data.remote.ApiConstants;
import com.hp.printosmobile.data.remote.models.BeatCoinData;

import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Path;
import rx.Observable;

/**
 * Created by Anwar Asbah 7/18/2017
 */
public interface BeatCoinService {

    @GET(ApiConstants.BEAT_COINS_API)
    Observable<Response<BeatCoinData>> getBeatCoinsDetails(@Path("id") String id);
}
