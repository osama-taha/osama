package com.hp.printosmobile.presentation.modules.timezonedialog;

import android.app.Activity;
import android.text.TextUtils;

import com.hp.printosmobile.AppseeSdk;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.data.remote.models.PreferencesData;
import com.hp.printosmobile.data.remote.services.PreferencesService;
import com.hp.printosmobile.data.remote.services.TimeZone;
import com.hp.printosmobile.presentation.modules.main.MainActivity;
import com.hp.printosmobilelib.core.logging.HPLogger;

import java.util.Calendar;
import java.util.List;

import retrofit2.Response;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by Osama Taha on 7/25/17.
 */

public class TimeZoneManager {

    private static final String TAG = TimeZoneManager.class.getSimpleName();

    private static TimeZoneManager timeZoneManager = new TimeZoneManager();

    private PreferencesData preferencesData;
    private boolean isShowing;
    private final static int MIN_NUMBER_OF_AUTHENTICATED_SESSIONS = 10;

    public static TimeZoneManager getInstance() {
        return timeZoneManager;
    }

    public boolean showTimeZoneDialog(final Activity activity) {

        int authenticatedSessions = PrintOSPreferences.getInstance(activity).getNumberOfAuthenticatedSessionsForTimeZoneDialog();
        boolean timeZonePopupShown = PrintOSPreferences.getInstance(activity).isTimeZonePopupShown();

        boolean shouldShowPopup = PrintOSPreferences.getInstance(activity).isTimeZonePopupEnabled() &&
                (!timeZonePopupShown || authenticatedSessions >= MIN_NUMBER_OF_AUTHENTICATED_SESSIONS);

        if (!shouldShowPopup) {
            HPLogger.d(TAG, "time zone popup is not enabled.");
            return false;
        }

        final PreferencesService preferencesService = PrintOSApplication.getApiServicesProvider()
                .getPreferencesService();

        preferencesService.getPreferences()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .flatMap(new Func1<Response<PreferencesData>, Observable<Response<List<TimeZone>>>>() {
                    @Override
                    public Observable<Response<List<TimeZone>>> call(Response<PreferencesData> preferencesDataResponse) {

                        if (preferencesDataResponse.isSuccessful()) {

                            preferencesData = preferencesDataResponse.body();

                            if (TextUtils.isEmpty(preferencesData.getGeneral().getTimeZone())) {

                                HPLogger.d(TAG, "timezone is not set, get list of timezones...");

                                return preferencesService.getTimeZones()
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribeOn(Schedulers.io());

                            } else {
                                HPLogger.d(TAG, "timezone is set.");
                                PrintOSPreferences.getInstance(activity).setTimeZonePopupShown(true);
                            }
                        }
                        onDialogDismissed(activity);
                        return Observable.empty();
                    }
                }).subscribe(new Subscriber<Response<List<TimeZone>>>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                HPLogger.e(TAG, "error getting user preferences/timezones list " + e);

                onDialogDismissed(activity);
            }

            @Override
            public void onNext(Response<List<TimeZone>> timeZoneListResponse) {
                
                if (timeZoneListResponse.isSuccessful()) {

                    List<TimeZone> timeZones = timeZoneListResponse.body();

                    if (timeZones != null) {

                        HPLogger.d(TAG, "list of supported timezones " + timeZones.size());

                        String deviceTimeZone = Calendar.getInstance().getTimeZone().getID();

                        if (TextUtils.isEmpty(deviceTimeZone)) {
                            HPLogger.d(TAG, "unable to get device timezone");
                            onDialogDismissed(activity);
                            return;
                        } else {
                            HPLogger.d(TAG, "current device timezone " + deviceTimeZone);
                        }

                        for (TimeZone timeZone : timeZones) {

                            if (!TextUtils.isEmpty(timeZone.getValue()) &&
                                    timeZone.getValue().equalsIgnoreCase(deviceTimeZone)) {

                                preferencesData.getGeneral().setTimeZone(timeZone.getValue());

                                showDialog(activity);
                                return;
                            }
                        }
                    }
                }
                onDialogDismissed(activity);
            }
        });

        return true;

    }

    private void showDialog(final Activity activity) {

        if (isShowing) {
            return;
        }

        PrintOSPreferences.getInstance(activity).setTimeZonePopupShown(true);

        HPLogger.d(TAG, "show timezone dialog.");

        AppseeSdk.getInstance(activity).sendEvent(AppseeSdk.EVENT_TIME_ZONE_DIALOG_SHOWN);

        TimeZoneDialog.getInstance(new TimeZoneDialog.TimeZoneDialogCallback() {
            @Override
            public void onDismiss() {
                isShowing = false;
                PrintOSPreferences.getInstance(activity)
                        .setNumberOfAuthenticatedSessionsForTimeZoneDialog(0);

                onDialogDismissed(activity);
            }

            @Override
            public void onSetButtonClicked() {
                updateUserPreferences(preferencesData);
            }
        }, preferencesData.getGeneral().getTimeZone())
                .show(activity.getFragmentManager(), TAG);


    }

    public void updateUserPreferences(PreferencesData preferencesData) {

        final PreferencesService preferencesService = PrintOSApplication.getApiServicesProvider()
                .getPreferencesService();

        preferencesService.savePreferences(preferencesData)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new Subscriber<Response<PreferencesData>>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                HPLogger.d(TAG, "save user preferences error " + e);
            }

            @Override
            public void onNext(Response<PreferencesData> preferencesDataResponse) {
                if (preferencesDataResponse.isSuccessful()) {
                    HPLogger.d(TAG, "user preferences successfully saved " + preferencesDataResponse.body());
                } else {
                    HPLogger.d(TAG, "response code for save preferences " + preferencesDataResponse.code());
                }
            }
        });

    }

    public void onDialogDismissed (Activity activity) {
        if (activity != null && activity instanceof MainActivity) {
            ((MainActivity) activity).onDialogDismissed();
        }
    }

}
