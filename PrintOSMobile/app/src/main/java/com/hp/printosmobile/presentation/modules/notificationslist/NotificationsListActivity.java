package com.hp.printosmobile.presentation.modules.notificationslist;

import android.content.Intent;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.AppseeSdk;
import com.hp.printosmobile.IntercomSdk;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.notification.NotificationUtils;
import com.hp.printosmobile.presentation.BaseActivity;
import com.hp.printosmobile.presentation.modules.main.MainActivity;
import com.hp.printosmobile.presentation.modules.settings.NotificationsFragment;
import com.hp.printosmobile.presentation.modules.settings.SubscriptionEvent;
import com.hp.printosmobile.utils.AppUtils;
import com.hp.printosmobile.utils.HPUIUtils;
import com.hp.printosmobilelib.core.communications.remote.APIException;
import com.hp.printosmobilelib.core.logging.HPLogger;
import com.hp.printosmobilelib.ui.common.HPFragment;
import com.hp.printosmobilelib.ui.utils.DividerItemDecoration;

import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * create by Anwar Asbah 1/23/2017
 */
public class NotificationsListActivity extends BaseActivity implements NotificationListView,
        NotificationsListAdapter.NotificationAdapterCallback, NotificationsFragment.NotificationFragmentCallback {

    private static final String TAG = NotificationsListActivity.class.getSimpleName();

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.toolbar_display_title)
    TextView toolbarDisplayName;
    @Bind(R.id.notifications_settings)
    ImageView notificationsSettingsView;
    @Bind(R.id.notifications_list)
    RecyclerView notificationsList;
    @Bind(R.id.progress_bar)
    View loadingView;
    @Bind(R.id.no_items_to_show_view)
    View noItemsToShowView;
    @Bind(R.id.intercepting_layout)
    View interceptingLayout;
    @Bind(R.id.error_layout)
    View errorLayout;
    @Bind(R.id.error_msg_text_view)
    TextView errorMsgTextView;
    @Bind(R.id.text_view_delete_all)
    View deleteAllView;

    private NotificationListPresenter presenter;
    private NotificationsListAdapter notificationsListAdapter;
    private HPFragment notificationsSettingsFragment;

    public static NotificationsListActivity newInstance() {
        NotificationsListActivity fragment = new NotificationsListActivity();
        return fragment;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        IntercomSdk.getInstance(this).logEvent(IntercomSdk.PBM_VIEW_NOTIFICATIONS_EVENT);
        Analytics.sendEvent(Analytics.MOBILE_PRINT_BEAT_CATEGORY, Analytics.VIEW_NOTIFICATIONS_LIST_ACTION);

        initView();
        returningFromBackground = true;
    }

    private void initView() {
        toolbarDisplayName.setText(getString(R.string.notifications_list_title));
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(ResourcesCompat.getDrawable(getResources(), R.drawable.back_button, null));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        loadingView.setVisibility(View.VISIBLE);
        deleteAllView.setVisibility(View.GONE);

        notificationsSettingsView.setVisibility(View.VISIBLE);

        //Gray out the settings icon if google play services are not enabled.
        if (!PrintOSApplication.isGooglePlayServicesEnabled()) {
            //Apply gray scale filter for notification settings icon.
            ColorMatrix matrix = new ColorMatrix();
            matrix.setSaturation(0);
            ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);
            notificationsSettingsView.setColorFilter(filter);
        }
    }

    private void initPresenter() {

        if (presenter == null) {
            presenter = new NotificationListPresenter();
            presenter.attachView(this);
        }

        loadingView.setVisibility(View.VISIBLE);
        presenter.getUserNotifications();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_notifications_list;
    }

    @Override
    public void updateNotificationListView(List<NotificationViewModel> notificationViewModels) {
        loadingView.setVisibility(View.GONE);
        displayModels(notificationViewModels);
    }

    private void displayModels(List<NotificationViewModel> notificationViewModels) {
        if (notificationViewModels == null) {
            noItemsToShowView.setVisibility(View.VISIBLE);
            notificationsList.setVisibility(View.INVISIBLE);
            errorLayout.setVisibility(View.GONE);
            return;
        }

        notificationsList.setVisibility(notificationViewModels.size() == 0 ? View.GONE : View.VISIBLE);
        errorLayout.setVisibility(View.GONE);

        boolean emptyDataSetVisible = notificationViewModels.size() == 0;
        showEmptyDataSet(emptyDataSetVisible);

        notificationsList.removeAllViews();
        final LinearLayoutManager manager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        notificationsList.setLayoutManager(manager);
        notificationsListAdapter = new NotificationsListAdapter(this, notificationViewModels, this, notificationsList);
        notificationsList.setAdapter(notificationsListAdapter);
        notificationsList.addItemDecoration(
                new DividerItemDecoration(this, 1, notificationViewModels.size(), false)
        );
    }

    private void showEmptyDataSet(boolean emptyDataSetVisible) {

        noItemsToShowView.setVisibility(emptyDataSetVisible ? View.VISIBLE : View.GONE);
        deleteAllView.setVisibility(emptyDataSetVisible ? View.GONE : View.VISIBLE);
        deleteAllView.setEnabled(true);

    }

    @Override
    public void onNotificationsError(APIException exception) {

        errorLayout.setVisibility(exception.getKind() == APIException.Kind.NETWORK ? View.VISIBLE : View.GONE);
        noItemsToShowView.setVisibility(exception.getKind() == APIException.Kind.NETWORK ? View.GONE : View.VISIBLE);
        notificationsList.setVisibility(View.INVISIBLE);

        loadingView.setVisibility(View.GONE);
        deleteAllView.setVisibility(View.GONE);
    }

    @Override
    public void onAllNotificationsDeleted() {

        loadingView.setVisibility(View.VISIBLE);
        deleteAllView.setVisibility(View.GONE);

        initPresenter();

    }

    @Override
    public void onDeleteAllNotificationsFailed(APIException e) {
        loadingView.setVisibility(View.GONE);
        deleteAllView.setEnabled(true);
        HPUIUtils.displayToast(this, getString(R.string.notification_list_failed_to_delete_notifications_error), false);
    }

    @Override
    public void deleteNotification(int notificationID, int userID) {
        if (presenter == null) {
            presenter = new NotificationListPresenter();
            presenter.attachView(this);
        }

        presenter.deleteNotification(notificationID, userID);

        showEmptyDataSet(notificationsListAdapter.getModels() == null ||
                notificationsListAdapter.getModels().size() == 0);

    }

    @Override
    public void markNotificationAsRead(int notificationID, int userID) {
        if (presenter == null) {
            presenter = new NotificationListPresenter();
            presenter.attachView(this);
        }

        presenter.markNotificationAsRead(notificationID, userID);
    }

    @Override
    public void onNotificationActionClicked(String extra, String notificationKey) {
        if (extra == null) {
            return;
        }

        if (notificationKey != null && notificationKey.equals(
                SubscriptionEvent.NotificationEventEnum.BOX_NEW_FOLDER.getKey())) {
            AppUtils.startApplication(this, Uri.parse(extra));
            return;
        }

        Intent intent = NotificationUtils.getNotificationIntent(this, extra, notificationKey,
                NotificationUtils.ERROR_INT_EXTRA, NotificationUtils.ERROR_INT_EXTRA, null, false);
        startActivity(intent);
        finish();
    }

    @OnClick(R.id.notifications_settings)
    public void openNotificationsSettings() {

        if (!PrintOSApplication.isGooglePlayServicesEnabled()) {
            HPUIUtils.displayToast(this, getString(R.string.google_play_services_not_installed), false);
            return;
        }

        notificationsSettingsFragment = NotificationsFragment.getNewInstance(this);

        FragmentTransaction trans = getSupportFragmentManager()
                .beginTransaction();

        trans.replace(R.id.content_layout, notificationsSettingsFragment);
        trans.commit();

        toolbarDisplayName.setText(getString(notificationsSettingsFragment.getToolbarDisplayName()));
        notificationsSettingsView.setVisibility(View.GONE);
    }

    @OnClick(R.id.try_again_text_view)
    public void onTryAgainClicked() {
        initPresenter();
    }

    @OnClick(R.id.text_view_delete_all)
    public void deleteAllNotifications() {
        loadingView.setVisibility(View.VISIBLE);
        deleteAllView.setEnabled(false);
        if (presenter != null) {
            presenter.deleteNotifications(notificationsListAdapter.getModels());
        }
    }

    @Override
    public void onBackPressed() {
        if (notificationsSettingsFragment != null) {
            removeSubMenu();
            AppseeSdk.getInstance(this).startScreen(AppseeSdk.SCREEN_NOTIFICATIONS_LIST);
            return;
        }

        super.onBackPressed();
    }

    private void removeSubMenu() {
        FragmentTransaction trans = getSupportFragmentManager()
                .beginTransaction();
        trans.remove(notificationsSettingsFragment);
        trans.commit();
        notificationsSettingsFragment = null;
        notificationsSettingsView.setVisibility(View.VISIBLE);
        toolbarDisplayName.setText(getString(R.string.notifications_list_title));
    }

    @Override
    public void closeNotificationsFragment() {
        onBackPressed();
    }

    @Override
    public boolean isBackwardCompatible() {
        return true;
    }

    @Override
    public boolean shouldValidateSession() {
        return true;
    }

    @Override
    public void onPreValidation() {
        super.onPreValidation();
        interceptingLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void onValidationCompleted() {
        super.onValidationCompleted();
        interceptingLayout.setVisibility(View.GONE);
        initPresenter();
    }

    @Override
    public void onValidationError() {
        super.onValidationError();
        interceptingLayout.setVisibility(View.GONE);
        initPresenter();
    }

    @Override
    public void onValidationUnauthorized() {
        super.onValidationUnauthorized();
        interceptingLayout.setVisibility(View.GONE);
        resetMainActivity();
    }

    @Override
    public void onValidationCompletedWithoutContextChange() {
        super.onValidationCompletedWithoutContextChange();
        resetMainActivity();
    }

    private void resetMainActivity() {

        HPLogger.d(TAG, "resetting main activity");

        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

}
