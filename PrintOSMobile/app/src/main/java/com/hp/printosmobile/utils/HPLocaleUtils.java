package com.hp.printosmobile.utils;

import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;

import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobilelib.core.communications.remote.APIException;
import com.hp.printosmobilelib.core.utils.HPDateUtils;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Created by Anwar Asbah on 5/12/2016.
 */
public class HPLocaleUtils {

    public static final String COUNTRY_CODE_LANGUAGE_SEPARATOR = "-";
    public static final String COUNTRY_CODE_LANGUAGE_OLD_SEPARATOR = "_";
    private static final String DATE_RANGE_SEPARATOR = " - ";
    private static Locale sLocale;

    //Get localized percentage value expression
    public static String getPercentageString(Context context, int value) {
        return String.format(context.getString(R.string.percentage), value);
    }

    //Get localized kilo value expression
    public static String getLocalizedValue(int rawValue) {
        return getDecimalString(rawValue, true);
    }

    public static String getKiloStringWithoutSign(int rawValue) {
        int value = Math.round(rawValue / 1000f);
        return getLocalizedValue(value);
    }


    //Get localized Mega value expression
    public static String getMegaString(Context context, int rawValue) {
        int value = Math.round(rawValue / 1000000f);
        return String.format(context.getString(R.string.mega), value);
    }

    //Get localized time expression
    public static String getLocalizedTimeString(Context context, int time, TimeUnit unit) {

        String timeString = "";

        switch (unit) {

            case DAYS:
                timeString = time == 1 ? context.getString(R.string.time_format_day)
                        : context.getString(R.string.time_format_days, time);
                break;
            case HOURS:
                long hours = time;
                long days = TimeUnit.DAYS.convert(time, TimeUnit.HOURS);
                hours -= TimeUnit.HOURS.convert(days, TimeUnit.DAYS);
                if (days > 1) {
                    timeString = hours == 1 ? context.getString(R.string.time_format_days_hour, days)
                            : context.getString(R.string.time_format_days_hours, days, hours);
                } else if (days == 1) {
                    timeString = hours == 1 ? context.getString(R.string.time_format_day_hour)
                            : context.getString(R.string.time_format_day_hours, hours);
                } else {
                    timeString = time == 1 ? context.getString(R.string.time_format_hr)
                            : context.getString(R.string.time_format_hrs, hours);
                }
                break;
            case MINUTES:
                long minutes = time;
                days = TimeUnit.DAYS.convert(minutes, TimeUnit.MINUTES);
                minutes -= TimeUnit.MINUTES.convert(days, TimeUnit.DAYS);
                hours = TimeUnit.HOURS.convert(minutes, TimeUnit.MINUTES);
                minutes -= TimeUnit.MINUTES.convert(hours, TimeUnit.HOURS);

                if (days > 1) {
                    if (hours > 0) {
                        timeString = context.getString(R.string.time_format_days_hours_minutes, days, hours, minutes);
                    } else if (minutes == 1) {
                        timeString = context.getString(R.string.time_format_days_minute, days);
                    } else {
                        timeString = context.getString(R.string.time_format_days_minutes, days, minutes);
                    }
                } else if (days == 1) {
                    if (hours > 0) {
                        timeString = context.getString(R.string.time_format_day_hours_minutes, days, hours, minutes);
                    } else if (minutes == 1) {
                        timeString = context.getString(R.string.time_format_day_minute, days);
                    } else {
                        timeString = context.getString(R.string.time_format_day_minutes, days, minutes);
                    }
                } else if (hours > 0) {
                    timeString = context.getString(R.string.time_format_min_hrs, hours, minutes);
                } else {
                    timeString = time == 1 ? context.getString(R.string.time_format_minute)
                            : context.getString(R.string.time_format_minutes, minutes);
                }
                break;
            default:
                break;
        }

        return timeString;
    }

    public static String getLocalizedTimeStringV2(Context context, int time, TimeUnit unit) {

        long sec = TimeUnit.SECONDS.convert(time, unit);
        long min = TimeUnit.MINUTES.convert(sec, TimeUnit.SECONDS);
        sec -= TimeUnit.SECONDS.convert(min, TimeUnit.MINUTES);

        String minutes = String.valueOf(min);
        minutes = minutes.length() < 2 ? "0" + minutes : minutes;
        String seconds = String.valueOf(sec);
        seconds = seconds.length() < 2 ? "0" + seconds : seconds;

        return context.getString(R.string.next_10_jobs_time_estimate_format, minutes, seconds);

    }

    public static String getDateRangeString(Context context, Date startDate, Date endDate) {

        if (context == null || startDate == null || endDate == null) {
            return "";
        }

        String dateFormat;
        if (startDate.getYear() == endDate.getYear()) {
            if (startDate.getMonth() == endDate.getMonth()) {

                if (startDate.getDate() == endDate.getDate()){
                    return HPDateUtils.formatDate(startDate, context.getString(R.string.date_range_different_year));
                }
                //Rule#1 : (Same year & month) ex: Feb 1 - 22, 2016
                dateFormat = context.getString(R.string.date_range_same_year_and_month_format);

            } else {

                //Rule#2 : (Same year & different month) ex: Feb 28 - Mar 7, 2017
                dateFormat = context.getString(R.string.date_range_same_year_and_different_month_format);
            }
        } else {
            //Rule#3: (Different year)
            dateFormat = context.getString(R.string.date_range_different_year);
        }

        return HPDateUtils.getDateRangeString(dateFormat, DATE_RANGE_SEPARATOR, startDate, endDate);
    }

    public static String getTimeRange(Context context, String startDay, int startHour, String endDay, int endHour) {
        if (startDay.equals(endDay)) {
            return context.getString(R.string.shift_from_to_same_day, startDay, startHour, endHour);
        }
        return context.getString(R.string.shift_from_to_different_day, startDay, startHour, endDay, endHour);
    }

    public static String getTimeAgo(Context context, Date lastUpdatedAt, boolean zeroAsToday) {
        if(lastUpdatedAt == null) {
            return "";
        }
        
        if (HPDateUtils.isToday(lastUpdatedAt)) {
            return HPDateUtils.getTime(context, lastUpdatedAt, null);
        }

        Calendar today = Calendar.getInstance();

        Calendar update = Calendar.getInstance();
        update.setTime(lastUpdatedAt);
        update.set(Calendar.HOUR_OF_DAY, 0);
        update.set(Calendar.MINUTE, 0);
        update.set(Calendar.SECOND, 0);
        update.set(Calendar.MILLISECOND, 0);

        int dayDiff = HPDateUtils.getDaysDiff(update, today);

        if(dayDiff == 0 && zeroAsToday) {
          return context.getString(R.string.today);
        } else if (dayDiff < HPDateUtils.DAYS_IN_WEEK) {
            return context.getString(dayDiff == 1 ? R.string.time_day_ago : R.string.time_days_ago, dayDiff);
        } else if (dayDiff / HPDateUtils.DAYS_IN_WEEK <= HPDateUtils.WEEKS_IN_MONTH) {
            int weeks = dayDiff / HPDateUtils.DAYS_IN_WEEK;
            return context.getString(weeks == 1 ? R.string.time_week_ago : R.string.time_weeks_ago, weeks);
        } else {

            int monthDifference = HPDateUtils.monthDifference(update, today);
            if (monthDifference < HPDateUtils.MONTHS_IN_YEAR) {
                return context.getString(monthDifference == 1 ? R.string.time_month_ago : R.string.time_months_ago, monthDifference);
            }

            int years = monthDifference / HPDateUtils.MONTHS_IN_YEAR;
            return context.getString(years == 1 ? R.string.time_year_ago : R.string.time_years_ago, years);
        }
    }

    public static String getLocalizedValueString(float value, float maxValue) {
        //log to the base 1000
        int power = (int) (Math.log10(maxValue) / 3);

        if(power > 0) {
            return getDecimalString((float)(value / Math.pow(1000, power)), true);
        }
        return getDecimalString(value);
    }

    public static String getRoundMeasure(double value, int decimalPlace) {
        BigDecimal bd = new BigDecimal(Double.toString(value));
        bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
        return getDecimalString(bd.floatValue(), true);
    }

    public static void updateLanguage(Context context, String localeString) {
        String languageCode = HPStringUtils.getStringBeforeRegex(localeString, COUNTRY_CODE_LANGUAGE_SEPARATOR);
        Locale locale = new Locale(languageCode);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
    }

    /**
     * Configure the application language.
     *
     * @param context - context from which this method called.
     * @param locale  - the default locale of the device.
     */
    public static void configureAppLanguage(Application context, Locale locale) {

        String selectedLanguage = PrintOSPreferences.getInstance(context).getLanguage("");

        //Since we've changed the languages in the resources file "eg: en_EN -> en-EU".
        //In order to support the change for old users, we will need to replace the "_" by "-" and save the locale again.
        if (selectedLanguage.contains(COUNTRY_CODE_LANGUAGE_OLD_SEPARATOR)) {
            selectedLanguage = selectedLanguage.replace(COUNTRY_CODE_LANGUAGE_OLD_SEPARATOR, COUNTRY_CODE_LANGUAGE_SEPARATOR);
            PrintOSPreferences.getInstance(context).setLanguage(selectedLanguage);
        }

        if (!selectedLanguage.equals("")) {

            String languageCode = HPStringUtils.getStringBeforeRegex(selectedLanguage, COUNTRY_CODE_LANGUAGE_SEPARATOR);
            HPLocaleUtils.setLocale(new Locale(languageCode));
            HPLocaleUtils.updateConfig(context, context.getResources().getConfiguration());

        } else {

            List<String> locales = Arrays.asList(context.getResources().getStringArray(R.array.language_key));

            //Loop through the supported locales and compare the value with the device locale.
            //in order to update the application locale.
            for (String localeString : locales) {
                if (localeString.contains(locale.getLanguage())) {
                    PrintOSPreferences.getInstance(context).setLanguage(localeString);
                    Locale.setDefault(new Locale(locale.getLanguage()));
                    return;
                }
            }

            //Get the default language in order to assign it as a language for the app.
            String defaultLanguage = context.getString(R.string.default_language);
            //Get the language code, the string before (-).
            String languageCode = HPStringUtils.getStringBeforeRegex(defaultLanguage, COUNTRY_CODE_LANGUAGE_SEPARATOR);

            //Save the default language as a selected language.
            PrintOSPreferences.getInstance(context).setLanguage(defaultLanguage);
            //Overrides the default locale.
            Locale.setDefault(new Locale(languageCode));
            HPLocaleUtils.setLocale(new Locale(languageCode));
            HPLocaleUtils.updateConfig(context, context.getResources().getConfiguration());
        }
    }

    public static void setLocale(Locale locale) {
        sLocale = locale;
        if (sLocale != null) {
            Locale.setDefault(sLocale);
        }
    }

    public static void updateConfig(Application app, Configuration configuration) {
        //Wrapping the configuration to avoid Activity endless loop
        Configuration config = new Configuration(configuration);
        config.locale = sLocale;
        Resources res = app.getBaseContext().getResources();
        res.updateConfiguration(config, res.getDisplayMetrics());
    }

    public static String getDecimalString(float number, boolean withGrouping, int digits){
        Locale fmtLocale = Locale.getDefault();
        NumberFormat formatter = NumberFormat.getNumberInstance(fmtLocale);
        formatter.setMaximumFractionDigits(digits);
        formatter.setGroupingUsed(withGrouping);
        //formatter.setGroupingUsed(false);

        return formatter.format(number);
    }

    public static String getDecimalString(float number, boolean withGrouping) {
        return getDecimalString(number, withGrouping, 1);
    }


    public static String getDecimalString(float number) {
        return getDecimalString(number, false);
    }

    public static String getSimpleErrorMsg(Context context, APIException exception) {

        switch (exception.getKind()) {
            case NETWORK:
                return context.getString(R.string.error_network_error);
            case SERVICE_TIME_OUT:
                return context.getString(R.string.error_service_time_out);
            case INTERNAL_SERVER_ERROR:
                return context.getString(R.string.error_internal_server_error);
            case URL_NOT_FOUND_ERROR:
                return context.getString(R.string.url_not_found_error);
            case FILE_NOT_FOUND:
                return context.getString(R.string.file_not_found);
            case UNAUTHORIZED:
                return context.getString(R.string.unauthorized_error);
            default:
                return context.getString(R.string.default_error);
        }
    }
}
