package com.hp.printosmobile.presentation.modules.personaladvisor.presenter;

import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.data.remote.ApiServicesProvider;
import com.hp.printosmobile.data.remote.models.AdviceData;
import com.hp.printosmobile.data.remote.models.DeviceData;
import com.hp.printosmobile.data.remote.services.PersonalAdvisorService;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitViewModel;
import com.hp.printosmobile.presentation.modules.personaladvisor.AdviceType;
import com.hp.printosmobile.presentation.modules.personaladvisor.AdviceViewModel;
import com.hp.printosmobile.presentation.modules.personaladvisor.PersonalAdvisorViewModel;
import com.hp.printosmobile.utils.HPUIUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Response;
import rx.Observable;
import rx.functions.Func1;

/**
 * Created by Anwar Asbah on 5/10/17.
 */
public class PersonalAdvisorDataManager {

    public static Observable<PersonalAdvisorViewModel> getAdvices(String language, final BusinessUnitViewModel businessUnitViewModel) {


        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
        PersonalAdvisorService personalAdvisorService = serviceProvider.getPersonalAdvisorService();
        List<String> serialNumbers = businessUnitViewModel.getFiltersViewModel().getSiteSerialNumbers();

        return personalAdvisorService.getAdvices(language, serialNumbers, true).map(new Func1<Response<List<AdviceData>>, PersonalAdvisorViewModel>() {
            @Override
            public PersonalAdvisorViewModel call(Response<List<AdviceData>> listResponse) {

                if (listResponse.isSuccessful()) {
                    return mapAdvicesData(listResponse.body(), businessUnitViewModel.getDevices());
                }
                return null;
            }
        });
    }

    private static PersonalAdvisorViewModel mapAdvicesData(List<AdviceData> advicesDataList, Map<String, DeviceData> devices) {

        PersonalAdvisorViewModel personalAdvisorViewModel = new PersonalAdvisorViewModel();
        List<AdviceViewModel> advicesList = new ArrayList<>();

        int index = 0;

        for (AdviceData adviceData : advicesDataList) {

            AdviceViewModel adviceViewModel = new AdviceViewModel();
            adviceViewModel.setIsReadMoreClicked(false);
            adviceViewModel.setTitle(adviceData.getAdviceTitle());
            adviceViewModel.setLiked(adviceData.getLiked());
            adviceViewModel.setSuppressed(adviceData.getSuppressed());
            adviceViewModel.setNew(adviceData.getIsNew());
            adviceViewModel.setAdviseId(adviceData.getAdviceID());
            adviceViewModel.setContent(adviceData.getAdviceText());
            adviceViewModel.setAdviceType(AdviceType.from(adviceData.getIconName()));
            adviceViewModel.setImageResourceId(HPUIUtils.getDrawableResourceId(String.format("advice_bg_%d", (index % 21) + 1)));

            adviceViewModel.setIndex(index++);

            if (devices.containsKey(adviceData.getDevice())) {
                DeviceData device = devices.get(adviceData.getDevice());
                adviceViewModel.setDeviceName(device.getDeviceName());
                adviceViewModel.setDeviceSerialNumber(device.getSerialNumber());
            } else {
                adviceViewModel.setDeviceName("");
            }

            advicesList.add(adviceViewModel);

        }

        personalAdvisorViewModel.setAdvices(advicesList);

        return personalAdvisorViewModel;
    }

    public static Observable<ResponseBody> likeAdvice(int adviceId, String device, boolean isLiked) {
        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
        PersonalAdvisorService personalAdvisorService = serviceProvider.getPersonalAdvisorService();
        return personalAdvisorService.likeAdvice(String.valueOf(adviceId), device, isLiked).asObservable();
    }

    public static Observable<ResponseBody> suppressAdvice(int adviceId, String device) {
        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
        PersonalAdvisorService personalAdvisorService = serviceProvider.getPersonalAdvisorService();
        return personalAdvisorService.suppressAdvice(String.valueOf(adviceId), device, true).asObservable();
    }

    public static Observable<ResponseBody> unhideAdvices(List<String> serialNumbers) {
        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
        PersonalAdvisorService personalAdvisorService = serviceProvider.getPersonalAdvisorService();
        return personalAdvisorService.unhideAllAdvices(serialNumbers, false).asObservable();
    }

}