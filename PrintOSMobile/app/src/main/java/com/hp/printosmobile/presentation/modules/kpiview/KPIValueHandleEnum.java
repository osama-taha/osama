package com.hp.printosmobile.presentation.modules.kpiview;

/**
 * An enum represents the kpi value handle.
 * <p/>
 * Created by Anwar Asbah on 5/12/16.
 */
public enum KPIValueHandleEnum {

    EMPTY(""), PERCENT("Percents"), KILO("Kilo"), MEGA("Mega"), SQM("Sqm"), HOURS("Hours"), TECHNICAL_ISSUES("Technical_issues");

    private String valueHandle;

    KPIValueHandleEnum(String valueHandle) {
        this.valueHandle = valueHandle;
    }

    public String getValueHandle() {
        return valueHandle;
    }

    public static KPIValueHandleEnum from(String valueHandle) {
        for (KPIValueHandleEnum kpiState : KPIValueHandleEnum.values()) {
            if (kpiState.getValueHandle().equals(valueHandle))
                return kpiState;
        }
        return KPIValueHandleEnum.EMPTY;
    }

}
