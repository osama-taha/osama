package com.hp.printosmobile.presentation.modules.shared;

import android.net.Uri;

import com.hp.printosmobile.presentation.modules.home.Panel;

/**
 * Created by Osama Taha on 2/26/17.
 */
public interface PanelShareCallbacks {

    void onShareButtonClicked(Panel panel);

    void onScreenshotCreated(Panel panel, Uri screenshotUri);

}
