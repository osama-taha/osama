package com.hp.printosmobile.presentation.modules.drawer;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitViewModel;

import java.util.Collections;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Anwar Asbah on 6/5/2016.
 */
public class DivisionSwitchAdapter extends RecyclerView.Adapter<DivisionSwitchAdapter.DivisionHolder> {

    private List<BusinessUnitViewModel> businessUnits;

    private int selected = -1;

    public DivisionSwitchAdapter(List<BusinessUnitViewModel> businessUnits, Context context) {
        this.businessUnits = businessUnits;

        Collections.sort(this.businessUnits, BusinessUnitViewModel.businessUnitViewModelNameComparator);

        BusinessUnitEnum businessUnitEnum = PrintOSPreferences.getInstance(context).getSelectedBusinessUnit();
        if (businessUnits != null) {
            for (int i = 0; i < businessUnits.size(); i++) {
                if (businessUnits.get(i).getBusinessUnit() == businessUnitEnum) {
                    selected = i;
                    break;
                }
            }
        }
    }

    @Override
    public DivisionHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.business_unit_item, parent, false);
        itemView.setBackgroundResource(R.drawable.list_item_bg_color);
        return new DivisionHolder(itemView);
    }

    @Override
    public void onBindViewHolder(DivisionHolder holder, int position) {
        holder.position = position;

        boolean isSelected = selected == position;
        String businessUnit = businessUnits.get(position).getBusinessUnit()
                .getBusinessUnitDisplayName();

        holder.selectedDivisionText.setText(businessUnit);
        holder.itemView.setSelected(isSelected);
        holder.selectedDivisionText.setSelected(isSelected);
    }

    @Override
    public int getItemCount() {
        return businessUnits == null ? 0 : businessUnits.size();
    }

    public BusinessUnitViewModel getSelection() {
        if (selected < 0 || selected >= businessUnits.size()) return null;
        return businessUnits.get(selected);
    }

    public class DivisionHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @Bind(R.id.business_unit_name)
        TextView selectedDivisionText;

        private int position;

        public DivisionHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            selected = position;
            notifyDataSetChanged();
        }
    }
}