package com.hp.printosmobile.presentation.modules.personaladvisor;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.AppseeSdk;
import com.hp.printosmobile.IntercomSdk;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.modules.home.Panel;
import com.hp.printosmobile.presentation.modules.personaladvisor.presenter.PersonalAdvisorFactory;
import com.hp.printosmobile.utils.FileUtils;
import com.hp.printosmobile.utils.HPUIUtils;
import com.hp.printosmobilelib.core.logging.HPLogger;
import com.hp.printosmobilelib.ui.widgets.HPViewPager;
import com.viewpagerindicator.CirclePageIndicator;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Anwar Asbah on 1/8/2017.
 */

public class PersonalAdvisorDialog extends DialogFragment implements ViewPager.OnPageChangeListener{

    private static final String ADVICES_ARG = "advices_arg";
    private static final String ADVICE_SCREEN_SHOT_FILE_NAME = "advice_screenshot";
    private static final long VISIBILITY_DELAY = 100;
    private static final long CAPTURE_SCREENSHOT_DELAY_MILLI_SECONDS = 200;
    private static final String TAG = PersonalAdvisorDialog.class.getSimpleName();

    @Bind(R.id.image_advice_icon)
    ImageView iconImageView;
    @Bind(R.id.text_advice_title)
    TextView titleTextView;
    @Bind(R.id.advise_pager)
    HPViewPager advisesPager;
    @Bind(R.id.advice_pager_indicator)
    CirclePageIndicator circlePageIndicator;
    @Bind(R.id.button_view_all)
    View viewAllButton;
    @Bind(R.id.dialog_layout)
    View dialogLayout;
    @Bind(R.id.advice_title_view)
    View adviceTitleView;
    @Bind(R.id.like_button)
    ImageView likeButton;
    @Bind(R.id.share_button)
    ImageView shareButton;
    @Bind(R.id.screen_shot_layout)
    View screenShotView;
    @Bind(R.id.close_button)
    View closeButton;

    int currentAdvicePosition = -1;

    private PersonalAdvisorAdapter advisorAdapter;
    private PersonalAdvisorViewModel model;

    private PersonalAdvisorDialogCallback callback;

    public static PersonalAdvisorDialog newInstance(PersonalAdvisorViewModel model,
                                                    PersonalAdvisorDialogCallback callback) {
        PersonalAdvisorDialog dialog = new PersonalAdvisorDialog();
        dialog.callback = callback;
        Bundle bundle = new Bundle();
        bundle.putSerializable(ADVICES_ARG, model);
        dialog.setArguments(bundle);
        return dialog;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey(ADVICES_ARG)) {
            model = (PersonalAdvisorViewModel) bundle.getSerializable(ADVICES_ARG);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dialog_personal_advisor, container, false);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        init();
        AppseeSdk.getInstance(getActivity()).startScreen(AppseeSdk.SCREEN_NAME_PERSONAL_ADVISOR_POPUP);
    }

    private void init() {

        advisesPager.setVisibility(View.INVISIBLE);

        advisorAdapter = new PersonalAdvisorAdapter(getContext(), model, null, false);
        advisesPager.setAdapter(advisorAdapter);
        circlePageIndicator.setViewPager(advisesPager);
        if (advisorAdapter.getCount() == 0) {
            circlePageIndicator.setCurrentItem(-1);
        }

        advisorAdapter.notifyDataSetChanged();

        circlePageIndicator.setVisibility(advisorAdapter.getCount() > 1 ? View.VISIBLE : View.GONE);

        advisesPager.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                advisesPager.getViewTreeObserver().removeOnGlobalLayoutListener(this);

                int available = dialogLayout.getMeasuredHeight()
                        - dialogLayout.getPaddingTop()
                        - dialogLayout.getPaddingBottom()
                        - circlePageIndicator.getMeasuredHeight()
                        - viewAllButton.getMeasuredHeight()
                        - adviceTitleView.getMeasuredHeight();
                advisesPager.setMaxHeight(available);

                advisesPager.addOnPageChangeListener(PersonalAdvisorDialog.this);

                if (advisesPager.getAdapter() != null && advisesPager.getAdapter().getCount() > 0) {
                    titleTextView.setText(advisorAdapter.getAdviceTitle(0));
                    iconImageView.setImageDrawable(advisorAdapter.getAdviceIcon(0));
                    onPageSelected(0);
                    currentAdvicePosition = 0;
                }

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        final View fistAdviceView = advisesPager.findViewWithTag(0);
                        advisesPager.measureCurrentView(fistAdviceView);
                                       advisesPager.setVisibility(View.VISIBLE);

                    }
                }, VISIBILITY_DELAY);
            }
        });

        dialogLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                close();
            }
        });
    }

    @OnClick(R.id.close_button)
    public void close() {
        dismiss();
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }

    @OnClick(R.id.button_view_all)
    public void goToPersonalAdvisor (){

        HPLogger.d(TAG, "Go To Personal Advisor Clicked");

        if(callback != null && currentAdvicePosition >= 0){
            callback.goToPersonalAdvisor(advisorAdapter.getAdvice(currentAdvicePosition));
            dismiss();
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        if (positionOffset == 0) {
            advisesPager.measureCurrentView(advisesPager.findViewWithTag(position));
        }
    }

    @Override
    public void onPageSelected(final int position) {
        titleTextView.setText(advisorAdapter.getAdviceTitle(position));
        iconImageView.setImageDrawable(advisorAdapter.getAdviceIcon(position));
        likeButton.setImageDrawable(ResourcesCompat.getDrawable(getResources(),
                advisorAdapter.isLiked(position) ? R.drawable.like_selected : R.drawable.like,
                null));
        currentAdvicePosition = position;
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }

    @Override
    public void dismiss() {
        super.dismiss();
        if(callback != null){
            callback.onAdvicePopupDismiss();
        }
    }

    @OnClick(R.id.suppress_button)
    public void onSuppressClicked() {
        AdviceViewModel adviceViewModel = advisorAdapter.getAdvice(currentAdvicePosition);

        HPLogger.d(TAG, "onSuppress Clicked adviceID: " + (adviceViewModel == null ? "null "
                : adviceViewModel.getAdviseId()));

        if(adviceViewModel != null) {
            PersonalAdvisorFactory.getInstance().suppressAdvice(adviceViewModel);
            advisorAdapter.notifyDataSetChanged();
            if(advisorAdapter.getCount() == 0){
                dismiss();
                return;
            }

            HPUIUtils.setVisibility(advisorAdapter.getCount() > 1, circlePageIndicator);
        }
    }

    @OnClick(R.id.like_button)
    public void onLikeClicked() {
        AdviceViewModel adviceViewModel = advisorAdapter.getAdvice(currentAdvicePosition);

        HPLogger.d(TAG, "onShare Clicked adviceID: " + (adviceViewModel == null ? "null "
                : adviceViewModel.getAdviseId()));

        if(adviceViewModel != null) {
            PersonalAdvisorFactory.getInstance().likeAdvice(adviceViewModel, !adviceViewModel.getLiked());
            likeButton.setImageDrawable(ResourcesCompat.getDrawable(getResources(),
                    adviceViewModel.getLiked() ? R.drawable.like_selected : R.drawable.like,
                    null));
        }

    }

    @OnClick(R.id.share_button)
    public void share() {
        HPLogger.d(TAG, "onShare Clicked");
        if (callback != null && advisorAdapter != null && advisorAdapter.getCount() > 0) {

            lockShareButton(true);

            //Execute after few milliseconds to make sure that current advice's text is expanded.
            new Handler().postDelayed(
                    new Runnable() {
                        @Override
                        public void run() {
                            HPUIUtils.setVisibility(false, closeButton);
                            Bitmap adviceViewBitmap = FileUtils.getScreenShot(screenShotView);
                            HPUIUtils.setVisibility(false, closeButton);

                            Uri storageUri = FileUtils.store(adviceViewBitmap, ADVICE_SCREEN_SHOT_FILE_NAME);
                            callback.onScreenshotCreated(Panel.PERSONAL_ADVISOR, storageUri);
                        }
                    }, CAPTURE_SCREENSHOT_DELAY_MILLI_SECONDS);
        }
    }

    public void lockShareButton(boolean lock) {
        shareButton.setEnabled(!lock);
        shareButton.setClickable(!lock);
    }

    public interface PersonalAdvisorDialogCallback {
        void goToPersonalAdvisor(AdviceViewModel adviceViewModel);

        void onAdvicePopupDismiss();

        void onScreenshotCreated(Panel personalAdvisor, Uri storageUri);
    }
}
