package com.hp.printosmobile.data.remote.services;


import com.hp.printosmobile.data.remote.ApiConstants;
import com.hp.printosmobile.data.remote.models.ResetPasswordBody;

import okhttp3.ResponseBody;
import retrofit2.http.Body;
import retrofit2.http.PUT;
import rx.Observable;

/**
 * Retrofit REST interface definition for ResetPassword Endpoint
 * of {@link ResetPasswordService#reset(ResetPasswordBody)} endpoint.
 * Created by Anwar Asbah on 7/26/16.
 */
public interface ResetPasswordService {

    @PUT(ApiConstants.RESET_PASSWORD_API)
    Observable<retrofit2.Response<ResponseBody>> reset(@Body ResetPasswordBody resetPasswordBody);
}
