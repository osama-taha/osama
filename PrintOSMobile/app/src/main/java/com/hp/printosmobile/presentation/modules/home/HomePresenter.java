package com.hp.printosmobile.presentation.modules.home;

import android.content.Context;

import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.presentation.Presenter;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitViewModel;
import com.hp.printosmobile.presentation.modules.personaladvisor.PersonalAdvisorPanel;
import com.hp.printosmobile.presentation.modules.personaladvisor.PersonalAdvisorViewModel;
import com.hp.printosmobile.presentation.modules.printersnapshot.PrinterSnapshotPanel;
import com.hp.printosmobile.presentation.modules.productionsnapshot.indigo.IndigoProductionPanel;
import com.hp.printosmobile.presentation.modules.servicecallpanel.ServiceCallPanel;
import com.hp.printosmobile.presentation.modules.servicecallpanel.ServiceCallViewModel;
import com.hp.printosmobile.presentation.modules.shared.DeviceViewModel;
import com.hp.printosmobile.presentation.modules.today.TodayPanel;
import com.hp.printosmobile.presentation.modules.todayhistogram.TodayHistogramViewModel;
import com.hp.printosmobile.presentation.modules.week.WeekCollectionViewModel;
import com.hp.printosmobile.presentation.modules.week.WeekPanel;
import com.hp.printosmobilelib.core.communications.remote.APIException;
import com.hp.printosmobilelib.core.logging.HPLogger;

import java.util.List;

import okhttp3.ResponseBody;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Osama Taha on 21/05/2016.
 */
public class HomePresenter extends Presenter<HomeView> {

    private static final String TAG = HomePresenter.class.getSimpleName();

    private BusinessUnitViewModel selectedBusinessUnit;
    private boolean isShiftSupport;

    private boolean isLoadingTodayData = false;


    public void getActiveShifts() {

        Subscription subscription = HomeDataManager.getActiveShifts(selectedBusinessUnit).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<ActiveShiftsViewModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        HPLogger.d(TAG, "Error while fetching the today data " + e);
                    }

                    @Override
                    public void onNext(ActiveShiftsViewModel activeShiftsViewModel) {
                        mView.updateTodayPanelWithActiveShifts(activeShiftsViewModel);
                    }
                });
        addSubscriber(subscription);
    }

    public void getTodayData() {

        Observable<PrintBeatDashboardViewModel> observable = null;

        if (selectedBusinessUnit.getBusinessUnit() == BusinessUnitEnum.LATEX_PRINTER) {
            observable = HomeDataManager.getLatexDashboardViewModel(selectedBusinessUnit, false, isShiftSupport);
        } else {
            observable = HomeDataManager.getPrintBeatViewModel(selectedBusinessUnit, false, isShiftSupport);
        }

        Subscription subscription = observable
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<PrintBeatDashboardViewModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        HPLogger.d(TAG, "Error while fetching the today data " + e);
                        mView.updateTodayPanel(null);
                        if (BusinessUnitEnum.INDIGO_PRESS == selectedBusinessUnit.getBusinessUnit()) {
                            mView.updateProductionSnapshot(null);
                        } else if (BusinessUnitEnum.LATEX_PRINTER == selectedBusinessUnit.getBusinessUnit()) {
                            mView.updateLatexProductionSnapshot(null);
                        }
                    }

                    @Override
                    public void onNext(PrintBeatDashboardViewModel printBeatDashboardViewModel) {

                        mView.updateTodayPanel(printBeatDashboardViewModel.getTodayViewModel());

                        if (BusinessUnitEnum.INDIGO_PRESS == selectedBusinessUnit.getBusinessUnit()) {
                            mView.updateProductionSnapshot(printBeatDashboardViewModel.getProductionViewModel());
                        } else if (BusinessUnitEnum.LATEX_PRINTER == selectedBusinessUnit.getBusinessUnit()) {
                            mView.updateLatexProductionSnapshot(printBeatDashboardViewModel.getProductionViewModel());
                        }
                    }
                });
        addSubscriber(subscription);
    }

    public synchronized void getActualVsTargetData(Context context) {

        if(isLoadingTodayData) {
           return;
        }

        isLoadingTodayData = true;

        Subscription subscription = HomeDataManager.getActualVsTarget(context, selectedBusinessUnit, isShiftSupport)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<TodayHistogramViewModel>() {
                    @Override
                    public void onCompleted() {
                        isLoadingTodayData = false;
                    }

                    @Override
                    public void onError(Throwable e) {
                        HPLogger.d(TAG, "Error while fetching actual vs target data " + e);
                        mView.updateHistogram(null);
                    }

                    @Override
                    public void onNext(TodayHistogramViewModel actualVsTargetViewModel) {
                        mView.updateHistogram(actualVsTargetViewModel);
                    }
                });
        addSubscriber(subscription);
    }

    public void getWeekData(Context context) {

        Subscription subscription = HomeDataManager.getMultipleWeekData(context, selectedBusinessUnit)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<WeekCollectionViewModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        HPLogger.d(TAG, "Error while fetching the week data " + e);
                        mView.updateWeekPanel(null);
                    }

                    @Override
                    public void onNext(WeekCollectionViewModel weekCollectionViewModel) {
                        if (mView != null) {
                            mView.updateWeekPanel(weekCollectionViewModel);
                        }
                    }
                });
        addSubscriber(subscription);

    }

    public void getServiceCalls(Context context) {
        Subscription subscription = HomeDataManager.getServiceCallData(selectedBusinessUnit)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<ServiceCallViewModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        HPLogger.d(TAG, "Error while fetching service call data " + e);
                        mView.updateServiceCallPanel(null);
                    }

                    @Override
                    public void onNext(ServiceCallViewModel serviceCallViewModel) {
                        if (mView != null) {
                            mView.updateServiceCallPanel(serviceCallViewModel);
                        }
                    }
                });
        addSubscriber(subscription);
    }

    public void getPrintersData(Context context) {

        Subscription subscription = HomeDataManager.getPWPDevices(selectedBusinessUnit, false).observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new Subscriber<List<DeviceViewModel>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        HPLogger.e(TAG, "Error while fetching devices data");
                        mView.updatePrintersPanel(null);
                    }

                    @Override
                    public void onNext(List<DeviceViewModel> deviceViewModels) {
                        mView.updatePrintersPanel(deviceViewModels);
                    }
                });
        addSubscriber(subscription);
    }

    private void onRequestError(Throwable e, String... tags) {

        APIException exception = APIException.create(APIException.Kind.UNEXPECTED);
        if (e instanceof APIException) {
            exception = (APIException) e;
        }

        mView.onError(exception, tags);
    }

    public BusinessUnitViewModel getSelectedBusinessUnit() {
        return selectedBusinessUnit;
    }

    public void setSelectedBusinessUnit(BusinessUnitViewModel selectedBusinessUnit) {
        this.selectedBusinessUnit = selectedBusinessUnit;
    }

    public boolean isShiftSupport() {
        return isShiftSupport;
    }

    public void setShiftSupport(boolean shiftSupport) {
        isShiftSupport = shiftSupport;
    }

    public void refresh() {
        stopSubscribers();
    }

    @Override
    public void detachView() {
        stopSubscribers();
        mView = null;
    }

}