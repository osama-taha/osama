package com.hp.printosmobile.data.remote.models;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

/**
 * //TODO: Add the shared properties between DeviceData model and other business units.
 * Created by Osama Taha on 5/31/16.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BaseDeviceData implements Serializable{
}
