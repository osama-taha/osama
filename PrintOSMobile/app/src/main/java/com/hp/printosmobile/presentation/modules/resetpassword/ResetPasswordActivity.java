package com.hp.printosmobile.presentation.modules.resetpassword;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.IntercomSdk;
import com.hp.printosmobile.R;
import com.hp.printosmobile.utils.HPUIUtils;
import com.hp.printosmobilelib.core.communications.remote.APIException;
import com.hp.printosmobilelib.ui.common.HPActivity;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Forgot Password Screen activity
 *
 * @author Anwar Asbah
 */
public class ResetPasswordActivity extends HPActivity implements ResetPasswordView {

    public static final String TAG = ResetPasswordActivity.class.getSimpleName();

    @Bind(R.id.content)
    View layout;
    @Bind(R.id.edit_text_username)
    EditText userNameEditText;
    @Bind(R.id.button_cancel)
    View cancelButton;
    @Bind(R.id.button_reset)
    View resetButton;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.toolbar_display_title)
    TextView toolbarDisplayName;

    private ResetPasswordPresenter presenter;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        presenter = new ResetPasswordPresenter();
        presenter.attachView(this);

        IntercomSdk.getInstance(this).logEvent(IntercomSdk.PBM_VIEW_FORGOT_EVENT);

        init();
    }

    private void init() {
        layout.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    hideSoftKeyboard();
                }
            }
        });
        userNameEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    onButtonResetClicked();
                    return true;
                }
                return false;
            }
        });

        toolbarDisplayName.setText(getString(R.string.activity_forgot_password_title));
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(ResourcesCompat.getDrawable(getResources(), R.drawable.back_button, null));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void hideSoftKeyboard() {
        HPUIUtils.hideSoftKeyboard(this, userNameEditText);
    }


    @Override
    protected int getLayoutResource() {
        return R.layout.activity_reset_password;
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

    @OnClick(R.id.button_cancel)
    public void onButtonCancelClicked() {
        finish();
    }

    @OnClick(R.id.button_reset)
    public void onButtonResetClicked() {
        String username = userNameEditText.getText().toString();
        if (username.isEmpty()) {
            return;
        }

        Analytics.sendEvent(Analytics.MOBILE_PRINT_BEAT_CATEGORY, Analytics.FORGOT_PASSWORD_REQUEST_SENT_ACTION, username);

        performReset(username);
    }

    private void performReset(String username) {
        presenter.reset(username);
    }

    @Override
    public void showLoading() {
        progressDialog = ProgressDialog.show(this, getString(R.string.activity_forgot_password_progress_dialog_message),
                "", true);
    }

    @Override
    public void hideLoading() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    @Override
    public void onResetPasswordSuccess() {
        HPUIUtils.displayToast(this, Gravity.CENTER, getString(R.string.activity_forgot_password_success), false);
        finish();
    }

    @Override
    public void onResetPasswordError(APIException error) {
        HPUIUtils.displayToastException(this, error, TAG, false);
    }

    @Override
    public void setViewEnabled(boolean isEnabled) {
        userNameEditText.setEnabled(isEnabled);
        cancelButton.setEnabled(isEnabled);
        resetButton.setEnabled(isEnabled);
    }

    @Override
    public void onError(APIException exception, String tag) {
        HPUIUtils.displayToastException(this, exception, tag, false);
    }
}
