package com.hp.printosmobile.data.remote.models;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Osama Taha on 5/17/17.
 */

public class RealtimeTargetManyDataV2 {

    @JsonProperty("data")
    private List<TargetData> data;
    @JsonProperty("unitSystem")
    private PreferencesData.UnitSystem unitSystem;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("data")
    public List<TargetData> getData() {
        return data;
    }

    @JsonProperty("data")
    public void setData(List<TargetData> data) {
        this.data = data;
    }

    @JsonProperty("unitSystem")
    public PreferencesData.UnitSystem getUnitSystem() {
        return unitSystem;
    }

    @JsonProperty("unitSystem")
    public void setUnitSystem(PreferencesData.UnitSystem unitSystem) {
        this.unitSystem = unitSystem;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
}
