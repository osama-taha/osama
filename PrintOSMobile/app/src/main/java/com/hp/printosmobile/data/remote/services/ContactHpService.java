package com.hp.printosmobile.data.remote.services;

import com.hp.printosmobile.data.remote.ApiConstants;
import com.hp.printosmobilelib.core.communications.remote.models.UserData;

import java.util.Map;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PartMap;
import rx.Observable;

/**
 * Created by anwar asbah on 10/12/2016.
 */
public interface ContactHpService {

    @Multipart
    @POST(ApiConstants.CONTACT_HP_URL)
    Observable<Response<ResponseBody>> contactHp(
            @PartMap() Map<String, RequestBody> partMap);

    @GET(ApiConstants.CONTACT_HP_USER_URL)
    Observable<Response<UserData.User>> getUser();
}
