package com.hp.printosmobile.presentation.indigowidget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.support.v4.content.res.ResourcesCompat;
import android.text.TextPaint;
import android.text.TextUtils;

import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.modules.today.TodayViewModel;
import com.hp.printosmobile.utils.HPScoreUtils;
import com.hp.printosmobile.utils.HPUIUtils;
import com.hp.printosmobilelib.ui.widgets.HPProgressArc;

/**
 * Created by Osama Taha on 1/16/17.
 */

public class WidgetUtils {

    public static Bitmap getTodayArcBitmap(TodayViewModel todayViewModel, Context context) {

        int width = (int) context.getResources().getDimension(R.dimen.widget_today_progress_arc_size);
        int height = width;
        int arcWidth = (int) context.getResources().getDimension(R.dimen.widget_today_progress_arc_arc_width);
        int backgroundWidth = (int) context.getResources().getDimension(R.dimen.widget_today_progress_arc_bg_width);
        int progressWidth = (int) context.getResources().getDimension(R.dimen.widget_today_progress_arc_progress_width);

        HPProgressArc progressArc = new HPProgressArc(context, width, height);
        progressArc.setBackgroundWidth(backgroundWidth);
        progressArc.setProgressWidth(progressWidth);
        progressArc.setArcWidth(arcWidth);

        int progressColor = HPScoreUtils.getProgressColor(context, todayViewModel.getPrintVolumeValue(),
                todayViewModel.getPrintVolumeIntraDailyTargetValue(), false);
        int trackColor = HPScoreUtils.getProgressColor(context, todayViewModel.getPrintVolumeValue(),
                todayViewModel.getPrintVolumeIntraDailyTargetValue(), true);

        progressArc.setTrackColor(trackColor);

        double percentageFill = todayViewModel.getPrintVolumeShiftTargetValue() == 0 ?
                (todayViewModel.getPrintVolumeValue() == 0 ? 0 : 1)
                : todayViewModel.getPrintVolumeValue() / todayViewModel.getPrintVolumeShiftTargetValue();

        percentageFill = Math.max(0, percentageFill);

        double percentageTarget = todayViewModel.getPrintVolumeShiftTargetValue() == 0 ? 0 :
                todayViewModel.getPrintVolumeIntraDailyTargetValue() /
                        todayViewModel.getPrintVolumeShiftTargetValue();

        percentageTarget = Math.max(0, percentageTarget);

        progressArc.setThumb((int) (percentageTarget * 100) == 0 ? null :
                ResourcesCompat.getDrawable(context.getResources(), R.drawable.circle, null));
        progressArc.setTarget((int) (percentageTarget * 100));
        progressArc.setProgress((int) (percentageFill * 100));

        progressArc.measure(width, height);
        progressArc.layout(0, 0, width, height);
        progressArc.setProgressColor(progressColor);

        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        progressArc.draw(new Canvas(bitmap));

        return bitmap;

    }

    public static Bitmap getTextBitmap(Context context, String text, Typeface typeface, int color, int fontSizeDP) {

        int textSizePixels = HPUIUtils.dpToPx(context, fontSizeDP);

        TextPaint textPaint = new TextPaint();
        textPaint.setTypeface(typeface);
        textPaint.setTextSize(textSizePixels);
        textPaint.setAntiAlias(true);
        textPaint.setStyle(Paint.Style.FILL);
        textPaint.setSubpixelText(true);
        textPaint.setColor(color);
        textPaint.setTextAlign(Paint.Align.LEFT);
        Bitmap bitmap = Bitmap.createBitmap((int) textPaint.measureText(text), textSizePixels + 6, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        CharSequence txt = TextUtils.ellipsize(text, textPaint, canvas.getWidth(), TextUtils.TruncateAt.END);

        int yPos = (int) ((canvas.getHeight() / 2) - ((textPaint.descent() + textPaint.ascent()) / 2));

        canvas.drawText(txt.toString(), 0, yPos, textPaint);

        return bitmap;
    }
}
