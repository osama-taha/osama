package com.hp.printosmobile.presentation.modules.servicecallpanel;

import android.content.Context;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.modules.servicecallpanel.ServiceCallViewModel.CallViewModel;
import com.hp.printosmobile.presentation.modules.servicecallpanel.ServiceCallViewModel.ServiceCallStateEnum;
import com.hp.printosmobilelib.core.utils.HPDateUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Anwar Asbah on 3/12/2017.
 */
public class ServiceCallsAdapter extends RecyclerView.Adapter<ServiceCallsAdapter.CallViewHolder> {

    private static final long ANIMATION_DURATION = 300;
    private static final int HEADER_TYPE = 0;
    private static final int ITEM_TYPE = 1;

    private Context context;
    private ServiceCallViewModel serviceCallViewModel;
    private List<ExpandableModel> models;
    ServiceCallAdapterCallback callback;

    public ServiceCallsAdapter(Context context, ServiceCallViewModel serviceCallViewModel,
                               ServiceCallAdapterCallback callback) {
        this.context = context;
        this.callback = callback;
        models = null;
        this.serviceCallViewModel = serviceCallViewModel;
        if (serviceCallViewModel != null && serviceCallViewModel.getCallViewModels() != null) {
            List<CallViewModel> callViewModels = serviceCallViewModel.getCallViewModels();
            models = new ArrayList<>();
            boolean headerAdded = false;
            for (int i = 0; i < callViewModels.size(); i++) {

                CallViewModel viewModel = callViewModels.get(i);

                if (viewModel.getState() == ServiceCallStateEnum.CLOSED || viewModel.getState() == ServiceCallStateEnum.CANCELED) {
                    if (!headerAdded) {
                        headerAdded = true;
                        ExpandableModel expandableModel = new ExpandableModel();
                        expandableModel.callViewModel = viewModel;
                        expandableModel.isHeader = true;

                        models.add(expandableModel);
                    }
                }

                ExpandableModel expandableModel = new ExpandableModel();
                expandableModel.callViewModel = viewModel;
                expandableModel.isExpanded = false;
                expandableModel.isHeader = false;

                models.add(expandableModel);
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        ExpandableModel expandableModel = models.get(position);
        return expandableModel.isHeader ? HEADER_TYPE : ITEM_TYPE;
    }

    @Override
    public CallViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case HEADER_TYPE:
                ViewGroup view = (ViewGroup) inflater.inflate(R.layout.view_service_call_header, parent, false);
                return new HeaderViewHolder(view);
            default:
                view = (ViewGroup) inflater.inflate(R.layout.view_service_call_item, parent, false);
                return new ItemViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(CallViewHolder viewHolder, int position) {
        ExpandableModel expandableModel = models.get(position);

        if (viewHolder instanceof ItemViewHolder) {
            ((ItemViewHolder) viewHolder).position = position;

            buildHeaderView(((ItemViewHolder) viewHolder), expandableModel);
            buildChildView(((ItemViewHolder) viewHolder), expandableModel);
            expandableModel.viewHolder = (ItemViewHolder) viewHolder;
        } else if (viewHolder instanceof HeaderViewHolder) {
            HeaderViewHolder headerViewHolder = (HeaderViewHolder) viewHolder;
            headerViewHolder.headerTextView.setText(context.getString(R.string.service_call_closed_in_last_x_days,
                    serviceCallViewModel.getClosedServiceCallsDays()));
        }
    }

    private void buildHeaderView(ItemViewHolder callViewHolder, ExpandableModel expandableModel) {

        CallViewModel callViewModel = expandableModel.callViewModel;
        if (callViewModel == null) {
            return;
        }

        String pressName = callViewModel.getImpressionType() != null ?
                context.getString(R.string.printer_name_with_model, callViewModel.getPressName(),
                        callViewModel.getImpressionType()) : callViewModel.getPressName();
        callViewHolder.pressNameTextView.setText(pressName);

        callViewHolder.pressExtraInfoTextView.setText(setHeaderInfo(callViewModel, expandableModel.isExpanded));

        callViewHolder.statusIndicator.setBackground(ResourcesCompat.getDrawable(context.getResources(),
                callViewModel.getState().getColorDrawable(), null));

        callViewHolder.statusTextView.setText(context.getString(callViewModel.getState().getDisplayNameString()));

        if (callViewModel.getDescription() != null) {
            setTextValue(callViewHolder.headerDescriptionText, callViewModel.getDescription());
        }
        callViewHolder.headerDescriptionText.setVisibility(expandableModel.isExpanded ? View.GONE : View.VISIBLE);

        callViewHolder.expandingIndicator.setImageResource(expandableModel.isExpanded ?
                R.drawable.expand_arrow_up : R.drawable.expand_arrow_down);
    }

    private String setHeaderInfo(CallViewModel callViewModel, boolean isExpanded) {
        if (!isExpanded) {
            Date dateToDisplay = null;
            switch (callViewModel.getState()) {
                case OPEN:
                    dateToDisplay = callViewModel.getDateOpened();
                    break;
                case CLOSED:
                case CANCELED:
                    dateToDisplay = callViewModel.getDateClosed();
                    break;
            }

            if (dateToDisplay != null) {
                return getDateString(dateToDisplay);
            }
        }

        return context.getString(R.string.service_call_serial_number, callViewModel.getPressSerialNumber());
    }

    private void buildChildView(ItemViewHolder callViewHolder, ExpandableModel expandableModel) {
        CallViewModel callViewModel = expandableModel.callViewModel;
        if (callViewModel == null) {
            return;
        }

        callViewHolder.containerLayout.setVisibility(expandableModel.isExpanded ? View.VISIBLE : View.GONE);

        setTextValue(callViewHolder.openedDateTextView, getDateString(callViewModel.getDateOpened()));
        setTextValue(callViewHolder.endDateTextView, getDateString(callViewModel.getDateClosed()));
        setTextValue(callViewHolder.openedByTextView, callViewModel.getOpenedBy());
        setTextValue(callViewHolder.callIDTextView, callViewModel.getId());
        setTextValue(callViewHolder.typeTextView, callViewModel.getType());
        setTextValue(callViewHolder.descriptionTextView, callViewModel.getDescription());

        callViewHolder.endDateLabelTextView.setText(context.getString(callViewModel.getState() == ServiceCallStateEnum.CLOSED ?
                R.string.service_call_end_date : R.string.service_call_canceled_date));
        callViewHolder.endDateLayout.setVisibility(callViewModel.getState() == ServiceCallStateEnum.OPEN ?
                View.GONE : View.VISIBLE);
    }

    private void setTextValue(TextView textView, String value) {
        if (textView == null) {
            return;
        }

        textView.setText(value == null ? context.getString(R.string.unknown_value) : value);
    }

    private String getDateString(Date date) {
        return date == null ? null : HPDateUtils.getDateTimeString(context, date, context.getString(
                R.string.date_range_different_year), null);
    }

    public int getFirstClosedOccurrence() {

        if (models == null) {
            return -1;
        }

        for (int i = 0; i < models.size(); i++) {
            ExpandableModel expandableModel = models.get(i);
            ServiceCallStateEnum stateEnum = expandableModel.callViewModel.getState();
            if (!expandableModel.isHeader && (stateEnum == ServiceCallStateEnum.CLOSED
                    || stateEnum == ServiceCallStateEnum.CANCELED)) {
                return i;
            }
        }

        return 0;
    }

    public void setExpanded(int index) {
        if (models == null || index < 0 || index >= models.size()) {
            return;
        }

        ExpandableModel model = models.get(index);
        if (model.viewHolder != null) {
            model.viewHolder.expandChild();
        }
    }

    @Override
    public int getItemCount() {
        return models == null ? 0 : models.size();
    }

    public class CallViewHolder extends RecyclerView.ViewHolder {

        public CallViewHolder(View itemView) {
            super(itemView);
        }
    }

    public class HeaderViewHolder extends CallViewHolder {

        @Bind(R.id.header_text_view)
        TextView headerTextView;

        public HeaderViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }

    public class ItemViewHolder extends CallViewHolder {

        @Bind(R.id.header_layout)
        View headerLayout;
        @Bind(R.id.press_name_text_view)
        TextView pressNameTextView;
        @Bind(R.id.extra_info_text_view)
        TextView pressExtraInfoTextView;
        @Bind(R.id.header_description_text)
        TextView headerDescriptionText;
        @Bind(R.id.status_indicator)
        ImageView statusIndicator;
        @Bind(R.id.status_text_view)
        TextView statusTextView;
        @Bind(R.id.expanding_indicator)
        ImageView expandingIndicator;
        @Bind(R.id.container_layout)
        View containerLayout;
        @Bind(R.id.opened_date_text_view)
        TextView openedDateTextView;
        @Bind(R.id.end_date_layout)
        View endDateLayout;
        @Bind(R.id.end_date_label_text_view)
        TextView endDateLabelTextView;
        @Bind(R.id.end_date_text_view)
        TextView endDateTextView;
        @Bind(R.id.opened_by_text_view)
        TextView openedByTextView;
        @Bind(R.id.call_id_text_view)
        TextView callIDTextView;
        @Bind(R.id.type_text_view)
        TextView typeTextView;
        @Bind(R.id.description_text_view)
        TextView descriptionTextView;

        int position;

        public ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            headerLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    expandChild();
                }
            });
        }

        public void expandChild() {
            ExpandableModel model = models.get(position);
            model.isExpanded = !model.isExpanded;

            if (model.isExpanded) {
                ServiceCallsAdapter.expand(containerLayout, position, callback);
            } else {
                ServiceCallsAdapter.collapse(containerLayout);
            }

            pressExtraInfoTextView.setText(setHeaderInfo(models.get(position).callViewModel, model.isExpanded));
            headerDescriptionText.setVisibility(model.isExpanded ? View.GONE : View.VISIBLE);
            expandingIndicator.setImageResource(model.isExpanded ?
                    R.drawable.expand_arrow_up : R.drawable.expand_arrow_down);
        }
    }

    public static void expand(final View view, final int position, final ServiceCallAdapterCallback callback) {
        view.measure(LinearLayout.LayoutParams.MATCH_PARENT, View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        final int targetHeight = view.getMeasuredHeight();

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        view.getLayoutParams().height = 1;
        view.setVisibility(View.VISIBLE);
        Animation animation = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation transformation) {
                view.getLayoutParams().height = interpolatedTime == 1
                        ? LinearLayout.LayoutParams.WRAP_CONTENT
                        : (int) (targetHeight * interpolatedTime);
                view.requestLayout();
                callback.onItemExpanded(position);
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        animation.setDuration(ANIMATION_DURATION);
        view.startAnimation(animation);
    }

    public static void collapse(final View view) {
        final int initialHeight = view.getMeasuredHeight();

        Animation animation = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation transformation) {
                if (interpolatedTime == 1) {
                    view.setVisibility(View.GONE);
                } else {
                    view.getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
                    view.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        animation.setDuration(ANIMATION_DURATION);
        view.startAnimation(animation);
    }


    public class ExpandableModel {
        CallViewModel callViewModel;
        boolean isExpanded;
        boolean isHeader;
        ItemViewHolder viewHolder;
    }

    public interface ServiceCallAdapterCallback {
        void onItemExpanded(int viewPosition);
    }
}
