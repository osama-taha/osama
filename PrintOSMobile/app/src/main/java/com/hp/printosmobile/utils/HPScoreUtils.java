package com.hp.printosmobile.utils;

import android.content.Context;
import android.support.v4.content.res.ResourcesCompat;

import com.hp.printosmobile.R;

/**
 * Created by Anwar Asbah on 5/22/2016.
 */
public class HPScoreUtils {

    public static final float SCORE_GOOD_LIMIT = 0.6f;
    public static final float SCORE_GREAT_LIMIT = 0.7f;
    public static final float DEFAULT_SCORE = 1f;

    public static int getProgressColor(Context context, double score, double target, boolean isTrack) {

        double percentage = target == 0 ?
                (score == 0 ? 100 : score)
                : score / target;

        if (percentage <= SCORE_GOOD_LIMIT) {
            return ResourcesCompat.getColor(context.getResources(), isTrack ? R.color.bar_light_red : R.color.bar_red, null);
        } else if (percentage <= SCORE_GREAT_LIMIT) {
            return ResourcesCompat.getColor(context.getResources(), isTrack ? R.color.bar_light_orange : R.color.bar_orange, null);
        } else {
            return ResourcesCompat.getColor(context.getResources(), isTrack ? R.color.bar_light_green : R.color.bar_green, null);
        }
    }
}
