package com.hp.printosmobile.data.remote.services;

import com.hp.printosmobile.data.remote.ApiConstants;
import com.hp.printosmobile.data.remote.models.ReportData;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Retrofit REST interface definition for Performance tracking Endpoints.
 *
 * @author Osama Taha
 */
public interface PerformanceTrackingService {

    @GET(ApiConstants.PERFORMANCE_TRACKING_WEEKLY_REPORT_API)
    Observable<Response<ResponseBody>> getWeeklyReport(@Query("bu") String businessUnit, @Query("devices") List<String> devices, @Query("weeks") int weeks, @Query("date") String date);

    @GET(ApiConstants.PERFORMANCE_TRACKING_WEEKLY_REPORT_API_V2)
    Observable<Response<WeeklyDataV2>> getWeeklyReport(@Query("bu") String businessUnit, @Query("devices") List<String> devices, @Query("weeks") int weeks, @Query("date") String date, @Query("unitSystem") String unitSystem, @Query("siteId") String siteId, @Query("lang") String lang);

    @GET(ApiConstants.PERFORMANCE_TRACKING_WEEKLY_OVERALL_REPORT_API)
    Observable<Response<ReportData>> getOverallReport(@Query("bu") String businessUnit, @Query("devices") List<String> devices, @Query("offset") int weekOffset);

    @GET(ApiConstants.SCOREABLE_API)
    Observable<Response<ReportData>> getKpiReport(@Path("kpiName") String kpiName, @Query("bu") String businessUnit, @Query("perpress") boolean perPress, @Query("devices") List<String> devices, @Query("offset") int offset, @Query("resolution") String resolution, @Query("unit") String unit);

    @GET(ApiConstants.SCOREABLE_API_V2)
    Observable<Response<ReportDataV2>> getKpiReportV2(@Path("kpiName") String kpiName, @Query("bu") String businessUnit, @Query("perpress") boolean perPress, @Query("devices") List<String> devices, @Query("offset") int offset, @Query("resolution") String resolution, @Query("unit") String unit, @Query("unitSystem") String unitSystem);

}
