package com.hp.printosmobile.presentation.modules.tooltip;

import java.io.Serializable;
import java.util.Comparator;
import java.util.List;

/**
 * Created by osama on 12/18/16.
 */
public class TodayInfoTooltipViewModel implements Serializable{

    List<InfoItemViewModel> printedList;
    List<InfoItemViewModel> impressionsList;
    private boolean hasInfo;

    public List<InfoItemViewModel> getPrintedList() {
        return printedList;
    }

    public void setPrintedList(List<InfoItemViewModel> printedList) {
        this.printedList = printedList;
    }

    public List<InfoItemViewModel> getImpressionsList() {
        return impressionsList;
    }

    public void setImpressionsList(List<InfoItemViewModel> impressionsList) {
        this.impressionsList = impressionsList;
    }

    public void setHasInfo(boolean hasInfo) {
        this.hasInfo = hasInfo;
    }

    public boolean hasInfo() {
        return hasInfo;
    }

    public static class InfoItemViewModel implements Serializable{

        private String infoItemName;
        private String infoItemValue;
        private int textColorResourceId;

        public InfoItemViewModel(String infoItemName, String infoItemValue, int textColorResourceId) {
            this.infoItemName = infoItemName;
            this.infoItemValue = infoItemValue;
            this.textColorResourceId = textColorResourceId;
        }

        public String getInfoItemName() {
            return infoItemName;
        }

        public void setInfoItemName(String infoItemName) {
            this.infoItemName = infoItemName;
        }

        public String getInfoItemValue() {
            return infoItemValue;
        }

        public void setInfoItemValue(String infoItemValue) {
            this.infoItemValue = infoItemValue;
        }

        public void setTextColorResourceId(int textColorResourceId) {
            this.textColorResourceId = textColorResourceId;
        }

        public int getTextColorResourceId() {
            return textColorResourceId;
        }
    }

    public static Comparator<InfoItemViewModel> NAME_COMPARATOR = new Comparator<InfoItemViewModel>() {
        @Override
        public int compare(InfoItemViewModel lhs, InfoItemViewModel rhs) {

            if (lhs == null && rhs == null) return 0;
            if (lhs == null && rhs != null) return 1;
            if (lhs != null && rhs == null) return -1;

            return lhs.getInfoItemName().compareTo(rhs.getInfoItemName());

        }
    };

}
