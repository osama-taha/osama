package com.hp.printosmobile.presentation.modules.devices;

import android.content.Context;

import com.hp.printosmobile.presentation.Presenter;
import com.hp.printosmobile.presentation.modules.home.HomeDataManager;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitViewModel;
import com.hp.printosmobile.presentation.modules.shared.BaseDeviceViewModel;
import com.hp.printosmobile.presentation.modules.shared.DeviceViewModel;
import com.hp.printosmobilelib.core.logging.HPLogger;

import java.util.ArrayList;
import java.util.List;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by Osama Taha on 5/23/16.
 */
public class DevicesPresenter extends Presenter<DevicesView> {

    private static final String TAG = DevicesPresenter.class.getSimpleName();

    private List<? extends BaseDeviceViewModel> mDeviceViewModels = new ArrayList<>();
    private BusinessUnitViewModel selectedBusinessUnit;
    private boolean isShiftSupport;

    public void loadData(Context context) {

        mView.showLoading();

        Subscription subscription = HomeDataManager.getDevicesDataForBusinessUnit(selectedBusinessUnit, true, isShiftSupport)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io()).subscribe(new Action1<List<DeviceViewModel>>() {
                    @Override
                    public void call(List<DeviceViewModel> deviceViewModels) {

                        if (deviceViewModels != null) {
                            mDeviceViewModels = deviceViewModels;

                            mView.hideLoading();

                            if (deviceViewModels.isEmpty()) {
                                mView.onEmptyDevicesList();
                            } else {
                                mView.onGettingDevicesCompleted(deviceViewModels);
                            }

                        } else {
                            loadError();
                        }

                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        loadError();
                        HPLogger.d(TAG, "Error while getting the devices " + throwable);
                    }
                });
        addSubscriber(subscription);
    }

    private void loadError() {

        //if (mDeviceViewModels.isEmpty()) {
            mView.hideLoading();
            mView.onError();
        //} else {
            //TODO: show error.
            mView.hideLoading();
        //}

    }

    public BusinessUnitViewModel getSelectedBusinessUnit() {
        return selectedBusinessUnit;
    }

    public void setSelectedBusinessUnit(BusinessUnitViewModel selectedBusinessUnit) {
        this.selectedBusinessUnit = selectedBusinessUnit;
    }

    public boolean isShiftSupport() {
        return isShiftSupport;
    }

    public void setShiftSupport(boolean shiftSupport) {
        isShiftSupport = shiftSupport;
    }

    public void onRefresh() {
        stopSubscribers();
    }

    @Override
    public void detachView() {
        stopSubscribers();
        mView = null;
    }

}