package com.hp.printosmobile.presentation.modules.npspopup;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.text.style.UnderlineSpan;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.AppseeSdk;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.data.remote.ApiConstants;
import com.hp.printosmobile.presentation.modules.drawer.OrganizationViewModel;
import com.hp.printosmobile.utils.AppUtils;
import com.hp.printosmobile.utils.HPUIUtils;
import com.hp.printosmobilelib.core.logging.HPLogger;
import com.hp.printosmobilelib.core.utils.HPDateUtils;
import com.hp.printosmobilelib.ui.widgets.HPTextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by anwar asbah on 7/17/2017.
 */
public class NPSPopup extends DialogFragment {

    public static final String TAG = NPSPopup.class.getSimpleName();
    private static final int NUMBER_OF_BIT_COINS = 25;
    private static final int MARGIN_BETWEEN_RATING_BUTTONS_DP = 5;
    private static final int RADIUS_DP = 3;

    public static int[] ratingColors = new int[]{
            Color.parseColor("#CE0000"),
            Color.parseColor("#d21000"),
            Color.parseColor("#d82c00"),
            Color.parseColor("#db3900"),
            Color.parseColor("#de4400"),
            Color.parseColor("#e04d00"),
            Color.parseColor("#F8B600"),
            Color.parseColor("#F4D944"),
            Color.parseColor("#5DA304"),
            Color.parseColor("#42A129"),
    };

    @Bind(R.id.rating_layout)
    LinearLayout ratingLayout;
    @Bind(R.id.title)
    TextView titleTextView;
    @Bind(R.id.not_sure_button)
    TextView notSureButton;

    NPSPopupCallback listener;

    public static NPSPopup getInstance(NPSPopupCallback popupCallback) {
        NPSPopup dialog = new NPSPopup();

        dialog.listener = popupCallback;
        Bundle bundle = new Bundle();
        dialog.setArguments(bundle);

        return dialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE,
                R.style.NPSDialogStyle);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.nps_popup, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        initView();
        getDialog().setCanceledOnTouchOutside(false);
        AppseeSdk.getInstance(getActivity()).startScreen(AppseeSdk.SCREEN_NPS);
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        }
    }

    private void initView() {
        ratingLayout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                ratingLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);

                int maxScore = ratingColors.length;

                int layoutWidth = ratingLayout.getMeasuredWidth();
                int availableWidth = layoutWidth - (maxScore - 1) * HPUIUtils.dpToPx(getActivity(), MARGIN_BETWEEN_RATING_BUTTONS_DP);
                int itemWidth = availableWidth / maxScore;

                for (int i = 0; i < 10; i++) {
                    ratingLayout.addView(getItemView(i, itemWidth));
                }
            }
        });

        String notSureText = getString(R.string.nps_not_sure);
        SpannableString notSureSpannable = new SpannableString(notSureText);
        notSureSpannable.setSpan(new UnderlineSpan(), 0, notSureText.length(), 0);
        notSureButton.setText(notSureSpannable);

        String title = getString(R.string.nps_title, NUMBER_OF_BIT_COINS);
        String bitCoinsString = String.valueOf(NUMBER_OF_BIT_COINS);
        int start = title.indexOf(bitCoinsString);
        int end = start + bitCoinsString.length();
        SpannableString titleSpannable = new SpannableString(title);
        titleSpannable.setSpan(new RelativeSizeSpan(2f), start, end, 0);
        titleTextView.setText(titleSpannable);
    }

    public TextView getItemView(final int position, int itemSize) {
        HPTextView textView = new HPTextView(getActivity());
        textView.setText(String.valueOf(position + 1));
        textView.setGravity(Gravity.CENTER);

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.height = itemSize;
        layoutParams.width = itemSize;
        layoutParams.setMargins(0, 0, position == ratingColors.length - 1 ? 0 :
                HPUIUtils.dpToPx(getActivity(), MARGIN_BETWEEN_RATING_BUTTONS_DP), 0);

        textView.setLayoutParams(layoutParams);
        textView.setBackground(getItemBackground(ratingColors[position]));
        textView.setTextColor(Color.WHITE);
        textView.setTextSize(16);

        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = getUrl(position + 1);
                if (url != null) {
                    AppUtils.startApplication(getActivity(), Uri.parse(url));
                }
                dismiss();
            }
        });

        return textView;
    }

    private String getUrl(int score) {

        HPLogger.d(TAG, "submit nps with score " + score);

        Analytics.sendEvent(Analytics.MOBILE_PRINT_BEAT_CATEGORY, Analytics.SUBMIT_NPS_ACTION, String.valueOf(score));
        AppseeSdk.getInstance(getActivity()).sendEventWithValueParameter(AppseeSdk.EVENT_SUBMIT_NPS, String.valueOf(score));

        PrintOSPreferences printOSPreferences = PrintOSPreferences.getInstance(getActivity());

        String organizationName = "";
        OrganizationViewModel organizationViewModel = printOSPreferences.getSelectedOrganization();
        if (organizationViewModel != null && organizationViewModel.getOrganizationName() != null) {
            organizationName = organizationViewModel.getOrganizationName();
        }
        String internalID = printOSPreferences.getInternalID();
        String locale = printOSPreferences.getLanguage(getString(R.string.default_language));

        return String.format(ApiConstants.NPS_URL, score, organizationName,
                internalID, locale,
                String.valueOf(HPDateUtils.getWeek()),
                String.valueOf(HPDateUtils.getYear()));

    }

    private Drawable getItemBackground(int color) {

        int radius = HPUIUtils.dpToPx(getActivity(), RADIUS_DP);
        float[] CORNER_RADII = new float[]{radius, radius, radius, radius, radius, radius, radius, radius};

        GradientDrawable shape = new GradientDrawable();
        shape.setShape(GradientDrawable.RECTANGLE);
        shape.setCornerRadii(CORNER_RADII);
        shape.setColor(color);
        shape.setStroke(HPUIUtils.dpToPx(getActivity(), 1), Color.WHITE);
        return shape;
    }

    @Override
    public void dismiss() {
        super.dismiss();
        if (listener != null) {
            listener.onDialogDismissed();
        }
    }

    @OnClick(R.id.not_sure_button)
    public void notSureClicked() {
        dismiss();
    }

    public interface NPSPopupCallback {
        void onDialogDismissed();
    }
}
