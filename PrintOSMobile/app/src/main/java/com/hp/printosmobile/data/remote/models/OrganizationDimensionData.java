package com.hp.printosmobile.data.remote.models;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Anwar Asbah
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OrganizationDimensionData {

    @JsonProperty("organizationId")
    private String organizationId;
    @JsonProperty("anonymous")
    private boolean anonymous;
    @JsonProperty("internalIds")
    List<InternalID> internalIDs;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("internalIds")
    public List<InternalID> getInternalIDs() {
        return internalIDs;
    }

    @JsonProperty("internalIds")
    public void setInternalIDs(List<InternalID> internalIDs) {
        this.internalIDs = internalIDs;
    }

    /**
     * @return The organizationId
     */
    @JsonProperty("organizationId")
    public String getOrganizationId() {
        return organizationId;
    }

    /**
     * @param organizationId The organizationId
     */
    @JsonProperty("organizationId")
    public void setName(String organizationId) {
        this.organizationId = organizationId;
    }

    /**
     * @return The anonymous
     */
    @JsonProperty("anonymous")
    public boolean getAnonymous() {
        return anonymous;
    }

    /**
     * @param anonymous The anonymous
     */
    @JsonProperty("anonymous")
    public void setAnonymous(boolean anonymous) {
        this.anonymous = anonymous;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("OrganizationDimensionData{");
        sb.append("organizationId='").append(organizationId).append('\'');
        sb.append(", anonymous=").append(anonymous);
        sb.append(", additionalProperties=").append(additionalProperties);
        sb.append('}');
        return sb.toString();
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class InternalID {

        @JsonProperty("gbuInternalId")
        String gbuInternalId;
        @JsonProperty("gbu")
        String gbu;
        @JsonProperty("internalIdType")
        String internalIdType;
        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<String, Object>();

        @JsonProperty("gbuInternalId")
        public String getGbuInternalId() {
            return gbuInternalId;
        }

        @JsonProperty("gbuInternalId")
        public void setGbuInternalId(String gbuInternalId) {
            this.gbuInternalId = gbuInternalId;
        }

        @JsonProperty("gbu")
        public String getGbu() {
            return gbu;
        }

        @JsonProperty("gbu")
        public void setGbu(String gbu) {
            this.gbu = gbu;
        }

        @JsonProperty("internalIdType")
        public String getInternalIdType() {
            return internalIdType;
        }

        @JsonProperty("internalIdType")
        public void setInternalIdType(String internalIdType) {
            this.internalIdType = internalIdType;
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }
    }
}
