package com.hp.printosmobile.presentation.modules.resetpassword;

import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.data.remote.models.ResetPasswordBody;
import com.hp.printosmobile.presentation.Presenter;
import com.hp.printosmobilelib.core.communications.remote.APIException;

import okhttp3.ResponseBody;
import retrofit2.Response;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * A presenter to manage the {@link ResetPasswordActivity}
 *
 * @author Anwar Asbah
 */
public class ResetPasswordPresenter extends Presenter<ResetPasswordView> {

    private static String TAG = ResetPasswordPresenter.class.getSimpleName();
    private static final int INVALID_USER_RESPONSE_CODE = 400;

    private Subscription subscription;

    public void reset(final String userName) {

        mView.showLoading();
        mView.setViewEnabled(false);

        subscription = PrintOSApplication.getApiServicesProvider().getResetPasswordService().reset(new ResetPasswordBody(userName))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<Response<ResponseBody>>() {
                    @Override
                    public void onCompleted() {
                        if (mView == null) {
                            return;
                        }
                        mView.hideLoading();
                    }

                    @Override
                    public void onError(Throwable e) {

                        if (mView == null) {
                            return;
                        }
                        mView.setViewEnabled(true);
                        mView.hideLoading();
                        if (e instanceof APIException) {
                            mView.onResetPasswordError((APIException) e);
                        } else {
                            mView.onResetPasswordError(APIException.create(APIException.Kind.UNEXPECTED));
                        }
                    }

                    @Override
                    public void onNext(Response<ResponseBody> responseBodyResponse) {

                        if (mView == null) {
                            return;
                        }

                        if (responseBodyResponse.isSuccessful()) {
                            mView.onResetPasswordSuccess();
                        } else if (responseBodyResponse.code() == INVALID_USER_RESPONSE_CODE) {
                            mView.setViewEnabled(true);
                            mView.onResetPasswordError(APIException.create(APIException.Kind.UNKNOWN_USER));
                        } else {
                            mView.setViewEnabled(true);
                            mView.onResetPasswordError(APIException.create(APIException.Kind.UNEXPECTED));
                        }
                    }
                });

    }

    @Override
    public void detachView() {
        mView = null;
        if (subscription != null) {
            subscription.unsubscribe();
            subscription = null;
        }

    }
}
