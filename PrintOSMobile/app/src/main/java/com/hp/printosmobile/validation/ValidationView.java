package com.hp.printosmobile.validation;

import com.hp.printosmobile.presentation.MVPView;

/**
 * Created by Anwar Asbah on 6/7/2017.
 */

public interface ValidationView extends MVPView {
    void onPreValidation();

    void onPreAutoLogin(boolean showToast);

    void onValidationCompleted();

    void onValidationError();

    void onValidationUnauthorized();

    void onValidationCompletedWithoutContextChange();
}
