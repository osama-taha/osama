package com.hp.printosmobile.presentation.modules.main;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.graphics.Color;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.OrientationEventListener;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.AppseeSdk;
import com.hp.printosmobile.BuildConfig;
import com.hp.printosmobile.Constants;
import com.hp.printosmobile.IntercomSdk;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.data.remote.services.VersionCheckService;
import com.hp.printosmobile.presentation.BaseActivity;
import com.hp.printosmobile.presentation.Navigator;
import com.hp.printosmobile.presentation.indigowidget.PrintOSAppWidgetProvider;
import com.hp.printosmobile.presentation.modules.about.AboutActivity;
import com.hp.printosmobile.presentation.modules.beatcoins.BeatCoinsPopup;
import com.hp.printosmobile.presentation.modules.beatcoins.BeatCoinsViewModel;
import com.hp.printosmobile.presentation.modules.contacthp.AskAQuestionActivity;
import com.hp.printosmobile.presentation.modules.contacthp.FeedbackActivity;
import com.hp.printosmobile.presentation.modules.contacthp.ReportAProblemActivity;
import com.hp.printosmobile.presentation.modules.contacthp.shared.ImageFetchListener;
import com.hp.printosmobile.presentation.modules.devices.DevicesFragment;
import com.hp.printosmobile.presentation.modules.drawer.BusinessUnitsAdapter;
import com.hp.printosmobile.presentation.modules.drawer.DivisionSwitchActivity;
import com.hp.printosmobile.presentation.modules.drawer.NavigationFragment;
import com.hp.printosmobile.presentation.modules.drawer.OrganizationViewModel;
import com.hp.printosmobile.presentation.modules.eula.EulaActivity;
import com.hp.printosmobile.presentation.modules.filters.FilterItem;
import com.hp.printosmobile.presentation.modules.filters.FiltersFragment;
import com.hp.printosmobile.presentation.modules.home.ActiveShiftsViewModel;
import com.hp.printosmobile.presentation.modules.home.HomeFragment;
import com.hp.printosmobile.presentation.modules.home.Panel;
import com.hp.printosmobile.presentation.modules.insights.InsightsFragment;
import com.hp.printosmobile.presentation.modules.login.LoginPresenter;
import com.hp.printosmobile.presentation.modules.next10jobs.JobsTodayFragment;
import com.hp.printosmobile.presentation.modules.notificationslist.NotificationListPresenter;
import com.hp.printosmobile.presentation.modules.notificationslist.NotificationsListActivity;
import com.hp.printosmobile.presentation.modules.npspopup.NPSNotificationManager;
import com.hp.printosmobile.presentation.modules.npspopup.NPSPopup;
import com.hp.printosmobile.presentation.modules.organization.OrganizationDialog;
import com.hp.printosmobile.presentation.modules.personaladvisor.AdviceViewModel;
import com.hp.printosmobile.presentation.modules.personaladvisor.presenter.PersonalAdvisorFactory;
import com.hp.printosmobile.presentation.modules.reports.ReportsFragment;
import com.hp.printosmobile.presentation.modules.settings.NotificationsPresenter;
import com.hp.printosmobile.presentation.modules.settings.SettingsActivity;
import com.hp.printosmobile.presentation.modules.settings.SubscriptionEvent;
import com.hp.printosmobile.presentation.modules.settings.SubscriptionEvent.NotificationEventEnum;
import com.hp.printosmobile.presentation.modules.shared.DeviceViewModel;
import com.hp.printosmobile.presentation.modules.shared.UserViewModel;
import com.hp.printosmobile.presentation.modules.today.TodayViewModel;
import com.hp.printosmobile.presentation.modules.todayhistogram.HistogramDetailsActivity;
import com.hp.printosmobile.presentation.modules.todayhistogram.TodayHistogramViewModel;
import com.hp.printosmobile.utils.AppUtils;
import com.hp.printosmobile.utils.DialogManager;
import com.hp.printosmobile.utils.HPUIUtils;
import com.hp.printosmobilelib.core.communications.remote.APIException;
import com.hp.printosmobilelib.core.logging.HPLogger;
import com.hp.printosmobilelib.ui.common.HPFragment;
import com.hp.printosmobilelib.ui.widgets.HPViewPager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.OnClick;
import io.intercom.android.sdk.Intercom;
import io.intercom.android.sdk.UnreadConversationCountListener;

/**
 * Main dashboard activity responsible for building the bottom tabs and managing the navigation.
 *
 * @author Osama Taha
 */

public class MainActivity extends BaseActivity implements NPSPopup.NPSPopupCallback, MainView, InsightsFragment.InsightsFragmentCallback, JobsTodayFragment.JobsTodayFragmentCallback, ReportsFragment.ReportFragmentCallback, TabLayout.OnTabSelectedListener, HomeFragment.HomeFragmentCallBack, NavigationFragment.NavigationFragmentCallbacks, DevicesFragment.DevicesCallbacks, BusinessUnitsAdapter.BusinessUnitsAdapterCallbacks, FiltersFragment.FiltersFragmentCallbacks {

    public static final String TAG = MainActivity.class.getSimpleName();

    private static final int SWITCH_DIVISION = 1;
    private static final int SWITCH_LANGUAGE = 2;
    private static final int SETTINGS_REQUEST_CODE = 3;
    private static final int NOTIFICATION_LIST_ACTIVITY_REQUEST_CODE = 4;
    private static final int HISTOGRAM_DETAILS_ACTIVITY_REQUEST_CODE = 5;

    private static final String POSITION = "tab_position";
    private static final String DIALOG_TAG = "Dialog";

    private static final int ANGLE_TOP_LEFT = 60;
    private static final int ANGLE_BOTTOM_LEFT = 120;
    private static final int ANGLE_BOTTOM_RIGHT = 240;
    private static final int ANGLE_TOP_RIGHT = 300;
    private static final int MAX_NUMBER_OF_NOTIFICATION_COUNT = 99;

    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private BroadcastReceiver mNotificationBroadcastReceiver;
    private boolean isReceiverRegistered;
    private static final long PRESENTER_DELAY_MILLI_SECOND = 100;
    private static final int DEFAULT_INDEX = 0;

    private List<OrganizationViewModel> organizations;
    private String selectedOrganizationID;

    private ImageFetchListener imageFetchListener;
    private LoginPresenter loginPresenter;

    private OrientationEventListener orientationEventListener;
    private boolean wasPhoneRotated = false;
    private boolean hasRTSupportedDevices = false;

    private boolean hasDialog;

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.toolbar_title)
    TextView toolbarTitle;
    @Bind(R.id.fragment_name)
    TextView toolbarFragmentName;
    @Bind(R.id.toolbar_main_layout)
    View toolbarMainLayout;
    @Bind(R.id.toolbar_back_button)
    View toolbarBackButton;
    @Bind(R.id.back_button_layout)
    View backButtonLayout;
    @Bind(R.id.sliding_tabs)
    TabLayout tabLayout;
    @Bind(R.id.viewpager)
    HPViewPager viewPager;
    @Bind(R.id.drawer_layout)
    DrawerLayout drawer;
    @Bind(R.id.loading_view)
    ProgressBar loadingView;
    @Bind(R.id.filters_fragment_container)
    View filtersContainer;
    @Bind(R.id.toolbar_filters_icon)
    View toolbarFilterButton;
    @Bind(R.id.number_of_notifications_text_view)
    TextView numberOfNotificationsTextView;
    @Bind(R.id.intercom_launcher_icon_layout)
    View intercomLauncherIconLayout;
    @Bind(R.id.intercom_launcher_icon)
    View intercomLauncherIcon;
    @Bind(R.id.number_of_intercom_notifications_text_view)
    TextView numberOfIntercomNotificationTextView;
    @Bind(R.id.intercepting_layout)
    View interceptingLayout;

    ActionBarDrawerToggle drawerToggle;
    private MainPagerAdapter adapter;
    private MainViewPresenter presenter;

    private NavigationFragment navigationFragment;
    private BusinessUnitViewModel businessUnitViewModel;
    private TodayHistogramViewModel todayHistogramViewModel;

    private boolean displayNotificationToast;

    private FiltersFragment filtersFragment;
    private boolean isFilterDisplayed;
    private int googlePlayServicesStatusCode;

    private UnreadConversationCountListener unreadConversationCountListener;
    private boolean isShiftSupport = false;
    private boolean isFirstEntry = true;
    private boolean isResumed = false;
    private TabLayout.Tab selectedTab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        returningFromBackground = true;
        isFilterDisplayed = false;
        hasDialog = false;
        initView();
        checkGooglePlayServicesAvailabilityStatus();

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                //for later use
            }
        };
        mNotificationBroadcastReceiver = new NotificationBroadCastReceiver();
        registerReceivers();

        if (intentHasExtra(Constants.NOTIFICATION_EVENT_KEY)) {
            String notificationType = getIntentStringExtra(Constants.NOTIFICATION_EVENT_KEY);
            IntercomSdk.getInstance(this).sendUserClickNotificationTypeEvent(notificationType);
            AppseeSdk.getInstance(this).sendEvent(AppseeSdk.EVENT_CLICK_NOTIFICATION, notificationType);
        }

        drawer.getViewTreeObserver()
                .addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {

                        drawer.getViewTreeObserver().removeOnGlobalLayoutListener(this);

                        requestNeededPermissions();
                    }
                });

        if (BuildConfig.isBetaVersion) {
            notifyBetaTesters();
        }

        NPSNotificationManager.setNotificationsEnabledByDefault();

    }

    @Override
    public boolean shouldValidateSession() {
        return true;
    }

    @Override
    public void onPreValidation() {
        super.onPreValidation();
        interceptingLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void onValidationCompleted() {
        super.onValidationCompleted();
        interceptingLayout.setVisibility(View.GONE);
        if (isFirstEntry) {
            isFirstEntry = false;
            init();
            navigationFragment.onValidationCompleted();
        } else {
            processFragmentsAfterValidation();
        }

        displayDialogsIfNeeded();
    }

    private void init() {
        initPresenter();

        if (PrintOSPreferences.getInstance(this).isFirstLaunch() &&
                googlePlayServicesStatusCode == ConnectionResult.SUCCESS) {
            subscribeToAllEvents();
            PrintOSPreferences.getInstance(this).setIsFirstLaunch(false);
        }

        logIntent();
        openNotificationListIfRequested();

        if (intentHasExtra(Constants.IntentExtras.MAIN_ACTIVITY_BEAT_COIN_EXTRA)) {
            if (presenter == null) {
                presenter = new MainViewPresenter();
                presenter.attachView(this);
            }
            presenter.getBeatCoinData(getIntentStringExtra(Constants.IntentExtras.MAIN_ACTIVITY_BEAT_COIN_EXTRA));
        }
    }

    @Override
    public void onValidationError() {
        super.onValidationError();
        interceptingLayout.setVisibility(View.GONE);
        init();
    }

    @Override
    public void onValidationUnauthorized() {
        super.onValidationUnauthorized();
        interceptingLayout.setVisibility(View.GONE);
        triggerSignOut();
    }

    @Override
    public void onValidationCompletedWithoutContextChange() {
        super.onValidationCompletedWithoutContextChange();
        interceptingLayout.setVisibility(View.GONE);
        if (isFirstEntry) {
            isFirstEntry = false;
            init();
        } else {
            resetActivity();
        }

        displayDialogsIfNeeded();

    }

    @Override
    public void onValidationPeriodPassed() {
        super.onValidationPeriodPassed();
        if (adapter != null && adapter.getFragments() != null) {
            for (Fragment fragment : adapter.getFragments()) {
                if (fragment instanceof IMainFragment) {
                    if (((IMainFragment) fragment).respondToValidationPeriodExceedance()) {
                        moveToHome();
                        return;
                    }
                }
            }
        }
    }

    private void notifyBetaTesters() {
        if (!PrintOSPreferences.getInstance(this).wasBetaTestersNotified()) {
            HPUIUtils.displayToast(this, getString(R.string.beta_version_welcome_message), false);
            PrintOSPreferences.getInstance(this).setBetaTestersNotified();
        }
    }

    private void updateIntercomBadge() {
        int unreadCount = Math.min(IntercomSdk.getInstance(this).getNumberOfUnreadMsgs(),
                MAX_NUMBER_OF_NOTIFICATION_COUNT);
        numberOfIntercomNotificationTextView.setText(String.valueOf(unreadCount));

        numberOfIntercomNotificationTextView.setVisibility(unreadCount == 0 ? View.INVISIBLE : View.VISIBLE);
    }

    private void checkGooglePlayServicesAvailabilityStatus() {

        googlePlayServicesStatusCode = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(this);
        String status = AppUtils.getGooglePlayServicesAvailabilityStatusMessage(googlePlayServicesStatusCode);

        PrintOSPreferences printOSPreferences = PrintOSPreferences.getInstance(this);

        if (!printOSPreferences.getGooglePlayServiceStatus().equals(status)) {

            Analytics.sendEvent(Analytics.MOBILE_PRINT_BEAT_CATEGORY, Analytics.GOOGLE_PLAY_SERVICES_STATUS_ACTION, status);

            //if status changed to SUCCESS, then call the registration service.
            if (googlePlayServicesStatusCode == ConnectionResult.SUCCESS) {
                startRegistrationService(Constants.IntentExtras.NOTIFICATION_REGISTER_INTENT_ACTION);
            }

            printOSPreferences.setGooglePlayServicesStatus(status);

        }
    }

    private void subscribeToAllEvents() {

        NotificationsPresenter presenter = new NotificationsPresenter();

        for (NotificationEventEnum event : NotificationEventEnum.values()) {

            if (!event.isSubscribeOnFirstLaunch() || event == NotificationEventEnum.UNKNOWN) {
                continue;
            }

            SubscriptionEvent subscriptionEvent = new SubscriptionEvent();
            subscriptionEvent.setNotificationEventEnum(event);
            presenter.subscribeEvent(subscriptionEvent);
        }

    }

    private void logIntent() {

        if (intentHasExtra(Constants.IntentExtras.MAIN_ACTIVITY_NOTIFICATION_ID_EXTRA) &&
                intentHasExtra(Constants.IntentExtras.MAIN_ACTIVITY_USER_ID_EXTRA)) {
            NotificationListPresenter presenter = new NotificationListPresenter();
            presenter.markNotificationAsRead(getIntentIntExtra(Constants.IntentExtras.MAIN_ACTIVITY_NOTIFICATION_ID_EXTRA),
                    getIntentIntExtra(Constants.IntentExtras.MAIN_ACTIVITY_USER_ID_EXTRA));
        }

        boolean hasNotificationExtra = intentHasExtra(Constants.IntentExtras.MAIN_ACTIVITY_NOTIFICATION_EXTRA);
        String notificationType = null;

        if (hasNotificationExtra) {
            notificationType = getIntentStringExtra(Constants.IntentExtras.MAIN_ACTIVITY_NOTIFICATION_EXTRA);
            HPLogger.d(TAG, "notificationKey: " + notificationType);
        }

        boolean openedFromPushNotification = intentHasExtra(Constants.IntentExtras.MAIN_ACTIVITY_IS_PUSH_NOTIFICATION);

        if (openedFromPushNotification) {

            Analytics.sendEvent(Analytics.APP_OPENED_ACTION);

            if (hasNotificationExtra) {
                HPLogger.d(TAG, "click push notification " + notificationType);
                AppseeSdk.getInstance(this).sendEvent(AppseeSdk.EVENT_OPEN_FROM_NOTIFICATION, notificationType);
                Analytics.sendEvent(Analytics.MOBILE_PRINT_BEAT_CATEGORY, Analytics.CLICK_NOTIFICATION_ACTION, notificationType);
                Analytics.sendNotificationClickedFirebaseEvent(notificationType);

            } else {
                AppseeSdk.getInstance(this).sendEvent(AppseeSdk.EVENT_OPEN_FROM_NOTIFICATION);
            }
        }

        if (intentHasExtra(Constants.IntentExtras.MAIN_ACTIVITY_ORGANIZATION_EXTRA)) {
            HPLogger.d(TAG, "organization: " + getIntentStringExtra(Constants.IntentExtras.MAIN_ACTIVITY_ORGANIZATION_EXTRA));
        }
        if (intentHasExtra(Constants.IntentExtras.MAIN_ACTIVITY_DIVISION_EXTRA)) {
            HPLogger.d(TAG, "division: " + getIntentStringExtra(Constants.IntentExtras.MAIN_ACTIVITY_DIVISION_EXTRA));
        }
        if (intentHasExtra(Constants.IntentExtras.MAIN_ACTIVITY_FILTER_EXTRA)) {
            HPLogger.d(TAG, "filter: " + getIntentStringExtra(Constants.IntentExtras.MAIN_ACTIVITY_FILTER_EXTRA));
        }
        if (intentHasExtra(Constants.IntentExtras.MAIN_ACTIVITY_TODAY_EXTRA)) {
            HPLogger.d(TAG, "today: " + getIntentStringExtra(Constants.IntentExtras.MAIN_ACTIVITY_TODAY_EXTRA));
        }
        if (intentHasExtra(Constants.IntentExtras.MAIN_ACTIVITY_BEAT_COIN_EXTRA)) {
            HPLogger.d(TAG, "beatCoin: " + getIntentStringExtra(Constants.IntentExtras.MAIN_ACTIVITY_BEAT_COIN_EXTRA));
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        isResumed = true;
        Intercom.client().handlePushMessage();
        shouldEnableOrientation();

        updateIntercomBadge();
        registerIntercomListener();
    }

    @Override
    protected void onPause() {
        super.onPause();
        enableOrientationEventListener(false);
        isResumed = false;
        unregisterIntercomListener();
    }

    private void registerIntercomListener() {
        if (unreadConversationCountListener == null) {
            unreadConversationCountListener = new UnreadConversationCountListener() {
                @Override
                public void onCountUpdate(int i) {
                    updateIntercomBadge();
                }
            };
        }

        IntercomSdk.getInstance(this).addUnreadConversationCountListener(unreadConversationCountListener);
    }

    private void unregisterIntercomListener() {
        if (unreadConversationCountListener == null) {
            IntercomSdk.getInstance(this).removeUnreadConversationCountListener(unreadConversationCountListener);
        }
    }

    private void openNotificationListIfRequested() {

        if (intentHasExtra(Constants.IntentExtras.MAIN_ACTIVITY_NOTIFICATION_EXTRA)) {

            String notificationType = getIntentStringExtra(Constants.IntentExtras.MAIN_ACTIVITY_NOTIFICATION_EXTRA);
            NotificationEventEnum eventEnum = NotificationEventEnum
                    .from(notificationType);
            if (eventEnum == NotificationEventEnum.BOX_NEW_FOLDER) {
                if (intentHasExtra(Constants.IntentExtras.MAIN_ACTIVITY_LINK_EXTRA)) {
                    String boxNewFolderLink = getIntentStringExtra(Constants.IntentExtras.MAIN_ACTIVITY_LINK_EXTRA);
                    AppUtils.startApplication(this, Uri.parse(boxNewFolderLink));
                }
            }

        }
    }

    private void initView() {

        setupToolbar();
        setupDrawer();
        setupViewPager();
        setupScreenMotionDetector();
    }


    void displayDialogsIfNeeded() {
        if (hasDialog) {
            return;
        }

        if (DialogManager.displayDialogs(this)) {
            hasDialog = true;
            enableOrientationEventListener(false);
        }
        shouldEnableOrientation();
    }

    private void initPresenter() {
        presenter = new MainViewPresenter();
        presenter.attachView(this);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //if intent has organization it is being called from notification action
                //stop normal initialization
                if (!intentHasExtra(Constants.IntentExtras.MAIN_ACTIVITY_ORGANIZATION_EXTRA)) {
                    if (presenter.getView() != null) {
                        presenter.initView(MainActivity.this);
                    }
                }
            }
        }, PRESENTER_DELAY_MILLI_SECOND);
        loginPresenter = new LoginPresenter();
    }

    private void setupToolbar() {
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(Color.BLACK);

        backButtonLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        displayIntercomIcon(true);
    }

    @OnClick(R.id.intercom_launcher_icon_layout)
    public void onIntercomIconClicked() {
        IntercomSdk.getInstance(this).displayConversationsList();
    }

    private void setupDrawer() {

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        navigationFragment = (NavigationFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_navigation);

        drawerToggle = new ActionBarDrawerToggle(this, drawer, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {

            /**
             * Called when a drawer has settled in a completely open state.
             * */
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                AppseeSdk.getInstance(MainActivity.this).sendEvent(AppseeSdk.EVENT_OPEN_MENU);
            }

            /**
             *  Called when a drawer has settled in a completely closed state.
             */
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                AppseeSdk.getInstance(MainActivity.this).sendEvent(AppseeSdk.EVENT_CLOSE_MENU);
            }
        };

        drawer.addDrawerListener(drawerToggle);
        drawerToggle.syncState();

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(POSITION, tabLayout.getSelectedTabPosition());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        viewPager.setCurrentItem(savedInstanceState.getInt(POSITION));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == NOTIFICATION_LIST_ACTIVITY_REQUEST_CODE) {
            updateBadge();
            return;
        }

        if (requestCode == HISTOGRAM_DETAILS_ACTIVITY_REQUEST_CODE) {
            wasPhoneRotated = false;
        }

        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case SWITCH_DIVISION:
                    todayHistogramViewModel = null;

                    String result = data.getStringExtra(DivisionSwitchActivity.SELECTION_KEY);

                    Analytics.sendEvent(Analytics.MOBILE_PRINT_BEAT_CATEGORY, Analytics.SWITCH_EQUIPMENT_ACTION,
                            result);

                    BusinessUnitEnum businessUnit = BusinessUnitEnum.from(result);

                    if (PrintOSApplication.getBusinessUnits() != null && PrintOSApplication.getBusinessUnits().containsKey(businessUnit)) {
                        IntercomSdk.getInstance(this).sendUserChangeEquipmentEvent(businessUnit.getShortName());
                        AppseeSdk.getInstance(this).sendEvent(AppseeSdk.EVENT_VIEW_BUSINESS_UNIT, businessUnit.getShortName());
                        onBusinessUnitSelected(PrintOSApplication.getBusinessUnits().get(businessUnit), true);
                    }

                    hideDetailedView();

                    break;
                case SWITCH_LANGUAGE:
                    resetActivity();
                    break;
                case SETTINGS_REQUEST_CODE:
                    resetActivity();
                    break;
                case HISTOGRAM_DETAILS_ACTIVITY_REQUEST_CODE:
                    Bundle bundle = data.getExtras();
                    if (bundle != null && bundle.containsKey(HistogramDetailsActivity.HISTOGRAM_VIEW_MODEL_PARAM)) {
                        todayHistogramViewModel = (TodayHistogramViewModel) bundle.getSerializable(
                                HistogramDetailsActivity.HISTOGRAM_VIEW_MODEL_PARAM);
                        String fragmentName = getString(R.string.fragment_home_name_key);
                        Fragment fragment = adapter.getFragment(fragmentName);
                        if (fragment != null && fragment instanceof HomeFragment) {
                            ((HomeFragment) fragment).setHistogramModel(todayHistogramViewModel);
                        }
                    }
                    break;
            }

        }
    }


    private void resetActivity() {
        HPLogger.d(TAG, "reset activity");
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    private void hideDetailedView() {
        for (Fragment fragment : adapter.getFragments()) {
            if (fragment instanceof HPFragment) {
                ((HPFragment) fragment).onBackPressed();
            }
        }
    }

    @OnClick(R.id.toolbar_menu_icon)
    public void onNavigationClicked() {

        if (drawer.isDrawerOpen(Gravity.RIGHT)) {
            drawer.closeDrawer(Gravity.RIGHT);
        } else {
            drawer.openDrawer(Gravity.RIGHT);
        }
    }

    @OnClick(R.id.toolbar_filters_icon)
    public void onFiltersClicked() {
        if (!isFilterDisplayed) {
            presentFilters();
        } else {
            filtersFragment.resetFilters();
            hideFilters();
        }

    }

    @OnClick(R.id.notification_icon_layout)
    public void onNotificationIconClicked() {
        startActivityForResult(new Intent(this, NotificationsListActivity.class), NOTIFICATION_LIST_ACTIVITY_REQUEST_CODE);
    }

    private void presentFilters() {
        HPLogger.d(TAG, "Show Filters");
        if (filtersFragment == null) {
            return;
        }
        filtersContainer.setVisibility(View.GONE);
        isFilterDisplayed = true;
        filtersFragment.onShowFilter();
        filtersContainer.setVisibility(View.VISIBLE);

        enableOrientationEventListener(false);
        hasDialog = true;

        if (businessUnitViewModel != null) {
            IntercomSdk.getInstance(this).logEvent(IntercomSdk.PBM_VIEW_FILTER_EVENT,
                    businessUnitViewModel.getBusinessUnit());
        }
    }

    private void hideFilters() {
        HPLogger.d(TAG, "hide Filters");
        isFilterDisplayed = false;
        if (filtersFragment != null) {
            filtersFragment.onHideFilter();
        }
    }

    @Override
    public void onFilterHideDone() {
        filtersContainer.setVisibility(View.GONE);
        hasDialog = false;
        shouldEnableOrientation();
    }

    private void setupViewPager() {

        adapter = new MainPagerAdapter(getSupportFragmentManager(), getApplicationContext());

        adapter.addFragment(HomeFragment.newInstance(), getString(R.string.fragment_home_name_key), R.drawable.home, R.drawable.home_selected);
        adapter.addFragment(InsightsFragment.newInstance(), getString(R.string.fragment_insights_name_key), R.drawable.insights, R.drawable.insights_selected);

        JobsTodayFragment nextTenJobsFragment = JobsTodayFragment.newInstance(null);
        nextTenJobsFragment.attachListener(this);
        adapter.addFragment(nextTenJobsFragment, getString(R.string.fragment_jobs_key), R.drawable.jobs, R.drawable.jobs_selected);
        adapter.addFragment(DevicesFragment.newInstance(), getString(R.string.fragment_devices_name_key), R.drawable.devices, R.drawable.devices_selected);
        adapter.addFragment(ReportsFragment.newInstance(), getString(R.string.fragment_reports_name_key), R.drawable.reports, R.drawable.reports_selected);

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        viewPager.setScrollingEnabled(false);
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(adapter.getFragments().size());

        tabLayout.setupWithViewPager(viewPager);

        tabLayout.addOnTabSelectedListener(this);

        // Iterate over all tabs and set the custom view
        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            tab.setCustomView(adapter.getTabView(i, i == DEFAULT_INDEX));
        }

        final int index = adapter.fragmentTitles.indexOf(getString(R.string.fragment_contact_hp_name_key));
        if (index > 0) {
            ((ViewGroup) tabLayout.getChildAt(0)).getChildAt(index).setVisibility(View.GONE);
        }

        tabLayout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                tabLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                setTabEnabled(getString(R.string.fragment_insights_name_key), false);
            }
        });
    }

    private void setupScreenMotionDetector() {
        orientationEventListener = new OrientationEventListener(this,
                SensorManager.SENSOR_DELAY_NORMAL) {

            @Override
            public void onOrientationChanged(int orientation) {
                if ((orientation > ANGLE_TOP_LEFT && orientation < ANGLE_BOTTOM_LEFT)
                        || (orientation > ANGLE_BOTTOM_RIGHT && orientation < ANGLE_TOP_RIGHT)) {
                    if (hasRTSupportedDevices && wasPhoneRotated && hasRTSupportedDevices) {
                        HPLogger.d(TAG, "onOrientationChanged");
                        enableOrientationEventListener(false);
                        onHistogramClicked(todayHistogramViewModel);
                    }
                } else if (orientation > 0) {
                    wasPhoneRotated = true;
                }
            }
        };

        if (orientationEventListener.canDetectOrientation()) {
            HPLogger.d(TAG, "Can DetectOrientation");
        } else {
            HPLogger.d(TAG, "Can't DetectOrientation");
        }

        enableOrientationEventListener(false);
    }

    @Override
    public void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }


    @Override
    protected int getLayoutResource() {
        return R.layout.activity_main;
    }

    @Override
    public void onBackPressed() {

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawer(GravityCompat.END);
        } else {

            if (filtersContainer.getVisibility() == View.VISIBLE) {
                filtersContainer.setVisibility(View.GONE);
                if (filtersFragment != null) {
                    filtersFragment.onCancelFiltersClicked();
                }
                return;
            }

            String tabName = tabLayout.getTabAt(tabLayout.getSelectedTabPosition()).getText().toString();
            int index = adapter.fragmentTitles.indexOf(tabName);
            Fragment fragment = adapter.getItem(index);
            if (fragment instanceof HPFragment) {
                if (((HPFragment) fragment).onBackPressed()) {
                    shouldEnableOrientation();
                    return;
                }
            }

            super.onBackPressed();
        }


    }

    private void updateToolbar(Fragment fragment) {
        if (fragment instanceof HPFragment) {
            toolbarFragmentName.setText(((HPFragment) fragment).getToolbarDisplayName());

            boolean update = ((HPFragment) fragment).isFragmentNameToBeDisplayed();
            toolbarMainLayout.setVisibility(update ? View.GONE : View.VISIBLE);
            backButtonLayout.setVisibility(update ? View.VISIBLE : View.GONE);

            toolbarFilterButton.setVisibility(((HPFragment) fragment).isFilteringAvailable() && !isSingleDevice(businessUnitViewModel) ?
                    View.VISIBLE : View.GONE);
        }
    }

    private void updateToolbarUsingExtras(Fragment fragment) {
        if (fragment instanceof HPFragment) {
            toolbarFragmentName.setText(getString(
                    ((HPFragment) fragment).getToolbarDisplayName(),
                    ((HPFragment) fragment).getToolbarDisplayNameExtra()));

            boolean update = ((HPFragment) fragment).isFragmentNameToBeDisplayed();
            toolbarMainLayout.setVisibility(update ? View.GONE : View.VISIBLE);
            //toolbarFragmentName.setVisibility(update ? View.VISIBLE : View.GONE);
            //toolbarBackButton.setVisibility(update ? View.VISIBLE : View.GONE);
            backButtonLayout.setVisibility(update ? View.VISIBLE : View.GONE);

            toolbarFilterButton.setVisibility(((HPFragment) fragment).isFilteringAvailable() && !isSingleDevice(businessUnitViewModel) ?
                    View.VISIBLE : View.GONE);
        }
    }

    private void displayIntercomIcon(boolean display) {

        intercomLauncherIcon.clearAnimation();
        if (display) {
            Animation pulse = AnimationUtils.loadAnimation(this, R.anim.pulse);
            intercomLauncherIcon.startAnimation(pulse);
        }
        intercomLauncherIconLayout.setVisibility(display ? View.VISIBLE : View.GONE);

    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        selectedTab = tab;
        viewPager.setCurrentItem(tab.getPosition());
        setFragmentName();

        Fragment fragment = adapter.getItem(adapter.fragmentTitles.indexOf(tab.getText()));
        if (fragment instanceof ReportsFragment) {
            ((ReportsFragment) fragment).loadScreen();
            //TODO ((ReportsFragment) fragment).openReportFragment(null);
        } else if (fragment instanceof DevicesFragment) {
            if (!((DevicesFragment) fragment).isDeviceDetailsDisplayed()) {
                Analytics.sendEvent(Analytics.MOBILE_PRINT_BEAT_CATEGORY, Analytics.MACHINE_VIEW_ACTION);
            }
        } else if (fragment instanceof InsightsFragment) {
            ((InsightsFragment) fragment).loadScreen();
        }

        String TabName = getString(R.string.fragment_devices_name_key);
        Fragment deviceFragment = adapter.getItem(adapter.fragmentTitles.indexOf(TabName));
        if (deviceFragment instanceof DevicesFragment) {
            ((DevicesFragment) deviceFragment).removeDeviceDetailsDrillDownViews();
        }

        hideHistogramDetailsView();

        if (fragment instanceof HomeFragment && PrintOSApplication.getBusinessUnits() != null) {
            shouldEnableOrientation();
        } else {
            enableOrientationEventListener(false);
        }

        hideHistogramDetailsView();
        updateToolbar(fragment);
        onTabSelected(fragment);

        //Activity can call this method from onRestoreInstanceState when you open the app from background.
        //check if the activity is completely loaded before updating the bottom tabs.
        if (businessUnitViewModel == null) {
            return;
        }

        adapter.setSelected(businessUnitViewModel.getBusinessUnit(), tab, tab.getPosition());

    }

    private void onTabSelected(Fragment fragment) {

        if (businessUnitViewModel != null) {
            String event = null;
            String tabName = null;

            if (fragment instanceof HomeFragment) {
                event = IntercomSdk.PBM_VIEW_HOME_EVENT;
                tabName = AppseeSdk.SCREEN_HOME;
            } else if (fragment instanceof ReportsFragment) {
                event = IntercomSdk.PBM_VIEW_REPORTS_EVENT;
                tabName = AppseeSdk.SCREEN_REPORTS;
            } else if (fragment instanceof DevicesFragment) {
                event = IntercomSdk.PBM_VIEW_DEVICE_EVENT;
                tabName = AppseeSdk.SCREEN_DEVICES;
            } else if (fragment instanceof JobsTodayFragment) {
                event = IntercomSdk.PBM_VIEW_QUEUED_EVENT;
                tabName = AppseeSdk.JOBS;
            } else if (fragment instanceof InsightsFragment) {
                tabName = AppseeSdk.SCREEN_INSIGHTS;
            }
            if (event != null) {
                IntercomSdk.getInstance(this).logEvent(event, businessUnitViewModel.getBusinessUnit());
                AppseeSdk.getInstance(this).startScreen(tabName);
            }
        }
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

        //Activity can call this method from onRestoreInstanceState when you open the app from background.
        //check if the activity is completely loaded before updating the bottom tabs.
        if (businessUnitViewModel == null) {
            return;
        }

        adapter.setUnselected(businessUnitViewModel.getBusinessUnit(), tab, tab.getPosition());
    }

    private void hideHistogramDetailsView() {
        String tabName = getString(R.string.fragment_home_name_key);
        int index = adapter.fragmentTitles.indexOf(tabName);
        if (index >= 0) {
            Fragment home = adapter.getItem(index);
            if (home instanceof HPFragment) {
                ((HPFragment) home).onBackPressed();
            }
        }
    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {
        Fragment fragment = adapter.getItem(adapter.fragmentTitles.indexOf(tab.getText()));

        if ((fragment instanceof HomeFragment)) {
            hideHistogramDetailsView();
        }
        if ((fragment instanceof DevicesFragment)) {
            ((DevicesFragment) fragment).removeDeviceDetailsDrillDownViews();
        }

        if (fragment instanceof HomeFragment && PrintOSApplication.getBusinessUnits() != null) {
            shouldEnableOrientation();
        } else {
            enableOrientationEventListener(false);
        }

        updateToolbar(fragment);

        onTabSelected(fragment);

    }

    @Override
    public void onPrinterSnapshotClicked() {
        openDevicesTab();
    }

    @Override
    public void onTodayPanelDevicesRetrieved(List<DeviceViewModel> models) {
        Fragment fragment = adapter.getFragment(getString(R.string.fragment_jobs_key));
        if (fragment instanceof JobsTodayFragment) {
            ((JobsTodayFragment) fragment).setViewModels(models);
        }
    }

    @Override
    public void onTodayDataFailure() {
        Fragment fragment = adapter.getFragment(getString(R.string.fragment_jobs_key));
        if (fragment instanceof JobsTodayFragment) {
            ((JobsTodayFragment) fragment).setLoading(false);
            ((JobsTodayFragment) fragment).onError();
        }
    }

    @Override
    public void onTodayViewAllClicked() {
        openDevicesTab();
    }

    @Override
    public void onHistogramClicked(TodayHistogramViewModel model) {
        Fragment fragment = adapter.getFragment(getString(R.string.fragment_home_name_key));
        TodayViewModel todayViewModel = null;
        ActiveShiftsViewModel activeShiftsViewModel = null;
        if (fragment instanceof HomeFragment) {
            todayViewModel = ((HomeFragment) fragment).getTodayViewModel();
            activeShiftsViewModel = ((HomeFragment) fragment).getActiveShiftViewModel();
        }
        HistogramDetailsActivity.startActivityForResult(this, model, todayViewModel, activeShiftsViewModel,
                businessUnitViewModel, isShiftSupport, HISTOGRAM_DETAILS_ACTIVITY_REQUEST_CODE);
    }

    @Override
    public void onHistogramViewClosed(HPFragment fragment) {
        updateToolbar(fragment);
    }

    @Override
    public void onHistogramDataRetrieved(TodayHistogramViewModel model) {
        this.todayHistogramViewModel = model;
    }

    @Override
    public void onKpiSelected(String kpiName) {
        openReportTab(kpiName);
    }

    @Override
    public void onWeekPanelClicked() {
        openReportTab(null);
    }

    @Override
    public void onRefresh() {
        HPLogger.d(TAG, "Swipe to Refresh");
        Analytics.sendEvent(Analytics.MOBILE_PRINT_BEAT_CATEGORY, Analytics.REFRESH_DATE_EVENT, Analytics.MAIN_SCREEN_NAME);

        for (Fragment fragment : adapter.getFragments()) {
            if (fragment instanceof ReportsFragment) {
                ((ReportsFragment) fragment).onRefresh();
            } else if (fragment instanceof DevicesFragment) {
                ((DevicesFragment) fragment).onRefresh();
            } else if (fragment instanceof JobsTodayFragment) {
                ((JobsTodayFragment) fragment).setLoading(true);
            }
        }

        if (businessUnitViewModel != null) {
            onBusinessUnitSelected(businessUnitViewModel, false);
        } else {
            if (presenter == null) {
                presenter = new MainViewPresenter();
                presenter.attachView(this);
            }
            presenter.initView(this);
        }
    }

    public void processFragmentsAfterValidation() {
        if (selectedTab != null && adapter != null && adapter.fragmentTitles != null) {

            String reportTabName = getString(R.string.fragment_reports_name_key);
            String selectedTabName = adapter.fragmentTitles.get(selectedTab.getPosition());

            if (reportTabName.equals(selectedTabName)) {
                Fragment fragment = adapter.getFragment(reportTabName);
                if (fragment instanceof ReportsFragment) {
                    ((ReportsFragment) fragment).onPostValidation();
                }
            }
        }

        onRefresh();
    }

    @Override
    public void reload() {
        navigationFragment.reload();
    }

    @Override
    public void onShiftSelected(boolean isShiftSupport) {
        this.isShiftSupport = isShiftSupport;
        String tabName = getString(R.string.fragment_devices_name_key);
        int index = adapter.fragmentTitles.indexOf(tabName);
        Fragment fragment = adapter.getItem(index);

        if (fragment instanceof DevicesFragment) {
            ((DevicesFragment) fragment).onShiftSelected(isShiftSupport);
        }
    }

    @Override
    public void triggerSignOut() {
        performLogout();
    }

    @Override
    public void onPrinterSnapshotLatexDeviceClicked(DeviceViewModel model) {
        //Mainly for PWP drill down.
    }

    @Override
    public void onTodayPanelDeviceClicked(DeviceViewModel model) {
        String tabName = getString(R.string.fragment_devices_name_key);
        int index = adapter.fragmentTitles.indexOf(tabName);
        Fragment fragment = adapter.getItem(index);

        if (fragment instanceof DevicesFragment) {
            openDevicesTab();
            ((DevicesFragment) fragment).onDeviceClicked(model, false);
        }
    }

    @Override
    public void onIndigoProductionQueueViewClicked(DeviceViewModel.JobType jobType) {
        openJobsTab(jobType);
    }

    @Override
    public void setNumberOfNotifications(int numberOfNotifications) {
        numberOfNotificationsTextView.setText(String.valueOf(numberOfNotifications));
        numberOfNotificationsTextView.setVisibility(numberOfNotifications > 0 ? View.VISIBLE : View.INVISIBLE);

        if (navigationFragment != null) {
            navigationFragment.setNumberOfNotifications(numberOfNotifications);
        }
    }

    @Override
    public void onServiceCallClicked(HPFragment fragment) {
        updateToolbar(fragment);
        enableOrientationEventListener(false);
    }

    @Override
    public void onAdvicePopupDisplayed() {
        onDialogDisplayed();
    }

    @Override
    public void onAdvicePopupDismissed() {
        onDialogDismissed();
    }

    @Override
    public void onDialogDisplayed() {
        hasDialog = true;
        enableOrientationEventListener(false);
    }

    @Override
    public void onDialogDismissed() {
        hasDialog = false;
        shouldEnableOrientation();
    }

    @Override
    public void setHasRtDevices(boolean hasRTDevices) {
        this.hasRTSupportedDevices = hasRTDevices;
        shouldEnableOrientation();
    }

    @Override
    public void scrollToAdvice(AdviceViewModel adviceViewModel) {
        String tabName = getString(R.string.fragment_insights_name_key);
        Fragment fragment = adapter.getFragment(tabName);
        if (fragment != null && fragment instanceof InsightsFragment) {
            ((InsightsFragment) fragment).scrollToAdvice(adviceViewModel);
        }

        int index = adapter.fragmentTitles.indexOf(tabName);
        if (index >= 0) {
            tabLayout.getTabAt(index).select();
        }
    }

    public DevicesFragment getDevicesFragment() {
        Fragment fragment = adapter.getFragment(getString(R.string.fragment_devices_name_key));
        if (fragment != null && fragment instanceof DevicesFragment) {
            return (DevicesFragment) fragment;
        }
        return null;
    }

    private void openReportTab(String kpiName) {
        String tabName = getString(R.string.fragment_reports_name_key);

        int index = adapter.fragmentTitles.indexOf(tabName);
        if (index > 0) {
            tabLayout.getTabAt(index).select();

            Fragment reportFragment = adapter.getItemByName(tabName);
            if (reportFragment != null && reportFragment instanceof ReportsFragment) {
                ((ReportsFragment) reportFragment).openReportFragment(kpiName);
            }
        }
    }

    private void openDevicesTab() {
        String tabName = getString(R.string.fragment_devices_name_key);
        int index = adapter.fragmentTitles.indexOf(tabName);
        if (index > 0) {
            tabLayout.getTabAt(index).select();
        }
    }

    private void openJobsTab(DeviceViewModel.JobType jobType) {
        String tabName = getString(R.string.fragment_jobs_key);
        int index = adapter.fragmentTitles.indexOf(tabName);
        if (index >= 0) {
            tabLayout.getTabAt(index).select();
            Fragment fragment = adapter.getFragment(tabName);
            if (fragment instanceof JobsTodayFragment) {
                ((JobsTodayFragment) fragment).focusOnPage(jobType);
            }
        }
    }

    @Override
    public void onBusinessUnitSelected(BusinessUnitViewModel businessUnitViewModel, boolean resetFilters) {

        if (businessUnitViewModel == null) {
            return;
        }

        //Reset old selected devices.
        if (resetFilters) {
            businessUnitViewModel.getFiltersViewModel().setSelectedDevices(null);
            updateFiltersView(businessUnitViewModel);
        }

        this.businessUnitViewModel = businessUnitViewModel;

        PrintOSPreferences.getInstance(this).saveSelectedBusinessUnit(businessUnitViewModel.getBusinessUnit());

        setFragmentName();

        if (adapter.getFragments() != null) {
            for (Fragment fragment : adapter.getFragments()) {
                if (fragment instanceof IMainFragment) {
                    IMainFragment mainFragment = (IMainFragment) fragment;
                    mainFragment.onBusinessUnitSelected(businessUnitViewModel);
                }
            }
        }

        if (navigationFragment != null) {
            navigationFragment.onBusinessUnitSelected(businessUnitViewModel);
        }


        //Show jobs in queue tabs for Indigo only, hide in all other divisions.
        setTabEnabled(getString(R.string.fragment_jobs_key),
                businessUnitViewModel.getBusinessUnit() == BusinessUnitEnum.INDIGO_PRESS);
        setTabEnabled(getString(R.string.fragment_insights_name_key),
                businessUnitViewModel.getBusinessUnit() == BusinessUnitEnum.INDIGO_PRESS);

        //Use different tab icon for Latex devices tab, all other divisions will use the default icon.
        Fragment devicesFragment = adapter.getFragment(getString(R.string.fragment_devices_name_key));
        int devicesFragmentIndex = adapter.getFragments().indexOf(devicesFragment);

        if (businessUnitViewModel.getBusinessUnit() == BusinessUnitEnum.LATEX_PRINTER) {
            adapter.updateTabIcon(devicesFragmentIndex, R.drawable.latex_devices, R.drawable.latex_devices_selected);
        } else {
            adapter.updateTabIcon(devicesFragmentIndex, R.drawable.devices, R.drawable.devices_selected);
        }

        adapter.resetTabsSelection(businessUnitViewModel.getBusinessUnit(), tabLayout);

        if (intentHasExtra(Constants.IntentExtras.MAIN_ACTIVITY_NOTIFICATION_EXTRA)) {
            NotificationEventEnum notificationEventEnum =
                    NotificationEventEnum.from(getIntentStringExtra(
                            Constants.IntentExtras.MAIN_ACTIVITY_NOTIFICATION_EXTRA));

            switch (notificationEventEnum) {
                case PERSONAL_ADVISOR:
                    moveToInsights();
                    scrollInsightsToPanel(Panel.PERSONAL_ADVISOR);
                    break;
                case DAILY_DIGEST:
                    scrollHomeToPanel(Panel.TODAY);
                    break;
                case NEW_PRINT_BEAT_REPORT:
                    scrollHomeToPanel(Panel.PERFORMANCE);
                    break;
                case SERVICE_CALL_UPDATES:
                    openServiceCallsTab();
                    break;
            }

            getIntent().removeExtra(Constants.IntentExtras.MAIN_ACTIVITY_NOTIFICATION_EXTRA);

        }

        if (intentHasExtra(Constants.IntentExtras.UNIVERSAL_LINK_SCREEN_KEY)) {

            String screenName = getIntentStringExtra(Constants.IntentExtras.UNIVERSAL_LINK_SCREEN_KEY);

            if (screenName.equals(Constants.IntentExtras.UNIVERSAL_LINK_SCREEN_INSIGHTS_TAB) &&
                    businessUnitViewModel.getBusinessUnit() == BusinessUnitEnum.INDIGO_PRESS) {
                moveToInsights();
            } else if (screenName.equals(Constants.IntentExtras.UNIVERSAL_LINK_SCREEN_SERVICE_CALLS_TAB)) {
                openServiceCallsTab();
            } else if (screenName.equals(Constants.IntentExtras.UNIVERSAL_LINK_SCREEN_NOTIFICATIONS)) {
                onNotificationIconClicked();
            } else {
                moveToHome();
            }

            getIntent().removeExtra(Constants.IntentExtras.UNIVERSAL_LINK_SCREEN_KEY);
        }

        TabLayout.Tab tab = tabLayout.getTabAt(tabLayout.getSelectedTabPosition());
        Fragment fragment = adapter.getItem(adapter.fragmentTitles.indexOf(tab.getText()));
        onTabSelected(fragment);
    }

    private void openServiceCallsTab() {
        moveToHome();
        String fragmentName = getString(R.string.fragment_home_name_key);
        Fragment fragment = adapter.getFragment(fragmentName);
        if (fragment != null && fragment instanceof HomeFragment) {
            HomeFragment home = (HomeFragment) fragment;
            home.openServiceCallFragmentWhenLoaded();

        }
    }

    private void scrollHomeToPanel(Panel panel) {
        String fragmentName = getString(R.string.fragment_home_name_key);
        Fragment fragment = adapter.getFragment(fragmentName);
        if (fragment != null && fragment instanceof HomeFragment) {
            HomeFragment home = (HomeFragment) fragment;

            String extra = null;
            if (intentHasExtra(Constants.IntentExtras.MAIN_ACTIVITY_TODAY_EXTRA)) {
                extra = getIntentStringExtra(Constants.IntentExtras.MAIN_ACTIVITY_TODAY_EXTRA);
            }
            home.setFocusablePanel(panel, extra);
        }
    }

    private void scrollInsightsToPanel(Panel panel) {
        String fragmentName = getString(R.string.fragment_insights_name_key);
        Fragment fragment = adapter.getFragment(fragmentName);
        if (fragment != null && fragment instanceof InsightsFragment) {
            ((InsightsFragment) fragment).setFocusablePanel(panel);
        }
    }

    private void updateFiltersView(BusinessUnitViewModel businessUnitViewModel) {

        if (intentHasExtra(Constants.IntentExtras.MAIN_ACTIVITY_FILTER_EXTRA)) {
            businessUnitViewModel.getFiltersViewModel().setSelectedSiteById(
                    getIntentStringExtra(Constants.IntentExtras.MAIN_ACTIVITY_FILTER_EXTRA));
            getIntent().removeExtra(Constants.IntentExtras.MAIN_ACTIVITY_FILTER_EXTRA);
        }

        if (filtersFragment == null || businessUnitViewModel != this.businessUnitViewModel) {

            toolbarFilterButton.setVisibility(isSingleDevice(businessUnitViewModel) ? View.GONE : View.VISIBLE);

            filtersFragment = FiltersFragment.newInstance(businessUnitViewModel.getFiltersViewModel(),
                    businessUnitViewModel.getBusinessUnit());
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.filters_fragment_container, filtersFragment)
                    .commit();
            this.filtersContainer.setVisibility(View.GONE);
        } else {

            toolbarFilterButton.setVisibility(isSingleDevice(businessUnitViewModel) ? View.GONE : View.VISIBLE);

        }

    }


    boolean isSingleDevice(BusinessUnitViewModel businessUnitViewModel) {

        return businessUnitViewModel == null || businessUnitViewModel.getFiltersViewModel().getDevices().size() == 1;
    }

    private void setTabEnabled(String tabName, boolean enable) {

        boolean isTabOpen = adapter.fragmentTitles.get(tabLayout.getSelectedTabPosition()).equals(tabName);
        if (isTabOpen && !enable) {
            moveToHome();
        }

        LinearLayout tabStrip = ((LinearLayout) tabLayout.getChildAt(0));
        for (int i = 0; i < tabStrip.getChildCount(); i++) {
            if (adapter.fragmentTitles.get(i).equals(tabName)) {
                tabStrip.getChildAt(i).setVisibility(enable ? View.VISIBLE : View.GONE);
            }
        }
    }

    private void setFragmentName() {

        if (businessUnitViewModel != null && businessUnitViewModel.getBusinessUnit().getBusinessUnitDisplayName() != null) {

            if (businessUnitViewModel.getFiltersViewModel().getSelectedSite() != null) {
                if (!businessUnitViewModel.getFiltersViewModel().getSelectedSite().isDefaultSite()) {
                    String title = businessUnitViewModel.getFiltersViewModel().getSelectedSite().getName();
                    toolbarTitle.setText(title);
                    toolbarTitle.setVisibility(View.VISIBLE);
                } else {
                    toolbarTitle.setVisibility(View.GONE);
                }
            }

        } else {
            toolbarTitle.setVisibility(View.GONE);
        }

    }

    @Override
    public void moveToHome() {
        String tabName = getString(R.string.fragment_home_name_key);
        int index = adapter.fragmentTitles.indexOf(tabName);
        if (index >= 0) {
            tabLayout.getTabAt(index).select();
        }
        AppseeSdk.getInstance(this).startScreen(AppseeSdk.SCREEN_HOME);
    }

    public void moveToInsights() {
        String tabName = getString(R.string.fragment_insights_name_key);
        int index = adapter.fragmentTitles.indexOf(tabName);
        if (index >= 0) {
            tabLayout.getTabAt(index).select();
        }
    }

    @Override
    public void onOrganizationSelected() {
        loadingView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onContextChanged(boolean isSuccessful, String selectedOrganizationID) {
        loadingView.setVisibility(View.GONE);

        if (intentHasExtra(Constants.IntentExtras.MAIN_ACTIVITY_ORGANIZATION_EXTRA)) {
            getIntent().removeExtra(Constants.IntentExtras.MAIN_ACTIVITY_ORGANIZATION_EXTRA);
            if (intentHasExtra(Constants.IntentExtras.MAIN_ACTIVITY_DIVISION_EXTRA)) {
                BusinessUnitEnum businessUnitEnum = BusinessUnitEnum.from(getIntentStringExtra(
                        Constants.IntentExtras.MAIN_ACTIVITY_DIVISION_EXTRA
                ));
                PrintOSPreferences.getInstance(this).saveSelectedBusinessUnit(businessUnitEnum);
                getIntent().removeExtra(Constants.IntentExtras.MAIN_ACTIVITY_DIVISION_EXTRA);
            }
            presenter.initView(MainActivity.this);
            return;
        }

        if (isSuccessful) {

            HPLogger.d(TAG, "Context changed.");
            this.selectedOrganizationID = selectedOrganizationID;
            IntercomSdk.getInstance(this).login();

            if (googlePlayServicesStatusCode == ConnectionResult.SUCCESS) {
                startRegistrationService(Constants.IntentExtras.NOTIFICATION_REGISTER_INTENT_ACTION);
            }

            resetActivity();
        }

        PrintOSAppWidgetProvider.updateAllWidgets(this);
    }

    public void onMenuItemSelected() {
        drawer.closeDrawer(Gravity.RIGHT);
    }

    @Override
    public void onContactHPClicked(int id) {

        Analytics.sendEvent(Analytics.MOBILE_PRINT_BEAT_CATEGORY,
                id == R.id.drawer_send_feedback_item ? Analytics.FEEDBACK_TO_HP_CLICKED_ACTION
                        : id == R.id.drawer_report_problem_item ? Analytics.REPORT_A_PROBLEM_CLICKED_ACTION
                        : Analytics.ASK_THE_EXPERT_CLICKED_ACTION);

        Intent intent = new Intent(this,
                id == R.id.drawer_send_feedback_item ? FeedbackActivity.class
                        : id == R.id.drawer_report_problem_item ? ReportAProblemActivity.class
                        : AskAQuestionActivity.class);

        startActivity(intent);
    }

    @Override
    public void onChangeOrganizationClicked() {
        final OrganizationDialog organizationDialog = OrganizationDialog.getInstance(organizations, selectedOrganizationID);
        organizationDialog.addOrganizationDialogCallback(new OrganizationDialog.OrganizationDialogCallback() {
            @Override
            public void onOrganizationsSelected(OrganizationViewModel selectedOrganization) {
                navigationFragment.onOrganizationSelected(selectedOrganization);
                //selectedOrganizationID = selectedOrganization.getOrganizationId();
                Analytics.sendEvent(Analytics.MOBILE_PRINT_BEAT_CATEGORY, Analytics.SWITCH_ACCOUNT_ACTION, selectedOrganization.getOrganizationName());
            }

            @Override
            public void onDialogDismissed() {
                hasDialog = false;
                shouldEnableOrientation();
            }
        });
        enableOrientationEventListener(false);
        hasDialog = true;
        organizationDialog.show(getFragmentManager(), DIALOG_TAG);
    }

    private void shouldEnableOrientation() {
        if (tabLayout != null && adapter != null && !hasDialog && isResumed) {
            TabLayout.Tab tab = tabLayout.getTabAt(tabLayout.getSelectedTabPosition());
            if (tab != null && adapter.fragmentTitles != null
                    && adapter.fragmentTitles.indexOf(tab.getText()) < adapter.getCount()) {
                Fragment fragment = adapter.getItem(adapter.fragmentTitles.indexOf(tab.getText()));
                if (fragment instanceof HomeFragment) {
                    if (!((HomeFragment) fragment).isDisplayingDetailsFragment()) {
                        enableOrientationEventListener(true);
                    }
                }
            }
        }
    }

    @Override
    public void onSettingMenuItemClicked() {
        startActivityForResult(new Intent(this, SettingsActivity.class), SETTINGS_REQUEST_CODE);
    }

    @Override
    public void onDivisionSwitchMenuItemClick() {
        List<String> divisions = new ArrayList<>();
        for (BusinessUnitEnum unit : PrintOSApplication.getBusinessUnits().keySet()) {
            divisions.add(unit.getName());
        }

        List<BusinessUnitViewModel> businessUnits = new ArrayList<>(PrintOSApplication.getBusinessUnits().values());

        Bundle bundle = new Bundle();
        bundle.putSerializable(DivisionSwitchActivity.DIVISIONS_KEY, (ArrayList<BusinessUnitViewModel>) businessUnits);

        Intent intent = new Intent(MainActivity.this, DivisionSwitchActivity.class);
        intent.putExtras(bundle);
        startActivityForResult(intent, SWITCH_DIVISION);
    }

    @Override
    public void onLogoutMenuItemClicked() {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);

        // set dialog message
        alertDialogBuilder
                .setMessage(getString(R.string.logout_confirmation_msg))
                .setCancelable(false)
                .setPositiveButton(getString(R.string.logout_yes), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        HPLogger.d(TAG, "Logout selected.");
                        performLogout();
                    }
                })
                .setNegativeButton(getString(R.string.logout_no), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        hasDialog = false;
                        dialog.cancel();
                        shouldEnableOrientation();
                    }
                });

        enableOrientationEventListener(false);
        AlertDialog alertDialog = alertDialogBuilder.create();
        hasDialog = true;
        alertDialog.show();
    }


    private void performLogout() {

        if (presenter == null) {
            presenter = new MainViewPresenter();
            presenter.attachView(this);
        }

        UserViewModel viewModel = PrintOSPreferences.getInstance(this).getUserInfo();
        if (viewModel != null) {
            Analytics.sendEvent(Analytics.MOBILE_PRINT_BEAT_CATEGORY, Analytics.LOGIN_EVENT_LOGOUT_ACTION,
                    viewModel.getUserId() + "|" + viewModel.getUserOrganization().getOrganizationId());
        } else {
            Analytics.sendEvent(Analytics.MOBILE_PRINT_BEAT_CATEGORY, Analytics.LOGIN_EVENT_LOGOUT_ACTION);
        }

        startRegistrationService(Constants.IntentExtras.NOTIFICATION_UNREGISTER_AND_CLEAR_CACHE_INTENT_ACTION);

        presenter.logout();
        PrintOSAppWidgetProvider.updateAllWidgets(this);

    }


    @Override
    public void onAboutMenuItemClicked() {
        Analytics.sendEvent(Analytics.MOBILE_PRINT_BEAT_CATEGORY, Analytics.ABOUT_CLICKED_ACTION);

        Intent intent = new Intent(this, AboutActivity.class);
        startActivity(intent);

    }

    @Override
    public void onPrivacyMenuItemClick() {
        Analytics.sendEvent(Analytics.MOBILE_PRINT_BEAT_CATEGORY, Analytics.OPEN_SOURCE_CLICKED_ACTION);

        Intent intent = new Intent(this, EulaActivity.class);
        Bundle bundle = new Bundle();
        bundle.putBoolean(Constants.IntentExtras.EULA_ACTIVITY_TO_ACCEPT_TERMS, false);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    public void onMenuItemClicked(int menuItemId) {

        if (tabLayout.getSelectedTabPosition() > 0) {
            TabLayout.Tab tab = tabLayout.getTabAt(tabLayout.getSelectedTabPosition());
            if (tab != null && tab.getText().equals(getString(R.string.fragment_contact_hp_name_key))) {
                if (menuItemId != R.id.drawer_ask_question_item && menuItemId != R.id.drawer_send_feedback_item
                        && menuItemId != R.id.drawer_report_problem_item) {
                    moveToHome();
                }
            }
        }

    }

    @Override
    public void onOrganizationsRetrieved(List<OrganizationViewModel> organizationViewModels, String selectedOrganizationID) {
        this.organizations = organizationViewModels;
        this.selectedOrganizationID = selectedOrganizationID;
        setIntentOrganizationIfAny();
        loginPresenter.getOrganizationDimension();
    }

    private void setIntentOrganizationIfAny() {
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (extras != null && extras.containsKey(Constants.IntentExtras.MAIN_ACTIVITY_ORGANIZATION_EXTRA)) {
            String organizationID = extras.getString(Constants.IntentExtras.MAIN_ACTIVITY_ORGANIZATION_EXTRA);
            if (organizationID != null) {
                for (int i = 0; i < (this.organizations == null ? 0 : this.organizations.size()); i++) {
                    if (organizationID.equalsIgnoreCase(organizations.get(i).getOrganizationId())) {
                        PrintOSPreferences.getInstance(this).clearOrganization();
                        navigationFragment.onOrganizationSelected(organizations.get(i));
                        selectedOrganizationID = organizations.get(i).getOrganizationId();
                        return;
                    }
                }

                //if organization not found then entity is corrupted
                onContextChanged(false, null);
            }
        }
    }

    @Override
    public void onNotificationButtonClicked() {
        onNotificationIconClicked();
    }

    @Override
    public void hideLoading() {
        loadingView.setVisibility(View.GONE);
    }

    @Override
    public void showLoading() {
        loadingView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onGettingBusinessUnitsCompleted(Map<BusinessUnitEnum, BusinessUnitViewModel> businessUnitViewModelMap) {

        PrintOSApplication.setBusinessUnits(businessUnitViewModelMap);
        navigationFragment.didFinishGettingBusinessUnits(businessUnitViewModelMap);

        BusinessUnitViewModel selectedBusinessUnit = null;

        if (businessUnitViewModelMap == null || businessUnitViewModelMap.size() == 0) {
            return;
        } else if (businessUnitViewModelMap.size() == 1) {
            selectedBusinessUnit = businessUnitViewModelMap.values().iterator().next();
        } else {

            BusinessUnitEnum businessUnitEnum = PrintOSPreferences.getInstance(this).getSelectedBusinessUnit();
            if (businessUnitEnum != BusinessUnitEnum.UNKNOWN) {

                if (businessUnitViewModelMap.containsKey(businessUnitEnum)) {
                    selectedBusinessUnit = businessUnitViewModelMap.get(businessUnitEnum);
                } else {
                    selectedBusinessUnit = businessUnitViewModelMap.get(new ArrayList<>(businessUnitViewModelMap.keySet()).get(0));
                }
            } else {
                selectedBusinessUnit = businessUnitViewModelMap.get(new ArrayList<>(businessUnitViewModelMap.keySet()).get(0));
            }
        }

        if (selectedBusinessUnit != null) {
            onBusinessUnitSelected(selectedBusinessUnit, true);
            IntercomSdk.getInstance(this).
                    sendUserChangeEquipmentEvent(selectedBusinessUnit.getBusinessUnit().getShortName());
            AppseeSdk.getInstance(this).sendEvent(AppseeSdk.EVENT_VIEW_BUSINESS_UNIT, selectedBusinessUnit.getBusinessUnit().getShortName());
        }


    }

    private void onMainError(String msg, boolean shouldTriggerSignOut) {

        for (Fragment fragment : adapter.getFragments()) {
            if (fragment instanceof IMainFragment) {
                ((IMainFragment) fragment).onMainError(msg, shouldTriggerSignOut);
            }
        }

        this.businessUnitViewModel = null;
        if (isFilterDisplayed) {
            hideFilters();
        }
        this.filtersFragment = null;
        this.navigationFragment.onMainError(msg, shouldTriggerSignOut);

        moveToHome();
    }

    @Override
    public void onEmptyBusinessUnits() {
        HPLogger.d(TAG, "Getting empty devices list");
        //TODO: Handle empty devices.
    }

    @Override
    public void onApplicationLogout() {

        Navigator.openLoginActivity(this);
        PrintOSPreferences.getInstance(this).setLoggedIn(false);
        PrintOSPreferences.getInstance(this).clearUserInfo();

        PrintOSApplication.initSdks();

        PrintOSPreferences.getInstance(this).setHasIndigoDivision(false);

        finish();
    }

    public void onError(APIException exception, String tag) {
        if (!TAG.equals(tag)) {
            return;
        }

        switch (exception.getKind()) {
            case NETWORK:
                onMainError(getString(R.string.error_network_error), false);
                break;
            case SERVICE_TIME_OUT:
                onMainError(getString(R.string.error_service_time_out), false);
                break;
            case INTERNAL_SERVER_ERROR:
                onMainError(getString(R.string.error_internal_server_error), false);
                break;
            case URL_NOT_FOUND_ERROR:
                onMainError(getString(R.string.url_not_found_error), false);
                break;
            case UNAUTHORIZED:
                onMainError(getString(R.string.unauthorized_error), true);
                break;
            case FILE_NOT_FOUND:
                onMainError(getString(R.string.file_not_found), true);
                break;
            default:
                onMainError(getString(R.string.unexpected_error), false);
                break;
        }
    }

    @Override
    public void onNoDevicesAttached() {
        onMainError(getString(R.string.error_no_devices), false);
    }

    @Override
    public void onBeatCoinDataRetrieved(BeatCoinsViewModel beatCoinsViewModel) {
        if (beatCoinsViewModel == null) {
            getIntent().removeExtra(Constants.IntentExtras.MAIN_ACTIVITY_BEAT_COIN_EXTRA);
            DialogManager.displayDialogs(this);
            return;
        }

        hasDialog = true;


        Analytics.sendEvent(Analytics.SHOW_BEAT_COINS_ACTION);
        AppseeSdk.getInstance(this).sendEvent(AppseeSdk.EVENT_SHOW_BEAT_COINS);

        HPLogger.d(TAG, "show Beat coins popup.");

        BeatCoinsPopup.getInstance(new BeatCoinsPopup.BeatCoinPopupCallback() {
            @Override
            public void onDialogDismissed() {
                hasDialog = false;
                shouldEnableOrientation();
            }
        }, beatCoinsViewModel).show(getSupportFragmentManager(), "BEAT_COIN_POPUP");
    }

    @Override
    public void onDevicesDetailedClicked(HPFragment hpFragment) {
        updateToolbar(hpFragment);
    }

    @Override
    public void onDetailsLevelChanged(HPFragment hpFragment) {
        updateToolbarUsingExtras(hpFragment);
    }

    @Override
    public void onDevicesDetailedClosed(HPFragment hpFragment) {
        if (tabLayout.getTabAt(tabLayout.getSelectedTabPosition()).getText().equals(getString(R.string.fragment_devices_name_key))) {
            Analytics.sendEvent(Analytics.MOBILE_PRINT_BEAT_CATEGORY, Analytics.MACHINE_VIEW_ACTION);
            updateToolbar(hpFragment);
        }
    }

    @Override
    public void moveFromDevicesToHome() {
        moveToHome();
    }

    @Override
    public void onGoToPersonalAdvisorClicked(AdviceViewModel model) {
        scrollToAdvice(model);
    }


    @Override
    public void onCancelFiltersSelected() {

        hideFilters();
    }

    @Override
    public void onApplyFiltersSelected(List<FilterItem> selectedDevices) {

        hideFilters();
        if (selectedDevices == null || businessUnitViewModel == null ||
                businessUnitViewModel.getFiltersViewModel() == null) {
            return;
        }

        this.businessUnitViewModel.getFiltersViewModel().setSelectedDevices(selectedDevices);

        setFragmentName();

        for (Fragment fragment : adapter.getFragments()) {
            if (fragment instanceof IMainFragment) {
                IMainFragment mainFragment = (IMainFragment) fragment;
                mainFragment.onFiltersChanged(businessUnitViewModel);
            }
        }

        PersonalAdvisorFactory.getInstance().loadData(businessUnitViewModel);
    }

    @Override
    public void requestFilterHiding() {
        hideFilters();
    }

    @Override
    public void onInitializeJobsView(JobsTodayFragment fragment) {

    }

    @Override
    public void onJobsTodayBackPressed() {
        moveToHome();
    }

    @Override
    public void onTryAgainClicked() {
        String tabName = getString(R.string.fragment_home_name_key);
        Fragment fragment = adapter.getFragment(tabName);
        if (fragment instanceof HomeFragment) {
            ((HomeFragment) fragment).tryAgain();
        }

    }

    @Override
    public void onInsightsFragmentBackPress() {
        moveToHome();
    }

    @Override
    public void onInsightsBusinessUnitSet() {
        if (selectedTab != null && adapter != null && adapter.fragmentTitles != null) {

            String insightsTabName = getString(R.string.fragment_insights_name_key);
            String selectedTabName = adapter.fragmentTitles.get(selectedTab.getPosition());

            if (insightsTabName.equals(selectedTabName)) {
                ((InsightsFragment) adapter.fragmentHashMap.get(insightsTabName)).loadScreen();
            }
        }
    }

    @Override
    public void moveToHomeFromTab(String tabKey) {
        if (tabKey != null && selectedTab != null && adapter != null && adapter.fragmentTitles != null) {
            String selectedTabName = adapter.fragmentTitles.get(selectedTab.getPosition());
            if (tabKey.equals(selectedTabName)) {
                moveToHome();
            }
        }
    }

    @Override
    public void onReportBusinessUnitSet() {
        if (selectedTab != null && adapter != null && adapter.fragmentTitles != null) {

            String reportTabName = getString(R.string.fragment_reports_name_key);
            String selectedTabName = adapter.fragmentTitles.get(selectedTab.getPosition());

            if (reportTabName.equals(selectedTabName)) {
                ((ReportsFragment) adapter.fragmentHashMap.get(reportTabName)).loadScreen();
            }
        }
    }

    /**
     * An adapter responsible for populating the pager dynamically.
     *
     * @author Osama Taha
     */
    private static class MainPagerAdapter extends FragmentStatePagerAdapter {

        private final Map<String, Fragment> fragmentHashMap = new HashMap<>();
        private final List<String> fragmentTitles = new ArrayList<>();
        private final List<Integer> fragmentIcons = new ArrayList<>();
        private final List<Integer> fragmentIconsSelected = new ArrayList<>();
        private final Context context;

        public MainPagerAdapter(FragmentManager fm, Context context) {
            super(fm);
            this.context = context;
        }

        public void addFragment(Fragment fragment, String title, Integer iconResourceId, Integer iconResourceIdSelected) {
            fragmentHashMap.put(title, fragment);
            fragmentTitles.add(title);
            fragmentIcons.add(iconResourceId);
            fragmentIconsSelected.add(iconResourceIdSelected);
        }

        public void updateTabIcon(int position, Integer iconResourceId, Integer iconResourceIdSelected) {
            fragmentIcons.set(position, iconResourceId);
            fragmentIconsSelected.set(position, iconResourceIdSelected);
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentHashMap.get(fragmentTitles.get(position));
        }

        public Fragment getItemByName(String keyName) {
            if (fragmentHashMap.containsKey(keyName)) {
                return fragmentHashMap.get(keyName);
            }
            return null;
        }

        @Override
        public boolean isViewFromObject(View view, Object fragment) {
            return ((Fragment) fragment).getView() == view;
        }

        /**
         * Here we can safely save a reference to the created
         * Fragment, no matter where it came from (either getItem() or
         * FragmentManager).
         */
        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            Fragment fragment = (Fragment) super.instantiateItem(container, position);

            String name = fragmentTitles.get(position);
            if (fragmentHashMap.containsKey(name)) {
                fragmentHashMap.remove(name);
            }
            fragmentHashMap.put(name, fragment);
            // save the appropriate reference depending on position
            return fragment;
        }

        @Override
        public int getCount() {
            return fragmentTitles.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return fragmentTitles.get(position);
        }

        public List<Fragment> getFragments() {
            return new ArrayList<>(fragmentHashMap.values());
        }

        public Fragment getFragment(String name) {
            if (name != null && fragmentHashMap.containsKey(name)) {
                return fragmentHashMap.get(name);
            }
            return null;
        }

        public View getTabView(int position, boolean isSelected) {
            // Given you have a custom layout in `res/layout/custom_tab.xml` an ImageView
            View view = LayoutInflater.from(context).inflate(R.layout.custom_tab, null);

            ImageView img = (ImageView) view.findViewById(R.id.tab_icon);
            if (isSelected) {
                img.setImageResource(fragmentIconsSelected.get(position));
            } else {
                img.setImageResource(fragmentIcons.get(position));
            }

            ImageView newBadge = (ImageView) view.findViewById(R.id.new_badge);
            newBadge.setVisibility(View.GONE);
            if (isInsightTab(position)) {
                if (VersionCheckService.getVersionName().equals(PrintOSPreferences.getInstance(context)
                        .getInsightsFirstOccuringVersion(VersionCheckService.getVersionName()))) {
                    if (PrintOSPreferences.getInstance(context).shouldShowInsightsNewBadge(true)) {
                        newBadge.setVisibility(View.VISIBLE);
                    }
                }
            }

            return view;
        }

        private boolean isInsightTab(int position) {
            String fragmentName = fragmentTitles.get(position);
            fragmentName = fragmentName == null ? "" : fragmentName;
            return context.getString(R.string.fragment_insights_name_key).equalsIgnoreCase(fragmentName);
        }

        public void setSelected(BusinessUnitEnum businessUnitEnum, TabLayout.Tab tab, int position) {
            View view = tab.getCustomView();
            ImageView img = (ImageView) view.findViewById(R.id.tab_icon);
            img.setImageResource(businessUnitEnum.getBottomTabIcon(true, position));


            if (isInsightTab(position)) {
                PrintOSPreferences.getInstance(context).setShowInsightsNewBadge(false);
                ImageView newBadge = (ImageView) view.findViewById(R.id.new_badge);
                newBadge.setVisibility(View.GONE);
            }
        }

        public void setUnselected(BusinessUnitEnum businessUnitEnum, TabLayout.Tab tab, int position) {
            View view = tab.getCustomView();
            ImageView img = (ImageView) view.findViewById(R.id.tab_icon);
            img.setImageResource(businessUnitEnum.getBottomTabIcon(false, position));
        }

        public void resetTabsSelection(BusinessUnitEnum businessUnitEnum, TabLayout tabLayout) {

            for (int tabIndex = 0; tabIndex < tabLayout.getTabCount(); tabIndex++) {
                if (tabIndex == tabLayout.getSelectedTabPosition()) {
                    setSelected(businessUnitEnum, tabLayout.getTabAt(tabIndex), tabIndex);
                } else {
                    setUnselected(businessUnitEnum, tabLayout.getTabAt(tabIndex), tabIndex);
                }
            }
        }

    }

    private void registerReceivers() {

        if (!isReceiverRegistered) {

            LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                    new IntentFilter(Constants.IntentExtras.NOTIFICATION_REGISTRATION_COMPLETED_INTENT_ACTION));

            registerReceiver(mNotificationBroadcastReceiver
                    , new IntentFilter(Constants.IntentExtras.NOTIFICATION_RECEIVED_ACTION));

            isReceiverRegistered = true;
        }
    }

    private void unregisterReceivers() {

        try {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);

            unregisterReceiver(mNotificationBroadcastReceiver);
        } catch (Exception e) {
            HPLogger.e(TAG, "Error in unregister receivers ", e);
            //Try/catch to avoid any unexpected issue while un registering the receivers.
        }


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (presenter != null) {
            presenter.detachView();
        }
        unregisterReceivers();
        enableOrientationEventListener(false);
    }

    private boolean intentHasExtra(String key) {
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        return bundle != null && key != null && bundle.containsKey(key);
    }

    private String getIntentStringExtra(String key) {
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        if (bundle != null && key != null && bundle.containsKey(key)) {
            return bundle.getString(key);
        }
        return "";
    }

    private int getIntentIntExtra(String key) {
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        if (bundle != null && key != null && bundle.containsKey(key)) {
            return bundle.getInt(key);
        }
        return -1;
    }

    private void updateBadge() {
        if (adapter != null) {
            String fragmentName = getString(R.string.fragment_home_name_key);
            Fragment fragment = adapter.getFragment(fragmentName);
            if (fragment != null && fragment instanceof HomeFragment) {
                HomeFragment home = (HomeFragment) fragment;
                home.updateNotificationBadge();
            }
        }
    }

    private void enableOrientationEventListener(boolean enable) {
        if (orientationEventListener != null) {
            if (businessUnitViewModel != null && businessUnitViewModel.getBusinessUnit().isHasHistogram()) {
                HPLogger.d(TAG, "enableOrientationEventListener(" + enable + ")");
                if (enable) {
                    orientationEventListener.enable();
                } else {
                    orientationEventListener.disable();
                }
            } else {
                HPLogger.d(TAG, "enableOrientationEventListener(false)");
                orientationEventListener.disable();
            }
        }
    }

    public class NotificationBroadCastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            updateBadge();
        }
    }
}
