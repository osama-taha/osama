package com.hp.printosmobile.data.remote.models;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Osama Taha on 10/4/16.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ShiftData {

    @JsonProperty("shiftMetaDataId")
    private String shiftMetaDataId;
    @JsonProperty("startTime")
    private String startTime;
    @JsonProperty("shiftName")
    private String shiftName;
    @JsonProperty("startDay")
    private int startDay;
    @JsonProperty("endDay")
    private int endDay;
    @JsonProperty("startHour")
    private int startHour;
    @JsonProperty("endHour")
    private int endHour;
    @JsonProperty("presentedDay")
    private int presentedDay;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    /**
     * @return The shiftMetaDataId
     */
    @JsonProperty("shiftMetaDataId")
    public String getShiftMetaDataId() {
        return shiftMetaDataId;
    }

    /**
     * @param shiftMetaDataId The shiftMetaDataId
     */
    @JsonProperty("shiftMetaDataId")
    public void setShiftMetaDataId(String shiftMetaDataId) {
        this.shiftMetaDataId = shiftMetaDataId;
    }

    /**
     * @return The startTime
     */
    @JsonProperty("startTime")
    public String getStartTime() {
        return startTime;
    }

    /**
     * @param startTime The startTime
     */
    @JsonProperty("startTime")
    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    /**
     * @return The shiftName
     */
    @JsonProperty("shiftName")
    public String getShiftName() {
        return shiftName;
    }

    /**
     * @param shiftName The shiftName
     */
    @JsonProperty("shiftName")
    public void setShiftName(String shiftName) {
        this.shiftName = shiftName;
    }

    /**
     * @return The startDay
     */
    @JsonProperty("startDay")
    public int getStartDay() {
        return startDay;
    }

    /**
     * @param startDay The startDay
     */
    @JsonProperty("startDay")
    public void setStartDay(int startDay) {
        this.startDay = startDay;
    }

    /**
     * @return The endDay
     */
    @JsonProperty("endDay")
    public int getEndDay() {
        return endDay;
    }

    /**
     * @param endDay The endDay
     */
    @JsonProperty("endDay")
    public void setEndDay(int endDay) {
        this.endDay = endDay;
    }

    /**
     * @return The startHour
     */
    @JsonProperty("startHour")
    public int getStartHour() {
        return startHour;
    }

    /**
     * @param startHour The startHour
     */
    @JsonProperty("startHour")
    public void setStartHour(int startHour) {
        this.startHour = startHour;
    }

    /**
     * @return The endHour
     */
    @JsonProperty("endHour")
    public int getEndHour() {
        return endHour;
    }

    /**
     * @param endHour The endHour
     */
    @JsonProperty("endHour")
    public void setEndHour(int endHour) {
        this.endHour = endHour;
    }

    /**
     * @return The presentedDay
     */
    @JsonProperty("presentedDay")
    public int getPresentedDay() {
        return presentedDay;
    }

    /**
     * @param presentedDay The presentedDay
     */
    @JsonProperty("presentedDay")
    public void setPresentedDay(int presentedDay) {
        this.presentedDay = presentedDay;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
