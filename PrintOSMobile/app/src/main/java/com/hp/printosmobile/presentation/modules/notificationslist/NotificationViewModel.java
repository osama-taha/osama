package com.hp.printosmobile.presentation.modules.notificationslist;

import com.hp.printosmobile.presentation.modules.settings.SubscriptionEvent;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;

/**
 * Created by Anwar Asbah on 1/23/2017.
 */

public class NotificationViewModel implements Serializable {

    public static Comparator<NotificationViewModel> dateComparator = new Comparator<NotificationViewModel>() {
        @Override
        public int compare(NotificationViewModel lhs, NotificationViewModel rhs) {
            Date lhsDate = lhs.getSentDate();
            Date rhsDate = rhs.getSentDate();
            if (lhsDate == null || rhsDate == null) {
                return lhsDate == null ? (rhsDate == null ? 0 : -1) : 1;
            }

            return -1 * lhs.getSentDate().compareTo(rhs.getSentDate());
        }
    };

    public static Comparator<NotificationViewModel> dateAndAcknowledgeComparator = new Comparator<NotificationViewModel>() {
        @Override
        public int compare(NotificationViewModel lhs, NotificationViewModel rhs) {
            Boolean lhsAcknowledge = lhs.isRead();
            Boolean rhsAcknowledge = rhs.isRead();
            if (lhsAcknowledge && rhsAcknowledge) {
                return dateComparator.compare(lhs, rhs);
            } else if (lhsAcknowledge) {
                return 1;
            } else if (rhsAcknowledge) {
                return -1;
            } else {
                return dateComparator.compare(lhs, rhs);
            }
        }
    };

    private int notificationID;
    private int userEventID;
    private SubscriptionEvent.NotificationEventEnum notificationEventEnum;
    private String notificationBody;
    private Date sentDate;
    private boolean isRead;
    private String entity;
    private String goLink;
    private String linkLabel;

    public SubscriptionEvent.NotificationEventEnum getNotificationEventEnum() {
        return notificationEventEnum;
    }

    public void setNotificationEventEnum(SubscriptionEvent.NotificationEventEnum notificationEventEnum) {
        this.notificationEventEnum = notificationEventEnum;
    }

    public String getNotificationBody() {
        return notificationBody;
    }

    public void setNotificationBody(String notificationBody) {
        this.notificationBody = notificationBody;
    }

    public Date getSentDate() {
        return sentDate;
    }

    public void setSentDate(Date sentDate) {
        this.sentDate = sentDate;
    }

    public boolean isRead() {
        return isRead;
    }

    public void setRead(boolean read) {
        isRead = read;
    }

    public int getNotificationID() {
        return notificationID;
    }

    public void setNotificationID(int notificationID) {
        this.notificationID = notificationID;
    }

    public int getUserEventID() {
        return userEventID;
    }

    public void setUserEventID(int userEventID) {
        this.userEventID = userEventID;
    }

    public String getEntity() {
        return entity;
    }

    public void setEntity(String entity) {
        this.entity = entity;
    }

    public String getGoLink() {
        return goLink;
    }

    public void setGoLink(String goLink) {
        this.goLink = goLink;
    }

    public void setLinkLabel(String linkLabel) {
        this.linkLabel = linkLabel;
    }

    public String getLinkLabel() {
        return linkLabel;
    }
}
