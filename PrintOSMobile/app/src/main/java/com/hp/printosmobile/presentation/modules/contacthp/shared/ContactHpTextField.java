package com.hp.printosmobile.presentation.modules.contacthp.shared;

import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Filterable;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.hp.printosmobile.R;
import com.hp.printosmobilelib.ui.widgets.HPAutoCompleteEditText;

import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * @author Anwar Asbah Create on 10/11/2016
 */
public class ContactHpTextField extends FrameLayout {

    private static final float DEFAULT_MENU_ID = 0;
    private static final java.lang.String KEY_LOCAL = "en";

    @Bind(R.id.edit_text)
    HPAutoCompleteEditText autoCompleteTextView;
    @Bind(R.id.auto_completion_button)
    View autoCompletionButton;
    @Bind(R.id.underline)
    View underLine;
    @Bind(R.id.counter_text_view)
    TextView counterTextView;

    private String hint;
    private boolean isAutoComplete;
    private int lineNumber;
    private int maxNumberOfChars;
    private int autoCompleteMenuId;
    private int textViewBorder;
    private boolean hasUnderline;
    private boolean isSingleLine;

    private String[] keys;
    private String[] list;

    public ContactHpTextField(Context context) {
        this(context, null);
        init();
    }

    public ContactHpTextField(Context context, AttributeSet attrs) {
        super(context, attrs);
        applyStyle(context.obtainStyledAttributes(attrs, R.styleable.ContactHpTextField, 0, 0));
        init();
    }

    public ContactHpTextField(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        applyStyle(context.obtainStyledAttributes(attrs, R.styleable.ContactHpTextField, defStyle, 0));
        init();
    }

    private void applyStyle(TypedArray typedArray) {
        try {
            isAutoComplete = typedArray.getBoolean(R.styleable.ContactHpTextField_contactHpAutoComplete, false);
            hint = typedArray.getString(R.styleable.ContactHpTextField_contactHpHint);
            lineNumber = typedArray.getInteger(R.styleable.ContactHpTextField_lineNumber, 1);
            maxNumberOfChars = typedArray.getInteger(R.styleable.ContactHpTextField_numberOfChar, 1);
            autoCompleteMenuId = typedArray.getResourceId(R.styleable.ContactHpTextField_contactHpAutoCompletionList, 0);
            textViewBorder = typedArray.getResourceId(R.styleable.ContactHpTextField_contactHpTextViewBoarder, 0);
            hasUnderline = typedArray.getBoolean(R.styleable.ContactHpTextField_showUnderLine, false);
            isSingleLine = typedArray.getBoolean(R.styleable.ContactHpTextField_isSingleLine, false);
        } finally {
            typedArray.recycle();
        }
    }

    public void init() {
        View view = inflate(getContext(), R.layout.contact_hp_edit_field, this);
        ButterKnife.bind(this, view);

        autoCompletionButton.setVisibility(isAutoComplete ? VISIBLE : GONE);
        underLine.setVisibility(hasUnderline ? VISIBLE : GONE);
        if (hint != null) {
            autoCompleteTextView.setHint(hint);
        }

        autoCompleteTextView.setSingleLine(isSingleLine);
        autoCompleteTextView.setLines(lineNumber);
        autoCompleteTextView.setBackgroundResource(textViewBorder);

        final InputFilter[] fArray = new InputFilter[1];
        fArray[0] = new InputFilter.LengthFilter(maxNumberOfChars);
        autoCompleteTextView.setFilters(fArray);

        if (autoCompleteMenuId != DEFAULT_MENU_ID) {

            getKeys();
            list = getResources().getStringArray(autoCompleteMenuId);

            final ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), R.layout.contact_hp_auto_complete_text_view, list);
            autoCompleteTextView.setAdapter(adapter);
            autoCompleteTextView.setThreshold(0);

            autoCompletionButton.setOnClickListener(new OnClickListener() {
                boolean isShowing = false;

                @Override
                public void onClick(View v) {
                    ((Filterable) autoCompleteTextView.getAdapter()).getFilter().filter("");

                    if (autoCompleteTextView.isPopupShowing()) {
                        autoCompleteTextView.requestLayout();
                        autoCompleteTextView.dismissDropDown();
                        isShowing = false;
                        return;
                    } else {
                        if (isShowing) {
                            autoCompleteTextView.requestLayout();
                            autoCompleteTextView.dismissDropDown();
                        } else {

                            autoCompleteTextView.requestLayout();
                            autoCompleteTextView.showDropDown();
                        }
                        isShowing = !isShowing;
                    }
                }
            });
        }

        counterTextView.setText(getContext().getString(R.string.counter_string, 0, maxNumberOfChars));
        autoCompleteTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                counterTextView.setText(getContext().getString(R.string.counter_string, s.length(), maxNumberOfChars));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public void getKeys(){
        Resources currentResources = getResources();
        AssetManager assets = currentResources.getAssets();
        DisplayMetrics metrics = currentResources.getDisplayMetrics();
        Configuration config = new Configuration(
                currentResources.getConfiguration());
        config.locale = Locale.ENGLISH;
        Resources resources = new Resources(assets, metrics, config);

        keys = resources.getStringArray(autoCompleteMenuId);

        new Resources(assets, metrics, currentResources.getConfiguration());
    }

    public void setTextFieldRightDrawable (Drawable drawable){
        autoCompleteTextView.setDrawableRight(drawable);
    }

    public String getText() {
        String input =  autoCompleteTextView.getText().toString().trim();
        if(list != null && input != null) {
            for (int i = 0; i < list.length; i++) {
                if(list[i].equalsIgnoreCase(input) && i < keys.length){
                    input = keys[i];
                    break;
                }
            }
        }

        return input;
    }

    public void setOnEditorActionListener(TextView.OnEditorActionListener actionListener) {
        autoCompleteTextView.setOnEditorActionListener(actionListener);
    }

    public AutoCompleteTextView getAutoCompleteTextView() {
        return autoCompleteTextView;
    }

    public void setEnabled(boolean isEnabled) {
        autoCompleteTextView.setEnabled(isEnabled);
    }

}