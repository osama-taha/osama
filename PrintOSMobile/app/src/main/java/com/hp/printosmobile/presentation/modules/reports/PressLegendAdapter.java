package com.hp.printosmobile.presentation.modules.reports;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hp.printosmobile.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Anwar Asbah on 5/4/2016.
 */
public class PressLegendAdapter extends RecyclerView.Adapter<PressLegendAdapter.PressLegendHolder> {

    private static final float[] CORNER_RADII = new float[]{100, 100, 100, 100, 100, 100, 100, 100};
    public Context context;
    public List<ReportKpiViewModel> reportPerPressList;

    public PressLegendAdapter(Context context, Map<String, ReportKpiViewModel> reportPerPressList) {
        this.context = context;
        this.reportPerPressList = new ArrayList<>();

        //it is necessary to copy all presses to new array as not to alter the kpi
        for (String pressName : reportPerPressList.keySet()) {
            this.reportPerPressList.add(reportPerPressList.get(pressName));
        }
    }

    @Override
    public PressLegendHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View printerView = LayoutInflater.from(context).inflate(R.layout.report_press_legend, null);
        PressLegendHolder viewHolder = new PressLegendHolder(printerView);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(PressLegendHolder holder, int position) {
        ReportKpiViewModel press = reportPerPressList.get(position);
        holder.pressName.setText(press.getPressName());
        holder.color.setImageDrawable(getLegendColorDrawable(press.getColor()));
    }

    @Override
    public int getItemCount() {
        return reportPerPressList == null ? 0 : reportPerPressList.size();
    }

    private Drawable getLegendColorDrawable(int color) {
        GradientDrawable shape = new GradientDrawable();
        shape.setShape(GradientDrawable.RECTANGLE);
        shape.setCornerRadii(CORNER_RADII);
        shape.setColor(color);
        return shape;
    }

    public void setPresses(Map<String, ReportKpiViewModel> presses) {
        if (this.reportPerPressList == null) {
            this.reportPerPressList = new ArrayList<>();
        }

        this.reportPerPressList.clear();

        for (String pressName : presses.keySet()) {
            this.reportPerPressList.add(presses.get(pressName));
        }
        notifyDataSetChanged();
    }

    public void clear() {
        this.reportPerPressList.clear();
        notifyDataSetChanged();
    }

    class PressLegendHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.press_name)
        TextView pressName;
        @Bind(R.id.legend_color)
        ImageView color;

        public PressLegendHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
