package com.hp.printosmobile.presentation.modules.reports;

import android.graphics.Typeface;
import android.graphics.drawable.Drawable;

import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.hp.printosmobile.data.remote.models.ReportData;
import com.hp.printosmobile.presentation.modules.main.ReportChartTypeEnum;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A model holds information and ui customizations for a such KPI that is used to draw a line chart.
 * <p/>
 *
 * @Author: Osama Taha
 * Created on 6/8/2015.
 */
public class ReportKpiViewModel implements Serializable {

    /**
     * Representing the KPI name.
     */
    private String name;
    /**
     * Representing the KPI values on OVERALL mode.
     */
    private List<Entry> overallValues;
    /**
     * Representing the KPI values on KPI mode.
     */
    private List<Entry> kpiValues;
    /**
     * Representing the KPI accumulative valued on the OVERALL mode.
     */
    private List<Entry> accumulativeValues;
    /**
     * Representing the parent KPI.
     */
    private ReportKpiViewModel parentKpi;
    /**
     * A list representing the sub KPIs
     */
    private List<ReportKpiViewModel> subKPIs;
    /**
     * A list of reportPerPressList
     */
    private Map<String, ReportKpiViewModel> reportPerPressList;
    /**
     * A list for bar entries in case it is a bar chart
     */
    private List<BarEntry> barEntries;
    /**
     * The color that is used for filling the line surface
     */
    private int color;
    /**
     * If true, the data will also be drawn filled
     */
    private boolean fillArea = true;
    private boolean activeAlone;
    /**
     * Representing the color that is used for the circles
     */
    private int circleColor;
    /**
     * The width of the drawn data lines
     */
    private float lineWidth;
    private int highLightColor;
    /**
     * The radius of the circle-shaped value indicators
     */
    private float circleSize = 4f;
    /**
     * If true, drawing circles is enabled
     */
    private boolean drawCircle;
    /**
     * Set this to true to allow drawing a hole in each data circle.
     */
    private boolean drawCircleHole;
    /**
     * If true, y-values are drawn on the chart
     */
    private boolean drawValues;
    /**
     * The color used for legend-image.
     */
    private Drawable iconUp;
    private Drawable iconDown;
    /**
     * The color used for circle hole.
     */
    private int circleHoleColor;
    /**
     * The color used for the value-text
     */
    private int valueTextColor;
    /**
     * The size of the value-text labels
     */
    private float valueTextSize = 10;
    /**
     * Representing values that are used for building the x-axis.
     */
    private List<String> xAxisValues;
    /**
     * The typeface used for the value text
     */
    private Typeface valueTypeFace;
    private List<ReportData.UnitEvent.Event> unitEvents;
    private String localizedName;
    private ReportView reportView = ReportView.LINE;
    private Boolean isAggregate = false;
    private int weekPoints;
    private String pressName;
    private String yAxisLabel;
    private ReportChartTypeEnum reportChartTypeEnum;

    public ReportChartTypeEnum getReportChartTypeEnum() {
        return reportChartTypeEnum;
    }

    public void setReportChartTypeEnum(ReportChartTypeEnum reportChartTypeEnum) {
        this.reportChartTypeEnum = reportChartTypeEnum;
    }

    public int getWeekPoints() {
        return weekPoints;
    }

    public void setWeekPoints(int weekPoints) {
        this.weekPoints = weekPoints;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Entry> getOverallValues() {
        return overallValues;
    }

    public void setOverallValues(List<Entry> overallValues) {
        this.overallValues = overallValues;
    }

    public List<Entry> getKpiValues() {
        return kpiValues;
    }

    public void setKpiValues(List<Entry> kpiValues) {
        this.kpiValues = kpiValues;
    }

    public List<Entry> getAccumulativeValues() {
        return accumulativeValues;
    }

    public void setAccumulativeValues(List<Entry> accumulativeValues) {
        this.accumulativeValues = accumulativeValues;
    }

    public ReportKpiViewModel getParentKpi() {
        return parentKpi;
    }

    public void setParentKpi(ReportKpiViewModel parentKpi) {
        this.parentKpi = parentKpi;
    }

    public List<ReportKpiViewModel> getSubKPIs() {
        return subKPIs;
    }

    public void setSubKPIs(List<ReportKpiViewModel> subKPIs) {
        this.subKPIs = subKPIs;
    }

    public boolean hasSubKpis() {
        return subKPIs != null && subKPIs.size() > 0;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public boolean isFillArea() {
        return fillArea;
    }

    public void setFillArea(boolean fillArea) {
        this.fillArea = fillArea;
    }

    public int getCircleColor() {
        return circleColor;
    }

    public void setCircleColor(int circleColor) {
        this.circleColor = circleColor;
    }

    public float getLineWidth() {
        return lineWidth;
    }

    public void setLineWidth(float lineWidth) {
        this.lineWidth = lineWidth;
    }

    public int getHighLightColor() {
        return highLightColor;
    }

    public void setHighLightColor(int highLightColor) {
        this.highLightColor = highLightColor;
    }

    public float getCircleSize() {
        return circleSize;
    }

    public void setCircleSize(float circleSize) {
        this.circleSize = circleSize;
    }

    public boolean isDrawCircle() {
        return drawCircle;
    }

    public void setDrawCircle(boolean drawCircle) {
        this.drawCircle = drawCircle;
    }

    public boolean isDrawCircleHole() {
        return drawCircleHole;
    }

    public void setDrawCircleHole(boolean drawCircleHole) {
        this.drawCircleHole = drawCircleHole;
    }

    public boolean isDrawValues() {
        return drawValues;
    }

    public void setDrawValues(boolean drawValues) {
        this.drawValues = drawValues;
    }

    public Drawable getIconUp() {
        return iconUp;
    }

    public void setIconUp(Drawable icon) {
        this.iconUp = icon;
    }

    public Drawable getIconDown() {
        return iconDown;
    }

    public void setIconDown(Drawable icon) {
        this.iconDown = icon;
    }

    public int getCircleHoleColor() {
        return circleHoleColor;
    }

    public void setCircleHoleColor(int circleHoleColor) {
        this.circleHoleColor = circleHoleColor;
    }

    public int getValueTextColor() {
        return valueTextColor;
    }

    public void setValueTextColor(int valueTextColor) {
        this.valueTextColor = valueTextColor;
    }

    public float getValueTextSize() {
        return valueTextSize;
    }

    public void setValueTextSize(float valueTextSize) {
        this.valueTextSize = valueTextSize;
    }

    public Typeface getValueTypeFace() {
        return valueTypeFace;
    }

    public void setValueTypeFace(Typeface valueTypeFace) {
        this.valueTypeFace = valueTypeFace;
    }

    public List<String> getXAxisValues() {
        return xAxisValues;
    }

    public void setXAxisValues(List<String> xAxisValues) {
        this.xAxisValues = xAxisValues;
    }

    public List<ReportData.UnitEvent.Event> getUnitEvents() {
        return unitEvents;
    }

    public void setUnitEvents(List<ReportData.UnitEvent.Event> unitEvents) {
        this.unitEvents = unitEvents;
    }

    public ReportView getDefaultView() {
        return reportView;
    }

    public void setDefaultView(ReportView reportView) {
        this.reportView = reportView;
    }

    public List<BarEntry> getBarEntries() {
        return barEntries;
    }

    public void setBarEntries(List<BarEntry> barEntries) {
        this.barEntries = barEntries;
    }

    public Boolean isAggregate() {
        return isAggregate;
    }

    public void setAggregate(Boolean aggregate) {
        isAggregate = aggregate;
    }

    public String getPressName() {
        return pressName;
    }

    public void setPressName(String pressName) {
        this.pressName = pressName;
    }

    public ReportKpiViewModel getSubKpiModel(String kpiName) {
        if (subKPIs == null) {
            return null;
        }

        for (ReportKpiViewModel kpi : subKPIs) {
            if (kpi.getName().equals(kpiName)) {
                return kpi;
            }
        }

        return null;
    }

    public Map<String, ReportKpiViewModel> getReportPerPressList() {
        return reportPerPressList;
    }

    public void setReportPerPressList(HashMap<String, ReportKpiViewModel> reportPerPressList) {
        this.reportPerPressList = reportPerPressList;
    }

    public String getLocalizedName() {
        return localizedName;
    }

    public void setLocalizedName(String localizedName) {
        this.localizedName = localizedName;
    }

    public boolean isReportPerPressListEmpty() {
        return reportPerPressList == null || reportPerPressList.size() == 0;
    }

    public void addPressReport(ReportKpiViewModel pressReport) {
        if (reportPerPressList == null) {
            reportPerPressList = new HashMap<>();
        }

        if (reportPerPressList.containsKey(pressReport.getName())) {
            reportPerPressList.remove(pressReport.getName());
        }

        reportPerPressList.put(pressReport.getName(), pressReport);
    }

    public String getYAxisLabel() {
        return yAxisLabel;
    }

    public void setYAxisLabel(String yAxisLabel) {
        this.yAxisLabel = yAxisLabel;
    }

    @Override
    public String toString() {
        return "ReportKpiViewModel{" +
                ", name='" + name + '\'' +
                ", overallValues=" + overallValues +
                ", kpiValues=" + kpiValues +
                ", accumulativeValues=" + accumulativeValues +
                ", color=" + color +
                ", fillArea=" + fillArea +
                ", activeAlone=" + activeAlone +
                ", circleColor=" + circleColor +
                ", lineWidth=" + lineWidth +
                ", highLightColor=" + highLightColor +
                ", circleSize=" + circleSize +
                ", drawCircle=" + drawCircle +
                ", drawCircleHole=" + drawCircleHole +
                ", drawValues=" + drawValues +
                ", iconUp=" + iconUp +
                ", circleHoleColor=" + circleHoleColor +
                ", valueTextColor=" + valueTextColor +
                ", valueTextSize=" + valueTextSize +
                ", valueTypeFace=" + valueTypeFace +
                '}';
    }

    public enum ReportView {
        BAR, LINE
    }
}
