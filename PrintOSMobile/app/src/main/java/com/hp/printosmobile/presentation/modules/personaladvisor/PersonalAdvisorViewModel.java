package com.hp.printosmobile.presentation.modules.personaladvisor;

import com.hp.printosmobile.presentation.modules.shared.DeviceViewModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Anwar Asbah on 4/12/2016.
 */
public class PersonalAdvisorViewModel implements Serializable {

    private List<AdviceViewModel> advices;

    public List<AdviceViewModel> getAllAdvices() {
        return advices;
    }

    public void setAdvices(List<AdviceViewModel> advices) {
        this.advices = advices;
    }

    public List<AdviceViewModel> getUnsuppressedAdvices() {
        List<AdviceViewModel> unsuppressedAdvices = new ArrayList<>();

        if (advices == null) {
            return unsuppressedAdvices;
        }

        for (int i = 0; i < advices.size(); i++) {
            AdviceViewModel advice = advices.get(i);
            if (advice != null && !advice.getSuppressed()) {
                unsuppressedAdvices.add(advice);
            }
        }

        return unsuppressedAdvices;
    }

    public void unSuppressAll() {

        if (advices == null) {
            return;
        }

        for (AdviceViewModel adviceViewModel : advices) {
            adviceViewModel.setSuppressed(false);
        }

    }

    public static boolean suppressAdvice(List<DeviceViewModel> deviceViewModels, String device, int adviceId) {

        if (deviceViewModels == null || device == null) {
            return false;
        }

        boolean suppressAny = false;

        for (DeviceViewModel deviceViewModel : deviceViewModels) {
            if (device.equals(deviceViewModel.getSerialNumber())) {
                if (deviceViewModel.hasAdvices()) {
                    for (AdviceViewModel adviceViewModel : deviceViewModel.getPersonalAdvisorViewModel().getAllAdvices()) {
                        if (adviceId == adviceViewModel.getAdviseId()) {
                            adviceViewModel.setSuppressed(true);
                            suppressAny = true;
                            break;
                        }
                    }
                }
            }
        }

        return suppressAny;
    }

    public static boolean unSuppressAllAdvices(List<DeviceViewModel> deviceViewModels) {

        if (deviceViewModels == null) {
            return false;
        }

        boolean unSuppressAny = false;

        for (DeviceViewModel deviceViewModel : deviceViewModels) {
            if (deviceViewModel.hasAdvices()) {
                for (AdviceViewModel adviceViewModel : deviceViewModel.getPersonalAdvisorViewModel().getAllAdvices()) {
                    adviceViewModel.setSuppressed(false);
                    unSuppressAny = true;
                }
            }
        }

        return unSuppressAny;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("PersonalAdvisorViewModel{");
        sb.append("advices=").append(advices);
        sb.append('}');
        return sb.toString();
    }

}
