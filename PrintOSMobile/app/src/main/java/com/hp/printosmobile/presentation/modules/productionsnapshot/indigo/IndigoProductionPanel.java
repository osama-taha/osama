package com.hp.printosmobile.presentation.modules.productionsnapshot.indigo;

import android.content.Context;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.util.AttributeSet;
import android.widget.TextView;

import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.presentation.modules.shared.DeviceViewModel;
import com.hp.printosmobile.utils.HPLocaleUtils;
import com.hp.printosmobilelib.ui.widgets.panel.PanelView;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by Anwar Asbah on 4/12/2016.
 */
public class IndigoProductionPanel extends PanelView<ProductionViewModel> {

    public final static String TAG = IndigoProductionPanel.class.getSimpleName();

    @Bind(R.id.print_queue_jobs_in_queue_text_view)
    TextView jobsInQueueValue;
    @Bind(R.id.text_completed_jobs_value)
    TextView completedJobsValue;

    IndigoProductionPanelCallback callback;

    public IndigoProductionPanel(Context context) {
        super(context);
    }

    public IndigoProductionPanel(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public IndigoProductionPanel(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public Spannable getTitleSpannable() {
        SpannableStringBuilder builder = new SpannableStringBuilder(getContext().getString(R.string.production_panel_title));
        return builder;
    }

    @Override
    public int getContentView() {
        return R.layout.production_snapshot_content_indigo;
    }

    @Override
    public void updateViewModel(ProductionViewModel viewModel) {

        setViewModel(viewModel);

        if (showEmptyCard()) {
            return;
        }

        jobsInQueueValue.setText(getLocalizedValue(viewModel.getJobsInQueue()));
        completedJobsValue.setText(getLocalizedValue(viewModel.getJobsCompleted()));

    }

    @Override
    protected boolean isValidViewModel() {
        return getViewModel() != null;
    }

    protected String getEmptyCardText() {
        return getContext().getString(R.string.no_information_to_display);
    }

    public void addCallback(IndigoProductionPanelCallback callback) {
        this.callback = callback;
    }

    @OnClick(R.id.job_in_queue_container)
    public void onJobsInQueueClicked() {
        if (callback != null) {
            callback.onJobsInQueueClicked(DeviceViewModel.JobType.QUEUED);
        }
    }

    @OnClick(R.id.job_completed_container)
    public void onJobsCompletedClicked() {
        if (callback != null && PrintOSPreferences.getInstance(getContext()).isCompletedJobsFeatureEnabled()
                && getViewModel() != null && getViewModel().isHasCompletedJobs()) {
            callback.onJobsInQueueClicked(DeviceViewModel.JobType.COMPLETED);
        }
    }

    public interface IndigoProductionPanelCallback {
        void onJobsInQueueClicked(DeviceViewModel.JobType jobType);
    }

    private String getLocalizedValue(int value) {
        return HPLocaleUtils.getLocalizedValue(value);
    }

}
