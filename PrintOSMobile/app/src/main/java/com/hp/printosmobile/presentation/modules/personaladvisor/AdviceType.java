package com.hp.printosmobile.presentation.modules.personaladvisor;

import com.hp.printosmobile.R;

/**
 * Created by OsamaTaha on 5/23/16.
 */
public enum AdviceType {

    EMPTY("",  0),
    SYNC_ERROR("sync-error", R.drawable.sync_error),
    PROOFING_RATIO("proofing_ratio", R.drawable.proofing_ratio),
    INK_PANIC_OVERHEAT("ink_panic_overheat", R.drawable.ink_panic_overheat),
    INK_DENSITY_TOO_LOW("ink_density_too_low", R.drawable.ink_density_too_low),
    INK_CONDUCTIVITY_TIMED_OUT("ink_conductivity_timed_out", R.drawable.ink_conductivity_timedout),
    FEEDER_DRAWER_IS_EMPTY("feeder_drawer_is_empty", R.drawable.feeder_drawer_is_empty),
    INDUSTRIAL_WASTE_BOTTLE_FULL_ERROR("industrial_waste_bottle_full_error", R.drawable.industrial_waste_bottle_full_error),
    INK_TEMPRATURE_TOO_HIGH("ink_temprature_too_high", R.drawable.ink_temprature_too_high),
    INVALID_PATCH_ERROR("invalid_patch_error", R.drawable.invalid_patch_error),
    INK_FLOW_FALL_TO_STANDBY("ink_flow_fall_to_standby", R.drawable.ink_flow_fall_to_standby),
    INK_LEVEL_TOO_LOW("ink_level_too_low", R.drawable.ink_level_too_low),
    PAPER_JAMS("paper_jams", R.drawable.paper_jams),
    CONSUMABLE_REPLACEMENT_DAILY_ROUTINE("consumable_replacement_daily_routine", R.drawable.consumable_replacement_daily_routine),
    RESTART_DAILY_ROUTINE("restart_daily_routine", R.drawable.restart_daily_routine),
    INK_NOT_SCANNED_TIMEOUT("ink_not_scanned_timeout", R.drawable.ink_not_scanned_timeout),
    SUBSTRATE_BATCHING("substrate_batching", R.drawable.substrate_batching),
    SUBSTRATE_RATIO("substrate_ratio", R.drawable.substrate_ratio),
    ACTIVATED_DEFINITION("substrate_definition", R.drawable.substrate_definition),
    ACTIVATED_JAM_SUBSTRATE_HIGH_USAGE("jam_per_substrate_high_usage", R.drawable.jam_per_substrate_high_usage),
    ACTIVATED_JAM_SUBSTRATE_LOW_USAGE("jam_per_substrate_low_usage", R.drawable.jam_per_substrate_low_usage),
    ACTIVATED_ACTIVATED_BYPASS("activated_bypass", R.drawable.activated_bypass);

    private final String adviceType;
    private final int adviceIcon;

    AdviceType(String adviceType, int adviceIcon) {
        this.adviceType = adviceType;
        this.adviceIcon = adviceIcon;
    }

    public int getAdviceIcon() {
        return adviceIcon;
    }

    public String getAdviceType() {
        return adviceType;
    }

    public static AdviceType from(String state) {
        for (AdviceType deviceState : AdviceType.values()) {
            if (deviceState.getAdviceType().equals(state))
                return deviceState;
        }
        return AdviceType.EMPTY;
    }

}
