package com.hp.printosmobile.presentation.modules.devicedetails;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hp.printosmobile.R;
import com.hp.printosmobile.data.remote.models.PreferencesData;
import com.hp.printosmobile.presentation.modules.shared.Unit;
import com.hp.printosmobile.utils.AppUtils;
import com.hp.printosmobile.utils.HPLocaleUtils;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Anwar Asbah on 8/16/2016.
 */
public class SubstrateAdapter extends RecyclerView.Adapter<SubstrateAdapter.SubstrateViewHolder> {

    private static final float METER_TO_MILLI_RATIO = 1000f;
    private final PreferencesData.UnitSystem unitSystem;
    private Context context;
    private List<SubstrateModel> substrates;

    public SubstrateAdapter(Context context, List<SubstrateModel> substrates, PreferencesData.UnitSystem unitSystem) {
        this.context = context;
        this.substrates = substrates;
        this.unitSystem = unitSystem;
    }

    @Override
    public SubstrateViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ViewGroup view = (ViewGroup) inflater.inflate(R.layout.substrate_item, parent, false);
        SubstrateViewHolder substrateViewHolder = new SubstrateViewHolder(view);
        return substrateViewHolder;
    }

    @Override
    public void onBindViewHolder(SubstrateViewHolder holder, int position) {
        SubstrateModel model = substrates.get(position);
        holder.typeTextView.setText(model.getMediaName());

        setMeasure(holder.widthTextView, getValueInMillimeters(model.getMediaWidth(), model.getUnit()), Unit.MILLIMETER);
        setMeasure(holder.substrateUsageTextView, getValueInMillimeters(model.getMediaCounter(), model.getUnit()), Unit.MILLIMETER);


        setMeasure(holder.remainingLengthTextView, model.getRemainingLength(), model.getUnit(), Unit.METER);

    }

    public double getValueInMillimeters(float value, SubstrateModel.SubstrateUnit unit) {

        float millimeters = 0;
        if (unit == SubstrateModel.SubstrateUnit.METER || unit == SubstrateModel.SubstrateUnit.METERS) {
            millimeters = value * METER_TO_MILLI_RATIO;
        } else if (unit == SubstrateModel.SubstrateUnit.MILLIMETERS) {
            millimeters = value;
        }

        if (unitSystem == PreferencesData.UnitSystem.Imperial) {
            return AppUtils.convertMillimetersToInch(millimeters);
        }

        return millimeters;
    }

    public double getValueInMeters(float value, SubstrateModel.SubstrateUnit unit) {

        float meters = 0;
        if (unit == SubstrateModel.SubstrateUnit.MILLIMETERS) {
            meters = value / METER_TO_MILLI_RATIO;
        } else if (unit == SubstrateModel.SubstrateUnit.METER || unit == SubstrateModel.SubstrateUnit.METERS) {
            meters = value;
        }

        if (unitSystem == PreferencesData.UnitSystem.Imperial) {
            return AppUtils.convertMetersToFeet(meters);
        }

        return meters;
    }

    private void setMeasure(TextView view, double value, Unit unit) {
        view.setText(value > 0 ? unit.getUnitTextWithValue(context, unitSystem, Unit.UnitStyle.VALUE,
                HPLocaleUtils.getRoundMeasure(value, 1)) :
                context.getString(R.string.unknown_value));
    }

    private void setMeasure(TextView view, String value, SubstrateModel.SubstrateUnit substrateUnit, Unit unit){
        try {
            double doubleValue = getValueInMeters((float)Double.parseDouble(value), substrateUnit);
            setMeasure(view, doubleValue, unit);
        } catch (Exception e) {
            view.setText(value == null ? context.getString(R.string.unknown_value) : value);
        }
    }

    @Override
    public int getItemCount() {
        return substrates == null ? 0 : substrates.size();
    }

    public class SubstrateViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.type_text_view)
        TextView typeTextView;
        @Bind(R.id.width_text_view)
        TextView widthTextView;
        @Bind(R.id.substrate_usage_text_view)
        TextView substrateUsageTextView;
        @Bind(R.id.remaining_length_text_view)
        TextView remainingLengthTextView;

        public SubstrateViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
