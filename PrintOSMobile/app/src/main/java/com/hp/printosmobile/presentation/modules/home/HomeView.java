package com.hp.printosmobile.presentation.modules.home;

import com.hp.printosmobile.presentation.modules.personaladvisor.PersonalAdvisorViewModel;
import com.hp.printosmobile.presentation.modules.productionsnapshot.indigo.ProductionViewModel;
import com.hp.printosmobile.presentation.modules.servicecallpanel.ServiceCallViewModel;
import com.hp.printosmobile.presentation.modules.shared.DeviceViewModel;
import com.hp.printosmobile.presentation.modules.today.TodayViewModel;
import com.hp.printosmobile.presentation.modules.todayhistogram.TodayHistogramViewModel;
import com.hp.printosmobile.presentation.modules.week.WeekCollectionViewModel;
import com.hp.printosmobilelib.core.communications.remote.APIException;

import java.util.List;

/**
 * Created by Osama Taha on 5/21/16.
 */
public interface HomeView {

    void updateProductionSnapshot(ProductionViewModel productionViewModel);

    void updateLatexProductionSnapshot(ProductionViewModel productionViewModel);

    void updateTodayPanel(TodayViewModel todayViewModel);

    void updateTodayPanelWithActiveShifts(ActiveShiftsViewModel activeShiftsViewModel);

    void updateHistogram(TodayHistogramViewModel todayHistogramViewModel);

    void updateWeekPanel(WeekCollectionViewModel weekCollectionViewModel);

    void updatePrintersPanel(List<DeviceViewModel> deviceViewModels);

    void updateServiceCallPanel (ServiceCallViewModel serviceCallViewModel);

    void onError(APIException exception, String... tag);
}
