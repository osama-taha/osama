package com.hp.printosmobile.presentation.modules.todayhistogram;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.hp.printosmobile.IntercomSdk;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;
import com.hp.printosmobilelib.ui.common.HPFragment;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * create by Anwar Asbah 9/27/2016
 */
public class HistogramDetailsFragment extends HPFragment {

    private static final String ARG_HISTOGRAM_VIEW_MODEL = "ARG_HISTOGRAM_VIEW_MODEL";
    private static final int MAX_NUMBERS_TO_DISPLAY = 7;
    private static final int NUMBER_OF_LEGENDS_PER_ROW = 4;
    private static final int NUMBER_OF_DAYS = 7;

    @Bind(R.id.title)
    TextView titleTextView;
    @Bind(R.id.histogram_pager)
    ViewPager histogramPager;
    @Bind(R.id.all_1000_values)
    View all100TextView;

    private TodayHistogramViewModel viewModel;
    private HistogramDetailsAdapter histogramAdapter;
    private HistogramLegendsAdapter legendsAdapter;

    public static HistogramDetailsFragment newInstance(TodayHistogramViewModel viewModel) {
        HistogramDetailsFragment fragment = new HistogramDetailsFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_HISTOGRAM_VIEW_MODEL, viewModel);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null) {
            viewModel = (TodayHistogramViewModel) bundle.getSerializable(ARG_HISTOGRAM_VIEW_MODEL);
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        IntercomSdk.getInstance(PrintOSApplication.getAppContext()).logEvent(IntercomSdk.PBM_VIEW_7DAY_DETAILED_EVENT);

        displayModel();
    }

    private void displayModel() {
        titleTextView.setText(getString(viewModel.isShiftSupport()? R.string.histogram_shifts_title
                : R.string.histogram_view_title, NUMBER_OF_DAYS));
        histogramAdapter = new HistogramDetailsAdapter(getContext(), viewModel, MAX_NUMBERS_TO_DISPLAY);
        histogramPager.setAdapter(histogramAdapter);
        if (histogramAdapter.getCount() > 0) {
            histogramPager.setCurrentItem(histogramAdapter.getCount() - 1);
        }
        all100TextView.setVisibility(viewModel.getBusinessUnit() == BusinessUnitEnum.INDIGO_PRESS ? View.VISIBLE :
                View.GONE);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.histogram_details_view;
    }

    @Override
    public boolean isFragmentNameToBeDisplayed() {
        return true;
    }

    @Override
    public boolean isFilteringAvailable() {
        return false;
    }

    @Override
    public int getToolbarDisplayName() {
        return R.string.histogram_view_title;
    }
}
