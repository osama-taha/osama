package com.hp.printosmobile.presentation.modules.settings;

import com.hp.printosmobile.presentation.Presenter;
import com.hp.printosmobilelib.core.communications.remote.APIException;
import com.hp.printosmobilelib.core.logging.HPLogger;

import java.util.List;

import okhttp3.ResponseBody;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Anwar Asbah on 1/17/2017.
 */
public class NotificationsPresenter extends Presenter<NotificationsSubMenuView> {

    private static final String TAG = NotificationsPresenter.class.getSimpleName();

    public void getAvailableSubscriptions() {
        HPLogger.d(TAG, "get Supported Subscriptions");
        Subscription subscription = NotificationsManager.getAvailableEvents()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<List<String>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        HPLogger.d(TAG, "Error while fetching supported subscription " + e);
                        if (mView != null && e instanceof APIException) {
                            mView.onFailedToGetSubscription((APIException) e);
                        }
                    }

                    @Override
                    public void onNext(List<String> events) {
                        HPLogger.d(TAG, "Supported subscriptions successfully retrieved");
                        if (mView != null) {
                            mView.setSupportedSubscriptions(events);
                        }
                    }
                });
        addSubscriber(subscription);
    }

    public void getUserSubscriptions() {
        HPLogger.d(TAG, "get User Subscriptions");
        Subscription subscription = NotificationsManager.getUserSubscriptions()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<List<SubscriptionEvent>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        HPLogger.d(TAG, "Error while fetching Notification Applications " + e);
                        e.printStackTrace();
                        if (mView != null && e instanceof APIException) {
                            mView.onFailedToGetSubscription((APIException) e);
                        }
                    }

                    @Override
                    public void onNext(List<SubscriptionEvent> events) {
                        HPLogger.d(TAG, "Subscriptions successfully retrieved");
                        if (mView != null) {
                            mView.setSubscriptions(events);
                        }
                    }
                });
        addSubscriber(subscription);
    }

    public void subscribeEvent(final SubscriptionEvent subscriptionEvent) {

        if (subscriptionEvent == null || subscriptionEvent.getNotificationEventEnum() == null) {
            return;
        }

        final String eventDescName = subscriptionEvent.getNotificationEventEnum().getKey();

        HPLogger.d(TAG, "subscribing to event: " + eventDescName);

        Subscription subscription = NotificationsManager.subscribeEvent(eventDescName)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<SubscriptionEvent>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        HPLogger.d(TAG, "Error while subscribing to event " + eventDescName + " due to " + e);
                    }

                    @Override
                    public void onNext(SubscriptionEvent event) {
                        HPLogger.d(TAG, "successfully subscribed to even: " + eventDescName);
                        if (mView != null) {
                            mView.onSubscribed(event);
                        }
                        if (event != null && event.getUserSubscriptionID() != null) {
                            setSubscriptionDestination(event.getUserSubscriptionID());
                        }
                    }
                });
        addSubscriber(subscription);
    }

    private void setSubscriptionDestination(final String subscriptionID) {
        HPLogger.d(TAG, "setting subscription destination with id: " + subscriptionID);

        Subscription subscription = NotificationsManager.setSubscriptionDestination(subscriptionID)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<ResponseBody>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        HPLogger.d(TAG, "Error while setting subscription destination for id " + subscriptionID + " due to " + e);
                    }

                    @Override
                    public void onNext(ResponseBody events) {
                        HPLogger.d(TAG, "successfully set subscription destination for id: " + subscriptionID);
                    }
                });
        addSubscriber(subscription);
    }

    public void unsubscribeToEvent(final SubscriptionEvent subscriptionEvent) {

        if (subscriptionEvent == null || subscriptionEvent.getUserSubscriptionID() == null) {
            return;
        }

        HPLogger.d(TAG, "un-subscribed to event with userID "
                + subscriptionEvent.getUserSubscriptionID());

        Subscription subscription = NotificationsManager.unsubscribeEvent(subscriptionEvent.getUserSubscriptionID())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<ResponseBody>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        HPLogger.d(TAG, "Error while unsubscribing to event with userID "
                                + subscriptionEvent.getUserSubscriptionID()
                                + " due to " + e);
                    }

                    @Override
                    public void onNext(ResponseBody responseBody) {
                        HPLogger.d(TAG, "Successfully un-subscribed to event with userID "
                                + subscriptionEvent.getUserSubscriptionID());
                    }
                });
        addSubscriber(subscription);
    }

    @Override
    public void detachView() {
        stopSubscribers();
        mView = null;
    }

}