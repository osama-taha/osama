package com.hp.printosmobile.presentation.modules.main;

import android.content.Context;

import com.hp.printosmobile.R;

/**
 * Created by Anwar Asbah on 10/6/2016.
 */
public enum ReportChartTypeEnum {
    BAR("ColumnChart"),
    AREA("AreaChart"),
    LINE("LineChart"),
    UNKNOWN("");

    private String key;

    ReportChartTypeEnum(String key) {
        this.key = key;
    }

    public static ReportChartTypeEnum from(Context context, String key, String kpi) {
        for (ReportChartTypeEnum type : ReportChartTypeEnum.values()) {
            if (key.toLowerCase().equals(type.key.toLowerCase())) {
                if (type == BAR) {
                    if (context.getString(R.string.indigo_kpi_availability_key).equals(kpi)) {
                        return BAR;
                    } else {
                        return LINE;
                    }
                }
                return type;
            }
        }
        return ReportChartTypeEnum.UNKNOWN;
    }
}
