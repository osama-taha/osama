package com.hp.printosmobile.presentation.modules.contacthp.shared;

import android.content.Context;
import android.net.Uri;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.hp.printosmobile.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Anwar Asbah on 10/10/2016.
 */
public class AttachImageAdapter extends RecyclerView.Adapter<AttachImageAdapter.ImageViewHolder> {

    public static final int MAX_NUMBER_OF_ATTACHED_IMAGES = 4;
    
    private static final int TYPE_ADD = 0;
    private static final int TYPE_IMAGE = 1;

    private List<Uri> imageUri;
    private AttachImageAdapterCallback listener;
    private Context context;
    private boolean isEnabled;

    public AttachImageAdapter(Context context, AttachImageAdapterCallback listener) {
        this.context = context;
        this.listener = listener;
        imageUri = new ArrayList<>();
        isEnabled = true;
    }


    @Override
    public int getItemViewType(int position) {
        return position < imageUri.size() ? TYPE_IMAGE : TYPE_ADD;
    }

    @Override
    public int getItemCount() {
        return Math.min(imageUri.size() + 1, MAX_NUMBER_OF_ATTACHED_IMAGES);
    }

    @Override
    public ImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View imageViewItem = inflater.inflate(R.layout.contact_hp_attach_image_view_item, parent, false);
        ImageViewHolder imageViewHolder = new ImageViewHolder(imageViewItem);
        return imageViewHolder;
    }

    @Override
    public void onBindViewHolder(final ImageViewHolder viewHolder, int position) {
        viewHolder.position = position;
        ImageView imageView = viewHolder.attachedImage;
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (viewHolder.getItemViewType() == TYPE_ADD) {
                    if (listener != null && isEnabled) {
                        listener.onAddImageClicked();
                    }
                }
            }
        });

        viewHolder.removeButton.setVisibility(viewHolder.getItemViewType() == TYPE_IMAGE ? View.VISIBLE : View.GONE);
        viewHolder.addSign.setVisibility(viewHolder.getItemViewType() != TYPE_IMAGE ? View.VISIBLE : View.GONE);

        switch (viewHolder.getItemViewType()) {
            case TYPE_IMAGE:
                Picasso.with(context).load(imageUri.get(position)).fit().into(imageView);
                viewHolder.itemView.setBackgroundResource(R.drawable.blue_frame);
                break;
            default:
                viewHolder.itemView.setBackgroundResource(R.drawable.dashed_blue_frame);
                break;
        }
    }

    public void addImage(Uri uri) {
        imageUri.add(uri);
        notifyDataSetChanged();
    }

    public List<Uri> getImageUri() {
        return imageUri;
    }

    public void clear() {
        imageUri.clear();
        notifyDataSetChanged();
    }

    public void setEnabled(boolean isEnabled) {
        this.isEnabled = isEnabled;
    }

    public class ImageViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.attached_image)
        ImageView attachedImage;
        @Bind(R.id.attached_image_remove_button)
        View removeButton;
        @Bind(R.id.add_sign)
        View addSign;

        View itemView;
        int position;

        public ImageViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.itemView = itemView;
        }

        @OnClick(R.id.attached_image_remove_button)
        public void setRemoveButtonClicked() {
            if (isEnabled) {
                imageUri.remove(position);
                notifyDataSetChanged();
            }
        }

    }

    public interface AttachImageAdapterCallback {
        void onAddImageClicked();
    }
}
