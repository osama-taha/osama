package com.hp.printosmobile.presentation.indigowidget;

import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;

import com.hp.printosmobile.AppseeSdk;
import com.hp.printosmobile.IntercomSdk;
import com.hp.printosmobile.presentation.modules.login.LoginActivity;
import com.hp.printosmobile.presentation.modules.main.MainActivity;
import com.hp.printosmobile.presentation.modules.splash.SplashActivity;
import com.hp.printosmobilelib.core.logging.HPLogger;

/**
 * Created by Osama Taha on 2/10/17.
 */
public abstract class PrintOSAppWidgetProvider extends android.appwidget.AppWidgetProvider {

    private static final String TAG = PrintOSAppWidgetProvider.class.getSimpleName();

    public static final String ACTION_REFRESH_MANUAL = "com.hp.printosforpsp.appwidget.action.APPWIDGET_REFRESH_MANUAL";
    public static final String ACTION_REFRESH_AUTO = "com.hp.printosforpsp.appwidget.action.APPWIDGET_REFRESH_AUTO";
    public static final String ACTION_OPEN_APP = "com.hp.printosforpsp.appwidget.action.APPWIDGET_OPEN_APP";
    public static final String ACTION_SELECT_NEXT_SITE = "com.hp.printosforpsp.appwidget.action.APPWIDGET_NEXT_SITE";
    public static final String ACTION_SELECT_PREVIOUS_SITE = "com.hp.printosforpsp.appwidget.action.APPWIDGET_PREVIOUS_SITE";
    public static final String ACTION_JUMP_DEVICE_ITEM = "com.hp.printosforpsp.appwidget.action.CLICK_DEVICE_ITEM";
    public static final String ACTION_CLICK_GAUGE = "com.hp.printosforpsp.appwidget.action.CLICK_GAUGE";

    @Override
    public void onReceive(final Context context, final Intent intent) {

        HPLogger.d(TAG, "App widget received intent " + intent.getAction());

        if (ACTION_OPEN_APP.equalsIgnoreCase(intent.getAction())) {

            IntercomSdk.getInstance(context).sendUserOpenAppByWidgetEvent();
            openLoginScreen(context);

        } else if (ACTION_REFRESH_MANUAL.equals(intent.getAction())) {

            IndigoWidgetRemoteViews remoteViews = new IndigoWidgetRemoteViews(context, getWidgetType());
            remoteViews.notifyAppWidgetViewDataChanged();

        } else if (ACTION_REFRESH_AUTO.equals(intent.getAction())) {

            IndigoWidgetRemoteViews remoteViews = new IndigoWidgetRemoteViews(context, getWidgetType());
            remoteViews.notifyAppWidgetViewDataChanged();

        } else if (ACTION_SELECT_NEXT_SITE.equals(intent.getAction())) {

            IndigoWidgetRemoteViews remoteViews = new IndigoWidgetRemoteViews(context, getWidgetType());
            remoteViews.showNext();

        } else if (ACTION_SELECT_PREVIOUS_SITE.equals(intent.getAction())) {

            IndigoWidgetRemoteViews remoteViews = new IndigoWidgetRemoteViews(context, getWidgetType());
            remoteViews.showPrevious();

        } else if (ACTION_JUMP_DEVICE_ITEM.equals(intent.getAction())) {

            IntercomSdk.getInstance(context).sendUserClickDeviceEvent();
            openApplication(context);

        } else if (ACTION_CLICK_GAUGE.equals(intent.getAction())) {

            IntercomSdk.getInstance(context).sendUserClickGaugeEvent();
            openApplication(context);

        } else if (AppWidgetManager.ACTION_APPWIDGET_DELETED.equals(intent.getAction())) {

            AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
            int length = appWidgetManager.getAppWidgetIds(intent.getComponent()).length;
            if (length == 0) {
                stopWidgetService(context);
            }
        }

        super.onReceive(context, intent);
    }

    private void openLoginScreen(Context context) {
        Intent newIntent = new Intent(context, LoginActivity.class);
        newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        context.startActivity(newIntent);
    }

    private void openApplication(Context context) {

        AppseeSdk.getInstance(context).sendEvent(AppseeSdk.EVENT_OPEN_FROM_WIDGET);

        Intent newIntent = new Intent(context, MainActivity.class);
        newIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        context.startActivity(newIntent);
    }

    private void stopWidgetService(Context context) {

        HPLogger.d(TAG, "Stop widget service " + getWidgetType().name());

        Intent intent = new Intent(context, getWidgetType().getServiceClass());
        context.stopService(intent);

    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        super.onUpdate(context, appWidgetManager, appWidgetIds);

        IndigoWidgetRemoteViews indigoWidgetRemoteViews = new IndigoWidgetRemoteViews(context, getWidgetType());
        indigoWidgetRemoteViews.setAppName();
        indigoWidgetRemoteViews.setOnPreviousSiteClickPendingIntent();
        indigoWidgetRemoteViews.setOnNextSiteClickPendingIntent();
        indigoWidgetRemoteViews.setOnOpenAppClickPendingIntent();
        indigoWidgetRemoteViews.bindListViewAdapter();

        appWidgetManager.updateAppWidget(appWidgetIds, indigoWidgetRemoteViews);

        super.onUpdate(context, appWidgetManager, appWidgetIds);
    }

    @Override
    public void onDeleted(Context context, int[] appWidgetIds) {
        super.onDeleted(context, appWidgetIds);
    }

    @Override
    public void onEnabled(Context context) {
        super.onEnabled(context);
        IntercomSdk.getInstance(context).sendUserInstallWidgetEvent();
    }

    @Override
    public void onDisabled(Context context) {
        super.onDisabled(context);
    }

    public abstract WidgetType getWidgetType();


    public static void updateAllWidgets(Context context) {

        HPLogger.d(TAG, "Update app widgets.");

        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
        for (WidgetType widgetType : WidgetType.values()) {
            int[] appWidgetIds = appWidgetManager.getAppWidgetIds(new ComponentName(context, widgetType.getProviderClass()));
            if (appWidgetIds.length > 0) {
                context.sendBroadcast(new Intent(PrintOSAppWidgetProvider.ACTION_REFRESH_MANUAL));
            }
        }
    }
}
