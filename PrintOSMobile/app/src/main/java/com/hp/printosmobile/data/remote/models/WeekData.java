package com.hp.printosmobile.data.remote.models;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.hp.printosmobile.data.remote.services.PerformanceTrackingService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A model representing the response of {@link PerformanceTrackingService#getWeeklyReport(List, String)} endpoint.
 *
 * @author Osama Taha
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class WeekData {

    @JsonProperty("name")
    private String name;
    @JsonProperty("startTime")
    private String startTime;
    @JsonProperty("endTime")
    private String endTime;
    @JsonProperty("weeklyPoints")
    private Integer weeklyPoints;
    @JsonProperty("totalPoints")
    private Integer totalPoints;
    @JsonProperty("weeks")
    private Integer weeks;
    @JsonProperty("weeklyAv")
    private Integer weeklyAv;
    @JsonProperty("kpiScoreState")
    private String kpiScoreState;
    @JsonProperty("uiStartDate")
    private String uiStartDate;
    @JsonProperty("lastWeightValue")
    private Integer lastWeightValue;
    @JsonProperty("uiEndDate")
    private String uiEndDate;
    @JsonProperty("scorables")
    private List<Scorable> scorables = new ArrayList<Scorable>();
    @JsonProperty("weightValue")
    private Integer weightValue;
    @JsonProperty("rankings")
    private Rankings rankings;

    @JsonProperty("pressesStatus")
    private HashMap<String, PressStatus> pressesStatus;

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * @return The name
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The startTime
     */
    @JsonProperty("startTime")
    public String getStartTime() {
        return startTime;
    }

    /**
     * @param startTime The startTime
     */
    @JsonProperty("startTime")
    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    /**
     * @return The endTime
     */
    @JsonProperty("endTime")
    public String getEndTime() {
        return endTime;
    }

    /**
     * @param endTime The endTime
     */
    @JsonProperty("endTime")
    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    /**
     * @return The weeklyPoints
     */
    @JsonProperty("weeklyPoints")
    public Integer getWeeklyPoints() {
        return weeklyPoints;
    }

    /**
     * @param weeklyPoints The weeklyPoints
     */
    @JsonProperty("weeklyPoints")
    public void setWeeklyPoints(Integer weeklyPoints) {
        this.weeklyPoints = weeklyPoints;
    }

    /**
     * @return The totalPoints
     */
    @JsonProperty("totalPoints")
    public Integer getTotalPoints() {
        return totalPoints;
    }

    /**
     * @param totalPoints The totalPoints
     */
    @JsonProperty("totalPoints")
    public void setTotalPoints(Integer totalPoints) {
        this.totalPoints = totalPoints;
    }

    /**
     * @return The weeks
     */
    @JsonProperty("weeks")
    public Integer getWeeks() {
        return weeks;
    }

    /**
     * @param weeks The weeks
     */
    @JsonProperty("weeks")
    public void setWeeks(Integer weeks) {
        this.weeks = weeks;
    }

    /**
     * @return The weeklyAv
     */
    @JsonProperty("weeklyAv")
    public Integer getWeeklyAv() {
        return weeklyAv;
    }

    /**
     * @param weeklyAv The weeklyAv
     */
    @JsonProperty("weeklyAv")
    public void setWeeklyAv(Integer weeklyAv) {
        this.weeklyAv = weeklyAv;
    }

    /**
     * @return The kpiScoreState
     */
    @JsonProperty("kpiScoreState")
    public String getKpiScoreState() {
        return kpiScoreState;
    }

    /**
     * @param kpiScoreState The kpiScoreState
     */
    @JsonProperty("kpiScoreState")
    public void setKpiScoreState(String kpiScoreState) {
        this.kpiScoreState = kpiScoreState;
    }

    /**
     * @return The uiStartDate
     */
    @JsonProperty("uiStartDate")
    public String getUiStartDate() {
        return uiStartDate;
    }

    /**
     * @param uiStartDate The uiStartDate
     */
    @JsonProperty("uiStartDate")
    public void setUiStartDate(String uiStartDate) {
        this.uiStartDate = uiStartDate;
    }

    /**
     * @return The lastWeightValue
     */
    @JsonProperty("lastWeightValue")
    public Integer getLastWeightValue() {
        return lastWeightValue;
    }

    /**
     * @param lastWeightValue The lastWeightValue
     */
    @JsonProperty("lastWeightValue")
    public void setLastWeightValue(Integer lastWeightValue) {
        this.lastWeightValue = lastWeightValue;
    }

    /**
     * @return The uiEndDate
     */
    @JsonProperty("uiEndDate")
    public String getUiEndDate() {
        return uiEndDate;
    }

    /**
     * @param uiEndDate The uiEndDate
     */
    @JsonProperty("uiEndDate")
    public void setUiEndDate(String uiEndDate) {
        this.uiEndDate = uiEndDate;
    }

    /**
     * @return The scorables
     */
    @JsonProperty("scorables")
    public List<Scorable> getScorables() {
        return scorables;
    }

    /**
     * @param scorables The scorables
     */
    @JsonProperty("scorables")
    public void setScorables(List<Scorable> scorables) {
        this.scorables = scorables;
    }

    /**
     * @return The weightValue
     */
    @JsonProperty("weightValue")
    public Integer getWeightValue() {
        return weightValue;
    }

    /**
     * @param weightValue The weightValue
     */
    @JsonProperty("weightValue")
    public void setWeightValue(Integer weightValue) {
        this.weightValue = weightValue;
    }

    @JsonProperty("rankings")
    public Rankings getRankings() {
        return rankings;
    }

    @JsonProperty("rankings")
    public void setRankings(Rankings rankings) {
        this.rankings = rankings;
    }

    @JsonProperty("pressesStatus")
    public HashMap<String, PressStatus> getPressesStatus() {
        return pressesStatus;
    }

    @JsonProperty("pressesStatus")
    public void setPressesStatus(HashMap<String, PressStatus> pressesStatus) {
        this.pressesStatus = pressesStatus;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }


    @Override
    public String toString() {
        return "WeekData{" +
                "name='" + name + '\'' +
                ", startTime='" + startTime + '\'' +
                ", endTime='" + endTime + '\'' +
                ", weeklyPoints=" + weeklyPoints +
                ", totalPoints=" + totalPoints +
                ", weeks=" + weeks +
                ", weeklyAv=" + weeklyAv +
                ", kpiScoreState='" + kpiScoreState + '\'' +
                ", uiStartDate='" + uiStartDate + '\'' +
                ", lastWeightValue=" + lastWeightValue +
                ", uiEndDate='" + uiEndDate + '\'' +
                ", scorables=" + scorables +
                ", weightValue=" + weightValue +
                ", rankings=" + rankings +
                ", additionalProperties=" + additionalProperties +
                '}';
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class Scorable {

        @JsonProperty("sortingValue")
        private Integer sortingValue;
        @JsonProperty("name")
        private String name;
        @JsonProperty("value")
        private Integer value;
        @JsonProperty("targetValue")
        private int targetValue;
        @JsonProperty("lastValue")
        private Integer lastValue;
        @JsonProperty("average")
        private Integer average;
        @JsonProperty("score")
        private Integer score;
        @JsonProperty("lastScore")
        private Integer lastScore;
        @JsonProperty("lblFormat")
        private String lblFormat;
        @JsonProperty("dailyBest")
        private Integer dailyBest;
        @JsonProperty("isWeight")
        private Boolean isWeight;
        @JsonProperty("maxScore")
        private Integer maxScore;
        @JsonProperty("kpi")
        private Object kpi;
        @JsonProperty("kpiScoreState")
        private String kpiScoreState;
        @JsonProperty("kpiScoreTrend")
        private String kpiScoreTrend;
        @JsonProperty("iconKey")
        private Object iconKey;
        @JsonProperty("unitKey")
        private Object unitKey;
        @JsonProperty("customText")
        private String customText;
        @JsonProperty("kpiScoreExplanation")
        List<KpiExplanationItem> kpiExplanationItems;
        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<String, Object>();

        @JsonProperty("kpiScoreExplanation")
        public List<KpiExplanationItem> getKpiExplanationItems() {
            return kpiExplanationItems;
        }

        @JsonProperty("kpiScoreExplanation")
        public void setKpiExplanationItems(List<KpiExplanationItem> kpiExplanationItems) {
            this.kpiExplanationItems = kpiExplanationItems;
        }

        @JsonProperty("targetValue")
        public int getTargetValue() {
            return targetValue;
        }

        @JsonProperty("targetValue")
        public void setTargetValue(int targetValue) {
            this.targetValue = targetValue;
        }

        @JsonProperty("customText")
        public String getCustomText() {
            return customText;
        }

        @JsonProperty("customText")
        public void setCustomText(String customText) {
            this.customText = customText;
        }

        /**
         * @return The sortingValue
         */
        @JsonProperty("sortingValue")
        public Integer getSortingValue() {
            return sortingValue;
        }

        /**
         * @param sortingValue The sortingValue
         */
        @JsonProperty("sortingValue")
        public void setSortingValue(Integer sortingValue) {
            this.sortingValue = sortingValue;
        }

        /**
         * @return The name
         */
        @JsonProperty("name")
        public String getName() {
            return name;
        }

        /**
         * @param name The name
         */
        @JsonProperty("name")
        public void setName(String name) {
            this.name = name;
        }

        /**
         * @return The value
         */
        @JsonProperty("value")
        public Integer getValue() {
            return value;
        }

        /**
         * @param value The value
         */
        @JsonProperty("value")
        public void setValue(Integer value) {
            this.value = value;
        }

        /**
         * @return The lastValue
         */
        @JsonProperty("lastValue")
        public Integer getLastValue() {
            return lastValue;
        }

        /**
         * @param lastValue The lastValue
         */
        @JsonProperty("lastValue")
        public void setLastValue(Integer lastValue) {
            this.lastValue = lastValue;
        }

        /**
         * @return The average
         */
        @JsonProperty("average")
        public Integer getAverage() {
            return average;
        }

        /**
         * @param average The average
         */
        @JsonProperty("average")
        public void setAverage(Integer average) {
            this.average = average;
        }

        /**
         * @return The score
         */
        @JsonProperty("score")
        public Integer getScore() {
            return score;
        }

        /**
         * @param score The score
         */
        @JsonProperty("score")
        public void setScore(Integer score) {
            this.score = score;
        }

        /**
         * @return The lastScore
         */
        @JsonProperty("lastScore")
        public Integer getLastScore() {
            return lastScore;
        }

        /**
         * @param lastScore The lastScore
         */
        @JsonProperty("lastScore")
        public void setLastScore(Integer lastScore) {
            this.lastScore = lastScore;
        }

        /**
         * @return The lblFormat
         */
        @JsonProperty("lblFormat")
        public String getLblFormat() {
            return lblFormat;
        }

        /**
         * @param lblFormat The lblFormat
         */
        @JsonProperty("lblFormat")
        public void setLblFormat(String lblFormat) {
            this.lblFormat = lblFormat;
        }

        /**
         * @return The dailyBest
         */
        @JsonProperty("dailyBest")
        public Integer getDailyBest() {
            return dailyBest;
        }

        /**
         * @param dailyBest The dailyBest
         */
        @JsonProperty("dailyBest")
        public void setDailyBest(Integer dailyBest) {
            this.dailyBest = dailyBest;
        }

        /**
         * @return The isWeight
         */
        @JsonProperty("isWeight")
        public Boolean getIsWeight() {
            return isWeight;
        }

        /**
         * @param isWeight The isWeight
         */
        @JsonProperty("isWeight")
        public void setIsWeight(Boolean isWeight) {
            this.isWeight = isWeight;
        }

        /**
         * @return The maxScore
         */
        @JsonProperty("maxScore")
        public Integer getMaxScore() {
            return maxScore;
        }

        /**
         * @param maxScore The maxScore
         */
        @JsonProperty("maxScore")
        public void setMaxScore(Integer maxScore) {
            this.maxScore = maxScore;
        }

        /**
         * @return The kpi
         */
        @JsonProperty("kpi")
        public Object getKpi() {
            return kpi;
        }

        /**
         * @param kpi The kpi
         */
        @JsonProperty("kpi")
        public void setKpi(Object kpi) {
            this.kpi = kpi;
        }

        /**
         * @return The kpiScoreState
         */
        @JsonProperty("kpiScoreState")
        public String getKpiScoreState() {
            return kpiScoreState;
        }

        /**
         * @param kpiScoreState The kpiScoreState
         */
        @JsonProperty("kpiScoreState")
        public void setKpiScoreState(String kpiScoreState) {
            this.kpiScoreState = kpiScoreState;
        }

        /**
         * @return The kpiScoreTrend
         */
        @JsonProperty("kpiScoreTrend")
        public String getKpiScoreTrend() {
            return kpiScoreTrend;
        }

        /**
         * @param kpiScoreTrend The kpiScoreTrend
         */
        @JsonProperty("kpiScoreTrend")
        public void setKpiScoreTrend(String kpiScoreTrend) {
            this.kpiScoreTrend = kpiScoreTrend;
        }

        /**
         * @return The iconKey
         */
        @JsonProperty("iconKey")
        public Object getIconKey() {
            return iconKey;
        }

        /**
         * @param iconKey The iconKey
         */
        @JsonProperty("iconKey")
        public void setIconKey(Object iconKey) {
            this.iconKey = iconKey;
        }

        /**
         * @return The unitKey
         */
        @JsonProperty("unitKey")
        public Object getUnitKey() {
            return unitKey;
        }

        /**
         * @param unitKey The unitKey
         */
        @JsonProperty("unitKey")
        public void setUnitKey(Object unitKey) {
            this.unitKey = unitKey;
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }

        @Override
        public String toString() {
            final StringBuffer sb = new StringBuffer("Scorable{");
            sb.append("sortingValue=").append(sortingValue);
            sb.append(", name='").append(name).append('\'');
            sb.append(", value=").append(value);
            sb.append(", lastValue=").append(lastValue);
            sb.append(", average=").append(average);
            sb.append(", score=").append(score);
            sb.append(", lastScore=").append(lastScore);
            sb.append(", lblFormat='").append(lblFormat).append('\'');
            sb.append(", dailyBest=").append(dailyBest);
            sb.append(", isWeight=").append(isWeight);
            sb.append(", maxScore=").append(maxScore);
            sb.append(", kpi=").append(kpi);
            sb.append(", kpiScoreState='").append(kpiScoreState).append('\'');
            sb.append(", kpiScoreTrend='").append(kpiScoreTrend).append('\'');
            sb.append(", iconKey=").append(iconKey);
            sb.append(", unitKey=").append(unitKey);
            sb.append(", additionalProperties=").append(additionalProperties);
            sb.append('}');
            return sb.toString();
        }
    }


    public static class KpiExplanationItem {
        @JsonProperty("name")
        private String name;
        @JsonProperty("state")
        private String state;
        @JsonProperty("min")
        private ValueItem min;
        @JsonProperty("max")
        private ValueItem max;
        @JsonProperty("impressions")
        private ValueItem impressions;
        @JsonProperty("value")
        private ValueItem value;
        @JsonProperty("score")
        private ValueItem score;
        @JsonProperty("limit")
        private int limit;
        @JsonProperty("properties")
        private List<Property> properties;
        @JsonProperty("showBar")
        private boolean showBar;
        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<String, Object>();

        @JsonProperty("name")
        public String getName() {
            return name;
        }

        @JsonProperty("name")
        public void setName(String name) {
            this.name = name;
        }

        @JsonProperty("state")
        public String getState() {
            return state;
        }

        @JsonProperty("state")
        public void setState(String state) {
            this.state = state;
        }

        @JsonProperty("min")
        public ValueItem getMin() {
            return min;
        }

        @JsonProperty("min")
        public void setMin(ValueItem min) {
            this.min = min;
        }

        @JsonProperty("max")
        public ValueItem getMax() {
            return max;
        }

        @JsonProperty("max")
        public void setMax(ValueItem max) {
            this.max = max;
        }

        @JsonProperty("impressions")
        public ValueItem getImpressions() {
            return impressions;
        }

        @JsonProperty("impressions")
        public void setImpressions(ValueItem impressions) {
            this.impressions = impressions;
        }

        @JsonProperty("value")
        public ValueItem getValue() {
            return value;
        }

        @JsonProperty("value")
        public void setValue(ValueItem value) {
            this.value = value;
        }

        @JsonProperty("score")
        public ValueItem getScore() {
            return score;
        }

        @JsonProperty("score")
        public void setScore(ValueItem score) {
            this.score = score;
        }

        @JsonProperty("limit")
        public int getLimit() {
            return limit;
        }

        @JsonProperty("limit")
        public void setLimit(int limit) {
            this.limit = limit;
        }

        @JsonProperty("properties")
        public List<Property> getProperties() {
            return properties;
        }

        @JsonProperty("properties")
        public void setProperties(List<Property> properties) {
            this.properties = properties;
        }

        @JsonProperty("showBar")
        public boolean isShowBar() {
            return showBar;
        }

        @JsonProperty("showBar")
        public void setShowBar(boolean showBar) {
            this.showBar = showBar;
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }
    }

    public static class ValueItem {
        @JsonProperty("unit")
        private String unit;
        @JsonProperty("value")
        private float value;
        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<String, Object>();

        @JsonProperty("unit")
        public String getUnit() {
            return unit;
        }

        @JsonProperty("unit")
        public void setUnit(String unit) {
            this.unit = unit;
        }

        @JsonProperty("value")
        public float getValue() {
            return value;
        }

        @JsonProperty("value")
        public void setValue(float value) {
            this.value = value;
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }
    }

    public static class Property {
        @JsonProperty("unit")
        private String unit;
        @JsonProperty("value")
        private float value;
        @JsonProperty("name")
        private String name;
        @JsonProperty("colored")
        private boolean colored;
        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<String, Object>();

        @JsonProperty("unit")
        public String getUnit() {
            return unit;
        }

        @JsonProperty("unit")
        public void setUnit(String unit) {
            this.unit = unit;
        }

        @JsonProperty("value")
        public float getValue() {
            return value;
        }

        @JsonProperty("value")
        public void setValue(float value) {
            this.value = value;
        }

        @JsonProperty("name")
        public String getName() {
            return name;
        }

        @JsonProperty("name")
        public void setName(String name) {
            this.name = name;
        }

        @JsonProperty("colored")
        public boolean isColored() {
            return colored;
        }

        @JsonProperty("colored")
        public void setColored(boolean colored) {
            this.colored = colored;
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }
    }

    public static class PressStatus {

        @JsonProperty("readyDate")
        private String readyDate;
        @JsonProperty("status")
        private String status;
        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<String, Object>();

        @JsonProperty("readyDate")
        public String getReadyDate() {
            return readyDate;
        }

        @JsonProperty("readyDate")
        public void setReadyDate(String readyDate) {
            this.readyDate = readyDate;
        }

        @JsonProperty("status")
        public String getStatus() {
            return status;
        }

        @JsonProperty("status")
        public void setStatus(String status) {
            this.status = status;
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public class Rankings {

        @JsonProperty("subRegion")
        private Area subRegion;
        @JsonProperty("region")
        private Area region;
        @JsonProperty("worldWide")
        private Area worldWide;

        @JsonProperty("subRegion")
        public Area getSubRegion() {
            return subRegion;
        }

        @JsonProperty("subRegion")
        public void setSubRegion(Area subRegion) {
            this.subRegion = subRegion;
        }

        @JsonProperty("region")
        public Area getRegion() {
            return region;
        }

        @JsonProperty("region")
        public void setRegion(Area region) {
            this.region = region;
        }

        @JsonProperty("worldWide")
        public Area getWorldWide() {
            return worldWide;
        }

        @JsonProperty("worldWide")
        public void setWorldWide(Area worldWide) {
            this.worldWide = worldWide;
        }

        @Override
        public String toString() {
            return "Rankings{" +
                    "subRegion=" + subRegion +
                    ", region=" + region +
                    ", worldWide=" + worldWide +
                    '}';
        }

        @JsonInclude(JsonInclude.Include.NON_NULL)
        public class Area {

            @JsonProperty("name")
            private String name;
            @JsonProperty("position")
            private int position;
            @JsonProperty("total")
            private int total;
            @JsonProperty("offset")
            private int offset;
            @JsonProperty("isTop")
            private boolean isTop;
            @JsonProperty("isLowerThanThreshold")
            private boolean isLowerThanThreshold;

            @JsonProperty("name")
            public String getName() {
                return name;
            }

            @JsonProperty("name")
            public void setName(String name) {
                this.name = name;
            }

            @JsonProperty("position")
            public int getPosition() {
                return position;
            }

            @JsonProperty("position")
            public void setPosition(int position) {
                this.position = position;
            }

            @JsonProperty("total")
            public int getTotal() {
                return total;
            }

            @JsonProperty("total")
            public void setTotal(int total) {
                this.total = total;
            }

            @JsonProperty("offset")
            public int getOffset() {
                return offset;
            }

            @JsonProperty("offset")
            public void setOffset(int offset) {
                this.offset = offset;
            }

            @JsonProperty("isTop")
            public boolean isTop() {
                return isTop;
            }

            @JsonProperty("isTop")
            public void setTop(boolean top) {
                isTop = top;
            }

            @JsonProperty("isLowerThanThreshold")
            public boolean isLowerThanThreshold() {
                return isLowerThanThreshold;
            }

            @JsonProperty("isLowerThanThreshold")
            public void setLowerThanThreshold(boolean lowerThanThreshold) {
                isLowerThanThreshold = lowerThanThreshold;
            }

            @Override
            public String toString() {
                return "Area{" +
                        "name='" + name + '\'' +
                        ", position=" + position +
                        ", total=" + total +
                        ", offset=" + offset +
                        ", isTop=" + isTop +
                        ", isLowerThanThreshold=" + isLowerThanThreshold +
                        '}';
            }
        }

    }
}

