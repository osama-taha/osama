package com.hp.printosmobile.presentation.modules.printersnapshot;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;
import com.hp.printosmobile.presentation.modules.shared.DeviceViewModel;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Anwar Asbah on 5/4/2016.
 */
public class PrinterSnapshotAdapter extends RecyclerView.Adapter<PrinterSnapshotAdapter.PrinterSnapshotViewHolder> {

    public Context context;
    public List<DeviceViewModel> devicesViewModels;

    private static final int PRINTER_VIEW_TYPE = 0;

    private PrinterSnapshotAdapterCallback callback;

    public PrinterSnapshotAdapter(Context context, List<DeviceViewModel> deviceViewModels, BusinessUnitEnum businessUnitEnum,
                                  PrinterSnapshotAdapterCallback callback) {
        this.context = context;
        this.devicesViewModels = deviceViewModels;
        this.callback = callback;
    }

    @Override
    public int getItemViewType(int position) {
        return PRINTER_VIEW_TYPE;
    }

    @Override
    public PrinterSnapshotViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View printerView = LayoutInflater.from(context).inflate(R.layout.printer_snapshot_item, null);

        return new PrinterSnapshotViewHolder(printerView);

    }

    @Override
    public void onBindViewHolder(PrinterSnapshotViewHolder holder, int position) {

        DeviceViewModel deviceViewModel = devicesViewModels.get(position);

        Picasso.with(context).load(deviceViewModel.getDeviceImageResource())
                .error(R.drawable.press_default)
                .into(holder.imagePrinter);

        holder.textPrinterName.setText(deviceViewModel.getName());

    }

    @Override
    public int getItemCount() {

        return devicesViewModels == null ? 0 : devicesViewModels.size();
    }

    class PrinterSnapshotViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.text_printer_name)
        TextView textPrinterName;
        @Bind(R.id.image_printer)
        ImageView imagePrinter;

        public PrinterSnapshotViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface PrinterSnapshotAdapterCallback {
        //Mainly for PWP device drill down.
    }
}
