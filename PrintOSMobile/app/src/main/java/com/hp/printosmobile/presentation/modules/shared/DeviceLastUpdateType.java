package com.hp.printosmobile.presentation.modules.shared;

import android.content.Context;
import android.text.format.DateUtils;

import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;
import com.hp.printosmobilelib.core.utils.HPDateUtils;

import java.util.Date;

/**
 * Created by Osama Taha on 5/14/17.
 */

public enum DeviceLastUpdateType {

    DATA_UP_TO_DATE(-1),
    LAST_UPDATE_IN_24HRS_TODAY(R.string.device_data_is_not_up_to_date_last_update_today),
    LAST_UPDATE_IN_24HRS_YESTERDAY(R.string.device_data_is_not_up_to_date_last_update_yesterday),
    LAST_UPDATE_YESTERDAY(R.string.device_data_is_not_up_to_date_last_update_yesterday),
    LAST_UPDATE_DAYS_AGO(R.string.device_data_is_not_up_to_date_last_update_days_ago),
    DATA_UNAVAILABLE(R.string.device_date_unavailable),
    WRONG_TIME_ZONE(R.string.wrong_time_zone_msg);

    private final int displayMessageResourceId;

    DeviceLastUpdateType(int displayMessageResourceId) {
        this.displayMessageResourceId = displayMessageResourceId;
    }

    public static DeviceLastUpdateType from(Date date, BusinessUnitEnum businessUnitEnum, boolean isWrongTZ) {

        if (businessUnitEnum == BusinessUnitEnum.LATEX_PRINTER) {

            if (isWrongTZ) {
              return WRONG_TIME_ZONE;
            } else if (date == null) {
                return DATA_UNAVAILABLE;
            } else {

                Date now = new Date();
                long timeDiff = now.getTime() - date.getTime();

                if (timeDiff <= 2.5 * DateUtils.HOUR_IN_MILLIS) {

                    return DATA_UP_TO_DATE;

                } else {

                    boolean isDateInToday = HPDateUtils.isToday(date);
                    boolean isDateInYesterday = HPDateUtils.isYesterday(date, null);

                    if (timeDiff <= DateUtils.DAY_IN_MILLIS) {

                        if (isDateInToday) {
                            return LAST_UPDATE_IN_24HRS_TODAY;
                        } else if (isDateInYesterday) {
                            return LAST_UPDATE_IN_24HRS_YESTERDAY;
                        }

                    } else {

                        if (isDateInYesterday) {
                            return LAST_UPDATE_YESTERDAY;
                        } else {
                            return LAST_UPDATE_DAYS_AGO;
                        }
                    }

                }

            }
        }

        return DATA_UP_TO_DATE;
    }

    public int getDisplayMessageResourceId() {
        return displayMessageResourceId;
    }

    public String getDisplayMessage(Context context, Date lastUpdateDate) {

        switch (this) {
            case LAST_UPDATE_IN_24HRS_TODAY:
            case LAST_UPDATE_IN_24HRS_YESTERDAY:
            case LAST_UPDATE_YESTERDAY:
                String time = HPDateUtils.getTime(context, lastUpdateDate, null);
                return context.getString(getDisplayMessageResourceId(), time);
            case LAST_UPDATE_DAYS_AGO:
                return context.getString(getDisplayMessageResourceId(),
                        HPDateUtils.getDifferenceDays(lastUpdateDate, new Date()));
            case DATA_UNAVAILABLE:
            case WRONG_TIME_ZONE:
                return context.getString(getDisplayMessageResourceId());
            default:
                return "";
        }
    }
}
