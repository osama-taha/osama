package com.hp.printosmobile.presentation.modules.home;

import android.Manifest;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hp.printosmobile.AppseeSdk;
import com.hp.printosmobile.Constants;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.BaseFragment;
import com.hp.printosmobile.presentation.indigowidget.PrintOSAppWidgetProvider;
import com.hp.printosmobile.presentation.modules.kpiview.KPIViewModel;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitViewModel;
import com.hp.printosmobile.presentation.modules.main.IMainFragment;
import com.hp.printosmobile.presentation.modules.notificationslist.NotificationListPresenter;
import com.hp.printosmobile.presentation.modules.notificationslist.NotificationListView;
import com.hp.printosmobile.presentation.modules.notificationslist.NotificationViewModel;
import com.hp.printosmobile.presentation.modules.personaladvisor.AdviceViewModel;
import com.hp.printosmobile.presentation.modules.personaladvisor.PersonalAdvisorDialog;
import com.hp.printosmobile.presentation.modules.personaladvisor.presenter.PersonalAdvisorFactory;
import com.hp.printosmobile.presentation.modules.printersnapshot.PrinterSnapshotPanel;
import com.hp.printosmobile.presentation.modules.printersnapshot.PrinterSnapshotViewModel;
import com.hp.printosmobile.presentation.modules.productionsnapshot.indigo.IndigoProductionPanel;
import com.hp.printosmobile.presentation.modules.productionsnapshot.indigo.ProductionViewModel;
import com.hp.printosmobile.presentation.modules.productionsnapshot.latex.LatexProductionPanel;
import com.hp.printosmobile.presentation.modules.servicecallpanel.ServiceCallPanel;
import com.hp.printosmobile.presentation.modules.servicecallpanel.ServiceCallViewModel;
import com.hp.printosmobile.presentation.modules.servicecallpanel.ServiceCallViewModel.ServiceCallStateEnum;
import com.hp.printosmobile.presentation.modules.servicecallpanel.ServiceCallsFragment;
import com.hp.printosmobile.presentation.modules.shared.DeviceViewModel;
import com.hp.printosmobile.presentation.modules.today.TodayPanel;
import com.hp.printosmobile.presentation.modules.today.TodayViewModel;
import com.hp.printosmobile.presentation.modules.todayhistogram.HistogramUtility;
import com.hp.printosmobile.presentation.modules.todayhistogram.TodayHistogramViewModel;
import com.hp.printosmobile.presentation.modules.week.WeekCollectionViewModel;
import com.hp.printosmobile.presentation.modules.week.WeekPanel;
import com.hp.printosmobile.presentation.modules.week.WeekPressStatusDialog;
import com.hp.printosmobile.presentation.modules.week.WeekViewModel;
import com.hp.printosmobile.presentation.modules.week.kpiexplanationdialog.KpiExplanationDialog;
import com.hp.printosmobile.utils.HPUIUtils;
import com.hp.printosmobilelib.core.communications.remote.APIException;
import com.hp.printosmobilelib.core.logging.HPLogger;
import com.hp.printosmobilelib.ui.common.HPFragment;
import com.hp.printosmobilelib.ui.widgets.HPScrollView;
import com.hp.printosmobilelib.ui.widgets.panel.PanelView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;


public class HomeFragment extends BaseFragment implements ServiceCallPanel.ServiceCallPanelCallback, HomeView, IMainFragment, SwipeRefreshLayout.OnRefreshListener, PrinterSnapshotPanel.PrinterSnapshotCallback, TodayPanel.TodayPanelCallbacks, WeekPanel.WeekPanelCallbacks, IndigoProductionPanel.IndigoProductionPanelCallback, NotificationListView {

    private static final String TAG = HomeFragment.class.getSimpleName();

    private static final int BADGE_MAX_NUMBER_OF_NOTIFICATIONS = 99;
    private static final long SWIPE_TO_REFRESH_DELAY_IN_MILLS = 500;
    private static final int READ_WRITE_EXTERNAL_STORAGE_PERMISSION = 1;
    private static final long SCROLL_ANIMATION_DURATION = 400;
    private static final long SCROLL_TO_PANEL_DELAY = 200;
    private static final String FOCUSABLE_PANEL_HISTOGRAM_EXTRA = "1";

    private Map<Panel, PanelView> panelViewMap = new HashMap<>();

    @Bind(R.id.panels_container)
    LinearLayout panelsContainerView;
    @Bind(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    @Bind(R.id.error_msg_text_view)
    TextView errorMsgTextView;
    @Bind(R.id.try_again_text_view)
    TextView tryAgainTextView;
    @Bind(R.id.error_layout)
    View errorLayout;
    @Bind(R.id.parent_container)
    View parentContainer;
    @Bind(R.id.scroll_view)
    HPScrollView scrollView;
    @Bind(R.id.fab)
    ImageView fab;

    private Panel focusablePanel;

    private HPFragment hpDetailsFragment;
    private HomeFragmentCallBack callBack;
    private HomePresenter homePresenter;
    private NotificationListPresenter notificationListPresenter;
    private FabArrowDirection fabArrowDirection = FabArrowDirection.DOWN;

    private BusinessUnitViewModel businessUnitViewModel;
    private Panel panelRequestedSharing;

    private boolean openServiceCallFragment = false;

    public HomeFragment() {
        // Required empty public constructor
    }

    public static HomeFragment newInstance() {
        HomeFragment homeFragment = new HomeFragment();
        return homeFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initView();
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            callBack = (HomeFragmentCallBack) getActivity();
        } catch (ClassCastException e) {
            throw new RuntimeException("parent activity must implement HomeFragmentCallBack");
        }
    }

    @Override
    public boolean isIntercomAccessible() {
        return true;
    }

    private void initView() {

        swipeRefreshLayout.setOnRefreshListener(this);
        homePresenter = new HomePresenter();
        homePresenter.attachView(this);

        notificationListPresenter = new NotificationListPresenter();
        notificationListPresenter.attachView(this);

        tryAgainTextView.setClickable(true);
        tryAgainTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tryAgain();
            }
        });


        scrollView.setOnScrollStoppedListener(new HPScrollView.OnScrollStopListener() {
            @Override
            public void onScrollStopped(int y) {
                //fab.setVisibility(View.VISIBLE);
            }

            @Override
            public void onScrollDown() {
                //fab.setVisibility(View.GONE);
            }

            @Override
            public void onScrollUp() {
                //fab.setVisibility(View.GONE);
            }

            @Override
            public void onTopReached() {
                updateFABArrowDirection(FabArrowDirection.DOWN);
                fab.setVisibility(View.VISIBLE);
            }

            @Override
            public void onBottomReached() {
                updateFABArrowDirection(FabArrowDirection.UP);
                fab.setVisibility(View.VISIBLE);
            }
        });


        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (fabArrowDirection == FabArrowDirection.UP) {

                    scrollToTop();

                } else {

                    scrollToBottom();
                }
            }
        });
    }

    public void tryAgain () {
        onRefresh();
        if (callBack != null) {
            callBack.reload();
            PrintOSAppWidgetProvider.updateAllWidgets(getContext());
        }
    }

    public void setFocusablePanel(Panel panel, String extra) {
        this.focusablePanel = panel;

        if (panel == Panel.TODAY && extra != null && extra.equals(FOCUSABLE_PANEL_HISTOGRAM_EXTRA)) {
            final PanelView panelView = panelViewMap.get(panel);
            if (panelView instanceof TodayPanel) {
                ((TodayPanel) panelView).setAutomaticSwipeEnabled(false);
            }
        }
    }

    private void scrollFragmentToPanel(final Panel panel) {
        focusablePanel = panel;
        if (focusablePanel == null || panelViewMap == null || !panelViewMap.containsKey(panel)) {
            return;
        }

        final PanelView panelView = panelViewMap.get(panel);

        scrollView.postDelayed(new Runnable() {
            @Override
            public void run() {
                scrollView.scrollTo(0, panelView.getTop());
            }
        }, SCROLL_TO_PANEL_DELAY);

    }

    private void scrollToBottom() {

        View lastChild = scrollView.getChildAt(scrollView.getChildCount() - 1);
        int bottom = lastChild.getBottom() + scrollView.getPaddingBottom();
        int sy = 0;
        int sh = scrollView.getHeight();
        final int delta = bottom - (sy + sh);

        ObjectAnimator objectAnimator = ObjectAnimator.ofInt(scrollView, "scrollY", 0, delta).setDuration(SCROLL_ANIMATION_DURATION);
        objectAnimator.start();

        //Revert arrow direction.
        updateFABArrowDirection(FabArrowDirection.UP);

    }


    private void scrollToTop() {

        ObjectAnimator objectAnimator = ObjectAnimator.ofInt(scrollView, "scrollY", 0).setDuration(SCROLL_ANIMATION_DURATION);
        objectAnimator.start();

        updateFABArrowDirection(FabArrowDirection.DOWN);

    }

    private void updateFABArrowDirection(FabArrowDirection arrowDirection) {

        fabArrowDirection = arrowDirection;
        if (fabArrowDirection == FabArrowDirection.DOWN) {
            //Revert arrow direction(DOWN).
            fab.setRotation(0);
        } else {
            //Revert arrow direction(UP).
            fab.setRotation(-180);
        }
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_home;
    }


    @Override
    public void onRefresh() {
        homePresenter.refresh();
        if(callBack != null) {
            callBack.onRefresh();
        }
        //Show the refresh view for 500ms then close.
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                swipeRefreshLayout.setRefreshing(false);
            }
        }, SWIPE_TO_REFRESH_DELAY_IN_MILLS);

    }

    @Override
    public void onPrinterSnapshotViewAllClicked() {

        if (callBack != null) {
            callBack.onPrinterSnapshotClicked();
        }

    }

    public boolean isDisplayingDetailsFragment() {
        return hpDetailsFragment != null;
    }

    @Override
    public void enableSwipeToRefresh(boolean enable) {
        swipeRefreshLayout.setEnabled(enable);
    }

    @Override
    public void onTodayViewInitialized() {
        if (focusablePanel != Panel.UNKNOWN) {
            scrollFragmentToPanel(focusablePanel);
            focusablePanel = Panel.UNKNOWN;
        }
    }

    @Override
    public void onTodayViewSelected() {

        final PanelView panelView = panelViewMap.get(Panel.TODAY);
        scrollView.scrollTo(0, panelView.getTop());

    }

    @Override
    public void setHasRTDevices(boolean hasRTDevices) {
        if(callBack != null) {
            callBack.setHasRtDevices(hasRTDevices);
        }
    }

    @Override
    public void loadHistogramData() {
        homePresenter.getActualVsTargetData(getActivity());
    }

    @Override
    public void resetHistogramData() {
        callBack.onHistogramDataRetrieved(null);
    }

    @Override
    public void onTodayPanelViewAllClicked() {

        if (callBack != null) {
            callBack.onTodayViewAllClicked();
        }

    }

    @Override
    public void onDeviceClicked(DeviceViewModel model) {
        if (callBack != null) {
            callBack.onTodayPanelDeviceClicked(model);
        }
    }

    @Override
    public void onHasAdviceIconClicked(DeviceViewModel model) {

        if (model.hasUnsuppressedAdvices()) {

            PersonalAdvisorDialog dialog = PersonalAdvisorDialog.newInstance(model.getPersonalAdvisorViewModel(),
                    new PersonalAdvisorDialog.PersonalAdvisorDialogCallback() {
                        @Override
                        public void goToPersonalAdvisor(AdviceViewModel adviceViewModel) {
                            if(callBack != null){
                                callBack.scrollToAdvice(adviceViewModel);
                            }
                        }

                        @Override
                        public void onAdvicePopupDismiss() {
                            if (callBack != null) {
                                callBack.onAdvicePopupDismissed();
                            }
                        }

                        @Override
                        public void onScreenshotCreated(Panel personalAdvisor, Uri storageUri) {
                            HomeFragment.this.onScreenshotCreated(personalAdvisor, storageUri);
                        }
                    });

            if (callBack != null) {
                callBack.onAdvicePopupDisplayed();
            }
            dialog.show(getFragmentManager(), TAG);
        }
    }

    @Override
    public void onShiftSelected(ShiftViewModel shiftViewModel) {
        boolean isShift = shiftViewModel.getShiftType() == ShiftViewModel.ShiftType.CURRENT;
        homePresenter.setShiftSupport(isShift);
        getTodayData(false);

        if (callBack != null) {
            callBack.onShiftSelected(isShift);
        }

    }

    @Override
    public void onBusinessUnitSelected(BusinessUnitViewModel businessUnitModel) {

        if (businessUnitModel == null) {
            return;
        }

        fab.setVisibility(View.VISIBLE);

        boolean reloadPanels = this.businessUnitViewModel == null || this.businessUnitViewModel.getBusinessUnit() != businessUnitModel.getBusinessUnit();
        swipeRefreshLayout.setEnabled(true);
        this.businessUnitViewModel = businessUnitModel;
        this.homePresenter.setSelectedBusinessUnit(businessUnitModel);

        if (reloadPanels) {
            panelsContainerView.removeAllViews();
            panelViewMap.clear();
        }

        for (Panel panel : businessUnitModel.getPanels()) {
            initPanel(panel, reloadPanels);
            getData(panel);
        }

        PersonalAdvisorFactory.getInstance().loadData(businessUnitModel);

        notificationListPresenter.getUserNotifications();

        errorLayout.setVisibility(View.GONE);
        panelsContainerView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onFiltersChanged(BusinessUnitViewModel businessUnitViewModel) {

        HPLogger.d(TAG, "Filter changed " + TAG);

        this.homePresenter.setShiftSupport(false);
        this.homePresenter.setSelectedBusinessUnit(businessUnitViewModel);

        for (Panel panel : businessUnitViewModel.getPanels()) {
            PanelView panelView = panelViewMap.get(panel);
            if (panelView != null) {
                panelView.showLoading();
                if (panelView instanceof TodayPanel) {
                    ((TodayPanel) panelView).onFilterSelected();
                }
            }
            getData(panel);
        }

        if(hpDetailsFragment instanceof ServiceCallsFragment){
            ((ServiceCallsFragment) hpDetailsFragment).onFilterSelected();
        }

        notificationListPresenter.getUserNotifications();
    }

    private void getData(Panel panel) {
        switch (panel) {
            case LATEX_PRODUCTION:
            case INDIGO_PRODUCTION:
                break;
            case TODAY:
                getTodayData(true);
                break;
            case PRINTERS:
                //TEMP
                if (businessUnitViewModel.getBusinessUnit() == BusinessUnitEnum.IHPS_PRESS) {
                    homePresenter.getPrintersData(getActivity());
                }
                break;
            case PERFORMANCE:
                homePresenter.getWeekData(getActivity());
                break;
            case PANEL_SERVICE_CALL:
                homePresenter.getServiceCalls(getActivity());
                break;
        }
    }

    private void getTodayData(boolean loadShifts) {

        if (loadShifts) {
            homePresenter.getActiveShifts();
        }

        homePresenter.getTodayData();

    }

    private void initPanel(Panel panel, boolean reload) {
        PanelView panelView = null;
        if (reload || !panelViewMap.containsKey(panel)) {
            LinearLayout.LayoutParams layoutParams = getPanelLayoutParams();
            switch (panel) {
                case LATEX_PRODUCTION:
                    panelView = new LatexProductionPanel(getActivity());
                    panelView.showLoading();
                    break;
                case PANEL_SERVICE_CALL:
                    panelView = new ServiceCallPanel(getActivity());
                    ((ServiceCallPanel) panelView).setServiceCallPanelCallback(this);
                    panelView.showLoading();
                    break;
                case INDIGO_PRODUCTION:
                    panelView = new IndigoProductionPanel(getActivity());
                    ((IndigoProductionPanel) panelView).addCallback(this);
                    panelView.showLoading();
                    break;
                case TODAY:
                    panelView = new TodayPanel(getActivity());
                    ((TodayPanel) panelView).addTodayPanelCallbacks(this);
                    panelView.showLoading();
                    break;
                case PRINTERS:
                    panelView = new PrinterSnapshotPanel(getActivity());
                    ((PrinterSnapshotPanel) panelView).addPrinterSnapshotCallback(this);
                    panelView.showLoading();
                    break;
                case PERFORMANCE:
                    panelView = new WeekPanel(getActivity());
                    panelView.showLoading();
                    break;
            }
            if (panelView != null) {
                panelViewMap.put(panel, panelView);
                panelsContainerView.addView(panelView, layoutParams);
            }
        } else {
            panelView = panelViewMap.get(panel);
            panelView.onRefresh();
            panelView.showLoading();
        }
    }

    private LinearLayout.LayoutParams getPanelLayoutParams() {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        int verticalMargin = (int) getContext().getResources().getDimension(R.dimen.home_fragment_panels_divider_half_height);
        layoutParams.setMargins(verticalMargin, 0, 0, verticalMargin);

        return layoutParams;
    }

    @Override
    public void didFinishGettingBusinessUnits(Map<BusinessUnitEnum, BusinessUnitViewModel> businessUnitsMap) {

    }

    @Override
    public void updateProductionSnapshot(ProductionViewModel productionViewModel) {

        if (!panelViewMap.containsKey(Panel.INDIGO_PRODUCTION)) {
            return;
        }

        IndigoProductionPanel panel = (IndigoProductionPanel) panelViewMap.get(Panel.INDIGO_PRODUCTION);
        panel.updateViewModel(productionViewModel);
        panel.hideLoading();

    }

    @Override
    public void updateLatexProductionSnapshot(ProductionViewModel productionViewModel) {

        if (!panelViewMap.containsKey(Panel.LATEX_PRODUCTION)) {
            return;
        }

        LatexProductionPanel panel = (LatexProductionPanel) panelViewMap.get(Panel.LATEX_PRODUCTION);
        panel.updateViewModel(productionViewModel);
        panel.hideLoading();

    }

    @Override
    public void updateTodayPanel(TodayViewModel todayViewModel) {

        if (!panelViewMap.containsKey(Panel.TODAY)) {
            if(callBack != null){
                callBack.onTodayDataFailure();
            }
            return;
        }

        TodayPanel todayPanel = (TodayPanel) panelViewMap.get(Panel.TODAY);
        todayPanel.updateViewModel(todayViewModel);
        todayPanel.hideLoading();

        if (todayViewModel != null && todayViewModel.getDeviceViewModels() != null) {
            if(callBack != null) {
                callBack.onTodayPanelDevicesRetrieved(todayViewModel.getDeviceViewModels());
            }
        } else {
            if(callBack != null){
                callBack.onTodayDataFailure();
            }
        }
    }

    @Override
    public void updateTodayPanelWithActiveShifts(ActiveShiftsViewModel activeShiftsViewModel) {

        if (!panelViewMap.containsKey(Panel.TODAY)) {
            return;
        }

        TodayPanel todayPanel = (TodayPanel) panelViewMap.get(Panel.TODAY);
        todayPanel.updateActiveShifts(activeShiftsViewModel);

    }

    @Override
    public void updateHistogram(TodayHistogramViewModel todayHistogramViewModel) {

        if (!panelViewMap.containsKey(Panel.TODAY)) {
            return;
        }

        TodayPanel todayPanel = (TodayPanel) panelViewMap.get(Panel.TODAY);
        HistogramUtility.appendTodayDataToHistogramData(todayHistogramViewModel, todayPanel.getViewModel(), todayPanel.getActiveShiftsViewModel());

        setHistogramModel(todayHistogramViewModel);
    }

    public void setHistogramModel (TodayHistogramViewModel todayHistogramViewModel) {
        if (!panelViewMap.containsKey(Panel.TODAY)) {
            return;
        }

        TodayPanel todayPanel = (TodayPanel) panelViewMap.get(Panel.TODAY);
        todayPanel.updateActualVsTarget(todayHistogramViewModel);
        todayPanel.hideLoading();

        if (todayHistogramViewModel != null) {
            callBack.onHistogramDataRetrieved(todayHistogramViewModel);
        }
    }

    public TodayViewModel getTodayViewModel () {
        TodayPanel todayPanel = (TodayPanel) panelViewMap.get(Panel.TODAY);
        if(todayPanel == null){
            return null;
        }
        return todayPanel.getViewModel();
    }

    public ActiveShiftsViewModel getActiveShiftViewModel () {
        TodayPanel todayPanel = (TodayPanel) panelViewMap.get(Panel.TODAY);
        return todayPanel.getActiveShiftsViewModel();
    }

    @Override
    public void updateWeekPanel(WeekCollectionViewModel weekCollectionViewModel) {

        if (!panelViewMap.containsKey(Panel.PERFORMANCE)) {
            return;
        }

        WeekPanel weekPanel = (WeekPanel) panelViewMap.get(Panel.PERFORMANCE);
        weekPanel.updateViewModel(weekCollectionViewModel);
        weekPanel.hideLoading();
        weekPanel.addWeekPanelListener(this);

    }

    @Override
    public void onKpiSelected(String kpiName) {
        callBack.onKpiSelected(kpiName);
    }

    @Override
    public void onWeekPanelClicked() {
        callBack.onWeekPanelClicked();
    }

    @Override
    public void onPartialDataWeekClicked(List<WeekViewModel.WeekPressStatus> weekPressStatuses) {
        WeekPressStatusDialog.newInstance((ArrayList<WeekViewModel.WeekPressStatus>) weekPressStatuses,
                new WeekPressStatusDialog.WeekPressStatusDialogCallback() {
                    @Override
                    public void onDialogDismiss() {
                        if(callBack != null) {
                            callBack.onDialogDismissed();
                        }
                    }
                })
                .show(getFragmentManager(), "WeekPressStatusDialog");
        if(callBack != null) {
            callBack.onDialogDisplayed();
        }
    }

    @Override
    public void showKpiExplanationDialog(ArrayList<KPIViewModel> kpiList, int position) {
        KpiExplanationDialog kpiExplanationDialog = KpiExplanationDialog.newInstance(kpiList, position, new KpiExplanationDialog.KpiExplanationDialogCallback() {
            @Override
            public void onDialogDismiss() {
                callBack.onDialogDismissed();
            }
        });
        kpiExplanationDialog.show(getFragmentManager(), "KpiExplanationDialog");
        if(callBack != null) {
            callBack.onDialogDisplayed();
        }
        AppseeSdk.getInstance(getActivity()).sendEvent(AppseeSdk.EVENT_KPI_EXPLANATION_SHOWN);
    }

    @Override
    public void onShareButtonClicked(Panel panel) {

        this.panelRequestedSharing = panel;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            int writePermissionGrantedCode = ContextCompat.checkSelfPermission(PrintOSApplication.getAppContext(),
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE);

            int readPermissionGrantedCode = ContextCompat.checkSelfPermission(PrintOSApplication.getAppContext(),
                    Manifest.permission.READ_EXTERNAL_STORAGE);

            if (readPermissionGrantedCode != PackageManager.PERMISSION_GRANTED ||
                    writePermissionGrantedCode != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE},
                        READ_WRITE_EXTERNAL_STORAGE_PERMISSION);

            } else {
                performPanelSharing(panel);
            }
        } else {
            performPanelSharing(panel);
        }
    }

    public void performPanelSharing(Panel panel) {

        if (panelViewMap.containsKey(panel)) {

            PanelView panelView = panelViewMap.get(panel);
            panelView.sharePanel();

        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case READ_WRITE_EXTERNAL_STORAGE_PERMISSION: {
                if (grantResults.length > 0) {
                    boolean allGranted = true;
                    for (int i = 0; i < grantResults.length; i++) {
                        allGranted = allGranted && grantResults[i] == PackageManager.PERMISSION_GRANTED;
                    }
                    if (allGranted) {
                        performPanelSharing(panelRequestedSharing);
                    }
                }
                return;
            }
        }
    }

    @Override
    public void onScreenshotCreated(Panel panel, Uri screenshotUri) {
        shareImage(screenshotUri);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case Constants.IntentExtras.APPS_PICKER_INTENT_REQUEST_CODE:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                    case Activity.RESULT_CANCELED:
                        if (panelViewMap.containsKey(panelRequestedSharing)) {
                            panelViewMap.get(panelRequestedSharing).lockShareButton(false);
                        }
                        break;
                }
        }

    }

    @Override
    public void updatePrintersPanel(List<DeviceViewModel> deviceViewModels) {

        if (!panelViewMap.containsKey(Panel.PRINTERS)) {
            return;
        }

        final PrinterSnapshotPanel printerSnapshotPanel = (PrinterSnapshotPanel) panelViewMap.get(Panel.PRINTERS);

        PrinterSnapshotViewModel printerSnapshotViewModel = new PrinterSnapshotViewModel();
        printerSnapshotViewModel.setDevices(deviceViewModels);
        printerSnapshotViewModel.setBusinessUnitEnum(businessUnitViewModel == null ? BusinessUnitEnum.UNKNOWN : businessUnitViewModel.getBusinessUnit());

        printerSnapshotPanel.updateViewModel(printerSnapshotViewModel);
        printerSnapshotPanel.hideLoading();

    }

    @Override
    public void updateServiceCallPanel(ServiceCallViewModel serviceCallViewModel) {

        if (!panelViewMap.containsKey(Panel.PANEL_SERVICE_CALL)) {
            return;
        }

        ServiceCallPanel serviceCallPanel = (ServiceCallPanel) panelViewMap.get(Panel.PANEL_SERVICE_CALL);
        serviceCallPanel.updateViewModel(serviceCallViewModel);
        serviceCallPanel.hideLoading();

        boolean showPanel = serviceCallViewModel != null && serviceCallViewModel.getCallViewModels() != null
                && serviceCallViewModel.getCallViewModels().size() > 0;
        HPUIUtils.setVisibility(showPanel, serviceCallPanel);

        if(showPanel && openServiceCallFragment) {
            openServiceCallFragment = false;
            onServiceCallPanelClicked(serviceCallViewModel, serviceCallViewModel.getNumberOfOpenCalls() == 0 ?
                    ServiceCallStateEnum.CLOSED : ServiceCallStateEnum.OPEN);
        }

        if(hpDetailsFragment instanceof ServiceCallsFragment){
            if ((serviceCallViewModel == null || serviceCallViewModel.getCallViewModels() == null ||
                    serviceCallViewModel.getCallViewModels().isEmpty()) && hpDetailsFragment.isAdded()){
                onBackPressed();
            } else {
                ((ServiceCallsFragment) hpDetailsFragment).displayModels(serviceCallViewModel);
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        homePresenter.detachView();
        notificationListPresenter.detachView();
    }

    @Override
    public void onDestroy() {
        if(panelViewMap != null){
            for (Panel panel : panelViewMap.keySet()){
                panelViewMap.get(panel).onDestroy();
            }
        }
        super.onDestroy();
    }

    @Override
    public void onError(APIException exception, String... tags) {
        for (String tag : tags) {
            if(tag.equals(ServiceCallPanel.TAG)){
                if(hpDetailsFragment instanceof ServiceCallsFragment){
                    ((ServiceCallsFragment) hpDetailsFragment).onServiceCallDataRetrived();
                }
            }
            if(tag.equals(TodayPanel.TAG) && callBack != null){
                callBack.onTodayDataFailure();
            }
            HPUIUtils.displayToastException(getContext(), exception, tag, false);
            hideLoading(Panel.from(tag));
        }
    }

    public TodayPanel getTodayPanel() {

        PanelView panelView = getPanel(Panel.TODAY);
        if (panelView != null && panelView instanceof TodayPanel) {
            return (TodayPanel) panelView;
        }

        return null;
    }

    public PanelView getPanel(Panel panel) {
        if (panelViewMap.containsKey(panel)) {
            return panelViewMap.get(panel);
        }
        return null;
    }

    private void hideLoading(Panel panel) {
        if (panelViewMap != null && panelViewMap.containsKey(panel)) {
            panelViewMap.get(panel).hideLoading();
        }
    }

    @Override
    public void onMainError(String msg, boolean triggerSignOut) {
        if (errorLayout == null) {
            return;
        }

        if (triggerSignOut) {
            //HPUIUtils.displayToast(getContext(), msg, false);
            if(callBack != null) {
                callBack.triggerSignOut();
            }
            return;
        }

        swipeRefreshLayout.setEnabled(false);
        errorLayout.setVisibility(View.VISIBLE);
        panelsContainerView.setVisibility(View.GONE);

        errorMsgTextView.setText(msg);
        errorLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean respondToValidationPeriodExceedance() {
        return hpDetailsFragment != null;
    }

    @Override
    public void onHistogramViewAllClicked(TodayHistogramViewModel model) {

        if (model == null) {
            HPLogger.d(TAG, "last 7 days data are not available.");
            return;
        }

        callBack.onHistogramClicked(model);
    }

    @Override
    public boolean onBackPressed() {
        if (hpDetailsFragment != null) {
            FragmentTransaction trans = getFragmentManager()
                    .beginTransaction();
            trans.remove(hpDetailsFragment);
            trans.commit();
            hpDetailsFragment = null;
            callBack.onHistogramViewClosed(this);
            AppseeSdk.getInstance(getActivity()).startScreen(AppseeSdk.SCREEN_HOME);
            return true;
        }
        return false;
    }

    @Override
    public boolean isFragmentNameToBeDisplayed() {
        return hpDetailsFragment == null ? false : hpDetailsFragment.isFragmentNameToBeDisplayed();
    }

    @Override
    public boolean isFilteringAvailable() {
        if (hpDetailsFragment != null) {
            return hpDetailsFragment.isFilteringAvailable();
        }

        return true;
    }

    @Override
    public int getToolbarDisplayName() {
        return hpDetailsFragment == null ? R.string.unknown_value : hpDetailsFragment.getToolbarDisplayName();
    }

    @Override
    public void onJobsInQueueClicked(DeviceViewModel.JobType jobType) {
        callBack.onIndigoProductionQueueViewClicked(jobType);
    }

    public void updateNotificationBadge() {
        if (homePresenter != null) {
            notificationListPresenter.getUserNotifications();
        }
    }

    @Override
    public void onServiceCallPanelClicked(ServiceCallViewModel serviceCallViewModel, ServiceCallStateEnum serviceCallStateEnum) {

        FragmentTransaction transaction = getFragmentManager()
                .beginTransaction();

        hpDetailsFragment = ServiceCallsFragment.newInstance(serviceCallViewModel, serviceCallStateEnum);

        transaction.replace(R.id.parent_container, hpDetailsFragment);
        transaction.commit();

        callBack.onServiceCallClicked(hpDetailsFragment);
    }

    public void openServiceCallFragmentWhenLoaded(){
        this.openServiceCallFragment = true;
    }

    @Override
    public void updateNotificationListView(List<NotificationViewModel> notificationViewModels) {
        int count = 0;
        if(notificationViewModels != null){
            for(NotificationViewModel model : notificationViewModels){
                count += (model.isRead() ? 0 : 1);
            }
        }
        count = Math.min(count, BADGE_MAX_NUMBER_OF_NOTIFICATIONS);
        if (callBack != null) {
            callBack.setNumberOfNotifications(count);
        }
    }

    @Override
    public void onNotificationsError(APIException exception) {

    }

    @Override
    public void onAllNotificationsDeleted() {

    }

    @Override
    public void onDeleteAllNotificationsFailed(APIException e) {

    }

    public interface HomeFragmentCallBack {

        void onPrinterSnapshotClicked();

        void onTodayPanelDevicesRetrieved(List<DeviceViewModel> models);

        void onTodayDataFailure();

        void onTodayViewAllClicked();

        void onHistogramClicked(TodayHistogramViewModel model);

        void onHistogramViewClosed(HPFragment fragment);

        void onHistogramDataRetrieved(TodayHistogramViewModel model);

        void onKpiSelected(String kpiName);

        void onWeekPanelClicked();

        void onRefresh();

        void reload();

        void onShiftSelected(boolean isShiftSupport);

        void triggerSignOut();

        void onPrinterSnapshotLatexDeviceClicked(DeviceViewModel model);

        void onTodayPanelDeviceClicked(DeviceViewModel model);

        void onIndigoProductionQueueViewClicked(DeviceViewModel.JobType jobType);

        void setNumberOfNotifications(int numberOfNotifications);

        void onServiceCallClicked(HPFragment fragment);

        void onAdvicePopupDisplayed();

        void onAdvicePopupDismissed();

        void onDialogDisplayed();

        void onDialogDismissed();

        void setHasRtDevices(boolean hasRTDevices);

        void scrollToAdvice(AdviceViewModel adviceViewModel);
    }

    public enum FabArrowDirection {
        UP, DOWN
    }
}