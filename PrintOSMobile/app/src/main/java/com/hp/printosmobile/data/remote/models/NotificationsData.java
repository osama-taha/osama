package com.hp.printosmobile.data.remote.models;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Anwar Asbah
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
public class NotificationsData {

    @JsonProperty("notificationEvents")
    private List<NotificationEvent> notificationEvents;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("notificationEvents")
    public List<NotificationEvent> getNotificationEvents() {
        return notificationEvents;
    }

    @JsonProperty("notificationEvents")
    public void setNotificationEvents(List<NotificationEvent> notificationEvents) {
        this.notificationEvents = notificationEvents;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class NotificationEvent {
        @JsonProperty("id")
        private String id;
        @JsonProperty("total")
        private int total;
        @JsonProperty("notificationEventList")
        private List<NotificationItem> notificationItems;
        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<String, Object>();

        @JsonProperty("id")
        public String getId() {
            return id;
        }

        @JsonProperty("id")
        public void setId(String id) {
            this.id = id;
        }

        @JsonProperty("total")
        public int getTotal() {
            return total;
        }

        @JsonProperty("total")
        public void setTotal(int total) {
            this.total = total;
        }

        @JsonProperty("notificationEventList")
        public List<NotificationItem> getNotificationItems() {
            return notificationItems;
        }

        @JsonProperty("notificationEventList")
        public void setNotificationItems(List<NotificationItem> notificationItems) {
            this.notificationItems = notificationItems;
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }

        @JsonInclude(JsonInclude.Include.NON_NULL)
        public static class NotificationItem {
            @JsonProperty("id")
            private int id;
            @JsonProperty("userEventId")
            private int userEventId;
            @JsonProperty("eventDescriptionId")
            private String eventDescriptionId;
            @JsonProperty("subValue")
            private String subValue;
            @JsonProperty("acknowledged")
            private boolean acknowledged;
            @JsonProperty("timestamp")
            private long timestamp;
            @JsonProperty("entityId")
            private String entity;
            @JsonProperty("subValueParams")
            private List<String> subValueParams;
            @JsonProperty("goLink")
            private String goLink;
            @JsonProperty("linkLabel")
            private String linkLabel;
            @JsonIgnore
            private Map<String, Object> additionalProperties = new HashMap<String, Object>();

            @JsonProperty("id")
            public int getId() {
                return id;
            }

            @JsonProperty("id")
            public void setId(int id) {
                this.id = id;
            }

            @JsonProperty("userEventId")
            public int getUserEventId() {
                return userEventId;
            }

            @JsonProperty("userEventId")
            public void setUserEventId(int userEventId) {
                this.userEventId = userEventId;
            }

            @JsonProperty("subValueParams")
            public List<String> getSubValueParams() {
                return subValueParams;
            }

            @JsonProperty("subValueParams")
            public void setSubValueParams(List<String> subValueParams) {
                this.subValueParams = subValueParams;
            }

            @JsonProperty("eventDescriptionId")
            public String getEventDescriptionId() {
                return eventDescriptionId;
            }

            @JsonProperty("eventDescriptionId")
            public void setEventDescriptionId(String eventDescriptionId) {
                this.eventDescriptionId = eventDescriptionId;
            }

            @JsonProperty("subValue")
            public String getSubValue() {
                return subValue;
            }

            @JsonProperty("subValue")
            public void setSubValue(String subValue) {
                this.subValue = subValue;
            }

            @JsonProperty("acknowledged")
            public boolean isAcknowledged() {
                return acknowledged;
            }

            @JsonProperty("acknowledged")
            public void setAcknowledged(boolean acknowledged) {
                this.acknowledged = acknowledged;
            }

            @JsonProperty("timestamp")
            public long getTimestamp() {
                return timestamp;
            }

            @JsonProperty("timestamp")
            public void setTimestamp(long timestamp) {
                this.timestamp = timestamp;
            }

            @JsonProperty("entityId")
            public String getEntity() {
                return entity;
            }

            @JsonProperty("entityId")
            public void setEntity(String entity) {
                this.entity = entity;
            }

            @JsonProperty("goLink")
            public String getGoLink() {
                return goLink;
            }

            @JsonProperty("goLink")
            public void setGoLink(String goLink) {
                this.goLink = goLink;
            }

            @JsonProperty("linkLabel")
            public String getLinkLabel() {
                return linkLabel;
            }

            @JsonProperty("linkLabel")
            public void setLinkLabel(String linkLabel) {
                this.linkLabel = linkLabel;
            }

            @JsonAnyGetter
            public Map<String, Object> getAdditionalProperties() {
                return this.additionalProperties;
            }

            @JsonAnySetter
            public void setAdditionalProperty(String name, Object value) {
                this.additionalProperties.put(name, value);
            }

        }
    }
}
