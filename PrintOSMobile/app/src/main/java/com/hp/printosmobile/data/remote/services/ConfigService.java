package com.hp.printosmobile.data.remote.services;

import com.hp.printosmobile.data.remote.ApiConstants;
import com.hp.printosmobile.data.remote.models.DevicesData;
import com.hp.printosmobile.data.remote.models.DevicesStatisticsData;
import com.hp.printosmobile.data.remote.models.DeviceData;
import com.hp.printosmobile.data.remote.models.ScorableConfigData;

import java.util.List;

import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Retrofit REST interface definition for Config devices Endpoints
 *
 * @author Osama Taha
 */
public interface ConfigService {

    /**
     * Returns list of devices for Indigo.
     */
    @GET(ApiConstants.CONFIG_DEVICES_API)
    Observable<Response<List<DeviceData>>> getConfigDevices(@Query("lang") String language);
    /**
     * Returns list of metadata for each scorable KPI given list of devices and business unit.
     */
    @GET(ApiConstants.CONFIG_SCOREABLES_API)
    Observable<Response<List<ScorableConfigData>>> getScorablesInfo(@Query("devices") List<String> devices, @Query("bu") String businessUnit);

    @GET(ApiConstants.CONFIG_ORGANIZATION_DEVICES_API)
    Observable<Response<DevicesData>> getOrganizationsDevices();

    @GET(ApiConstants.DEVICES_STATISTICS_API)
    Observable<Response<DevicesStatisticsData>> getDevicesStatisticsData(@Query("device") List<String> devices);
}
