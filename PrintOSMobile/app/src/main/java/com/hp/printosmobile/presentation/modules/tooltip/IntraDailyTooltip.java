package com.hp.printosmobile.presentation.modules.tooltip;

import android.content.Context;
import android.text.Spannable;
import android.widget.TextView;

import com.hp.printosmobile.R;

/**
 * Created by osama on 12/17/16.
 */
public class IntraDailyTooltip extends Tooltip<String> {

    private TextView hintTextView;

    public IntraDailyTooltip(Context context) {
        super(context);
    }

    @Override
    protected void initContentView() {
        hintTextView = (TextView) contentView.findViewById(R.id.hint_text_view);
    }

    @Override
    public int getLayoutResourceId() {
        return R.layout.intra_daily_tool_tip_layout;
    }

    @Override
    public void updateView(String tipText) {
        hintTextView.setText(tipText);
    }

    public void updateView(Spannable spannableStringBuilder) {
        hintTextView.setText(spannableStringBuilder);
    }
}
