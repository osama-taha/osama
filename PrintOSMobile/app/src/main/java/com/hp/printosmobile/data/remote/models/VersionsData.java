package com.hp.printosmobile.data.remote.models;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class VersionsData {

    @JsonProperty("versions")
    private Versions versions;
    @JsonProperty("messages")
    private Messages messages;
    @JsonProperty("configuration")
    private Configuration configuration;

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("versions")
    public Versions getVersions() {
        return versions;
    }

    @JsonProperty("versions")
    public void setVersions(Versions versions) {
        this.versions = versions;
    }

    @JsonProperty("messages")
    public Messages getMessages() {
        return messages;
    }

    @JsonProperty("messages")
    public void setMessages(Messages messages) {
        this.messages = messages;
    }

    @JsonProperty("configuration")
    public Configuration getConfiguration() {
        return configuration;
    }

    @JsonProperty("configuration")
    public void setConfiguration(Configuration configuration) {
        this.configuration = configuration;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return "VersionsData{" +
                "versions=" + versions +
                ", messages=" + messages +
                '}';
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class Versions {

        @JsonProperty("printbeat")
        private Application printbeat;
        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<String, Object>();

        @JsonProperty("printbeat")
        public Application getPrintbeat() {
            return printbeat;
        }

        @JsonProperty("printbeat")
        public void setPrintbeat(Application printbeat) {
            this.printbeat = printbeat;
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }

        @Override
        public String toString() {
            return "Versions{" +
                    "printbeat=" + printbeat +
                    '}';
        }
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class Messages {

        @JsonProperty("message_default")
        private HashMap<String, String> messageDefault;
        @JsonProperty("message_update")
        private HashMap<String, String> messageUpdate;
        @JsonProperty("message_warning")
        private HashMap<String, String> messageWarning;
        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<String, Object>();

        @JsonProperty("message_default")
        public HashMap<String, String> getMessageDefault() {
            return messageDefault;
        }

        @JsonProperty("message_default")
        public void setMessageDefault(HashMap<String, String> messageDefault) {
            this.messageDefault = messageDefault;
        }

        @JsonProperty("message_update")
        public HashMap<String, String> getMessageUpdate() {
            return messageUpdate;
        }

        @JsonProperty("message_update")
        public void setMessageUpdate(HashMap<String, String> messageUpdate) {
            this.messageUpdate = messageUpdate;
        }

        @JsonProperty("message_warning")
        public HashMap<String, String> getMessageWarning() {
            return messageWarning;
        }

        @JsonProperty("message_warning")
        public void setMessageWarning(HashMap<String, String> messageWarning) {
            this.messageWarning = messageWarning;
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }

        @Override
        public String toString() {
            return "Messages{" +
                    "messageDefault=" + messageDefault +
                    ", messageUpdate=" + messageUpdate +
                    ", messageWarning=" + messageWarning +
                    '}';
        }
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class Application {

        @JsonProperty("ios")
        private Platform ios;
        @JsonProperty("android")
        private Platform android;
        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<String, Object>();

        @JsonProperty("ios")
        public Platform getIos() {
            return ios;
        }

        @JsonProperty("ios")
        public void setIos(Platform ios) {
            this.ios = ios;
        }

        @JsonProperty("android")
        public Platform getAndroid() {
            return android;
        }

        @JsonProperty("android")
        public void setAndroid(Platform android) {
            this.android = android;
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }

        @Override
        public String toString() {
            return "Application{" +
                    "ios=" + ios +
                    ", android=" + android +
                    '}';
        }

    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class Platform {

        @JsonProperty("warning")
        private List<String> warning = new ArrayList<String>();
        @JsonProperty("update")
        private List<String> update = new ArrayList<String>();
        @JsonProperty("valid")
        private List<String> valid = new ArrayList<String>();
        @JsonProperty("link_in_store")
        private String linkInStore;
        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<String, Object>();

        @JsonProperty("warning")
        public List<String> getWarning() {
            return warning;
        }

        @JsonProperty("warning")
        public void setWarning(List<String> warning) {
            this.warning = warning;
        }

        @JsonProperty("update")
        public List<String> getUpdate() {
            return update;
        }

        @JsonProperty("update")
        public void setUpdate(List<String> update) {
            this.update = update;
        }

        @JsonProperty("valid")
        public List<String> getValid() {
            return valid;
        }

        @JsonProperty("valid")
        public void setValid(List<String> valid) {
            this.valid = valid;
        }

        @JsonProperty("link_in_store")
        public String getLinkInStore() {
            return linkInStore;
        }

        @JsonProperty("link_in_store")
        public void setLinkInStore(String linkInStore) {
            this.linkInStore = linkInStore;
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }

        @Override
        public String toString() {
            return "Platform{" +
                    "warning=" + warning +
                    ", update=" + update +
                    ", valid=" + valid +
                    ", linkInStore='" + linkInStore + '\'' +
                    '}';
        }
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class Configuration {

        @JsonProperty("app_rating")
        private AppRating appRating;
        @JsonProperty("v2_support")
        private boolean v2Support;
        @JsonProperty("validation")
        private Validation validation;
        @JsonProperty("insights")
        private Insights insights;
        @JsonProperty("completed_jobs_enabled")
        private boolean completedJobsEnabled;
        @JsonProperty("supported_languages")
        private List<String> supportedLanguages;
        @JsonProperty("weekly_printbeat")
        private WeeklyPrintbeat weeklyPrintbeat;
        @JsonProperty("beat_coins_popup_enabled")
        private Boolean beatCoinsPopupEnabled;
        @JsonProperty("nps_popup_enabled")
        private Boolean npsPopupEnabled;
        @JsonProperty("kpi_explanation_enabled")
        private Boolean kpiExplanationEnabled;
        @JsonProperty("time_zone_popup_enabled")
        private Boolean timeZonePopupEnabled;
        @JsonProperty("refer_a_friend")
        private ReferFriend referFriend;
        @JsonProperty("location_based_notifications_enabled")
        private boolean locationBasedNotificationsEnabled;
        @JsonProperty("closed_service_calls_days")
        private int closedServiceCallsDays;

        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<String, Object>();

        @JsonProperty("refer_a_friend")
        public ReferFriend getReferFriend() {
            return referFriend;
        }

        @JsonProperty("refer_a_friend")
        public void setReferFriend(ReferFriend referFriend) {
            this.referFriend = referFriend;
        }

        @JsonProperty("beat_coins_popup_enabled")
        public Boolean isBeatCoinsPopupEnabled() {
            return beatCoinsPopupEnabled;
        }

        @JsonProperty("beat_coins_popup_enabled")
        public void setBeatCoinsPopupEnabled(Boolean beatCoinsPopupEnabled) {
            this.beatCoinsPopupEnabled = beatCoinsPopupEnabled;
        }

        @JsonProperty("nps_popup_enabled")
        public Boolean isNpsPopupEnabled() {
            return npsPopupEnabled;
        }

        @JsonProperty("nps_popup_enabled")
        public void setNpsPopupEnabled(Boolean npsPopupEnabled) {
            this.npsPopupEnabled = npsPopupEnabled;
        }

        @JsonProperty("time_zone_popup_enabled")
        public Boolean isTimeZonePopupEnabled() {
            return timeZonePopupEnabled;
        }

        @JsonProperty("time_zone_popup_enabled")
        public void setTimeZonePopupEnabled(Boolean timeZonePopupEnabled) {
            this.timeZonePopupEnabled = timeZonePopupEnabled;
        }

        @JsonProperty("insights")
        public Insights getInsights() {
            return insights;
        }

        @JsonProperty("insights")
        public void setInsights(Insights insights) {
            this.insights = insights;
        }

        @JsonProperty("app_rating")
        public AppRating getAppRating() {
            return appRating;
        }

        @JsonProperty("app_rating")
        public void setAppRating(AppRating appRating) {
            this.appRating = appRating;
        }

        @JsonProperty("v2_support")
        public boolean isV2Support() {
            return v2Support;
        }

        @JsonProperty("v2_support")
        public void setV2Support(boolean v2Support) {
            this.v2Support = v2Support;
        }

        @JsonProperty("validation")
        public Validation getValidation() {
            return validation;
        }

        @JsonProperty("validation")
        public void setValidation(Validation validation) {
            this.validation = validation;
        }

        @JsonProperty("completed_jobs_enabled")
        public boolean isCompletedJobsEnabled() {
            return completedJobsEnabled;
        }

        @JsonProperty("completed_jobs_enabled")
        public void setCompletedJobsEnabled(boolean completedJobsEnabled) {
            this.completedJobsEnabled = completedJobsEnabled;
        }

        @JsonProperty("supported_languages")
        public void setSupportedLanguages(List<String> supportedLanguages) {
            this.supportedLanguages = supportedLanguages;
        }

        @JsonProperty("supported_languages")
        public List<String> getSupportedLanguages() {
            return supportedLanguages;
        }

        @JsonProperty("weekly_printbeat")
        public WeeklyPrintbeat getWeeklyPrintbeat() {
            return weeklyPrintbeat;
        }

        @JsonProperty("weekly_printbeat")
        public void setWeeklyPrintbeat(WeeklyPrintbeat weeklyPrintbeat) {
            this.weeklyPrintbeat = weeklyPrintbeat;
        }

        @JsonProperty("kpi_explanation_enabled")
        public Boolean getKpiExplanationEnabled() {
            return kpiExplanationEnabled;
        }

        @JsonProperty("kpi_explanation_enabled")
        public void setKpiExplanationEnabled(Boolean kpiExplanationEnabled) {
            this.kpiExplanationEnabled = kpiExplanationEnabled;
        }

        @JsonProperty("location_based_notifications_enabled")
        public boolean isLocationBasedNotificationsEnabled() {
            return locationBasedNotificationsEnabled;
        }

        @JsonProperty("location_based_notifications_enabled")
        public void setLocationBasedNotificationsEnabled(boolean locationBasedNotificationsEnabled) {
            this.locationBasedNotificationsEnabled = locationBasedNotificationsEnabled;
        }

        @JsonProperty("closed_service_calls_days")
        public void setClosedServiceCallsDays(int closedServiceCallsDays) {
            this.closedServiceCallsDays = closedServiceCallsDays;
        }

        @JsonProperty("closed_service_calls_days")
        public int getClosedServiceCallsDays() {
            return closedServiceCallsDays;
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }


        @JsonInclude(JsonInclude.Include.NON_NULL)
        public static class AppRating {

            @JsonProperty("rated_versions")
            private List<String> versionList;
            @JsonProperty("days_after_installation")
            private int daysAfterInstallation;
            @JsonIgnore
            private Map<String, Object> additionalProperties = new HashMap<String, Object>();

            @JsonProperty("rated_versions")
            public List<String> getVersionList() {
                return versionList;
            }

            @JsonProperty("rated_versions")
            public void setVersionList(List<String> versionList) {
                this.versionList = versionList;
            }

            @JsonProperty("days_after_installation")
            public int getDaysAfterInstallation() {
                return daysAfterInstallation;
            }

            @JsonProperty("days_after_installation")
            public void setDaysAfterInstallation(int daysAfterInstallation) {
                this.daysAfterInstallation = daysAfterInstallation;
            }

            @JsonAnyGetter
            public Map<String, Object> getAdditionalProperties() {
                return this.additionalProperties;
            }

            @JsonAnySetter
            public void setAdditionalProperty(String name, Object value) {
                this.additionalProperties.put(name, value);
            }
        }

        @JsonInclude(JsonInclude.Include.NON_NULL)
        public static class Validation {

            @JsonProperty("check_cookie_expiry_hrs")
            private int checkCookieExpiryHrs;
            @JsonProperty("move_to_home_mins")
            private int moveToHomeMins;
            @JsonProperty("show_toast_message")
            private boolean showToastMessage;
            @JsonIgnore
            private Map<String, Object> additionalProperties = new HashMap<String, Object>();

            @JsonProperty("check_cookie_expiry_hrs")
            public int getCheckCookieExpiryHrs() {
                return checkCookieExpiryHrs;
            }

            @JsonProperty("check_cookie_expiry_hrs")
            public void setCheckCookieExpiryHrs(int checkCookieExpiryHrs) {
                this.checkCookieExpiryHrs = checkCookieExpiryHrs;
            }

            @JsonProperty("move_to_home_mins")
            public int getMoveToHomeMins() {
                return moveToHomeMins;
            }

            @JsonProperty("move_to_home_mins")
            public void setMoveToHomeMins(int moveToHomeMins) {
                this.moveToHomeMins = moveToHomeMins;
            }

            @JsonProperty("show_toast_message")
            public boolean isShowToastMessage() {
                return showToastMessage;
            }

            @JsonProperty("show_toast_message")
            public void setShowToastMessage(boolean showToastMessage) {
                this.showToastMessage = showToastMessage;
            }

            @JsonAnyGetter
            public Map<String, Object> getAdditionalProperties() {
                return this.additionalProperties;
            }

            @JsonAnySetter
            public void setAdditionalProperty(String name, Object value) {
                this.additionalProperties.put(name, value);
            }

        }

        @JsonInclude(JsonInclude.Include.NON_NULL)
        public static class Insights {

            @JsonProperty("jams_enabled")
            private boolean jamsEnabled;
            @JsonProperty("failures_enabled")
            private boolean failuresEnabled;
            @JsonProperty("badge_version")
            private String badgeVersion;
            @JsonIgnore
            private Map<String, Object> additionalProperties = new HashMap<String, Object>();

            @JsonProperty("jams_enabled")
            public boolean isJamsEnabled() {
                return jamsEnabled;
            }

            @JsonProperty("jams_enabled")
            public void setJamsEnabled(boolean jamsEnabled) {
                this.jamsEnabled = jamsEnabled;
            }

            @JsonProperty("failures_enabled")
            public boolean isFailuresEnabled() {
                return failuresEnabled;
            }

            @JsonProperty("failures_enabled")
            public void setFailuresEnabled(boolean failuresEnabled) {
                this.failuresEnabled = failuresEnabled;
            }

            @JsonProperty("badge_version")
            public String getBadgeVersion() {
                return badgeVersion;
            }

            @JsonProperty("badge_version")
            public void setBadgeVersion(String badgeVersion) {
                this.badgeVersion = badgeVersion;
            }

            @JsonAnyGetter
            public Map<String, Object> getAdditionalProperties() {
                return this.additionalProperties;
            }

            @JsonAnySetter
            public void setAdditionalProperty(String name, Object value) {
                this.additionalProperties.put(name, value);
            }
        }

        @JsonInclude(JsonInclude.Include.NON_NULL)
        public static class WeeklyPrintbeat {

            @JsonProperty("kpi_data_incrementally_enabled")
            private Boolean kpiDataIncrementallyEnabled;
            @JsonProperty("ranking_feature")
            private RankingFeature rankingFeature;
            @JsonIgnore
            private Map<String, Object> additionalProperties = new HashMap<String, Object>();

            @JsonProperty("kpi_data_incrementally_enabled")
            public Boolean isKpiDataIncrementallyEnabled() {
                return kpiDataIncrementallyEnabled;
            }

            @JsonProperty("kpi_data_incrementally_enabled")
            public void setKpiDataIncrementallyEnabled(Boolean kpiDataIncrementlyEnabled) {
                this.kpiDataIncrementallyEnabled = kpiDataIncrementlyEnabled;
            }

            @JsonProperty("ranking_feature")
            public RankingFeature getRankingFeature() {
                return rankingFeature;
            }

            @JsonProperty("ranking_feature")
            public void setRankingFeature(RankingFeature rankingFeature) {
                this.rankingFeature = rankingFeature;
            }

            public void setAdditionalProperties(Map<String, Object> additionalProperties) {
                this.additionalProperties = additionalProperties;
            }

            @JsonAnyGetter
            public Map<String, Object> getAdditionalProperties() {
                return this.additionalProperties;
            }

            @JsonAnySetter
            public void setAdditionalProperty(String name, Object value) {
                this.additionalProperties.put(name, value);
            }

            @JsonInclude(JsonInclude.Include.NON_NULL)
            public static class RankingFeature {

                @JsonProperty("enabled")
                private boolean enabled;
                @JsonProperty("supported_equipments")
                private List<String> supportedEquipments;

                @JsonProperty("enabled")
                public boolean isEnabled() {
                    return enabled;
                }

                @JsonProperty("enabled")
                public void setEnabled(boolean enabled) {
                    this.enabled = enabled;
                }

                @JsonProperty("supported_equipments")
                public List<String> getSupportedEquipments() {
                    return supportedEquipments;
                }

                @JsonProperty("supported_equipments")
                public void setSupportedEquipments(List<String> supportedEquipments) {
                    this.supportedEquipments = supportedEquipments;
                }

                public void setAdditionalProperties(Map<String, Object> additionalProperties) {
                    this.additionalProperties = additionalProperties;
                }

                @JsonIgnore
                private Map<String, Object> additionalProperties = new HashMap<String, Object>();

                @JsonAnyGetter
                public Map<String, Object> getAdditionalProperties() {
                    return this.additionalProperties;
                }

                @JsonAnySetter
                public void setAdditionalProperty(String name, Object value) {
                    this.additionalProperties.put(name, value);
                }

            }
        }

        @JsonInclude(JsonInclude.Include.NON_NULL)
        public static class ReferFriend {

            @JsonProperty("enabled")
            private Boolean enabled;
            @JsonProperty("url")
            private String url;
            @JsonIgnore
            private Map<String, Object> additionalProperties = new HashMap<String, Object>();

            @JsonProperty("enabled")
            public Boolean isEnabled() {
                return enabled;
            }

            @JsonProperty("enabled")
            public void setEnabled(Boolean enabled) {
                this.enabled = enabled;
            }

            @JsonProperty("url")
            public String getUrl() {
                return url;
            }

            @JsonProperty("url")
            public void setUrl(String url) {
                this.url = url;
            }

            @JsonAnyGetter
            public Map<String, Object> getAdditionalProperties() {
                return this.additionalProperties;
            }

            @JsonAnySetter
            public void setAdditionalProperty(String name, Object value) {
                this.additionalProperties.put(name, value);
            }

        }
    }
}