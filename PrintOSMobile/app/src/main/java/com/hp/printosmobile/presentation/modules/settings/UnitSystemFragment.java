package com.hp.printosmobile.presentation.modules.settings;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.AppseeSdk;
import com.hp.printosmobile.IntercomSdk;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.data.remote.models.PreferencesData;
import com.hp.printosmobile.data.remote.services.PreferencesService;
import com.hp.printosmobile.utils.HPUIUtils;
import com.hp.printosmobilelib.core.logging.HPLogger;
import com.hp.printosmobilelib.ui.common.HPFragment;
import com.hp.printosmobilelib.ui.utils.DividerItemDecoration;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class UnitSystemFragment extends HPFragment {

    private static final String TAG = UnitSystemFragment.class.getName();

    @Bind(R.id.unit_system_list)
    RecyclerView unitSystemList;
    @Bind(R.id.loading_view)
    ProgressBar loadingView;
    @Bind(R.id.done_button)
    Button doneButton;

    UnitSystemAdapter unitSystemAdapter;
    UnitSystemFragmentCallback callback;

    public static UnitSystemFragment getNewInstance(UnitSystemFragmentCallback callback) {
        UnitSystemFragment unitSystemFragment = new UnitSystemFragment();
        if (callback != null) {
            unitSystemFragment.callback = callback;
        }
        return unitSystemFragment;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        IntercomSdk.getInstance(getActivity()).logEvent(IntercomSdk.PBM_VIEW_LANGUAGE_EVENT);
        AppseeSdk.getInstance(getActivity()).startScreen(AppseeSdk.SCREEN_UNIT_SYSTEM);

        initView();
    }

    private void initView() {

        PreferencesData.UnitSystem[] unitSystems = PreferencesData.UnitSystem.values();

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        unitSystemList.setLayoutManager(layoutManager);

        unitSystemAdapter = new UnitSystemAdapter(unitSystems);
        unitSystemAdapter.setSelectedUnitSystem(PrintOSPreferences.getInstance(getActivity()).getUnitSystem());
        unitSystemList.setAdapter(unitSystemAdapter);

        unitSystemList.addItemDecoration(new DividerItemDecoration(getActivity(), 1, unitSystems.length, false));
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_unit_system;
    }

    @Override
    public boolean isFragmentNameToBeDisplayed() {
        return true;
    }

    @Override
    public boolean isFilteringAvailable() {
        return false;
    }

    @Override
    public int getToolbarDisplayName() {
        return R.string.unit_system;
    }

    @OnClick(R.id.done_button)
    public void onUnitSystemSelected() {

        if (callback != null && unitSystemAdapter != null) {

            HPLogger.d(TAG, "select unit system " + unitSystemAdapter.getSelectedUnitSystem().name());

            updatePreferences(unitSystemAdapter.getSelectedUnitSystem());
        }
    }

    public class UnitSystemAdapter extends RecyclerView.Adapter<UnitSystemAdapter.UnitSystemItemViewHolder> {

        private PreferencesData.UnitSystem[] unitSystems;
        private int selectedIndex;

        public UnitSystemAdapter(PreferencesData.UnitSystem[] unitSystems) {
            this.unitSystems = unitSystems;
            this.selectedIndex = -1;
        }

        @Override
        public UnitSystemItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            ViewGroup view = (ViewGroup) inflater.inflate(R.layout.fragment_unit_system_item, parent, false);
            UnitSystemItemViewHolder unitSystemItemViewHolder = new UnitSystemItemViewHolder(view);
            return unitSystemItemViewHolder;
        }

        @Override
        public void onBindViewHolder(UnitSystemItemViewHolder holder, int position) {
            PreferencesData.UnitSystem unitSystem = unitSystems[position];
            holder.unitSystemTextView.setText(unitSystem.getLocalizedName(getContext()));
            holder.index = position;

            holder.viewItem.setSelected(selectedIndex == position);
            holder.checkImage.setVisibility(selectedIndex == position ? View.VISIBLE : View.GONE);
        }

        @Override
        public int getItemCount() {
            return unitSystems == null ? 0 : unitSystems.length;
        }

        public PreferencesData.UnitSystem getSelectedUnitSystem() {
            return unitSystems != null && selectedIndex >= 0 && selectedIndex < unitSystems.length ?
                    unitSystems[selectedIndex] : null;
        }

        public void setSelectedUnitSystem(PreferencesData.UnitSystem selectedUnitSystem) {
            if (unitSystems != null && selectedUnitSystem != null) {
                for (int i = 0; i < unitSystems.length; i++) {
                    if (selectedUnitSystem == unitSystems[i]) {
                        selectedIndex = i;
                        break;
                    }
                }
            }
        }

        public class UnitSystemItemViewHolder extends RecyclerView.ViewHolder {

            @Bind(R.id.unit_system_text_view)
            TextView unitSystemTextView;
            @Bind(R.id.checked_image)
            View checkImage;

            View viewItem;
            int index;

            public UnitSystemItemViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
                viewItem = itemView;

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        selectedIndex = index;
                        notifyDataSetChanged();
                    }
                });
            }
        }
    }

    public interface UnitSystemFragmentCallback {
        void onUnitSystemChanged();
    }

    public void updatePreferences(PreferencesData.UnitSystem unitSystem) {

        final PreferencesData.UnitSystem selectedUnitSystem = unitSystem;

        showLoadingView(true);

        final PreferencesService preferencesService = PrintOSApplication.getApiServicesProvider()
                .getPreferencesService();

        preferencesService.getPreferences()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<Response<PreferencesData>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        onRequestFailed();
                        HPLogger.d(TAG, "unable to get the user pref " + e);
                    }

                    @Override
                    public void onNext(Response<PreferencesData> preferencesDataResponse) {

                        if (preferencesDataResponse.isSuccessful()) {

                            if (preferencesDataResponse != null && preferencesDataResponse.body() != null) {

                                final PreferencesData preferencesData = new PreferencesData();
                                preferencesData.setGeneral(new PreferencesData.General());
                                preferencesData.getGeneral().setUnitSystem(selectedUnitSystem);

                                if (preferencesDataResponse.body().getGeneral() != null) {
                                    preferencesData.getGeneral().setLocale(preferencesDataResponse.body()
                                            .getGeneral().getLocale());
                                    preferencesData.getGeneral().setTimeZone(preferencesDataResponse.body()
                                            .getGeneral().getTimeZone());
                                }

                                preferencesService.savePreferences(preferencesData)
                                        .observeOn(AndroidSchedulers.mainThread())
                                        .subscribeOn(Schedulers.io()).subscribe(new Subscriber<Response<PreferencesData>>() {
                                    @Override
                                    public void onCompleted() {

                                    }

                                    @Override
                                    public void onError(Throwable e) {
                                        onRequestFailed();
                                        HPLogger.d(TAG, "unable to save the selected unit " + e);
                                    }

                                    @Override
                                    public void onNext(Response<PreferencesData> preferencesDataResponse) {
                                        if (preferencesDataResponse.isSuccessful()) {
                                            Analytics.sendEvent(Analytics.MOBILE_PRINT_BEAT_CATEGORY, Analytics.SETTINGS_CHANGE_UNIT_SYSTEM_ACTION, selectedUnitSystem.name());
                                            PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).saveUnitSystem(selectedUnitSystem);
                                            HPLogger.d(TAG, "Unit system has been successfully changed.");
                                            onRequestCompleted();
                                        } else {
                                            HPLogger.d(TAG, "response code for unit system PUT api " + preferencesDataResponse.code());
                                            onRequestFailed();
                                        }
                                    }
                                });

                            } else {
                                onRequestFailed();
                            }
                        } else {
                            onRequestFailed();
                        }
                    }
                });
    }

    private void onRequestFailed() {
        showLoadingView(false);
        if (getActivity() != null) {
            HPUIUtils.displayToast(getActivity(), getActivity().getString(R.string.error_change_pref_failed), false);
        }
    }

    private void onRequestCompleted() {
        showLoadingView(false);
        if (callback != null) {
            callback.onUnitSystemChanged();
        }
    }

    private void showLoadingView(boolean show) {
        HPUIUtils.setVisibility(show, loadingView);
        doneButton.setEnabled(!show);
        doneButton.setClickable(!show);
    }
}
