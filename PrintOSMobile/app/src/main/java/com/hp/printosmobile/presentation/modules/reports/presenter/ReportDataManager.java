package com.hp.printosmobile.presentation.modules.reports.presenter;

import android.content.Context;

import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.data.remote.ApiServicesProvider;
import com.hp.printosmobile.data.remote.models.PreferencesData;
import com.hp.printosmobile.data.remote.models.ReportData;
import com.hp.printosmobile.data.remote.models.ScorableConfigData;
import com.hp.printosmobile.data.remote.services.ConfigService;
import com.hp.printosmobile.data.remote.services.PerformanceTrackingService;
import com.hp.printosmobile.data.remote.services.ReportDataV2;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitViewModel;
import com.hp.printosmobile.presentation.modules.main.ReportChartTypeEnum;
import com.hp.printosmobile.presentation.modules.main.ReportTypeEnum;
import com.hp.printosmobile.presentation.modules.reports.ReportFilter;
import com.hp.printosmobile.presentation.modules.reports.ReportKpiViewModel;
import com.hp.printosmobile.utils.HPUIUtils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;
import rx.Observable;
import rx.functions.Func1;
import rx.functions.Func2;

/**
 * Created by Anwar Asbah on 5/23/16.
 */
public class ReportDataManager {

    public static final String DATE_FORMAT = "yyyy-MM-dd";

    public static Observable<ReportKpiViewModel> getOverallData(final Context context, final ReportFilter reportFilter) {

        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
        PerformanceTrackingService performanceTrackingService = serviceProvider.getPerformanceTrackingService();
        ConfigService configService = serviceProvider.getConfigService();

        final BusinessUnitViewModel businessUnit = reportFilter.getBusinessUnit();

        String buName = businessUnit.getBusinessUnit().getName();
        List<String> devices = businessUnit.getFiltersViewModel().getSerialNumbers();
        int offset = reportFilter.getOffSet();

        return Observable.combineLatest(configService.getScorablesInfo(devices, buName),

                performanceTrackingService.getOverallReport(buName, devices, offset),
                new Func2<Response<List<ScorableConfigData>>, Response<ReportData>, ReportKpiViewModel>() {

                    @Override
                    public ReportKpiViewModel call(Response<List<ScorableConfigData>> scorableResponse, Response<ReportData> reportDataResponse) {

                        if (scorableResponse.isSuccessful()) {
                            ReportPresenter.setReportKPIMetaDataList(scorableResponse.body());
                        }

                        if (reportDataResponse.isSuccessful()) {
                            if (reportDataResponse.body().getUnitEvents() == null
                                    || reportDataResponse.body().getUnitEvents().size() == 0) {
                                return null;
                            }
                            return parseOverAllApiResponse(context, reportFilter, reportDataResponse.body(), businessUnit.getBusinessUnit(), ReportTypeEnum.VALUE);
                        }
                        return null;
                    }
                }
        );
    }

    private static ReportKpiViewModel parseOverAllApiResponse(Context context, final ReportFilter reportFilter, ReportData reportData, BusinessUnitEnum businessUnitEnum, ReportTypeEnum typeEnum) {
        HPUIUtils.resetRandomColorIndex();
        ArrayList<ReportKpiViewModel> kpiModel = ReportsDataParser.parseReportData(context, reportData, DATE_FORMAT, false, businessUnitEnum, typeEnum);

        ReportKpiViewModel overAllKpi = ReportsDataParser.initKPI(context, context.getString(R.string.kpi_overall_key), businessUnitEnum);

        overAllKpi.setLocalizedName(context.getString(R.string.reports_performance_chart_title));
        overAllKpi.setAggregate(true);
        overAllKpi.setSubKPIs(kpiModel);
        if (kpiModel != null && kpiModel.size() > 0) {
            overAllKpi.setXAxisValues(kpiModel.get(0).getXAxisValues());
            overAllKpi.setUnitEvents(kpiModel.get(0).getUnitEvents());
        }
        overAllKpi.setFillArea(true);
        overAllKpi.setReportChartTypeEnum(ReportChartTypeEnum.AREA);

        for (ReportKpiViewModel model : kpiModel) {
            model.setAggregate(false); //TODO read from meta data
            model.setParentKpi(overAllKpi);

            ReportChartTypeEnum reportChartTypeEnum = ReportChartTypeEnum.UNKNOWN;
            if (reportFilter != null && reportFilter.getBusinessUnit() != null && reportFilter.getBusinessUnit().getBusinessUnitKpi() != null) {
                List<BusinessUnitViewModel.BusinessUnitKpi> businessUnitKpis = reportFilter.getBusinessUnit().getBusinessUnitKpi();
                for (BusinessUnitViewModel.BusinessUnitKpi businessUnitKpi : businessUnitKpis) {
                    if (businessUnitKpi.getkpiKeyName() != null && businessUnitKpi.getkpiKeyName().equalsIgnoreCase(model.getName())) {
                        reportChartTypeEnum = businessUnitKpi.getReportChartTypeEnum();
                    }
                }
            }

            model.setFillArea(true);
            model.setReportChartTypeEnum(reportChartTypeEnum);

        }

        return overAllKpi;
    }

    public static Observable<ReportKpiViewModel> getKpiData(final Context context, final ReportFilter reportFilter, final ReportKpiViewModel kpi) {
        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
        PerformanceTrackingService performanceTrackingService = serviceProvider.getPerformanceTrackingService();

        final BusinessUnitViewModel businessUnit = reportFilter.getBusinessUnit();
        String buName = businessUnit.getBusinessUnit().getName();
        List<String> devices = businessUnit.getFiltersViewModel().getSerialNumbers();

        ReportTypeEnum reportTypeEnum = ReportTypeEnum.UNKNOWN;
        ReportChartTypeEnum reportChartTypeEnum = ReportChartTypeEnum.UNKNOWN;
        if (reportFilter != null && reportFilter.getBusinessUnit() != null && reportFilter.getBusinessUnit().getBusinessUnitKpi() != null) {
            List<BusinessUnitViewModel.BusinessUnitKpi> businessUnitKpis = reportFilter.getBusinessUnit().getBusinessUnitKpi();
            for (BusinessUnitViewModel.BusinessUnitKpi businessUnitKpi : businessUnitKpis) {
                if (businessUnitKpi.getkpiKeyName() != null && businessUnitKpi.getkpiKeyName().equalsIgnoreCase(kpi.getName())) {
                    reportTypeEnum = businessUnitKpi.getReportTypeEnum();
                    reportChartTypeEnum = businessUnitKpi.getReportChartTypeEnum();
                }
            }
        }

        final ReportTypeEnum typeEnum = reportTypeEnum;
        final ReportChartTypeEnum typeChartEnum = reportChartTypeEnum;

        PrintOSPreferences printOSPreferences = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext());
        if (printOSPreferences.supportV2()) {

            PreferencesData.UnitSystem unitSystem = printOSPreferences.getUnitSystem();

            return performanceTrackingService.getKpiReportV2(kpi.getName(), buName,
                    !kpi.getName().equals(context.getString(R.string.indigo_kpi_availability_key)),
                    devices,
                    reportFilter.getOffSet(),
                    reportFilter.getResolution().getValue(),
                    reportFilter.getUnit().getValue(), unitSystem.name()).map(new Func1<Response<ReportDataV2>, ReportKpiViewModel>() {
                @Override
                public ReportKpiViewModel call(Response<ReportDataV2> reportDataResponse) {
                    if (reportDataResponse.isSuccessful()) {
                        parseKpiData(context, reportDataResponse.body().getData(), kpi, businessUnit, typeEnum, typeChartEnum);
                        return reportFilter.getKpi();
                    }
                    return null;
                }
            });
        } else {
            return performanceTrackingService.getKpiReport(kpi.getName(), buName,
                    !kpi.getName().equals(context.getString(R.string.indigo_kpi_availability_key)),
                    devices,
                    reportFilter.getOffSet(),
                    reportFilter.getResolution().getValue(),
                    reportFilter.getUnit().getValue()).map(new Func1<Response<ReportData>, ReportKpiViewModel>() {
                @Override
                public ReportKpiViewModel call(Response<ReportData> reportDataResponse) {
                    if (reportDataResponse.isSuccessful()) {
                        parseKpiData(context, reportDataResponse.body(), kpi, businessUnit, typeEnum, typeChartEnum);
                        return reportFilter.getKpi();
                    }
                    return null;
                }
            });
        }
    }

    private static void parseKpiData(Context context, ReportData reportData, ReportKpiViewModel kpi, BusinessUnitViewModel businessUnitViewModel, ReportTypeEnum typeEnum, ReportChartTypeEnum chartTypeEnum) {
        ArrayList<ReportKpiViewModel> kpiModel = ReportsDataParser.parseReportData(context, reportData, DATE_FORMAT, false, businessUnitViewModel.getBusinessUnit(), typeEnum);

        for (ReportKpiViewModel model : kpiModel) {
            if (model.getName().equalsIgnoreCase(kpi.getName())) {
                kpi.setBarEntries(model.getBarEntries());
                kpi.setXAxisValues(model.getXAxisValues());
                kpi.setKpiValues(model.getOverallValues());
                kpi.setUnitEvents(model.getUnitEvents());
            } else {
                model.setKpiValues(model.getOverallValues());
                model.setFillArea(chartTypeEnum == ReportChartTypeEnum.AREA);
                model.setPressName(businessUnitViewModel.getDeviceName(model.getName()));
                kpi.addPressReport(model);
                kpi.setXAxisValues(model.getXAxisValues());
            }
            model.setReportChartTypeEnum(chartTypeEnum);
            ReportsDataParser.getWeekPoints(kpi);
        }
    }

    public static Observable<Response<List<ScorableConfigData>>> getScorableConfigData(ReportFilter reportFilter) {
        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
        ConfigService configService = serviceProvider.getConfigService();

        BusinessUnitViewModel businessUnit = reportFilter.getBusinessUnit();
        String buName = businessUnit.getBusinessUnit().getName();
        List<String> devices = businessUnit.getFiltersViewModel().getSerialNumbers();

        return configService.getScorablesInfo(devices, buName);
    }
}