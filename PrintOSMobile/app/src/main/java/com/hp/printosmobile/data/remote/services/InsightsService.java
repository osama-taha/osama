package com.hp.printosmobile.data.remote.services;

import com.hp.printosmobile.data.remote.ApiConstants;
import com.hp.printosmobile.data.remote.models.BoxModel;
import com.hp.printosmobile.data.remote.models.InsightData;

import java.util.List;

import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by Anwar Asbah 6/20/2017
 */
public interface InsightsService {

    @GET(ApiConstants.INSIGHTS_API)
    Observable<Response<InsightData>> getInsightData(@Query("device") List<String> devices,
                                                     @Query("from") String fromDate,
                                                     @Query("to") String toDate,
                                                     @Query("kpi") String kpi,
                                                     @Query("top") int top);
}
