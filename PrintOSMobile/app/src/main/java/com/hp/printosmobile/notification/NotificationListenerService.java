package com.hp.printosmobile.notification;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.text.TextUtils;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.AppseeSdk;
import com.hp.printosmobile.Constants;
import com.hp.printosmobile.R;
import com.hp.printosmobilelib.core.logging.HPLogger;

import java.util.Map;

import io.intercom.android.sdk.push.IntercomPushClient;

/**
 * Created by anwar asbah on 1/17/2017.
 */

public class NotificationListenerService extends FirebaseMessagingService {

    private static final String TAG = NotificationListenerService.class.getSimpleName();

    private final IntercomPushClient intercomPushClient = new IntercomPushClient();

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        HPLogger.d(TAG, "received push notification " + remoteMessage.getData());

        Map message = remoteMessage.getData();

        if (intercomPushClient.isIntercomPush(message)) {
            intercomPushClient.handlePush(getApplication(), message);
        } else {
            displayNotification(remoteMessage);
        }
    }

    private void displayNotification(RemoteMessage remoteMessage) {

        RemoteMessage.Notification notification = remoteMessage.getNotification();
        Map<String, String> data = remoteMessage.getData();

        String title = getString(R.string.app_name);
        String entity = getStringExtra(data, Constants.NOTIFICATION_ENTITY_ID_KEY);
        String eventKey = getStringExtra(data, Constants.NOTIFICATION_EVENT_KEY);
        String link = getStringExtra(data, Constants.NOTIFICATION_LINK_KEY);
        int notificationID = getIntExtra(data, Constants.NOTIFICATION_ID_KEY);
        int userEventID = getIntExtra(data, Constants.NOTIFICATION_USER_EVENT_ID_KEY);

        String message = null;
        if (notification != null) {
            message = remoteMessage.getNotification().getBody();
        }
        message = message == null ? getStringExtra(data, Constants.NOTIFICATION_BODY_KEY) : message;

        fireNotification(notificationID, userEventID, title, message, entity, eventKey, link);
        broadcastNotificationArrival();
    }

    private void fireNotification(int notificationID, int userID, String title, String message, String entity, String eventKey, String link) {
        HPLogger.d(TAG, "displaying notification with id: " + notificationID + " as read");

        int requestCode = NotificationUtils.getUniqueRequestCode();

        Intent intent = NotificationUtils.getNotificationIntent(this, entity, eventKey, notificationID, userID, link, true);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, requestCode, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_notification)
                .setColor(ResourcesCompat.getColor(getResources(), R.color.c62, null))
                .setContentTitle(title)
                .setContentText(message)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(message).setBigContentTitle(title))
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        int id = notificationID != NotificationUtils.ERROR_INT_EXTRA ? notificationID : (int) System.currentTimeMillis();
        notificationManager.notify(id, notificationBuilder.build());

        if (!TextUtils.isEmpty(eventKey)) {
            HPLogger.d(TAG, "show notification " + eventKey);
            AppseeSdk.getInstance(this).sendEvent(AppseeSdk.EVENT_SHOW_NOTIFICATION, eventKey);
            Analytics.sendNotificationArriveFirebaseEvent(eventKey);
            Analytics.sendEvent(Analytics.MOBILE_PRINT_BEAT_CATEGORY, Analytics.SHOW_NOTIFICATION_ACTION, eventKey);
        }

    }

    private void broadcastNotificationArrival() {
        Intent intent = new Intent(Constants.IntentExtras.NOTIFICATION_RECEIVED_ACTION);
        sendBroadcast(intent);
    }

    private String getStringExtra(Map<String, String> data, String key) {
        if (data != null && data.containsKey(key)) {
            return data.get(key);
        }

        return null;
    }

    private int getIntExtra(Map<String, String> data, String key) {
        if (data != null && data.containsKey(key)) {
            String value = data.get(key);
            try {
                return Integer.parseInt(value);
            } catch (Exception e) {
            }

        }
        return NotificationUtils.ERROR_INT_EXTRA;
    }

}
