package com.hp.printosmobile.presentation.modules.main;

import com.hp.printosmobile.presentation.MVPView;
import com.hp.printosmobile.presentation.modules.beatcoins.BeatCoinsViewModel;

import java.util.Map;

/**
 * Created by Osama Taha on 5/23/16.
 */
public interface MainView extends MVPView {

    void hideLoading();

    void showLoading();

    void onGettingBusinessUnitsCompleted(Map<BusinessUnitEnum, BusinessUnitViewModel> deviceDataList);

    void onEmptyBusinessUnits();

    void onApplicationLogout();

    void onNoDevicesAttached();

    void onBeatCoinDataRetrieved(BeatCoinsViewModel beatCoinsViewModel);
}
