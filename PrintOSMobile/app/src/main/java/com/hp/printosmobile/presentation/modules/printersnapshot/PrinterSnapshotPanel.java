package com.hp.printosmobile.presentation.modules.printersnapshot;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;
import com.hp.printosmobile.presentation.modules.shared.BaseDeviceViewModel;
import com.hp.printosmobile.presentation.modules.shared.DeviceViewModel;
import com.hp.printosmobilelib.ui.utils.DividerItemDecoration;
import com.hp.printosmobilelib.ui.widgets.panel.PanelView;

import java.util.Collections;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Anwar Asbah on 5/4/2016.
 */
public class PrinterSnapshotPanel extends PanelView<PrinterSnapshotViewModel> implements PrinterSnapshotAdapter.PrinterSnapshotAdapterCallback {

    public final static String TAG = PrinterSnapshotPanel.class.getSimpleName();
    private static final int DECORATOR_MARGIN = 20;

    @Bind(R.id.button_view_all)
    TextView viewAllButton;
    @Bind(R.id.printer_grid)
    RecyclerView printerRecyclerView;
    @Bind(R.id.printer_snapshot_panel_content)
    View printerSnapshotPanelContent;

    private PrinterSnapshotCallback snapshotCallback;

    public PrinterSnapshotPanel(Context context) {
        super(context);
        bindViews();
    }

    public PrinterSnapshotPanel(Context context, AttributeSet attrs) {
        super(context, attrs);
        bindViews();
    }

    public PrinterSnapshotPanel(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        bindViews();
    }

    private void bindViews() {
        ButterKnife.bind(this, getView());
        printerSnapshotPanelContent.setVisibility(GONE);
        viewAllButton.setClickable(true);

        printerRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener(){
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                int topRowVerticalPosition =
                        (recyclerView == null || recyclerView.getChildCount() == 0) ? 0 : recyclerView.getChildAt(0).getTop();
                snapshotCallback.enableSwipeToRefresh(topRowVerticalPosition >= 0);

            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }
        });
    }

    @Override
    public Spannable getTitleSpannable() {
        SpannableStringBuilder builder;

        if (getViewModel() == null) {
            builder = new SpannableStringBuilder(getContext().getString(
                    R.string.printers_snapshot_panel_title));
        } else if (getViewModel().getDevices() == null) {
            builder = new SpannableStringBuilder(getContext().getString(
                    getViewModel().getBusinessUnitEnum() == BusinessUnitEnum.IHPS_PRESS ?
                            R.string.devices_snapshot_panel_title
                            : R.string.printers_snapshot_panel_title));
        } else {
            builder = new SpannableStringBuilder(String.format(
                    getContext().getString(
                            getViewModel().getBusinessUnitEnum() == BusinessUnitEnum.IHPS_PRESS ?
                                    R.string.devices_snapshot_panel_title_with_extra
                                    : R.string.printers_snapshot_panel_title_with_extra),
                    getViewModel().getDevices().size()));
        }
        return builder;
    }

    @Override
    public int getContentView() {
        return R.layout.printer_snapshot_content;
    }

    @Override
    public void updateViewModel(PrinterSnapshotViewModel viewModel) {
        setViewModel(viewModel);

        int numOfPrinters = viewModel == null ? 0 : viewModel.getDevices() == null ? 0 : viewModel.getDevices().size();

        setTitle(getTitleSpannable());

        if (numOfPrinters == 0) {
            printerSnapshotPanelContent.setVisibility(GONE);
            return;
        }

        int printersSize = numOfPrinters;
        if (viewModel.getBusinessUnitEnum() != BusinessUnitEnum.LATEX_PRINTER) {
            int maxNumOfPrintersToShow = viewModel.getNumberOfRows() * viewModel.getNumberOfColumns();
            printersSize = numOfPrinters > maxNumOfPrintersToShow ? maxNumOfPrintersToShow : numOfPrinters;
        }

        Collections.sort(getViewModel().getDevices(), BaseDeviceViewModel.STATUS_COMPARATOR);

        List<DeviceViewModel> deviceViewModels = getViewModel().getDevices().subList(0, printersSize);

        PrinterSnapshotAdapter printerSnapshotAdapter = new PrinterSnapshotAdapter(getContext(), deviceViewModels, viewModel.getBusinessUnitEnum(), this);

        int numberOfColumns = Math.min(viewModel.getNumberOfColumns(), deviceViewModels.size());
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), numberOfColumns);

        printerRecyclerView.setHasFixedSize(true);
        printerRecyclerView.setLayoutManager(gridLayoutManager);
        printerRecyclerView.setAdapter(printerSnapshotAdapter);
        printerRecyclerView.addItemDecoration(new DividerItemDecoration(getContext(), numOfPrinters, deviceViewModels.size(), DECORATOR_MARGIN, false));

        printerSnapshotPanelContent.setVisibility(VISIBLE);
    }

    protected String getEmptyCardText() {
        return getContext().getString(R.string.no_information_to_display);
    }

    @OnClick(R.id.button_view_all)
    void onViewAllButtonClicked() {
        if (snapshotCallback != null) {
            snapshotCallback.onPrinterSnapshotViewAllClicked();
        }
    }

    public void addPrinterSnapshotCallback(PrinterSnapshotCallback listener) {
        this.snapshotCallback = listener;
    }

    public int getNumberOfPrinters () {
        if (printerRecyclerView != null && printerRecyclerView.getAdapter() != null){
            return printerRecyclerView.getAdapter().getItemCount();
        }
        return 0;
    }

    public interface PrinterSnapshotCallback {
        void onPrinterSnapshotViewAllClicked();
        void enableSwipeToRefresh(boolean enable);
    }
}
