package com.hp.printosmobile.presentation.modules.drawer;

import android.text.TextUtils;

import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.data.remote.ApiServicesProvider;
import com.hp.printosmobile.data.remote.models.OrganizationBody;
import com.hp.printosmobile.data.remote.services.NotificationService;
import com.hp.printosmobile.data.remote.services.OrganizationService;
import com.hp.printosmobilelib.core.communications.remote.models.OrganizationJsonBody;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Response;
import rx.Observable;
import rx.functions.Func1;

/**
 * Created by Anwar Asbah on 8/29/16.
 */
public class NavigationManager {

    private static final String TAG = NavigationManager.class.getSimpleName();

    public static Observable<List<OrganizationViewModel>> getOrganizations() {

        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
        OrganizationService organizationService = serviceProvider.getOrganizationService();

        return organizationService.getOrganization().map(new Func1<Response<OrganizationBody>, List<OrganizationViewModel>>() {
            @Override
            public List<OrganizationViewModel> call(Response<OrganizationBody> organizationResponse) {
                if (organizationResponse.isSuccessful()) {
                    return parseOrganizations(organizationResponse.body());
                }
                return null;
            }
        });

    }

    public static Observable<String> unregisterNotification() {

        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
        NotificationService notificationService = serviceProvider.getNotificationService();
        PrintOSPreferences printOSPreferences = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext());
        String token = printOSPreferences.getNotificationToken();
        return notificationService.unRegisterDeviceForPushNotificationAsync(token)
                .map(new Func1<Response<Void>, String>() {
                         @Override
                         public String call(Response<Void> voidResponse) {
                             return "";
                         }
                     }

                );

    }

    public static Observable<String> changeContext(final OrganizationJsonBody organizationJsonBody) {

        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();

        OrganizationService organizationService = serviceProvider.getOrganizationService();

        return organizationService.changeOrganizationContext(organizationJsonBody).map(new Func1<ResponseBody, String>() {

            @Override
            public String call(ResponseBody responseBody) {
                try {
                    return responseBody.string();
                } catch (IOException e) {
                    return "error";
                }
            }
        });
    }

    private static List<OrganizationViewModel> parseOrganizations(OrganizationBody organizationBody) {

        if (organizationBody == null || organizationBody.getContexts() == null) {
            return null;
        }

        List<OrganizationViewModel> models = new ArrayList<>();

        boolean isHPUser = false;
        boolean isChannelUser = false;

        for (OrganizationBody.Organization organization : organizationBody.getContexts()) {

            OrganizationViewModel viewModel = new OrganizationViewModel();
            viewModel.setOrganizationId(organization.getId());
            viewModel.setOrganizationName(organization.getName());
            viewModel.setOrganizationTypeDisplayName(organization.getType());

            String orgType = organization.getType();
            if (!TextUtils.isEmpty(orgType)) {
                if (orgType.toUpperCase().contains(OrganizationViewModel.OrganizationType.HP.name())) {
                    isHPUser = true;
                } else if (orgType.toUpperCase().contains(OrganizationViewModel.OrganizationType.CHANNEL.name())) {
                    isChannelUser = true;
                }
            }

            models.add(viewModel);
        }

        if (isHPUser) {
            PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).
                    saveUserType(OrganizationViewModel.OrganizationType.HP.name());
        } else if (isChannelUser) {
            PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).
                    saveUserType(OrganizationViewModel.OrganizationType.CHANNEL.name());
        } else {
            PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).
                    saveUserType(OrganizationViewModel.OrganizationType.PSP.name());
        }

        return models;
    }

}