package com.hp.printosmobile.presentation.modules.today;

import android.content.Context;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.data.remote.models.PreferencesData;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;
import com.hp.printosmobile.presentation.modules.shared.BaseDeviceViewModel;
import com.hp.printosmobile.presentation.modules.shared.DeviceExtraData;
import com.hp.printosmobile.presentation.modules.shared.DeviceState;
import com.hp.printosmobile.presentation.modules.shared.DeviceViewModel;
import com.hp.printosmobile.presentation.modules.shared.DevicesUtils;
import com.hp.printosmobile.presentation.modules.shared.PrinterUtils;
import com.hp.printosmobile.presentation.modules.shared.Unit;
import com.hp.printosmobile.utils.HPLocaleUtils;
import com.hp.printosmobile.utils.HPScoreUtils;
import com.hp.printosmobilelib.ui.widgets.HPProgressBar;
import com.squareup.picasso.Picasso;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Osama Taha on 5/15/2016.
 * <p/>
 * TODO: Both {@link TodayDevicesAdapter} and {@link com.hp.printosmobile.presentation.modules.devices.DevicesAdapter} should be merged into a single class.
 */
public class TodayDevicesAdapter extends RecyclerView.Adapter<TodayDevicesAdapter.TodayDeviceViewHolder> {

    private static final int RT_SUPPORTED = 0;
    private static final int NON_RT_SUPPORTED = 1;
    private static final int SINGLE_PRESS = 2;
    private static final int NON_RT_SUPPORTED_SINGLE = 3;
    private static final int SCITEX_DEFAULT = 4;
    private static final int LATEX_DEFAULT = 5;
    private static final int TYPE_LATEX_DATA_NOT_AVAILABLE = 6;
    private static final int TYPE_LATEX_DATA_IS_NOT_UP_TO_DATE = 7;
    private static final int TYPE_LATEX_WRONG_TIME_ZONE = 8;
    private static final int TYPE_INDIGO_LOCKED = 9;

    private final TodayDeviceAdapterCallback callback;
    private final BusinessUnitEnum businessUnitEnum;
    private final PreferencesData.UnitSystem unitSystem;

    public Context context;
    public List<DeviceViewModel> deviceViewModels;

    public TodayDevicesAdapter(Context context, BusinessUnitEnum businessUnitEnum, List<DeviceViewModel> deviceViewModels, TodayDeviceAdapterCallback callback) {
        this.context = context;
        this.deviceViewModels = deviceViewModels;
        this.callback = callback;
        this.businessUnitEnum = businessUnitEnum;
        this.unitSystem = PrintOSPreferences.getInstance(context).getUnitSystem();
        if (businessUnitEnum == BusinessUnitEnum.LATEX_PRINTER && deviceViewModels != null) {
            Collections.sort(deviceViewModels, BaseDeviceViewModel.STATUS_COMPARATOR);
        }
    }

    @Override
    public int getItemViewType(int position) {

        DeviceViewModel deviceViewModel = deviceViewModels.get(position);

        switch (businessUnitEnum) {
            case INDIGO_PRESS:
                return deviceViewModel.getMessage() != null ? TYPE_INDIGO_LOCKED :
                        deviceViewModel.isRTSupported() ? RT_SUPPORTED : NON_RT_SUPPORTED;
            case SCITEX_PRESS:
                return SCITEX_DEFAULT;
            case LATEX_PRINTER:
                return getLatexItemViewType(deviceViewModel);
        }

        return 0;
    }

    @Override
    public TodayDeviceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        switch (businessUnitEnum) {

            case INDIGO_PRESS:

                switch (viewType) {

                    case RT_SUPPORTED:
                        return new TodayIndigoDeviceRTViewHolder(LayoutInflater.from(context)
                                .inflate(R.layout.today_device_item, parent, false));
                    case TYPE_INDIGO_LOCKED:
                        return new TodayIndigoLockedDeviceViewHolder(LayoutInflater.from(context)
                                .inflate(R.layout.today_device_item_locked, parent, false));
                    case SINGLE_PRESS:
                        return new TodayIndigoDeviceSingleViewHolder(LayoutInflater.from(context)
                                .inflate(R.layout.today_single_device_item, parent, false));
                    case NON_RT_SUPPORTED_SINGLE:
                        return new TodayIndigoDeviceViewHolder(LayoutInflater.from(context)
                                .inflate(R.layout.today_single_device_item_non_rt, parent, false));
                    default:
                        return new TodayIndigoDeviceViewHolder(LayoutInflater.from(context)
                                .inflate(R.layout.today_device_item_non_rt, parent, false));
                }

            case SCITEX_PRESS:

                return new TodayScitexDeviceRTViewHolder(LayoutInflater.from(context)
                        .inflate(R.layout.today_scitex_device_item, parent, false));

            case LATEX_PRINTER:

                switch (viewType) {

                    case LATEX_DEFAULT:
                        return new TodayLatexDefaultDeviceViewHolder(LayoutInflater.from(context)
                                .inflate(R.layout.today_latex_device_default_layout, parent, false));
                    case TYPE_LATEX_WRONG_TIME_ZONE:
                        return new TodayLatexDeviceWrongTimeZoneViewHolder(LayoutInflater.from(context)
                                .inflate(R.layout.today_latex_device_wrong_time_zone, parent, false));
                    case TYPE_LATEX_DATA_NOT_AVAILABLE:
                        return new TodayLatexDeviceDataNotAvailableViewHolder(LayoutInflater.from(context)
                                .inflate(R.layout.today_latex_device_not_under_warranty_layout, parent, false));
                    case TYPE_LATEX_DATA_IS_NOT_UP_TO_DATE:
                        return new TodayLatexDeviceDataNotUpToDateViewHolder(LayoutInflater.from(context)
                                .inflate(R.layout.latex_device_data_no_up_to_date, parent, false));
                }
        }

        return null;
    }

    private int getLatexItemViewType(DeviceViewModel deviceViewModel) {

        switch (deviceViewModel.getDeviceLastUpdateType()) {
            case DATA_UP_TO_DATE:
                return LATEX_DEFAULT;
            case LAST_UPDATE_IN_24HRS_TODAY:
            case LAST_UPDATE_IN_24HRS_YESTERDAY:
                return TYPE_LATEX_DATA_IS_NOT_UP_TO_DATE;
            case LAST_UPDATE_YESTERDAY:
            case LAST_UPDATE_DAYS_AGO:
            case DATA_UNAVAILABLE:
                return TYPE_LATEX_DATA_NOT_AVAILABLE;
            case WRONG_TIME_ZONE:
                return TYPE_LATEX_WRONG_TIME_ZONE;
            default:
                return LATEX_DEFAULT;
        }
    }

    @Override
    public void onBindViewHolder(TodayDeviceViewHolder baseHolder, int position) {

        DeviceViewModel deviceViewModel = deviceViewModels.get(position);

        if (businessUnitEnum == BusinessUnitEnum.INDIGO_PRESS) {

            TodayIndigoBaseDeviceViewHolder holder = (TodayIndigoBaseDeviceViewHolder) baseHolder;

            Picasso.with(context).load(deviceViewModel.getDeviceImageResource()).into(holder.printerImageView);

            String impressionType = deviceViewModel.getImpressionType();
            String deviceName = deviceViewModel.getName();
            String printerName = impressionType == null ? deviceName : String.format("%s (%s)", deviceViewModel.getName(), impressionType);
            holder.printerNameTextView.setText(printerName);

            holder.model = deviceViewModel;

            if(holder instanceof TodayIndigoDeviceViewHolder) {
                ((TodayIndigoDeviceViewHolder)holder).hasAdviceImage.setVisibility(deviceViewModel.hasUnsuppressedAdvices() ? View.VISIBLE : View.INVISIBLE);
            }

            if (holder instanceof TodayIndigoDeviceRTViewHolder) {

                ((TodayIndigoDeviceRTViewHolder) holder).model = deviceViewModel;

                Picasso.with(context).load(deviceViewModel.getDeviceImageResource()).into(((TodayIndigoDeviceRTViewHolder) holder).printerImageView);
                Picasso.with(context).load(DevicesUtils.getTodayPanelDeviceMaintenanceImage(deviceViewModel.getMaintenanceState())).into(((TodayIndigoDeviceRTViewHolder) holder).maintenanceStateImageView);

                int timeInState = deviceViewModel.getTimeInStateMin();
                if (timeInState == 0) {
                    ((TodayIndigoDeviceRTViewHolder) holder).printingTimeTextView.setText(context.getString(R.string.press_entered_state));
                    ((TodayIndigoDeviceRTViewHolder) holder).printingTimeTextView.setTextColor(ResourcesCompat.getColor(context.getResources(),
                            deviceViewModel.getDeviceState().getColor(), null));
                } else {
                    ((TodayIndigoDeviceRTViewHolder) holder).printingTimeTextView.setText(HPLocaleUtils.getLocalizedTimeString(context, timeInState, TimeUnit.MINUTES));
                    ((TodayIndigoDeviceRTViewHolder) holder).printingTimeTextView.setTextColor(ResourcesCompat.getColor(context.getResources(),
                            R.color.hp_default, null));
                }

                double shiftTarget = deviceViewModel.getPrintVolumeShiftTargetValue();
                double dailyTarget = deviceViewModel.getPrintVolumeIntraDailyTargetValue();

                ((TodayIndigoDeviceRTViewHolder) holder).printingVolumeNominatorTextView
                        .setText(HPLocaleUtils.getLocalizedValue((int) deviceViewModel.getPrintVolumeValue()));
                ((TodayIndigoDeviceRTViewHolder) holder).printingVolumeNominatorTextView
                        .setTextColor(HPScoreUtils.getProgressColor(context, deviceViewModel.getPrintVolumeValue(), dailyTarget, false));
                ((TodayIndigoDeviceRTViewHolder) holder).printingVolumeDenominatorTextView
                        .setText(context.getString(R.string.today_imp_denominator,
                                HPLocaleUtils.getLocalizedValue((int) shiftTarget)));

                if (deviceViewModel.isPressDisconnected()) {
                    ((TodayIndigoDeviceRTViewHolder) holder).deviceStateTextView.setText(context.getString(R.string.press_disconnected));
                    ((TodayIndigoDeviceRTViewHolder) holder).deviceStateTextView.setTextColor(ResourcesCompat.getColor(context.getResources(), R.color.c1, null));
                    ((TodayIndigoDeviceRTViewHolder) holder).timeInStateContainer.setVisibility(View.GONE);
                } else {
                    ((TodayIndigoDeviceRTViewHolder) holder).deviceStateTextView.setText(DevicesUtils.getDeviceStateString(context, deviceViewModel.getDeviceState()));
                    ((TodayIndigoDeviceRTViewHolder) holder).deviceStateTextView.setTextColor(ResourcesCompat.getColor(context.getResources(), deviceViewModel.getDeviceState().getColor(), null));
                    ((TodayIndigoDeviceRTViewHolder) holder).timeInStateContainer.setVisibility(View.VISIBLE);
                }


                DevicesUtils.updateProgressBar(context, ((TodayIndigoDeviceRTViewHolder) holder).progressBar,
                        deviceViewModel.getPrintVolumeShiftTargetValue(),
                        deviceViewModel.getPrintVolumeIntraDailyTargetValue(),
                        deviceViewModel.getPrintVolumeValue());

            }

            if(holder instanceof TodayIndigoLockedDeviceViewHolder) {
                String msg = deviceViewModel.getMessage() != null && deviceViewModel.getMessage().getMsg() != null ?
                        deviceViewModel.getMessage().getMsg() : "";
                ((TodayIndigoLockedDeviceViewHolder) holder).messageTextView.setText(msg);
            }
        } else if (businessUnitEnum == BusinessUnitEnum.SCITEX_PRESS) {

            TodayScitexDeviceRTViewHolder holder = (TodayScitexDeviceRTViewHolder) baseHolder;

            holder.model = deviceViewModel;

            Picasso.with(context).load(deviceViewModel.getDeviceImageResource()).into((holder.printerImageView));

            holder.printerNameTextView.setText(deviceViewModel.getName());

            holder.printingVolumeTextView.setText(HPLocaleUtils.getLocalizedValue((int) deviceViewModel.getPrintVolumeValue()));

            if (deviceViewModel.isPressDisconnected() || deviceViewModel.getState() == DeviceState.UNKNOWN) {

                holder.deviceStateTextView.setText(context.getString(R.string.press_disconnected));
                holder.deviceStateTextView.setTextColor(ResourcesCompat.getColor(context.getResources(), R.color.c1, null));
                holder.timeInStateContainer.setVisibility(View.GONE);

            } else {

                holder.deviceStateTextView.setText(DevicesUtils.getDeviceStateString(context, deviceViewModel.getDeviceState()));
                holder.deviceStateTextView.setTextColor(ResourcesCompat.getColor(context.getResources(), deviceViewModel.getDeviceState().getColor(), null));

                int timeInState = deviceViewModel.getTimeInStateMin();

                if (timeInState == 0) {
                    holder.printingTimeTextView.setText(context.getString(R.string.press_entered_state));
                    holder.printingTimeTextView.setTextColor(ResourcesCompat.getColor(context.getResources(),
                            deviceViewModel.getDeviceState().getColor(), null));
                } else {
                    holder.printingTimeTextView.setText(HPLocaleUtils.getLocalizedTimeString(context, timeInState, TimeUnit.MINUTES));
                    holder.printingTimeTextView.setTextColor(ResourcesCompat.getColor(context.getResources(),
                            R.color.hp_default, null));
                }
                holder.timeInStateContainer.setVisibility(View.VISIBLE);
            }

            DevicesUtils.updateProgressBar(context, holder.progressBar,
                    deviceViewModel.getPrintVolumeShiftTargetValue(),
                    deviceViewModel.getPrintVolumeIntraDailyTargetValue(),
                    deviceViewModel.getPrintVolumeValue());

        } else if (businessUnitEnum == BusinessUnitEnum.LATEX_PRINTER) {

            TodayLatexDeviceViewHolder holder = (TodayLatexDeviceViewHolder) baseHolder;
            holder.model = deviceViewModel;

            DeviceExtraData printerExtraData = deviceViewModel.getExtraData();

            if (deviceViewModel.getModel() != null) {
                holder.printerNameTextView.setText(context.getString(R.string.printer_name_with_model,
                        deviceViewModel.getName(), deviceViewModel.getModel()));
            } else {
                holder.printerNameTextView.setText(deviceViewModel.getName());
            }

            Picasso.with(context)
                    .load(deviceViewModel.getDeviceImageResource())
                    .into(holder.printerImageView);

            if (getItemViewType(position) == TYPE_LATEX_DATA_NOT_AVAILABLE) {

                TodayLatexDeviceDataNotAvailableViewHolder defaultHolder = (TodayLatexDeviceDataNotAvailableViewHolder) holder;
                defaultHolder.dataUnavailableTextView.setText(deviceViewModel.getDeviceLastUpdateType()
                        .getDisplayMessage(context, deviceViewModel.getLastUpdateTime()));

            } else if (getItemViewType(position) == TYPE_LATEX_WRONG_TIME_ZONE) {

                TodayLatexDeviceWrongTimeZoneViewHolder wrongTZHolder = (TodayLatexDeviceWrongTimeZoneViewHolder) holder;
                wrongTZHolder.msgTextView.setText(deviceViewModel.getDeviceLastUpdateType()
                        .getDisplayMessage(context, deviceViewModel.getLastUpdateTime()));

            } else if (getItemViewType(position) == TYPE_LATEX_DATA_IS_NOT_UP_TO_DATE) {

                TodayLatexDeviceDataNotUpToDateViewHolder defaultHolder = (TodayLatexDeviceDataNotUpToDateViewHolder) holder;

                double shiftTarget = deviceViewModel.getPrintVolumeShiftTargetValue();
                double dailyTarget = deviceViewModel.getPrintVolumeIntraDailyTargetValue();

                defaultHolder.printingVolumeNominatorTextView
                        .setText(HPLocaleUtils.getLocalizedValue((int) deviceViewModel.getPrintVolumeValue()));
                defaultHolder.printingVolumeNominatorTextView
                        .setTextColor(HPScoreUtils.getProgressColor(context, deviceViewModel.getPrintVolumeValue(), dailyTarget, false));
                defaultHolder.printingVolumeDenominatorTextView
                        .setText(context.getString(R.string.today_imp_denominator, HPLocaleUtils.getLocalizedValue((int) shiftTarget)));
                defaultHolder.printVolumeUnitTextView.setText(Unit.SQM.getUnitText(context, unitSystem, Unit.UnitStyle.DESCRIPTION));

                DevicesUtils.updateProgressBar(context, defaultHolder.progressBar,
                        deviceViewModel.getPrintVolumeShiftTargetValue(),
                        deviceViewModel.getPrintVolumeIntraDailyTargetValue(),
                        deviceViewModel.getPrintVolumeValue(), true);

                defaultHolder.dataUnavailableTextView.setText(deviceViewModel.getDeviceLastUpdateType()
                        .getDisplayMessage(context, deviceViewModel.getLastUpdateTime()));


            } else if (getItemViewType(position) == LATEX_DEFAULT) {

                TodayLatexDefaultDeviceViewHolder defaultHolder = (TodayLatexDefaultDeviceViewHolder) holder;

                Picasso.with(context)
                        .load(PrinterUtils.getPrinterStateImage(deviceViewModel.getState()))
                        .into(defaultHolder.imagePrinterStatus);

                defaultHolder.hasAdviceImage.setVisibility(deviceViewModel.hasUnsuppressedAdvices() ? View.VISIBLE : View.INVISIBLE);

                Picasso.with(context)
                        .load(deviceViewModel.getDeviceImageResource())
                        .into(holder.printerImageView);

                int timeInState = deviceViewModel.getTimeInStateMin();

                if (timeInState == 0) {
                    defaultHolder.printingTimeTextView.setText(context.getString(R.string.press_entered_state));
                    defaultHolder.printingTimeTextView.setTextColor(ResourcesCompat.getColor(context.getResources(),
                            deviceViewModel.getDeviceState().getColor(), null));
                } else {
                    defaultHolder.printingTimeTextView.setText(HPLocaleUtils.getLocalizedTimeString(context, timeInState, TimeUnit.MINUTES));
                    defaultHolder.printingTimeTextView.setTextColor(ResourcesCompat.getColor(context.getResources(),
                            R.color.hp_default, null));
                }

                double shiftTarget = deviceViewModel.getPrintVolumeShiftTargetValue();
                double dailyTarget = deviceViewModel.getPrintVolumeIntraDailyTargetValue();

                defaultHolder.printVolumeUnitTextView.setText(Unit.SQM.getUnitText(context, unitSystem, Unit.UnitStyle.DESCRIPTION));

                defaultHolder.printingVolumeNominatorTextView
                        .setText(HPLocaleUtils.getLocalizedValue((int) deviceViewModel.getPrintVolumeValue()));
                defaultHolder.printingVolumeNominatorTextView
                        .setTextColor(HPScoreUtils.getProgressColor(context, deviceViewModel.getPrintVolumeValue(), dailyTarget, false));
                defaultHolder.printingVolumeDenominatorTextView
                        .setText(context.getString(R.string.today_imp_denominator, HPLocaleUtils.getLocalizedValue((int) shiftTarget)));

                if (printerExtraData.getFrontPanelMessage() != null) {
                    defaultHolder.printerStatusMessage.setVisibility(View.VISIBLE);
                    defaultHolder.printerStatusMessage.setText(deviceViewModel.getExtraData().getFrontPanelMessage());
                } else {
                    defaultHolder.printerStatusMessage.setVisibility(View.GONE);
                }

                if (deviceViewModel.isPressDisconnected()) {

                    defaultHolder.deviceStateTextView.setText(context.getString(R.string.press_disconnected));
                    defaultHolder.deviceStateTextView.setTextColor(ResourcesCompat.getColor(context.getResources(), R.color.c1, null));
                    defaultHolder.timeInStateContainer.setVisibility(View.GONE);

                } else {

                    defaultHolder.deviceStateTextView.setText(DevicesUtils.getDeviceStateString(context, deviceViewModel.getDeviceState()));
                    defaultHolder.deviceStateTextView.setTextColor(ResourcesCompat.getColor(context.getResources(), deviceViewModel.getDeviceState().getColor(), null));
                    defaultHolder.timeInStateContainer.setVisibility(View.VISIBLE);

                    if (deviceViewModel.getState() == DeviceState.NULL) {

                        defaultHolder.timeInStateContainer.setVisibility(View.GONE);

                    }

                }

                DevicesUtils.updateProgressBar(context, defaultHolder.progressBar,
                        deviceViewModel.getPrintVolumeShiftTargetValue(),
                        deviceViewModel.getPrintVolumeIntraDailyTargetValue(),
                        deviceViewModel.getPrintVolumeValue());
            }
        }

    }


    @Override
    public int getItemCount() {
        return deviceViewModels == null ? 0 : deviceViewModels.size();
    }


    class TodayDeviceViewHolder extends RecyclerView.ViewHolder {

        public TodayDeviceViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    class TodayScitexDeviceViewHolder extends TodayDeviceViewHolder {

        @Bind(R.id.image_printer)
        ImageView printerImageView;
        @Bind(R.id.text_printer_name)
        TextView printerNameTextView;

        public TodayScitexDeviceViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    class TodayScitexDeviceRTViewHolder extends TodayScitexDeviceViewHolder {

        @Bind(R.id.progress_bar)
        HPProgressBar progressBar;
        @Bind(R.id.text_time_in)
        TextView printingTimeTextView;
        @Bind(R.id.text_printing_state)
        TextView deviceStateTextView;
        @Bind(R.id.text_printing_volume)
        TextView printingVolumeTextView;
        @Bind(R.id.time_in_state_container)
        View timeInStateContainer;

        DeviceViewModel model;

        public TodayScitexDeviceRTViewHolder(View itemView) {
            super(itemView);
        }
    }

    class TodayIndigoBaseDeviceViewHolder extends  TodayDeviceViewHolder {
        @Bind(R.id.image_printer)
        ImageView printerImageView;
        @Bind(R.id.text_printer_name)
        TextView printerNameTextView;

        DeviceViewModel model;

        public TodayIndigoBaseDeviceViewHolder (View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    class TodayIndigoDeviceViewHolder extends TodayIndigoBaseDeviceViewHolder {

        @Bind(R.id.image_has_advice)
        ImageView hasAdviceImage;

        public TodayIndigoDeviceViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            hasAdviceImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (callback != null && model != null) {
                        callback.onHasAdviceIconClicked(model);
                    }
                }
            });
        }
    }

    class TodayIndigoDeviceRTViewHolder extends TodayIndigoDeviceViewHolder {

        @Bind(R.id.image_maintenance_state)
        ImageView maintenanceStateImageView;
        @Bind(R.id.progress_bar)
        HPProgressBar progressBar;
        @Bind(R.id.text_time_in)
        TextView printingTimeTextView;
        @Bind(R.id.text_printing_state)
        TextView deviceStateTextView;
        @Bind(R.id.text_printing_volume_nominator)
        TextView printingVolumeNominatorTextView;
        @Bind(R.id.text_printing_volume_dinominator)
        TextView printingVolumeDenominatorTextView;
        @Bind(R.id.time_in_state_container)
        View timeInStateContainer;

        DeviceViewModel model;

        public TodayIndigoDeviceRTViewHolder(View itemView) {
            super(itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (callback != null && model != null) {
                        callback.onDeviceClicked(model);
                    }
                }
            });

            maintenanceStateImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    callback.onMaintenanceIconClicked(view, model);
                }
            });

        }
    }

    class TodayIndigoDeviceSingleViewHolder extends TodayIndigoDeviceRTViewHolder {

        @Bind(R.id.time_container)
        View timeView;
        @Bind(R.id.text_time_to_done)
        TextView textTimeToDone;
        @Bind(R.id.print_queue_jobs_in_queue_text_view)
        TextView jobsInQueueTextView;

        public TodayIndigoDeviceSingleViewHolder(View itemView) {
            super(itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (callback != null && model != null) {
                        callback.onDeviceClicked(model);
                    }
                }
            });
        }
    }

    class TodayLatexDeviceViewHolder extends TodayDeviceViewHolder {

        @Bind(R.id.image_printer)
        ImageView printerImageView;
        @Bind(R.id.text_printer_name)
        TextView printerNameTextView;

        DeviceViewModel model;

        public TodayLatexDeviceViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (callback != null && model != null && !model.isWrongTZ()) {
                        callback.onDeviceClicked(model);
                    }
                }
            });
        }
    }

    class TodayLatexDefaultDeviceViewHolder extends TodayLatexDeviceViewHolder {

        @Bind(R.id.image_has_advice)
        ImageView hasAdviceImage;
        @Bind(R.id.image_printer_state)
        ImageView imagePrinterStatus;
        @Bind(R.id.progress_bar)
        HPProgressBar progressBar;
        @Bind(R.id.text_time_in)
        TextView printingTimeTextView;
        @Bind(R.id.text_printing_state)
        TextView deviceStateTextView;
        @Bind(R.id.print_volume_unit_text_view)
        TextView printVolumeUnitTextView;
        @Bind(R.id.text_printing_volume_nominator)
        TextView printingVolumeNominatorTextView;
        @Bind(R.id.text_printing_volume_dinominator)
        TextView printingVolumeDenominatorTextView;
        @Bind(R.id.text_printer_status_message)
        TextView printerStatusMessage;
        @Bind(R.id.time_in_state_container)
        View timeInStateContainer;

        public TodayLatexDefaultDeviceViewHolder(View itemView) {
            super(itemView);
        }

    }

    class TodayLatexDeviceDataNotAvailableViewHolder extends TodayLatexDeviceViewHolder {

        @Bind(R.id.text_data_unavailable)
        TextView dataUnavailableTextView;

        public TodayLatexDeviceDataNotAvailableViewHolder(View itemView) {
            super(itemView);
        }

    }

    class TodayLatexDeviceWrongTimeZoneViewHolder extends TodayLatexDeviceViewHolder {

        @Bind(R.id.msg_text_view)
        TextView msgTextView;

        public TodayLatexDeviceWrongTimeZoneViewHolder(View itemView) {
            super(itemView);
        }

    }

    class TodayLatexDeviceDataNotUpToDateViewHolder extends TodayLatexDeviceViewHolder {

        @Bind(R.id.progress_bar)
        HPProgressBar progressBar;
        @Bind(R.id.text_printing_volume_nominator)
        TextView printingVolumeNominatorTextView;
        @Bind(R.id.text_printing_volume_dinominator)
        TextView printingVolumeDenominatorTextView;
        @Bind(R.id.text_data_unavailable)
        TextView dataUnavailableTextView;
        @Bind(R.id.print_volume_unit_text_view)
        TextView printVolumeUnitTextView;

        public TodayLatexDeviceDataNotUpToDateViewHolder(View itemView) {
            super(itemView);
        }

    }

    class TodayIndigoLockedDeviceViewHolder extends TodayIndigoBaseDeviceViewHolder{

        @Bind(R.id.message_text_view)
        TextView messageTextView;

        public TodayIndigoLockedDeviceViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public interface TodayDeviceAdapterCallback {

        void onDeviceClicked(DeviceViewModel model);

        void onHasAdviceIconClicked(DeviceViewModel model);

        void onMaintenanceIconClicked(View view, DeviceViewModel model);
    }

}
