package com.hp.printosmobile.presentation.modules.tooltip;

import android.content.Context;
import android.widget.TextView;

import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.modules.shared.MaintenanceState;

/**
 * Created by osama on 12/17/16.
 */
public class MaintenanceFanTooltip extends Tooltip<MaintenanceState> {

    private TextView hintTextView;

    public MaintenanceFanTooltip(Context context) {
        super(context);
    }

    @Override
    protected void initContentView() {
        hintTextView = (TextView) contentView.findViewById(R.id.hint_text_view);
    }

    @Override
    public int getLayoutResourceId() {
        return R.layout.main_fan_tool_tip_layout;
    }

    @Override
    public void updateView(MaintenanceState maintenanceState) {
        hintTextView.setText(maintenanceState.getTooltipText());
    }

}
