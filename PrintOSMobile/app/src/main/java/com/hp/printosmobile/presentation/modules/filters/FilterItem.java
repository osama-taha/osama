package com.hp.printosmobile.presentation.modules.filters;

import java.util.Comparator;
import java.util.List;

/**
 * Created by Osama Taha on 10/9/16.
 */
public interface FilterItem extends Comparable<FilterItem> {

    String getId();

    String getName();

    List<FilterItem> getItems();

    void setSelected(boolean selected);

    boolean isSelected();

    public static Comparator<FilterItem> NAME_COMPARATOR = new Comparator<FilterItem>() {
        @Override
        public int compare(FilterItem lhs, FilterItem rhs) {

            if (lhs == null && rhs == null) return 0;
            if (lhs == null && rhs != null) return 1;
            if (lhs != null && rhs == null) return -1;

            String lhsName = lhs.getName();
            String rhsName = rhs.getName();

            if (lhsName == null && rhsName == null) return 0;
            if (lhsName == null && rhsName != null) return 1;
            if (lhsName != null && rhsName == null) return -1;

            return lhsName.compareTo(rhsName);
        }
    };
}