
package com.hp.printosmobile.data.remote.models.ExtendedStatistics;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Notifications {

    @JsonProperty("front_panel_message")
    private String frontPanelMessage;
    @JsonProperty("list")
    private java.util.List<List> list = new ArrayList<List>();
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * @return The frontPanelMessage
     */
    @JsonProperty("front_panel_message")
    public String getFrontPanelMessage() {
        return frontPanelMessage;
    }

    /**
     * @param frontPanelMessage The front_panel_message
     */
    @JsonProperty("front_panel_message")
    public void setFrontPanelMessage(String frontPanelMessage) {
        this.frontPanelMessage = frontPanelMessage;
    }

    /**
     * @return The list
     */
    @JsonProperty("list")
    public java.util.List<List> getList() {
        return list;
    }

    /**
     * @param list The list
     */
    @JsonProperty("list")
    public void setList(java.util.List<List> list) {
        this.list = list;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class List {

        @JsonProperty("description")
        private String description;
        @JsonProperty("time_stamp")
        private String timeStamp;
        @JsonProperty("severity")
        private String severity;
        @JsonProperty("alert_id")
        private String alertId;
        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<String, Object>();

        /**
         * @return The description
         */
        @JsonProperty("description")
        public String getDescription() {
            return description;
        }

        /**
         * @param description The description
         */
        @JsonProperty("description")
        public void setDescription(String description) {
            this.description = description;
        }

        /**
         * @return The timeStamp
         */
        @JsonProperty("time_stamp")
        public String getTimeStamp() {
            return timeStamp;
        }

        /**
         * @param timeStamp The time_stamp
         */
        @JsonProperty("time_stamp")
        public void setTimeStamp(String timeStamp) {
            this.timeStamp = timeStamp;
        }

        /**
         * @return The severity
         */
        @JsonProperty("severity")
        public String getSeverity() {
            return severity;
        }

        /**
         * @param severity The severity
         */
        @JsonProperty("severity")
        public void setSeverity(String severity) {
            this.severity = severity;
        }

        /**
         * @return The alertId
         */
        @JsonProperty("alert_id")
        public String getAlertId() {
            return alertId;
        }

        /**
         * @param alertId The alert_id
         */
        @JsonProperty("alert_id")
        public void setAlertId(String alertId) {
            this.alertId = alertId;
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }

    }
}
