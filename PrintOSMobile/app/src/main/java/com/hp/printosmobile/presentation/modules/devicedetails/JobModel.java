package com.hp.printosmobile.presentation.modules.devicedetails;

import com.hp.printosmobile.R;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Anwar Asbah on 8/17/16.
 */
public class JobModel implements Serializable {

    public enum EndState {

        PROCESSING("processing", R.string.job_status_processing, R.color.hp_default),
        ABORTED("aborted", R.string.job_status_aborted, R.color.hp_default),
        CANCELED("canceled", R.string.job_status_canceled, R.color.hp_default),
        COMPLETED("completed", R.string.job_status_completed, R.color.hp_default),
        CREATING("creating", R.string.job_status_creating, R.color.hp_default),
        PENDING("pending", R.string.job_status_pending, R.color.hp_default),
        PENDING_HELD("pending-held", R.string.job_status_pending_held, R.color.hp_default),
        PROCESSING_STOPPED("ProcessingStopped", R.string.job_status_stopped, R.color.hp_default),
        STARTED("Started", R.string.job_status_started, R.color.hp_default),
        TERMINATING("Terminating", R.string.job_status_terminating, R.color.hp_default),
        EMPTY("Empty", R.string.job_status_empty, R.color.hp_default),
        PRINTING("Printing", R.string.job_status_printing, R.color.c58a),
        HELD("held", R.string.job_status_held, R.color.hp_default),
        CANCELED_BY_PRINTER("Canceled by printer", R.string.job_status_printer_cancel, R.color.hp_default),
        CANCELED_BY_USER("Canceled by user", R.string.job_status_user_cancel, R.color.hp_default),
        UNKNOWN("UNKNOWN", R.string.job_status_unknown, R.color.hp_default);

        private String name;
        private int localizedNameID;
        private final int stateTextColorResId;

        EndState(String name, int localizedNameID, int stateTextColorResId) {
            this.name = name;
            this.localizedNameID = localizedNameID;
            this.stateTextColorResId = stateTextColorResId;
        }

        public String getName() {
            return name;
        }

        public int getLocalizedNameID() {
            return localizedNameID;
        }

        public int getStateTextColorResId() {
            return stateTextColorResId;
        }

        public static EndState from(String state) {
            if (state == null) {
                return UNKNOWN;
            }
            for (EndState endState : EndState.values()) {
                if (endState.getName().toLowerCase().equals(state.toLowerCase()))
                    return endState;
            }

            return UNKNOWN;
        }
    }

    private String jobName;
    private String jobImageUrl;
    private Date finishTime;
    private float progress;
    private EndState endState;

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getJobImageUrl() {
        return jobImageUrl;
    }

    public void setJobImageUrl(String jobImageUrl) {
        this.jobImageUrl = jobImageUrl;
    }

    public Date getFinishTime() {
        return finishTime;
    }

    public void setFinishTime(Date finishTime) {
        this.finishTime = finishTime;
    }

    public EndState getEndState() {
        return endState;
    }

    public void setEndState(EndState endState) {
        this.endState = endState;
    }

    public void setProgress(float progress) {
        this.progress = progress;
    }

    public float getProgress() {
        return progress;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("JobModel{");
        sb.append("jobName='").append(jobName).append('\'');
        sb.append(", jobImageUrl='").append(jobImageUrl).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
