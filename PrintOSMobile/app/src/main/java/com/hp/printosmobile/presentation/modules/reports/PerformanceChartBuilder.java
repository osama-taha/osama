package com.hp.printosmobile.presentation.modules.reports;

import android.app.Activity;
import android.os.Handler;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.mikephil.charting.charts.CombinedChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.CombinedData;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.XAxisValueFormatter;
import com.github.mikephil.charting.formatter.YAxisValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.ChartTouchListener;
import com.github.mikephil.charting.listener.OnChartGestureListener;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ViewPortHandler;
import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.modules.main.ReportChartTypeEnum;
import com.hp.printosmobile.utils.HPLocaleUtils;
import com.hp.printosmobilelib.core.utils.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This chart class allows switching between lines and bars data.
 * <p/>
 * 1-Building the data-sets.
 * 2-Building the axises.
 * 3-Customizing the legends.
 * 4.Scaling the chart.
 * <p/>
 * <p/>
 * Created Osama Taha on 06/09/2015.
 */
public class PerformanceChartBuilder
        extends PerformanceChartBuilderInterface<CombinedChart>
        implements View.OnClickListener,
        OnChartValueSelectedListener,
        View.OnLongClickListener,
        OnChartGestureListener {

    private static final String TAG = PerformanceChartBuilder.class.getSimpleName();

    private static final float MAX_ZOOM_OUT = 9.0f;
    private static final float CHART_SCALE_VALUE = 2.2f;
    private static final float CHART_SCALE_Y_VALUE = 1;
    private static final float DRAG_DECELERATION_FRICTION_COEF = 0.4f;
    private static final float BAR_CHART_Y_AXIS_MAX_VALUE = 100f;
    private static final int NUM_OFF_PRESS_PER_LINE = 4;
    private static final long INITIAL_PERIOD_UPDATE_TIME_DELAY = 100;
    public PerformanceChartBuilderCallbacks callbacks;
    public ReportChartTypeEnum reportChartType = ReportChartTypeEnum.UNKNOWN;
    private Activity context;
    private ChartConfiguration chartConfiguration;
    private View chartView;
    private List<String> xValues;
    private int xValuesCount;
    private ReportKpiViewModel selectedKpi;
    private List<String> xValuesZoomedIn;
    private List<String> xValuesZoomedOut;
    private LinearLayout legendLayout;
    private RecyclerView pressLegendLayout;
    private PressLegendAdapter pressLegendAdapter;
    private int totalPoints;
    float prevChartScale = 0;
    private TextView allValuesDescriptionLabel;

    private boolean resetLineChartScale = false;

    private PerformanceChartBuilder(Activity context, View chartView, TextView allValuesDescriptionLabel) {
        this.context = context;
        this.chartView = chartView;
        this.allValuesDescriptionLabel = allValuesDescriptionLabel;
        resetLineChartScale = true;
    }

    public static PerformanceChartBuilder create(Activity context, View chartView, TextView allValuesDescriptionLabel) {
        return new PerformanceChartBuilder(context, chartView, allValuesDescriptionLabel);
    }

    public PerformanceChartBuilder setConfigurations(ChartConfiguration configuration) {
        this.chartConfiguration = configuration;
        return this;
    }

    public PerformanceChartBuilder setCallbacks(PerformanceChartBuilderCallbacks callbacks) {
        this.callbacks = callbacks;
        return this;
    }

    public ReportKpiViewModel getSelectedKpi() {
        return selectedKpi;
    }

    public void setSelectedKpi(ReportKpiViewModel selectedKpi) {
        this.selectedKpi = selectedKpi;
    }

    public void displayKpi(ReportKpiViewModel kpiModel) {
        selectedKpi = kpiModel;
        setXAxisValues(kpiModel);
        drawLegends(kpiModel);
        updateLegendIconsSelection(kpiModel);

        if (kpiModel.getSubKPIs() != null && kpiModel.getSubKPIs().size() > 0) {
            if (kpiModel.isAggregate()) {
                drawReport(kpiModel);
                callbacks.onChartInitialized(kpiModel);
            } else {
                for (ReportKpiViewModel model : kpiModel.getSubKPIs()) {
                    if (model.getXAxisValues() != null) {
                        drawReport(model);
                        updateLegendIconsSelection(model);
                        callbacks.onChartInitialized(model);
                        break;
                    }
                }
            }
        } else {
            drawReport(kpiModel);
            callbacks.onChartInitialized(kpiModel);
        }

        updatePointsExpression();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                updatePeriod();
                updateLabel();

            }
        }, INITIAL_PERIOD_UPDATE_TIME_DELAY);
    }

    private void updateLabel() {
        if(allValuesDescriptionLabel != null){
            int power = (int) (Math.log10(chart.getAxisLeft().mAxisMaximum) / 3);
            if(power > 0) {
                allValuesDescriptionLabel.setText(context.getString(R.string.all_values_in, (
                        context.getString(power == 1 ? R.string.thousands : power == 2 ?
                                R.string.millions : R.string.billions))));
            }
            allValuesDescriptionLabel.setVisibility(power > 0 ? View.VISIBLE : View.GONE);
        }
    }

    private void drawReport(ReportKpiViewModel kpi) {

        Analytics.sendEvent(Analytics.MOBILE_PRINT_BEAT_CATEGORY, Analytics.REPORT_SET_REPORT_TYPE_ACTION, kpi.getName());

        pressLegendLayout = (RecyclerView) chartView.findViewById(R.id.press_legend);
        pressLegendLayout.setVisibility(View.GONE);

        this.reportChartType = kpi.getReportChartTypeEnum();
        clearPressLegend();

        CombinedData combinedData = null;

        if (this.reportChartType == ReportChartTypeEnum.BAR) {
            this.xValues = xValuesZoomedOut;
            combinedData = new CombinedData(xValues);
            combinedData.setData(getBarData(kpi));

            chart.getAxisLeft().setAxisMaxValue(BAR_CHART_Y_AXIS_MAX_VALUE);
            chart.getAxisLeft().setValueFormatter(new YAxisValueFormatter() {
                @Override
                public String getFormattedValue(float value, YAxis yAxis) {
                    return (int) value + StringUtils.PERCENT_SYMBOL;
                }
            });
        } else {
            this.xValues = getXAxisValues();
            combinedData = new CombinedData(xValues);
            combinedData.setData(getLineData(kpi));
            chart.getAxisLeft().resetAxisMaxValue();
            chart.getAxisLeft().setValueFormatter(new YAxisValueFormatter() {
                @Override
                public String getFormattedValue(float value, YAxis yAxis) {
                    return HPLocaleUtils.getLocalizedValueString(value, yAxis.mAxisMaximum);
                }
            });

        }

        chart.setData(combinedData);
        resetChart();
    }

    /**
     * @return a new list of x_axis labels based on zoomed out labels except for the first values
     * this is done to avoid trimming issues
     */
    private List<String> getXAxisValues() {
        ArrayList<String> values = new ArrayList<>();
        for (int i = 0; i < xValuesZoomedOut.size(); i++) {
            values.add(i == 0 || i == xValuesZoomedOut.size() - 1 ? xValuesZoomedIn.get(i) : xValuesZoomedOut.get(i));
        }
        return values;
    }

    private PerformanceChartBuilder setXAxisValues(ReportKpiViewModel kpi) {
        if (kpi == null || kpi.getXAxisValues() == null) {
            throw new RuntimeException(TAG + " setXAxisValues received corrupted kpi");
        }

        xValuesZoomedOut = kpi.getXAxisValues();
        xValuesZoomedIn = new ArrayList<>();
        int count = xValuesZoomedOut.size();
        for (int i = 0; i < count; i++) {
            int countDiff = count - i - 1;

            xValuesZoomedIn.add(countDiff != 0 ? String.format(context.getString(R.string.weeks_earlier), countDiff + 1)
                    : context.getString(R.string.reports_last_week_x_axis).trim());
        }
        xValuesCount = xValuesZoomedOut.size();

        return this;
    }

    public void drawLegends(ReportKpiViewModel parentKpi) {
        List<ReportKpiViewModel> subKPIs = parentKpi.getSubKPIs();
        if (subKPIs == null) {
            parentKpi = parentKpi.getParentKpi();
            if (parentKpi == null) {
                return;
            }
            subKPIs = parentKpi.getSubKPIs();
            if (subKPIs == null) {
                return;
            }
        }

        legendLayout = (LinearLayout) chartView.findViewById(R.id.report_legend_view);
        legendLayout.removeAllViews();

        for (int index = subKPIs.size() - 1; index >= 0; index--) {
            ReportKpiViewModel kpi = subKPIs.get(index);
            View legendView = LayoutInflater.from(context).inflate(R.layout.reports_legend_item, legendLayout, false);
            ImageView legendImage = (ImageView) legendView.findViewById(R.id.ivLegendIcon);
            legendImage.setImageDrawable(kpi.getIconUp());
            legendView.setTag(kpi);
            legendView.setOnClickListener(this);
            legendView.setOnLongClickListener(this);
            legendLayout.addView(legendView);
        }
    }

    private void updateLegendIconsSelection(ReportKpiViewModel selectedKPI) {
        if (legendLayout != null) {
            for (int i = 0; i < legendLayout.getChildCount(); i++) {
                View legendItem = legendLayout.getChildAt(i);

                ImageView legendItemImage = (ImageView) legendItem.findViewById(R.id.ivLegendIcon);
                ReportKpiViewModel kpiModel = (ReportKpiViewModel) legendItem.getTag();

                if (legendItemImage != null && kpiModel != null) {
                    legendItemImage.setImageDrawable(kpiModel.getName().equals(selectedKPI.getName()) ? kpiModel.getIconDown() : kpiModel.getIconUp());
                }
            }
        }
    }

    private LineData getLineData(ReportKpiViewModel kpi) {
        List<ILineDataSet> dataSets = new ArrayList<>();

        if (kpi.getSubKPIs() == null || kpi.getSubKPIs().size() <= 0) {
            if (kpi.getReportPerPressList() != null && kpi.getReportPerPressList().size() > 0) {
                Map<String, ReportKpiViewModel> reportPerPressMap = kpi.getReportPerPressList();
                for (String pressReportName : reportPerPressMap.keySet()) {
                    ReportKpiViewModel pressReport = reportPerPressMap.get(pressReportName);
                    dataSets.add(buildLineDataSet(pressReport,
                            pressReport.getReportChartTypeEnum() == ReportChartTypeEnum.AREA ?
                                    pressReport.getAccumulativeValues() : pressReport.getOverallValues()));
                }
                drawPressLegend(kpi);
            } else {
                dataSets.add(buildLineDataSet(kpi, kpi.getKpiValues()));
            }

        } else if (!kpi.isAggregate()) {
            ReportKpiViewModel child = kpi.getSubKPIs().get(0);
            dataSets.add(buildLineDataSet(child, child.getOverallValues()));

        } else {
            for (ReportKpiViewModel subKpi : kpi.getSubKPIs()) {
                dataSets.add(buildLineDataSet(subKpi, subKpi.getAccumulativeValues()));
            }
        }

        //Reverse data-sets to achieve better coloring.
        Collections.reverse(dataSets);
        return new LineData(xValues, dataSets);
    }

    private void drawPressLegend(ReportKpiViewModel kpi) {

        pressLegendLayout = (RecyclerView) chartView.findViewById(R.id.press_legend);
        if (pressLegendAdapter == null) {
            pressLegendAdapter = new PressLegendAdapter(context, kpi.getReportPerPressList());
            GridLayoutManager gridLayoutManager = new GridLayoutManager(context, NUM_OFF_PRESS_PER_LINE);
            pressLegendLayout.setLayoutManager(gridLayoutManager);
            pressLegendLayout.setAdapter(pressLegendAdapter);
        }

        pressLegendAdapter.setPresses(getSubset(kpi.getReportPerPressList(),2* NUM_OFF_PRESS_PER_LINE));
        pressLegendLayout.setVisibility(pressLegendAdapter.getItemCount() > 0 ? View.VISIBLE : View.GONE);
    }

    private  Map<String, ReportKpiViewModel> getSubset (Map<String, ReportKpiViewModel> map, int subsetMaxLength){
        //TODO temporary fix
        Map<String, ReportKpiViewModel> modelMap = new HashMap<>();

        int maxCount = Math.min(subsetMaxLength, map.size());
        List<String> keys = new ArrayList<>(map.keySet());

        for(int i = 0; i < maxCount; i++){
            String key = keys.get(i);
            modelMap.put(key, map.get(key));
        }

        return modelMap;
    }

    private void clearPressLegend() {
        if (pressLegendAdapter != null) {
            pressLegendAdapter.clear();
        }

    }

    private LineDataSet buildLineDataSet(ReportKpiViewModel kpi, List<Entry> entries) {
        List<Entry> modifiedEntry = new ArrayList();
        int indexDiff = xValuesCount - entries.size();
        for(int i = 0; i < xValuesCount; i++){
            modifiedEntry.add(new Entry(i - indexDiff >= 0 ? entries.get(i - indexDiff).getVal() : 0, i));
        }

        LineDataSet set = new LineDataSet(modifiedEntry, kpi.getName());
        set.setAxisDependency(YAxis.AxisDependency.LEFT);
        set.setColor(kpi.getColor());
        set.setCircleColor(kpi.getCircleColor());
        set.setCircleColorHole(kpi.getCircleHoleColor());
        set.setLineWidth(kpi.getLineWidth());
        set.setCircleSize(kpi.getCircleSize());
        set.setValueTypeface(kpi.getValueTypeFace());
        set.setFillColor(kpi.getColor());
        set.setDrawFilled(kpi.isFillArea());
        set.setHighLightColor(kpi.getHighLightColor());
        set.setDrawCircleHole(kpi.isDrawCircleHole());
        set.setDrawCircles(kpi.isDrawCircle());
        set.setValueTextSize(kpi.getValueTextSize());
        set.setValueTextColor(kpi.getValueTextColor());
        set.setDrawValues(kpi.isDrawValues());
        set.setDrawHighlightIndicators(false);

        return set;
    }

    private BarData getBarData(ReportKpiViewModel kpi) {
        List<IBarDataSet> dataSets = new ArrayList<>();

        if (kpi.getSubKPIs() != null && kpi.getSubKPIs().size() > 0) {
            for (ReportKpiViewModel subKpi : kpi.getSubKPIs()) {
                dataSets.add(buildBarDataSet(subKpi, xValues));
            }
        } else if (kpi.getReportPerPressList() != null && kpi.getReportPerPressList().size() > 0) {
            for (String press : kpi.getReportPerPressList().keySet()) {
                dataSets.add(buildBarDataSet(kpi.getReportPerPressList().get(press), xValues));
            }
        } else {

            dataSets.add(buildBarDataSet(kpi, xValues));

        }

        return new BarData(xValues, dataSets);
    }

    private BarDataSet buildBarDataSet(ReportKpiViewModel kpi, List<String> xValues) {
        BarDataSet set = new BarDataSet(kpi.getBarEntries(), "");
        set.setDrawValues(false);
        set.setColors(getColorsForStackBarChart(xValues.size()));
        return set;
    }

    private List<Integer> getColorsForStackBarChart(int xValuesCount) {
        List<Integer> colors = new ArrayList<>();

        for (int i = 0; i < xValuesCount; i++) {
            colors.add(ResourcesCompat.getColor(context.getResources(), R.color.c6, null));
            colors.add(ResourcesCompat.getColor(context.getResources(), R.color.c7, null));
        }

        return colors;
    }

    /**
     * Reset the chart scale and position to its initial state.
     */
    public void resetChart() {
        if (reportChartType == ReportChartTypeEnum.BAR) {
            scaleBarChart();
        } else {
            scaleLineChart();
        }
        chart.invalidate();
    }

    private void scaleLineChart() {
        chart.setScaleXEnabled(chartConfiguration.isScaleXEnabled());
        chart.setScaleYEnabled(chartConfiguration.isScaleYEnabled());
        chart.getViewPortHandler().setMaximumScaleX((xValuesCount - 1) / CHART_SCALE_VALUE);
        if(resetLineChartScale){
            resetLineChartScale = false;
            chart.zoom((xValuesCount - 1) / CHART_SCALE_VALUE, CHART_SCALE_Y_VALUE, chart.getCenter().x, chart.getCenter().y);
            chart.moveViewToX(xValues.size() - CHART_SCALE_VALUE);
        }
        chart.setVisibleXRangeMaximum(MAX_ZOOM_OUT);
    }

    private void scaleBarChart() {
        //Resets all offsets, allows the chart to again calculate all offsets automatically.
        chart.resetViewPortOffsets();
        //Resets all zooming and dragging and makes the chart fit exactly it's bounds.
        chart.fitScreen();
        chart.setVisibleXRangeMaximum(MAX_ZOOM_OUT);
        chart.moveViewToX(xValuesCount - 1);
        chart.setScaleXEnabled(false);
        chart.setScaleYEnabled(false);
    }

    private void updatePeriod() {
        int highestVisibleWeekIndex = chart.getHighestVisibleXIndex();
        int lowestVisibleWeekIndex = chart.getLowestVisibleXIndex();

        float scale = chart.getScaleX();

        if(scale > 0) {
            if (highestVisibleWeekIndex == chart.getXValCount() - 1) {
                lowestVisibleWeekIndex = Math.max(0, highestVisibleWeekIndex - (int) (chart.getXValCount() / chart.getScaleX()));
            } else if (lowestVisibleWeekIndex == 0) {
                highestVisibleWeekIndex = Math.min(chart.getXValCount()-1, lowestVisibleWeekIndex + (int) (chart.getXValCount() / chart.getScaleX()));
            }
        }

        callbacks.onPeriodChanged(lowestVisibleWeekIndex, highestVisibleWeekIndex);
    }

    public void setReportTotalPoints(int value) {
        this.totalPoints = value;
    }

    private void updatePointsExpression() {
        TextView pointValueText = (TextView) chartView.findViewById(R.id.text_report_point_value);
        TextView pointMaxValueText = (TextView) chartView.findViewById(R.id.text_report_point_expression);

        pointValueText.setText(String.valueOf(selectedKpi.getWeekPoints()));
        pointMaxValueText.setText("/" + this.totalPoints);
    }

    /***********************************************************************************************
     * PerformanceChartBuilderInterface
     ***********************************************************************************************/
    @Override
    public void buildChart() {
        chart = (CombinedChart) chartView.findViewById(R.id.report_chart_view);

        chart.setBackgroundColor(chartConfiguration.getBackgroundColor());
        chart.setDescription(chartConfiguration.getDescription());
        chart.setNoDataTextDescription(chartConfiguration.getNoDataTextDescription());
        chart.setHighlightPerDragEnabled(chartConfiguration.isHighlightEnabled());
        chart.setTouchEnabled(chartConfiguration.isTouchEnabled());
        chart.setOnChartValueSelectedListener(this);
        // enable/disable scaling and dragging
        chart.setDragEnabled(chartConfiguration.isDragEnabled());
        chart.setScaleXEnabled(chartConfiguration.isScaleXEnabled());
        chart.setScaleYEnabled(chartConfiguration.isScaleYEnabled());
        chart.setHighlightPerDragEnabled(chartConfiguration.isHighlightPerDragEnabled());
        // If disabled, scaling can be done on x- and y-axis separately
        chart.setPinchZoom(chartConfiguration.isPinchZoomEnabled());
        // set an alternative background color
        chart.setHardwareAccelerationEnabled(true);
        chart.setDoubleTapToZoomEnabled(chartConfiguration.isDoubleTapToZoomEnabled());
        chart.getViewPortHandler().setMaximumScaleX((xValuesCount - 1) / CHART_SCALE_VALUE);
        chart.setDragDecelerationFrictionCoef(DRAG_DECELERATION_FRICTION_COEF);
        chart.setOnChartGestureListener(this);

        buildAxes();
    }

    @Override
    public void buildAxes() {
        //Since we need a custom legends design we have to disable the default one.
        Legend legend = chart.getLegend();
        legend.setEnabled(false);

        //Customize the bottom x-axis.
        final XAxis xAxis = chart.getXAxis();
        xAxis.setTextSize(chartConfiguration.getxAxisTextSize());
        xAxis.setTextColor(chartConfiguration.getxAxisTextColor());
        xAxis.setTypeface(chartConfiguration.getxAxisTypeFace());
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawAxisLine(true);
        xAxis.setAvoidFirstLastClipping(true);
        xAxis.setValueFormatter(new XAxisValueFormatter() {
            @Override
            public String getXValue(String original, int index, ViewPortHandler viewPortHandler) {
                //changing the way the formatter chooses the labels' values
                float scaleLimit = (chart.getXValCount() - 1) / (float) ((int) CHART_SCALE_VALUE + 1);
                float currentScale = chart.getScaleX();

                return (scaleLimit < currentScale) ? xValuesZoomedIn.get(index) : xValuesZoomedOut.get(index);
            }
        });
        //this is added to prevent x-axis ticks range while zooming out
        xAxis.setLabelsToSkip(0);

        //Customize the left-y-axis.
        YAxis leftAxis = chart.getAxisLeft();
        leftAxis.setTextSize(chartConfiguration.getyAxisTextSize());
        leftAxis.setTextColor(chartConfiguration.getyAxisTextColor());
        leftAxis.setTypeface(chartConfiguration.getyAxisTypeFace());
        leftAxis.setStartAtZero(false);
        leftAxis.setDrawGridLines(true);

        //Disable the right-y-axis.
        YAxis rightAxis = chart.getAxisRight();
        rightAxis.setEnabled(false);
    }

    /**
     * Animates the rendering of the chart on the x-axis with the specified
     * animation time.
     */
    @Override
    public void animateX() {
        chart.animateX(chartConfiguration.getAnimationDuration());
    }

    /**
     * Animates the rendering of the chart on the y-axis with the specified
     * animation time.
     */
    @Override
    public void animateY() {
        chart.animateY(chartConfiguration.getAnimationDuration());
    }

    /***********************************************************************************************
     * View.onClick
     ***********************************************************************************************/
    @Override
    public void onClick(View view) {
        if (view.getTag() != null && view.getTag() instanceof ReportKpiViewModel) {
            selectedKpi = (ReportKpiViewModel) view.getTag();

            updateLegendIconsSelection(selectedKpi);

            if (selectedKpi.getKpiValues() == null && !selectedKpi.hasSubKpis() && selectedKpi.isReportPerPressListEmpty()) {
                callbacks.onKpiSelected(selectedKpi);
            } else {
                displayKpi(selectedKpi);
            }
        }
    }

    /***********************************************************************************************
     * OnChartValueSelectedListener
     ***********************************************************************************************/
    @Override
    public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {
        if (dataSetIndex == 0) {
            if (reportChartType != ReportChartTypeEnum.BAR && selectedKpi.isAggregate()) {
                //Note that: The last line is represented by the data-set of index 0.
                onLineChartLastLineClicked();
            }
        }
    }

    /**
     * Draw y-values on the chart or hide them when the line clicked again.
     */
    private void onLineChartLastLineClicked() {
        //The last line is represented by the dataset #0.
        ILineDataSet lineDataSet = chart.getLineData().getDataSetByIndex(0);
        lineDataSet.setDrawValues(!lineDataSet.isDrawValuesEnabled());

        Analytics.sendEvent(Analytics.MOBILE_PRINT_BEAT_CATEGORY, Analytics.REPORT_ACTION,
                lineDataSet.isDrawValuesEnabled() ? Analytics.REPORT_SHOW_VALUES_LABEL : Analytics.REPORT_HIDE_VALUES_LABEL);

        if (lineDataSet instanceof LineDataSet) {
            ((LineDataSet) lineDataSet).setDrawCircles(!lineDataSet.isDrawCirclesEnabled());
        }
        chart.invalidate();
    }

    @Override
    public void onNothingSelected() {
    }

    /***********************************************************************************************
     * View.OnLongClickListener
     ***********************************************************************************************/
    @Override
    public boolean onLongClick(View view) {
        if (view.getTag() != null && view.getTag() instanceof ReportKpiViewModel) {
            ReportKpiViewModel model = (ReportKpiViewModel) view.getTag();
            callbacks.onLegendViewLongClicked(model);
        }
        return false;
    }

    /***********************************************************************************************
     * OnChartGestureListener
     ***********************************************************************************************/
    @Override
    public void onChartGestureStart(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {
    }

    @Override
    public void onChartGestureEnd(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {
    }

    @Override
    public void onChartLongPressed(MotionEvent me) {
    }

    @Override
    public void onChartDoubleTapped(MotionEvent me) {
    }

    @Override
    public void onChartSingleTapped(MotionEvent me) {
    }

    @Override
    public void onChartFling(MotionEvent me1, MotionEvent me2, float velocityX, float velocityY) {
    }

    @Override
    public void onChartScale(MotionEvent me, float scaleX, float scaleY) {
        //This is done to avoid trimming issues
        //Library fixes the bounds of the x_axis labels' TextViews
        //once the chart.invalidate() is called neglecting ValueFormatter's
        //returned string length. This results in skewing and trimming
        //first label

        float currentChartScale = scaleX;
        if(currentChartScale != 1){
            if(prevChartScale <= 1 && currentChartScale > 1){
                Analytics.sendEvent(Analytics.MOBILE_PRINT_BEAT_CATEGORY, Analytics.REPORT_ACTION, Analytics.REPORT_ZOOM_IN_LABEL);
            } else if (prevChartScale >= 1 && currentChartScale < 1){
                Analytics.sendEvent(Analytics.MOBILE_PRINT_BEAT_CATEGORY, Analytics.REPORT_ACTION, Analytics.REPORT_ZOOM_OUT_LABEL);
            }
        }
        prevChartScale = currentChartScale;

        float scaleBoundary = (xValuesCount - 1) / (float) ((int) CHART_SCALE_VALUE + 1);
        chart.getXAxis().getValues().set(0, scaleBoundary < chart.getScaleX() ? xValuesZoomedIn.get(0) : xValuesZoomedOut.get(0));
        chart.invalidate();
        updatePeriod();
    }

    @Override
    public void onChartTranslate(MotionEvent me, float dX, float dY) {
        updatePeriod();
    }
}
