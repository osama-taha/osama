
package com.hp.printosmobile.data.remote.models;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.Map;

public class BoxModel {

    @JsonProperty("filesToday")
    private Integer filesToday;
    @JsonProperty("foldersToday")
    private Integer foldersToday;
    @JsonProperty("foldersUnread")
    private Integer foldersUnread;
    @JsonProperty("foldersAlert")
    private Integer foldersAlert;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * @return The filesToday
     */
    @JsonProperty("filesToday")
    public Integer getFilesToday() {
        return filesToday;
    }

    /**
     * @param filesToday The filesToday
     */
    @JsonProperty("filesToday")
    public void setFilesToday(Integer filesToday) {
        this.filesToday = filesToday;
    }

    /**
     * @return The foldersToday
     */
    @JsonProperty("foldersToday")
    public Integer getFoldersToday() {
        return foldersToday;
    }

    /**
     * @param foldersToday The foldersToday
     */
    @JsonProperty("foldersToday")
    public void setFoldersToday(Integer foldersToday) {
        this.foldersToday = foldersToday;
    }

    /**
     * @return The foldersUnread
     */
    @JsonProperty("foldersUnread")
    public Integer getFoldersUnread() {
        return foldersUnread;
    }

    /**
     * @param foldersUnread The foldersUnread
     */
    @JsonProperty("foldersUnread")
    public void setFoldersUnread(Integer foldersUnread) {
        this.foldersUnread = foldersUnread;
    }

    /**
     * @return The foldersAlert
     */
    @JsonProperty("foldersAlert")
    public Integer getFoldersAlert() {
        return foldersAlert;
    }

    /**
     * @param foldersAlert The foldersAlert
     */
    @JsonProperty("foldersAlert")
    public void setFoldersAlert(Integer foldersAlert) {
        this.foldersAlert = foldersAlert;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
