package com.hp.printosmobile.presentation.modules.next10jobs;

import android.os.Bundle;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.modules.shared.DeviceViewModel;
import com.hp.printosmobilelib.ui.common.HPFragment;
import com.hp.printosmobilelib.ui.utils.SpaceItemDecoration;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * create by anwar asbah 5/16/2017
 */
public class JobsListFragment extends HPFragment {

    private static final String ARG_IS_COMPLETED = "param2";

    @Bind(R.id.fragment_layout)
    View fragmentLayout;
    @Bind(R.id.jobs_list)
    RecyclerView jobsList;
    @Bind(R.id.error_msg_text_view)
    TextView errorMsgTextView;

    List<DeviceViewModel> deviceViewModels;
    DeviceViewModel.JobType jobType;
    JobsAdapter jobsAdapter;
    JobsListFragmentCallback callback;


    public static JobsListFragment newInstance(DeviceViewModel.JobType jobType) {
        JobsListFragment fragment = new JobsListFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_IS_COMPLETED, jobType);
        fragment.setArguments(args);
        return fragment;
    }

    public static JobsListFragment newInstance(DeviceViewModel.JobType jobType, JobsListFragmentCallback callback) {
        JobsListFragment fragment = newInstance(jobType);
        fragment.callback = callback;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey(ARG_IS_COMPLETED)) {
            jobType = (DeviceViewModel.JobType) bundle.getSerializable(ARG_IS_COMPLETED);
        } else {
            jobType = DeviceViewModel.JobType.QUEUED;
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();

        Analytics.sendEvent(Analytics.MOBILE_PRINT_BEAT_CATEGORY, Analytics.VIEW_NEXT_10_JOBS_ACTION);
    }

    @Override
    public boolean isIntercomAccessible() {
        return true;
    }

    private void init() {
        jobsList.addItemDecoration(new SpaceItemDecoration((int) PrintOSApplication.getAppContext()
                .getResources().getDimension(R.dimen.next_10_jobs_middle_separator_width)));
    }

    public void setViewModels(List<DeviceViewModel> model) {
        deviceViewModels = model;
        displayModels();
    }

    private void displayModels() {

        if(getActivity() == null || !isAdded()){
            return;
        }

        if (deviceViewModels == null) {
            errorMsgTextView.setText(getString(R.string.jobs_no_devices_msg));
            errorMsgTextView.setVisibility(View.VISIBLE);
            jobsList.setVisibility(View.GONE);
            return;
        }

        if (!hasJobs()) {
            errorMsgTextView.setText(getString(R.string.jobs_no_info_msg));
            errorMsgTextView.setVisibility(View.VISIBLE);
            jobsList.setVisibility(View.GONE);
            return;
        }

        jobsList.setVisibility(View.VISIBLE);
        errorMsgTextView.setVisibility(View.GONE);

        jobsList.removeAllViews();
        final LinearLayoutManager manager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        jobsList.setLayoutManager(manager);
        jobsAdapter = new JobsAdapter(PrintOSApplication.getAppContext(), deviceViewModels, jobType,
                new JobsAdapter.JobsInQueueAdapterCallback() {
                    @Override
                    public void onJobExpanded(int viewPosition) {
                        manager.scrollToPositionWithOffset(viewPosition, 0);
                    }
                });
        jobsList.setAdapter(jobsAdapter);

        int horizontalPadding = jobsAdapter.getItemCount() == 1 ? 0
                : (int) getActivity().getResources().getDimension(R.dimen.next_10_jobs_fragment_horizontal_padding);
        fragmentLayout.setPadding(horizontalPadding, fragmentLayout.getPaddingTop(),
                horizontalPadding, fragmentLayout.getPaddingBottom());

        fragmentLayout.setBackgroundColor(ResourcesCompat.getColor(getResources(),
                jobsAdapter.getItemCount() == 1 ? android.R.color.white : R.color.c80d, null));

        int expandingIndex = jobsAdapter.getFirstExpandableItemIndex();
        if (expandingIndex >= 0) {
            manager.scrollToPositionWithOffset(expandingIndex, 0);
        }
    }

    public void onError () {
        if(getActivity() == null || !isAdded()){
            return;
        }

        errorMsgTextView.setText(getString(R.string.error_no_data_msg));
        errorMsgTextView.setVisibility(View.VISIBLE);
        jobsList.setVisibility(View.GONE);
    }

    private boolean hasJobs() {
        if (deviceViewModels == null) {
            return false;
        }

        for (DeviceViewModel model : deviceViewModels) {
            List<DeviceViewModel.Job> jobs = model.getJobs(jobType);
            if (jobs != null && jobs.size() > 0) {
                return true;
            }
        }

        return false;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_next_10_jobs;
    }

    @Override
    public String getToolbarDisplayNameExtra() {
        return "";
    }

    @Override
    public boolean isFragmentNameToBeDisplayed() {
        return true;
    }

    @Override
    public boolean isFilteringAvailable() {
        return false;
    }

    @Override
    public int getToolbarDisplayName() {
        return R.string.next_10_jobs_title;
    }

    @Override
    public boolean onBackPressed() {
        return true;
    }

    public interface JobsListFragmentCallback {
        void onTryAgainClicked();
    }
}