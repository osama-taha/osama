package com.hp.printosmobile.presentation.modules.contacthp;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.data.remote.ApiServicesProvider;
import com.hp.printosmobile.data.remote.models.ContactHpCase;
import com.hp.printosmobile.data.remote.models.ContactHpCase.ContactHpPreferredContactMethod.ContactMethodType;
import com.hp.printosmobile.data.remote.services.ContactHpService;
import com.hp.printosmobilelib.core.communications.remote.models.UserData;
import com.hp.printosmobilelib.core.logging.HPLogger;
import com.hp.printosmobilelib.core.utils.HPDateUtils;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Response;
import rx.Observable;
import rx.functions.Func1;

/**
 * Created by anwar asbah on 10/12/2016.
 */
public class ContactHpManager {

    public static final String TAG = ContactHpManager.class.getSimpleName();

    private static final String CONTACT_HP_API_BROWSER = "Android Mobile app";
    private static final String MIME_APPLICATION = "application/json";
    private static final String MIME_IMAGE = "image/jpg";
    private static final String MIME_TEXT = "text/plain";

    private static final String REQUEST_BODY_NAME = "case";
    private static final String IMAGE_FILE_ATTACHMENT_NAME = "attachment0%d";

    private static final String DATE_FORMAT = "yyyy-MM-dd";

    public static Observable<Boolean> hasPhone() {
        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
        ContactHpService contactHpService = serviceProvider.getContactHpService();

        return contactHpService.getUser().map(new Func1<Response<UserData.User>, Boolean>() {

            @Override
            public Boolean call(Response<UserData.User> userResponse) {
                if (userResponse.isSuccessful()) {
                    if (userResponse.body() != null && userResponse.body().getPrimaryPhone() != null) {
                        return true;
                    }
                }
                return false;
            }
        });
    }

    public static Observable<ResponseBody> sendRequest(Context context, ContactHpCase.CaseType caseType, String subject,
                                                       List<ContactHpCase.ContactHpUserEntry> entries, List<Uri> attachments,
                                                       ContactMethodType contactMethodType) {

        PrintOSPreferences preferences = PrintOSPreferences.getInstance(context);
        ContactHpCase body = new ContactHpCase();
        body.setCaseSource(ContactHpCase.CaseSource.MOBILE);
        body.setAttachments(null);
        body.setUserId(preferences.getUserInfo().getUserId());
        body.setOrganizationId(preferences.getSavedOrganizationId());
        body.setCreatedAt(HPDateUtils.formatDate(new Date(), DATE_FORMAT));
        body.setCareforceState(new ContactHpCase.ContactHpCareForceState());
        body.setType(caseType);
        body.setSubject(subject);
        body.setUserEntries(entries);

        ContactHpCase.ContactHpUserSystemInfo userSystemInfo = new ContactHpCase.ContactHpUserSystemInfo();
        userSystemInfo.setBrowser(CONTACT_HP_API_BROWSER);
        body.setUserSystemInfo(userSystemInfo);

        ContactHpCase.ContactHpPreferredContactMethod method = new ContactHpCase.ContactHpPreferredContactMethod();
        method.setContactMethodType(contactMethodType);
        body.setPreferredContactMethod(method);

        return submitRequest(context, body, attachments);
    }

    private static Observable<ResponseBody> submitRequest(Context context, ContactHpCase data, List<Uri> attachments) {

        Map<String, RequestBody> bodyMap = new HashMap<>();

        ObjectMapper mapper = new ObjectMapper();
        String jsonAsString = null;
        try {
            jsonAsString = mapper.writeValueAsString(data);
        } catch (JsonProcessingException e) {
            HPLogger.d(TAG, e.toString());
        }
        final RequestBody thread = RequestBody.create(MediaType.parse(MIME_APPLICATION), jsonAsString);
        bodyMap.put(REQUEST_BODY_NAME, thread);

        int attachmentIndex = 1;
        HPLogger.enableFileLogging(false);
        if (attachments != null) {
            for (int i = 0; i < attachments.size(); i++) {
                Uri uri = attachments.get(i);
                bodyMap.put(String.format(IMAGE_FILE_ATTACHMENT_NAME, i + 1), RequestBody.create(
                        MediaType.parse(MIME_IMAGE), new File(getPath(context, uri))));
                attachmentIndex += 1;
            }
        }

        File logsFile = new File(HPLogger.getInstance(context).getFilePath());
        if (logsFile != null && logsFile.exists()) {
            bodyMap.put(String.format(IMAGE_FILE_ATTACHMENT_NAME, attachmentIndex), RequestBody.create(
                    MediaType.parse(MIME_TEXT), logsFile));
        }

        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
        ContactHpService contactHpService = serviceProvider.getContactHpService();

        return contactHpService.contactHp(bodyMap).map(new Func1<Response<ResponseBody>, ResponseBody>() {
            @Override
            public ResponseBody call(Response<ResponseBody> responseBodyResponse) {
                HPLogger.enableFileLogging(true);
                if (responseBodyResponse != null && responseBodyResponse.isSuccessful()) {
                    return responseBodyResponse.body();
                }
                return null;
            }
        });
    }

    public static String getPath(Context context, Uri uri) {
        if (context == null || uri == null) {
            return "";
        }
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = context.getContentResolver().query(uri, projection, null, null, null);
        if (cursor == null) return null;
        int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String s = cursor.getString(columnIndex);
        cursor.close();
        return s;
    }
}
