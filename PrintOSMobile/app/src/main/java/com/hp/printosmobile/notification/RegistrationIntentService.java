package com.hp.printosmobile.notification;

import android.app.IntentService;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import com.google.firebase.iid.FirebaseInstanceId;
import com.hp.printosmobile.Constants;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.data.remote.ApiServicesProvider;
import com.hp.printosmobile.data.remote.services.NotificationService;
import com.hp.printosmobilelib.core.communications.remote.SessionManager;
import com.hp.printosmobilelib.core.logging.HPLogger;

import java.io.IOException;

/**
 * An intent service to called in order to get the device token for gcm service and persist registration to the server.
 *
 * @author Osama Taha
 */
public class RegistrationIntentService extends IntentService {

    private static final String TAG = RegistrationIntentService.class.getSimpleName();

    public RegistrationIntentService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        if (intent == null){
            return;
        }

        String action = intent.getAction();
        String receiverExtra;

        if (action == null) {
            return;
        } else if (action.equals(Constants.IntentExtras.NOTIFICATION_REGISTER_INTENT_ACTION)) {

            if (PrintOSApplication.isGooglePlayServicesEnabled()) {
                register();
            }

            receiverExtra = Constants.IntentExtras.NOTIFICATION_REGISTRATION_COMPLETED_INTENT_ACTION;

        } else if (action.equals(Constants.IntentExtras.NOTIFICATION_UNREGISTER_INTENT_ACTION)) {

            if (PrintOSApplication.isGooglePlayServicesEnabled()) {
                unregister();
            }

            receiverExtra = Constants.IntentExtras.NOTIFICATION_UNREGISTER_COMPLETED_INTENT_ACTION;

        } else if (action.equals(Constants.IntentExtras.NOTIFICATION_UNREGISTER_AND_CLEAR_CACHE_INTENT_ACTION)) {

            if (PrintOSApplication.isGooglePlayServicesEnabled()) {
                unregister();
            }

            SessionManager sessionManager = SessionManager.getInstance(PrintOSApplication.getAppContext());
            sessionManager.clearCookie();
            sessionManager.clearUserCredentials();

            receiverExtra = Constants.IntentExtras.NOTIFICATION_UNREGISTER_COMPLETED_INTENT_ACTION;

            cancelAllNotifications();

        } else {
            return;
        }

        // Notify UI upon completion.
        Intent registrationCompleteIntent = new Intent(receiverExtra);
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationCompleteIntent);

    }

    public void cancelAllNotifications() {

        HPLogger.d(TAG, "Removing all app notifications from status bar..");

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();
    }


    private void register() {

        PrintOSPreferences printOSPreferences = PrintOSPreferences.getInstance(this);

        try {

            String token = FirebaseInstanceId.getInstance().getToken();
            HPLogger.i(TAG, "Notification Registration Token: " + token);

            sendRegistrationToServer(token);

            // Store a boolean that indicates whether the generated token has been
            // sent to your server. If the boolean is false, send the token to your server,
            // otherwise your server should have already received the token.
            printOSPreferences.setSentNotificationTokenToServer(true);
            printOSPreferences.setNotificationToken(token);

        } catch (Exception e) {

            HPLogger.e(TAG, "Failed to complete token refresh", e);
            // If an exception happens while fetching the new token or updating our registration data
            // on the server, this ensures that we'll attempt the update at a later time.
            printOSPreferences.setSentNotificationTokenToServer(false);
            printOSPreferences.setNotificationToken("");
        }

    }

    private void unregister() {

        PrintOSPreferences printOSPreferences = PrintOSPreferences.getInstance(this);

        try {
            String token = printOSPreferences.getNotificationToken();

            HPLogger.i(TAG, "Notification UnRegistration Token: " + token);

            removeRegistrationFromServer(token);
        } catch (Exception e) {
            HPLogger.e(TAG, "Failed to complete Token UnRegistration", e);
        }

        printOSPreferences.setSentNotificationTokenToServer(false);
        printOSPreferences.setNotificationToken("");
    }

    private void removeRegistrationFromServer(String token) throws IOException {

        ApiServicesProvider servicesProvider = PrintOSApplication.getApiServicesProvider();
        NotificationService notificationService = servicesProvider.getNotificationService();
        int responseCode = notificationService.unRegisterDeviceForPushNotification(token, NotificationService.APP_NAME_PARAMETER_VALUE).execute().code();

        HPLogger.d(TAG, "Response code for Notification un registration " + responseCode);
    }

    /**
     * Persist registration to the server.
     * <p>
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(String token) throws IOException {

        SessionManager sessionManager = SessionManager.getInstance(PrintOSApplication.getAppContext());
        if (!sessionManager.hasCookie()) {
            return;
        }

        ApiServicesProvider servicesProvider = PrintOSApplication.getApiServicesProvider();
        NotificationService notificationService = servicesProvider.getNotificationService();

        String language = PrintOSPreferences.getInstance(this).getLanguage(getString(R.string.default_language));

        int responseCode = notificationService.registerDeviceForPushNotification(token, language, NotificationService.APP_NAME_PARAMETER_VALUE).execute().code();

        HPLogger.d(TAG, "Response code for Notification registration " + responseCode);
    }

}