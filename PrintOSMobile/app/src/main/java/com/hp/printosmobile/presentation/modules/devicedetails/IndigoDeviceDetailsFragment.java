package com.hp.printosmobile.presentation.modules.devicedetails;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.AppseeSdk;
import com.hp.printosmobile.IntercomSdk;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.data.remote.models.PreferencesData;
import com.hp.printosmobile.presentation.BaseFragment;
import com.hp.printosmobile.presentation.modules.home.Panel;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;
import com.hp.printosmobile.presentation.modules.next10jobs.JobsTodayFragment;
import com.hp.printosmobile.presentation.modules.personaladvisor.AdviceViewModel;
import com.hp.printosmobile.presentation.modules.personaladvisor.PersonalAdvisorDialog;
import com.hp.printosmobile.presentation.modules.personaladvisor.presenter.PersonalAdvisorFactory;
import com.hp.printosmobile.presentation.modules.shared.DeviceViewModel;
import com.hp.printosmobile.presentation.modules.shared.DevicesUtils;
import com.hp.printosmobile.presentation.modules.shared.MaintenanceState;
import com.hp.printosmobile.presentation.modules.tooltip.MaintenanceFanTooltip;
import com.hp.printosmobile.utils.HPLocaleUtils;
import com.hp.printosmobile.utils.HPScoreUtils;
import com.hp.printosmobile.utils.HPUIUtils;
import com.hp.printosmobilelib.core.communications.remote.APIException;
import com.hp.printosmobilelib.core.logging.HPLogger;
import com.hp.printosmobilelib.core.utils.HPDateUtils;
import com.hp.printosmobilelib.ui.common.HPFragment;
import com.hp.printosmobilelib.ui.widgets.HPProgressBar;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * create by anwar asbah 10/8/2016
 */
public class IndigoDeviceDetailsFragment extends BaseFragment implements JobsTodayFragment.JobsTodayFragmentCallback,
        PersonalAdvisorDialog.PersonalAdvisorDialogCallback, PersonalAdvisorFactory.PersonalAdvisorObserver {

    private static final String TAG = IndigoDeviceDetailsFragment.class.getSimpleName();
    private static final String ARG_VIEW_MODEL = "param1";

    private static final float BAR_CORNER_RADIUS = 0;
    private static final float TARGET_INDICATOR_RADIUS = 3;

    @Bind(R.id.press_name_text_view)
    TextView pressNameTextView;
    @Bind(R.id.updated_time_text_view)
    TextView updateTimeTextView;
    @Bind(R.id.press_image_view)
    ImageView pressImageView;
    @Bind(R.id.maintenance_state_image_view)
    ImageView maintenanceStateImageView;
    @Bind(R.id.progress_bar)
    HPProgressBar progressBar;
    @Bind(R.id.impression_nominator_text_view)
    TextView nominatorImpressionTextView;
    @Bind(R.id.impression_denominator_text_view)
    TextView denominatorImpressionTextView;
    @Bind(R.id.time_in_state_text_view)
    TextView timeInStateTextView;
    @Bind(R.id.press_state_text_view)
    TextView pressStateTextView;
    @Bind(R.id.current_job_container_layout)
    View currentJobLayout;
    @Bind(R.id.current_job_layout_separator_view)
    View currentJobLayoutSeparatorView;
    @Bind(R.id.current_job_name_text_view)
    TextView currentJobNameTextView;
    @Bind(R.id.estimated_time_value_text_view)
    TextView estimatedTimeValueTextView;
    @Bind(R.id.print_queue_view)
    View printQueueView;
    @Bind(R.id.print_queue_jobs_in_queue_text_view)
    TextView jobsInQueueValueTextView;
    @Bind(R.id.time_container)
    View jobsInQueueTimeContainer;
    @Bind(R.id.next_10_jobs_indicator)
    View next10JobsIndicator;
    @Bind(R.id.jobs_in_queue_time_text_view)
    TextView jobsInQueueTimeTextView;
    @Bind(R.id.completed_view)
    View completedJobsView;
    @Bind(R.id.completed_jobs_value_text_view)
    TextView completedJobsValueTextView;
    @Bind(R.id.completed_jobs_indicator)
    View completedJobsIndicator;
    @Bind(R.id.printed_value_text_view)
    TextView printedValueTextView;
    @Bind(R.id.has_advice_icon)
    View hasAdviceIcon;
    @Bind(R.id.printer_serial_number_value_text_view)
    TextView printerSerialNumberTextView;

    private DeviceViewModel indigoDeviceViewModel;
    private JobsTodayFragment nextTenJobsFragment;
    private IndigoDeviceDetailsFragmentCallbacks listener;

    public static IndigoDeviceDetailsFragment newInstance(DeviceViewModel indigoDeviceViewModel) {
        IndigoDeviceDetailsFragment fragment = new IndigoDeviceDetailsFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_VIEW_MODEL, indigoDeviceViewModel);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null) {
            indigoDeviceViewModel = (DeviceViewModel) bundle.getSerializable(ARG_VIEW_MODEL);
        }

        PersonalAdvisorFactory.getInstance().addObserver(this);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        displayModel();

        IntercomSdk.getInstance(PrintOSApplication.getAppContext()).logEvent(
                IntercomSdk.PBM_VIEW_DEVICELIST_EVENT, BusinessUnitEnum.INDIGO_PRESS);
        Analytics.sendEvent(Analytics.MOBILE_PRINT_BEAT_CATEGORY, Analytics.VIEW_DEVICE_DETAILS_ACTION, indigoDeviceViewModel.getName());
        AppseeSdk.getInstance(getActivity()).startScreen(AppseeSdk.SCREEN_INDIGO_DEVICE_DETAILS);

    }

    private void onJobClicked(DeviceViewModel.JobType jobType) {
        FragmentTransaction trans = getFragmentManager()
                .beginTransaction();

        nextTenJobsFragment = JobsTodayFragment.newInstance(jobType);
        nextTenJobsFragment.attachListener(this);

        trans.replace(R.id.device_details_root_frame, nextTenJobsFragment);
        trans.commit();
        AppseeSdk.getInstance(getActivity()).startScreen(AppseeSdk.JOBS);

    }

    private void displayModel() {

        pressNameTextView.setText(indigoDeviceViewModel.getName());
        updateTimeTextView.setText(String.format(getString(R.string.device_detail_last_update), HPDateUtils.getCurrentTime(getContext())));

        Picasso.with(getActivity()).load(indigoDeviceViewModel.getDeviceImageResource()).into(pressImageView);

        if (indigoDeviceViewModel.getMaintenanceState() != MaintenanceState.EMPTY) {
            Picasso.with(getActivity()).load(DevicesUtils.getDeviceDetailsMaintenanceImage(indigoDeviceViewModel.getMaintenanceState())).into(maintenanceStateImageView);
        }

        double shiftTarget = indigoDeviceViewModel.getPrintVolumeShiftTargetValue();
        double dailyTarget = indigoDeviceViewModel.getPrintVolumeIntraDailyTargetValue();
        DevicesUtils.updateProgressBar(getActivity(), progressBar,
                shiftTarget,
                dailyTarget,
                indigoDeviceViewModel.getPrintVolumeValue(),
                TARGET_INDICATOR_RADIUS,
                BAR_CORNER_RADIUS,
                false);

        nominatorImpressionTextView.setText(HPLocaleUtils.getLocalizedValue((int) indigoDeviceViewModel.getPrintVolumeValue()));
        nominatorImpressionTextView.setTextColor(HPScoreUtils.getProgressColor(getActivity(), indigoDeviceViewModel.getPrintVolumeValue(), dailyTarget, false));
        denominatorImpressionTextView.setText(String.format(getString(R.string.press_progress_impression_v2), HPLocaleUtils.getLocalizedValue((int) shiftTarget)));

        DevicesUtils.updateTimeInState(getActivity(), indigoDeviceViewModel, BusinessUnitEnum.INDIGO_PRESS, timeInStateTextView, pressStateTextView);

        String estimatedTime = getString(R.string.unknown_value);
        String currentJob = indigoDeviceViewModel.getCurrentJob();
        if (indigoDeviceViewModel.getDurationToDoneUnit() != null && indigoDeviceViewModel.getDurationToDone() >= 0) {
            estimatedTime = HPLocaleUtils.getLocalizedTimeString(getActivity(), indigoDeviceViewModel.getDurationToDone(), indigoDeviceViewModel.getDurationToDoneUnit());
        }
        if (currentJob == null || currentJob.isEmpty()) {
            currentJob = getString(R.string.unknown_value);
            estimatedTime = getString(R.string.unknown_value);
            HPUIUtils.setVisibility(false, currentJobLayout, currentJobLayoutSeparatorView);

        } else {
            HPUIUtils.setVisibility(true, currentJobLayout, currentJobLayoutSeparatorView);
        }

        currentJobNameTextView.setText(currentJob);
        estimatedTimeValueTextView.setText(estimatedTime);

        int completedJobs = Math.max(0, indigoDeviceViewModel.getPrintedJobs());
        completedJobsValueTextView.setText(completedJobs == 1 ? getString(R.string.device_detail_completed_jobs_value_one_job) :
                String.format(getString(R.string.device_detail_completed_jobs_value_jobs), HPLocaleUtils.getLocalizedValue(completedJobs)));

        PreferencesData.UnitSystem unitSystem = PrintOSPreferences.getInstance(getContext()).getUnitSystem();
        printedValueTextView.setText(DevicesUtils.getPrinted(getActivity(), indigoDeviceViewModel, unitSystem));

        List<DeviceViewModel.Job> jobs = indigoDeviceViewModel.getJobs(DeviceViewModel.JobType.QUEUED);
        final boolean hasQueuedJobs = jobs != null && jobs.size() > 0;
        printQueueView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (hasQueuedJobs) {
                    onJobClicked(DeviceViewModel.JobType.QUEUED);
                }
            }
        });
        next10JobsIndicator.setVisibility(hasQueuedJobs ? View.VISIBLE : View.GONE);
        DevicesUtils.updateJobsInQueue(getActivity(), indigoDeviceViewModel, hasQueuedJobs, jobsInQueueValueTextView, jobsInQueueTimeTextView, jobsInQueueTimeContainer);


        jobs = indigoDeviceViewModel.getJobs(DeviceViewModel.JobType.COMPLETED);
        final boolean hasCompletedJobs = PrintOSPreferences.getInstance(getActivity())
                .isCompletedJobsFeatureEnabled() && jobs != null && jobs.size() > 0;
        completedJobsView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (hasCompletedJobs) {
                    onJobClicked(DeviceViewModel.JobType.COMPLETED);
                }
            }
        });
        completedJobsIndicator.setVisibility(hasCompletedJobs ? View.VISIBLE : View.GONE);

        hasAdviceIcon.setVisibility(indigoDeviceViewModel.hasUnsuppressedAdvices() ? View.VISIBLE : View.GONE);
        hasAdviceIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openAdvicesDialog();
            }
        });

        printerSerialNumberTextView.setText(indigoDeviceViewModel.getSerialNumberDisplay() == null ?
                getString(R.string.unknown_value) : indigoDeviceViewModel.getSerialNumberDisplay());
    }

    private void openAdvicesDialog() {
        PersonalAdvisorDialog dialog = PersonalAdvisorDialog.newInstance(indigoDeviceViewModel.getPersonalAdvisorViewModel(), this);
        dialog.show(getFragmentManager(), "advices");
    }

    public void attachListener(IndigoDeviceDetailsFragmentCallbacks listener) {
        this.listener = listener;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_indigo_device_details;
    }

    @Override
    public boolean isFragmentNameToBeDisplayed() {
        return true;
    }

    @Override
    public boolean isFilteringAvailable() {
        return false;
    }

    @Override
    public int getToolbarDisplayName() {
        return nextTenJobsFragment == null ? R.string.device_detail : nextTenJobsFragment.getToolbarDisplayName();
    }

    @Override
    public boolean onBackPressed() {
        if (nextTenJobsFragment != null) {
            FragmentTransaction trans = getFragmentManager()
                    .beginTransaction();
            trans.remove(nextTenJobsFragment);
            trans.commit();
            nextTenJobsFragment = null;

            if (listener != null) {
                listener.onNext10JobsClosed(this);
            }

            AppseeSdk.getInstance(getActivity()).startScreen(AppseeSdk.SCREEN_INDIGO_DEVICE_DETAILS);

            return true;
        }
        return false;
    }

    @Override
    public void onInitializeJobsView(JobsTodayFragment fragment) {
        if (listener != null) {
            listener.onNext10JobsClicked(nextTenJobsFragment);
        }
        ArrayList<DeviceViewModel> models = new ArrayList<>();
        models.add(indigoDeviceViewModel);
        fragment.setViewModels(models);
    }

    @Override
    public void onJobsTodayBackPressed() {
        //Nothing
    }

    @Override
    public void onTryAgainClicked() {
        //Nothing
    }

    @Override
    public void moveToHomeFromTab(String string) {
        //Nothing
    }

    @OnClick(R.id.maintenance_state_image_view)
    public void onMaintenanceIconClicked() {

        MaintenanceFanTooltip maintenanceFanTooltip = new MaintenanceFanTooltip(PrintOSApplication.getAppContext());
        maintenanceFanTooltip.updateView(indigoDeviceViewModel.getMaintenanceState());

        int[] locations = new int[2];
        maintenanceStateImageView.getLocationOnScreen(locations);

        int tooltipRightCorner = locations[0] + maintenanceStateImageView.getWidth() / 2;
        int tooltipTopCorner = locations[1] + maintenanceStateImageView.getHeight();

        maintenanceFanTooltip.showToolTip(maintenanceStateImageView, tooltipRightCorner, tooltipTopCorner, Gravity.LEFT);
    }

    @Override
    public void goToPersonalAdvisor(AdviceViewModel adviceViewModel) {
        if (listener != null) {
            listener.onGoToPersonalAdvisorClicked(adviceViewModel);
        }
    }

    @Override
    public void onAdvicePopupDismiss() {

    }

    @Override
    public void onScreenshotCreated(Panel personalAdvisor, Uri storageUri) {
        shareImage(storageUri);
    }

    @Override
    public boolean isIntercomAccessible() {
        return true;
    }

    @Override
    public void updatePersonalAdvisorObserver() {
        if (indigoDeviceViewModel != null && hasAdviceIcon != null) {
            HPLogger.d(TAG, "updating Personal Advisor Observer");
            hasAdviceIcon.setVisibility(indigoDeviceViewModel.hasUnsuppressedAdvices() ? View.VISIBLE : View.GONE);
        }
    }

    @Override
    public void onPersonalAdvisorError(APIException e, String tag) {

    }

    @Override
    public void onDestroy() {
        PersonalAdvisorFactory.getInstance().addObserver(this);
        super.onDestroy();
    }

    public interface IndigoDeviceDetailsFragmentCallbacks {
        void onNext10JobsClosed(HPFragment fragment);

        void onNext10JobsClicked(HPFragment fragment);

        void onGoToPersonalAdvisorClicked(AdviceViewModel model);
    }
}
