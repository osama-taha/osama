package com.hp.printosmobile.presentation.modules.shared;

import com.hp.printosmobile.R;

/**
 * A utils class for Latex and PWP printers.
 * <p/>
 * Created by Osama Taha on 6/1/16.
 */
public class PrinterUtils {

    public static int getPrinterStateImage(DeviceState state) {

        switch (state) {
            case WARNING:
                return R.drawable.warning_triangle;
            case ERROR:
                return R.drawable.not_ok;
            case READY:
                return R.drawable.ok_green;
            default:
                return android.R.color.transparent;
        }
    }

}
