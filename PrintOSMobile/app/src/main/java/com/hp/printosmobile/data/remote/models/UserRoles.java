package com.hp.printosmobile.data.remote.models;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Anwar Asbah
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserRoles {

    @JsonProperty("userRoles")
    private List<UserRole> userRoles = new ArrayList<>();
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * @return The userRoles
     */
    @JsonProperty("userRoles")
    public List<UserRole> getUserRoles() {
        return userRoles;
    }

    /**
     * @param userRoles The userRoles
     */
    @JsonProperty("userRoles")
    public void setUserRoles(List<UserRole> userRoles) {
        this.userRoles = userRoles;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("UserRoles{");
        sb.append("UserRole='").append(userRoles.toString()).append('\'');
        return sb.toString();
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class UserRole {
        @JsonProperty("systemRole")
        private SystemRole systemRole;
        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<String, Object>();

        /**
         * @return The systemRole
         */
        @JsonProperty("systemRole")
        public SystemRole getSystemRole() {
            return systemRole;
        }

        /**
         * @param systemRole The systemRole
         */
        @JsonProperty("userRoles")
        public void setSystemRole(SystemRole systemRole) {
            this.systemRole = systemRole;
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }

        @Override
        public String toString() {
            final StringBuffer sb = new StringBuffer("UserRoles{");
            sb.append("SystemRole='").append(systemRole.toString()).append('\'');
            return sb.toString();
        }

        @JsonInclude(JsonInclude.Include.NON_NULL)
        public static class SystemRole {
            @JsonProperty("name")
            private String name;
            @JsonIgnore
            private Map<String, Object> additionalProperties = new HashMap<String, Object>();

            /**
             * @return The name
             */
            @JsonProperty("name")
            public String getName() {
                return name;
            }

            /**
             * @param name The name
             */
            @JsonProperty("name")
            public void setName(String name) {
                this.name = name;
            }

            @JsonAnyGetter
            public Map<String, Object> getAdditionalProperties() {
                return this.additionalProperties;
            }

            @JsonAnySetter
            public void setAdditionalProperty(String name, Object value) {
                this.additionalProperties.put(name, value);
            }

            @Override
            public String toString() {
                final StringBuffer sb = new StringBuffer("UserRoles{");
                sb.append("Name='").append(name).append('\'');
                return sb.toString();
            }
        }
    }
}
