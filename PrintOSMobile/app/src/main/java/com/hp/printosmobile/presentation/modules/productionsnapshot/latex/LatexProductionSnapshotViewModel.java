package com.hp.printosmobile.presentation.modules.productionsnapshot.latex;

/**
 * Created by Anwar Asbah on 4/12/2016.
 */
public class LatexProductionSnapshotViewModel {

    public static final int ERROR_VALUE = -1;

    private int numberOfQueuedJobs;
    private int numberOfPrintingJobs;
    private int numberOfTodayPrintedJobs;
    private int numberOfTodayFailedPrintedJobs;

    public int getNumberOfQueuedJobs() {
        return numberOfQueuedJobs;
    }

    public void setNumberOfQueuedJobs(int numberOfQueuedJobs) {
        this.numberOfQueuedJobs = numberOfQueuedJobs;
    }

    public int getNumberOfPrintingJobs() {
        return numberOfPrintingJobs;
    }

    public void setNumberOfPrintingJobs(int numberOfPrintingJobs) {
        this.numberOfPrintingJobs = numberOfPrintingJobs;
    }

    public int getNumberOfTodayPrintedJobs() {
        return numberOfTodayPrintedJobs;
    }

    public void setNumberOfTodayPrintedJobs(int numberOfTodayPrintedJobs) {
        this.numberOfTodayPrintedJobs = numberOfTodayPrintedJobs;
    }

    public int getNumberOfTodayFailedPrintedJobs() {
        return numberOfTodayFailedPrintedJobs;
    }

    public void setNumberOfTodayFailedPrintedJobs(int numberOfTodayFailedPrintedJobs) {
        this.numberOfTodayFailedPrintedJobs = numberOfTodayFailedPrintedJobs;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("LatexProductionSnapshotViewModel{");
        sb.append("numberOfQueuedJobs=").append(numberOfQueuedJobs);
        sb.append(", numberOfPrintingJobs=").append(numberOfPrintingJobs);
        sb.append(", numberOfTodayPrintedJobs=").append(numberOfTodayPrintedJobs);
        sb.append(", numberOfTodayFailedPrintedJobs=").append(numberOfTodayFailedPrintedJobs);
        sb.append('}');
        return sb.toString();
    }
}