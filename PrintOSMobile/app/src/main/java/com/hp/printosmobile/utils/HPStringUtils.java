package com.hp.printosmobile.utils;

import android.graphics.Typeface;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.StyleSpan;

/**
 * Created by User on 5/15/2016.
 */
public class HPStringUtils {

    public static String capitalizeWords(String string, String separator) {
        if (string == null) {
            return "";
        }

        String[] words = string.split(separator);
        if (words.length == 0) {
            return "";
        }

        String name = "";
        for (int i = 0; i < words.length; i++) {
            String word = words[i];
            if (word.length() > 0) {
                name += Character.toUpperCase(words[i].charAt(0)) + words[i].substring(1) + " ";
            }
        }
        return name;
    }

    public static String getStringBeforeRegex(String string, String regex) {
        if (string == null || regex == null) {
            return string;
        }

        return string.split(regex)[0].trim();
    }

    public static Spannable setSpannableRegexBold(String string, String regex) {
        int startDailyValueIndex = string.indexOf(regex);
        int lastDailyValueIndex = startDailyValueIndex + regex.length();
        final SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(string);

        final StyleSpan boldStyle = new StyleSpan(android.graphics.Typeface.BOLD);
        spannableStringBuilder.setSpan(boldStyle, startDailyValueIndex, lastDailyValueIndex, Spannable.SPAN_INCLUSIVE_INCLUSIVE);

        return spannableStringBuilder;
    }

    public static int getNumberOfExp(String string, String splitRegex) {
        if (string == null || splitRegex == null) {
            return 0;
        }

        return string.split(splitRegex).length;
    }

    public static String addStringIfNotFound(String query, String itemToBeAdded, String splitRegex) {
        if (itemToBeAdded == null || splitRegex == null || query == null) {
            return "";
        }

        String[] items = query.split(splitRegex);
        for (String item : items) {
            if (item.equals(itemToBeAdded)) {
                return query;
            }
        }

        if (query.length() > 0) {
            query += splitRegex;
        }

        return query + itemToBeAdded;
    }

    /**
     * Makes a substring of a string bold.
     *
     * @param text       Full text
     * @param textToBold Text you want to make bold
     * @return String with bold substring
     */
    public static SpannableStringBuilder boldPartOfAString(String text, String textToBold) {

        SpannableStringBuilder builder = new SpannableStringBuilder();

        if (textToBold.length() > 0 && !textToBold.trim().equals("")) {

            //for counting start/end indexes
            String testText = text.toLowerCase();
            String testTextToBold = textToBold.toLowerCase();
            int startingIndex = testText.indexOf(testTextToBold);
            int endingIndex = startingIndex + testTextToBold.length();

            if (startingIndex < 0 || endingIndex < 0) {
                return builder.append(text);
            } else if (startingIndex >= 0 && endingIndex >= 0) {

                builder.append(text);
                builder.setSpan(new StyleSpan(Typeface.BOLD), startingIndex, endingIndex, 0);
            }
        } else {
            return builder.append(text);
        }

        return builder;
    }
}
