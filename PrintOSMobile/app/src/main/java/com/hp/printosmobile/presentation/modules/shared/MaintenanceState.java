package com.hp.printosmobile.presentation.modules.shared;

import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;

/**
 * An enumeration representing the press maintenance state.
 *
 * @Author: Osama Taha
 * <p>
 * Created on 7/15/2015.
 */
public enum MaintenanceState {

    ERROR("Error", 0, R.string.maintenance_state_error),
    WARNING("Warning", 1, R.string.maintenance_state_warning),
    OK("Ok", 2, R.string.maintenance_state_good),
    EMPTY("", 3, 0);

    private String state;
    private int sortOrder;
    private int tooltipTextResourceId;

    MaintenanceState(String state, int sortOrder, int tooltipTextResourceId) {
        this.state = state;
        this.sortOrder = sortOrder;
        this.tooltipTextResourceId = tooltipTextResourceId;

    }

    public String getState() {
        return state;
    }

    public int getSortOrder() {
        return sortOrder;
    }

    public static MaintenanceState from(String state) {
        for (MaintenanceState pressState : MaintenanceState.values()) {
            if (pressState.getState().equals(state))
                return pressState;
        }
        return MaintenanceState.EMPTY;
    }

    public String getTooltipText() {
        return PrintOSApplication.getAppContext().getString(tooltipTextResourceId);
    }
}
