package com.hp.printosmobile.presentation.modules.week;

import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;

import java.util.List;

/**
 * Created by Anwar Asbah on 4/12/2016.
 */
public class WeekCollectionViewModel {

    private List<WeekViewModel> weeks;
    private boolean showRanking;
    private BusinessUnitEnum businessUnitEnum;

    public List<WeekViewModel> getWeeks() {
        return weeks;
    }

    public void setWeeks(List<WeekViewModel> weeks) {
        this.weeks = weeks;
    }

    @Override
    public String toString() {
        return "WeekCollectionViewModel{" +
                "weeks=" + weeks +
                '}';
    }

    public void setShowRanking(boolean showRanking) {
        this.showRanking = showRanking;
    }

    public boolean isShowRanking() {
        return showRanking;
    }

    public BusinessUnitEnum getBusinessUnitEnum() {
        return businessUnitEnum;
    }

    public void setBusinessUnitEnum(BusinessUnitEnum businessUnitEnum) {
        this.businessUnitEnum = businessUnitEnum;
    }
}