package com.hp.printosmobile.presentation.modules.login;

import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.data.remote.models.OrganizationDimensionData;
import com.hp.printosmobile.data.remote.models.UserRoles;
import com.hp.printosmobile.presentation.Presenter;
import com.hp.printosmobilelib.core.communications.remote.APIException;
import com.hp.printosmobilelib.core.communications.remote.SessionManager;
import com.hp.printosmobilelib.core.communications.remote.models.UserCredentials;
import com.hp.printosmobilelib.core.communications.remote.models.UserData;
import com.hp.printosmobilelib.core.logging.HPLogger;

import java.util.List;

import retrofit2.Response;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * A presenter to manage the {@link LoginActivity}
 *
 * @author Osama Taha
 */
public class LoginPresenter extends Presenter<LoginView> {

    private static String TAG = LoginPresenter.class.getSimpleName();

    private Subscription subscription;

    public LoginPresenter() {

    }

    public void login(final String userName, final String password, final String eulaVersion) {

        mView.showLoading();
        mView.setLoginInButtonEnabled(false);

        final UserCredentials credentials = new UserCredentials(userName, password, eulaVersion);

        subscription = PrintOSApplication.getApiServicesProvider().getLoginService().login("", credentials)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<Response<UserData>>() {
                    @Override
                    public void onCompleted() {

                        mView.hideLoading();

                    }

                    @Override
                    public void onError(Throwable e) {

                        mView.setLoginInButtonEnabled(true);
                        mView.hideLoading();
                        if (e instanceof APIException) {
                            mView.showGeneralLoginError((APIException) e);
                        } else {
                            mView.showGeneralLoginError(APIException.create(APIException.Kind.UNEXPECTED));
                        }
                    }

                    @Override
                    public void onNext(Response<UserData> result) {

                        if (result.isSuccessful()) {

                            if (result.body() == null || result.body().getContext() == null
                                    || result.body().getContext().getId() == null) {
                                SessionManager.getInstance(PrintOSApplication.getAppContext()).clearCookie();
                                mView.setLoginInButtonEnabled(true);
                                mView.hideLoading();
                                mView.showErrorMsg(PrintOSApplication.getAppContext().getString(R.string.error_login_no_context));
                                return;
                            }

                            SessionManager.getInstance(PrintOSApplication.getAppContext()).saveUser(credentials);
                            PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).saveOrganization(result.body().getContext());

                            if (result.body().getUser() != null) {
                                HPLogger.d(TAG, "user first name " + result.body().getUser().getFirstName());
                            }

                            mView.onLoginSuccessful(result.body());

                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    getUserRoles();
                                }
                            }).start();

                            getOrganizationDimension();
                        } else {

                            mView.setLoginInButtonEnabled(true);
                        }
                    }
                });

        addSubscriber(subscription);
    }

    @Override
    public void detachView() {
        stopSubscribers();
        mView = null;
    }

    public void getOrganizationDimension() {

        PrintOSApplication.getApiServicesProvider()
                .getOrganizationService().getOrganizationDimension().map(new Func1<Response<OrganizationDimensionData>, Void>() {
            @Override
            public Void call(Response<OrganizationDimensionData> response) {
                if (response.isSuccessful() && response.body() != null) {
                    boolean provision = response.body().getAnonymous();
                    PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).saveSelfProvisionOrganization(
                            String.valueOf(provision));
                    PrintOSApplication.initSdks();

                    List<OrganizationDimensionData.InternalID> internalIDList = response.body().getInternalIDs();
                    if(internalIDList != null && internalIDList.size() > 0) {
                        OrganizationDimensionData.InternalID internalID = internalIDList.get(0);
                        if(internalID != null && internalID.getGbuInternalId() != null) {
                            PrintOSPreferences.getInstance(PrintOSApplication.getAppContext())
                                    .setInternalID(internalID.getGbuInternalId());
                        } else {
                            PrintOSPreferences.getInstance(PrintOSApplication.getAppContext())
                                    .setInternalID("");
                        }
                    } else {
                        PrintOSPreferences.getInstance(PrintOSApplication.getAppContext())
                                .setInternalID("");
                    }
                }
                return null;
            }
        })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<Void>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onNext(Void result) {
                    }
                });
    }

    public void getUserRoles() {

        try {

            Response<UserRoles> userRoles = PrintOSApplication.getApiServicesProvider()
                    .getUserDataService().getUserRoles().execute();

            if (userRoles.isSuccessful()) {
                String roles = "";
                if (userRoles.body() != null && userRoles.body().getUserRoles() != null) {

                    for (int i = 0; i < userRoles.body().getUserRoles().size(); i++) {
                        UserRoles.UserRole role = userRoles.body().getUserRoles().get(i);

                        if (role != null && role.getSystemRole() != null && role.getSystemRole().getName() != null) {
                            roles += role.getSystemRole().getName() +
                                    (i == userRoles.body().getUserRoles().size() - 1 ? "" : "|");
                        }
                    }
                }

                PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).saveUserRoles(roles);

                HPLogger.d(TAG, "user roles " + roles);

                PrintOSApplication.initSdks();

            } else {
                HPLogger.d(TAG, "Error fetching user roles.");
            }

        } catch (Exception e) {
            HPLogger.d(TAG, "Error fetching user roles. " + e.getMessage());
        }
    }
}
