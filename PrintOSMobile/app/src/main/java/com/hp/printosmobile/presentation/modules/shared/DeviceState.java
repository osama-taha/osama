package com.hp.printosmobile.presentation.modules.shared;

import com.hp.printosmobile.R;

/**
 * An enumeration representing the press states.
 *
 * @Author: Osama Taha
 * <p/>
 * Created on 7/15/2015.
 */
public enum DeviceState {

    ERROR("ERROR", R.string.press_state_error, 0, R.color.c58c, true, 1),
    WARNING("WARNING", R.string.press_state_warning, 1, R.color.c58c, true, 2),
    READY("READY", R.string.press_state_ready, 2, R.color.c58c, true, 3),
    UP("UP", R.string.press_state_up, 0, R.color.c58c, true, 3),
    DOWN("DOWN", R.string.press_state_down, 0, R.color.c58c, true, 3),
    PRINTING("PRINTING", R.string.press_state_printing, 0, R.color.c58c, true, 3),
    OFF("OFF", R.string.press_state_off, 0, R.color.c58c, true, 3),
    IDLE("IDLE", R.string.press_state_idle, 0, R.color.c58c, true, 3),
    CHECKING_PRINTER("CHECKINGPRINTER", R.string.press_state_checking_printer, 0, R.color.c58c, true, 3),
    JOBS_ON_HOLD("JOBSONHOLD", R.string.press_state_jobs_on_hold, 0, R.color.c58c, true, 3),
    LAMINATING("LAMINATING", R.string.press_state_laminating, 0, R.color.c58c, true, 3),
    STARTUP("STARTUP", R.string.press_state_startup, 0, R.color.c58c, true, 3),
    CALIBRATING("CALIBRATING", R.string.press_state_calibrating, 0, R.color.c58c, true, 3),
    DIAGNOSTIC_MODE("DIAGNOSTICMODE", R.string.press_state_diagnostic_mode, 0, R.color.c58c, true, 3),
    UNKNOWN("UNKNOWN", R.string.press_state_unknown, 0, R.color.c58c, true, 3),
    MEDIA_LOAD("MEDIALOAD", R.string.press_state_media_load, 0, R.color.c58c, true, 3),
    MAINTENANCE("MAINTENANCE", R.string.press_state_maintenance, 0, R.color.c58c, true, 3),
    PAUSED("PAUSED", R.string.press_state_paused, 0, R.color.c58c, true, 3),
    IN_POWER_SAVE("INPOWERSAVE", R.string.press_state_in_power_save, 0, R.color.c58c, true, 3),
    SLEEPING("SLEEPING", R.string.press_state_sleeping, 0, R.color.c58c, true, 3),
    INITIALIZING("INITIALIZING", R.string.press_state_initializing, 0, R.color.c58c, true, 3),
    STOPPED("STOPPED", R.string.press_state_checking_stopped, 0, R.color.c58c, true, 3),
    PROCESSING("PROCESSING", R.string.press_state_processing, 0, R.color.c58c, true, 3),
    FINISHING("FINISHING", R.string.press_state_finishing, 0, R.color.c58c, true, 3),
    DRYING("DRYING", R.string.press_state_drying, 0, R.color.c58c, true, 3),
    CANCELING("CANCELING", R.string.press_state_canceling, 0, R.color.c58c, true, 3),
    CANCEL_JOB("CANCELJOB", R.string.press_state_cancel_job, 0, R.color.c58c, true, 3),
    EJECTING("EJECTING", R.string.press_state_ejecting, 0, R.color.c58c, true, 3),
    RECEIVING("RECEIVING", R.string.press_state_receiving, 0, R.color.c58c, true, 3),
    CUTTING("CUTTING", R.string.press_state_cutting, 0, R.color.c58c, true, 3),
    PREPARING_TO_PRINT("PREPARINGTOPRINT", R.string.press_state_preparing_to_print, 0, R.color.c58c, true, 3),
    WAITING_TO_PRINT("WAITINGTOPRINT", R.string.press_state_waiting_to_print, 0, R.color.c58c, true, 3),
    DISCONNECTED("DISCONNECTED", R.string.press_state_disconnected, 0, R.color.c58c, true, 3),
    PRINTER_ERROR("PRINTERERROR", R.string.press_state_printer_error, 0, R.color.c58c, true, 3),
    NULL("NULL", R.string.press_state_null, 0, R.color.c58c, true, 4),
    NO_WARRANTY("NO_WARRANTY", R.string.press_state_printer_error, 0, R.color.c58c, true, 5);

    private final int displayNameResourceId;
    private String state;
    private int sortOrder;
    private int color;
    private boolean visibility;
    private int latexSortOrder;

    DeviceState(String state, int displayNameResourceId, int sortOrder, int color, boolean visibility, int latexSortOrder) {
        this.state = state;
        this.displayNameResourceId = displayNameResourceId;
        this.sortOrder = sortOrder;
        this.color = color;
        this.visibility = visibility;
        this.latexSortOrder = latexSortOrder;
    }

    public String getState() {
        return state;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public int getColor() {
        return color;
    }

    public int getSortOder() {
        return sortOrder;
    }

    public boolean isVisible() {
        return visibility;
    }

    public int getDisplayNameResourceId() {
        return displayNameResourceId;
    }

    public int getLatexSortOrder() {
        return latexSortOrder;
    }

    public static DeviceState from(String state, String color) {

        if (state == null) {
            return DeviceState.UNKNOWN;
        }

        for (DeviceState deviceState : DeviceState.values()) {
            if (deviceState.getState().toLowerCase().equals(state.toLowerCase())) {
                deviceState.setColor(DeviceStateColor.from(color).colorResource);
                return deviceState;
            }
        }

        return DeviceState.UNKNOWN;

    }

    public enum DeviceStateColor {

        GOOD("GOOD", R.color.c58a),
        BAD("BAD", R.color.c58c),
        NEUTRAL("NEUTRAL", R.color.c58b);

        private final String colorType;
        private final int colorResource;

        DeviceStateColor(String colorType, int colorResource) {
            this.colorType = colorType;
            this.colorResource = colorResource;
        }

        public String getColorType() {
            return colorType;
        }

        public static DeviceStateColor from(String colorType) {

            if (colorType == null) {
                return DeviceStateColor.NEUTRAL;
            }

            for (DeviceStateColor deviceStateColor : DeviceStateColor.values()) {
                if (deviceStateColor.getColorType().toLowerCase().equals(colorType.toLowerCase())) {
                    return deviceStateColor;
                }
            }
            return DeviceStateColor.NEUTRAL;

        }
    }
}