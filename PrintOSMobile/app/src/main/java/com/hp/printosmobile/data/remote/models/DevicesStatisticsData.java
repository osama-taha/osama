package com.hp.printosmobile.data.remote.models;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.hp.printosmobile.data.remote.models.ExtendedStatistics.GeneralData;
import com.hp.printosmobile.data.remote.models.ExtendedStatistics.JobsData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Osama Taha on 6/1/16.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class DevicesStatisticsData {

    @JsonProperty("total")
    private int total;
    @JsonProperty("limit")
    private int limit;
    @JsonProperty("offset")
    private int offset;
    @JsonProperty("deviceStatistics")
    private List<DeviceStatistics> deviceStatistics = new ArrayList<DeviceStatistics>();
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * @return The deviceStatistics
     */
    @JsonProperty("deviceStatistics")
    public List<DeviceStatistics> getDeviceStatistics() {
        return deviceStatistics;
    }

    /**
     * @param deviceStatistics The deviceStatistics
     */
    @JsonProperty("deviceStatistics")
    public void setDeviceStatistics(List<DeviceStatistics> deviceStatistics) {
        this.deviceStatistics = deviceStatistics;
    }

    /**
     * @return The total
     */
    @JsonProperty("total")
    public Integer getTotal() {
        return total;
    }

    /**
     * @param total The total
     */
    @JsonProperty("total")
    public void setTotal(Integer total) {
        this.total = total;
    }

    /**
     * @return The limit
     */
    @JsonProperty("limit")
    public Integer getLimit() {
        return limit;
    }

    /**
     * @param limit The limit
     */
    @JsonProperty("limit")
    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    /**
     * @return The offset
     */
    @JsonProperty("offset")
    public Integer getOffset() {
        return offset;
    }

    /**
     * @param offset The offset
     */
    @JsonProperty("offset")
    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class DeviceStatistics {

        @JsonProperty("id")
        private int id;
        @JsonProperty("data")
        private Data data;
        @JsonProperty("deviceId")
        private String deviceId;
        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<String, Object>();

        /**
         * @return The id
         */
        @JsonProperty("id")
        public Integer getId() {
            return id;
        }

        /**
         * @param id The id
         */
        @JsonProperty("id")
        public void setId(Integer id) {
            this.id = id;
        }

        /**
         * @return The data
         */
        @JsonProperty("data")
        public Data getData() {
            return data;
        }

        /**
         * @param data The data
         */
        @JsonProperty("data")
        public void setData(Data data) {
            this.data = data;
        }

        /**
         * @return The deviceId
         */
        @JsonProperty("deviceId")
        public String getDeviceId() {
            return deviceId;
        }

        /**
         * @param deviceId The deviceId
         */
        @JsonProperty("deviceId")
        public void setDeviceId(String deviceId) {
            this.deviceId = deviceId;
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }

        @JsonInclude(JsonInclude.Include.NON_NULL)
        @JsonIgnoreProperties(ignoreUnknown = true)
        public static class Data {

            @JsonProperty("deviceStatus")
            private String deviceStatus;
            @JsonProperty("totalJobs")
            private int totalJobs;
            @JsonProperty("generalData")
            private GeneralData generalData;
            @JsonProperty("inkData")
            private InkData inkData;
            @JsonProperty("substrateData")
            private SubstrateData substrateData;
            @JsonProperty("jobData")
            private JobData jobData;
            @JsonProperty("deviceId")
            private String deviceId;
            @JsonIgnore
            private Map<String, Object> additionalProperties = new HashMap<String, Object>();

            /**
             * @return The deviceStatus
             */
            @JsonProperty("deviceStatus")
            public String getDeviceStatus() {
                return deviceStatus;
            }

            /**
             * @param deviceStatus The deviceStatus
             */
            @JsonProperty("deviceStatus")
            public void setDeviceStatus(String deviceStatus) {
                this.deviceStatus = deviceStatus;
            }

            /**
             * @return The totalJobs
             */
            @JsonProperty("totalJobs")
            public Integer getTotalJobs() {
                return totalJobs;
            }

            /**
             * @param totalJobs The totalJobs
             */
            @JsonProperty("totalJobs")
            public void setTotalJobs(Integer totalJobs) {
                this.totalJobs = totalJobs;
            }

            /**
             * @return The general data
             */
            @JsonProperty("generalData")
            public GeneralData getGeneralData() {
                return generalData;
            }

            /**
             * @param generalData
             */
            @JsonProperty("generalData")
            public void setGeneralData(GeneralData generalData) {
                this.generalData = generalData;
            }

            /**
             * @return The inkData
             */
            @JsonProperty("inkData")
            public InkData getInkData() {
                return inkData;
            }

            /**
             * @param inkData The inkData
             */
            @JsonProperty("inkData")
            public void setInkData(InkData inkData) {
                this.inkData = inkData;
            }

            /**
             * @return The substrateData
             */
            @JsonProperty("substrateData")
            public SubstrateData getSubstrateData() {
                return substrateData;
            }

            /**
             * @param substrateData The substrateData
             */
            @JsonProperty("substrateData")
            public void setSubstrateData(SubstrateData substrateData) {
                this.substrateData = substrateData;
            }

            /**
             * @return The jobData
             */
            @JsonProperty("jobData")
            public JobData getJobData() {
                return jobData;
            }

            /**
             * @param jobData The jobData
             */
            @JsonProperty("jobData")
            public void setJobData(JobData jobData) {
                this.jobData = jobData;
            }

            /**
             * @return The deviceId
             */
            @JsonProperty("deviceId")
            public String getDeviceId() {
                return deviceId;
            }

            /**
             * @param deviceId The deviceId
             */
            @JsonProperty("deviceId")
            public void setDeviceId(String deviceId) {
                this.deviceId = deviceId;
            }

            @JsonAnyGetter
            public Map<String, Object> getAdditionalProperties() {
                return this.additionalProperties;
            }

            @JsonAnySetter
            public void setAdditionalProperty(String name, Object value) {
                this.additionalProperties.put(name, value);
            }

            @Override
            public String toString() {
                final StringBuffer sb = new StringBuffer("PrinterData{");
                sb.append("deviceStatus='").append(deviceStatus).append('\'');
                sb.append(", totalJobs=").append(totalJobs);
                sb.append(", additionalProperties=").append(additionalProperties);
                sb.append('}');
                return sb.toString();
            }
        }

        @Override
        public String toString() {
            final StringBuffer sb = new StringBuffer("DeviceStatistics{");
            sb.append("id=").append(id);
            sb.append(", data=").append(data);
            sb.append(", deviceId='").append(deviceId).append('\'');
            sb.append(", additionalProperties=").append(additionalProperties);
            sb.append('}');
            return sb.toString();
        }
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class InkData {

        @JsonProperty("ink_info")
        private List<InkInfo> inkInfo;
        @JsonProperty("intermediate_tanks")
        private List<Object> intermediateTanks = new ArrayList<Object>();
        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<String, Object>();

        /**
         * @return The inkInfo
         */
        @JsonProperty("ink_info")
        public List<InkInfo> getInkInfo() {
            return inkInfo;
        }

        /**
         * @param inkInfo The inkInfo
         */
        @JsonProperty("ink_info")
        public void setInkInfo(List<InkInfo> inkInfo) {
            this.inkInfo = inkInfo;
        }

        /**
         * @return The inkInfo
         */
        @JsonProperty("intermediate_tanks")
        public List<Object> getIntermediateTanks() {
            return intermediateTanks;
        }

        /**
         * @param intermediateTanks The inkInfo
         */
        @JsonProperty("intermediate_tanks")
        public void setIntermediateTanks(List<Object> intermediateTanks) {
            this.intermediateTanks = intermediateTanks;
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }

        @JsonInclude(JsonInclude.Include.NON_NULL)
        public static class InkInfo {

            @JsonProperty("consumable_label_code")
            private String consumableLabelCode;
            @JsonProperty("serial_number")
            private Object serialNumber;
            @JsonProperty("product_number")
            private String productNumber;
            @JsonProperty("expiration_date")
            private String expirationDate;
            @JsonProperty("product_name")
            private String productName;
            @JsonProperty("unit")
            private String unit;
            @JsonProperty("status_id")
            private String statusId;
            @JsonProperty("Level")
            private String level;
            @JsonProperty("max_capacity")
            private String maxCapacity;
            @JsonProperty("measured_quantity_state")
            private String measuredQuantityState;
            @JsonProperty("consumable_state")
            private String consumableState;
            @JsonIgnore
            private Map<String, Object> additionalProperties = new HashMap<String, Object>();

            /**
             * @return The consumableLabelCode
             */
            @JsonProperty("consumable_label_code")
            public String getConsumableLabelCode() {
                return consumableLabelCode;
            }

            /**
             * @param consumableLabelCode The consumable_label_code
             */
            @JsonProperty("consumable_label_code")
            public void setConsumableLabelCode(String consumableLabelCode) {
                this.consumableLabelCode = consumableLabelCode;
            }

            /**
             * @return The serialNumber
             */
            @JsonProperty("serial_number")
            public Object getSerialNumber() {
                return serialNumber;
            }

            /**
             * @param serialNumber The serial_number
             */
            @JsonProperty("serial_number")
            public void setSerialNumber(Object serialNumber) {
                this.serialNumber = serialNumber;
            }

            /**
             * @return The productNumber
             */
            @JsonProperty("product_number")
            public String getProductNumber() {
                return productNumber;
            }

            /**
             * @param productNumber The product_number
             */
            @JsonProperty("product_number")
            public void setProductNumber(String productNumber) {
                this.productNumber = productNumber;
            }

            /**
             * @return The expirationDate
             */
            @JsonProperty("expiration_date")
            public String getExpirationDate() {
                return expirationDate;
            }

            /**
             * @param expirationDate The expiration_date
             */
            @JsonProperty("expiration_date")
            public void setExpirationDate(String expirationDate) {
                this.expirationDate = expirationDate;
            }

            /**
             * @return The productName
             */
            @JsonProperty("product_name")
            public String getProductName() {
                return productName;
            }

            /**
             * @param productName The product_name
             */
            @JsonProperty("product_name")
            public void setProductName(String productName) {
                this.productName = productName;
            }

            /**
             * @return The unit
             */
            @JsonProperty("unit")
            public String getUnit() {
                return unit;
            }

            /**
             * @param unit The unit
             */
            @JsonProperty("unit")
            public void setUnit(String unit) {
                this.unit = unit;
            }

            /**
             * @return The statusId
             */
            @JsonProperty("status_id")
            public String getStatusId() {
                return statusId;
            }

            /**
             * @param statusId The status_id
             */
            @JsonProperty("status_id")
            public void setStatusId(String statusId) {
                this.statusId = statusId;
            }

            /**
             * @return The level
             */
            @JsonProperty("Level")
            public String getLevel() {
                return level;
            }

            /**
             * @param level The Level
             */
            @JsonProperty("Level")
            public void setLevel(String level) {
                this.level = level;
            }

            /**
             * @return The maxCapacity
             */
            @JsonProperty("max_capacity")
            public String getMaxCapacity() {
                return maxCapacity;
            }

            /**
             * @param maxCapacity The max_capacity
             */
            @JsonProperty("max_capacity")
            public void setMaxCapacity(String maxCapacity) {
                this.maxCapacity = maxCapacity;
            }

            /**
             * @return The measuredQuantityState
             */
            @JsonProperty("measured_quantity_state")
            public String getMeasuredQuantityState() {
                return measuredQuantityState;
            }

            /**
             * @param measuredQuantityState The measured_quantity_state
             */
            @JsonProperty("measured_quantity_state")
            public void setMeasuredQuantityState(String measuredQuantityState) {
                this.measuredQuantityState = measuredQuantityState;
            }

            /**
             * @return The consumableState
             */
            @JsonProperty("consumable_state")
            public String getConsumableState() {
                return consumableState;
            }

            /**
             * @param consumableState The consumable_state
             */
            @JsonProperty("consumable_state")
            public void setConsumableState(String consumableState) {
                this.consumableState = consumableState;
            }

            @JsonAnyGetter
            public Map<String, Object> getAdditionalProperties() {
                return this.additionalProperties;
            }

            @JsonAnySetter
            public void setAdditionalProperty(String name, Object value) {
                this.additionalProperties.put(name, value);
            }

        }
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class SubstrateData {
        @JsonProperty("substrates")
        private List<Substrate> substrates = new ArrayList<>();
        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<String, Object>();

        /**
         * @return The substrates
         */
        @JsonProperty("substrates")
        public List<Substrate> getSubstrates() {
            return substrates;
        }

        /**
         * @param substrates The substrates
         */
        @JsonProperty("substrates")
        public void setInkInfo(List<Substrate> substrates) {
            this.substrates = substrates;
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }

        @JsonInclude(JsonInclude.Include.NON_NULL)
        public static class Substrate {

            @JsonProperty("remaining_length")
            private String remainingLength;
            @JsonProperty("substrate_counter")
            private String substrateCounter;
            @JsonProperty("media_name")
            private String mediaName;
            @JsonProperty("media_width")
            private String mediaWidth;
            @JsonProperty("unit")
            private String unit;
            @JsonProperty("tray_state")
            private String trayState;
            @JsonProperty("measured_quantity")
            private String measuredQuantity;
            @JsonIgnore
            private Map<String, Object> additionalProperties = new HashMap<String, Object>();

            /**
             * @return The remainingLength
             */
            @JsonProperty("remaining_length")
            public String getRemainingLength() {
                return remainingLength;
            }

            /**
             * @param remainingLength The remaining_length
             */
            @JsonProperty("remaining_length")
            public void setRemainingLength(String remainingLength) {
                this.remainingLength = remainingLength;
            }

            /**
             * @return The substrateCounter
             */
            @JsonProperty("substrate_counter")
            public String getSubstrateCounter() {
                return substrateCounter;
            }

            /**
             * @param substrateCounter The substrate_counter
             */
            @JsonProperty("substrate_counter")
            public void setSubstrateCounter(String substrateCounter) {
                this.substrateCounter = substrateCounter;
            }

            /**
             * @return The mediaName
             */
            @JsonProperty("media_name")
            public String getMediaName() {
                return mediaName;
            }

            /**
             * @param mediaName The media_name
             */
            @JsonProperty("media_name")
            public void setMediaName(String mediaName) {
                this.mediaName = mediaName;
            }

            /**
             * @return The mediaWidth
             */
            @JsonProperty("media_width")
            public String getMediaWidth() {
                return mediaWidth;
            }

            /**
             * @param mediaWidth The media_width
             */
            @JsonProperty("media_width")
            public void setMediaWidth(String mediaWidth) {
                this.mediaWidth = mediaWidth;
            }

            /**
             * @return The unit
             */
            @JsonProperty("unit")
            public String getUnit() {
                return unit;
            }

            /**
             * @param unit The unit
             */
            @JsonProperty("unit")
            public void setUnit(String unit) {
                this.unit = unit;
            }

            /**
             * @return The trayState
             */
            @JsonProperty("tray_state")
            public String getTrayState() {
                return trayState;
            }

            /**
             * @param trayState The tray_state
             */
            @JsonProperty("tray_state")
            public void setTrayState(String trayState) {
                this.trayState = trayState;
            }

            /**
             * @return The measuredQuantity
             */
            @JsonProperty("measured_quantity")
            public String getMeasuredQuantity() {
                return measuredQuantity;
            }

            /**
             * @param measuredQuantity The measured_quantity
             */
            @JsonProperty("measured_quantity")
            public void setMeasuredQuantity(String measuredQuantity) {
                this.measuredQuantity = measuredQuantity;
            }

            @JsonAnyGetter
            public Map<String, Object> getAdditionalProperties() {
                return this.additionalProperties;
            }

            @JsonAnySetter
            public void setAdditionalProperty(String name, Object value) {
                this.additionalProperties.put(name, value);
            }

        }
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class JobData {
        @JsonProperty("job")
        private JobsData jobs;
        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<String, Object>();

        /**
         * @return The job
         */
        @JsonProperty("job")
        public JobsData getJobs() {
            return jobs;
        }

        /**
         * @param jobs The jobs
         */
        @JsonProperty("job")
        public void setJobs(JobsData jobs) {
            this.jobs = jobs;
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("DevicesStatisticsData{");
        sb.append("total=").append(total);
        sb.append(", limit=").append(limit);
        sb.append(", offset=").append(offset);
        sb.append(", deviceStatistics=").append(deviceStatistics);
        sb.append(", additionalProperties=").append(additionalProperties);
        sb.append('}');
        return sb.toString();
    }
}
