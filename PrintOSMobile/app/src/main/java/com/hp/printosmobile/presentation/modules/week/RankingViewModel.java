package com.hp.printosmobile.presentation.modules.week;

import com.hp.printosmobile.R;

/**
 * Created by Osama Taha on 7/9/17.
 */
public class RankingViewModel {

    private boolean showTrophy;
    private String trophyMessage;
    private boolean hasSubRegionSection;
    private AreaViewModel regionViewModel;
    private boolean hasRegionSection;
    private AreaViewModel subRegionViewModel;
    private boolean hasWorldWideSection;
    private AreaViewModel worldWideViewModel;
    private String top5RankingRegionMessage;
    private boolean worldWideTop5;

    public boolean isShowTrophy() {
        return showTrophy;
    }

    public void setShowTrophy(boolean showTrophy) {
        this.showTrophy = showTrophy;
    }

    public String getTrophyMessage() {
        return trophyMessage;
    }

    public void setTrophyMessage(String trophyMessage) {
        this.trophyMessage = trophyMessage;
    }

    public boolean hasSubRegionSection() {
        return hasSubRegionSection;
    }

    public void setHasSubRegionSection(boolean hasSubRegionSection) {
        this.hasSubRegionSection = hasSubRegionSection;
    }

    public AreaViewModel getRegionViewModel() {
        return regionViewModel;
    }

    public void setRegionViewModel(AreaViewModel regionViewModel) {
        this.regionViewModel = regionViewModel;
    }

    public boolean hasRegionSection() {
        return hasRegionSection;
    }

    public void setHasRegionSection(boolean hasRegionSection) {
        this.hasRegionSection = hasRegionSection;
    }

    public AreaViewModel getSubRegionViewModel() {
        return subRegionViewModel;
    }

    public void setSubRegionViewModel(AreaViewModel subRegionViewModel) {
        this.subRegionViewModel = subRegionViewModel;
    }

    public boolean hasWorldWideSection() {
        return hasWorldWideSection;
    }

    public void setHasWorldWideSection(boolean hasWorldWideSection) {
        this.hasWorldWideSection = hasWorldWideSection;
    }

    public AreaViewModel getWorldWideViewModel() {
        return worldWideViewModel;
    }

    public void setWorldWideViewModel(AreaViewModel worldWideViewModel) {
        this.worldWideViewModel = worldWideViewModel;
    }

    @Override
    public String toString() {
        return "RankingViewModel{" +
                "showTrophy=" + showTrophy +
                ", trophyMessage='" + trophyMessage + '\'' +
                ", hasSubRegionSection=" + hasSubRegionSection +
                ", regionViewModel=" + regionViewModel +
                ", hasRegionSection=" + hasRegionSection +
                ", subRegionViewModel=" + subRegionViewModel +
                ", hasWorldWideSection=" + hasWorldWideSection +
                ", worldWideViewModel=" + worldWideViewModel +
                '}';
    }

    public void setTop5RankingRegionMessage(String top5RankingRegionMessage) {
        this.top5RankingRegionMessage = top5RankingRegionMessage;
    }

    public String getTop5RankingRegionMessage() {
        return top5RankingRegionMessage;
    }

    public void setWorldWideTop5(boolean worldWideTop5) {
        this.worldWideTop5 = worldWideTop5;
    }

    public boolean isWorldWideTop5() {
        return worldWideTop5;
    }

    public static class AreaViewModel {

        private String title;
        private String name;
        private String message;
        private boolean isLowerThird;
        private int actualRank;
        private int total;
        private int offset;
        private Trend trend;
        private RankAreaType rankAreaType;

        public void setTitle(String title) {
            this.title = title;
        }

        public String getTitle() {
            return title;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public boolean isLowerThird() {
            return isLowerThird;
        }

        public void setLowerThird(boolean lowerThird) {
            this.isLowerThird = lowerThird;
        }

        public int getActualRank() {
            return actualRank;
        }

        public void setActualRank(int actualRank) {
            this.actualRank = actualRank;
        }

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public Trend getTrend() {
            return trend;
        }

        public void setTrend(Trend trend) {
            this.trend = trend;
        }

        public RankAreaType getRankAreaType() {
            return rankAreaType;
        }

        public void setRankAreaType(RankAreaType rankAreaType) {
            this.rankAreaType = rankAreaType;
        }

        @Override
        public String toString() {
            return "AreaViewModel{" +
                    "name='" + name + '\'' +
                    ", message='" + message + '\'' +
                    ", isLowerThird=" + isLowerThird +
                    ", actualRank=" + actualRank +
                    ", total=" + total +
                    ", trend=" + trend +
                    '}';
        }

        public void setOffset(int offset) {
            this.offset = offset;
        }

        public int getOffset() {
            return offset;
        }
    }

    public enum RankAreaType {

        SUB_REGION(R.string.weekly_printbeat_site_weekly_rank_sub_region),
        REGION(R.string.weekly_printbeat_site_weekly_rank_region),
        WORLD_WIDE(R.string.weekly_printbeat_site_weekly_rank_world);

        private final int titleResourceId;

        RankAreaType(int titleResourceId) {
            this.titleResourceId = titleResourceId;
        }

        public int getTitleResourceId() {
            return titleResourceId;
        }
    }

    public enum Trend {

        UP(R.drawable.up, R.drawable.green),
        DOWN(R.drawable.down, R.drawable.red),
        EQUALS(R.drawable.orange, R.drawable.orange);

        private final int directionIconResId;
        private final int backgroundIconResId;

        Trend(int trendIconResId, int backgroundIconResId) {
            this.directionIconResId = trendIconResId;
            this.backgroundIconResId = backgroundIconResId;
        }

        public int getDirectionIconResId() {
            return directionIconResId;
        }

        public int getBackgroundIconResId() {
            return backgroundIconResId;
        }
    }
}
