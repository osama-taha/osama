package com.hp.printosmobile.presentation.modules.contacthp.shared;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.hp.printosmobile.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * @author Anwar Asbah Create on 10/11/2016
 */
public class ContactHpContactThroughView extends FrameLayout {

    public enum ContactMethod {
        DO_NOT_CONTACT, PHONE, EMAIL
    }

    @Bind(R.id.enable_contact_check_box)
    CheckBox enableContactCheckBox;
    @Bind(R.id.contact_through_options)
    View optionsView;
    @Bind(R.id.instruction_text_view)
    TextView instructionTextView;
    @Bind(R.id.phone_radio_button)
    RadioButton phoneRadioButton;
    @Bind(R.id.email_radio_button)
    RadioButton emailRadioButton;
    @Bind(R.id.has_no_phone_text_view)
    View hasNoPhone;
    @Bind(R.id.by_text_view)
    View byTextView;

    private String instructionText;
    private boolean isOptional;
    private boolean isPhoneEnabled;

    public ContactHpContactThroughView(Context context) {
        this(context, null);
        init();
    }

    public ContactHpContactThroughView(Context context, AttributeSet attrs) {
        super(context, attrs);
        applyStyle(context.obtainStyledAttributes(attrs, R.styleable.ContactHpContactThroughView, 0, 0));
        init();
    }

    public ContactHpContactThroughView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        applyStyle(context.obtainStyledAttributes(attrs, R.styleable.ContactHpContactThroughView, defStyle, 0));
        init();
    }

    private void applyStyle(TypedArray typedArray) {
        try {
            isOptional = typedArray.getBoolean(R.styleable.ContactHpContactThroughView_contactHpIsOptional, false);
            instructionText = typedArray.getString(R.styleable.ContactHpContactThroughView_contactHpInstructionMessage);
        } finally {
            typedArray.recycle();
        }
    }

    public void init() {
        View view = inflate(getContext(), R.layout.contact_hp_contact_through_view, this);
        ButterKnife.bind(this, view);

        if (instructionText != null) {
            instructionTextView.setText(instructionText);
        }

        optionsView.setVisibility(isOptional ? GONE : VISIBLE);
        byTextView.setVisibility(isOptional? VISIBLE : GONE);
        hasNoPhone.setVisibility(GONE);
        phoneRadioButton.setEnabled(false);
        isPhoneEnabled = false;

        if (!isOptional) {
            enableContactCheckBox.setChecked(true);
            enableContactCheckBox.setVisibility(GONE);
        }

        enableContactCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                optionsView.setVisibility(isChecked ? VISIBLE : GONE);
                hasNoPhone.setVisibility(isChecked && !isPhoneEnabled ? VISIBLE : GONE);
            }
        });
    }

    public ContactMethod getContactOption() {
        if (enableContactCheckBox.isChecked()) {
            return phoneRadioButton.isChecked() ? ContactMethod.PHONE
                    : emailRadioButton.isChecked() ? ContactMethod.EMAIL
                    : ContactMethod.DO_NOT_CONTACT;
        }
        return ContactMethod.DO_NOT_CONTACT;
    }

    public void setHasPhone(boolean hasPhone) {
        isPhoneEnabled = hasPhone;
        phoneRadioButton.setEnabled(hasPhone);
        hasNoPhone.setVisibility(!hasPhone && enableContactCheckBox.isChecked() ? VISIBLE : GONE);
    }

    public void setEnabled(boolean isEnabled) {
        enableContactCheckBox.setEnabled(isEnabled);
        phoneRadioButton.setEnabled(isEnabled && isPhoneEnabled);
        emailRadioButton.setEnabled(isEnabled);
    }
}