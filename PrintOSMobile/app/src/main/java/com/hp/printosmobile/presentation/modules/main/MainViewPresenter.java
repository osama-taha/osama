package com.hp.printosmobile.presentation.modules.main;

import android.content.Context;

import com.hp.printosmobile.IntercomSdk;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.presentation.Presenter;
import com.hp.printosmobile.presentation.modules.beatcoins.BeatCoinsViewModel;
import com.hp.printosmobile.presentation.modules.home.HomeDataManager;
import com.hp.printosmobilelib.core.communications.remote.APIException;
import com.hp.printosmobilelib.core.logging.HPLogger;

import java.util.Map;

import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;

/**
 * Created by Osama Taha on 5/23/16.
 */
public class MainViewPresenter extends Presenter<MainView> {

    private static final String TAG = MainViewPresenter.class.getSimpleName();

    private Subscription mSubscription;
    private Map<BusinessUnitEnum, BusinessUnitViewModel> mBusinessUnitViewModelsMap;

    public void initView(final Context context) {

        if(mView != null){
            mView.showLoading();
        }

        mSubscription = HomeDataManager.initBusinessUnits(context)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Action1<Map<BusinessUnitEnum, BusinessUnitViewModel>>() {
                    @Override
                    public void call(Map<BusinessUnitEnum, BusinessUnitViewModel> businessUnitViewModelsMap) {

                        if (businessUnitViewModelsMap != null && businessUnitViewModelsMap.size() > 0) {

                            mBusinessUnitViewModelsMap = businessUnitViewModelsMap;

                            mView.hideLoading();

                            if (businessUnitViewModelsMap.isEmpty()) {
                                mView.onEmptyBusinessUnits();
                            } else {
                                mView.onGettingBusinessUnitsCompleted(businessUnitViewModelsMap);
                                if(businessUnitViewModelsMap != null && businessUnitViewModelsMap.containsKey(
                                        BusinessUnitEnum.INDIGO_PRESS)){
                                    PrintOSPreferences.getInstance(context).setHasIndigoDivision(true);
                                }
                            }

                        } else {
                            mView.hideLoading();
                            mView.onNoDevicesAttached();
                        }
                    }
                }, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        loadError(throwable);
                    }
                });
        addSubscriber(mSubscription);

    }

    public void getBeatCoinData(String id) {
        HPLogger.d(TAG, "getBeatCoinData("+id+")");
        mSubscription = HomeDataManager.getBeatCoins(id)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<BeatCoinsViewModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        HPLogger.d(TAG, "getBeatCoinData: fail " + e.getMessage());
                        if(mView != null) {
                            mView.onBeatCoinDataRetrieved(null);
                        }
                    }

                    @Override
                    public void onNext(BeatCoinsViewModel viewModel) {
                        HPLogger.d(TAG, "getBeatCoinData: success");
                        if(mView != null) {
                            mView.onBeatCoinDataRetrieved(viewModel);
                        }
                    }
                });

        addSubscriber(mSubscription);

    }

    private void loadError(Throwable throwable) {
        APIException exception = APIException.create(APIException.Kind.UNEXPECTED);
        if (throwable instanceof APIException) {
            exception = (APIException) throwable;
        }

        if (mView != null) {
            mView.hideLoading();
            mView.onError(exception, MainActivity.TAG);
        }
    }

    public void logout() {
        if(mView != null) {
            mView.showLoading();

            mView.hideLoading();
            mView.onApplicationLogout();
        }

        IntercomSdk.getInstance(PrintOSApplication.getAppContext()).logout();

    }

    @Override
    public void detachView() {
        stopSubscribers();
        mView = null;
    }
}
