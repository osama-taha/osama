package com.hp.printosmobile.presentation.modules.timezonedialog;

import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.hp.printosmobile.AppseeSdk;
import com.hp.printosmobile.R;
import com.hp.printosmobile.utils.HPStringUtils;
import com.hp.printosmobilelib.core.logging.HPLogger;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Osama Taha
 */
public class TimeZoneDialog extends DialogFragment {

    public static final String TAG = TimeZoneDialog.class.getSimpleName();

    private static final String KEY_SUGGESTED_TIME_ZONE = "key_time_zone";

    @Bind(R.id.time_zone_dialog_title)
    TextView titleTextView;

    @Bind(R.id.time_zone_dialog_message)
    TextView messageTextView;

    private TimeZoneDialogCallback callback;

    public static TimeZoneDialog getInstance(TimeZoneDialogCallback callback, String suggestedTimeZone) {
        TimeZoneDialog timeZoneDialog = new TimeZoneDialog();
        Bundle args = new Bundle();
        args.putString(KEY_SUGGESTED_TIME_ZONE, suggestedTimeZone);
        timeZoneDialog.setArguments(args);
        timeZoneDialog.callback = callback;
        return timeZoneDialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        View rootView = inflater.inflate(R.layout.time_zone_dialog, container, false);
        ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        AppseeSdk.getInstance(getActivity()).startScreen(AppseeSdk.SCREEN_TIME_ZONE_DIALOG);

        titleTextView.setText(getString(R.string.app_name));

        String timezone = getArguments().getString(KEY_SUGGESTED_TIME_ZONE);
        String message = view.getContext().getString(R.string.time_zone_dialog_message, timezone);

        SpannableStringBuilder spannableStringBuilder = HPStringUtils.boldPartOfAString(message, timezone);

        messageTextView.setText(spannableStringBuilder);

        setCancelable(false);
    }

    @OnClick(R.id.time_zone_dialog_no_button)
    public void onNoButtonClicked() {

        HPLogger.d(TAG, "timezone - No button clicked.");

        AppseeSdk.getInstance(getActivity()).sendEvent(AppseeSdk.EVENT_TIME_ZONE_NO_CLICKED);

        dismiss();
    }

    @OnClick(R.id.time_zone_dialog_yes_button)
    public void onSetTimeZoneButtonClicked() {

        HPLogger.d(TAG, "timezone - Yes button clicked.");

        AppseeSdk.getInstance(getActivity()).sendEvent(AppseeSdk.EVENT_TIME_ZONE_YES_CLICKED);

        callback.onSetButtonClicked();

        dismiss();
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if (callback != null) {
            callback.onDismiss();
        }
    }

    public interface TimeZoneDialogCallback {

        void onDismiss();

        void onSetButtonClicked();
    }
}
