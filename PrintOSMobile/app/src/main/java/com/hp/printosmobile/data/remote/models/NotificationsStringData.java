package com.hp.printosmobile.data.remote.models;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Anwar Asbah
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
public class NotificationsStringData {

    @JsonProperty("messages")
    private List<NotificationString> messages;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("messages")
    public List<NotificationString> getMessages() {
        return messages;
    }

    @JsonProperty("messages")
    public void setMessages(List<NotificationString> messages) {
        this.messages = messages;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public static class NotificationString {
        @JsonProperty("msgId")
        private String msgId;
        @JsonProperty("msgText")
        private String msgText;
        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<String, Object>();

        @JsonProperty("msgId")
        public String getMsgId() {
            return msgId;
        }

        @JsonProperty("msgId")
        public void setMsgId(String msgId) {
            this.msgId = msgId;
        }

        @JsonProperty("msgText")
        public String getMsgText() {
            return msgText;
        }

        @JsonProperty("msgText")
        public void setMsgText(String msgText) {
            this.msgText = msgText;
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }
    }

}
