package com.hp.printosmobile.data.remote.models;

import android.content.Context;
import com.hp.printosmobilelib.core.logging.HPLogger;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobilelib.core.communications.remote.Preferences;

/**
 * Created by user on 4/12/2016.
 */
public class ProductionViewModel {

    Context mAppContext;
    private static final String PROD_DATA_NAME = "prod_data";
    private static String TAG = ProductionViewModel.class.getSimpleName();

    public ProductionViewModel( Context appContext) {
        mAppContext=appContext;
    }

    public void save(ProductionData productionData) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            String jsonInString = mapper.writeValueAsString(productionData);
            Preferences.getInstance(mAppContext).put(PROD_DATA_NAME,jsonInString);
            Preferences.getInstance(mAppContext).commit();
            HPLogger.d(TAG,"Save data to shared preferences") ;
        } catch (JsonProcessingException e) {
            HPLogger.d(TAG,e.toString()) ;
        }
    }

    public ProductionData loadFeed() {
        ProductionData ret=null;
        try {
            ObjectMapper mapper = new ObjectMapper();
            String jsonInString=Preferences.getInstance(mAppContext).getString(PROD_DATA_NAME,null);
            if (jsonInString!=null) {
                ret = mapper.readValue(jsonInString, ProductionData.class);
            }
        } catch (Exception e) {
            HPLogger.d(TAG,e.toString()) ;
        }
        return ret;
    }

    public void clear() {
    }
}
