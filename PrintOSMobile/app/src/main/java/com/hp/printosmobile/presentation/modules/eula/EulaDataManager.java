package com.hp.printosmobile.presentation.modules.eula;

import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.data.remote.ApiServicesProvider;
import com.hp.printosmobile.data.remote.services.EulaService;

import okhttp3.ResponseBody;
import retrofit2.Response;
import rx.Observable;
import rx.functions.Func1;

/**
 * Created by Anwar Asbah on 9/1/16.
 */
public class EulaDataManager {

    private static final String TAG = EulaDataManager.class.getSimpleName();

    private static final String EULA_VERSION_HEADER = "EULA-Version";

    /**
     * Returns an observable object responsible for retrieving EULA content.
     */
    public static Observable<EULAViewModel> getEulaText(String language) {

        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
        EulaService eulaService = serviceProvider.getEULAService();

        return eulaService.getEULA(language).map(new Func1<Response<ResponseBody>, EULAViewModel>() {
            @Override
            public EULAViewModel call(Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {

                        EULAViewModel eulaViewModel = new EULAViewModel();
                        eulaViewModel.setEulaText(response.body().string());
                        eulaViewModel.setEulaVersion(response.headers().get(EULA_VERSION_HEADER));

                        return eulaViewModel;

                    } catch (Exception e) {
                        return null;
                    }
                }
                return null;
            }
        });
    }
}