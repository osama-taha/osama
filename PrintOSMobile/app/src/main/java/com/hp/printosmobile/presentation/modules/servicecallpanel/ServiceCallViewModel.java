package com.hp.printosmobile.presentation.modules.servicecallpanel;

import com.hp.printosmobile.R;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

/**
 * Created by Anwar Asbah on 3/9/2017.
 */
public class ServiceCallViewModel implements Serializable {

    public static Comparator<CallViewModel> SERVICE_CALL_COMPARATOR = new Comparator<CallViewModel>() {
        @Override
        public int compare(CallViewModel lhs, CallViewModel rhs) {
            if (lhs == null) {
                return 1;
            }
            return lhs.compareTo(rhs);
        }
    };

    public enum ServiceCallStateEnum implements Serializable {

        OPEN("OPEN", R.string.service_call_open, R.drawable.rounded_bg_red, R.color.bar_red, 1),
        CLOSED("CLOSED", R.string.service_call_closed, R.drawable.rounded_bg_green, R.color.bar_green, 2),
        CANCELED("CANCELED", R.string.service_call_closed, R.drawable.rounded_bg_green, R.color.bar_green, 2),
        UNKNOWN("UNKNOWN", R.string.unknown_value, R.drawable.rounded_bg_red, R.color.bar_green, 3);

        private String state;
        private int colorDrawable;
        private int displayNameString;
        private int sortOrder;
        private int color;

        ServiceCallStateEnum(String state, int displayNameString, int colorDrawable, int color,
                             int sortOrder) {
            this.state = state;
            this.colorDrawable = colorDrawable;
            this.displayNameString = displayNameString;
            this.sortOrder = sortOrder;
            this.color = color;
        }

        public String getState() {
            return state;
        }

        public int getColorDrawable() {
            return colorDrawable;
        }

        public int getDisplayNameString() {
            return displayNameString;
        }

        public int getSortOrder() {
            return sortOrder;
        }

        public int getColor() {
            return color;
        }

        public static ServiceCallStateEnum from(String state) {
            if (state != null) {
                for (ServiceCallStateEnum stateEnum : ServiceCallStateEnum.values()) {
                    if (stateEnum.getState().toLowerCase().equals(state.toLowerCase()))
                        return stateEnum;
                }
            }
            return UNKNOWN;
        }
    }


    private List<CallViewModel> callViewModels;
    private int numberOfClosedCalls;
    private int numberOfOpenCalls;
    private int closedServiceCallsDays;

    public int getNumberOfClosedCalls() {
        return numberOfClosedCalls;
    }

    public void setNumberOfClosedCalls(int numberOfClosedCalls) {
        this.numberOfClosedCalls = numberOfClosedCalls;
    }

    public int getNumberOfOpenCalls() {
        return numberOfOpenCalls;
    }

    public void setNumberOfOpenCalls(int numberOfOpenCalls) {
        this.numberOfOpenCalls = numberOfOpenCalls;
    }

    public List<CallViewModel> getCallViewModels() {
        return callViewModels;
    }

    public void setCallViewModels(List<CallViewModel> callViewModels) {
        this.callViewModels = callViewModels;
    }

    public void setClosedServiceCallsDays(int closedServiceCallsDays) {
        this.closedServiceCallsDays = closedServiceCallsDays;
    }

    public int getClosedServiceCallsDays() {
        return closedServiceCallsDays;
    }

    public static class CallViewModel implements Serializable, Comparable<CallViewModel> {

        private String pressName;
        private String impressionType;
        private String pressSerialNumber;
        private Date dateOpened;
        private Date dateClosed;
        private String openedBy;
        private String id;
        private String type;
        private String description;
        private ServiceCallStateEnum state;

        public String getPressName() {
            return pressName;
        }

        public void setPressName(String pressName) {
            this.pressName = pressName;
        }

        public String getImpressionType() {
            return impressionType;
        }

        public void setImpressionType(String impressionType) {
            this.impressionType = impressionType;
        }

        public String getPressSerialNumber() {
            return pressSerialNumber;
        }

        public void setPressSerialNumber(String pressSerialNumber) {
            this.pressSerialNumber = pressSerialNumber;
        }

        public Date getDateOpened() {
            return dateOpened;
        }

        public void setDateOpened(Date dateOpened) {
            this.dateOpened = dateOpened;
        }

        public Date getDateClosed() {
            return dateClosed;
        }

        public void setDateClosed(Date dateClosed) {
            this.dateClosed = dateClosed;
        }

        public String getOpenedBy() {
            return openedBy;
        }

        public void setOpenedBy(String openedBy) {
            this.openedBy = openedBy;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public ServiceCallStateEnum getState() {
            return state;
        }

        public void setState(ServiceCallStateEnum state) {
            this.state = state;
        }

        @Override
        public int compareTo(CallViewModel another) {
            if (another == null) return -1;

            int sortDiff = getState().getSortOrder() - another.getState().getSortOrder();

            if (sortDiff == 0) {
                Date thisDate = getState() == ServiceCallStateEnum.OPEN ? getDateOpened() : getDateClosed();
                Date anotherDate = another.getState() == ServiceCallStateEnum.OPEN ? another.getDateOpened() : another.getDateClosed();

                if (thisDate == null && another == null) {
                    return 0;
                } else if (thisDate == null && anotherDate != null) {
                    return 1;
                } else if (thisDate != null && anotherDate == null) {
                    return -1;
                }
                return -1 * thisDate.compareTo(anotherDate);
            }

            return sortDiff;
        }
    }

}
