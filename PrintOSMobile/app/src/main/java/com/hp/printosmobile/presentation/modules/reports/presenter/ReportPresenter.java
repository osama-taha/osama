package com.hp.printosmobile.presentation.modules.reports.presenter;

import android.content.Context;

import com.hp.printosmobile.data.remote.models.ScorableConfigData;
import com.hp.printosmobile.presentation.Presenter;
import com.hp.printosmobile.presentation.modules.reports.ReportFilter;
import com.hp.printosmobile.presentation.modules.reports.ReportKpiViewModel;
import com.hp.printosmobile.presentation.modules.reports.ReportsFragment;
import com.hp.printosmobile.utils.HPUIUtils;
import com.hp.printosmobilelib.core.communications.remote.APIException;
import com.hp.printosmobilelib.core.logging.HPLogger;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Response;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Anwar Asbah on 23/05/2016.
 */
public class ReportPresenter extends Presenter<ReportView> {

    private static final String TAG = ReportPresenter.class.getSimpleName();
    public static final String TAG_OVERALL = "OVERALL";
    public static final String TAG_KPI = "KPI";

    private static List<ScorableConfigData> reportKPIMetaDataList = null;

    private static List<String> IGNORED_KPIS = Arrays.asList("Bid".toLowerCase(),
            "Bid Base".toLowerCase(), "Bid Roller".toLowerCase());

    public static void setReportKPIMetaDataList(List<ScorableConfigData> data) {
        reportKPIMetaDataList = data;
    }

    public void getOverAllData(Context context, ReportFilter reportFilter) {
        Subscription subscription = ReportDataManager.getOverallData(context, reportFilter)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<ReportKpiViewModel>() {
                    @Override
                    public void onCompleted() {
                        mView.onRequestCompleted();
                    }

                    @Override
                    public void onError(Throwable e) {
                        onFailure(e, TAG_OVERALL);
                        mView.onOverallError();
                    }

                    @Override
                    public void onNext(ReportKpiViewModel overAllKpi) {
                        mView.displayKpiReport(overAllKpi, true, ReportsFragment.OVERALL_POINTS);
                    }
                });
        addSubscriber(subscription);
    }

    public void getKpiData(Context context, ReportFilter reportFilter, boolean resetMetadata) {
        if (reportKPIMetaDataList != null || resetMetadata) {
            fetchKpiData(context, reportFilter);
        } else {
            fetchReportMetaData(context, reportFilter);
        }
    }

    private void fetchReportMetaData(final Context context, final ReportFilter reportFilter) {
        Subscription subscription = ReportDataManager.getScorableConfigData(reportFilter)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<Response<List<ScorableConfigData>>>() {
                    @Override
                    public void onCompleted() {
                        fetchKpiData(context, reportFilter);
                    }

                    @Override
                    public void onError(Throwable e) {
                        HPLogger.d(TAG, "Error in getting report metadata " + e);
                    }

                    @Override
                    public void onNext(Response<List<ScorableConfigData>> scorableConfigDataResponse) {
                        reportKPIMetaDataList = scorableConfigDataResponse.body();
                    }
                });
        addSubscriber(subscription);
    }

    private void fetchKpiData(final Context context, final ReportFilter reportFilter) {
        ReportKpiViewModel kpi = getApiReportKPIViewModel(context, reportFilter);

        Subscription subscription = ReportDataManager.getKpiData(context, reportFilter, kpi)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<ReportKpiViewModel>() {
                    @Override
                    public void onCompleted() {
                        mView.onRequestCompleted();
                    }

                    @Override
                    public void onError(Throwable e) {
                        onFailure(e, TAG_KPI);
                    }

                    @Override
                    public void onNext(ReportKpiViewModel kpiViewModel) {
                        mView.displayKpiReport(kpiViewModel, false, ReportsFragment.KPI_POINTS);
                    }
                });
        addSubscriber(subscription);
    }

    private ReportKpiViewModel getApiReportKPIViewModel(Context context, ReportFilter reportFilter) {
        HPUIUtils.resetRandomColorIndex();
        ReportKpiViewModel kpi = reportFilter.getKpi();
        if (reportKPIMetaDataList != null) {
            for (ScorableConfigData scorableConfigData : reportKPIMetaDataList) {
                if (kpi.getName().equals(scorableConfigData.getName())) {
                    List<String> units = scorableConfigData.getUnits();

                    if (units != null && units.size() > 0 && units.get(0) != null) {

                        if (kpi.getSubKPIs() == null) {
                            kpi.setSubKPIs(new ArrayList<ReportKpiViewModel>());
                        }

                        List<ReportKpiViewModel> subKpiList = kpi.getSubKPIs();

                        for (String subKpi : units) {
                            ReportKpiViewModel kpiViewModel = kpi.getSubKpiModel(subKpi);

                            if (IGNORED_KPIS.contains(subKpi.toLowerCase())) {
                                continue;
                            }

                            if (kpiViewModel == null) {
                                kpiViewModel = ReportsDataParser.initKPI(context, subKpi, reportFilter.getBusinessUnit().getBusinessUnit());
                                kpiViewModel.setParentKpi(kpi);
                                kpiViewModel.setAggregate(false); //TODO fetch from meta data
                                subKpiList.add(kpiViewModel);
                            }
                        }

                        if (!kpi.isAggregate()) {
                            kpi = kpi.getSubKPIs().get(0);
                        }

                    }
                    break;
                }
            }
        }
        return kpi;
    }

    private void onFailure(Throwable e, String tag) {
        HPLogger.d(TAG, "Error in Report " + e);
        mView.onRequestCompleted();
        APIException exception = APIException.create(APIException.Kind.UNEXPECTED);
        if (e instanceof APIException) {
            exception = (APIException) e;
        }
        mView.onError(exception, tag);
    }

    public void onRefresh() {
        stopSubscribers();
    }

    @Override
    public void detachView() {
        stopSubscribers();
        mView = null;
    }
}
