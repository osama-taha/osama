package com.hp.printosmobile.presentation.modules.insights;

import com.hp.printosmobile.presentation.Presenter;
import com.hp.printosmobile.presentation.modules.insights.InsightsViewModel.InsightKpiEnum;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitViewModel;
import com.hp.printosmobilelib.core.communications.remote.APIException;
import com.hp.printosmobilelib.core.logging.HPLogger;

import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Anwar Asbah on 6/20/2017.
 */
public class InsightsPresenter extends Presenter<InsightsView> {

    private static final String TAG = InsightsPresenter.class.getSimpleName();

    public void getInsightsData(BusinessUnitViewModel bu, final InsightKpiEnum kpi, String fromDate, String toDate, final boolean isDataSelected) {

        Subscription subscription = InsightsDataManager.getInsightsData(bu, kpi, fromDate, toDate)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<InsightsViewModel>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        HPLogger.d(kpi.getTAG(), "Error while fetching insights data " + e);
                        onRequestError(e, kpi.getTAG());
                    }

                    @Override
                    public void onNext(InsightsViewModel insightsViewModel) {
                        mView.onInsightDataRetrieved(insightsViewModel, kpi, isDataSelected);
                    }
                });
        addSubscriber(subscription);
    }


    private void onRequestError(Throwable e, String tag) {

        APIException exception = APIException.create(APIException.Kind.UNEXPECTED);
        if (e instanceof APIException) {
            exception = (APIException) e;
        }

        mView.onError(exception, tag);
    }

    public void refresh() {
        stopSubscribers();
    }

    @Override
    public void detachView() {
        stopSubscribers();
        mView = null;
    }
}