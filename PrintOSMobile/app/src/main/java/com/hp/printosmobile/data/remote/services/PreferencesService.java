package com.hp.printosmobile.data.remote.services;

import com.hp.printosmobile.data.remote.ApiConstants;
import com.hp.printosmobile.data.remote.models.PreferencesData;

import java.util.List;

import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.PUT;
import rx.Observable;

/**
 * Retrofit REST interface definition for Preferences Endpoint
 * Created by Osama Taha on 5/17/2017.
 */
public interface PreferencesService {

    @GET(ApiConstants.PREFERENCES_API)
    Observable<Response<PreferencesData>> getPreferences();

    @PUT(ApiConstants.PREFERENCES_API)
    Observable<Response<PreferencesData>> savePreferences(@Body PreferencesData preferencesData);

    @GET(ApiConstants.PREFERENCES_TIME_ZONES_API)
    Observable<Response<List<TimeZone>>> getTimeZones();

}
