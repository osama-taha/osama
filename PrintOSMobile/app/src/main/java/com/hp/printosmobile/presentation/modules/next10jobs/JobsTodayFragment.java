package com.hp.printosmobile.presentation.modules.next10jobs;

import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitViewModel;
import com.hp.printosmobile.presentation.modules.main.IMainFragment;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.presentation.modules.shared.DeviceViewModel;
import com.hp.printosmobile.utils.HPUIUtils;
import com.hp.printosmobilelib.ui.common.HPFragment;
import com.hp.printosmobilelib.ui.widgets.HPViewPager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;

/**
 * create by anwar asbah 5/16/2017
 */
public class JobsTodayFragment extends HPFragment implements IMainFragment, JobsListFragment.JobsListFragmentCallback {

    private static final String ARG_FOCUS_ON = "param2";
    private static final long DELAY = 300;

    @Bind(R.id.viewpager)
    HPViewPager viewPager;
    @Bind(R.id.sliding_tabs)
    TabLayout jobsTabs;
    @Bind(R.id.loading_view)
    View loadingView;

    List<DeviceViewModel> deviceViewModels;
    JobsPagerAdapter jobsPagerAdapter;
    private DeviceViewModel.JobType jobType;

    private JobsTodayFragmentCallback listener;

    public static JobsTodayFragment newInstance() {
        return newInstance(DeviceViewModel.JobType.QUEUED);
    }

    public static JobsTodayFragment newInstance(DeviceViewModel.JobType jobType) {
        JobsTodayFragment fragment = new JobsTodayFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_FOCUS_ON, jobType);
        fragment.setArguments(args);
        return fragment;
    }

    public void attachListener(JobsTodayFragmentCallback listener) {
        this.listener = listener;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle bundle = getArguments();
        jobType = DeviceViewModel.JobType.QUEUED;
        if (bundle != null && bundle.containsKey(ARG_FOCUS_ON)) {
            jobType = (DeviceViewModel.JobType) bundle.getSerializable(ARG_FOCUS_ON);
        }

        initView();
        setViewModels(null);
        Analytics.sendEvent(Analytics.MOBILE_PRINT_BEAT_CATEGORY, Analytics.VIEW_NEXT_10_JOBS_ACTION);
    }

    private void initView() {
        if (listener != null) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    listener.onInitializeJobsView(JobsTodayFragment.this);
                }
            }, DELAY);
        }

        for (int i = 0; i < jobsTabs.getTabCount(); i++) {
            View tab = ((ViewGroup) jobsTabs.getChildAt(0)).getChildAt(i);
            ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) tab.getLayoutParams();
            p.setMargins(0, 0, HPUIUtils.dpToPx(getActivity(), 10), 0);
            tab.requestLayout();
        }
    }

    private void setupViewPager(boolean hasCompleted) {

        jobsTabs.setVisibility(View.VISIBLE);
        jobsPagerAdapter = new JobsPagerAdapter(getChildFragmentManager());

        jobsPagerAdapter.addFragment(DeviceViewModel.JobType.QUEUED, JobsListFragment.newInstance(DeviceViewModel.JobType.QUEUED, this));
        if(hasCompleted && PrintOSPreferences.getInstance(getActivity()).isCompletedJobsFeatureEnabled()) {
            jobsPagerAdapter.addFragment(DeviceViewModel.JobType.COMPLETED, JobsListFragment.newInstance(DeviceViewModel.JobType.COMPLETED, this));
        }

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(jobsTabs));
        viewPager.setScrollingEnabled(false);
        viewPager.setAdapter(jobsPagerAdapter);
        viewPager.setOffscreenPageLimit(jobsPagerAdapter.getCount());

        jobsTabs.setupWithViewPager(viewPager);

        focusOnPage(jobType);
    }

    @Override
    public boolean isIntercomAccessible() {
        return true;
    }

    public void setLoading(boolean isLoading) {
        loadingView.setVisibility(isLoading ? View.VISIBLE : View.GONE);
    }

    public void setViewModels(List<DeviceViewModel> models) {

        if(!isAdded() || getActivity() == null) {
            return;
        }

        boolean hasCompleted = false;
        if(models != null) {
            for (DeviceViewModel model : models) {
                List<DeviceViewModel.Job> jobs = model.getJobs(DeviceViewModel.JobType.COMPLETED);
                if (jobs != null && jobs.size() > 0) {
                    hasCompleted = true;
                    break;
                }
            }
        }
        setupViewPager(hasCompleted);

        deviceViewModels = models;
        setLoading(false);
        displayModels();

        focusOnPage(jobType);
    }

    private void displayModels() {
        for (int i = 0; i < jobsPagerAdapter.getCount(); i++) {
            jobsPagerAdapter.getItem(i).setViewModels(deviceViewModels);
        }
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_today_jobs;
    }

    @Override
    public String getToolbarDisplayNameExtra() {
        return "";
    }

    @Override
    public boolean isFragmentNameToBeDisplayed() {
        return true;
    }

    @Override
    public boolean isFilteringAvailable() {
        return true;
    }

    @Override
    public int getToolbarDisplayName() {
        return R.string.next_10_jobs_title;
    }

    @Override
    public boolean onBackPressed() {
        if (listener != null) {
            listener.onJobsTodayBackPressed();
        }
        return true;
    }

    public void focusOnPage(DeviceViewModel.JobType jobType) {
        int index = jobsPagerAdapter.getFragmentPosition(jobType);
        if (index >= 0) {
            viewPager.setCurrentItem(index, true);
        }
    }

    @Override
    public void onTryAgainClicked() {
        if (listener != null) {
            listener.onTryAgainClicked();
        }
    }

    @Override
    public void onBusinessUnitSelected(BusinessUnitViewModel businessUnitViewModel) {

    }

    @Override
    public void onFiltersChanged(BusinessUnitViewModel businessUnitViewModel) {
        loadingView.setVisibility(View.VISIBLE);
    }

    @Override
    public void didFinishGettingBusinessUnits(Map<BusinessUnitEnum, BusinessUnitViewModel> businessUnitsMap) {

    }

    @Override
    public void onMainError(String msg, boolean triggerSignOut){
        deviceViewModels = null;
        setLoading(false);
        if(jobsPagerAdapter != null) {
            for (int i = 0; i < jobsPagerAdapter.getCount(); i++) {
                jobsPagerAdapter.getItem(i).onError();
            }
        }
    }

    @Override
    public boolean respondToValidationPeriodExceedance() {
        return false;
    }

    public void onError() {
        if(listener != null) {
            listener.moveToHomeFromTab(getString(R.string.fragment_jobs_key));
        }
    }

    private class JobsPagerAdapter extends FragmentStatePagerAdapter {

        private final Map<DeviceViewModel.JobType, JobsListFragment> fragmentHashMap = new HashMap<>();
        private final List<DeviceViewModel.JobType> fragmentType = new ArrayList<>();

        private JobsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        private void addFragment(DeviceViewModel.JobType jobType, JobsListFragment fragment) {
            fragmentHashMap.put(jobType, fragment);
            fragmentType.add(jobType);
        }

        @Override
        public JobsListFragment getItem(int position) {
            return fragmentHashMap.get(fragmentType.get(position));
        }

        @Override
        public boolean isViewFromObject(View view, Object fragment) {
            return ((Fragment) fragment).getView() == view;
        }

        /**
         * Here we can safely save a reference to the created
         * Fragment, no matter where it came from (either getItem() or
         * FragmentManager).
         */
        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            return super.instantiateItem(container, position);
        }

        @Override
        public int getCount() {
            return fragmentType.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return getString(fragmentType.get(position).getNameID());
        }

        private int getFragmentPosition(DeviceViewModel.JobType jobType) {
            return fragmentType.contains(jobType) ? fragmentType.indexOf(jobType) : -1;
        }

    }

    public interface JobsTodayFragmentCallback {
        void onInitializeJobsView(JobsTodayFragment fragment);

        void onJobsTodayBackPressed();

        void onTryAgainClicked();

        void moveToHomeFromTab(String string);
    }
}
