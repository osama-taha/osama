package com.hp.printosmobile.presentation.modules.today;

import com.hp.printosmobile.presentation.modules.filters.SiteViewModel;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitViewModel;
import com.hp.printosmobile.presentation.modules.shared.DeviceViewModel;
import com.hp.printosmobile.presentation.modules.tooltip.TodayInfoTooltipViewModel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Osama Taha on 5/15/2016.
 */
public class TodayViewModel implements Serializable{

    public static final int NUMBER_OF_DEVICES = 3;

    private List<DeviceViewModel> deviceViewModels;
    private List<DeviceViewModel> statusSortedDeviceViewModels;
    private List<DeviceViewModel> nameSortedDeviceViewModels;
    private double printVolumeShiftTargetValue;
    private double printVolumeIntraDailyTargetValue;
    private double printVolumeValue;
    private List<Impression> valueImpressions;
    private SiteViewModel siteViewModel;
    private TodayInfoTooltipViewModel infoTooltipViewModel;
    private BusinessUnitViewModel businessUnitViewModel;
    private int meters;
    private int squareMeters;
    private int sheets;

    public List<DeviceViewModel> getDeviceViewModels() {
        return deviceViewModels;
    }

    public void setDeviceViewModels(List<DeviceViewModel> deviceViewModels) {
        this.deviceViewModels = deviceViewModels;
    }

    public List<DeviceViewModel> getStatusSortedDeviceViewModels() {
        if (statusSortedDeviceViewModels == null) {
            statusSortedDeviceViewModels = new ArrayList<>();
            for (DeviceViewModel model : deviceViewModels) {
                statusSortedDeviceViewModels.add(model);
            }
            //Collections.sort(statusSortedDeviceViewModels, BaseDeviceViewModel.SEVERITY_COMPARATOR);
        }
        return statusSortedDeviceViewModels;
    }

    public List<DeviceViewModel> getNameSortedDeviceViewModels() {
        if (nameSortedDeviceViewModels == null) {
            nameSortedDeviceViewModels = new ArrayList<>();
            for (DeviceViewModel model : deviceViewModels) {
                nameSortedDeviceViewModels.add(model);
            }
            Collections.sort(nameSortedDeviceViewModels, DeviceViewModel.NAME_COMPARATOR);
        }
        return nameSortedDeviceViewModels;
    }

    public double getPrintVolumeShiftTargetValue() {
        return printVolumeShiftTargetValue;
    }

    public void setPrintVolumeShiftTargetValue(double printVolumeShiftTargetValue) {
        this.printVolumeShiftTargetValue = printVolumeShiftTargetValue;
    }

    public double getPrintVolumeIntraDailyTargetValue() {
        return printVolumeIntraDailyTargetValue;
    }

    public void setPrintVolumeIntraDailyTargetValue(double printVolumeIntraDailyTargetValue) {
        this.printVolumeIntraDailyTargetValue = printVolumeIntraDailyTargetValue;
    }

    public double getPrintVolumeValue() {
        return printVolumeValue;
    }

    public void setPrintVolumeValue(double printVolumeValue) {
        this.printVolumeValue = printVolumeValue;
    }

    public List<Impression> getValueImpressions() {
        return valueImpressions;
    }

    public void setValueImpressions(List<Impression> valueImpressions) {
        this.valueImpressions = valueImpressions;
    }

    public void setSiteViewModel(SiteViewModel siteViewModel) {
        this.siteViewModel = siteViewModel;
    }

    public SiteViewModel getSiteViewModel() {
        return siteViewModel;
    }

    public void setBusinessUnitViewModel(BusinessUnitViewModel businessUnitViewModel) {
        this.businessUnitViewModel = businessUnitViewModel;
    }

    public BusinessUnitViewModel getBusinessUnitViewModel() {
        return businessUnitViewModel;
    }

    public int getMeters() {
        return meters;
    }

    public void setMeters(int meters) {
        this.meters = meters;
    }

    public int getSquareMeters() {
        return squareMeters;
    }

    public void setSquareMeters(int squareMeters) {
        this.squareMeters = squareMeters;
    }

    public int getSheets() {
        return sheets;
    }

    public void setSheets(int sheets) {
        this.sheets = sheets;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("TodayViewModel{");
        sb.append("deviceViewModels=").append(deviceViewModels);
        sb.append(", statusSortedDeviceViewModels=").append(statusSortedDeviceViewModels);
        sb.append(", nameSortedDeviceViewModels=").append(nameSortedDeviceViewModels);
        sb.append(", printVolumeShiftTargetValue=").append(printVolumeShiftTargetValue);
        sb.append(", printVolumeIntraDailyTargetValue=").append(printVolumeIntraDailyTargetValue);
        sb.append(", printVolumeValue=").append(printVolumeValue);
        sb.append(", valueImpressions=").append(valueImpressions);
        sb.append('}');
        return sb.toString();
    }

    public void setInfoTooltipViewModel(TodayInfoTooltipViewModel infoTooltipViewModel) {
        this.infoTooltipViewModel = infoTooltipViewModel;
    }

    public TodayInfoTooltipViewModel getInfoTooltipViewModel() {
        return infoTooltipViewModel;
    }

    public static class Impression implements Serializable{
        private String name;
        private double value;

        public Impression (String name, double value){
            this.name = name;
            this.value = value;
        }

        public String getName() {
            return name;
        }

        public double getValue() {
            return value;
        }
    }
}
