package com.hp.printosmobile.presentation.modules.settings;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.AppseeSdk;
import com.hp.printosmobile.Constants;
import com.hp.printosmobile.IntercomSdk;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.presentation.BaseActivity;
import com.hp.printosmobile.presentation.indigowidget.PrintOSAppWidgetProvider;
import com.hp.printosmobile.utils.HPLocaleUtils;
import com.hp.printosmobile.utils.HPUIUtils;
import com.hp.printosmobilelib.core.logging.HPLogger;
import com.hp.printosmobilelib.ui.common.HPFragment;

import java.util.Locale;

import butterknife.Bind;
import butterknife.OnClick;

public class SettingsActivity extends BaseActivity implements LanguageFragment.LanguageFragmentCallback,
        NotificationsFragment.NotificationFragmentCallback, UnitSystemFragment.UnitSystemFragmentCallback {

    private static final String TAG = SettingsActivity.class.getSimpleName();
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.toolbar_display_title)
    TextView toolbarDisplayName;

    HPFragment subMenuFragment;

    @Bind(R.id.notifications_button)
    View notificationSettingsView;
    @Bind(R.id.notification_settings_title)
    TextView notificationSettingsTitle;
    @Bind(R.id.unit_system_button)
    View unitSystemView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        IntercomSdk.getInstance(this).logEvent(IntercomSdk.PBM_VIEW_SETTINGS_EVENT);
        AppseeSdk.getInstance(this).startScreen(AppseeSdk.SCREEN_SETTINGS);

        initView();
    }

    private void initView() {
        toolbarDisplayName.setText(getString(R.string.settings_title));
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(ResourcesCompat.getDrawable(getResources(), R.drawable.back_button, null));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (!PrintOSApplication.isGooglePlayServicesEnabled()) {
            notificationSettingsTitle.setEnabled(false);
        }

        HPUIUtils.setVisibility(PrintOSPreferences.getInstance(this).supportV2(), unitSystemView);

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_settings;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @OnClick(R.id.languages_button)
    public void onLanguagesSelected() {
        openSubMenu(LanguageFragment.getNewInstance(this));
    }

    @OnClick(R.id.unit_system_button)
    public void onUnitSystemSelected() {
        openSubMenu(UnitSystemFragment.getNewInstance(this));
    }

    @OnClick(R.id.notifications_button)
    public void onNotificationsSelected() {

        if (PrintOSApplication.isGooglePlayServicesEnabled()) {
            openSubMenu(NotificationsFragment.getNewInstance(this));
        } else {
            HPUIUtils.displayToast(this, getString(R.string.google_play_services_not_installed), false);
        }

    }

    private void openSubMenu(HPFragment fragment) {
        HPLogger.d(TAG, "open sub menu " + (fragment == null ? "null"
                : fragment.getClass().getSimpleName()));
        FragmentTransaction trans = getSupportFragmentManager()
                .beginTransaction();

        subMenuFragment = fragment;

        trans.replace(R.id.root_frame, subMenuFragment);
        trans.commit();

        toolbarDisplayName.setText(getString(subMenuFragment.getToolbarDisplayName()));
    }

    @Override
    public void onBackPressed() {

        if (subMenuFragment != null) {
            removeSubMenu();
            AppseeSdk.getInstance(this).startScreen(AppseeSdk.SCREEN_SETTINGS);
            return;
        }

        super.onBackPressed();
    }

    private void removeSubMenu() {
        FragmentTransaction trans = getSupportFragmentManager()
                .beginTransaction();
        trans.remove(subMenuFragment);
        trans.commit();
        subMenuFragment = null;
        toolbarDisplayName.setText(getString(R.string.settings_title));
    }

    @Override
    public void onLanguageChanged(String selectedLanguageKey) {

        Analytics.sendEvent(Analytics.MOBILE_PRINT_BEAT_CATEGORY, Analytics.SETTINGS_CHANGE_LANGUAGE_ACTION, selectedLanguageKey);

        PrintOSPreferences.getInstance(this).setLanguage(selectedLanguageKey);
        HPLocaleUtils.updateLanguage(this, selectedLanguageKey);
        HPLocaleUtils.configureAppLanguage(PrintOSApplication.getInstance(), Locale.getDefault());

        startRegistrationService(Constants.IntentExtras.NOTIFICATION_REGISTER_INTENT_ACTION);
        PrintOSAppWidgetProvider.updateAllWidgets(SettingsActivity.this);

        HPLogger.i(TAG, String.format("user changed language to: %s",
                PrintOSPreferences.getInstance(PrintOSApplication.getInstance()).getLanguageCode()));

        IntercomSdk.getInstance(this).login();

        restartApp();
    }

    @Override
    public void closeNotificationsFragment() {
        onBackPressed();
    }

    @Override
    public void onUnitSystemChanged() {

        PrintOSAppWidgetProvider.updateAllWidgets(this);

        restartApp();
    }

    private void restartApp() {
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_OK, returnIntent);
        this.finish();
    }

    @Override
    public boolean isBackwardCompatible() {
        return true;
    }
}
