package com.hp.printosmobile.presentation.modules.home;

import android.content.Context;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.data.remote.ApiConstants;
import com.hp.printosmobile.data.remote.ApiServicesProvider;
import com.hp.printosmobile.data.remote.models.ActiveShiftsData;
import com.hp.printosmobile.data.remote.models.ActualVsTargetData;
import com.hp.printosmobile.data.remote.models.BeatCoinData;
import com.hp.printosmobile.data.remote.models.BusinessUnit;
import com.hp.printosmobile.data.remote.models.DeviceData;
import com.hp.printosmobile.data.remote.models.DevicesData;
import com.hp.printosmobile.data.remote.models.DevicesStatisticsData;
import com.hp.printosmobile.data.remote.models.ExtendedStatistics.GeneralData;
import com.hp.printosmobile.data.remote.models.ExtendedStatistics.JobsData;
import com.hp.printosmobile.data.remote.models.ExtendedStatistics.Notifications;
import com.hp.printosmobile.data.remote.models.ExtendedStatistics.PrinterData;
import com.hp.printosmobile.data.remote.models.LastSiteData;
import com.hp.printosmobile.data.remote.models.PreferencesData;
import com.hp.printosmobile.data.remote.models.PrintVolumeData;
import com.hp.printosmobile.data.remote.models.RealtimeActualVsTargetDataV2;
import com.hp.printosmobile.data.remote.models.RealtimeManyDataV2;
import com.hp.printosmobile.data.remote.models.RealtimeTargetManyDataV2;
import com.hp.printosmobile.data.remote.models.ServiceCallData;
import com.hp.printosmobile.data.remote.models.ShiftData;
import com.hp.printosmobile.data.remote.models.TargetData;
import com.hp.printosmobile.data.remote.models.VersionsData;
import com.hp.printosmobile.data.remote.models.WeekData;
import com.hp.printosmobile.data.remote.services.BeatCoinService;
import com.hp.printosmobile.data.remote.services.ConfigService;
import com.hp.printosmobile.data.remote.services.MetaDataService;
import com.hp.printosmobile.data.remote.services.PerformanceTrackingService;
import com.hp.printosmobile.data.remote.services.PersonalAdvisorService;
import com.hp.printosmobile.data.remote.services.PreferencesService;
import com.hp.printosmobile.data.remote.services.RealtimeTrackingService;
import com.hp.printosmobile.data.remote.services.ServiceCallService;
import com.hp.printosmobile.data.remote.services.UserDataService;
import com.hp.printosmobile.data.remote.services.WeeklyDataV2;
import com.hp.printosmobile.presentation.modules.beatcoins.BeatCoinsViewModel;
import com.hp.printosmobile.presentation.modules.devicedetails.InkModel;
import com.hp.printosmobile.presentation.modules.devicedetails.JobModel;
import com.hp.printosmobile.presentation.modules.devicedetails.SubstrateModel;
import com.hp.printosmobile.presentation.modules.filters.DeviceFilterViewModel;
import com.hp.printosmobile.presentation.modules.filters.FilterItem;
import com.hp.printosmobile.presentation.modules.filters.FiltersViewModel;
import com.hp.printosmobile.presentation.modules.filters.SiteViewModel;
import com.hp.printosmobile.presentation.modules.kpiview.KPIScoreStateEnum;
import com.hp.printosmobile.presentation.modules.kpiview.KPIScoreTrendEnum;
import com.hp.printosmobile.presentation.modules.kpiview.KPIValueHandleEnum;
import com.hp.printosmobile.presentation.modules.kpiview.KPIViewModel;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitViewModel;
import com.hp.printosmobile.presentation.modules.main.ReportChartTypeEnum;
import com.hp.printosmobile.presentation.modules.main.ReportKpiEnum;
import com.hp.printosmobile.presentation.modules.main.ReportTypeEnum;
import com.hp.printosmobile.presentation.modules.productionsnapshot.indigo.ProductionViewModel;
import com.hp.printosmobile.presentation.modules.servicecallpanel.ServiceCallViewModel;
import com.hp.printosmobile.presentation.modules.shared.DeviceExtraData;
import com.hp.printosmobile.presentation.modules.shared.DeviceLastUpdateType;
import com.hp.printosmobile.presentation.modules.shared.DeviceState;
import com.hp.printosmobile.presentation.modules.shared.DeviceViewModel;
import com.hp.printosmobile.presentation.modules.shared.DevicesUtils;
import com.hp.printosmobile.presentation.modules.shared.MaintenanceState;
import com.hp.printosmobile.presentation.modules.shared.Unit;
import com.hp.printosmobile.presentation.modules.today.TodayViewModel;
import com.hp.printosmobile.presentation.modules.todayhistogram.TodayHistogramViewModel;
import com.hp.printosmobile.presentation.modules.tooltip.TodayInfoTooltipViewModel;
import com.hp.printosmobile.presentation.modules.week.RankingViewModel;
import com.hp.printosmobile.presentation.modules.week.WeekCollectionViewModel;
import com.hp.printosmobile.presentation.modules.week.WeekViewModel;
import com.hp.printosmobile.presentation.modules.week.WeekViewModel.WeekPressStatus.WeeklyPressStatusEnum;
import com.hp.printosmobile.presentation.modules.week.kpiexplanationdialog.KpiExplanationEnum;
import com.hp.printosmobile.presentation.modules.week.kpiexplanationdialog.KpiExplanationUtils;
import com.hp.printosmobile.presentation.modules.week.kpiexplanationdialog.KpiExplanationViewModel;
import com.hp.printosmobile.utils.HPLocaleUtils;
import com.hp.printosmobile.utils.HPScoreUtils;
import com.hp.printosmobilelib.core.logging.HPLogger;
import com.hp.printosmobilelib.core.utils.HPDateUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import okhttp3.ResponseBody;
import retrofit2.Response;
import rx.Observable;
import rx.functions.Func1;
import rx.functions.Func2;
import rx.functions.Func4;

/**
 * Created by Osama Taha on 5/21/16.
 */
public class HomeDataManager {

    private static final String TAG = HomeDataManager.class.getSimpleName();

    public static final String DATA_FORMAT_ACTUAL_VS_TARGET = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
    private static final String SELECTED_PRESS_All = "selected";
    private static final String PRINT_VOLUME_ITEM = "Print Volume";
    private static final String PRINTED_IMPRESSIONS_UNIT = "PrintedImpressions";
    private static final String WEEK_DATE_FORMAT = "yyyy-MM-dd";
    private static final int NUMBER_OF_WEEKS = 4;
    private static final String LAST_UPDATED_TIME_DATE_FORMAT_V1 = "yyyy-MM-dd'T'HH:mm:ssZZZ";
    private static final String LAST_UPDATED_TIME_DATE_FORMAT_V2 = "yyyy-MM-dd'T'HH:mm:ss'Z'";
    private static final String NOTIFICATION_UNREAD_EVENT_ID = "current";
    private static final String SERVICE_CALL_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
    private static final String LAST_UPDATE_TIME_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
    private static final TimeZone UTC_TIME_ZONE = TimeZone.getTimeZone("UTC");
    public static final String DATA_FORMAT_LATEX_JOB_COMPLETION = "yyyy-MM-dd'T'HH:mm:ssZZZ";
    private static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
    private static final String DATE_FORMAT_BEAT_COIN = "yyyyy-MM-dd'T'HH:mm:ss.SSSZ";
    private static final int CLOSED_SERVICE_CALLS_DAYS_DEFAULT = 3;

    private static int[] SHIFTS_COLORS;

    static {
        SHIFTS_COLORS = new int[]{
                R.color.shift_color_1,
                R.color.shift_color_2,
                R.color.shift_color_3,
                R.color.shift_color_4,
                R.color.shift_color_5,
                R.color.shift_color_6,
                R.color.shift_color_7
        };
    }

    public static Observable<List<DeviceViewModel>> getDevicesDataForBusinessUnit(final BusinessUnitViewModel businessUnitViewModel, boolean perSite, boolean isShiftSupport) {

        if (businessUnitViewModel.getBusinessUnit() == BusinessUnitEnum.IHPS_PRESS) {

            return getPWPDevices(businessUnitViewModel, perSite);

        } else {

            Observable observable = businessUnitViewModel.getBusinessUnit() == BusinessUnitEnum.LATEX_PRINTER ?
                    getLatexDashboardViewModel(businessUnitViewModel, perSite, isShiftSupport)
                    : getPrintBeatViewModel(businessUnitViewModel, perSite, isShiftSupport);

            return observable.map(new Func1<PrintBeatDashboardViewModel, List<DeviceViewModel>>() {
                @Override
                public List<DeviceViewModel> call(PrintBeatDashboardViewModel printBeatDashboardViewModel) {
                    return printBeatDashboardViewModel.getTodayViewModel().getDeviceViewModels();
                }
            });
        }

    }


    public static Observable<List<DeviceViewModel>> getPWPDevices(final BusinessUnitViewModel businessUnitViewModel, boolean perSite) {

        return Observable.just(perSite ? businessUnitViewModel.getFiltersViewModel().getSelectedSite().getDevices()
                : businessUnitViewModel.getFiltersViewModel().getSelectedDevices())
                .map(new Func1<List<FilterItem>, List<DeviceViewModel>>() {
                    @Override
                    public List<DeviceViewModel> call(List<FilterItem> filterItems) {

                        List<DeviceViewModel> deviceViewModels = new ArrayList<>();

                        for (FilterItem filterItem : filterItems) {

                            DeviceFilterViewModel deviceFilterItem = (DeviceFilterViewModel) filterItem;

                            DeviceViewModel deviceViewModel = new DeviceViewModel();

                            deviceViewModel.setSerialNumber(deviceViewModel.getSerialNumber());
                            deviceViewModel.setSerialNumberDisplay(deviceFilterItem.getSerialNumberDisplay());
                            deviceViewModel.setName(deviceFilterItem.getDeviceName());
                            deviceViewModel.setBusinessUnitEnum(businessUnitViewModel.getBusinessUnit());
                            deviceViewModel.setDeviceImageResource(DevicesUtils.getPressImage(BusinessUnitEnum.IHPS_PRESS, deviceFilterItem.getPressModel()));
                            deviceViewModel.setDeviceImageUrl(getDeviceImageUrlByDeviceModel(PrintOSApplication.getAppContext(), deviceFilterItem.getPressModel()));
                            deviceViewModel.setDeviceState(DeviceState.UNKNOWN);

                            deviceViewModels.add(deviceViewModel);

                        }

                        return deviceViewModels;
                    }
                });
    }

    public static Observable<ActiveShiftsViewModel> getActiveShifts(final BusinessUnitViewModel businessUnitViewModel) {

        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
        RealtimeTrackingService realtimeTrackingService = serviceProvider.getRealtimeTrackingService();

        List<String> serialNumbers = businessUnitViewModel.getFiltersViewModel().getSerialNumbers();
        return realtimeTrackingService.getActiveShifts(businessUnitViewModel.getBusinessUnit().getName(), serialNumbers).map(new Func1<Response<ActiveShiftsData>, ActiveShiftsViewModel>() {
            @Override
            public ActiveShiftsViewModel call(Response<ActiveShiftsData> activeShiftsDataResponse) {
                return mapShiftsData(activeShiftsDataResponse.body());
            }
        });

    }

    private static ActiveShiftsViewModel mapShiftsData(ActiveShiftsData activeShiftsData) {

        ActiveShiftsViewModel activeShiftsViewModel = new ActiveShiftsViewModel();

        if (activeShiftsData == null) {

            activeShiftsViewModel.setSiteHasShifts(false);

        } else {

            activeShiftsViewModel.setSiteHasShifts(activeShiftsData.getSiteContainShifts());

            if (activeShiftsViewModel.isSiteHasShifts()) {

                List<ShiftViewModel> shiftViewModels = new ArrayList<>();

                shiftViewModels.add(parseShiftData(activeShiftsData.getDayShift()));
                shiftViewModels.add(parseShiftData(activeShiftsData.getCurrShift()));

                activeShiftsViewModel.setShiftViewModels(shiftViewModels);
            }
        }

        return activeShiftsViewModel;
    }

    private static ShiftViewModel parseShiftData(ShiftData shiftData) {

        if (shiftData == null) {
            return null;
        }

        ShiftViewModel shiftViewModel = new ShiftViewModel();
        shiftViewModel.setShiftId(shiftData.getShiftMetaDataId());
        shiftViewModel.setShiftName(shiftData.getShiftName());
        shiftViewModel.setShiftType(ShiftViewModel.ShiftType.fromString(shiftData.getShiftMetaDataId()));
        shiftViewModel.setStartDay(shiftData.getStartDay());
        shiftViewModel.setStartTime(shiftData.getStartTime());
        shiftViewModel.setStartHour(shiftData.getStartHour());
        shiftViewModel.setEndDay(shiftData.getEndDay());
        shiftViewModel.setEndHour(shiftData.getEndHour());
        shiftViewModel.setPresentedDay(shiftData.getPresentedDay());

        return shiftViewModel;
    }

    /**
     * Returns an observable object responsible for combining the response of PrintVolume & targetData APIs.
     */
    public static Observable<PrintBeatDashboardViewModel> getPrintBeatViewModel(final BusinessUnitViewModel businessUnitViewModel, final boolean perSite, boolean isShiftSupport) {

        //TODO: Check with the server team if they can combine these APIs in one API to make things simple.

        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
        RealtimeTrackingService realtimeTrackingService = serviceProvider.getRealtimeTrackingService();

        List<String> apiSerialNumbers;
        final List<String> serialNumbers = perSite ? businessUnitViewModel.getFiltersViewModel().getSiteSerialNumbers() :
                businessUnitViewModel.getFiltersViewModel().getSerialNumbers(DeviceFilterViewModel.RTSupported);

        apiSerialNumbers = serialNumbers;
        if (apiSerialNumbers == null || apiSerialNumbers.size() == 0) {
            apiSerialNumbers = businessUnitViewModel.getFiltersViewModel().getSerialNumbers();
        }

        final SiteViewModel siteViewModel = businessUnitViewModel.getFiltersViewModel().getSelectedSite();

        final List<FilterItem> selectedDevices = perSite ? (siteViewModel == null ? new ArrayList<FilterItem>()
                : businessUnitViewModel.getFiltersViewModel().getSelectedSite().getDevices())
                : businessUnitViewModel.getFiltersViewModel().getSelectedDevices();

        PrintOSPreferences printOSPreferences = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext());

        if (!printOSPreferences.supportV2()) {

            return Observable.combineLatest(realtimeTrackingService.getTargetData(businessUnitViewModel.getBusinessUnit().getName(), apiSerialNumbers, PRINT_VOLUME_ITEM, PRINTED_IMPRESSIONS_UNIT, isShiftSupport),
                    realtimeTrackingService.getPrintVolumeData(businessUnitViewModel.getBusinessUnit().getName(), apiSerialNumbers, PRINT_VOLUME_ITEM, PRINTED_IMPRESSIONS_UNIT, isShiftSupport),
                    new Func2<Response<List<TargetData>>, Response<List<PrintVolumeData>>, PrintBeatDashboardViewModel>() {
                        @Override
                        public PrintBeatDashboardViewModel call(Response<List<TargetData>> targetDataResponse, Response<List<PrintVolumeData>> printVolumeResponse) {

                            PrintBeatDashboardViewModel viewModel = null;
                            if (targetDataResponse.isSuccessful() && printVolumeResponse.isSuccessful() && serialNumbers.size() > 0) {
                                viewModel = mapTodayData(PrintOSApplication.getAppContext(), businessUnitViewModel, siteViewModel, perSite, targetDataResponse.body(), printVolumeResponse.body(), selectedDevices);
                            } else if (serialNumbers == null || serialNumbers.size() == 0) {
                                viewModel = mapTodayData(PrintOSApplication.getAppContext(), businessUnitViewModel, siteViewModel, perSite, null, null, selectedDevices);
                            }
                            return viewModel;
                        }
                    }
            );

        } else {

            String unitSystem = printOSPreferences.getUnitSystem().name();

            return Observable.combineLatest(realtimeTrackingService.getTargetDataV2(businessUnitViewModel.getBusinessUnit().getName(), apiSerialNumbers, PRINT_VOLUME_ITEM, PRINTED_IMPRESSIONS_UNIT, isShiftSupport, unitSystem),
                    realtimeTrackingService.getPrintVolumeDataV2(businessUnitViewModel.getBusinessUnit().getName(), apiSerialNumbers, PRINT_VOLUME_ITEM, PRINTED_IMPRESSIONS_UNIT, isShiftSupport, unitSystem),
                    new Func2<Response<RealtimeTargetManyDataV2>, Response<RealtimeManyDataV2>, PrintBeatDashboardViewModel>() {
                        @Override
                        public PrintBeatDashboardViewModel call(Response<RealtimeTargetManyDataV2> realtimeTargetManyDataV2Response, Response<RealtimeManyDataV2> realtimeManyDataV2Response) {

                            PrintBeatDashboardViewModel viewModel = null;
                            if (realtimeTargetManyDataV2Response.isSuccessful() && realtimeManyDataV2Response.isSuccessful() && serialNumbers.size() > 0) {
                                viewModel = mapTodayData(PrintOSApplication.getAppContext(), businessUnitViewModel, siteViewModel, perSite, realtimeTargetManyDataV2Response.body().getData(), realtimeManyDataV2Response.body().getData(), selectedDevices);
                            } else if (serialNumbers == null || serialNumbers.size() == 0) {
                                viewModel = mapTodayData(PrintOSApplication.getAppContext(), businessUnitViewModel, siteViewModel, perSite, null, null, selectedDevices);
                            }
                            return viewModel;
                        }
                    }
            );

        }

    }

    public static Observable<TodayHistogramViewModel> getActualVsTarget(Context context, final BusinessUnitViewModel businessUnitViewModel, final boolean isShiftSupport) {

        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
        RealtimeTrackingService realtimeTrackingService = serviceProvider.getRealtimeTrackingService();

        String days = String.valueOf(1 + context.getResources().getInteger(R.integer.today_panel_histogram_span));

        List<String> apiSerialNumbers = businessUnitViewModel.getFiltersViewModel().getSerialNumbers(DeviceFilterViewModel.RTSupported);
        if (apiSerialNumbers == null || apiSerialNumbers.size() == 0) {
            apiSerialNumbers = businessUnitViewModel.getFiltersViewModel().getSerialNumbers();
        }

        PrintOSPreferences printOSPreferences = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext());

        if (printOSPreferences.supportV2()) {
            return realtimeTrackingService.getActualVsTargetDataV2(businessUnitViewModel.getBusinessUnit().getName(), apiSerialNumbers, PRINT_VOLUME_ITEM, PRINTED_IMPRESSIONS_UNIT, days, isShiftSupport, printOSPreferences.getUnitSystem().name()).map(new Func1<Response<RealtimeActualVsTargetDataV2>, TodayHistogramViewModel>() {
                @Override
                public TodayHistogramViewModel call(Response<RealtimeActualVsTargetDataV2> realtimeActualVsTargetDataV2Response) {

                    ActualVsTargetData actualVsTargetData = realtimeActualVsTargetDataV2Response == null ? null : realtimeActualVsTargetDataV2Response.body().getData();
                    return parseActualVsTargetData(businessUnitViewModel, actualVsTargetData);
                }
            });

        } else {

            return realtimeTrackingService.getActualVsTargetData(businessUnitViewModel.getBusinessUnit().getName(), apiSerialNumbers, PRINT_VOLUME_ITEM, PRINTED_IMPRESSIONS_UNIT, days, isShiftSupport).map(new Func1<Response<ActualVsTargetData>, TodayHistogramViewModel>() {
                @Override
                public TodayHistogramViewModel call(Response<ActualVsTargetData> actualVsTargetDataResponse) {
                    return parseActualVsTargetData(businessUnitViewModel, actualVsTargetDataResponse.body());
                }
            });

        }
    }

    private static TodayHistogramViewModel parseActualVsTargetData(BusinessUnitViewModel businessUnitViewModel, ActualVsTargetData actualVsTargetData) {

        TodayHistogramViewModel viewModel = new TodayHistogramViewModel();
        viewModel.setBusinessUnit(businessUnitViewModel.getBusinessUnit());

        Calendar tomorrow = Calendar.getInstance();
        tomorrow.add(Calendar.DATE, 1);
        tomorrow.set(Calendar.HOUR_OF_DAY, 0);
        tomorrow.set(Calendar.MINUTE, 0);
        tomorrow.set(Calendar.SECOND, 0);
        tomorrow.set(Calendar.MILLISECOND, 0);

        if (actualVsTargetData != null) {

            int lowerThresholds = 0;
            int higherThresholds = 0;
            int shiftsColorIndex = 0;
            if (actualVsTargetData.getThresholds() != null) {
                higherThresholds = actualVsTargetData.getThresholds().getSucceedPercent();
                lowerThresholds = actualVsTargetData.getThresholds().getFailPercent();
            }

            viewModel.setLowerThreshold(lowerThresholds);
            viewModel.setHigherThreshold(higherThresholds);

            List<TodayHistogramViewModel.TodayHistogramItem> histogramData = new ArrayList<>();

            Map<String, TodayHistogramViewModel.HistogramShiftLegend> histogramLegends = new HashMap<>();

            if (actualVsTargetData.getDays() != null && actualVsTargetData.getDays().size() > 0) {

                for (int index = 0; index < actualVsTargetData.getDays().size(); index++) {

                    ActualVsTargetData.Day day = actualVsTargetData.getDays().get(index);

                    TodayHistogramViewModel.TodayHistogramItem histogramItem = new TodayHistogramViewModel.TodayHistogramItem();

                    histogramItem.setActual(day.getActual());
                    histogramItem.setTarget(day.getTarget());
                    histogramItem.setStatus(TodayHistogramViewModel.TodayHistogramItem.TodayHistogramItemStatus.getStatus(
                            day.getActual(), day.getTarget(), lowerThresholds, higherThresholds));
                    histogramItem.setDayOrdinal(day.getDay());

                    if (day.getData() != null) {

                        if (day.getData().getDate() != null) {
                            histogramItem.setDate(HPDateUtils.parseDate(day.getData().getDate(), DATA_FORMAT_ACTUAL_VS_TARGET));
                            if (histogramItem.getDate() != null && tomorrow.getTime().compareTo(histogramItem.getDate()) == 1) {
                                histogramData.add(histogramItem);
                            }
                        }

                        float errorValue = TodayHistogramViewModel.TodayHistogramItem.ERROR_VALUE;
                        histogramItem.setSheets(getFloat(day.getData().getSheets(), errorValue));
                        histogramItem.setMeters(getFloat(day.getData().getMeters(), errorValue));
                        histogramItem.setMetersSquare(getFloat(day.getData().getSquareMeters(), errorValue));
                    }

                    ShiftViewModel shiftViewModel = parseShiftData(day.getShiftData());

                    if (shiftViewModel != null) {

                        viewModel.setShiftSupport(shiftViewModel.getShiftType() == ShiftViewModel.ShiftType.CURRENT);

                        if (!histogramLegends.containsKey(shiftViewModel.getShiftId())) {

                            TodayHistogramViewModel.HistogramShiftLegend histogramShiftLegend = new TodayHistogramViewModel.HistogramShiftLegend();
                            histogramShiftLegend.setLegendName(shiftViewModel.getShiftName());
                            histogramShiftLegend.setLegendColor(SHIFTS_COLORS[shiftsColorIndex++]);
                            histogramLegends.put(shiftViewModel.getShiftId(), histogramShiftLegend);

                        }
                    }

                    histogramItem.setShift(shiftViewModel);

                }

            }

            Collections.sort(histogramData, TodayHistogramViewModel.TodayHistogramItem.COMPARATOR);

            viewModel.setData(histogramData);
            viewModel.setShiftLegends(histogramLegends);

            return viewModel;
        }

        return null;
    }

    private static float getFloat(Double value, float defaultValue) {
        if (value != null) {
            return value.floatValue();
        }
        return defaultValue;
    }

    public static Observable<PrintBeatDashboardViewModel> getLatexDashboardViewModel(final BusinessUnitViewModel businessUnitViewModel, boolean perSite, boolean isShiftSupport) {

        return Observable.combineLatest(getPrintBeatViewModel(businessUnitViewModel, perSite, isShiftSupport), getLatexDevicesStatisticsData(businessUnitViewModel), new Func2<PrintBeatDashboardViewModel, List<DeviceExtraData>, PrintBeatDashboardViewModel>() {
            @Override
            public PrintBeatDashboardViewModel call(PrintBeatDashboardViewModel printBeatDashboardViewModel, List<DeviceExtraData> deviceExtraDatas) {

                List<DeviceViewModel> printBeatDevices = printBeatDashboardViewModel.getTodayViewModel().getDeviceViewModels();

                if (printBeatDevices != null && printBeatDevices.size() > 0
                        && deviceExtraDatas != null && deviceExtraDatas.size() > 0) {

                    for (DeviceViewModel deviceViewModel : printBeatDevices) {
                        for (DeviceExtraData deviceExtraData : deviceExtraDatas) {
                            if (deviceExtraData.getSerialNumber().equals(deviceViewModel.getSerialNumber())) {
                                deviceViewModel.setExtraData(deviceExtraData);
                                deviceViewModel.setHasExtraData(true);
                                //Get device last update time from device stats if it is null in PB.
                                if (deviceViewModel.getLastUpdateTime() == null) {
                                    if (deviceExtraData.getLastUpdatedAt() != null) {
                                        deviceViewModel.setLastUpdateTime(deviceExtraData.getLastUpdatedAt());
                                        deviceViewModel.setDeviceLastUpdateType(DeviceLastUpdateType.from(deviceViewModel.getLastUpdateTime(),
                                                businessUnitViewModel.getBusinessUnit(), deviceViewModel.isWrongTZ()));
                                    }
                                }
                                break;
                            }
                        }
                    }
                }

                printBeatDashboardViewModel.setProductionViewModel(parseLatexProductionSnapshotData(printBeatDevices, deviceExtraDatas));

                return printBeatDashboardViewModel;
            }
        });

    }

    private static ProductionViewModel parseLatexProductionSnapshotData(List<DeviceViewModel> printBeatDevices, List<DeviceExtraData> deviceExtraDatas) {

        if (deviceExtraDatas == null) {
            return null;
        }

        int queued = 0, printing = 0, printed = 0, failedPrinted = 0;

        for (DeviceExtraData model : deviceExtraDatas) {
            queued += model.getFutureJobs() == null ? 0 : model.getFutureJobs().size();
            printing += model.getPrintingJobs() == null ? 0 : model.getPrintingJobs().size();
        }

        //Get total number of printed jobs from PB as requested.
        if (printBeatDevices != null) {
            for (DeviceViewModel deviceViewModel : printBeatDevices) {
                printed += (deviceViewModel.getPrintedJobs() > 0 ? deviceViewModel.getPrintedJobs() : 0);
            }
        }

        ProductionViewModel viewModel = new ProductionViewModel();
        viewModel.setNumberOfPrintingJobs(printing);
        viewModel.setBusinessUnitEnum(BusinessUnitEnum.LATEX_PRINTER);
        viewModel.setJobsInQueue(queued);
        viewModel.setNumberOfTodayFailedPrintedJobs(failedPrinted);
        viewModel.setNumberOfTodayPrintedJobs(printed);

        return viewModel;
    }


    private static PrintBeatDashboardViewModel mapTodayData(Context context, BusinessUnitViewModel businessUnitViewModel, SiteViewModel siteViewModel, boolean perSite, List<TargetData> targetDataList, List<PrintVolumeData> printVolumeDataList, List<FilterItem> selectedDevices) {

        List<DeviceViewModel> pressViewModels = new ArrayList<>();

        TodayViewModel todayViewModel = new TodayViewModel();

        ProductionViewModel productionViewModel = new ProductionViewModel();
        productionViewModel.setBusinessUnitEnum(businessUnitViewModel.getBusinessUnit());

        //Getting the Indigo devices.
        Map<String, DeviceData> buDevices = businessUnitViewModel.getDevices();

        if (buDevices != null && buDevices.values().size() > 0) {

            //Loops through the data and combines the print volume data and target data in one object for each press.
            if (printVolumeDataList != null && targetDataList != null) {

                for (PrintVolumeData printVolumeData : printVolumeDataList) {

                    TargetData currentTargetData = null;
                    for (TargetData targetData : targetDataList) {
                        if (targetData.getIntraDailyTarget().getDevice().equalsIgnoreCase(printVolumeData.getDevice())) {
                            currentTargetData = targetData;
                            break;
                        }
                    }

                    //selected device here is representing the total printing and target data of all presses.
                    if (printVolumeData.getDevice().equalsIgnoreCase(SELECTED_PRESS_All)) {

                        todayViewModel.setPrintVolumeValue(printVolumeData.getValue());

                        if (currentTargetData != null) {
                            todayViewModel.setPrintVolumeShiftTargetValue(currentTargetData.getShiftTarget().getValue());
                            todayViewModel.setPrintVolumeIntraDailyTargetValue(currentTargetData.getIntraDailyTarget().getValue());
                        }

                        TodayInfoTooltipViewModel tooltipViewModel = new TodayInfoTooltipViewModel();
                        tooltipViewModel.setImpressionsList(new ArrayList<TodayInfoTooltipViewModel.InfoItemViewModel>());
                        tooltipViewModel.setPrintedList(new ArrayList<TodayInfoTooltipViewModel.InfoItemViewModel>());

                        todayViewModel.setInfoTooltipViewModel(tooltipViewModel);

                        int color = HPScoreUtils.getProgressColor(context, todayViewModel.getPrintVolumeValue(), todayViewModel.getPrintVolumeIntraDailyTargetValue(), false);

                        List<TodayViewModel.Impression> impressions = new ArrayList<>();

                        for (PrintVolumeData.ImpressionValue impressionValue : printVolumeData.getValues()) {

                            impressions.add(new TodayViewModel.Impression(impressionValue.getName(), impressionValue.getValue()));

                            TodayInfoTooltipViewModel.InfoItemViewModel infoItemViewModel = new TodayInfoTooltipViewModel.InfoItemViewModel(impressionValue.getName(),
                                    HPLocaleUtils.getLocalizedValue(impressionValue.getValue().intValue()), color);
                            tooltipViewModel.getImpressionsList().add(infoItemViewModel);

                        }

                        PreferencesData.UnitSystem unitSystem = PrintOSPreferences.getInstance(context).getUnitSystem();

                        if (printVolumeData.getLitersConsumed() != null) {
                            String itemName = context.getString(Unit.LITERS.
                                    getUnitTextResourceId(unitSystem, Unit.UnitStyle.DEFAULT));
                            TodayInfoTooltipViewModel.InfoItemViewModel infoItemViewModel = new TodayInfoTooltipViewModel.InfoItemViewModel(itemName, String.valueOf(printVolumeData.getLitersConsumed().intValue()), color);
                            tooltipViewModel.getPrintedList().add(infoItemViewModel);
                        }

                        if (printVolumeData.getSheets() != null) {
                            String label = businessUnitViewModel.getBusinessUnit() == BusinessUnitEnum.INDIGO_PRESS ? context.getString(R.string.sheets) : context.getString(R.string.sheets_printed);
                            String value = businessUnitViewModel.getBusinessUnit() == BusinessUnitEnum.INDIGO_PRESS ? DevicesUtils.getPrintedValue(context, printVolumeData.getSheets()) : String.valueOf(printVolumeData.getSheets());
                            TodayInfoTooltipViewModel.InfoItemViewModel infoItemViewModel = new TodayInfoTooltipViewModel.InfoItemViewModel(label, value, color);
                            tooltipViewModel.getPrintedList().add(infoItemViewModel);
                        }
                        todayViewModel.setSheets(printVolumeData.getSheets() == null ? -1 : printVolumeData.getSheets());


                        if (printVolumeData.getMeters() != null) {
                            String itemName = context.getString(Unit.METER.
                                    getUnitTextResourceId(unitSystem, Unit.UnitStyle.DEFAULT));
                            TodayInfoTooltipViewModel.InfoItemViewModel infoItemViewModel = new TodayInfoTooltipViewModel.InfoItemViewModel(itemName, DevicesUtils.getPrintedValue(context, printVolumeData.getMeters().intValue()), color);
                            tooltipViewModel.getPrintedList().add(infoItemViewModel);
                        }
                        todayViewModel.setMeters(printVolumeData.getMeters() == null ? -1 : printVolumeData.getMeters().intValue());

                        if (printVolumeData.getSquareMeters() != null) {
                            String itemName = context.getString(Unit.SQM.
                                    getUnitTextResourceId(unitSystem, Unit.UnitStyle.DEFAULT));
                            TodayInfoTooltipViewModel.InfoItemViewModel infoItemViewModel = new TodayInfoTooltipViewModel.InfoItemViewModel(itemName, DevicesUtils.getPrintedValue(context, printVolumeData.getSquareMeters().intValue()), color);
                            tooltipViewModel.getPrintedList().add(infoItemViewModel);
                        }
                        todayViewModel.setSquareMeters(printVolumeData.getSquareMeters() == null ? -1 : printVolumeData.getSquareMeters().intValue());

                        todayViewModel.setValueImpressions(impressions);

                        if (tooltipViewModel.getImpressionsList().size() > 0) {
                            Collections.sort(tooltipViewModel.getImpressionsList(), TodayInfoTooltipViewModel.NAME_COMPARATOR);
                        }

                        tooltipViewModel.setHasInfo(businessUnitViewModel.getBusinessUnit() != BusinessUnitEnum.LATEX_PRINTER
                                & (tooltipViewModel.getImpressionsList().size() > 0 ||
                                tooltipViewModel.getPrintedList().size() > 0));

                    } else {


                        DeviceViewModel deviceViewModel = new DeviceViewModel();

                        DeviceData deviceData = buDevices.get(printVolumeData.getDevice());

                        List<DeviceViewModel.Job> queuedJobs = new ArrayList<>();
                        List<PrintVolumeData.job> queuedJobsData = printVolumeData.getNextTenJobs();
                        if (queuedJobsData != null) {
                            for (PrintVolumeData.job job : queuedJobsData) {
                                DeviceViewModel.Job indigoJob = new DeviceViewModel.Job();
                                indigoJob.setIndex(job.getIndex());
                                indigoJob.setName(job.getJobName());
                                indigoJob.setDuration(job.getTimeToComplete() == null ? 0 : job.getTimeToComplete().getDuration());
                                try {
                                    indigoJob.setTimeUnit((job.getTimeToComplete() == null || job.getTimeToComplete().getUnit() == null) ?
                                            TimeUnit.SECONDS : TimeUnit.valueOf(job.getTimeToComplete().getUnit()));
                                } catch (Exception e) {
                                    indigoJob.setTimeUnit(TimeUnit.SECONDS);
                                }
                                queuedJobs.add(indigoJob);
                            }
                        }

                        if (deviceData != null && deviceData.getMessage() != null && deviceData.getMessage().getLocalizedMsg() != null) {
                            DeviceViewModel.Message message = new DeviceViewModel.Message();
                            message.setMsg(deviceData.getMessage().getLocalizedMsg());
                            message.setMsgID(deviceData.getMessage().getMsgId());

                            deviceViewModel.setMessage(message);
                        }

                        List<DeviceViewModel.Job> completedJobs = new ArrayList<>();
                        List<PrintVolumeData.job> completedJobsData = printVolumeData.getLastPrintedJobs();
                        if (completedJobsData != null) {
                            for (PrintVolumeData.job job : completedJobsData) {
                                DeviceViewModel.Job indigoJob = new DeviceViewModel.Job();
                                indigoJob.setName(job.getJobName());
                                completedJobs.add(indigoJob);
                            }
                        }

                        deviceViewModel.setSerialNumber(deviceData.getSerialNumber());
                        deviceViewModel.setSerialNumberDisplay(deviceData.getSerialNumberDisplay() == null ?
                                deviceData.getSerialNumber() : deviceData.getSerialNumberDisplay());

                        Map<DeviceViewModel.JobType, List<DeviceViewModel.Job>> jobs = new HashMap<>();
                        jobs.put(DeviceViewModel.JobType.QUEUED, queuedJobs);
                        jobs.put(DeviceViewModel.JobType.COMPLETED, completedJobs);
                        deviceViewModel.setJobs(jobs);

                        String lastUpdateDateUTC = printVolumeData.getLastUpdateLfp();
                        if (lastUpdateDateUTC != null) {
                            Date utcDate = HPDateUtils.parseDate(lastUpdateDateUTC, LAST_UPDATE_TIME_DATE_FORMAT, UTC_TIME_ZONE);
                            String defaultDateFormat = HPDateUtils.formatDate(utcDate, DEFAULT_DATE_FORMAT, UTC_TIME_ZONE);
                            deviceViewModel.setLastUpdateTime(HPDateUtils.parseDate(defaultDateFormat, DEFAULT_DATE_FORMAT));
                        }

                        deviceViewModel.setDeviceLastUpdateType(DeviceLastUpdateType.from(deviceViewModel.getLastUpdateTime(), businessUnitViewModel.getBusinessUnit(),
                                printVolumeData.getTimeSynced() != null && !printVolumeData.getTimeSynced()));

                        PrintVolumeData.PressState pressState = printVolumeData.getPressState();
                        boolean isUnderWarranty = printVolumeData.isUnderWarranty() == null ?
                                true : printVolumeData.isUnderWarranty().booleanValue();

                        DeviceState deviceState = pressState == null ? (businessUnitViewModel.getBusinessUnit() == BusinessUnitEnum.LATEX_PRINTER ?
                                DeviceState.from(DeviceState.NULL.getState(),
                                        DeviceState.DeviceStateColor.NEUTRAL.getColorType()) : DeviceState.UNKNOWN) :
                                DeviceState.from(pressState.getState(), pressState.getColor());

                        deviceViewModel.setDeviceState(deviceState);
                        deviceViewModel.setMaintenanceState(MaintenanceState.from(printVolumeData.getMaintenanceState()));
                        deviceViewModel.setUnderWarranty(isUnderWarranty);
                        PrintVolumeData.TimeToDone timeToDone = printVolumeData.getTimeToDone();
                        deviceViewModel.setDurationToDone(timeToDone == null ? -1 : timeToDone.getDuration());
                        deviceViewModel.setDurationToDoneUnit(timeToDone == null ? null : TimeUnit.valueOf(timeToDone.getUnit()));
                        deviceViewModel.setPrintVolumeValue(printVolumeData.getValue());
                        deviceViewModel.setName(deviceData.getDeviceName());
                        deviceViewModel.setWrongTZ(printVolumeData.getTimeSynced() != null && !printVolumeData.getTimeSynced());
                        deviceViewModel.setModel(deviceData.getPressModel());
                        deviceViewModel.setDeviceImageUrl(getDeviceImageUrlByDeviceModel(context, deviceData.getPressModel()));
                        deviceViewModel.setDeviceImageResource(DevicesUtils.getPressImage(businessUnitViewModel.getBusinessUnit(), deviceData.getPressModel()));
                        deviceViewModel.setTimeInStateMin(printVolumeData.getTimeInStateMin());
                        deviceViewModel.setImpressionType(printVolumeData.getImpressionType());

                        deviceViewModel.setPrintedJobs(printVolumeData.getPrintedJobs() == null ? -1 : (int) printVolumeData.getPrintedJobs().longValue());
                        deviceViewModel.setSheets(printVolumeData.getSheets() == null ? -1 : printVolumeData.getSheets());
                        deviceViewModel.setMeters(printVolumeData.getMeters() == null ? -1 : (int) printVolumeData.getMeters().doubleValue());
                        deviceViewModel.setMetersSquare(printVolumeData.getSquareMeters() == null ? -1 : (int) printVolumeData.getSquareMeters().doubleValue());
                        deviceViewModel.setLitersConsumed(printVolumeData.getLitersConsumed() == null ? -1 : printVolumeData.getLitersConsumed());
                        deviceViewModel.setCurrentJob(printVolumeData.getCurrentJob());
                        deviceViewModel.setCurrentJobProgress(printVolumeData.getCurrentJobProgress());

                        PrintVolumeData.TimeToDone currentJobTimeToDone = printVolumeData.getCurrentJobTimeToDone();
                        deviceViewModel.setCurrentJobTimeToDone(currentJobTimeToDone == null ? -1 : currentJobTimeToDone.getDuration());
                        deviceViewModel.setCurrentJobTimeToDoneUnit(currentJobTimeToDone == null ? null : TimeUnit.valueOf(currentJobTimeToDone.getUnit()));

                        deviceViewModel.setJobsInQueue(printVolumeData.getJobsInQueue() != null ? printVolumeData.getJobsInQueue() : 0);
                        deviceViewModel.setLastSeenMin(printVolumeData.getLastSeenMin() != null ? printVolumeData.getLastSeenMin() : 0);

                        deviceViewModel.setRTSupported(deviceData.isRtSupported());

                        if (deviceViewModel.getPrintedJobs() > -1) {
                            productionViewModel.setJobsCompleted(productionViewModel.getJobsCompleted() + deviceViewModel.getPrintedJobs());
                        }
                        productionViewModel.setJobsInQueue(productionViewModel.getJobsInQueue() + deviceViewModel.getJobsInQueue());

                        productionViewModel.setHasCompletedJobs(productionViewModel.isHasCompletedJobs() ||
                                (completedJobs != null && completedJobs.size() > 0));

                        //Match a print volume object with its corresponding object in the target data list.
                        if (currentTargetData != null) {
                            deviceViewModel.setPrintVolumeShiftTargetValue(currentTargetData.getShiftTarget().getValue());
                            deviceViewModel.setPrintVolumeIntraDailyTargetValue(currentTargetData.getIntraDailyTarget().getValue());
                        }

                        deviceViewModel.setBusinessUnitEnum(businessUnitViewModel.getBusinessUnit());

                        pressViewModels.add(deviceViewModel);
                    }

                }
            }
        }

        //If perSite.. then all the non-RT devices are already included.
        if (!perSite) {
            for (FilterItem filterItem : selectedDevices) {
                if (filterItem instanceof DeviceFilterViewModel) {
                    if (!((DeviceFilterViewModel) filterItem).isRtSupported()) {
                        DeviceViewModel deviceViewModel = new DeviceViewModel();
                        deviceViewModel.setName(filterItem.getName());
                        deviceViewModel.setRTSupported(false);
                        deviceViewModel.setDeviceState(DeviceState.UNKNOWN);
                        deviceViewModel.setSerialNumber(((DeviceFilterViewModel) filterItem).getSerialNumber());
                        deviceViewModel.setSerialNumberDisplay(((DeviceFilterViewModel) filterItem).getSerialNumberDisplay());
                        deviceViewModel.setDeviceImageResource(DevicesUtils.getPressImage(businessUnitViewModel.getBusinessUnit(), ((DeviceFilterViewModel) filterItem).getPressModel()));
                        deviceViewModel.setDeviceImageUrl(getDeviceImageUrlByDeviceModel(context, ((DeviceFilterViewModel) filterItem).getPressModel()));
                        deviceViewModel.setImpressionType(((DeviceFilterViewModel) filterItem).getImpressionType());
                        deviceViewModel.setBusinessUnitEnum(businessUnitViewModel.getBusinessUnit());

                        DeviceData deviceData = buDevices.get(deviceViewModel.getSerialNumber());
                        if (deviceData != null && deviceData.getMessage() != null && deviceData.getMessage().getLocalizedMsg() != null) {
                            DeviceViewModel.Message message = new DeviceViewModel.Message();
                            message.setMsg(deviceData.getMessage().getLocalizedMsg());
                            message.setMsgID(deviceData.getMessage().getMsgId());

                            deviceViewModel.setMessage(message);
                        }

                        pressViewModels.add(deviceViewModel);
                    }
                }
            }
        }

        Collections.sort(pressViewModels, DeviceViewModel.NAME_COMPARATOR);

        todayViewModel.setDeviceViewModels(pressViewModels);

        PrintBeatDashboardViewModel dashboardViewModel = new PrintBeatDashboardViewModel();
        todayViewModel.setSiteViewModel(siteViewModel);
        todayViewModel.setBusinessUnitViewModel(businessUnitViewModel);
        dashboardViewModel.setTodayViewModel(todayViewModel);
        dashboardViewModel.setProductionViewModel(productionViewModel);

        return dashboardViewModel;

    }

    public static Observable<List<DeviceExtraData>> getLatexDevicesStatisticsData(final BusinessUnitViewModel businessUnitViewModel) {

        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
        ConfigService configService = serviceProvider.getConfigService();

        List<String> devicesIds = businessUnitViewModel.getFiltersViewModel().getSiteDevicesIds();

        return configService.getDevicesStatisticsData(devicesIds).map(new Func1<Response<DevicesStatisticsData>, List<DeviceExtraData>>() {
            @Override
            public List<DeviceExtraData> call(Response<DevicesStatisticsData> devicesDataResponse) {
                return mapDeviceStatisticsDataToDevices(devicesDataResponse.body(), businessUnitViewModel.getFiltersViewModel().getSelectedDevices());
            }
        });

    }

    private static List<DeviceExtraData> mapDeviceStatisticsDataToDevices(DevicesStatisticsData devicesStatisticsData, List<FilterItem> filterItems) {

        List<DevicesStatisticsData.DeviceStatistics> devicesStatistics = devicesStatisticsData == null ? null : devicesStatisticsData.getDeviceStatistics();

        List<DeviceExtraData> printersViewModel = new ArrayList<>();

        for (FilterItem filterItem : filterItems) {

            DeviceFilterViewModel deviceViewModel = (DeviceFilterViewModel) filterItem;

            DeviceExtraData deviceExtraData = new DeviceExtraData();

            deviceExtraData.setSerialNumber(deviceViewModel.getSerialNumber());
            deviceExtraData.setName(deviceViewModel.getDeviceName());
            deviceExtraData.setPrinterImageUrl(getDeviceImageUrlByDeviceModel(PrintOSApplication.getAppContext(), deviceViewModel.getPressModel()));
            deviceExtraData.setState(DeviceState.UNKNOWN);

            printersViewModel.add(deviceExtraData);

            if (devicesStatistics != null) {

                for (DevicesStatisticsData.DeviceStatistics deviceStatistics : devicesStatistics) {

                    if (deviceStatistics.getDeviceId().equals(deviceViewModel.getDeviceId())) {

                        DevicesStatisticsData.DeviceStatistics.Data data = deviceStatistics.getData();

                        List<InkModel> inkModels = new ArrayList<>();
                        List<SubstrateModel> substrateModels = new ArrayList<>();
                        List<JobModel> futureJobs = new ArrayList<>();
                        List<JobModel> printingJobs = new ArrayList<>();
                        List<JobModel> printedJobs = new ArrayList<>();

                        if (data != null) {

                            GeneralData generalData = data.getGeneralData();

                            if (generalData != null && generalData.getPrinterData() != null) {

                                PrinterData printerData = generalData.getPrinterData().getData();
                                PrinterData.PrinterInfo info = printerData == null ? null : printerData.getPrinterInfo();
                                Notifications notifications = printerData == null ? null : printerData.getNotifications();

                                if (info != null) {
                                    deviceExtraData.setState(DeviceState.from(info.getDeviceStatus(),
                                            DeviceState.DeviceStateColor.NEUTRAL.getColorType()));
                                    deviceExtraData.setModel(info.getPrinterModel());
                                    deviceExtraData.setMessage(info.getStatusMessage());

                                    //different formats returned
                                    Date updateDate = HPDateUtils.parseDate(info.getLastUpdateAt(),
                                            LAST_UPDATED_TIME_DATE_FORMAT_V1);
                                    updateDate = updateDate != null ? updateDate : HPDateUtils.parseDate(info.getLastUpdateAt(),
                                            LAST_UPDATED_TIME_DATE_FORMAT_V2);
                                    deviceExtraData.setLastUpdatedAt(updateDate);
                                }

                                if (notifications != null) {
                                    deviceExtraData.setFrontPanelMessage(notifications.getFrontPanelMessage());
                                }
                            }

                            if (generalData != null && generalData.getHeaders() != null) {
                                GeneralData.Headers headers = generalData.getHeaders();
                                deviceExtraData.setFirmwareVersion(headers.getPrinterFirmwareVersion());
                            }

                            DevicesStatisticsData.InkData inkData = data.getInkData();
                            List<DevicesStatisticsData.InkData.InkInfo> inkInfoList = inkData == null ? null : inkData.getInkInfo();
                            if (inkInfoList != null) {
                                for (DevicesStatisticsData.InkData.InkInfo inkInfo : inkInfoList) {
                                    InkModel inkModel = new InkModel();
                                    inkModel.setInkEnum(InkModel.InkEnum.from(inkInfo.getConsumableLabelCode()));
                                    inkModel.setInkState(InkModel.InkState.from(inkInfo.getMeasuredQuantityState()));
                                    try {
                                        inkModel.setLevel((int) Double.parseDouble(inkInfo.getLevel()));
                                        inkModel.setCapacity((int) Double.parseDouble(inkInfo.getMaxCapacity()));
                                    } catch (Exception e) {
                                        HPLogger.d(TAG, " error while reading inks");
                                    }
                                    inkModel.setExpiryDate(inkInfo.getExpirationDate());
                                    inkModels.add(inkModel);
                                }
                            }

                            DevicesStatisticsData.SubstrateData substrateData = data.getSubstrateData();
                            List<DevicesStatisticsData.SubstrateData.Substrate> substrateList = substrateData == null ? null : substrateData.getSubstrates();
                            if (substrateList != null) {
                                for (DevicesStatisticsData.SubstrateData.Substrate substrate : substrateList) {
                                    SubstrateModel substrateModel = new SubstrateModel();

                                    substrateModel.setMediaName(substrate.getMediaName());
                                    substrateModel.setUnit(SubstrateModel.SubstrateUnit.from(substrate.getUnit()));

                                    try {
                                        substrateModel.setMediaCounter((float) Double.parseDouble(substrate.getSubstrateCounter()));
                                    } catch (Exception e) {
                                        substrateModel.setMediaCounter(-1f);
                                    }

                                    try {
                                        substrateModel.setMediaWidth((float) Double.parseDouble(substrate.getMediaWidth()));
                                    } catch (Exception e) {
                                        substrateModel.setMediaWidth(-1f);
                                    }

                                    substrateModel.setRemainingLength(substrate.getRemainingLength());

                                    substrateModels.add(substrateModel);
                                }
                            }

                            DevicesStatisticsData.JobData jobData = data.getJobData();
                            JobsData jobs = jobData == null ? null : jobData.getJobs();
                            List<JobsData.FutureJob> futureJobData = jobs == null ? null : jobs.getFuture();
                            List<JobsData.FutureJob> presentJobData = jobs == null ? null : jobs.getPrintingJobs();
                            List<JobsData.HistoryJob> historyJobData = jobs == null ? null : jobs.getHistory();

                            if (futureJobData != null) {
                                for (JobsData.FutureJob futureJob : futureJobData) {
                                    JobModel futureJobModel = new JobModel();
                                    futureJobModel.setJobName(futureJob.getFileName());
                                    futureJobModel.setJobImageUrl(futureJob.getThumbnailUrl());
                                    futureJobModel.setProgress(futureJob.getProgress());
                                    futureJobModel.setEndState(JobModel.EndState.from(futureJob.getJobState()));
                                    futureJobs.add(futureJobModel);
                                }
                            }
                            if (presentJobData != null) {
                                for (JobsData.FutureJob printingJob : presentJobData) {
                                    JobModel jobModel = new JobModel();
                                    jobModel.setJobName(printingJob.getFileName());
                                    jobModel.setJobImageUrl(printingJob.getThumbnailUrl());
                                    jobModel.setProgress(printingJob.getProgress());
                                    jobModel.setEndState(JobModel.EndState.from(printingJob.getJobState()));
                                    printingJobs.add(jobModel);
                                }
                            }
                            if (historyJobData != null) {
                                for (JobsData.HistoryJob historyJob : historyJobData) {
                                    JobModel jobModel = new JobModel();
                                    jobModel.setJobName(historyJob.getFileName());
                                    jobModel.setFinishTime(HPDateUtils.parseDate(historyJob.getCompletionTime(), DATA_FORMAT_LATEX_JOB_COMPLETION));
                                    jobModel.setEndState(JobModel.EndState.from(historyJob.getEndState()));
                                    printedJobs.add(jobModel);
                                }
                            }
                        }

                        Collections.sort(inkModels, InkModel.COMPARATOR);
                        deviceExtraData.setInkList(inkModels);
                        deviceExtraData.setSubstrateList(substrateModels);
                        deviceExtraData.setFutureJobs(futureJobs);
                        deviceExtraData.setPrintedJobs(printedJobs);
                        deviceExtraData.setPrintingJobs(printingJobs);

                        break;
                    }
                }
            }

        }

        return printersViewModel;
    }

    public static Observable<ServiceCallViewModel> getServiceCallData(final BusinessUnitViewModel businessUnitViewModel) {
        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
        ServiceCallService serviceCallService = serviceProvider.getServiceCallSerivce();

        List<String> serialNumbers = businessUnitViewModel.getFiltersViewModel().getSerialNumbers();
        final int closedDaysBack = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).closedServiceCallsDays(CLOSED_SERVICE_CALLS_DAYS_DEFAULT);
        return serviceCallService.getServiceCalls(serialNumbers, closedDaysBack).map(new Func1<Response<ServiceCallData>, ServiceCallViewModel>() {
            @Override
            public ServiceCallViewModel call(Response<ServiceCallData> serviceCallDataResponse) {
                if (serviceCallDataResponse != null && serviceCallDataResponse.isSuccessful()) {
                    ServiceCallViewModel serviceCallViewModel = parseServiceCallData(serviceCallDataResponse.body(), businessUnitViewModel);
                    serviceCallViewModel.setClosedServiceCallsDays(closedDaysBack);
                    return serviceCallViewModel;
                }
                return null;
            }
        });
    }

    private static ServiceCallViewModel parseServiceCallData(ServiceCallData serviceCallData, BusinessUnitViewModel businessUnitViewModel) {
        if (serviceCallData == null || serviceCallData.getCallsDataList() == null || businessUnitViewModel == null
                || businessUnitViewModel.getDevices() == null) {
            return null;
        }

        Map<String, DeviceData> deviceViewModels = businessUnitViewModel.getDevices();
        List<ServiceCallData.CallsData> callsData = serviceCallData.getCallsDataList();

        List<ServiceCallViewModel.CallViewModel> callViewModels = new LinkedList<>();
        int numberOfOpenCalls = 0;
        int numberOfClosedCalls = 0;
        ServiceCallViewModel serviceCallViewModel = new ServiceCallViewModel();

        for (int i = 0; i < callsData.size(); i++) {
            ServiceCallData.CallsData call = callsData.get(i);

            ServiceCallViewModel.CallViewModel callViewModel = new ServiceCallViewModel.CallViewModel();
            callViewModel.setState(ServiceCallViewModel.ServiceCallStateEnum.from(call.getStatus()));
            callViewModel.setOpenedBy(call.getCallerName());
            callViewModel.setId(call.getCallId());
            callViewModel.setType(call.getIssueType());
            callViewModel.setDescription(call.getDescription());

            if (call.getOpenDate() != null) {
                callViewModel.setDateOpened(HPDateUtils.parseDate(call.getOpenDate(), SERVICE_CALL_DATE_FORMAT));
            }

            if (call.getCloseDate() != null) {
                callViewModel.setDateClosed(HPDateUtils.parseDate(call.getCloseDate(), SERVICE_CALL_DATE_FORMAT));
            }

            if (call.getSerialNumber() != null) {
                callViewModel.setPressSerialNumber(call.getSerialNumber());
                if (deviceViewModels.containsKey(call.getSerialNumber())) {
                    DeviceData deviceData = deviceViewModels.get(call.getSerialNumber());
                    callViewModel.setPressName(deviceData.getDeviceName());
                    callViewModel.setImpressionType(deviceData.getImpressionType());
                }

            }

            callViewModels.add(callViewModel);
            switch (callViewModel.getState()) {
                case OPEN:
                    numberOfOpenCalls += 1;
                    break;
                case CANCELED:
                case CLOSED:
                    numberOfClosedCalls += 1;
                    break;
            }
        }

        Collections.sort(callViewModels, ServiceCallViewModel.SERVICE_CALL_COMPARATOR);
        serviceCallViewModel.setCallViewModels(callViewModels);
        serviceCallViewModel.setNumberOfClosedCalls(numberOfClosedCalls);
        serviceCallViewModel.setNumberOfOpenCalls(numberOfOpenCalls);

        return serviceCallViewModel;
    }

    /**
     * Returns an observable object to get a list of week view models given a list of devices.
     */
    public static Observable<WeekCollectionViewModel> getMultipleWeekData(final Context context, final BusinessUnitViewModel businessUnitViewModel) {

        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
        PerformanceTrackingService performanceTrackingService = serviceProvider.getPerformanceTrackingService();
        MetaDataService metaDataService = serviceProvider.getMetaDataService();

        List<String> serialNumbers = businessUnitViewModel.getFiltersViewModel().getSerialNumbers();

        PrintOSPreferences printOSPreferences = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext());

        if (printOSPreferences.supportV2()) {

            PreferencesData.UnitSystem unitSystem = PrintOSPreferences.getInstance(context).getUnitSystem();

            return Observable.combineLatest(metaDataService.getMetaData(),
                    performanceTrackingService.getWeeklyReport(businessUnitViewModel.getBusinessUnit().getName(), serialNumbers, NUMBER_OF_WEEKS, null, unitSystem.name(), businessUnitViewModel.getFiltersViewModel().getSelectedSite().getId(), PrintOSPreferences.getInstance(context).getLanguage()),
                    new Func2<Response<Map<String, BusinessUnit>>, Response<WeeklyDataV2>, WeekCollectionViewModel>() {
                        @Override
                        public WeekCollectionViewModel call(Response<Map<String, BusinessUnit>> businessUnitsMapResponse, Response<WeeklyDataV2> weekDataListResponse) {
                            if (businessUnitsMapResponse.isSuccessful() && weekDataListResponse.isSuccessful()) {
                                if (weekDataListResponse.body() != null && weekDataListResponse.body().getData() != null) {
                                    return parseWeekData(context, weekDataListResponse.body().getData(), businessUnitViewModel, businessUnitsMapResponse.body().get(businessUnitViewModel.getBusinessUnit().getName()));
                                }

                                return parseWeekData(context, new ArrayList<WeekData>(), businessUnitViewModel, businessUnitsMapResponse.body().get(businessUnitViewModel.getBusinessUnit().getName()));
                            }

                            return null;
                        }
                    });
        } else {
            return Observable.combineLatest(metaDataService.getMetaData(),
                    performanceTrackingService.getWeeklyReport(businessUnitViewModel.getBusinessUnit().getName(), serialNumbers, NUMBER_OF_WEEKS, null),
                    new Func2<Response<Map<String, BusinessUnit>>, Response<ResponseBody>, WeekCollectionViewModel>() {
                        @Override
                        public WeekCollectionViewModel call(Response<Map<String, BusinessUnit>> businessUnitsMapResponse, Response<ResponseBody> weekDataListResponse) {
                            if (businessUnitsMapResponse.isSuccessful() && weekDataListResponse.isSuccessful()) {
                                try {
                                    ObjectMapper objectMapper = new ObjectMapper();
                                    List<WeekData> weekDatas = Arrays.asList(objectMapper.readValue(weekDataListResponse.body().string(), WeekData[].class));
                                    return parseWeekData(context, weekDatas, businessUnitViewModel, businessUnitsMapResponse.body().get(businessUnitViewModel.getBusinessUnit().getName()));
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                                return parseWeekData(context, new ArrayList<WeekData>(), businessUnitViewModel, businessUnitsMapResponse.body().get(businessUnitViewModel.getBusinessUnit().getName()));
                            }
                            return null;
                        }
                    });
        }
    }

    /**
     * Converts a list of weekData to a WeekCollectionViewModel.
     */
    private static WeekCollectionViewModel parseWeekData(Context context, List<WeekData> weeks, BusinessUnitViewModel businessUnitViewModel, BusinessUnit metaData) {

        HashMap<String, BusinessUnit.Kpi> kpiMetaData = metaData.getKpiMetaDataMap();
        BusinessUnitEnum businessUnitEnum = BusinessUnitEnum.from(metaData.getBusinessUnit());

        int index = 0;
        boolean isRankingEnabled = businessUnitEnum == BusinessUnitEnum.INDIGO_PRESS;

        VersionsData versionsData = PrintOSPreferences.getInstance(context).getVersionsData();

        if (versionsData != null && versionsData.getConfiguration() != null &&
                versionsData.getConfiguration().getWeeklyPrintbeat() != null) {

            VersionsData.Configuration.WeeklyPrintbeat.RankingFeature rankingFeature
                    = versionsData.getConfiguration().getWeeklyPrintbeat().getRankingFeature();

            isRankingEnabled = rankingFeature.isEnabled() && rankingFeature.getSupportedEquipments().contains(businessUnitEnum.getName());
        } else {
            isRankingEnabled = false;
        }

        if (isRankingEnabled && businessUnitViewModel.getFiltersViewModel() != null) {

            boolean devicesBelongToGroup = false;

            List<FilterItem> groups = businessUnitViewModel.getFiltersViewModel().getGroups();

            if (groups != null) {
                List<String> selectedDevices = businessUnitViewModel.getFiltersViewModel().getSerialNumbers();
                for (FilterItem group : groups) {

                    devicesBelongToGroup = true;

                    List<String> groupSerialNumbers = businessUnitViewModel.getFiltersViewModel().getSerialNumbers(group.getItems());

                    if (groupSerialNumbers.size() == selectedDevices.size()) {
                        for (String serialNumber : selectedDevices) {
                            if (serialNumber != null) {
                                devicesBelongToGroup = devicesBelongToGroup && groupSerialNumbers.contains(serialNumber);
                            }
                        }
                    } else {
                        devicesBelongToGroup = false;
                    }

                    if (devicesBelongToGroup) {
                        break;
                    }

                }
            }

            isRankingEnabled = !devicesBelongToGroup;
        }

        boolean someWeeksHasRanking = false;

        List<WeekViewModel> listWeekViewModel = new ArrayList<>();
        for (WeekData weekData : weeks) {
            WeekViewModel weekViewModel = new WeekViewModel();

            weekViewModel.setPerformance(KPIScoreStateEnum.from(weekData.getKpiScoreState()));
            weekViewModel.setRelativeIndexToLastWeek(index);
            index += 1;

            weekViewModel.setWeekScore(weekData.getWeeklyPoints());
            weekViewModel.setWeekMaxScore(100);//weekData.getTotalPoints() == 0 ? 100 : weekData.getTotalPoints());
            weekViewModel.setFromDate(HPDateUtils.parseDate(weekData.getUiStartDate(), WEEK_DATE_FORMAT));
            weekViewModel.setToDate(HPDateUtils.parseDate(weekData.getUiEndDate(), WEEK_DATE_FORMAT));
            weekViewModel.setWeekNumber(HPDateUtils.getWeekOfYear(weekData.getUiStartDate(), WEEK_DATE_FORMAT));

            List<KPIViewModel> kpis = new ArrayList<>();

            for (WeekData.Scorable kpi : weekData.getScorables()) {

                KPIViewModel kpiViewModel = new KPIViewModel();

                kpiViewModel.setName(kpi.getName());
                kpiViewModel.setLocalizedName(ReportKpiEnum.from(context, kpi.getName(), BusinessUnitEnum.from(metaData.getBusinessUnit()))
                        .getLocalizedNameString(context, kpi.getName()));
                kpiViewModel.setScore(kpi.getScore());
                kpiViewModel.setValue(kpi.getValue());
                kpiViewModel.setScoreTrend(KPIScoreTrendEnum.from(kpi.getKpiScoreTrend()));
                kpiViewModel.setScoreState(KPIScoreStateEnum.from(kpi.getKpiScoreState()));
                kpiViewModel.setCustomText(kpi.getCustomText());
                kpiViewModel.setKpiEnum(KpiExplanationEnum.from(context, kpi.getName(), BusinessUnitEnum.from(metaData.getBusinessUnit())));

                BusinessUnit.Kpi kpiData;

                if (kpiMetaData.containsKey(kpi.getName())) {

                    kpiData = kpiMetaData.get(kpi.getName());

                    if (kpiData != null) {
                        kpiViewModel.setUnit(KPIValueHandleEnum.from(businessUnitEnum == BusinessUnitEnum.SCITEX_PRESS
                                || businessUnitEnum == BusinessUnitEnum.LATEX_PRINTER ? kpiData.getText() : kpiData.getValueHandle()));
                        kpiViewModel.setMaxScore(kpiData.getScoreWeight());
                        kpiViewModel.setKpiDrawable(ReportKpiEnum.from(context, kpiViewModel.getName(), businessUnitEnum).getWeekPanelKpiIcon());
                    }
                }

                List<WeekData.KpiExplanationItem> kpiExplanationItems = kpi.getKpiExplanationItems();
                List<KpiExplanationViewModel> kpiExplanationViewModels = new ArrayList<>();
                if (kpiExplanationItems != null && businessUnitEnum == BusinessUnitEnum.INDIGO_PRESS) {
                    for (WeekData.KpiExplanationItem item : kpiExplanationItems) {
                        KpiExplanationViewModel kpiExplanationViewModel = KpiExplanationUtils.parseKpiExplanationItemForIndigo(item, context);
                        if (kpiExplanationViewModel != null) {
                            kpiExplanationViewModels.add(kpiExplanationViewModel);
                        }
                    }
                } else if (businessUnitEnum == BusinessUnitEnum.LATEX_PRINTER) {
                    KpiExplanationViewModel kpiExplanationViewModel = KpiExplanationUtils.parseKpiExplanationItemForLatex(kpi, context, kpiViewModel.getMaxScore());
                    if (kpiExplanationViewModel != null) {
                        kpiExplanationViewModels.add(kpiExplanationViewModel);
                    }
                }
                kpiViewModel.setKpiExplanationViewModels(kpiExplanationViewModels);

                kpis.add(kpiViewModel);
            }

            if (isRankingEnabled && weekData.getRankings() != null) {

                WeekData.Rankings rankings = weekData.getRankings();

                boolean weekHasRankingData = rankings.getRegion() != null || rankings.getWorldWide() != null || rankings.getSubRegion() != null;

                someWeeksHasRanking = someWeeksHasRanking || weekHasRankingData;

                weekViewModel.setHasRankingData(weekHasRankingData);

                if (weekHasRankingData) {

                    RankingViewModel rankingViewModel = new RankingViewModel();

                    rankingViewModel.setSubRegionViewModel(getRankAreaViewModel(context, RankingViewModel.RankAreaType.SUB_REGION, rankings.getSubRegion()));
                    rankingViewModel.setHasSubRegionSection(rankingViewModel.getSubRegionViewModel() != null);

                    rankingViewModel.setRegionViewModel(getRankAreaViewModel(context, RankingViewModel.RankAreaType.REGION, rankings.getRegion()));
                    rankingViewModel.setHasRegionSection(rankingViewModel.getRegionViewModel() != null);

                    rankingViewModel.setWorldWideViewModel(getRankAreaViewModel(context, RankingViewModel.RankAreaType.WORLD_WIDE, rankings.getWorldWide()));
                    rankingViewModel.setHasWorldWideSection(rankingViewModel.getWorldWideViewModel() != null);

                    boolean showTrophy = true;
                    boolean worldWideTop5 = false;
                    String topFiveInRegion = null;
                    if (rankingViewModel.hasWorldWideSection() && rankingViewModel.getWorldWideViewModel().getActualRank() <= 5) {
                        topFiveInRegion = context.getString(R.string.weekly_printbeat_site_weekly_rank_top_5_in_the_world);
                        worldWideTop5 = true;
                    } else if (rankingViewModel.hasRegionSection() && rankingViewModel.getRegionViewModel().getActualRank() <= 5) {
                        topFiveInRegion = context.getString(R.string.weekly_printbeat_site_weekly_rank_top_5_in_region, rankingViewModel.getRegionViewModel().getName());
                    } else if (rankingViewModel.hasSubRegionSection() && rankingViewModel.getSubRegionViewModel().getActualRank() <= 5) {
                        topFiveInRegion = context.getString(R.string.weekly_printbeat_site_weekly_rank_top_5_in_sub_region, rankingViewModel.getSubRegionViewModel().getName());
                    } else {
                        showTrophy = false;
                    }

                    rankingViewModel.setShowTrophy(showTrophy);
                    rankingViewModel.setWorldWideTop5(worldWideTop5);
                    rankingViewModel.setTop5RankingRegionMessage(topFiveInRegion);

                    weekViewModel.setRankingViewModel(rankingViewModel);

                }

            }

            weekViewModel.setKpis(kpis);

            List<WeekViewModel.WeekPressStatus> weekPressStatuses = new ArrayList<>();
            HashMap<String, WeekData.PressStatus> pressStatusHashMap = weekData.getPressesStatus();
            Map<String, DeviceData> deviceViewModelMap = businessUnitViewModel.getDevices();
            if (pressStatusHashMap != null && businessUnitEnum == BusinessUnitEnum.INDIGO_PRESS &&
                    PrintOSPreferences.getInstance(context).isWeekIncrementalFeatureEnabled()) {
                for (String serialNumber : pressStatusHashMap.keySet()) {
                    WeekData.PressStatus pressStatus = pressStatusHashMap.get(serialNumber);
                    WeekViewModel.WeekPressStatus weekPressStatus = new WeekViewModel.WeekPressStatus();
                    weekPressStatus.setSerialNumber(serialNumber);
                    if (deviceViewModelMap != null && deviceViewModelMap.containsKey(serialNumber)) {
                        weekPressStatus.setPressName(deviceViewModelMap.get(serialNumber).getDeviceName());
                        weekPressStatus.setPressType(deviceViewModelMap.get(serialNumber).getImpressionType());
                    }
                    weekPressStatus.setDateReady(HPDateUtils.parseDate(pressStatus.getReadyDate(),
                            DATA_FORMAT_ACTUAL_VS_TARGET));
                    weekPressStatus.setWeeklyPressStatusEnum(WeeklyPressStatusEnum.from(
                            pressStatus.getStatus()
                    ));
                    weekPressStatuses.add(weekPressStatus);
                }
            }
            weekViewModel.setWeekPressStatuses(weekPressStatuses);

            listWeekViewModel.add(weekViewModel);
        }

        Collections.sort(listWeekViewModel, WeekViewModel.DATE_COMPARATOR);

        WeekCollectionViewModel weekCollectionViewModel = new WeekCollectionViewModel();
        weekCollectionViewModel.setWeeks(listWeekViewModel);
        weekCollectionViewModel.setShowRanking(someWeeksHasRanking);
        weekCollectionViewModel.setBusinessUnitEnum(businessUnitViewModel.getBusinessUnit());

        return weekCollectionViewModel;
    }

    private static RankingViewModel.AreaViewModel getRankAreaViewModel(Context context, RankingViewModel.RankAreaType rankAreaType, WeekData.Rankings.Area region) {

        if (region == null) {
            return null;
        }

        RankingViewModel.AreaViewModel areaViewModel = new RankingViewModel.AreaViewModel();
        areaViewModel.setRankAreaType(rankAreaType);
        areaViewModel.setTitle(context.getString(rankAreaType.getTitleResourceId()));
        areaViewModel.setActualRank(region.getPosition());
        areaViewModel.setTotal(region.getTotal());
        int offset = region.getOffset();
        areaViewModel.setTrend(offset > 0 ? RankingViewModel.Trend.UP : (offset < 0
                ? RankingViewModel.Trend.DOWN : RankingViewModel.Trend.EQUALS));
        areaViewModel.setOffset(offset);
        areaViewModel.setName(region.getName());
        areaViewModel.setMessage(region.isLowerThanThreshold() ?
                context.getString(R.string.weekly_printbeat_site_weekly_rank_lower_third) : null);
        areaViewModel.setLowerThird(region.isLowerThanThreshold());

        return areaViewModel;

    }

    public static Observable<Map<BusinessUnitEnum, BusinessUnitViewModel>> initBusinessUnits(final Context context) {

        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();

        ConfigService configService = serviceProvider.getConfigService();
        MetaDataService metaDataService = serviceProvider.getMetaDataService();
        PreferencesService preferencesService = serviceProvider.getPreferencesService();

        return Observable.combineLatest(preferencesService.getPreferences(), configService.getOrganizationsDevices(), configService.getConfigDevices(
                PrintOSPreferences.getInstance(context).getLanguageCode()
        ), metaDataService.getMetaData(), new Func4<Response<PreferencesData>, Response<DevicesData>, Response<List<DeviceData>>, Response<Map<String, BusinessUnit>>, Map<BusinessUnitEnum, BusinessUnitViewModel>>() {
            @Override
            public Map<BusinessUnitEnum, BusinessUnitViewModel> call(Response<PreferencesData> preferencesDataResponse, Response<DevicesData> devicesDataResponse, Response<List<DeviceData>> listResponse, Response<Map<String, BusinessUnit>> businessUnitsMapResponse) {

                PreferencesData.UnitSystem unitSystem = null;
                if (preferencesDataResponse != null && preferencesDataResponse.body() != null) {
                    PreferencesData.General general = preferencesDataResponse.body().getGeneral();
                    if (general != null) {
                        unitSystem = general.getUnitSystem();
                        PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).saveUnitSystem(general.getUnitSystem());
                    }
                }

                Map<BusinessUnitEnum, BusinessUnitViewModel> businessUnitMap = mapDevicesToBusinessUnits(devicesDataResponse.body().getDevices(), listResponse.body(), unitSystem);
                mapKPIToBusinessUnits(context, businessUnitMap, businessUnitsMapResponse.body());

                return businessUnitMap;
            }
        });
    }


    private static void mapKPIToBusinessUnits(Context context, Map<BusinessUnitEnum, BusinessUnitViewModel> businessUnitMap, Map<String, BusinessUnit> businessUnitResponse) {

        if (businessUnitResponse == null) {
            return;
        }

        for (String businessUnitKey : businessUnitResponse.keySet()) {
            BusinessUnitEnum businessUnitEnum = BusinessUnitEnum.from(businessUnitKey);
            if (businessUnitMap.containsKey(businessUnitEnum)) {
                BusinessUnitViewModel businessUnitViewModel = businessUnitMap.get(businessUnitEnum);

                BusinessUnit businessUnit = businessUnitResponse.get(businessUnitKey);
                if (businessUnit != null) {
                    Map<String, BusinessUnit.Kpi> kpiMap = businessUnit.getKpiMetaDataMap();
                    List<BusinessUnitViewModel.BusinessUnitKpi> businessUnitKpiList = new ArrayList<>();
                    if (kpiMap != null) {
                        for (String kpiKey : kpiMap.keySet()) {
                            ReportTypeEnum reportTypeEnum = ReportTypeEnum.UNKNOWN;
                            ReportChartTypeEnum reportChartTypeEnum = ReportChartTypeEnum.UNKNOWN;
                            BusinessUnit.Kpi kpiData = kpiMap.get(kpiKey);
                            if (kpiData != null && kpiData.getDataField() != null) {
                                reportTypeEnum = ReportTypeEnum.from(kpiData.getDataField());
                                reportChartTypeEnum = reportChartTypeEnum.from(context, kpiData.getDefaultChart(), kpiData.getName());
                            }
                            BusinessUnitViewModel.BusinessUnitKpi kpiUnit = new BusinessUnitViewModel.BusinessUnitKpi();
                            kpiUnit.setReportKpiEnum(kpiKey);
                            kpiUnit.setReportTypeEnum(reportTypeEnum);
                            kpiUnit.setReportChartTypeEnum(reportChartTypeEnum);
                            businessUnitKpiList.add(kpiUnit);
                        }
                    }
                    businessUnitViewModel.setBusinessUnitKpi(businessUnitKpiList);
                }
            }
        }
    }

    private static Map<BusinessUnitEnum, BusinessUnitViewModel> mapDevicesToBusinessUnits(List<DevicesData.DeviceData> organizationsDevices, List<DeviceData> devicesData, PreferencesData.UnitSystem unitSystem) {

        Map<BusinessUnitEnum, BusinessUnitViewModel> businessUnitsDevicesMap = new HashMap<>();

        for (int deviceIndex = 0; deviceIndex < devicesData.size(); deviceIndex++) {

            DeviceData deviceData = devicesData.get(deviceIndex);

            BusinessUnitEnum businessUnit = BusinessUnitEnum.from(deviceData.getBusinessUnit());

            // Work only with the supported business units.
            if (businessUnit == BusinessUnitEnum.UNKNOWN) {
                continue;
            }

            for (DevicesData.DeviceData organizationDevice : organizationsDevices) {

                String deviceSerialNumber = deviceData.getSerialNumber();
                deviceSerialNumber = deviceSerialNumber == null ? "" : deviceSerialNumber;

                String[] splitSerialNumber = deviceSerialNumber.split("!");
                String serialNumber = splitSerialNumber.length > 0 ? splitSerialNumber[0] : "";
                String productNumber = splitSerialNumber.length > 1 ? splitSerialNumber[1] : null;

                boolean sameSerialNumber = serialNumber.equals(organizationDevice.getSerialNumber());
                boolean sameProductNumber = productNumber == null || productNumber.equals(organizationDevice.getModel());

                if (sameSerialNumber && sameProductNumber) {
                    deviceData.setDeviceId(organizationDevice.getDeviceId());
                    //Server bug
                    //deviceData.setPressModel(organizationDevice.getModel());
                    break;
                }
            }

            BusinessUnitViewModel businessUnitViewModel;

            if (!businessUnitsDevicesMap.containsKey(businessUnit)) {

                businessUnitViewModel = new BusinessUnitViewModel();
                businessUnitViewModel.setBusinessUnit(businessUnit);
                businessUnitViewModel.setDevices(new LinkedHashMap<String, DeviceData>());
                businessUnitsDevicesMap.put(businessUnit, businessUnitViewModel);

            } else {
                businessUnitViewModel = businessUnitsDevicesMap.get(businessUnit);
            }

            businessUnitViewModel.getDevices().put(deviceData.getSerialNumber(), deviceData);

            List<Panel> panels;

            switch (businessUnitViewModel.getBusinessUnit()) {

                case INDIGO_PRESS:
                    panels = Arrays.asList(Panel.INDIGO_PRODUCTION, Panel.TODAY, Panel.PERFORMANCE, Panel.PANEL_SERVICE_CALL, Panel.PERSONAL_ADVISOR);
                    break;
                case LATEX_PRINTER:
                    panels = Arrays.asList(Panel.LATEX_PRODUCTION, Panel.TODAY, Panel.PERFORMANCE);
                    break;
                case IHPS_PRESS:
                    panels = Arrays.asList(Panel.PRINTERS, Panel.PERFORMANCE);
                    break;
                case SCITEX_PRESS:
                    panels = Arrays.asList(Panel.TODAY, Panel.PERFORMANCE);
                    break;
                default:
                    panels = new ArrayList<>();
                    break;
            }

            businessUnitViewModel.setPanels(panels);
        }

        for (BusinessUnitViewModel businessUnitViewModel : businessUnitsDevicesMap.values()) {

            List<DeviceData> deviceDataList = new LinkedList<>();
            for (String deviceKey : businessUnitViewModel.getDevices().keySet()) {
                deviceDataList.add(businessUnitViewModel.getDevices().get(deviceKey));
            }

            FiltersViewModel filtersViewModel = new FiltersViewModel();
            filtersViewModel.initFiltersViewModel(PrintOSApplication.getAppContext(), deviceDataList);
            businessUnitViewModel.setFiltersViewModel(filtersViewModel);
        }

        return businessUnitsDevicesMap;
    }

    private static String getDeviceImageUrlByDeviceModel(Context context, String deviceModel) {

        if (deviceModel == null || deviceModel.isEmpty()) {
            return "";
        }

        String imageName = deviceModel.replaceAll(" ", "_");

        return String.format(PrintOSPreferences.getInstance(context).getServerUrl(context) +
                ApiConstants.DEVICES_IMAGES_URL, imageName);
    }

    public static Observable<ResponseBody> likeAdvice(int adviceId, String device, boolean isLiked) {
        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
        PersonalAdvisorService personalAdvisorService = serviceProvider.getPersonalAdvisorService();
        return personalAdvisorService.likeAdvice(String.valueOf(adviceId), device, isLiked).asObservable();
    }

    public static Observable<ResponseBody> suppressAdvice(int adviceId, String device) {
        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
        PersonalAdvisorService personalAdvisorService = serviceProvider.getPersonalAdvisorService();
        return personalAdvisorService.suppressAdvice(String.valueOf(adviceId), device, true).asObservable();
    }

    public static Observable<ResponseBody> unhideAdvices(List<String> serialNumbers) {
        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
        PersonalAdvisorService personalAdvisorService = serviceProvider.getPersonalAdvisorService();
        return personalAdvisorService.unhideAllAdvices(serialNumbers, false).asObservable();
    }

    public static Observable<ResponseBody> saveLastSelectedSite(String siteId) {
        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
        UserDataService userDataService = serviceProvider.getUserDataService();
        return userDataService.saveLastSite(new LastSiteData.LastSite(siteId, "lastSite", "0.1")).asObservable();
    }


    public static Observable<BeatCoinsViewModel> getBeatCoins(String id) {
        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
        BeatCoinService beatCoinService = serviceProvider.getBeatCoinService();
        return beatCoinService.getBeatCoinsDetails(id).map(new Func1<Response<BeatCoinData>, BeatCoinsViewModel>() {
            @Override
            public BeatCoinsViewModel call(Response<BeatCoinData> beatCoinDataResponse) {
                if (beatCoinDataResponse != null && beatCoinDataResponse.isSuccessful() && beatCoinDataResponse.body() != null) {
                    return parseBeatCoinViewModel(beatCoinDataResponse.body());
                }
                return null;
            }
        });
    }

    private static BeatCoinsViewModel parseBeatCoinViewModel(BeatCoinData beatCoinData) {
        if (beatCoinData.getCoins() <= 0 || beatCoinData.getCoinsURL() == null ||
                beatCoinData.getCoinsURLExpirationDate() == null) {
            HPLogger.d(TAG, "getBeatCoins returned corrupted response");
            return null;
        }
        BeatCoinsViewModel viewModel = new BeatCoinsViewModel();
        viewModel.setNumberOfBeatCoins(beatCoinData.getCoins());
        viewModel.setUrl(beatCoinData.getCoinsURL());
        if (beatCoinData.getCoinsURLExpirationDate() != null) {
            Date expiryDate = HPDateUtils.parseDate(beatCoinData.getCoinsURLExpirationDate(), DATE_FORMAT_BEAT_COIN);
            if (expiryDate == null || HPDateUtils.isExpired(expiryDate)) {
                return null;
            }
            viewModel.setNumberOfDaysTillExpiry(HPDateUtils.getNumberOfDaysFromToday(expiryDate));
        }
        return viewModel;
    }
}