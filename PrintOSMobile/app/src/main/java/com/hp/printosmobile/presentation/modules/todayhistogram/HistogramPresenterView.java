package com.hp.printosmobile.presentation.modules.todayhistogram;

import com.hp.printosmobilelib.core.communications.remote.APIException;

/**
 * Created by anwar asbah on 5/3/2017.
 */
 public interface HistogramPresenterView {
    void updateHistogram(TodayHistogramViewModel actualVsTargetViewModel);

    void onRequestError(APIException exception);
}
