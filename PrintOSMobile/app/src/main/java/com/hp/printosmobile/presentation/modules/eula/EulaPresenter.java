package com.hp.printosmobile.presentation.modules.eula;

import com.hp.printosmobilelib.core.logging.HPLogger;

import com.hp.printosmobile.presentation.Presenter;
import com.hp.printosmobilelib.core.communications.remote.APIException;

import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Anwar Asbah on 9/1/2016.
 */
public class EulaPresenter extends Presenter<EulaView> {

    private static final String TAG = EulaPresenter.class.getSimpleName();

    public void getEula(String language) {

        Subscription subscription = EulaDataManager.getEulaText(language)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<EULAViewModel>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        HPLogger.d(TAG, "Error while fetching Eula " + e);
                        onRequestError(e, EulaActivity.TAG);
                    }

                    @Override
                    public void onNext(EULAViewModel eulaViewModel) {
                        mView.displayEula(eulaViewModel);
                    }
                });
        addSubscriber(subscription);
    }

    private void onRequestError(Throwable e, String tag) {
        APIException exception = APIException.create(APIException.Kind.UNEXPECTED);
        if (e instanceof APIException) {
            exception = (APIException) e;
        }

        mView.onError(exception, tag);
    }

    @Override
    public void detachView() {
        stopSubscribers();
        mView = null;
    }

}
