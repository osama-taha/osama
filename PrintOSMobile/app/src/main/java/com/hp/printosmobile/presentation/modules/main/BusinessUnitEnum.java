package com.hp.printosmobile.presentation.modules.main;

import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;

/**
 * An enum represents the supported business units.
 * <p/>
 * Created by Osama Taha on 5/31/16.
 */
public enum BusinessUnitEnum {

    UNKNOWN("", "", false),
    INDIGO_PRESS("IndigoPress", "IND", true),
    LATEX_PRINTER("LatexPrinter", "LAT", true),
    IHPS_PRESS("IHPSPress", "PWP", false),
    SCITEX_PRESS("ScitexPress", "SCI", true);

    private String name;
    private String shortName;
    private boolean hasHistogram;

    BusinessUnitEnum(String name, String shortName, boolean hasHistogram) {
        this.name = name;
        this.shortName = shortName;
        this.hasHistogram = hasHistogram;
    }

    public String getName() {
        return name;
    }

    public String getShortName() {
        return shortName;
    }

    public boolean isHasHistogram() {
        return hasHistogram;
    }

    public String getBusinessUnitDisplayName() {
        switch (this) {
            case INDIGO_PRESS:
                return PrintOSApplication.getAppContext().getString(R.string.hp_indigo);
            case LATEX_PRINTER:
                return PrintOSApplication.getAppContext().getString(R.string.latex);
            case IHPS_PRESS:
                return PrintOSApplication.getAppContext().getString(R.string.hp_pwp);
            case SCITEX_PRESS:
                return PrintOSApplication.getAppContext().getString(R.string.hp_scitex);
            default:
                return null;
        }
    }

    public int getBottomTabIcon(boolean selected, int tabIndex) {

        switch (tabIndex) {
            case 0:
                return selected ? R.drawable.home_selected : R.drawable.home;
            case 1:
                return selected ? R.drawable.insights_selected : R.drawable.insights;
            case 2:
                return selected ? R.drawable.jobs_selected : R.drawable.jobs;
            case 3:
                if (this == LATEX_PRINTER) {
                    return selected ? R.drawable.latex_devices_selected : R.drawable.latex_devices;
                } else {
                    return selected ? R.drawable.devices_selected : R.drawable.devices;
                }
            case 4:
                return selected ? R.drawable.reports_selected : R.drawable.reports;
        }

        return 0;
    }

    public static BusinessUnitEnum from(String state) {
        if(state == null){
            return UNKNOWN;
        }
        for (BusinessUnitEnum businessUnitEnum : BusinessUnitEnum.values()) {
            if (businessUnitEnum.getName().equals(state))
                return businessUnitEnum;
        }
        return BusinessUnitEnum.UNKNOWN;
    }

}
