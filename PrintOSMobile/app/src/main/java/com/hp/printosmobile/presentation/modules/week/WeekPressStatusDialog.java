package com.hp.printosmobile.presentation.modules.week;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.modules.week.WeekViewModel.WeekPressStatus.WeeklyPressStatusEnum;
import com.hp.printosmobilelib.ui.utils.DividerItemDecoration;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Anwar Asbah on 7/9/2017.
 */

public class WeekPressStatusDialog extends DialogFragment {

    private static final String TAG = WeekPressStatusDialog.class.getSimpleName();
    private static final String PRESS_STATUS_ARG = "press_status_arg";

    @Bind(R.id.content_layout)
    View contentLayout;
    @Bind(R.id.press_list)
    RecyclerView pressList;
    @Bind(R.id.updated_text_view)
    TextView updateTextView;

    WeekStatusAdapter weekStatusAdapter;
    List<WeekViewModel.WeekPressStatus> weekPressStatusList;
    WeekPressStatusDialogCallback callback;

    public static WeekPressStatusDialog newInstance(ArrayList<WeekViewModel.WeekPressStatus> weekPressStatuses,
                                                    WeekPressStatusDialogCallback callback) {
        WeekPressStatusDialog dialog = new WeekPressStatusDialog();
        dialog.callback = callback;
        Bundle bundle = new Bundle();
        bundle.putSerializable(PRESS_STATUS_ARG, weekPressStatuses);
        dialog.setArguments(bundle);
        return dialog;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey(PRESS_STATUS_ARG)) {
            weekPressStatusList = (List<WeekViewModel.WeekPressStatus>) bundle.getSerializable(PRESS_STATUS_ARG);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dialog_week_press_status, container, false);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        init();
    }

    private void init() {
        weekStatusAdapter = new WeekStatusAdapter();
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        pressList.setLayoutManager(layoutManager);
        pressList.setAdapter(weekStatusAdapter);
        pressList.addItemDecoration(new DividerItemDecoration(getContext(), 1, weekStatusAdapter.getItemCount(), false));

        int total = 0;
        int updated = 0;

        for (WeekViewModel.WeekPressStatus status : weekPressStatusList) {
            total += 1;
            if (status.getWeeklyPressStatusEnum() != WeeklyPressStatusEnum.MISSING) {
                updated += 1;
            }
        }

        String updatedMsg = getString(R.string.week_press_status_msg_updated, updated, total);
        updateTextView.setText(updatedMsg);

        contentLayout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                contentLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                contentLayout.setVisibility(View.VISIBLE);
                contentLayout.setTranslationY(contentLayout.getMeasuredHeight());
                contentLayout.animate().translationY(0);
            }
        });

    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }

    @OnClick(R.id.close_button)
    public void close() {
        dismiss();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (callback != null) {
            callback.onDialogDismiss();
        }
    }

    public class WeekStatusAdapter extends RecyclerView.Adapter<WeekStatusAdapter.WeekStatusViewHolder> {

        @Override
        public WeekStatusViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            ViewGroup deviceView = (ViewGroup) inflater.inflate(R.layout.week_press_status_view, parent, false);
            WeekStatusViewHolder weekStatusViewHolder = new WeekStatusViewHolder(deviceView);
            return weekStatusViewHolder;
        }

        @Override
        public void onBindViewHolder(WeekStatusViewHolder holder, int position) {
            WeekViewModel.WeekPressStatus weekPressStatus = weekPressStatusList.get(position);
            String pressName = weekPressStatus.getPressType() == null ? weekPressStatus.getPressName()
                    : getString(R.string.printer_name_with_model, weekPressStatus.getPressName(),
                    weekPressStatus.getPressType());
            holder.pressName.setText(pressName);
            holder.daysAgo.setText(WeeklyPressStatusEnum.getMsg(getContext(), weekPressStatus.getWeeklyPressStatusEnum(),
                    weekPressStatus.getDateReady()));
            holder.statusIndicator.setImageDrawable(WeeklyPressStatusEnum.getColorDrawable(getContext(),
                    weekPressStatus.getWeeklyPressStatusEnum()));
        }

        @Override
        public int getItemCount() {
            return weekPressStatusList == null ? 0 : weekPressStatusList.size();
        }

        class WeekStatusViewHolder extends RecyclerView.ViewHolder {

            @Bind(R.id.press_name_text_view)
            TextView pressName;
            @Bind(R.id.status_indicator_image_view)
            ImageView statusIndicator;
            @Bind(R.id.days_ago_text_view)
            TextView daysAgo;

            WeekStatusViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }
        }
    }

    public interface WeekPressStatusDialogCallback {
        void onDialogDismiss();
    }
}
