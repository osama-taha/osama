package com.hp.printosmobile.presentation.modules.todayhistogram;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.hp.printosmobile.R;
import com.hp.printosmobile.utils.HPLocaleUtils;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * @author Anwar Asbah Create on 3/19/2017
 */
public class TodayBarView extends FrameLayout {

    private static final long ANIMATION_DURATION = 300;
    private static final String PERCENTAGE_STRING = "%s%%";

    @Bind(R.id.day_text_view)
    TextView dayTextView;
    @Bind(R.id.shift_text_view)
    TextView shiftTextView;
    @Bind(R.id.bar_layout)
    View barLayout;
    @Bind(R.id.progress_bar)
    View progressBar;
    @Bind(R.id.percentage_layout)
    View percentageLayout;
    @Bind(R.id.percentage_text_view)
    TextView percentageTextView;
    @Bind(R.id.target_bar)
    View targetBar;

    int maxProgressDistance;
    int minContainingDistance;
    int progress = 0;
    int target = 0;
    int percentage = 0;
    int total = 0;
    int barColor = Color.WHITE;
    boolean showTargetBar = false;

    public TodayBarView(Context context) {
        super(context);
        init();
    }

    public TodayBarView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TodayBarView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        View view = inflate(getContext(), R.layout.today_bar_view, this);

        ButterKnife.bind(this, view);

        percentageTextView.setVisibility(INVISIBLE);
        targetBar.setVisibility(INVISIBLE);

        barLayout.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                barLayout.getViewTreeObserver().removeOnPreDrawListener(this);

                maxProgressDistance = barLayout.getMeasuredHeight();
                minContainingDistance = percentageTextView.getMeasuredHeight();

                setProgress(progress, target, total, showTargetBar);

                return true;
            }
        });
    }

    public void setDay(String day, boolean isShift) {
        if (day != null) {
            shiftTextView.setVisibility(isShift ? VISIBLE : GONE);
            dayTextView.setVisibility(isShift ? GONE : VISIBLE);
            if (isShift) {
                shiftTextView.setText(day);
            } else {
                dayTextView.setText(day);
            }
        }
    }

    public void setProgress(int progress, int target, int total, boolean showTargetBar) {
        this.total = Math.max(total, 0);
        this.progress = Math.min(this.total, Math.max(progress, 0));
        this.target = Math.max(target, 0);
        this.showTargetBar = showTargetBar;

        this.percentage = this.target == 0 ? (progress == 0 ? 0 : 100) : Math.round(((float) this.progress / (float) this.target) * 100);
        percentageTextView.setText(String.format(PERCENTAGE_STRING, HPLocaleUtils.getLocalizedValue(percentage)));

        barLayout.getMeasuredHeight();

        changeProgressHeight();
    }

    public void setBarColor(int color) {
        progressBar.setBackgroundColor(color);
        shiftTextView.setTextColor(color);
        barColor = color;
    }

    public void changeProgressHeight() {
        final float progressPercentage = total == 0 ? 0 : (float) progress / (float) total;
        final int progressHeight = (int) (progressPercentage * maxProgressDistance);

        final float targetPercentage = total == 0 ? 0 : (float) target / (float) total;
        final int targetHeight = (int) (targetPercentage * maxProgressDistance);

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        if (progressBar.getLayoutParams().height == 0) {
            progressBar.getLayoutParams().height = 1;
        }

        Animation animation = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation transformation) {
                progressBar.getLayoutParams().height = (int) (progressHeight * interpolatedTime);
                progressBar.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                percentageTextView.setVisibility(INVISIBLE);
                targetBar.setVisibility(INVISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                percentageLayout.getLayoutParams().height = progressHeight < minContainingDistance ?
                        progressHeight + minContainingDistance : progressHeight;
                percentageTextView.setTextColor(progressHeight < minContainingDistance ? barColor : Color.WHITE);
                percentageLayout.requestLayout();
                percentageTextView.setVisibility(VISIBLE);

                targetBar.getLayoutParams().height = targetHeight;
                targetBar.setVisibility(showTargetBar ? VISIBLE : INVISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });

        animation.setDuration(ANIMATION_DURATION);
        progressBar.startAnimation(animation);
    }
}
