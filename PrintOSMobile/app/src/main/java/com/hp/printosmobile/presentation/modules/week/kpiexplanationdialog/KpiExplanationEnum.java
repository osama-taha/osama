package com.hp.printosmobile.presentation.modules.week.kpiexplanationdialog;

import android.content.Context;

import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;

import java.io.Serializable;

/**
 * Created by Anwar Asbah on 8/8/2017.
 */
public enum KpiExplanationEnum implements Serializable {

    BLANKET(R.string.indigo_kpi_blanket_key,
            BusinessUnitEnum.INDIGO_PRESS,
            R.string.indigo_kpi_week_blanket,
            R.drawable.blanket,
            R.drawable.kpi_explanation_blanket,
            true, R.string.kpi_explanation_bar_tag_blanket,
            R.string.kpi_explanation_bar_tag_blanket),
    PIP(R.string.indigo_kpi_pip_key,
            BusinessUnitEnum.INDIGO_PRESS,
            R.string.indigo_kpi_week_pip,
            R.drawable.pip,
            R.drawable.kpi_explanation_pip,
            true, R.string.kpi_explanation_bar_tag_pip,
            R.string.kpi_explanation_bar_tag_pip),
    SUPPLIES_LIFE_SPAN(R.string.indigo_kpi_supplies_lifespan_key,
            BusinessUnitEnum.INDIGO_PRESS,
            R.string.indigo_kpi_week_parameter_supplies,
            R.drawable.supplied_yield,
            R.drawable.kpi_explanation_supplied_yield,
            true, R.string.unknown_value,
            R.string.unknown_value),
    RESTARTS(R.string.indigo_kpi_restarts_key,
            BusinessUnitEnum.INDIGO_PRESS,
            R.string.indigo_kpi_week_restarts,
            R.drawable.restart,
            R.drawable.kpi_explanation_restart,
            true, R.string.kpi_explanation_bar_tag_restarts,
            R.string.kpi_explanation_bar_tag_restarts),
    JAMS(R.string.indigo_kpi_jams_key,
            BusinessUnitEnum.INDIGO_PRESS,
            R.string.indigo_kpi_week_jams,
            R.drawable.jam,
            R.drawable.kpi_explanation_jam,
            true, R.string.kpi_explanation_bar_tag_jams,
            R.string.kpi_explanation_bar_tag_jams),
    FAILURES(R.string.indigo_kpi_failures_key,
            BusinessUnitEnum.INDIGO_PRESS,
            R.string.indigo_kpi_week_failures,
            R.drawable.failur,
            R.drawable.kpi_explanation_failur,
            true, R.string.kpi_explanation_bar_tag_failure,
            R.string.kpi_explanation_bar_tag_failure),
    PRINT_VOLUME_INDIGO(R.string.indigo_kpi_print_volume_key,
            BusinessUnitEnum.INDIGO_PRESS,
            R.string.indigo_kpi_week_parameter_volume,
            R.drawable.printing_volume,
            R.drawable.kpi_explanation_printing_volume,
            true, R.string.kpi_explanation_bar_tag_print_volume_indigo,
            R.string.kpi_explanation_bar_tag_print_volume_indigo),
    AVAILABILITY(R.string.indigo_kpi_availability_key,
            BusinessUnitEnum.INDIGO_PRESS,
            R.string.indigo_kpi_week_parameter_availability,
            R.drawable.workweek_availability,
            R.drawable.kpi_explanation_workweek_availability,
            true, R.string.kpi_explanation_bar_tag_availability,
            R.string.kpi_explanation_bar_tag_availability),
    TECHNICAL(R.string.indigo_kpi_technical_key,
            BusinessUnitEnum.INDIGO_PRESS,
            R.string.indigo_kpi_week_parameter_technical,
            R.drawable.workweek_technical,
            R.drawable.kpi_explanation_workweek_technical,
            true, R.string.unknown_value,
            R.string.unknown_value),
    PRINT_VOLUME_LATEX(R.string.latex_kpi_print_volume_key,
            BusinessUnitEnum.LATEX_PRINTER,
            R.string.indigo_kpi_week_parameter_volume,
            R.drawable.printing_volume,
            R.drawable.kpi_explanation_printing_volume,
            true, R.string.kpi_explanation_bar_tag_print_volume_latex_ft,
            R.string.kpi_explanation_bar_tag_print_volume_latex_sqm),
    MAINTENANCE_LATEX(R.string.latex_kpi_maintenance_key,
            BusinessUnitEnum.LATEX_PRINTER,
            R.string.indigo_kpi_week_parameter_maitanance,
            R.drawable.workweek_maintenance,
            R.drawable.kpi_explanation_workweek_maintenance,
            false, R.string.unknown_value,
            R.string.unknown_value),
    UTILIZATION_LATEX(R.string.latex_kpi_utilization_key,
            BusinessUnitEnum.LATEX_PRINTER,
            R.string.latex_kpi_week_utilization,
            R.drawable.maintenance_score,
            R.drawable.kpi_explanation_utilization,
            true, R.string.kpi_explanation_bar_tag_utilization,
            R.string.kpi_explanation_bar_tag_utilization),
    UNKNOWN(R.string.unknown_value,
            BusinessUnitEnum.UNKNOWN,
            R.string.unknown_value,
            R.drawable.generic,
            R.drawable.kpi_explanation_generic,
            false, R.string.unknown_value,
            R.string.unknown_value);

    private int keyNameStringID;
    private BusinessUnitEnum businessUnitEnum;
    private int localizedNameStringID;
    private int kpiExplanationHeaderDrawable;
    private int kpiExplanationIndicatorDrawable;
    private boolean showBar;
    private int barDescriptiveTagStringIDImperial;
    private int barDescriptiveTagStringIDMetric;

    KpiExplanationEnum(int keyNameStringID, BusinessUnitEnum businessUnitEnum, int localizedNameStringID,
                       int kpiExplanationHeaderDrawable, int kpiExplanationIndicatorDrawable, boolean showBar,
                       int barDescriptiveTagStringIDImperial, int barDescriptiveTagStringIDMetric) {

        this.keyNameStringID = keyNameStringID;
        this.businessUnitEnum = businessUnitEnum;
        this.localizedNameStringID = localizedNameStringID;
        this.kpiExplanationHeaderDrawable = kpiExplanationHeaderDrawable;
        this.kpiExplanationIndicatorDrawable = kpiExplanationIndicatorDrawable;
        this.showBar = showBar;
        this.barDescriptiveTagStringIDImperial = barDescriptiveTagStringIDImperial;
        this.barDescriptiveTagStringIDMetric = barDescriptiveTagStringIDMetric;
    }

    public String getKeyName(Context context) {
        return context.getString(keyNameStringID);
    }

    public BusinessUnitEnum getBusinessUnitEnum() {
        return businessUnitEnum;
    }

    public int getLocalizedNameStringID() {
        return localizedNameStringID;
    }

    public int getKpiExplanationHeaderDrawable() {
        return kpiExplanationHeaderDrawable;
    }

    public int getKpiExplanationIndicatorDrawable() {
        return kpiExplanationIndicatorDrawable;
    }

    public boolean isShowBar() {
        return showBar;
    }

    public int getBarDescriptiveTagStringIDImperial() {
        return barDescriptiveTagStringIDImperial;
    }

    public int getBarDescriptiveTagStringIDMetric() {
        return barDescriptiveTagStringIDMetric;
    }

    public static KpiExplanationEnum from(Context context, String name, BusinessUnitEnum businessUnitEnum) {
        for (KpiExplanationEnum kpi : KpiExplanationEnum.values()) {
            if (businessUnitEnum == kpi.getBusinessUnitEnum() &&
                    kpi.getKeyName(context).toLowerCase().equals(name.toLowerCase()))
                return kpi;
        }
        return KpiExplanationEnum.UNKNOWN;
    }
}
