package com.hp.printosmobile.presentation.modules.servicecallpanel;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.AppseeSdk;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.modules.servicecallpanel.ServiceCallViewModel.ServiceCallStateEnum;
import com.hp.printosmobilelib.ui.common.HPFragment;
import com.hp.printosmobilelib.ui.utils.SpaceItemDecoration;

import butterknife.Bind;

/**
 * create by anwar asbah 3/12/2017
 */
public class ServiceCallsFragment extends HPFragment {

    private static final String ARG_VIEW_MODEL = "param1";
    private static final String ARG_SERVICE_CALL_FOCUS_STATE = "param2";
    private static final long ITEM_EXPAND_DELAY = 300;

    @Bind(R.id.calls_list)
    RecyclerView callsList;
    @Bind(R.id.loading_view)
    View loadingView;

    ServiceCallViewModel serviceCallViewModel;
    ServiceCallsAdapter serviceCallsAdapter;
    ServiceCallStateEnum serviceCallStateEnum = ServiceCallStateEnum.OPEN;

    public static ServiceCallsFragment newInstance(ServiceCallViewModel serviceCallViewModel,
                                                   ServiceCallStateEnum serviceCallStateEnum) {
        ServiceCallsFragment fragment = new ServiceCallsFragment();
        if (serviceCallViewModel != null) {
            Bundle args = new Bundle();
            args.putSerializable(ARG_VIEW_MODEL, serviceCallViewModel);
            args.putSerializable(ARG_SERVICE_CALL_FOCUS_STATE, serviceCallStateEnum);
            fragment.setArguments(args);
        }
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null) {
            if (bundle.containsKey(ARG_VIEW_MODEL)) {
                serviceCallViewModel = (ServiceCallViewModel) bundle.getSerializable(ARG_VIEW_MODEL);
            }

            if (bundle.containsKey(ARG_SERVICE_CALL_FOCUS_STATE)) {
                serviceCallStateEnum = (ServiceCallStateEnum) bundle.getSerializable(ARG_SERVICE_CALL_FOCUS_STATE);
            }
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
        Analytics.sendEvent(Analytics.MOBILE_PRINT_BEAT_CATEGORY, Analytics.VIEW_SERVICE_CALLS_ACTION);
        AppseeSdk.getInstance(getActivity()).startScreen(AppseeSdk.SCREEN_SERVICE_CALLS);
    }

    private void init() {
        callsList.addItemDecoration(new SpaceItemDecoration((int) PrintOSApplication.getAppContext()
                .getResources().getDimension(R.dimen.next_10_jobs_middle_separator_width)));
        displayModels();
    }

    public void displayModels(ServiceCallViewModel serviceCallViewModel){
        this.serviceCallViewModel = serviceCallViewModel;
        displayModels();
    }

    private void displayModels() {
        if (serviceCallViewModel == null || serviceCallViewModel.getCallViewModels() == null ||
                serviceCallViewModel.getCallViewModels().isEmpty()) {
            return;
        }

        loadingView.setVisibility(View.GONE);

        final LinearLayoutManager manager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        callsList.setLayoutManager(manager);
        serviceCallsAdapter = new ServiceCallsAdapter(PrintOSApplication.getAppContext(), serviceCallViewModel,
                new ServiceCallsAdapter.ServiceCallAdapterCallback() {
                    @Override
                    public void onItemExpanded(int viewPosition) {
                        manager.scrollToPositionWithOffset(viewPosition, 0);
                    }
                });
        callsList.setAdapter(serviceCallsAdapter);

        int firstOccurrence = 0;
        if(serviceCallStateEnum != ServiceCallStateEnum.OPEN) {
            firstOccurrence = serviceCallsAdapter.getFirstClosedOccurrence();
            if(serviceCallsAdapter.getFirstClosedOccurrence() > 0) {
                callsList.scrollToPosition(firstOccurrence);
            }
        }
        /*final int expandIndex = firstOccurrence;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                serviceCallsAdapter.setExpanded(expandIndex);
            }
        }, ITEM_EXPAND_DELAY);*/
    }

    public void onFilterSelected(){
        loadingView.setVisibility(View.VISIBLE);
    }

    public void onServiceCallDataRetrived(){
        loadingView.setVisibility(View.GONE);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_service_calls;
    }

    @Override
    public String getToolbarDisplayNameExtra() {
        return "";
    }

    @Override
    public boolean isFragmentNameToBeDisplayed() {
        return true;
    }

    @Override
    public boolean isFilteringAvailable() {
        return true;
    }

    @Override
    public int getToolbarDisplayName() {
        return R.string.service_call_fragment_name;
    }

    @Override
    public boolean onBackPressed() {
        return true;
    }

    @Override
    public boolean isIntercomAccessible() {
        return true;
    }
}
