package com.hp.printosmobile.presentation.modules.hiddensettings;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;

import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.presentation.BaseActivity;
import com.hp.printosmobile.presentation.modules.login.LoginActivity;
import com.hp.printosmobile.utils.EmailUtils;
import com.hp.printosmobile.utils.HPUIUtils;
import com.hp.printosmobilelib.core.communications.remote.Preferences;
import com.hp.printosmobilelib.core.logging.HPLogger;

import java.io.File;
import java.util.ArrayList;

public class HiddenSettingsActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.animator.left_to_right, R.animator.right_to_left);

        // Display the fragment as the main content.
        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new SettingsFragment())
                .commit();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.hidden_settings;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, LoginActivity.class);
        this.startActivity(intent);
    }

    /**
     * A preference value change listener that updates the preference's summary
     * to reflect its new value.
     */
    private static Preference.OnPreferenceChangeListener listener = new Preference.OnPreferenceChangeListener() {
        @Override
        public boolean onPreferenceChange(Preference preference, Object value) {
            String stringValue = value.toString();

            if (preference instanceof ListPreference) {
                ListPreference listPreference = (ListPreference) preference;
                int index = listPreference.findIndexOfValue(stringValue);

                preference.setSummary(index >= 0 ? listPreference.getEntries()[index] : null);
                PrintOSApplication.initServicesProvider(PrintOSApplication.getAppContext(), stringValue);
                PrintOSApplication.initSdks(stringValue);
            }
            return true;
        }
    };


    private static void bindListener(Context context, Preference preference) {
        preference.setOnPreferenceChangeListener(listener);

        listener.onPreferenceChange(preference,
                PrintOSPreferences.getInstance(context)
                        .getPreferenceValue(preference.getKey()));
    }

    public static class SettingsFragment extends PreferenceFragment implements Preference.OnPreferenceClickListener {

        private static final String TAG = SettingsFragment.class.getSimpleName();

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            getPreferenceManager().setSharedPreferencesName(Preferences.getFileName(PrintOSApplication.getAppContext()));

            addPreferencesFromResource(R.xml.hidden_pref_general);
            bindListener(getActivity(), findPreference(getString(R.string.pref_server_choose)));

            Preference preference = findPreference(getString(R.string.pref_send_feedback));
            preference.setOnPreferenceClickListener(this);

        }

        @Override
        public boolean onPreferenceClick(Preference preference) {

            if (preference.getKey().equals(getString(R.string.pref_send_feedback))) {
                sendFeedbackWithAttachmentThroughEmail();
            }

            return false;
        }

        private void sendFeedbackWithAttachmentThroughEmail() {

            String to = getString(R.string.hp_support_email);
            String subject = getString(R.string.send_feedback_email_title);
            String body = "";
            File logsFile = new File(HPLogger.getInstance(getActivity()).getFilePath());

            HPLogger.d(TAG, "Sending log file..");

            if (logsFile != null) {

                ArrayList<Uri> attachments = new ArrayList<>();
                attachments.add(Uri.fromFile(logsFile));
                EmailUtils.sendEmail(getActivity(), new String[]{to}, subject, body, attachments);

            } else {
                HPLogger.e(TAG, "Error occurred while attaching the log file..");
                HPUIUtils.displayToast(getActivity(), getActivity().getString(R.string.unable_to_attach_log_file), false);
            }
        }
    }


}



