package com.hp.printosmobile.presentation;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;

import com.hp.printosmobile.Constants;
import com.hp.printosmobile.IntercomSdk;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.utils.HPUIUtils;
import com.hp.printosmobilelib.ui.common.HPFragment;

/**
 * Created by Anwar Asbah on 5/11/17.
 */
public abstract class BaseFragment extends HPFragment {

    private static final String INTENT_MEDIA_TYPE = "image/*";

    protected void shareImage(Uri storageUri) {
        Intent intent;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            intent = new Intent(Intent.ACTION_SEND);
            intent.addFlags(Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION);
        } else {
            intent = new Intent(Intent.ACTION_SEND);
        }

        intent.setType(INTENT_MEDIA_TYPE);

        intent.putExtra(android.content.Intent.EXTRA_SUBJECT, "");
        intent.putExtra(android.content.Intent.EXTRA_TEXT, "");
        intent.putExtra(Intent.EXTRA_STREAM, storageUri);
        try {
            startActivityForResult(Intent.createChooser(intent, PrintOSApplication.getAppContext().getString(R.string.share_with)), Constants.IntentExtras.APPS_PICKER_INTENT_REQUEST_CODE);
        } catch (ActivityNotFoundException e) {
            HPUIUtils.displayToast(PrintOSApplication.getAppContext(), PrintOSApplication.getAppContext().getString(R.string.no_app_for_sharing), false);
        }

        IntercomSdk.getInstance(PrintOSApplication.getAppContext()).sendClickShareIconEvent();
    }
}
