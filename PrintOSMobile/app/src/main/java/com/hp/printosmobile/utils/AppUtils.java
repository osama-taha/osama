package com.hp.printosmobile.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.BuildConfig;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobilelib.core.communications.remote.APIException;
import com.hp.printosmobilelib.core.communications.remote.models.AAAError;
import com.hp.printosmobilelib.core.utils.DeviceUtils;

/**
 * Created by Osama Taha on 3/17/17.
 */

public final class AppUtils {

    private static final double MILLI_TO_INCH_RATIO = 0.0393701;
    private static final double METER_TO_FEET_RATIO = 3.28084;

    /**
     * Launch an activity that handles the ACTION_VIEW action.
     *
     * @param context
     * @param uri     - The Intent data URI.
     */
    public static void startApplication(Context context, Uri uri) {
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        // Make sure that there is an Activity component to handle this intent.
        if (intent.resolveActivity(context.getPackageManager()) != null) {
            context.startActivity(intent);
        } else {
            HPUIUtils.displayToast(context, context.getString(R.string.default_error), false);
        }
    }

    public static String getGooglePlayServicesAvailabilityStatusMessage(int status) {

        switch (status) {
            case ConnectionResult.SUCCESS:
                return "SUCCESS";
            case ConnectionResult.SERVICE_DISABLED:
                return "SERVICE_DISABLED";
            case ConnectionResult.SERVICE_INVALID:
                return "SERVICE_INVALID";
            case ConnectionResult.SERVICE_MISSING:
                return "SERVICE_MISSING";
            case ConnectionResult.SERVICE_MISSING_PERMISSION:
                return "SERVICE_MISSING_PERMISSION";
            case ConnectionResult.SERVICE_UPDATING:
                return "SERVICE_UPDATING";
            case ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED:
                return "SERVICE_VERSION_UPDATE_REQUIRED";
        }

        return String.valueOf(status);
    }

    /**
     * Get app and device info formatted.
     * <p/>
     * Platform (iOS/Android)
     * •	Device model
     * •	OS version
     * •	UDID/serial
     * •	GUID
     * •	App version
     * •	Google play services status
     * •	Current working stack
     * •	GUID (see above)
     * •	IP address
     * •	Wi-Fi / Cellular (can we know download speed?)
     */
    public static String getDeviceInfo(Context context) {

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("AppInfo:");
        stringBuilder.append("\n");
        stringBuilder.append("Platform: Android");
        stringBuilder.append("\n");
        stringBuilder.append("Model: " + Build.MODEL);
        stringBuilder.append("\n");
        stringBuilder.append("Api version: " + Build.VERSION.RELEASE);
        stringBuilder.append("\n");
        stringBuilder.append("Device Id: " + DeviceUtils.getDeviceIdMD5(context));
        stringBuilder.append("\n");
        stringBuilder.append("App version: " + BuildConfig.VERSION_NAME);
        stringBuilder.append("\n");
        stringBuilder.append("isBetaVersion: " + BuildConfig.isBetaVersion);
        stringBuilder.append("\n");
        String googlePlayServicesStatus = getGooglePlayServicesAvailabilityStatusMessage(GoogleApiAvailability.getInstance()
                .isGooglePlayServicesAvailable(context));
        stringBuilder.append("Google play services status: " + googlePlayServicesStatus);
        stringBuilder.append("\n");
        stringBuilder.append("Stack: " + PrintOSPreferences.getInstance(context).getServerName());
        stringBuilder.append("\n");
        stringBuilder.append("IP Address: " + DeviceUtils.getIPAddress(context));

        return stringBuilder.toString();
    }

    public static void sendLoginErrorEvent(APIException error, String action) {

        if (error.getAAAError() != null) {

            if (error.getKind() == APIException.Kind.UNAUTHORIZED) {

                AAAError.SmsError smsError = error.getAAAError().getSmsError();
                AAAError.ErrorSubCode errorSubCode = smsError.getSubCode();

                switch (errorSubCode) {
                    case INVALID_USER_NAME_PASSWORD:
                        Analytics.sendEvent(Analytics.MOBILE_PRINT_BEAT_CATEGORY, action,
                                Analytics.LOGIN_EVENT_CREDENTIALS_LABLE);
                        return;
                    case ACCOUNT_LOCKED:
                        Analytics.sendEvent(Analytics.MOBILE_PRINT_BEAT_CATEGORY, action,
                                Analytics.LOGIN_EVENT_LOCKED_LABEL);
                        return;
                    case EULA_NOT_ACCEPTED:
                        Analytics.sendEvent(Analytics.MOBILE_PRINT_BEAT_CATEGORY, action,
                                Analytics.LOGIN_EVENT_EULA_LABEL);
                        return;
                    case PASSWORD_EXPIRED:
                        Analytics.sendEvent(Analytics.MOBILE_PRINT_BEAT_CATEGORY, action,
                                Analytics.LOGIN_EVENT_EXPIRED_LABEL);
                        return;
                }
            }

            Analytics.sendEvent(Analytics.MOBILE_PRINT_BEAT_CATEGORY, action,
                    String.format(Analytics.LOGIN_EVENT_LOGIN_OTHER_LABEL, error.getAAAError().getSmsError().getSubCode()));
        }

        Analytics.sendEvent(Analytics.MOBILE_PRINT_BEAT_CATEGORY, action,
                String.format(Analytics.LOGIN_EVENT_LOGIN_OTHER_LABEL, "0"));
    }

    public static double convertMillimetersToInch(float millimeters) {
        return millimeters * MILLI_TO_INCH_RATIO;
    }

    public static double convertMetersToFeet(float meters) {
        return meters * METER_TO_FEET_RATIO;
    }

    public static void openAppSettings(Context context) {

        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                Uri.fromParts("package", context.getPackageName(), null));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);

    }
}
