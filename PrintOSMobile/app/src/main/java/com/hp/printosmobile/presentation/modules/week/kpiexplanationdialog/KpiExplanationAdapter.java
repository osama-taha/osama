package com.hp.printosmobile.presentation.modules.week.kpiexplanationdialog;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.modules.kpiview.KPIViewModel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Anwar Asbah on 8/6/2017.
 */
public class KpiExplanationAdapter extends PagerAdapter {

    public final static String TAG = KpiExplanationAdapter.class.getSimpleName();

    private Context context;
    private List<KPIViewModel> viewModels;
    private KpiViewBuilder viewBuilder;
    private Map<Integer, View> viewsList;

    public KpiExplanationAdapter(Context context, List<KPIViewModel> viewModels) {
        this.context = context;
        this.viewModels = viewModels;
        this.viewBuilder = new KpiViewBuilder();
        this.viewsList = new HashMap<>();
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        if (viewsList.containsKey(position)) {
            return viewsList.get(position);
        }

        LayoutInflater inflater = LayoutInflater.from(context);
        View layout = inflater.inflate(viewBuilder.getLayout(), container, false);
        viewBuilder.buildView(layout, viewModels, position);
        container.addView(layout);

        viewsList.put(position, layout);

        layout.setTag(position);
        return layout;

    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        viewsList.remove(position);
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return viewModels == null ? 0 : viewModels.size();
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    class KpiViewBuilder {

        @Bind(R.id.score_bar)
        KPIExplanationView barView;

        void buildView(final View view, final List<KPIViewModel> viewModels, int position) {
            ButterKnife.bind(this, view);
            barView.setViewModel(viewModels.get(position));
        }

        int getLayout() {
            return R.layout.view_kpi_explanation_page;
        }
    }
}
