package com.hp.printosmobile.presentation.modules.printersnapshot;

import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;
import com.hp.printosmobile.presentation.modules.shared.DeviceViewModel;

import java.util.List;

/**
 * Created by Anwar Asbah on 5/4/2016.
 */
public class PrinterSnapshotViewModel {

    private static final int PWP_NUMBER_OF_ROWS = 3;
    private static final int PWP_NUMBER_OF_COLUMNS = 1;
    private static final int LATEX_NUMBER_OF_ROWS = 3;
    private static final int LATEX_NUMBER_OF_COLUMNS = 1;

    private List<DeviceViewModel> devices;
    private BusinessUnitEnum businessUnitEnum;

    public List<DeviceViewModel> getDevices() {
        return devices;
    }

    public void setDevices(List<DeviceViewModel> devices) {
        this.devices = devices;
    }

    public BusinessUnitEnum getBusinessUnitEnum() {
        return businessUnitEnum;
    }

    public void setBusinessUnitEnum(BusinessUnitEnum businessUnitEnum) {
        this.businessUnitEnum = businessUnitEnum;
    }

    public int getNumberOfRows() {
        if (businessUnitEnum == null) {
            return PWP_NUMBER_OF_ROWS;
        }
        switch (businessUnitEnum) {
            case LATEX_PRINTER:
                return LATEX_NUMBER_OF_ROWS;
            default:
                return PWP_NUMBER_OF_ROWS;
        }
    }

    public int getNumberOfColumns() {
        if (businessUnitEnum == null) {
            return PWP_NUMBER_OF_COLUMNS;
        }
        switch (businessUnitEnum) {
            case LATEX_PRINTER:
                return LATEX_NUMBER_OF_COLUMNS;
            default:
                return PWP_NUMBER_OF_COLUMNS;
        }
    }
}
