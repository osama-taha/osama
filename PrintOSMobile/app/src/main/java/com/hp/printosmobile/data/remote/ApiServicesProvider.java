package com.hp.printosmobile.data.remote;

import android.content.Context;

import com.hp.printosmobile.data.remote.services.BeatCoinService;
import com.hp.printosmobile.data.remote.services.BoxService;
import com.hp.printosmobile.data.remote.services.ConfigService;
import com.hp.printosmobile.data.remote.services.ContactHpService;
import com.hp.printosmobile.data.remote.services.EulaService;
import com.hp.printosmobile.data.remote.services.InsightsService;
import com.hp.printosmobile.data.remote.services.MetaDataService;
import com.hp.printosmobile.data.remote.services.NotificationService;
import com.hp.printosmobile.data.remote.services.OrganizationService;
import com.hp.printosmobile.data.remote.services.PerformanceTrackingService;
import com.hp.printosmobile.data.remote.services.PersonalAdvisorService;
import com.hp.printosmobile.data.remote.services.PreferencesService;
import com.hp.printosmobile.data.remote.services.ProductionService;
import com.hp.printosmobile.data.remote.services.RealtimeTrackingService;
import com.hp.printosmobile.data.remote.services.ResetPasswordService;
import com.hp.printosmobile.data.remote.services.ServiceCallService;
import com.hp.printosmobile.data.remote.services.SupportedVersionsService;
import com.hp.printosmobile.data.remote.services.UserDataService;
import com.hp.printosmobilelib.core.communications.remote.ApiClientFactory;
import com.hp.printosmobilelib.core.communications.remote.ApiConfig;
import com.hp.printosmobilelib.core.communications.remote.services.LogDispatchService;
import com.hp.printosmobilelib.core.communications.remote.services.LoginService;

/**
 * A class responsible for Building the APIs.
 *
 * @author Osama Taha
 */
public class ApiServicesProvider {

    private final ApiClientFactory apiClientFactory;
    //private final ApiClientFactory boxApiClientFactory;

    public ApiServicesProvider(Context context, ApiConfig apiConfig) {
        apiClientFactory = new ApiClientFactory(context, apiConfig, true);
        //boxApiClientFactory = new ApiClientFactory(context, boxApiConfig, false);
    }

    public ApiServicesProvider(Context context, ApiConfig apiConfig, boolean hasAuthenticator) {
        apiClientFactory = new ApiClientFactory(context, apiConfig, hasAuthenticator);
    }

    public LoginService getLoginService() {
        return apiClientFactory.getLoginService();
    }

    public ConfigService getConfigService() {
        return apiClientFactory.getService(ConfigService.class);
    }

    public RealtimeTrackingService getRealtimeTrackingService() {
        return apiClientFactory.getService(RealtimeTrackingService.class);
    }

    public MetaDataService getMetaDataService() {
        return apiClientFactory.getService(MetaDataService.class);
    }


    public PerformanceTrackingService getPerformanceTrackingService() {
        return apiClientFactory.getService(PerformanceTrackingService.class);
    }

    public ProductionService getProductionService() {
        return apiClientFactory.getService(ProductionService.class);
    }

    public PersonalAdvisorService getPersonalAdvisorService() {
        return apiClientFactory.getService(PersonalAdvisorService.class);
    }

    public ContactHpService getContactHpService() {
        return apiClientFactory.getService(ContactHpService.class);
    }

    public EulaService getEULAService() {
        return apiClientFactory.getService(EulaService.class);
    }

    public ResetPasswordService getResetPasswordService() {
        return apiClientFactory.getService(ResetPasswordService.class);
    }

    public OrganizationService getOrganizationService() {
        return apiClientFactory.getService(OrganizationService.class);
    }

    public SupportedVersionsService getSupportedVersionsService() {
        return apiClientFactory.getService(SupportedVersionsService.class);
    }

    public BoxService getBoxService() {
        return apiClientFactory.getService(BoxService.class);
    }

    public NotificationService getNotificationService() {
        return apiClientFactory.getService(NotificationService.class);
    }

    public ServiceCallService getServiceCallSerivce() {
        return apiClientFactory.getService(ServiceCallService.class);
    }

    public UserDataService getUserDataService() {
        return apiClientFactory.getService(UserDataService.class);
    }

    public LogDispatchService getLogService() {
        return apiClientFactory.getService(LogDispatchService.class);
    }

    public PreferencesService getPreferencesService() {
        return apiClientFactory.getService(PreferencesService.class);
    }

    public InsightsService getInsightsService() {
        return apiClientFactory.getService(InsightsService.class);
    }

    public BeatCoinService getBeatCoinService() {
        return apiClientFactory.getService(BeatCoinService.class);
    }

}