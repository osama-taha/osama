package com.hp.printosmobile.presentation.modules.insights;

import android.content.Context;
import android.graphics.drawable.Drawable;

import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.modules.insights.InsightsViewModel.InsightKpiEnum;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Anwar Asbah on 6/20/2017.
 */

public class InsightsUtils {

    public static String getPanelTitle(Context context, InsightsViewModel insightsViewModel) {

        if (insightsViewModel == null || insightsViewModel.getInsightModels() == null) {
            if (context == null) {
                return "";
            }
            return context.getString(R.string.insights_tab_name);
        }

        List<InsightsViewModel.InsightModel> models = new ArrayList<>();
        models.addAll(insightsViewModel.getInsightModels());
        Collections.sort(models, InsightsViewModel.COMPARATOR);

        return insightsViewModel.getInsightKpiEnum() == InsightKpiEnum.JAM ? getJamTitle(context, models)
                : getFailureTitle(context, models);
    }

    private static String getJamTitle(Context context, List<InsightsViewModel.InsightModel> models) {
        int sumOfTop1 = 0;
        int sumOfTop5 = 0;
        int sumOfTop3 = 0;
        int sumOfTop2 = 0;

        if (models != null) {
            for (int i = 0; i < models.size(); i++) {
                if (models.get(i) != null) {
                    sumOfTop1 += i < 1 ? models.get(i).getPercentage() : 0;
                    sumOfTop2 += i < 2 ? models.get(i).getPercentage() : 0;
                    sumOfTop3 += i < 3 ? models.get(i).getPercentage() : 0;
                    sumOfTop5 += i < 5 ? models.get(i).getPercentage() : 0;
                }
            }
        }

        if (sumOfTop1 >= 50) {
            return context.getString(R.string.insights_jam_title_1);
        } else if (sumOfTop1 >= 40) {
            return context.getString(R.string.insights_jam_title_2, String.valueOf(sumOfTop1));
        } else if (sumOfTop5 > 80) {
            return context.getString(R.string.insights_jam_title_3);
        } else if (sumOfTop5 < 50) {
            return context.getString(R.string.insights_jam_title_4);
        } else if (sumOfTop2 >= 50) {
            return context.getString(R.string.insights_jam_title_5, String.valueOf(sumOfTop2));
        } else if (sumOfTop5 >= 70) {
            return context.getString(R.string.insights_jam_title_6);
        } else if (sumOfTop5 < 55) {
            return context.getString(R.string.insights_jam_title_7);
        } else if (sumOfTop3 > 50) {
            return context.getString(R.string.insights_jam_title_8);
        } else {
            return context.getString(R.string.insights_jam_title_9);
        }
    }

    private static String getFailureTitle(Context context, List<InsightsViewModel.InsightModel> models) {
        int sumOfTop1 = 0;
        int sumOfTop5 = 0;
        int sumOfTop3 = 0;
        int sumOfTop2 = 0;

        if (models != null) {
            for (int i = 0; i < models.size(); i++) {
                if (models.get(i) != null) {
                    sumOfTop1 += i < 1 ? models.get(i).getPercentage() : 0;
                    sumOfTop2 += i < 2 ? models.get(i).getPercentage() : 0;
                    sumOfTop3 += i < 3 ? models.get(i).getPercentage() : 0;
                    sumOfTop5 += i < 5 ? models.get(i).getPercentage() : 0;
                }
            }
        }

        if (sumOfTop1 >= 50) {
            return context.getString(R.string.insights_failure_title_1);
        } else if (sumOfTop1 >= 35) {
            return context.getString(R.string.insights_failure_title_2, String.valueOf(sumOfTop1));
        } else if (sumOfTop5 > 70) {
            return context.getString(R.string.insights_failure_title_3);
        } else if (sumOfTop5 < 40) {
            return context.getString(R.string.insights_failure_title_4);
        } else if (sumOfTop2 >= 40) {
            return context.getString(R.string.insights_failure_title_5, String.valueOf(sumOfTop2));
        } else if (sumOfTop5 >= 60) {
            return context.getString(R.string.insights_failure_title_6);
        } else if (sumOfTop5 < 50) {
            return context.getString(R.string.insights_failure_title_7);
        } else if (sumOfTop3 > 40) {
            return context.getString(R.string.insights_failure_title_8);
        } else {
            return context.getString(R.string.insights_failure_title_9);
        }
    }
}
