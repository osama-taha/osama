package com.hp.printosmobile.presentation.modules.notificationslist;

import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.data.remote.ApiServicesProvider;
import com.hp.printosmobile.data.remote.models.NotificationApplicationData;
import com.hp.printosmobile.data.remote.models.NotificationApplicationEventData;
import com.hp.printosmobile.data.remote.models.NotificationsData;
import com.hp.printosmobile.data.remote.models.NotificationsData.NotificationEvent.NotificationItem;
import com.hp.printosmobile.data.remote.models.NotificationsStringData;
import com.hp.printosmobile.data.remote.services.NotificationService;
import com.hp.printosmobile.presentation.modules.settings.SubscriptionEvent;
import com.hp.printosmobilelib.core.utils.HPDateUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Response;
import rx.Observable;
import rx.functions.Func2;
import rx.functions.Func3;

/**
 * Created by Anwar Asbah on 1/23/17.
 */
public class NotificationListDataManager {

    private static final String TAG = NotificationListDataManager.class.getSimpleName();

    private static final int NOTIFICATIONS_LIMIT = 250;
    private static final int NOTIFICATIONS_OFFSET = 0;
    private static final String SUB_PARAM_FORMAT = "{%d}";

    public static Observable<List<NotificationViewModel>> getUserNotifications(final List<String> supportedEvents,
                                                                               final String locale) {

        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
        NotificationService notificationService = serviceProvider.getNotificationService();

        return Observable.combineLatest(notificationService.getNotificationLocalizedStrings(locale),
                notificationService.getUserNotifications(NOTIFICATIONS_LIMIT, NOTIFICATIONS_OFFSET),
                new Func2<Response<NotificationsStringData>, Response<NotificationsData>, List<NotificationViewModel>>() {
                    @Override
                    public List<NotificationViewModel> call(Response<NotificationsStringData> notificationsStringDataResponse, Response<NotificationsData> notificationsDataResponse) {
                        if (notificationsStringDataResponse != null && notificationsStringDataResponse.isSuccessful()
                                && notificationsDataResponse != null && notificationsDataResponse.isSuccessful()) {
                            return parseNotificationData(notificationsDataResponse.body(), notificationsStringDataResponse.body(), supportedEvents);
                        }
                        return null;
                    }
                }
        );
    }

    private static List<NotificationViewModel> parseNotificationData(NotificationsData notificationsData,
                                                                     NotificationsStringData notificationsStringData,
                                                                     List<String> supportedEvents) {
        if (notificationsData == null || notificationsData.getNotificationEvents() == null
                || notificationsStringData == null || notificationsStringData.getMessages() == null) {
            return null;
        }

        Map<String, String> messageStringMap = new HashMap<>();
        for (NotificationsStringData.NotificationString notificationString : notificationsStringData.getMessages()) {
            if (notificationString.getMsgId() != null && notificationString.getMsgText() != null) {
                messageStringMap.put(notificationString.getMsgId(), notificationString.getMsgText());
            }
        }

        List<NotificationViewModel> notificationViewModels = new ArrayList<>();
        for (NotificationsData.NotificationEvent notificationEvent : notificationsData.getNotificationEvents()) {
            if (notificationEvent.getNotificationItems() != null) {
                for (NotificationItem notificationItem : notificationEvent.getNotificationItems()) {

                    SubscriptionEvent.NotificationEventEnum notificationEventEnum = SubscriptionEvent
                            .NotificationEventEnum.from(notificationItem.getEventDescriptionId());

                    if (notificationEventEnum == SubscriptionEvent.NotificationEventEnum.UNKNOWN ||
                            (notificationEventEnum.isOptional()
                                    && !supportedEvents.contains(notificationItem.getEventDescriptionId()))) {
                        continue;
                    }

                    NotificationViewModel model = new NotificationViewModel();
                    model.setRead(notificationItem.isAcknowledged());
                    model.setNotificationEventEnum(notificationEventEnum);
                    model.setLinkLabel(notificationItem.getLinkLabel());
                    if (messageStringMap.containsKey(notificationItem.getSubValue())) {
                        String msg = messageStringMap.get(notificationItem.getSubValue());
                        if (notificationItem.getSubValueParams() != null) {
                            for (int i = 0; i < notificationItem.getSubValueParams().size(); i++) {
                                String param = notificationItem.getSubValueParams().get(i);
                                if (param != null) {
                                    msg = msg.replace(String.format(SUB_PARAM_FORMAT, i), param);
                                }
                            }
                        }
                        model.setNotificationBody(msg);
                    } else {
                        model.setNotificationBody(notificationItem.getSubValue());
                    }
                    model.setSentDate(HPDateUtils.getDateFromTimeStamp(notificationItem.getTimestamp()));
                    model.setNotificationID(notificationItem.getId());
                    model.setUserEventID(notificationItem.getUserEventId());
                    model.setEntity(notificationItem.getEntity());
                    model.setGoLink(notificationItem.getGoLink());

                    notificationViewModels.add(model);
                }
            }
        }

        Collections.sort(notificationViewModels, NotificationViewModel.dateComparator);

        return notificationViewModels;
    }

    private static List<String> getEventsPerApp(String applicationId) {
        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
        NotificationService notificationService = serviceProvider.getNotificationService();
        try {
            Response<NotificationApplicationEventData> response = notificationService
                    .getNotificationApplicationEvents(applicationId).execute();
            if (response != null && response.isSuccessful() && response.body() != null &&
                    response.body().getEventDescriptionList() != null) {
                List<String> subscriptionEvents = new ArrayList<>();
                for (NotificationApplicationEventData.EventDescriptionData descriptionData : response.body().getEventDescriptionList()) {
                    if (descriptionData.isMobile()) {
                        subscriptionEvents.add(descriptionData.getEventDescriptionName());
                    }
                }
                return subscriptionEvents;
            }
            return null;
        } catch (IOException e) {
            return null;
        }
    }

    public static Observable<ResponseBody> markNotificationAsRead(int notificationID, int userID) {

        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
        NotificationService notificationService = serviceProvider.getNotificationService();

        return notificationService.markNotificationAsRead(notificationID, userID);
    }

    public static Observable<ResponseBody> deleteNotification(int notificationID, int userID) {

        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
        NotificationService notificationService = serviceProvider.getNotificationService();

        return notificationService.deleteNotification(notificationID, userID);
    }

}