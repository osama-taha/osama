package com.hp.printosmobile.presentation.modules.todayhistogram;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.data.remote.models.PreferencesData;
import com.hp.printosmobile.presentation.modules.home.ShiftViewModel;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;
import com.hp.printosmobile.presentation.modules.shared.Unit;
import com.hp.printosmobile.presentation.modules.todayhistogram.TodayHistogramViewModel.TodayHistogramItem;
import com.hp.printosmobile.presentation.modules.todayhistogram.TodayHistogramViewModel.TodayHistogramItem.TodayHistogramItemStatus;
import com.hp.printosmobile.utils.HPLocaleUtils;

import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Calendar;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * @author Anwar Asbah Create on 9/27/2016
 */
public class TodayHistogramView extends FrameLayout {

    private static final float THOUSAND = 1000;
    private static final String SHIFTS_NOW_LABLE = "%s\n%s";

    private boolean isLandscape;
    private boolean isShift;
    private int daysSpan;
    private TodayHistogramViewModel viewModel;
    private String[] localizedDaysSymbols;
    private int todayDayOrdinal;
    private int offset;
    private int pagingSpan;
    private boolean isPaging = false;
    private HistogramLegendsAdapter legendsAdapter;

    private HistogramViewBuilder histogramViewBuilder;
    private PreferencesData.UnitSystem unitSystem;

    public TodayHistogramView(Context context) {
        super(context);
        init(context, null);
    }

    public TodayHistogramView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public TodayHistogramView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        isLandscape = false;
        if (attrs != null) {
            TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.TodayHistogramView, 0, 0);
            try {
                isLandscape = typedArray.getBoolean(R.styleable.TodayHistogramView_isLandscape, false);
            } catch (Exception e) {
            } finally {
                typedArray.recycle();
            }
        }
    }

    private void setupView () {
        View view = inflate(getContext(), isLandscape ? (isShift?
                R.layout.today_histogram_view_landscape_shifts :
                R.layout.today_histogram_view_landscape) :
                R.layout.today_histogram_view_portrait, this);

        if (isLandscape) {
            histogramViewBuilder = new LandscapeBuilder(view);
        } else {
            histogramViewBuilder = new PortraitBuilder(view);
        }

        todayDayOrdinal = Calendar.getInstance().get(Calendar.DAY_OF_WEEK);
        unitSystem = PrintOSPreferences.getInstance(getContext()).getUnitSystem();
    }

    public void setDaysSpan(int daysSpan) {
        this.daysSpan = daysSpan;
    }

    public void setViewModel(TodayHistogramViewModel viewModel) {
        this.viewModel = viewModel;
        this.isShift = viewModel.isShiftSupport();
        setupView();
        histogramViewBuilder.update();
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public void setPagingSpan(int pagingSpan) {
        this.pagingSpan = pagingSpan;
    }

    public void setPaging(boolean isPaging) {
        this.isPaging = isPaging;
    }

    public abstract class HistogramViewBuilder {

        @Bind(R.id.histogram_row)
        TableRow histogramRow;
        @Bind(R.id.legend_list)
        RecyclerView legendList;

        public HistogramViewBuilder(View view) {
            ButterKnife.bind(this, view);
        }

        protected void update() {
            if (viewModel == null) {
                return;
            }

            clearTable();

            DateFormatSymbols dateFormatSymbols = new DateFormatSymbols();
            localizedDaysSymbols = dateFormatSymbols.getShortWeekdays();

            int dataSize = viewModel.getData() == null ? -1 : viewModel.getData().size();

            for (int i = 0; i <= daysSpan; i++) {
                TodayHistogramItem item = null;
                if (dataSize + i > daysSpan) {
                    int dayDiff = daysSpan - i;
                    item = viewModel.getData().get(dataSize - 1 - dayDiff - offset);
                }

                int max = viewModel.getMaxValue(isPaging ? pagingSpan : daysSpan);
                displayBarItem(i, item, max);
            }

            updateTable();
            initLegendsList();
        }

        private void initLegendsList() {

            if (viewModel.isShiftSupport()) {
                legendList.setVisibility(View.VISIBLE);

                legendsAdapter = new HistogramLegendsAdapter(getContext(), new ArrayList<>(viewModel.getShiftLegends().values()));
                legendList.setLayoutManager(getLayoutManager());
                legendList.setAdapter(legendsAdapter);
                legendList.setHasFixedSize(true);

            } else {
                hideLegend();
            }
        }

        protected abstract RecyclerView.LayoutManager getLayoutManager();

        protected abstract void displayBarItem(int childIndex, TodayHistogramItem item, int maxValue);

        protected abstract void clearTable();

        protected abstract void hideLegend();

        protected abstract void updateTable();
    }

    public class PortraitBuilder extends HistogramViewBuilder {

        public PortraitBuilder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        @Override
        protected RecyclerView.LayoutManager getLayoutManager() {
            return new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);

        }

        @Override
        protected void displayBarItem(int childIndex, TodayHistogramItem item, int maxValue) {
            updateBarEntry(histogramRow, childIndex, item, maxValue);
        }

        @Override
        protected void clearTable() {
            clearTableRows(histogramRow);
        }

        @Override
        protected void hideLegend() {
            legendList.setVisibility(GONE);
        }

        @Override
        protected void updateTable() {

        }
    }

    public class LandscapeBuilder extends HistogramViewBuilder {

        @Bind(R.id.values_label)
        TextView valuesLabel;
        @Bind(R.id.values_row)
        TableRow valuesRow;
        @Bind(R.id.sheets_legend)
        View sheetsLegend;
        @Bind(R.id.sheets_row)
        TableRow sheetsRow;
        @Bind(R.id.meters_legend)
        TextView metersLegend;
        @Bind(R.id.meter_row)
        TableRow metersRow;
        @Bind(R.id.sqm_legend)
        TextView sqmLegend;
        @Bind(R.id.square_meter_row)
        TableRow squareMetersRow;

        boolean hasSheets = false;
        boolean hasMeters = false;
        boolean hasSqm = false;

        public LandscapeBuilder(View view) {
            super(view);
            ButterKnife.bind(this, view);

        }

        @Override
        protected void displayBarItem(int childIndex, TodayHistogramItem item, int maxValue) {
            updateBarEntry(histogramRow, childIndex, item, maxValue);
            updateValueCell(valuesRow, childIndex, item);

            float sheets = item.getSheets();
            updateEntryCell(sheetsRow, childIndex, sheets);
            hasSheets = hasSheets || !isErrorValue(sheets);

            float meters = item.getMeters();
            updateEntryCell(metersRow, childIndex, meters);
            hasMeters = hasMeters || !isErrorValue(meters);

            float squareMeters = item.getMetersSquare();
            updateEntryCell(squareMetersRow, childIndex, squareMeters);
            hasSqm = hasSqm || !isErrorValue(squareMeters);
        }

        @Override
        protected RecyclerView.LayoutManager getLayoutManager() {
            return new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        }

        @Override
        protected void clearTable() {
            clearTableRows(histogramRow, valuesRow, sheetsRow, metersRow, squareMetersRow);
        }

        @Override
        protected void hideLegend() {
            legendList.setVisibility(INVISIBLE);
        }

        @Override
        protected void updateTable() {
            setVisibility(hasSheets, sheetsLegend, sheetsRow);
            setVisibility(hasMeters, metersLegend, metersRow);
            setVisibility(hasSqm, sqmLegend, squareMetersRow);
            //setVisibility(viewModel.getBusinessUnit() == BusinessUnitEnum.INDIGO_PRESS, allThousandValueText);

            View[][] sectionedRow = new View[3][2];

            int index = 0;
            if (hasSheets) {
                sectionedRow[index][0] = sheetsLegend;
                sectionedRow[index][1] = sheetsRow;
                index += 1;
            }

            if (hasMeters) {
                metersLegend.setText(Unit.METER.getUnitText(getContext(), unitSystem, Unit.UnitStyle.DEFAULT));
                sectionedRow[index][0] = metersLegend;
                sectionedRow[index][1] = metersRow;
                index += 1;
            }

            if (hasSqm) {
                sqmLegend.setText(Unit.SQM.getUnitText(getContext(), unitSystem, Unit.UnitStyle.DEFAULT));
                sectionedRow[index][0] = sqmLegend;
                sectionedRow[index][1] = sqmLegend;
            }

            for (int i = 0; i < sectionedRow.length; i++) {
                if (sectionedRow[i][0] != null && sectionedRow[i][1] != null) {
                    setBgColor(ResourcesCompat.getColor(getResources(), i % 2 == 1 ? R.color.c101d
                            : R.color.c101c, null), sectionedRow[i][0], sectionedRow[i][1]);
                }
            }

            setBgColor(ResourcesCompat.getColor(getResources(), hasSheets ? R.color.c101d
                    : R.color.c101c, null), metersLegend, metersRow);

            setBgColor(ResourcesCompat.getColor(getResources(), hasSheets ? R.color.c101d
                    : R.color.c101c, null), metersLegend, metersRow);

            if (viewModel.getBusinessUnit() == BusinessUnitEnum.INDIGO_PRESS) {
                valuesLabel.setText(getContext().getString(R.string.today_panel_imp_target));
            } else {
                valuesLabel.setText(getContext().getString(unitSystem == PreferencesData.UnitSystem.Metric ?
                        R.string.today_panel_sqm_target
                        : R.string.today_panel_ft_square_target));
            }
        }

        private void setVisibility(boolean isVisible, View... views) {
            for (View view : views) {
                view.setVisibility(isVisible ? VISIBLE : GONE);
            }
        }

        private void setBgColor(int color, View... views) {
            for (View view : views) {
                view.setBackgroundColor(color);
            }
        }
    }

    private void updateBarEntry(TableRow histogramRow, int childIndex,
                                final TodayHistogramItem todayHistogramItem, final int maxValue) {

        LayoutInflater.from(getContext()).inflate(R.layout.today_histogram_bar_view_item, histogramRow, true);
        final TodayBarView bar = (TodayBarView) histogramRow.getChildAt(childIndex)
                .findViewById(com.hp.printosmobile.R.id.progress);

        if (todayHistogramItem != null) {

            bar.setDay(localizedDaysSymbols[todayHistogramItem.getDayOrdinal()], false);

            final boolean isToday = todayDayOrdinal == todayHistogramItem.getDayOrdinal() && childIndex == daysSpan + offset;

            if (isToday) {
                bar.setDay(getContext().getString(viewModel.isShiftSupport() ? R.string.now : R.string.today).toUpperCase(), false);
            }

            bar.setBarColor(getBarColor(todayHistogramItem));

            bar.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    bar.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    bar.setProgress(todayHistogramItem.getActual(), todayHistogramItem.getTarget(), maxValue, isToday);
                }
            });

            if (viewModel.isShiftSupport() && isLandscape) {

                ShiftViewModel shiftViewModel = todayHistogramItem.getShift();

                String barLable = HPLocaleUtils.getTimeRange(getContext(),
                        localizedDaysSymbols[shiftViewModel.getStartDay()],
                        shiftViewModel.getStartHour(),
                        localizedDaysSymbols[shiftViewModel.getEndDay()],
                        shiftViewModel.getEndHour());

                barLable = !isToday ? barLable : String.format(SHIFTS_NOW_LABLE, barLable, getContext().getString(R.string.now));
                bar.setDay(barLable, true);
            }
        }
    }

    private int getBarColor(TodayHistogramItem todayHistogramItem) {

        if(todayHistogramItem == null){
            return Color.WHITE;
        }

        if (viewModel.isShiftSupport()) {
            TodayHistogramViewModel.HistogramShiftLegend shiftLegend = viewModel.getShiftLegends()
                    .get(todayHistogramItem.getShift().getShiftId());
            if(shiftLegend != null) {
                return ResourcesCompat.getColor(getResources(), shiftLegend.getLegendColor(),null);
            }
        }
        return ResourcesCompat.getColor(getResources(), todayHistogramItem.getStatus().getColorId(), null);
    }

    private void updateEntryCell(TableRow tableRow, int childIndex, float value) {

        updateEntry(tableRow, childIndex,
                !isErrorValue(value) ?
                        HPLocaleUtils.getKiloStringWithoutSign((int) value)
                        : null, null, null, false);

    }

    private void updateValueCell(TableRow valuesRow, int childIndex, TodayHistogramItem todayHistogramItem) {

        String upperString;
        String lowerString;

        if (viewModel.getBusinessUnit() == BusinessUnitEnum.INDIGO_PRESS) {
            upperString = HPLocaleUtils.getKiloStringWithoutSign(todayHistogramItem.getActual());
            lowerString = HPLocaleUtils.getKiloStringWithoutSign(todayHistogramItem.getTarget());
        } else {
            upperString = HPLocaleUtils.getLocalizedValue(todayHistogramItem.getActual());
            lowerString = HPLocaleUtils.getLocalizedValue(todayHistogramItem.getTarget());
        }

        updateEntry(valuesRow, childIndex, upperString,
                "/" + lowerString, todayHistogramItem.getStatus(), true);

    }

    private boolean isErrorValue(float value) {
        return value == TodayHistogramItem.ERROR_VALUE;
    }

    private void updateEntry(TableRow row, int childIndex, String upperString, String lowerString,
                             TodayHistogramItemStatus status, boolean isValue) {

        LayoutInflater.from(getContext()).inflate(
                isValue ? (isShift ? R.layout.today_histogram_value_entry_view_item_shifts :
                        R.layout.today_histogram_value_entry_view_item) :
                        (isShift? R.layout.today_histogram_entry_view_item_shifts :
                                R.layout.today_histogram_entry_view_item),
                row, true);

        if (upperString != null) {
            TextView upperText = (TextView) row.getChildAt(childIndex).findViewById(R.id.entry_value);
            upperText.setText(upperString);
            if (isValue) {
                status = status == null ? TodayHistogramItemStatus.BELOW_AVERAGE : status;
                upperText.setTextColor(ResourcesCompat.getColor(getContext().getResources(), status.getColorId(), null));
            }
        }

        if (lowerString != null) {
            TextView lowerText = (TextView) row.getChildAt(childIndex).findViewById(R.id.lower_text);
            lowerText.setText(lowerString);
        }
    }

    private static void clearTableRows(TableRow... rows) {

        for (TableRow row : rows) {
            row.removeAllViews();
        }
    }
}
