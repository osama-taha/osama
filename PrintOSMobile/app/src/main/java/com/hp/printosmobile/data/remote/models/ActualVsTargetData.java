package com.hp.printosmobile.data.remote.models;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.hp.printosmobile.data.remote.services.RealtimeTrackingService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A model representing the response of {@link RealtimeTrackingService#getActualVsTargetData(String, List, String, String, String, boolean)} endpoint.
 *
 * @author Osama Taha
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ActualVsTargetData {

    @JsonProperty("thresholds")
    private Thresholds thresholds;
    @JsonProperty("days")
    private List<Day> days = new ArrayList<>();
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    public ActualVsTargetData() {

    }

    /**
     * @return The thresholds
     */
    @JsonProperty("thresholds")
    public Thresholds getThresholds() {
        return thresholds;
    }

    /**
     * @param thresholds The thresholds
     */
    @JsonProperty("thresholds")
    public void setThresholds(Thresholds thresholds) {
        this.thresholds = thresholds;
    }

    /**
     * @return The days
     */
    @JsonProperty("days")
    public List<Day> getDays() {
        return days;
    }

    /**
     * @param days The days
     */
    @JsonProperty("days")
    public void setDays(List<Day> days) {
        this.days = days;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("ActualVsTargetData{");
        sb.append("thresholds=").append(thresholds);
        sb.append(", days=").append(days);
        sb.append(", additionalProperties=").append(additionalProperties);
        sb.append('}');
        return sb.toString();
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class Day {

        @JsonProperty("target")
        private Integer target;
        @JsonProperty("actual")
        private Integer actual;
        @JsonProperty("day")
        private Integer day;
        @JsonProperty("data")
        private Data data;
        @JsonProperty("shiftData")
        private ShiftData shiftData;

        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<String, Object>();

        /**
         * @return The target
         */
        @JsonProperty("target")
        public Integer getTarget() {
            return target;
        }

        /**
         * @param target The target
         */
        @JsonProperty("target")
        public void setTarget(Integer target) {
            this.target = target;
        }

        /**
         * @return The actual
         */
        @JsonProperty("actual")
        public Integer getActual() {
            return actual;
        }

        /**
         * @param actual The actual
         */
        @JsonProperty("actual")
        public void setActual(Integer actual) {
            this.actual = actual;
        }

        /**
         * @return The day
         */
        @JsonProperty("day")
        public Integer getDay() {
            return day;
        }

        /**
         * @param day The day
         */
        @JsonProperty("day")
        public void setDay(Integer day) {
            this.day = day;
        }

        @JsonProperty("shiftData")
        public ShiftData getShiftData() {
            return shiftData;
        }

        @JsonProperty("shiftData")
        public void setShiftData(ShiftData shiftData) {
            this.shiftData = shiftData;
        }

        /**
         * @return The data
         */
        @JsonProperty("data")
        public Data getData() {
            return data;
        }

        /**
         * @param data The data
         */
        @JsonProperty("data")
        public void setData(Data data) {
            this.data = data;
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }

        @Override
        public String toString() {
            final StringBuffer sb = new StringBuffer("Day{");
            sb.append("target=").append(target);
            sb.append(", actual=").append(actual);
            sb.append(", day=").append(day);
            sb.append(", additionalProperties=").append(additionalProperties);
            sb.append('}');
            return sb.toString();
        }

        @JsonInclude(JsonInclude.Include.NON_NULL)
        public class Data {

            @JsonProperty("date")
            private String date;
            @JsonProperty("sheets")
            private Double sheets;
            @JsonProperty("meters")
            private Double meters;
            @JsonProperty("squareMeters")
            private Double squareMeters;
            @JsonProperty("shiftDataRespond")
            private ShiftData shiftData;
            @JsonIgnore
            private Map<String, Object> additionalProperties = new HashMap<>();

            /**
             * @return The succeedPercent
             */
            @JsonProperty("date")
            public String getDate() {
                return date;
            }

            /**
             * @param date The date
             */
            @JsonProperty("date")
            public void setDate(String date) {
                this.date = date;
            }

            @JsonProperty("sheets")
            public Double getSheets() {
                return sheets;
            }

            @JsonProperty("sheets")
            public void setSheets(Double sheets) {
                this.sheets = sheets;
            }

            @JsonProperty("meters")
            public Double getMeters() {
                return meters;
            }

            @JsonProperty("meters")
            public void setMeters(Double meters) {
                this.meters = meters;
            }

            @JsonProperty("squareMeters")
            public Double getSquareMeters() {
                return squareMeters;
            }

            @JsonProperty("squareMeters")
            public void setSquareMeters(Double squareMeters) {
                this.squareMeters = squareMeters;
            }

            @JsonProperty("shiftDataRespond")
            public ShiftData getShiftData() {
                return shiftData;
            }

            @JsonProperty("shiftDataRespond")
            public void setShiftData(ShiftData shiftData) {
                this.shiftData = shiftData;
            }

            @JsonAnyGetter
            public Map<String, Object> getAdditionalProperties() {
                return this.additionalProperties;
            }

            @JsonAnySetter
            public void setAdditionalProperty(String name, Object value) {
                this.additionalProperties.put(name, value);
            }
        }
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public class Thresholds {

        @JsonProperty("succeedPercent")
        private Integer succeedPercent;
        @JsonProperty("failPercent")
        private Integer failPercent;
        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<>();

        public Thresholds() {

        }

        /**
         * @return The succeedPercent
         */
        @JsonProperty("succeedPercent")
        public Integer getSucceedPercent() {
            return succeedPercent;
        }

        /**
         * @param succeedPercent The succeedPercent
         */
        @JsonProperty("succeedPercent")
        public void setSucceedPercent(Integer succeedPercent) {
            this.succeedPercent = succeedPercent;
        }

        /**
         * @return The failPercent
         */
        @JsonProperty("failPercent")
        public Integer getFailPercent() {
            return failPercent;
        }

        /**
         * @param failPercent The failPercent
         */
        @JsonProperty("failPercent")
        public void setFailPercent(Integer failPercent) {
            this.failPercent = failPercent;
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }

        @Override
        public String toString() {
            final StringBuffer sb = new StringBuffer("Thresholds{");
            sb.append("succeedPercent=").append(succeedPercent);
            sb.append(", failPercent=").append(failPercent);
            sb.append(", additionalProperties=").append(additionalProperties);
            sb.append('}');
            return sb.toString();
        }
    }
}