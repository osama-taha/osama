package com.hp.printosmobile.presentation.modules.personaladvisor;

import java.io.Serializable;

/**
 * Created by Anwar Asbah on 4/12/2016.
 */
public class AdviceViewModel implements Serializable{

    private String title;
    private String content;
    private AdviceType adviceType;
    private int imageResourceId;
    private int index;
    private Integer adviseId;
    private Boolean liked;
    private Boolean suppressed;
    private String deviceName;
    private String deviceSerialNumber;
    private boolean isReadMoreClicked;
    private boolean isNew;

    public boolean getIsReadMoreClicked() {
        return isReadMoreClicked;
    }

    public void setIsReadMoreClicked(boolean isReadMoreClicked) {
        this.isReadMoreClicked = isReadMoreClicked;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public AdviceType getAdviceType() {
        return adviceType;
    }

    public void setAdviceType(AdviceType adviceType) {
        this.adviceType = adviceType;
    }

    public int getImageResourceId() {
        return imageResourceId;
    }

    public void setImageResourceId(int imageResourceId) {
        this.imageResourceId = imageResourceId;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public int getIndex() {
        return index;
    }

    public void setAdviseId(Integer adviseId) {
        this.adviseId = adviseId;
    }

    public Integer getAdviseId() {
        return adviseId;
    }

    public void setLiked(Boolean liked) {
        this.liked = liked;
    }

    public Boolean getLiked() {
        return liked;
    }

    public void setSuppressed(Boolean suppressed) {
        this.suppressed = suppressed;
    }

    public Boolean getSuppressed() {
        return suppressed;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public void setDeviceSerialNumber(String deviceSerialNumber) {
        this.deviceSerialNumber = deviceSerialNumber;
    }

    public String getDeviceSerialNumber() {
        return deviceSerialNumber;
    }

    public boolean isNew() {
        return isNew;
    }

    public void setNew(boolean aNew) {
        isNew = aNew;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("AdviceViewModel{");
        sb.append("title='").append(title).append('\'');
        sb.append(", content='").append(content).append('\'');
        sb.append(", adviceType=").append(adviceType);
        sb.append(", imageResourceId=").append(imageResourceId);
        sb.append(", index=").append(index);
        sb.append(", adviseId=").append(adviseId);
        sb.append(", liked=").append(liked);
        sb.append(", suppressed=").append(suppressed);
        sb.append(", deviceName='").append(deviceName).append('\'');
        sb.append(", deviceSerialNumber='").append(deviceSerialNumber).append('\'');
        sb.append('}');
        return sb.toString();
    }

}
