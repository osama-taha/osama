package com.hp.printosmobile.presentation.modules.settings;

import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.data.remote.ApiServicesProvider;
import com.hp.printosmobile.data.remote.models.NotificationApplicationData;
import com.hp.printosmobile.data.remote.models.NotificationApplicationData.ApplicationType;
import com.hp.printosmobile.data.remote.models.NotificationApplicationEventData;
import com.hp.printosmobile.data.remote.models.NotificationApplicationEventData.EventDescriptionData;
import com.hp.printosmobile.data.remote.models.NotificationSubscription;
import com.hp.printosmobile.data.remote.services.NotificationService;
import com.hp.printosmobile.notification.LocationBasedNotificationsManager;
import com.hp.printosmobile.presentation.modules.settings.SubscriptionEvent.NotificationApplicationEnum;
import com.hp.printosmobile.presentation.modules.settings.SubscriptionEvent.NotificationEventEnum;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Response;
import rx.Observable;
import rx.functions.Func1;

/**
 * Created by anwar asbah on 1/17/2017.
 */
public class NotificationsManager {

    public static Observable<List<String>> getAvailableEvents() {
        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
        NotificationService notificationService = serviceProvider.getNotificationService();

        return notificationService.getNotificationApplications().map(new Func1<Response<NotificationApplicationData>, List<String>>() {
            @Override
            public List<String> call(Response<NotificationApplicationData> notificationApplicationDataResponse) {
                if (notificationApplicationDataResponse.isSuccessful() && notificationApplicationDataResponse.body() != null) {
                    List<ApplicationType> applicationTypes = notificationApplicationDataResponse.body().getApplicationList();
                    if (applicationTypes != null) {
                        List<String> events = new ArrayList<>();
                        for (ApplicationType applicationType : applicationTypes) {
                            NotificationApplicationEnum applicationEnum = NotificationApplicationEnum.from(applicationType.getName());
                            if (applicationEnum != NotificationApplicationEnum.UNKNOWN) {
                                List<String> appEvents = getEventsPerApp(applicationType.getId());
                                if (appEvents != null) {
                                    events.addAll(appEvents);
                                }
                            }
                        }
                        return events;
                    }
                }
                return null;
            }
        });
    }

    private static List<String> getEventsPerApp(String applicationId) {
        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
        NotificationService notificationService = serviceProvider.getNotificationService();
        try {
            Response<NotificationApplicationEventData> response = notificationService
                    .getNotificationApplicationEvents(applicationId).execute();
            if (response != null && response.isSuccessful() && response.body() != null &&
                    response.body().getEventDescriptionList() != null) {
                List<String> subscriptionEvents = new ArrayList<>();
                for (EventDescriptionData descriptionData : response.body().getEventDescriptionList()) {
                    if (descriptionData.isMobile()) {
                        subscriptionEvents.add(descriptionData.getEventDescriptionName());
                    }
                }
                return subscriptionEvents;
            }
            return null;
        } catch (IOException e) {
            return null;
        }
    }

    public static Observable<List<SubscriptionEvent>> getUserSubscriptions() {

        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
        NotificationService notificationService = serviceProvider.getNotificationService();

        return notificationService.getUserSubscriptions().map(new Func1<Response<NotificationSubscription>, List<SubscriptionEvent>>() {
            @Override
            public List<SubscriptionEvent> call(Response<NotificationSubscription> subscriptionResponse) {
                if (subscriptionResponse != null && subscriptionResponse.isSuccessful()) {
                    return parseSubscription(subscriptionResponse.body());
                }
                return null;
            }
        });
    }

    public static Observable<SubscriptionEvent> subscribeEvent(String eventDescName) {
        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
        NotificationService notificationService = serviceProvider.getNotificationService();

        return notificationService.subscribe(eventDescName).map(new Func1<Response<NotificationSubscription.Subscription>, SubscriptionEvent>() {
            @Override
            public SubscriptionEvent call(Response<NotificationSubscription.Subscription> subscriptionResponse) {
                if (subscriptionResponse != null && subscriptionResponse.isSuccessful()) {
                    return parseSubscriptionEvent(subscriptionResponse.body());
                }
                return null;
            }
        });
    }

    public static Observable<ResponseBody> setSubscriptionDestination(String subscriptionID) {
        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
        NotificationService notificationService = serviceProvider.getNotificationService();

        return notificationService.setSubscriptionDestination(subscriptionID);
    }

    private static List<SubscriptionEvent> parseSubscription(NotificationSubscription notificationSubscriptions) {
        if (notificationSubscriptions == null || notificationSubscriptions.getSubscriptions() == null) {
            return null;
        }

        List<SubscriptionEvent> userSubscriptions = new ArrayList<>();
        for (NotificationSubscription.Subscription subscription : notificationSubscriptions.getSubscriptions()) {
            SubscriptionEvent subscriptionEvent = parseSubscriptionEvent(subscription);
            if (subscriptionEvent != null) {
                userSubscriptions.add(subscriptionEvent);
            }
        }

        if (PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).isNPSNotificationsEnabled() &&
                PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).isNPSFeatureEnabled()) {
            SubscriptionEvent subscriptionEvent = new SubscriptionEvent();
            subscriptionEvent.setNotificationEventEnum(NotificationEventEnum.NPS_NOTIFICATIONS);
            subscriptionEvent.setUserSubscriptionID("");
            userSubscriptions.add(subscriptionEvent);
        }


        if (LocationBasedNotificationsManager.getInstance(PrintOSApplication.getAppContext()).locationBasedNotificationToggleOn()) {
            SubscriptionEvent subscriptionEvent = new SubscriptionEvent();
            subscriptionEvent.setNotificationEventEnum(NotificationEventEnum.LOCATION_BASED);
            subscriptionEvent.setUserSubscriptionID("");
            userSubscriptions.add(subscriptionEvent);
        }

        return userSubscriptions;
    }

    private static SubscriptionEvent parseSubscriptionEvent(NotificationSubscription.Subscription subscription) {
        if (subscription == null) {
            return null;
        }

        NotificationEventEnum eventEnum = NotificationEventEnum.from(subscription.getEventDescriptionId());
        if (eventEnum != NotificationEventEnum.UNKNOWN) {
            SubscriptionEvent subscriptionEvent = new SubscriptionEvent();
            subscriptionEvent.setNotificationEventEnum(eventEnum);
            subscriptionEvent.setUserSubscriptionID(subscription.getUserSubscriptionId());
            return subscriptionEvent;
        }

        return null;
    }

    public static Observable<ResponseBody> unsubscribeEvent(String eventSubID) {
        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
        NotificationService notificationService = serviceProvider.getNotificationService();

        return notificationService.unsubscribe(eventSubID).map(new Func1<ResponseBody, ResponseBody>() {
            @Override
            public ResponseBody call(ResponseBody responseBody) {
                return responseBody;
            }
        });
    }
}