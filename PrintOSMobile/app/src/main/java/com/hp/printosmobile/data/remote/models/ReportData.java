package com.hp.printosmobile.data.remote.models;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.hp.printosmobile.data.remote.services.PerformanceTrackingService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A model representing the response of {@link PerformanceTrackingService#getOverallReport(String, List, String, String, String, String)} endpoint.
 *
 * @author Osama Taha
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ReportData {

    @JsonProperty("kpiName")
    private String kpiName;
    @JsonProperty("averageLevel")
    private Integer averageLevel;
    @JsonProperty("greatLevel")
    private Integer greatLevel;
    @JsonProperty("fromDate")
    private String fromDate;
    @JsonProperty("toDate")
    private String toDate;
    @JsonProperty("unitEvents")
    private List<UnitEvent> unitEvents = new ArrayList<UnitEvent>();
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * @return The kpiName
     */
    @JsonProperty("kpiName")
    public String getKpiName() {
        return kpiName;
    }

    /**
     * @param kpiName The kpiName
     */
    @JsonProperty("kpiName")
    public void setKpiName(String kpiName) {
        this.kpiName = kpiName;
    }

    /**
     * @return The averageLevel
     */
    @JsonProperty("averageLevel")
    public Integer getAverageLevel() {
        return averageLevel;
    }

    /**
     * @param averageLevel The averageLevel
     */
    @JsonProperty("averageLevel")
    public void setAverageLevel(Integer averageLevel) {
        this.averageLevel = averageLevel;
    }

    /**
     * @return The greatLevel
     */
    @JsonProperty("greatLevel")
    public Integer getGreatLevel() {
        return greatLevel;
    }

    /**
     * @param greatLevel The greatLevel
     */
    @JsonProperty("greatLevel")
    public void setGreatLevel(Integer greatLevel) {
        this.greatLevel = greatLevel;
    }

    /**
     * @return The fromDate
     */
    @JsonProperty("fromDate")
    public String getFromDate() {
        return fromDate;
    }

    /**
     * @param fromDate The fromDate
     */
    @JsonProperty("fromDate")
    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    /**
     * @return The toDate
     */
    @JsonProperty("toDate")
    public String getToDate() {
        return toDate;
    }

    /**
     * @param toDate The toDate
     */
    @JsonProperty("toDate")
    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    /**
     * @return The unitEvents
     */
    @JsonProperty("unitEvents")
    public List<UnitEvent> getUnitEvents() {
        return unitEvents;
    }

    /**
     * @param unitEvents The unitEvents
     */
    @JsonProperty("unitEvents")
    public void setUnitEvents(List<UnitEvent> unitEvents) {
        this.unitEvents = unitEvents;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("ReportData{");
        sb.append("kpiName='").append(kpiName).append('\'');
        sb.append(", averageLevel=").append(averageLevel);
        sb.append(", greatLevel=").append(greatLevel);
        sb.append(", fromDate='").append(fromDate).append('\'');
        sb.append(", toDate='").append(toDate).append('\'');
        sb.append(", unitEvents=").append(unitEvents);
        sb.append(", additionalProperties=").append(additionalProperties);
        sb.append('}');
        return sb.toString();
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class UnitEvent {

        @JsonProperty("unitName")
        private String unitName;
        @JsonProperty("unitInfo")
        private String unitInfo;
        @JsonProperty("events")
        private List<Event> events = new ArrayList<Event>();
        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<String, Object>();

        /**
         * @return The unitName
         */
        @JsonProperty("unitName")
        public String getUnitName() {
            return unitName;
        }

        /**
         * @param unitName The unitName
         */
        @JsonProperty("unitName")
        public void setUnitName(String unitName) {
            this.unitName = unitName;
        }

        /**
         * @return The unitInfo
         */
        @JsonProperty("unitInfo")
        public String getUnitInfo() {
            return unitInfo;
        }

        /**
         * @param unitInfo The unitInfo
         */
        @JsonProperty("unitInfo")
        public void setUnitInfo(String unitInfo) {
            this.unitInfo = unitInfo;
        }

        /**
         * @return The events
         */
        @JsonProperty("events")
        public List<Event> getEvents() {
            return events;
        }

        /**
         * @param events The events
         */
        @JsonProperty("events")
        public void setEvents(List<Event> events) {
            this.events = events;
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }

        @Override
        public String toString() {
            final StringBuffer sb = new StringBuffer("UnitEvent{");
            sb.append("unitName='").append(unitName).append('\'');
            sb.append(", unitInfo='").append(unitInfo).append('\'');
            sb.append(", events=").append(events);
            sb.append(", additionalProperties=").append(additionalProperties);
            sb.append('}');
            return sb.toString();
        }

        @JsonInclude(JsonInclude.Include.NON_NULL)
        public static class Event {

            @JsonProperty("happenedOn")
            private String happenedOn;
            @JsonProperty("eventName")
            private String eventName;
            @JsonProperty("value")
            private Integer value;
            @JsonProperty("values")
            private List<Value> values = new ArrayList<Value>();
            @JsonProperty("unit")
            private String unit;
            @JsonProperty("count")
            private Integer count;
            @JsonProperty("rate")
            private Integer rate;
            @JsonProperty("guidelinesPercent")
            private Integer guidelinesPercent;
            @JsonProperty("weight")
            private Integer weight;
            @JsonIgnore
            private Map<String, Object> additionalProperties = new HashMap<String, Object>();

            /**
             * @return The happenedOn
             */
            @JsonProperty("happenedOn")
            public String getHappenedOn() {
                return happenedOn;
            }

            /**
             * @param happenedOn The happenedOn
             */
            @JsonProperty("happenedOn")
            public void setHappenedOn(String happenedOn) {
                this.happenedOn = happenedOn;
            }

            /**
             * @return The eventName
             */
            @JsonProperty("eventName")
            public String getEventName() {
                return eventName;
            }

            /**
             * @param eventName The eventName
             */
            @JsonProperty("eventName")
            public void setEventName(String eventName) {
                this.eventName = eventName;
            }

            /**
             * @return The value
             */
            @JsonProperty("value")
            public Integer getValue() {
                return value;
            }

            /**
             * @param value The value
             */
            @JsonProperty("value")
            public void setValue(Integer value) {
                this.value = value;
            }

            /**
             * @return The values
             */
            @JsonProperty("values")
            public List<Value> getValues() {
                return values;
            }

            /**
             * @param values The values
             */
            @JsonProperty("values")
            public void setValues(List<Value> values) {
                this.values = values;
            }

            /**
             * @return The unit
             */
            @JsonProperty("unit")
            public String getUnit() {
                return unit;
            }

            /**
             * @param unit The unit
             */
            @JsonProperty("unit")
            public void setUnit(String unit) {
                this.unit = unit;
            }

            /**
             * @return The count
             */
            @JsonProperty("count")
            public Integer getCount() {
                return count;
            }

            /**
             * @param count The count
             */
            @JsonProperty("count")
            public void setCount(Integer count) {
                this.count = count;
            }

            /**
             * @return The rate
             */
            @JsonProperty("rate")
            public Integer getRate() {
                return rate;
            }

            /**
             * @param rate The rate
             */
            @JsonProperty("rate")
            public void setRate(Integer rate) {
                this.rate = rate;
            }

            /**
             * @return The guidelinesPercent
             */
            @JsonProperty("guidelinesPercent")
            public Integer getGuidelinesPercent() {
                return guidelinesPercent;
            }

            /**
             * @param guidelinesPercent The guidelinesPercent
             */
            @JsonProperty("guidelinesPercent")
            public void setGuidelinesPercent(Integer guidelinesPercent) {
                this.guidelinesPercent = guidelinesPercent;
            }

            /**
             * @return The weight
             */
            @JsonProperty("weight")
            public Integer getWeight() {
                return weight;
            }

            /**
             * @param weight The weight
             */
            @JsonProperty("weight")
            public void setWeight(Integer weight) {
                this.weight = weight;
            }

            @JsonAnyGetter
            public Map<String, Object> getAdditionalProperties() {
                return this.additionalProperties;
            }

            @JsonAnySetter
            public void setAdditionalProperty(String name, Object value) {
                this.additionalProperties.put(name, value);
            }

            @Override
            public String toString() {
                final StringBuffer sb = new StringBuffer("Event{");
                sb.append("happenedOn='").append(happenedOn).append('\'');
                sb.append(", eventName='").append(eventName).append('\'');
                sb.append(", value=").append(value);
                sb.append(", values=").append(values);
                sb.append(", unit='").append(unit).append('\'');
                sb.append(", count=").append(count);
                sb.append(", rate=").append(rate);
                sb.append(", guidelinesPercent=").append(guidelinesPercent);
                sb.append(", weight=").append(weight);
                sb.append(", additionalProperties=").append(additionalProperties);
                sb.append('}');
                return sb.toString();
            }

            @JsonInclude(JsonInclude.Include.NON_NULL)
            public static class Value {

                @JsonProperty("name")
                private String name;
                @JsonProperty("value")
                private Double value;
                @JsonIgnore
                private Map<String, Object> additionalProperties = new HashMap<String, Object>();

                /**
                 * @return The name
                 */
                @JsonProperty("name")
                public String getName() {
                    return name;
                }

                /**
                 * @param name The name
                 */
                @JsonProperty("name")
                public void setName(String name) {
                    this.name = name;
                }

                /**
                 * @return The value
                 */
                @JsonProperty("value")
                public Double getValue() {
                    return value;
                }

                /**
                 * @param value The value
                 */
                @JsonProperty("value")
                public void setValue(Double value) {
                    this.value = value;
                }

                @JsonAnyGetter
                public Map<String, Object> getAdditionalProperties() {
                    return this.additionalProperties;
                }

                @JsonAnySetter
                public void setAdditionalProperty(String name, Object value) {
                    this.additionalProperties.put(name, value);
                }


                @Override
                public String toString() {
                    final StringBuffer sb = new StringBuffer("Value{");
                    sb.append("name='").append(name).append('\'');
                    sb.append(", value=").append(value);
                    sb.append(", additionalProperties=").append(additionalProperties);
                    sb.append('}');
                    return sb.toString();
                }
            }
        }
    }


}
