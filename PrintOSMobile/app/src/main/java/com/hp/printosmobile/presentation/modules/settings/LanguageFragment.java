package com.hp.printosmobile.presentation.modules.settings;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hp.printosmobile.AppseeSdk;
import com.hp.printosmobile.IntercomSdk;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.data.remote.models.VersionsData;
import com.hp.printosmobile.utils.ZipUtils;
import com.hp.printosmobilelib.core.utils.StringUtils;
import com.hp.printosmobilelib.ui.common.HPFragment;
import com.hp.printosmobilelib.ui.utils.DividerItemDecoration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LanguageFragment extends HPFragment {

    @Bind(R.id.language_list)
    RecyclerView languageList;

    LanguageAdapter languageAdapter;
    LanguageFragmentCallback callback;

    public static LanguageFragment getNewInstance(LanguageFragmentCallback callback) {
        LanguageFragment languageFragment = new LanguageFragment();
        if (callback != null) {
            languageFragment.callback = callback;
        }
        return languageFragment;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        IntercomSdk.getInstance(getActivity()).logEvent(IntercomSdk.PBM_VIEW_LANGUAGE_EVENT);
        AppseeSdk.getInstance(getActivity()).startScreen(AppseeSdk.SCREEN_LANGUAGES);

        initView();
    }

    private void initView() {

        List<String> languages = new ArrayList<>(Arrays.asList(this.getResources().getStringArray(R.array.language_display_name)));
        List<String> languageKeys = new ArrayList<>(Arrays.asList(this.getResources().getStringArray(R.array.language_key)));

        VersionsData versionsData = PrintOSPreferences.getInstance(getActivity()).getVersionsData();
        List<String> supportedLanguages = null;
        if (versionsData != null && versionsData.getConfiguration() != null) {
            supportedLanguages = versionsData.getConfiguration().getSupportedLanguages();
        }

        final List<String> finalSupportedLanguages = supportedLanguages;

        List<? extends ZipUtils.Pair<String, String>> languagesPairs = ZipUtils.zip(languageKeys, languages, new ZipUtils.ZipIterator<String, String>() {
            @Override
            public boolean each(String key, String value) {
                String languageCode = StringUtils.getSubStringBeforeSeparator(key, "-");
                return finalSupportedLanguages == null || finalSupportedLanguages.size() == 0 || finalSupportedLanguages.contains(languageCode);
            }
        });


        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        languageList.setLayoutManager(layoutManager);

        languageAdapter = new LanguageAdapter(languagesPairs);
        languageAdapter.setSelectedLanguageKey(PrintOSPreferences.getInstance(getActivity()).getLanguage(getString(R.string.default_language)));
        languageList.setAdapter(languageAdapter);
        languageList.addItemDecoration(new DividerItemDecoration(getActivity(), 1, languages.size(), false));


    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_languages;
    }

    @Override
    public boolean isFragmentNameToBeDisplayed() {
        return true;
    }

    @Override
    public boolean isFilteringAvailable() {
        return false;
    }

    @Override
    public int getToolbarDisplayName() {
        return R.string.language_title;
    }

    @OnClick(R.id.done_button)
    public void onLanguageSelected() {
        if (callback != null && languageAdapter != null) {
            callback.onLanguageChanged(languageAdapter.getSelectedLanguageKey());
        }
    }

    public class LanguageAdapter extends RecyclerView.Adapter<LanguageAdapter.LanguageViewHolder> {

        private List<? extends ZipUtils.Pair<String, String>> languages;
        private int selectedIndex;

        public LanguageAdapter(List<? extends ZipUtils.Pair<String, String>> languagesPairs) {
            this.languages = languagesPairs;
            this.selectedIndex = -1;
        }

        @Override
        public LanguageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            ViewGroup view = (ViewGroup) inflater.inflate(R.layout.fragment_language_item, parent, false);
            LanguageViewHolder languageViewHolder = new LanguageViewHolder(view);
            return languageViewHolder;
        }

        @Override
        public void onBindViewHolder(LanguageViewHolder holder, int position) {
            String language = languages.get(position).getValue();
            holder.languageName.setText(language);
            holder.index = position;

            holder.viewItem.setSelected(selectedIndex == position);
            holder.checkImage.setVisibility(selectedIndex == position ? View.VISIBLE : View.GONE);
        }

        @Override
        public int getItemCount() {
            return languages == null ? 0 : languages.size();
        }

        public String getSelectedLanguageKey() {
            return languages != null && selectedIndex >= 0 && selectedIndex < languages.size() ?
                    languages.get(selectedIndex).getKey() : null;
        }

        public void setSelectedLanguageKey(String selectedLanguageKey) {
            if (languages != null && selectedLanguageKey != null) {
                for (int i = 0; i < languages.size(); i++) {
                    if (selectedLanguageKey.equals(languages.get(i).getKey())) {
                        selectedIndex = i;
                        break;
                    }
                }
            }
        }

        public class LanguageViewHolder extends RecyclerView.ViewHolder {

            @Bind(R.id.language_name_text_view)
            TextView languageName;
            @Bind(R.id.checked_image)
            View checkImage;

            View viewItem;
            int index;

            public LanguageViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
                viewItem = itemView;

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        selectedIndex = index;
                        notifyDataSetChanged();
                    }
                });
            }
        }
    }

    public interface LanguageFragmentCallback {
        void onLanguageChanged(String selectedLanguageKey);
    }

}
