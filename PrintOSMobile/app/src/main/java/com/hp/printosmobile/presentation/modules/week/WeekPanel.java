package com.hp.printosmobile.presentation.modules.week;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.util.AttributeSet;
import android.view.View;

import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.modules.home.Panel;
import com.hp.printosmobile.presentation.modules.kpiview.KPIView;
import com.hp.printosmobile.presentation.modules.kpiview.KPIViewModel;
import com.hp.printosmobile.presentation.modules.shared.PanelShareCallbacks;
import com.hp.printosmobile.presentation.modules.week.kpiexplanationdialog.KpiExplanationDialog;
import com.hp.printosmobile.utils.FileUtils;
import com.hp.printosmobilelib.ui.widgets.panel.PanelView;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Anwar Asbah on 4/12/2016.
 */
public class WeekPanel extends PanelView<WeekCollectionViewModel> implements WeekAdapter.WeekViewCallback {

    public final static String TAG = WeekPanel.class.getSimpleName();
    private static final String WEEK_PANEL_SCREEN_SHOT_FILE_NAME = "PrintOS";

    @Bind(R.id.week_pager)
    ViewPager weekPager;
    @Bind(R.id.week_pager_indicator)
    CirclePageIndicator weekPagerIndicator;
    View shareButton;

    private WeekAdapter weekAdapter;
    private WeekPanelCallbacks callback;

    public WeekPanel(Context context) {
        super(context);
        bindViews();
    }

    public WeekPanel(Context context, AttributeSet attrs) {
        super(context, attrs);
        bindViews();
    }

    public WeekPanel(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        bindViews();
    }

    private void bindViews() {
        ButterKnife.bind(this, getView());

        weekPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                PagerAdapter pagerAdapter = weekPager.getAdapter();
                if (pagerAdapter != null) {
                    int numOfWeeks = weekAdapter.getCount();
                    int diff = numOfWeeks - position - 1;
                    Analytics.sendEvent(Analytics.MOBILE_PRINT_BEAT_CATEGORY, Analytics.WEEK_VIEW_ACTION,
                            String.format(Analytics.WEEK_VIEW_SHOW_LABEL, diff));
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        View headerLeftView = inflate(getContext(), R.layout.week_panel_header_left_view, getHeaderLeftView());
        shareButton = headerLeftView.findViewById(R.id.share_button);
        shareButton.setVisibility(GONE);
        shareButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (callback != null) {
                    callback.onShareButtonClicked(Panel.PERFORMANCE);
                }
            }
        });

    }

    public void sharePanel() {

        if (callback != null && weekAdapter != null && weekAdapter.getCount() > 0) {

            View weekView = weekPager.findViewWithTag(weekPager.getCurrentItem());

            if (weekView == null) {
                return;
            }

            lockShareButton(true);

            RecyclerView kpiRecyclerView = (RecyclerView) weekView.findViewById(R.id.kpis_layout);
            if (kpiRecyclerView != null) {
                for (int i = 0; i < kpiRecyclerView.getChildCount(); i++) {
                    KPIView kpiItem = (KPIView) kpiRecyclerView.getChildAt(i);
                    kpiItem.hidePoints(true);
                }
            }
            weekView.setBackgroundColor(ResourcesCompat.getColor(getResources(), R.color.panel_view_bg_color, null));

            Bitmap weekPanelBitmap = FileUtils.getScreenShot(weekView);
            Uri storageUri = FileUtils.store(weekPanelBitmap, WEEK_PANEL_SCREEN_SHOT_FILE_NAME);

            if (kpiRecyclerView != null) {
                for (int i = 0; i < kpiRecyclerView.getChildCount(); i++) {
                    KPIView kpiItem = (KPIView) kpiRecyclerView.getChildAt(i);
                    kpiItem.hidePoints(false);
                }
            }
            weekView.setBackgroundColor(ResourcesCompat.getColor(getResources(), android.R.color.transparent, null));

            callback.onScreenshotCreated(Panel.PERFORMANCE, storageUri);
        }
    }

    public void lockShareButton(boolean lock) {
        shareButton.setEnabled(!lock);
        shareButton.setClickable(!lock);
    }

    @Override
    public Spannable getTitleSpannable() {
        SpannableStringBuilder builder = new SpannableStringBuilder(getContext().getString(R.string.week_panel_title));
        return builder;
    }

    @Override
    public int getContentView() {
        return R.layout.week_content;
    }

    @Override
    public void updateViewModel(WeekCollectionViewModel viewModel) {

        setViewModel(viewModel);

        if (showEmptyCard()) {
            return;
        }

        weekAdapter = new WeekAdapter(getContext(), viewModel, this);
        weekPager.setAdapter(weekAdapter);
        weekPagerIndicator.setViewPager(weekPager);

        weekPagerIndicator.setVisibility(GONE);
        if (weekAdapter.getCount() > 1) {
            weekPagerIndicator.setCurrentItem(weekAdapter.getCount() - 1);
            weekPagerIndicator.setVisibility(VISIBLE);
            shareButton.setVisibility(VISIBLE);
        }

    }

    @Override
    protected boolean isValidViewModel() {
        return !(getViewModel() == null || getViewModel().getWeeks() == null || getViewModel().getWeeks().size() == 0);
    }

    @Override
    protected String getEmptyCardText() {
        return getContext().getString(R.string.no_information_to_display);
    }

    public void addWeekPanelListener(WeekPanelCallbacks callback) {
        this.callback = callback;

    }

    @Override
    public void onKPISelected(String kpiName) {
        if (callback != null) {
            callback.onKpiSelected(kpiName);
        }
    }

    @Override
    public void onWeekSelected() {
        if (callback != null) {
            Analytics.sendEvent(Analytics.MOBILE_PRINT_BEAT_CATEGORY, Analytics.CHART_CLICKED_ACTION);

            callback.onWeekPanelClicked();
        }
    }

    @Override
    public void onPartialDataWarningClicked(List<WeekViewModel.WeekPressStatus> weekPressStatuses) {
        if (callback != null) {
            callback.onPartialDataWeekClicked(weekPressStatuses);
        }
    }

    @Override
    public void showKpiExplanationDialog(ArrayList<KPIViewModel> kpiList, int position) {
        if (callback != null) {
            callback.showKpiExplanationDialog(kpiList, position);
        }
    }

    public interface WeekPanelCallbacks extends PanelShareCallbacks {

        void onKpiSelected(String kpiName);

        void onWeekPanelClicked();

        void onPartialDataWeekClicked (List<WeekViewModel.WeekPressStatus> weekPressStatuses);

        void showKpiExplanationDialog(ArrayList<KPIViewModel> kpiList, int position);

    }
}
