package com.hp.printosmobile.presentation.modules.week.kpiexplanationdialog;

import com.hp.printosmobile.presentation.modules.kpiview.KPIScoreStateEnum;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Anwar Asbah on 8/7/2017.
 */
public class KpiExplanationViewModel implements Serializable {

    private int min;
    private int max;
    private int value;
    private int limit;
    private int score;
    private KpiExplanationEnum kpiExplanationEnum;
    private KPIScoreStateEnum stateEnum;
    private String descriptionText;
    private List<Property> properties;

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getDescriptionText() {
        return descriptionText;
    }

    public void setDescriptionText(String descriptionText) {
        this.descriptionText = descriptionText;
    }

    public List<Property> getProperties() {
        return properties;
    }

    public void setProperties(List<Property> properties) {
        this.properties = properties;
    }

    public KpiExplanationEnum getKpiExplanationEnum() {
        return kpiExplanationEnum;
    }

    public void setKpiExplanationEnum(KpiExplanationEnum kpiExplanationEnum) {
        this.kpiExplanationEnum = kpiExplanationEnum;
    }

    public KPIScoreStateEnum getStateEnum() {
        return stateEnum;
    }

    public void setStateEnum(KPIScoreStateEnum stateEnum) {
        this.stateEnum = stateEnum;
    }

    public static class Property implements Serializable {

        private int StringID;
        private String value;

        public int getStringID() {
            return StringID;
        }

        public void setStringID(int stringID) {
            StringID = stringID;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }
}