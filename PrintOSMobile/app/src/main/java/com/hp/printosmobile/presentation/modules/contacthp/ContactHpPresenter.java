package com.hp.printosmobile.presentation.modules.contacthp;

import android.content.Context;
import android.net.Uri;

import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.data.remote.models.ContactHpCase;
import com.hp.printosmobile.data.remote.models.ContactHpCase.ContactHpPreferredContactMethod.ContactMethodType;
import com.hp.printosmobile.data.remote.models.ContactHpCase.ContactHpUserEntry.UserEntryType;
import com.hp.printosmobile.presentation.Presenter;
import com.hp.printosmobile.presentation.modules.contacthp.shared.ContactHpContactThroughView;
import com.hp.printosmobilelib.core.communications.remote.APIException;
import com.hp.printosmobilelib.core.logging.HPLogger;
import com.hp.printosmobilelib.core.utils.DeviceUtils;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by anwar asbah on 10/12/2016.
 */
public class ContactHpPresenter extends Presenter<ContactHpView> {
    public static final String TAG = ContactHpPresenter.class.getSimpleName();
    public static final String APPLICATION_ENTRY_VALUE = "Printbeat";

    public void hasPhone() {
        ContactHpManager.hasPhone()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Boolean>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        HPLogger.d(TAG, "Error while exploring User info " + e);
                        onRequestError(e, ContactHPBaseActivity.TAG);
                        mView.setHasPhone(false);
                    }

                    @Override
                    public void onNext(Boolean hasPhone) {
                        if (hasPhone != null && hasPhone.booleanValue()) {
                            mView.setHasPhone(true);
                        } else {
                            mView.setHasPhone(false);
                        }
                    }
                });
    }

    public void askQuestion(Context context, String subject, String question, List<Uri> attachments,
                            ContactHpContactThroughView.ContactMethod contactMethodType) {

        ContactHpCase.CaseType caseType = ContactHpCase.CaseType.ASK_A_QUESTION;

        List<ContactHpCase.ContactHpUserEntry> entries = new ArrayList<>();
        entries.add(getEntry(UserEntryType.APPLICATION, APPLICATION_ENTRY_VALUE));
        entries.add(getEntry(UserEntryType.DESCRIPTION, question));

        sendRequest(context, caseType, subject, entries, attachments, getContactMethodType(contactMethodType));
    }

    public void sendFeedback(Context context, String subject, String description, List<Uri> attachments,
                             ContactHpContactThroughView.ContactMethod contactMethodType, boolean allowContact,
                             FeedbackActivity.FeedbackEnum feedbackEnum) {

        ContactHpCase.CaseType caseType = ContactHpCase.CaseType.FEEDBACK_TO_HP;

        List<ContactHpCase.ContactHpUserEntry> entries = new ArrayList<>();
        entries.add(getEntry(UserEntryType.DESCRIPTION, description));
        entries.add(getEntry(UserEntryType.ALLOW_CONTACT, String.valueOf(allowContact)));
        entries.add(getEntry(UserEntryType.FEELING, feedbackEnum.getDescriptiveValue()));

        sendRequest(context, caseType, subject, entries, attachments, getContactMethodType(contactMethodType));
    }


    public void reportAProblem(Context context, String subject, String howYouGotHere,
                               String whatHappened, String expected, List<Uri> attachments,
                               ContactHpContactThroughView.ContactMethod contactMethodType,
                               boolean allowContact) {

        ContactHpCase.CaseType caseType = ContactHpCase.CaseType.REPORT_A_PROBLEM;

        List<ContactHpCase.ContactHpUserEntry> entries = new ArrayList<>();
        entries.add(getEntry(UserEntryType.APPLICATION, APPLICATION_ENTRY_VALUE));
        entries.add(getEntry(UserEntryType.PATH_TO_PROBLEM, howYouGotHere));
        entries.add(getEntry(UserEntryType.DESCRIPTION, whatHappened));
        entries.add(getEntry(UserEntryType.EXPECTED_OUTCOME, expected));
        entries.add(getEntry(UserEntryType.ALLOW_CONTACT, String.valueOf(allowContact)));

        sendRequest(context, caseType, subject, entries, attachments, getContactMethodType(contactMethodType));
    }

    private void sendRequest(Context context, ContactHpCase.CaseType caseType, String subject,
                             List<ContactHpCase.ContactHpUserEntry> entries, List<Uri> attachments,
                             ContactMethodType contactMethodType) {

        if (caseType == ContactHpCase.CaseType.ASK_A_QUESTION) {
            HPLogger.d(TAG, String.format("user clicked token - ask a question - <%s>", DeviceUtils.getDeviceIdMD5(context)));
        } else if (caseType == ContactHpCase.CaseType.REPORT_A_PROBLEM) {
            HPLogger.d(TAG, String.format("user clicked token - report a problem - <%s>", DeviceUtils.getDeviceIdMD5(context)));
        }

        ContactHpManager.sendRequest(context, caseType, subject, entries, attachments, contactMethodType)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ResponseBody>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        HPLogger.d(TAG, "Error while sending Contact Hp request " + e);
                        onRequestError(e, ContactHPBaseActivity.TAG);
                    }

                    @Override
                    public void onNext(ResponseBody responseBody) {
                        try {
                            responseBody.string();
                            mView.onSuccess();
                        } catch (Exception e) {
                            onRequestError(null, ContactHPBaseActivity.TAG);
                        }

                    }
                });
    }

    private static ContactMethodType getContactMethodType(ContactHpContactThroughView.ContactMethod contactMethod) {
        if (contactMethod == null) {
            return ContactMethodType.NO_CONTACT;
        }

        switch (contactMethod) {
            case PHONE:
                return ContactMethodType.PROFILE_PHONE;
            case EMAIL:
                return ContactMethodType.PROFILE_EMAIL;
            default:
                return ContactMethodType.NO_CONTACT;
        }
    }

    private static ContactHpCase.ContactHpUserEntry getEntry(UserEntryType type, String value) {
        ContactHpCase.ContactHpUserEntry entry = new ContactHpCase.ContactHpUserEntry();
        entry.setUserEntryType(type);
        entry.setValue(appendDeviceTokenToBody(type, value));

        return entry;
    }

    private static String appendDeviceTokenToBody(UserEntryType type, String body) {

        if (type == UserEntryType.DESCRIPTION) {
            return String.format("%s \n token: < %s >", body,
                    DeviceUtils.getDeviceIdMD5(PrintOSApplication.getAppContext()));
        }

        return body;
    }


    private void onRequestError(Throwable e, String tag) {
        APIException exception = APIException.create(APIException.Kind.UNEXPECTED);
        if (e instanceof APIException) {
            exception = (APIException) e;
        }

        mView.onError(exception, tag);
    }

    @Override
    public void detachView() {
        mView = null;
    }
}
