package com.hp.printosmobile.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Environment;

import com.hp.printosmobilelib.core.logging.HPLogger;

import java.util.ArrayList;
import java.util.List;

/**
 * A Helper class containing utils for sending emails via Intent.
 * <p/>
 * Created by Osama Taha on 11/21/15.
 */
public class EmailUtils {

    private static final String TAG = EmailUtils.class.getSimpleName();

    private static final String EMAIL_INTENT_TYPE = "message/rfc822";
    // Constants to discriminate Gmail application.
    private static final String GMAIL_PACKAGE_INDICATOR = ".gm";
    private static final String GMAIL_ACTIVITY_INDICATOR = "gmail";
    private static final int OPEN_MAIL_APP_INTENT_REQUEST_CODE = 2332;

    /**
     * Send email via intent.
     *
     * @param activity        represents the activity calling this method.
     * @param to              array of recipients.
     * @param subject         email subject.
     * @param body            email body.
     * @param attachmentsUris list of attachments URIs.
     */
    public static void sendEmail(Activity activity, String[] to, String subject, String body,
                                 ArrayList<Uri> attachmentsUris) {

        Intent intent = new Intent(Intent.ACTION_SEND_MULTIPLE);
        intent.setType(EMAIL_INTENT_TYPE);
        intent.putExtra(Intent.EXTRA_EMAIL, to);
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(Intent.EXTRA_TEXT, body);
        intent.addFlags(Intent.FLAG_ACTIVITY_FORWARD_RESULT);

        // Check if the status of external storage available to read/write.
        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            intent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, attachmentsUris);
            activity.startActivityForResult(Intent.createChooser(intent, ""), OPEN_MAIL_APP_INTENT_REQUEST_CODE);
        } else {
            HPLogger.d(TAG, "Unable to attach files, the external storage is not available to read and write");
        }

    }

    /**
     * Check if the Gmail application found on the device & update the given
     * intent depends on it.
     *
     * @param context application context.
     * @param intent  email intent.
     * @return updated intent if needed.
     */
    private static Intent checkGmailApp(Context context, Intent intent) {

        // Get all packages that can handle this intent.
        final PackageManager packageManager = context.getPackageManager();
        final List<ResolveInfo> matches = packageManager.queryIntentActivities(intent, 0);
        ResolveInfo targetActivityInfo = null;

        for (final ResolveInfo info : matches) {

            // Check if the Gmail package is one of this packages.
            if (info.activityInfo.packageName.endsWith(GMAIL_PACKAGE_INDICATOR)
                    || info.activityInfo.name.toLowerCase().contains(
                    GMAIL_ACTIVITY_INDICATOR)) {

                targetActivityInfo = info;
                break;
            }
        }

        // if Gmail found use it.
        if (targetActivityInfo != null) {

            intent.setClassName(targetActivityInfo.activityInfo.packageName,
                    targetActivityInfo.activityInfo.name);
        }

        return intent;
    }

    public void sendFilesThroughEmail(Activity activity, String emailAddress, String subject, String body, ArrayList<Uri> attachmentsUris) {
        sendEmail(activity, new String[]{emailAddress}, subject, body, attachmentsUris);
    }

}