package com.hp.printosmobile.presentation.modules.drawer;

import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.presentation.Presenter;
import com.hp.printosmobile.utils.HPLocaleUtils;
import com.hp.printosmobile.utils.HPUIUtils;
import com.hp.printosmobilelib.core.communications.remote.APIException;
import com.hp.printosmobilelib.core.communications.remote.models.OrganizationJsonBody;
import com.hp.printosmobilelib.core.logging.HPLogger;

import java.util.List;

import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Osama Taha on 5/20/16.
 */
public class NavigationPresenter extends Presenter<NavView> {

    public static final String TAG = NavigationPresenter.class.getSimpleName();

    public void getOrganizations() {
        Subscription subscription = NavigationManager.getOrganizations()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<List<OrganizationViewModel>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        HPLogger.d("TAG", "Error while retrieving organizations data due to " + e.getMessage());
                        mView.onOrganizationsRetrieved(null);
                    }

                    @Override
                    public void onNext(List<OrganizationViewModel> response) {
                        mView.onOrganizationsRetrieved(response);
                    }
                });
        addSubscriber(subscription);
    }

    public void changeContext(final OrganizationViewModel viewModel) {

        Subscription subscription = NavigationManager.changeContext(viewModel.getOrganizationJsonBody())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<String>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        HPLogger.d(TAG, "Error while retrieving organizations data due to " + e.getMessage());
                        APIException exception;
                        if(e instanceof APIException) {
                            exception = (APIException) e;
                        } else {
                            exception = APIException.create(APIException.Kind.UNEXPECTED);
                        }
                        mView.onContextChanged(false, null, exception);
                    }

                    @Override
                    public void onNext(String response) {
                        mView.onContextChanged(true, viewModel, null);
                    }
                });

        addSubscriber(subscription);
    }

    @Override
    public void detachView() {
        stopSubscribers();
        mView = null;
    }
}
