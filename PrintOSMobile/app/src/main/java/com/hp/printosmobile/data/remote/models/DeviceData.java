package com.hp.printosmobile.data.remote.models;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.hp.printosmobile.data.remote.services.ConfigService;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A model representing the response of {@link ConfigService#getConfigDevices()} endpoint.
 * <p>
 *
 * @author Osama Taha
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonAutoDetect
public class DeviceData extends BaseDeviceData implements Serializable {

    private String deviceId;
    @JsonProperty("SerialNumber")
    private String SerialNumber;
    @JsonProperty("serialNumberDisplay")
    private String serialNumberDisplay;
    @JsonProperty("PressDescription")
    private String PressDescription;
    @JsonProperty("PressModel")
    private String PressModel;
    @JsonProperty("organizationID")
    private String organizationID;
    @JsonProperty("deviceName")
    private String deviceName;
    @JsonProperty("deviceNickName")
    private String deviceNickName;
    @JsonProperty("businessUnit")
    private String businessUnit;
    @JsonProperty("impressionType")
    private String impressionType;
    @JsonProperty("sortPosition")
    private Integer sortPosition;
    @JsonProperty("groups")
    private List<String> groups;
    @JsonProperty("isRtSupported")
    private boolean isRtSupported;
    @JsonProperty("site")
    private Site site;
    @JsonProperty("message")
    private Message message;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public DeviceData() {
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    /**
     * @return The SerialNumber
     */
    @JsonProperty("SerialNumber")
    public String getSerialNumber() {
        return SerialNumber;
    }

    /**
     * @param SerialNumber The SerialNumber
     */
    @JsonProperty("SerialNumber")
    public void setSerialNumber(String SerialNumber) {
        this.SerialNumber = SerialNumber;
    }

    /**
     * @return The PressDescription
     */
    @JsonProperty("PressDescription")
    public String getPressDescription() {
        return PressDescription;
    }

    /**
     * @param PressDescription The PressDescription
     */
    @JsonProperty("PressDescription")
    public void setPressDescription(String PressDescription) {
        this.PressDescription = PressDescription;
    }

    /**
     * @return The PressModel
     */
    @JsonProperty("PressModel")
    public String getPressModel() {
        return PressModel;
    }

    /**
     * @param PressModel The PressModel
     */
    @JsonProperty("PressModel")
    public void setPressModel(String PressModel) {
        this.PressModel = PressModel;
    }

    /**
     * @return The organizationID
     */
    @JsonProperty("organizationID")
    public String getOrganizationID() {
        return organizationID;
    }

    /**
     * @param organizationID The organizationID
     */
    @JsonProperty("organizationID")
    public void setOrganizationID(String organizationID) {
        this.organizationID = organizationID;
    }

    /**
     * @return The deviceName
     */
    @JsonProperty("deviceName")
    public String getDeviceName() {
        return deviceName;
    }

    /**
     * @param deviceName The deviceName
     */
    @JsonProperty("deviceName")
    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    /**
     * @return The deviceNickName
     */
    @JsonProperty("deviceNickName")
    public String getDeviceNickName() {
        return deviceNickName;
    }

    /**
     * @param deviceNickName The deviceNickName
     */
    @JsonProperty("deviceNickName")
    public void setDeviceNickName(String deviceNickName) {
        this.deviceNickName = deviceNickName;
    }

    /**
     * @return The businessUnit
     */
    @JsonProperty("businessUnit")
    public String getBusinessUnit() {
        return businessUnit;
    }

    /**
     * @param businessUnit The businessUnit
     */
    @JsonProperty("businessUnit")
    public void setBusinessUnit(String businessUnit) {
        this.businessUnit = businessUnit;
    }

    public void setImpressionType(String impressionType) {
        this.impressionType = impressionType;
    }

    public String getImpressionType() {
        return impressionType;
    }

    public void setRtSupported(boolean rtSupported) {
        isRtSupported = rtSupported;
    }

    public boolean isRtSupported() {
        return isRtSupported;
    }

    public void setSortPosition(Integer sortPosition) {
        this.sortPosition = sortPosition;
    }

    public int getSortPosition() {
        return sortPosition == null? -1 : sortPosition.intValue();
    }

    public void setGroups(List<String> groups) {
        this.groups = groups;
    }

    public List<String> getGroups() {
        return groups;
    }

    public void setSite(Site site) {
        this.site = site;
    }

    public Site getSite() {
        return site;
    }

    @JsonProperty("message")
    public Message getMessage() {
        return message;
    }

    @JsonProperty("message")
    public void setMessage(Message message) {
        this.message = message;
    }

    @JsonProperty("serialNumberDisplay")
    public String getSerialNumberDisplay() {
        return serialNumberDisplay;
    }

    @JsonProperty("serialNumberDisplay")
    public void setSerialNumberDisplay(String serialNumberDisplay) {
        this.serialNumberDisplay = serialNumberDisplay;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("DeviceData{");
        sb.append("SerialNumber='").append(SerialNumber).append('\'');
        sb.append(", PressDescription='").append(PressDescription).append('\'');
        sb.append(", PressModel='").append(PressModel).append('\'');
        sb.append(", organizationID='").append(organizationID).append('\'');
        sb.append(", deviceName='").append(deviceName).append('\'');
        sb.append(", deviceNickName='").append(deviceNickName).append('\'');
        sb.append(", businessUnit='").append(businessUnit).append('\'');
        sb.append(", additionalProperties=").append(additionalProperties);
        sb.append('}');
        return sb.toString();
    }

    public class Site implements Serializable {

        @JsonProperty("siteID")
        private String siteID;
        @JsonProperty("siteName")
        private String siteName;
        @JsonProperty("siteTZ")
        private String siteTZ;

        @JsonProperty("siteID")
        public String getSiteID() {
            return siteID;
        }

        @JsonProperty("siteID")
        public void setSiteID(String siteID) {
            this.siteID = siteID;
        }

        @JsonProperty("siteName")
        public String getSiteName() {
            return siteName;
        }

        @JsonProperty("siteName")
        public void setSiteName(String siteName) {
            this.siteName = siteName;
        }

        @JsonProperty("siteTZ")
        public String getSiteTZ() {
            return siteTZ;
        }

        @JsonProperty("siteTZ")
        public void setSiteTZ(String siteTZ) {
            this.siteTZ = siteTZ;
        }

        @Override
        public String toString() {
            final StringBuffer sb = new StringBuffer("Site{");
            sb.append("siteID='").append(siteID).append('\'');
            sb.append(", siteName='").append(siteName).append('\'');
            sb.append(", siteTZ='").append(siteTZ).append('\'');
            sb.append('}');
            return sb.toString();
        }
    }

    public class Message implements Serializable {

        @JsonProperty("localizedMsg")
        private String localizedMsg;
        @JsonProperty("msgId")
        private int msgId;

        @JsonProperty("localizedMsg")
        public String getLocalizedMsg() {
            return localizedMsg;
        }

        @JsonProperty("localizedMsg")
        public void setLocalizedMsg(String localizedMsg) {
            this.localizedMsg = localizedMsg;
        }

        @JsonProperty("msgId")
        public int getMsgId() {
            return msgId;
        }

        @JsonProperty("msgId")
        public void setMsgId(int msgId) {
            this.msgId = msgId;
        }

        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<String, Object>();

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }

        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder("Message{");
            sb.append("localizedMsg='").append(localizedMsg).append('\'');
            sb.append(", msgId=").append(msgId);
            sb.append(", additionalProperties=").append(additionalProperties);
            sb.append('}');
            return sb.toString();
        }
    }
}