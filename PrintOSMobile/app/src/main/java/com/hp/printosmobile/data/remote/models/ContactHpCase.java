package com.hp.printosmobile.data.remote.models;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ContactHpCase {
    public enum CaseType {
        ASK_A_QUESTION,
        ANONYMOUS_QUESTION,
        FEEDBACK_TO_HP,
        REPORT_A_PROBLEM
    }

    public enum CaseSource {
        WEB,
        MOBILE
    }

    @JsonProperty("id")
    private String id;
    @JsonProperty("type")
    private CaseType type;
    @JsonProperty("organizationId")
    private String organizationId;
    @JsonProperty("userId")
    private String userId;
    @JsonProperty("subject")
    private String subject;
    @JsonProperty("userEntries")
    private List<ContactHpUserEntry> userEntries = new ArrayList<ContactHpUserEntry>();
    @JsonProperty("createdAt")
    private String createdAt;
    @JsonProperty("caseSource")
    private CaseSource caseSource;
    @JsonProperty("careforceState")
    private ContactHpCareForceState careforceState;
    @JsonProperty("preferredContactMethod")
    private ContactHpPreferredContactMethod preferredContactMethod;
    @JsonProperty("attachments")
    private List<ContactHpAttachment> attachments = new ArrayList<ContactHpAttachment>();
    @JsonProperty("userSystemInfo")
    private ContactHpUserSystemInfo userSystemInfo;
    @JsonProperty("emailSupportStatus")
    private String emailSupportStatus;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * @return The id
     */
    @JsonProperty("id")
    public String getId() {
        return id;
    }

    /**
     * @param id The id
     */
    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return The type
     */
    @JsonProperty("type")
    public CaseType getType() {
        return type;
    }

    /**
     * @param type The type
     */
    @JsonProperty("type")
    public void setType(CaseType type) {
        this.type = type;
    }

    /**
     * @return The organizationId
     */
    @JsonProperty("organizationId")
    public String getOrganizationId() {
        return organizationId;
    }

    /**
     * @param organizationId The organizationId
     */
    @JsonProperty("organizationId")
    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }

    /**
     * @return The userId
     */
    @JsonProperty("userId")
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId The userId
     */
    @JsonProperty("userId")
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * @return The subject
     */
    @JsonProperty("subject")
    public String getSubject() {
        return subject;
    }

    /**
     * @param subject The subject
     */
    @JsonProperty("subject")
    public void setSubject(String subject) {
        this.subject = subject;
    }

    /**
     * @return The userEntries
     */
    @JsonProperty("userEntries")
    public List<ContactHpUserEntry> getUserEntries() {
        return userEntries;
    }

    /**
     * @param userEntries The userEntries
     */
    @JsonProperty("userEntries")
    public void setUserEntries(List<ContactHpUserEntry> userEntries) {
        this.userEntries = userEntries;
    }

    /**
     * @return The createdAt
     */
    @JsonProperty("createdAt")
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * @param createdAt The createdAt
     */
    @JsonProperty("createdAt")
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * @return The caseSource
     */
    @JsonProperty("caseSource")
    public CaseSource getCaseSource() {
        return caseSource;
    }

    /**
     * @param caseSource The caseSource
     */
    @JsonProperty("caseSource")
    public void setCaseSource(CaseSource caseSource) {
        this.caseSource = caseSource;
    }

    /**
     * @return The careforceState
     */
    @JsonProperty("careforceState")
    public ContactHpCareForceState getCareforceState() {
        return careforceState;
    }

    /**
     * @param careforceState The careforceState
     */
    @JsonProperty("careforceState")
    public void setCareforceState(ContactHpCareForceState careforceState) {
        this.careforceState = careforceState;
    }

    /**
     * @return The preferredContactMethod
     */
    @JsonProperty("preferredContactMethod")
    public ContactHpPreferredContactMethod getPreferredContactMethod() {
        return preferredContactMethod;
    }

    /**
     * @param preferredContactMethod The preferredContactMethod
     */
    @JsonProperty("preferredContactMethod")
    public void setPreferredContactMethod(ContactHpPreferredContactMethod preferredContactMethod) {
        this.preferredContactMethod = preferredContactMethod;
    }

    /**
     * @return The attachments
     */
    @JsonProperty("attachments")
    public List<ContactHpAttachment> getAttachments() {
        return attachments;
    }

    /**
     * @param attachments The attachments
     */
    @JsonProperty("attachments")
    public void setAttachments(List<ContactHpAttachment> attachments) {
        this.attachments = attachments;
    }

    /**
     * @return The userSystemInfo
     */
    @JsonProperty("userSystemInfo")
    public ContactHpUserSystemInfo getUserSystemInfo() {
        return userSystemInfo;
    }

    /**
     * @param userSystemInfo The userSystemInfo
     */
    @JsonProperty("userSystemInfo")
    public void setUserSystemInfo(ContactHpUserSystemInfo userSystemInfo) {
        this.userSystemInfo = userSystemInfo;
    }

    /**
     * @return The emailSupportStatus
     */
    @JsonProperty("emailSupportStatus")
    public String getEmailSupportStatus() {
        return emailSupportStatus;
    }

    /**
     * @param emailSupportStatus The emailSupportStatus
     */
    @JsonProperty("emailSupportStatus")
    public void setEmailSupportStatus(String emailSupportStatus) {
        this.emailSupportStatus = emailSupportStatus;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }


    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonPropertyOrder({
            "browser",
            "browserVersion",
            "osVersion"
    })
    public static class ContactHpUserSystemInfo {

        @JsonProperty("browser")
        private String browser;
        @JsonProperty("browserVersion")
        private String browserVersion;
        @JsonProperty("osVersion")
        private String osVersion;
        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<String, Object>();

        /**
         * @return The browser
         */
        @JsonProperty("browser")
        public String getBrowser() {
            return browser;
        }

        /**
         * @param browser The browser
         */
        @JsonProperty("browser")
        public void setBrowser(String browser) {
            this.browser = browser;
        }

        /**
         * @return The browserVersion
         */
        @JsonProperty("browserVersion")
        public String getBrowserVersion() {
            return browserVersion;
        }

        /**
         * @param browserVersion The browserVersion
         */
        @JsonProperty("browserVersion")
        public void setBrowserVersion(String browserVersion) {
            this.browserVersion = browserVersion;
        }

        /**
         * @return The osVersion
         */
        @JsonProperty("osVersion")
        public String getOsVersion() {
            return osVersion;
        }

        /**
         * @param osVersion The osVersion
         */
        @JsonProperty("osVersion")
        public void setOsVersion(String osVersion) {
            this.osVersion = osVersion;
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }

    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class ContactHpUserEntry {

        public enum UserEntryType {
            DESCRIPTION,
            APPLICATION,
            FEELING,
            ALLOW_CONTACT,
            PATH_TO_PROBLEM,
            EXPECTED_OUTCOME,
            EMAIL,
            FIRST_NAME,
            LAST_NAME,
            ACCOUNT_NAME,
            RECAPTCHA_RESPONSE
        }

        @JsonProperty("id")
        private String id;
        @JsonProperty("userEntryType")
        private UserEntryType userEntryType;
        @JsonProperty("value")
        private String value;
        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<String, Object>();

        /**
         * @return The id
         */
        @JsonProperty("id")
        public String getId() {
            return id;
        }

        /**
         * @param id The id
         */
        @JsonProperty("id")
        public void setId(String id) {
            this.id = id;
        }

        /**
         * @return The userEntryType
         */
        @JsonProperty("userEntryType")
        public UserEntryType getUserEntryType() {
            return userEntryType;
        }

        /**
         * @param userEntryType The userEntryType
         */
        @JsonProperty("userEntryType")
        public void setUserEntryType(UserEntryType userEntryType) {
            this.userEntryType = userEntryType;
        }

        /**
         * @return The value
         */
        @JsonProperty("value")
        public String getValue() {
            return value;
        }

        /**
         * @param value The value
         */
        @JsonProperty("value")
        public void setValue(String value) {
            this.value = value;
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }

    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class ContactHpPreferredContactMethod {

        public enum ContactMethodType {
            PROFILE_EMAIL,
            PROFILE_PHONE,
            ALT_EMAIL,
            ALT_PHONE,
            NO_CONTACT
        }

        @JsonProperty("contactMethodType")
        private ContactMethodType contactMethodType;
        @JsonProperty("altContactValue")
        private String altContactValue;
        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<String, Object>();

        /**
         * @return The contactMethodType
         */
        @JsonProperty("contactMethodType")
        public ContactMethodType getContactMethodType() {
            return contactMethodType;
        }

        /**
         * @param contactMethodType The contactMethodType
         */
        @JsonProperty("contactMethodType")
        public void setContactMethodType(ContactMethodType contactMethodType) {
            this.contactMethodType = contactMethodType;
        }

        /**
         * @return The altContactValue
         */
        @JsonProperty("altContactValue")
        public String getAltContactValue() {
            return altContactValue;
        }

        /**
         * @param altContactValue The altContactValue
         */
        @JsonProperty("altContactValue")
        public void setAltContactValue(String altContactValue) {
            this.altContactValue = altContactValue;
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }

    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class ContactHpCareForceState {

        @JsonProperty("careforceId")
        private String careforceId;
        @JsonProperty("careforceNumber")
        private String careforceNumber;
        @JsonProperty("careforceSupportStatus")
        private String careforceSupportStatus;
        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<String, Object>();

        /**
         * @return The careforceId
         */
        @JsonProperty("careforceId")
        public String getCareforceId() {
            return careforceId;
        }

        /**
         * @param careforceId The careforceId
         */
        @JsonProperty("careforceId")
        public void setCareforceId(String careforceId) {
            this.careforceId = careforceId;
        }

        /**
         * @return The careforceNumber
         */
        @JsonProperty("careforceNumber")
        public String getCareforceNumber() {
            return careforceNumber;
        }

        /**
         * @param careforceNumber The careforceNumber
         */
        @JsonProperty("careforceNumber")
        public void setCareforceNumber(String careforceNumber) {
            this.careforceNumber = careforceNumber;
        }

        /**
         * @return The careforceSupportStatus
         */
        @JsonProperty("careforceSupportStatus")
        public String getCareforceSupportStatus() {
            return careforceSupportStatus;
        }

        /**
         * @param careforceSupportStatus The careforceSupportStatus
         */
        @JsonProperty("careforceSupportStatus")
        public void setCareforceSupportStatus(String careforceSupportStatus) {
            this.careforceSupportStatus = careforceSupportStatus;
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }

    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class ContactHpAttachment {

        @JsonProperty("id")
        private String id;
        @JsonProperty("name")
        private String name;
        @JsonProperty("mimeType")
        private String mimeType;
        @JsonProperty("value")
        private List<String> value = new ArrayList<String>();
        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<String, Object>();

        /**
         * @return The id
         */
        @JsonProperty("id")
        public String getId() {
            return id;
        }

        /**
         * @param id The id
         */
        @JsonProperty("id")
        public void setId(String id) {
            this.id = id;
        }

        /**
         * @return The name
         */
        @JsonProperty("name")
        public String getName() {
            return name;
        }

        /**
         * @param name The name
         */
        @JsonProperty("name")
        public void setName(String name) {
            this.name = name;
        }

        /**
         * @return The mimeType
         */
        @JsonProperty("mimeType")
        public String getMimeType() {
            return mimeType;
        }

        /**
         * @param mimeType The mimeType
         */
        @JsonProperty("mimeType")
        public void setMimeType(String mimeType) {
            this.mimeType = mimeType;
        }

        /**
         * @return The value
         */
        @JsonProperty("value")
        public List<String> getValue() {
            return value;
        }

        /**
         * @param value The value
         */
        @JsonProperty("value")
        public void setValue(List<String> value) {
            this.value = value;
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }

    }
}