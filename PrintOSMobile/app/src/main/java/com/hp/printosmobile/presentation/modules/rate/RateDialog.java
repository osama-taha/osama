package com.hp.printosmobile.presentation.modules.rate;

import android.app.DialogFragment;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.hp.printosmobile.AppseeSdk;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.utils.RateUtils;
import com.hp.printosmobilelib.core.logging.HPLogger;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by anwar asbah on 4/10/2017.
 */
public class RateDialog extends DialogFragment {

    public static final String TAG = RateDialog.class.getSimpleName();
    private static final String GOOGLE_PLAY_URI = "market://details?id=";
    private static final String GOOGLE_PLAY_WEB_URI = "http://play.google.com/store/apps/details?id=";

    @Bind(R.id.rate_dialog_title)
    TextView title;

    private RateDialogCallback callback;

    public static RateDialog getInstance(RateDialogCallback callback) {
        RateDialog rateDialog = new RateDialog();
        rateDialog.callback = callback;
        return rateDialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        View rootView = inflater.inflate(R.layout.rate_dialog, container, false);
        ButterKnife.bind(this, rootView);
        title.setText(getString(R.string.rate_app_title, getString(R.string.app_name)));
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setCancelable(false);
    }

    @OnClick(R.id.thanks_button)
    public void onThanksButtonClicked() {
        HPLogger.d(TAG, "on no,thanks button clicked");

        AppseeSdk.getInstance(getActivity()).sendEvent(AppseeSdk.EVENT_RATE_DIALOG_NO_THANKS_CLICKED);

        PrintOSPreferences.getInstance(getActivity()).setRateStatus(RateUtils.RateStatusEnum.SUPPRESS.getKey());
        dismiss();
    }

    @OnClick(R.id.later_button)
    public void onLaterButtonClicked() {
        HPLogger.d(TAG, "on later button clicked");

        AppseeSdk.getInstance(getActivity()).sendEvent(AppseeSdk.EVENT_RATE_DIALOG_REMIND_ME_LATER_CLICKED);

        PrintOSPreferences.getInstance(getActivity()).setRateStatus(RateUtils.RateStatusEnum.REMIND_ME_LATER.getKey());
        PrintOSPreferences.getInstance(getActivity()).setNumberOfAuthenticatedSessionsForRateDialog(0);

        dismiss();

    }

    @OnClick(R.id.rate_button)
    public void onRateButtonClicked() {
        HPLogger.d(TAG, "on rate app button clicked");

        AppseeSdk.getInstance(getActivity()).sendEvent(AppseeSdk.EVENT_RATE_DIALOG_SEND_FEEDBACK_CLICKED);

        PrintOSPreferences.getInstance(getActivity()).setRateStatus(RateUtils.RateStatusEnum.FEEDBACK_SENT.getKey());
        goToGooglePlay();
        dismiss();
    }

    private void goToGooglePlay() {
        Uri uri = Uri.parse(GOOGLE_PLAY_URI + getActivity().getPackageName());
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        // To count with Play market back stack, After pressing back button,
        // to taken back to our application, we need to add following flags to intent.
        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        try {
            startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            try {
                startActivity(new Intent(Intent.ACTION_VIEW,
                        Uri.parse(GOOGLE_PLAY_WEB_URI + getActivity().getPackageName())));
            } catch (ActivityNotFoundException e2) {
                HPLogger.e(TAG, "unable to open the app store " + e2);
            }
        }
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        if (callback != null) {
            callback.onDismiss();
        }
    }

    public interface RateDialogCallback {
        void onDismiss();
    }
}
