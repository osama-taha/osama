package com.hp.printosmobile.data.remote;

/**
 * A class to hold the API constants.
 *
 * @author Osama Taha
 */
public final class ApiConstants {

    public static final String BOX_SERVICE = "api/dashboard/stats";

    public static final String PRINT_BEAT_SERVICES = "api/PrintbeatService/";

    public static final String CONFIG_DEVICES_API = PRINT_BEAT_SERVICES + "Config/devices?bu=*";
    public static final String CONFIG_ORGANIZATION_DEVICES_API = "/api/aaa/v1/organizations/devices?limit=10000&offset=0";
    public static final String DEVICES_STATISTICS_API = "/api/system-statistics/v2/devices/latest";
    public static final String CONFIG_SCOREABLES_API = PRINT_BEAT_SERVICES + "Config/scoreables";

    public static final String META_DATA_API = PRINT_BEAT_SERVICES + "Metadata/kpi";

    public static final String PERFORMANCE_TRACKING_WEEKLY_REPORT_API = PRINT_BEAT_SERVICES + "gsb/reports";
    public static final String PERFORMANCE_TRACKING_WEEKLY_REPORT_API_V2 = PERFORMANCE_TRACKING_WEEKLY_REPORT_API + "/v2";
    public static final String PERFORMANCE_TRACKING_WEEKLY_OVERALL_REPORT_API = PRINT_BEAT_SERVICES + "gsb/weekly/overall";
    public static final String SCOREABLE_API = PRINT_BEAT_SERVICES + "gsb/Scoreable/{kpiName}";
    public static final String SCOREABLE_API_V2 = PRINT_BEAT_SERVICES + "gsb/Scoreable/v2/{kpiName}";

    public static final String REALTIME_TRACKING_MANY = PRINT_BEAT_SERVICES + "RealtimeTracking/many";
    public static final String REALTIME_TRACKING_MANY_V2 = REALTIME_TRACKING_MANY + "/v2";
    public static final String REALTIME_TRACKING_TARGET = PRINT_BEAT_SERVICES + "RealtimeTracking/target/many";
    public static final String REALTIME_TRACKING_TARGET_V2 = REALTIME_TRACKING_TARGET + "/v2";
    public static final String REALTIME_TRACKING_ACTUAL_VS_TARGET = PRINT_BEAT_SERVICES + "RealtimeTracking/actualVStarget/many";
    public static final String REALTIME_TRACKING_ACTUAL_VS_TARGET_V2 = REALTIME_TRACKING_ACTUAL_VS_TARGET + "/v2";
    public static final String ACTIVE_SHIFTS = PRINT_BEAT_SERVICES + "Shift/ActiveShifts";

    public static final String PERSONAL_ADVISOR_API = PRINT_BEAT_SERVICES + "Advisor";
    public static final String PERSONAL_ADVISOR_LIKE_API = PRINT_BEAT_SERVICES + "Advisor/user/like/{adviceId}";
    public static final String PERSONAL_ADVISOR_SUPPRESS_API = PRINT_BEAT_SERVICES + "Advisor/user/suppress/{adviceId}";
    public static final String PERSONAL_ADVISOR_UNHIDE_ALL_API = PRINT_BEAT_SERVICES + "Advisor/user/suppress/many";
    public static final String ORGANIZATION_DIMENSION = "api/aaa/v3/organizations";

    public static String DEVICES_IMAGES_URL = "/account/components/devices/images/%s.png";

    public static final String NOTIFICATION_SERVICE_REGISTRATION_API = "api/notification-service/v1/mobile?os=android";

    public static final String EULA_URL = "/api/portal/v1/i18n/eula/{language}";

    public static final String RESET_PASSWORD_API = "/api/email-service/v1/invitations/resetpw";

    public static final String CONTACT_HP_URL = "/api/supportapp-service/v1/cases";
    public static final String CONTACT_HP_USER_URL = "/api/aaa/v1/users";

    public static final String SUPPORTED_VERSIONS_URL = "/api/PrintbeatService/mobile/version";

    public static final String GET_ORGANIZATION_URL = "api/aaa/v1/users/context?limit=10000";
    public static final String CHANGE_ORGANIZATION_CONTEXT_URL = "api/aaa/v1/users/context";

    public static final String LAST_SELECT_SITE_API = PRINT_BEAT_SERVICES + "/userData/lastSite";

    public static final String USER_ROLES_API = "api/aaa/v1/users/roles/";

    public static final String NOTIFICATION_SUBSCRIPTION_API = "api/notification-service/v1/subscription";
    public static final String NOTIFICATION_SUBSCRIBE_API = "api/notification-service/v1/subscription";
    public static final String NOTIFICATIONS_REMOVAL_API = "api/notification-service/v1/subscription/{eventDescID}";
    public static final String NOTIFICATIONS_STRINGS_API = "api/notification-service/v1/i18n/notification/{locale}";
    public static final String NOTIFICATIONS_ITEMS_API = "api/notification-service/v1/";
    public static final String NOTIFICATIONS_ACTION_API = "api/notification-service/v1/event/{id}";
    public static final String NOTIFICATION_SUBSCRIPTION_DESTINATION = "api/notification-service/v1/subscription/{subscription_id}?toggle=mobile";
    public static final String NOTIFICATION_APPLICATION_API = "api/notification-service/v1/application";
    public static final String NOTIFICATION_EVENT_DESCRIPTION_API = "api/notification-service/v1/application/{application_id}";

    public static final String INSIGHTS_API = "api/PrintbeatService/PerformanceTracking/DrillDown/charts";

    public static final String SERVICE_CALL_API = "api/PrintbeatService/serviceCalls";

    public static final String PREFERENCES_API = "api/portal/v1/preferences";
    public static final String PREFERENCES_TIME_ZONES_API = "api/portal/v1/preferences/choices?appId=general&name=timeZone";

    public static final String NPS_URL = "https://delighted.com/t/cXOs4aVP?score=%1$d&name=%2$s&end_user_id=%3$s&locale=%4$s&week=%5$s&year=%6$s&source=PrintOS_Mobile";

    public static final String BEAT_COINS_API = PRINT_BEAT_SERVICES + "notifications/data/{id}";


    private ApiConstants() {
    }
}
