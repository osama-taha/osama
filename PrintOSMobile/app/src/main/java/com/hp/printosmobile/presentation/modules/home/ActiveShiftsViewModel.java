package com.hp.printosmobile.presentation.modules.home;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Osama Taha on 10/5/16.
 */
public class ActiveShiftsViewModel implements Serializable{

    private boolean isSiteHasShifts;
    private List<ShiftViewModel> shiftViewModels;

    public boolean isSiteHasShifts() {
        return isSiteHasShifts;
    }

    public void setSiteHasShifts(boolean siteHasShifts) {
        isSiteHasShifts = siteHasShifts;
    }

    public List<ShiftViewModel> getShiftViewModels() {
        return shiftViewModels;
    }

    public void setShiftViewModels(List<ShiftViewModel> shiftViewModels) {
        this.shiftViewModels = shiftViewModels;
    }

    @Override
    public String toString() {
        return "ActiveShiftsViewModel{" +
                "isSiteHasShifts=" + isSiteHasShifts +
                ", shiftViewModels=" + shiftViewModels +
                '}';
    }
}
