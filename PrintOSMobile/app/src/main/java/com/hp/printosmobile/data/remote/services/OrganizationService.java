package com.hp.printosmobile.data.remote.services;

import com.hp.printosmobile.data.remote.ApiConstants;
import com.hp.printosmobile.data.remote.models.OrganizationBody;
import com.hp.printosmobile.data.remote.models.OrganizationDimensionData;
import com.hp.printosmobilelib.core.communications.remote.models.OrganizationJsonBody;

import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.PUT;
import rx.Observable;

/**
 * Retrofit REST interface definition for Organization Endpoints
 *
 * @author Anwar Asbah
 */
public interface OrganizationService {

    /**
     * Returns list of accounts.
     */
    @GET(ApiConstants.GET_ORGANIZATION_URL)
    Observable<Response<OrganizationBody>> getOrganization();

    /**
     * change context.
     */
    @PUT(ApiConstants.CHANGE_ORGANIZATION_CONTEXT_URL)
    Observable<ResponseBody> changeOrganizationContext(@Body OrganizationJsonBody organization);

    /**
     * get organization dimension
     */
    @GET(ApiConstants.ORGANIZATION_DIMENSION)
    Observable<Response<OrganizationDimensionData>> getOrganizationDimension();
}
