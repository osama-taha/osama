package com.hp.printosmobile.data.remote.models;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.hp.printosmobile.data.remote.services.MetaDataService;

import java.util.HashMap;
import java.util.Map;

/**
 * A model representing the response of {@link MetaDataService#getMetaData()} endpoint.
 *
 * @author Osama Taha
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
public class BusinessUnit {

    @JsonProperty("Id")
    private Integer Id;
    @JsonProperty("BusinessUnit")
    private String BusinessUnit;
    @JsonProperty("realTimeSupport")
    private RealTimeSupport realTimeSupport;
    @JsonProperty("persAdvisor")
    private Boolean persAdvisor;
    @JsonProperty("workWeek")
    private Boolean workWeek;
    @JsonProperty("mailSupport")
    private Boolean mailSupport;
    @JsonProperty("kpiMetaDataMap")
    private HashMap<String, Kpi> kpiMetaDataMap;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * @return The Id
     */
    @JsonProperty("Id")
    public Integer getId() {
        return Id;
    }

    /**
     * @param Id The Id
     */
    @JsonProperty("Id")
    public void setId(Integer Id) {
        this.Id = Id;
    }

    /**
     * @return The BusinessUnit
     */
    @JsonProperty("BusinessUnit")
    public String getBusinessUnit() {
        return BusinessUnit;
    }

    /**
     * @param BusinessUnit The BusinessUnit
     */
    @JsonProperty("BusinessUnit")
    public void setBusinessUnit(String BusinessUnit) {
        this.BusinessUnit = BusinessUnit;
    }

    /**
     * @return The realTimeSupport
     */
    @JsonProperty("realTimeSupport")
    public RealTimeSupport getRealTimeSupport() {
        return realTimeSupport;
    }

    /**
     * @param realTimeSupport The realTimeSupport
     */
    @JsonProperty("realTimeSupport")
    public void setRealTimeSupport(RealTimeSupport realTimeSupport) {
        this.realTimeSupport = realTimeSupport;
    }

    /**
     * @return The persAdvisor
     */
    @JsonProperty("persAdvisor")
    public Boolean getPersAdvisor() {
        return persAdvisor;
    }

    /**
     * @param persAdvisor The persAdvisor
     */
    @JsonProperty("persAdvisor")
    public void setPersAdvisor(Boolean persAdvisor) {
        this.persAdvisor = persAdvisor;
    }

    /**
     * @return The workWeek
     */
    @JsonProperty("workWeek")
    public Boolean getWorkWeek() {
        return workWeek;
    }

    /**
     * @param workWeek The workWeek
     */
    @JsonProperty("workWeek")
    public void setWorkWeek(Boolean workWeek) {
        this.workWeek = workWeek;
    }

    /**
     * @return The mailSupport
     */
    @JsonProperty("mailSupport")
    public Boolean getMailSupport() {
        return mailSupport;
    }

    /**
     * @param mailSupport The mailSupport
     */
    @JsonProperty("mailSupport")
    public void setMailSupport(Boolean mailSupport) {
        this.mailSupport = mailSupport;
    }

    /**
     * @return The kpiMetaDataMap
     */
    @JsonProperty("kpiMetaDataMap")
    public HashMap<String, Kpi> getKpiMetaDataMap() {
        return kpiMetaDataMap;
    }

    /**
     * @param kpiMetaDataMap The kpiMetaDataMap
     */
    @JsonProperty("kpiMetaDataMap")
    public void setKpiMetaDataMap(HashMap<String, Kpi> kpiMetaDataMap) {
        this.kpiMetaDataMap = kpiMetaDataMap;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("BusinessUnit{");
        sb.append("Id=").append(Id);
        sb.append(", BusinessUnit='").append(BusinessUnit).append('\'');
        sb.append(", realTimeSupport='").append(realTimeSupport).append('\'');
        sb.append(", persAdvisor=").append(persAdvisor);
        sb.append(", workWeek=").append(workWeek);
        sb.append(", mailSupport=").append(mailSupport);
        sb.append(", kpiMetaDataMap=").append(kpiMetaDataMap);
        sb.append(", additionalProperties=").append(additionalProperties);
        sb.append('}');
        return sb.toString();
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class Kpi {

        @JsonProperty("Id")
        private Integer Id;
        @JsonProperty("BusinessUnit")
        private String BusinessUnit;
        @JsonProperty("name")
        private String name;
        @JsonProperty("description")
        private String description;
        @JsonProperty("kpiImagePath")
        private String kpiImagePath;
        @JsonProperty("parent")
        private Integer parent;
        @JsonProperty("scoreWeight")
        private Integer scoreWeight;
        @JsonProperty("valueHandle")
        private String valueHandle;
        @JsonProperty("text")
        private String text;
        @JsonProperty("vAxisText")
        private String vAxisText;
        @JsonProperty("defaultChart")
        private String defaultChart;
        @JsonProperty("dataField")
        private String dataField;
        @JsonProperty("tooltipFormat")
        private String tooltipFormat;
        @JsonProperty("calcType")
        private String calcType;
        @JsonProperty("pivotKpi")
        private Integer pivotKpi;
        @JsonProperty("Resolutions")
        private Resolutions resolutions;

        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<String, Object>();

        /**
         * @return The Id
         */
        @JsonProperty("Id")
        public Integer getId() {
            return Id;
        }

        /**
         * @param Id The Id
         */
        @JsonProperty("Id")
        public void setId(Integer Id) {
            this.Id = Id;
        }

        /**
         * @return The BusinessUnit
         */
        @JsonProperty("BusinessUnit")
        public String getBusinessUnit() {
            return BusinessUnit;
        }

        /**
         * @param BusinessUnit The BusinessUnit
         */
        @JsonProperty("BusinessUnit")
        public void setBusinessUnit(String BusinessUnit) {
            this.BusinessUnit = BusinessUnit;
        }

        /**
         * @return The name
         */
        @JsonProperty("name")
        public String getName() {
            return name;
        }

        /**
         * @param name The name
         */
        @JsonProperty("name")
        public void setName(String name) {
            this.name = name;
        }

        /**
         * @return The description
         */
        @JsonProperty("description")
        public String getDescription() {
            return description;
        }

        /**
         * @param description The description
         */
        @JsonProperty("description")
        public void setDescription(String description) {
            this.description = description;
        }

        /**
         * @return The kpiImagePath
         */
        @JsonProperty("kpiImagePath")
        public String getKpiImagePath() {
            return kpiImagePath;
        }

        /**
         * @param kpiImagePath The kpiImagePath
         */
        @JsonProperty("kpiImagePath")
        public void setKpiImagePath(String kpiImagePath) {
            this.kpiImagePath = kpiImagePath;
        }

        /**
         * @return The parent
         */
        @JsonProperty("parent")
        public Integer getParent() {
            return parent;
        }

        /**
         * @param parent The parent
         */
        @JsonProperty("parent")
        public void setParent(Integer parent) {
            this.parent = parent;
        }

        /**
         * @return The scoreWeight
         */
        @JsonProperty("scoreWeight")
        public Integer getScoreWeight() {
            return scoreWeight;
        }

        /**
         * @param scoreWeight The scoreWeight
         */
        @JsonProperty("scoreWeight")
        public void setScoreWeight(Integer scoreWeight) {
            this.scoreWeight = scoreWeight;
        }

        /**
         * @return The valueHandle
         */
        @JsonProperty("valueHandle")
        public String getValueHandle() {
            return valueHandle;
        }

        /**
         * @param valueHandle The valueHandle
         */
        @JsonProperty("valueHandle")
        public void setValueHandle(String valueHandle) {
            this.valueHandle = valueHandle;
        }

        /**
         * @return The text
         */
        @JsonProperty("text")
        public String getText() {
            return text;
        }

        /**
         * @param text The text
         */
        @JsonProperty("text")
        public void setText(String text) {
            this.text = text;
        }

        /**
         * @return The vAxisText
         */
        @JsonProperty("vAxisText")
        public String getVAxisText() {
            return vAxisText;
        }

        /**
         * @param vAxisText The vAxisText
         */
        @JsonProperty("vAxisText")
        public void setVAxisText(String vAxisText) {
            this.vAxisText = vAxisText;
        }

        /**
         * @return The defaultChart
         */
        @JsonProperty("defaultChart")
        public String getDefaultChart() {
            return defaultChart;
        }

        /**
         * @param defaultChart The defaultChart
         */
        @JsonProperty("defaultChart")
        public void setDefaultChart(String defaultChart) {
            this.defaultChart = defaultChart;
        }

        /**
         * @return The dataField
         */
        @JsonProperty("dataField")
        public String getDataField() {
            return dataField;
        }

        /**
         * @param dataField The dataField
         */
        @JsonProperty("dataField")
        public void setDataField(String dataField) {
            this.dataField = dataField;
        }

        /**
         * @return The tooltipFormat
         */
        @JsonProperty("tooltipFormat")
        public String getTooltipFormat() {
            return tooltipFormat;
        }

        /**
         * @param tooltipFormat The tooltipFormat
         */
        @JsonProperty("tooltipFormat")
        public void setTooltipFormat(String tooltipFormat) {
            this.tooltipFormat = tooltipFormat;
        }

        /**
         * @return The resolutions
         */
        @JsonProperty("resolutions")
        public Resolutions getResolutions() {
            return resolutions;
        }

        /**
         * @param resolutions The resolutions
         */
        @JsonProperty("resolutions")
        public void setResolutions(Resolutions resolutions) {
            this.resolutions = resolutions;
        }

        /**
         * @return The calcType
         */
        @JsonProperty("calcType")
        public String getCalcType() {
            return calcType;
        }

        /**
         * @param calcType The calcType
         */
        @JsonProperty("calcType")
        public void setCalcType(String calcType) {
            this.calcType = calcType;
        }

        /**
         * @return The pivotKpi
         */
        @JsonProperty("pivotKpi")
        public Integer getPivotKpi() {
            return pivotKpi;
        }

        /**
         * @param pivotKpi The pivotKpi
         */
        @JsonProperty("pivotKpi")
        public void setPivotKpi(Integer pivotKpi) {
            this.pivotKpi = pivotKpi;
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }

        @Override
        public String toString() {
            final StringBuffer sb = new StringBuffer("Kpi{");
            sb.append("Id=").append(Id);
            sb.append(", BusinessUnit='").append(BusinessUnit).append('\'');
            sb.append(", name='").append(name).append('\'');
            sb.append(", description='").append(description).append('\'');
            sb.append(", kpiImagePath='").append(kpiImagePath).append('\'');
            sb.append(", parent=").append(parent);
            sb.append(", scoreWeight=").append(scoreWeight);
            sb.append(", valueHandle='").append(valueHandle).append('\'');
            sb.append(", text='").append(text).append('\'');
            sb.append(", vAxisText='").append(vAxisText).append('\'');
            sb.append(", defaultChart='").append(defaultChart).append('\'');
            sb.append(", dataField='").append(dataField).append('\'');
            sb.append(", tooltipFormat='").append(tooltipFormat).append('\'');
            sb.append(", calcType='").append(calcType).append('\'');
            sb.append(", pivotKpi=").append(pivotKpi);
            sb.append(", resolutions=").append(resolutions);
            sb.append(", additionalProperties=").append(additionalProperties);
            sb.append('}');
            return sb.toString();
        }

        @JsonInclude(JsonInclude.Include.NON_NULL)
        public class Resolutions {

            @JsonProperty("Day")
            private Boolean Day;
            @JsonProperty("Week")
            private Boolean Week;
            @JsonProperty("Month")
            private Boolean Month;
            @JsonProperty("Hour")
            private Boolean Hour;
            @JsonProperty("Year")
            private Boolean Year;
            @JsonIgnore
            private Map<String, Object> additionalProperties = new HashMap<String, Object>();

            /**
             * @return The Day
             */
            @JsonProperty("Day")
            public Boolean getDay() {
                return Day;
            }

            /**
             * @param Day The Day
             */
            @JsonProperty("Day")
            public void setDay(Boolean Day) {
                this.Day = Day;
            }

            /**
             * @return The Week
             */
            @JsonProperty("Week")
            public Boolean getWeek() {
                return Week;
            }

            /**
             * @param Week The Week
             */
            @JsonProperty("Week")
            public void setWeek(Boolean Week) {
                this.Week = Week;
            }

            /**
             * @return The Month
             */
            @JsonProperty("Month")
            public Boolean getMonth() {
                return Month;
            }

            /**
             * @param Month The Month
             */
            @JsonProperty("Month")
            public void setMonth(Boolean Month) {
                this.Month = Month;
            }

            /**
             * @return The Hour
             */
            @JsonProperty("Hour")
            public Boolean getHour() {
                return Hour;
            }

            /**
             * @param Hour The Hour
             */
            @JsonProperty("Hour")
            public void setHour(Boolean Hour) {
                this.Hour = Hour;
            }

            /**
             * @return The Year
             */
            @JsonProperty("Year")
            public Boolean getYear() {
                return Year;
            }

            /**
             * @param Year The Year
             */
            @JsonProperty("Year")
            public void setYear(Boolean Year) {
                this.Year = Year;
            }

            @JsonAnyGetter
            public Map<String, Object> getAdditionalProperties() {
                return this.additionalProperties;
            }

            @JsonAnySetter
            public void setAdditionalProperty(String name, Object value) {
                this.additionalProperties.put(name, value);
            }

        }
    }

    public enum RealTimeSupport {

        @JsonProperty("Restricted")
        RESTRICTED,
        @JsonProperty("Full")
        FULL
    }
}
