package com.hp.printosmobile.presentation.modules.settings;

import com.hp.printosmobile.R;

import java.io.Serializable;

/**
 * Created by Anwar Asbah on 1/18/2017.
 */
public class SubscriptionEvent implements Serializable {

    public enum NotificationApplicationEnum {
        PRINTOS_BOX("PRINTOS_BOX"),
        PRINT_BEAT_SERVICE("PRINT_BEAT_SERVICE"),
        SERVICE_CALL("SERVICE_CALL"),
        UNKNOWN("");

        private String key;

        NotificationApplicationEnum(String key) {
            this.key = key;
        }

        public String getKey() {
            return key;
        }

        public static NotificationApplicationEnum from(String key) {
            if (key != null) {
                for (NotificationApplicationEnum application : NotificationApplicationEnum.values()) {
                    if (application.getKey().toLowerCase().equals(key.toLowerCase()))
                        return application;
                }
            }
            return NotificationApplicationEnum.UNKNOWN;
        }
    }

    public enum NotificationEventEnum {
        NEW_PRINT_BEAT_REPORT("EVENT_PRINT_BEAT_WEEKLY_SCORE_NOTIFICATION",
                R.string.notification_new_print_beat_report,
                R.drawable.notification_report_rounded_bg,
                R.string.subscription_event_print_beat_header,
                true, false, true),
        PERSONAL_ADVISOR("EVENT_PRINT_BEAT_ADVICE_NOTIFICATION",
                R.string.notification_personal_advisor,
                R.drawable.notification_advice_rounded_bg,
                R.string.subscription_event_print_beat_header,
                true, false, true),
        DAILY_DIGEST("EVENT_PRINT_BEAT_END_OF_DAY_NOTIFICATION",
                R.string.notification_daily_digest,
                R.drawable.notification_daily_rounded_bg,
                R.string.subscription_event_print_beat_header,
                true, false, true),
        INTRA_DAY_PROGRESS_UPDATE("EVENT_INTRADAY_PROGRESS_UPDATE_NOTIFICATION",
                R.string.notification_intra_day_progress_update,
                R.drawable.notification_intraday_rounded_bg,
                R.string.subscription_event_print_beat_header,
                false, true, true),
        SERVICE_CALL_UPDATES("EVENT_SERVICE_CALL_NOTIFICATION",
                R.string.notification_service_call_updates,
                R.drawable.notification_call_rounded_bg,
                R.string.subscription_event_service_call_header,
                true, true, true),
        BOX_NEW_FOLDER("EVENT_NEW_FOLDER_CREATED",
                R.string.notification_box_new_folder,
                R.drawable.notification_box_rounded_bg,
                R.string.subscription_event_box_header,
                false, true, true),
        NPS_NOTIFICATIONS("NPS_NOTIFICATIONS",
                R.string.notification_nps,
                0,
                R.string.subscription_event_others,
                false, false, false),
        LOCATION_BASED("LOCATION_BASED_NOTIFICATIONS",
                R.string.notification_location_based,
                0,
                R.string.subscription_event_others,
                false, false, false),
        LOCATION_BASED_NOTIFICATIONS_ARRIVE_WORK("ARRIVE_WORK",
                R.string.notification_location_based,
                0,
                R.string.subscription_event_others,
                false, true, false),
        LOCATION_BASED_NOTIFICATIONS_ARRIVE_HOME("ARRIVE_HOME",
                R.string.notification_location_based,
                0,
                R.string.subscription_event_others,
                false, true, false),
        UNKNOWN("", 0, 0, 0, false, true, true);

        private final boolean isSubscribable;
        private int nameID;
        private int bgDrawableID;
        private String key;
        private boolean subscribeOnFirstLaunch;
        private int sectionNameStringID;
        private boolean isOptional;

        NotificationEventEnum(String key, int nameID, int bgDrawableID, int sectionNameStringID,
                              boolean subscribeOnFirstLaunch, boolean isOptional, boolean subscribable) {
            this.key = key;
            this.nameID = nameID;
            this.bgDrawableID = bgDrawableID;
            this.sectionNameStringID = sectionNameStringID;
            this.subscribeOnFirstLaunch = subscribeOnFirstLaunch;
            this.isOptional = isOptional;
            this.isSubscribable = subscribable;
        }

        public int getNameID() {
            return nameID;
        }

        public int getBgDrawableID() {
            return bgDrawableID;
        }

        public String getKey() {
            return key;
        }

        public boolean isSubscribeOnFirstLaunch() {
            return subscribeOnFirstLaunch;
        }

        public int getSectionNameStringID() {
            return sectionNameStringID;
        }

        public boolean isOptional() {
            return isOptional;
        }

        public boolean isSubscribable() {
            return isSubscribable;
        }

        public static NotificationEventEnum from(String key) {
            if (key != null) {
                for (NotificationEventEnum event : NotificationEventEnum.values()) {
                    if (event.getKey().toLowerCase().equals(key.toLowerCase()))
                        return event;
                }
            }
            return NotificationEventEnum.UNKNOWN;
        }

    }

    private NotificationEventEnum notificationEventEnum;
    private String userSubscriptionID;

    public NotificationEventEnum getNotificationEventEnum() {
        return notificationEventEnum;
    }

    public void setNotificationEventEnum(NotificationEventEnum notificationEventEnum) {
        this.notificationEventEnum = notificationEventEnum;
    }

    public String getUserSubscriptionID() {
        return userSubscriptionID;
    }

    public void setUserSubscriptionID(String userSubscriptionID) {
        this.userSubscriptionID = userSubscriptionID;
    }
}
