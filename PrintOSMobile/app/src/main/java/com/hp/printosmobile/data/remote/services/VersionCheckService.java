package com.hp.printosmobile.data.remote.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hp.printosmobile.BuildConfig;
import com.hp.printosmobile.Constants;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.data.remote.ApiServicesProvider;
import com.hp.printosmobile.data.remote.models.VersionUpdateResult;
import com.hp.printosmobile.data.remote.models.VersionsData;
import com.hp.printosmobile.presentation.BaseActivity;
import com.hp.printosmobile.presentation.modules.npspopup.NPSNotificationManager;
import com.hp.printosmobile.utils.HPStringUtils;
import com.hp.printosmobilelib.core.utils.StringUtils;

import java.io.IOException;

import retrofit2.Response;
import retrofit2.http.Query;

/**
 * Created by Osama Taha on 6/27/16.
 */
public class VersionCheckService extends Service {

    private static final String TAG = VersionCheckService.class.getSimpleName();
    private static final String DEBUG_VERSION_SPLIT_REGEX = "-";

    public VersionCheckService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, final int flags, int startId) {
        new Thread(new Runnable() {
            @Override
            public void run() {

                ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
                SupportedVersionsService supportedVersionsService = serviceProvider.getSupportedVersionsService();

                Response<VersionsData> response;
                try {
                    response = supportedVersionsService.getVersionsData().execute();
                } catch (IOException e) {
                    response = null;
                }

                VersionsData versionsData = response != null ?
                        response.body() : null;

                if (versionsData == null) {

                    stopSelf();

                } else {

                    if (versionsData.getConfiguration() != null) {

                        PrintOSPreferences.getInstance(VersionCheckService.this)
                                .setV2Support(versionsData.getConfiguration().isV2Support());


                        VersionsData.Configuration.Validation validation = versionsData.getConfiguration().getValidation();
                        if(validation != null){
                            PrintOSPreferences.getInstance(VersionCheckService.this).saveValidationParams(
                                    validation.getCheckCookieExpiryHrs(), validation.getMoveToHomeMins(), validation.isShowToastMessage()
                            );
                        }

                        PrintOSPreferences.getInstance(VersionCheckService.this).enableCompletedJobsFeature(
                                versionsData.getConfiguration().isCompletedJobsEnabled()
                        );

                        VersionsData.Configuration.Insights insights = versionsData.getConfiguration().getInsights();
                        if(insights != null){
                            PrintOSPreferences.getInstance(VersionCheckService.this).setInsightsJamsEnabled(
                                    insights.isJamsEnabled()
                            );
                            PrintOSPreferences.getInstance(VersionCheckService.this).setInsightsFailureEnabled(
                                    insights.isFailuresEnabled()
                            );
                            PrintOSPreferences.getInstance(VersionCheckService.this).setInsightsFirstOccuringVersion(
                                    insights.getBadgeVersion() == null ? "" : insights.getBadgeVersion()
                            );
                        }

                        Boolean nps = versionsData.getConfiguration().isNpsPopupEnabled();
                        Boolean locationBasedNotificationsEnabled = versionsData.getConfiguration().isLocationBasedNotificationsEnabled();
                        PrintOSPreferences.getInstance(VersionCheckService.this).setNPSFeatureEnabled(
                                nps == null || nps);
                        PrintOSPreferences.getInstance(VersionCheckService.this).setLocationBasedNotificationsFeatureEnabled(
                                locationBasedNotificationsEnabled == null || locationBasedNotificationsEnabled);

                        PrintOSPreferences.getInstance(VersionCheckService.this).setClosedServiceCallsDays(versionsData.getConfiguration().getClosedServiceCallsDays());

                        if (!PrintOSPreferences.getInstance(VersionCheckService.this).isNPSFeatureEnabled()) {
                            NPSNotificationManager.enableNotifications(false);
                        }

                        Boolean kpiExplanation = versionsData.getConfiguration().getKpiExplanationEnabled();
                        PrintOSPreferences.getInstance(VersionCheckService.this).setKpiExplanationEnabled(
                                kpiExplanation == null || kpiExplanation);

                        Boolean beatCoin = versionsData.getConfiguration().isBeatCoinsPopupEnabled();
                        PrintOSPreferences.getInstance(VersionCheckService.this).setBeatCoinFeatureEnabled(
                                beatCoin == null || beatCoin);

                        Boolean timeZone = versionsData.getConfiguration().isTimeZonePopupEnabled();
                        PrintOSPreferences.getInstance(VersionCheckService.this).setTimeZonePopupEnabled(
                                timeZone == null || timeZone);

                        VersionsData.Configuration.WeeklyPrintbeat weeklyPrintbeat = versionsData.getConfiguration().getWeeklyPrintbeat();
                        if (weeklyPrintbeat != null) {
                            Boolean weekIncrementalEnabled = weeklyPrintbeat.isKpiDataIncrementallyEnabled();
                            PrintOSPreferences.getInstance(VersionCheckService.this).setWeekIncrementalFeatureEnabled(
                                    weekIncrementalEnabled == null || weekIncrementalEnabled);
                        }

                        VersionsData.Configuration.ReferFriend referFriend = versionsData.getConfiguration().getReferFriend();
                        if (referFriend != null) {
                            PrintOSPreferences.getInstance(VersionCheckService.this).setReferToAFriendEnabled(
                                    referFriend.isEnabled() == null || referFriend.isEnabled());
                            PrintOSPreferences.getInstance(VersionCheckService.this).setReferToAFriendUrl(
                                    referFriend.getUrl());
                        }
                    }

                    if (versionsData.getVersions() == null
                            || versionsData.getVersions().getPrintbeat() == null ||
                            versionsData.getVersions().getPrintbeat().getAndroid() == null) {

                        //No need to do anything.
                        stopSelf();

                    } else {

                        VersionUpdateResult result = getVersionUpdateResult(versionsData, VersionCheckService.this);

                        if (result == null) {
                            stopSelf();
                        }

                        String rawResponse = "";
                        ObjectMapper mapper = new ObjectMapper();
                        try {
                            rawResponse = mapper.writeValueAsString(versionsData);
                        } catch (JsonProcessingException e) {
                        }

                        PrintOSPreferences.getInstance(VersionCheckService.this).setSupportedVersions(rawResponse);
                        PrintOSPreferences.getInstance(VersionCheckService.this).setVersionCheckingTime(System.currentTimeMillis());
                        PrintOSPreferences.getInstance(VersionCheckService.this).enableWarningUpdateDialog(true);

                        //Send version force-update/warning message to the running activity (if needed).
                        Intent notifyIntent = new Intent();
                        notifyIntent.setAction(Constants.IntentExtras.VERSION_UPDATE_INTENT_ACTION);
                        notifyIntent.putExtra(Constants.IntentExtras.VERSION_UPDATE_INTENT_EXTRA_KEY, result);
                        VersionCheckService.this.sendBroadcast(notifyIntent);


                    }
                }

                BaseActivity.isServiceStarted = false;
                //Stop service once it finishes its task
                stopSelf();

            }
        }).start();

        return Service.START_STICKY;

    }

    public static VersionUpdateResult getVersionUpdateResult(VersionsData versionsData, Context context) {
        VersionUpdateResult versionUpdateResult = new VersionUpdateResult();

        String languageCode = PrintOSPreferences.getInstance(context).getLanguageCode();

        String version = getVersionName();

        VersionsData.Platform android = versionsData.getVersions().getPrintbeat().getAndroid();

        if (android.getValid() != null && android.getValid().contains(version)) {
            //No need to do anything.
            return null;
        } else if (android.getWarning() != null && android.getWarning().contains(version)) {
            versionUpdateResult.setMessageType(VersionUpdateResult.MessageType.WARNING);
            versionUpdateResult.setUpdateMessage(versionsData.getMessages().getMessageWarning().get(languageCode));
        } else if (android.getUpdate() != null && android.getUpdate().contains(version)) {
            versionUpdateResult.setUpdateMessage(versionsData.getMessages().getMessageUpdate().get(languageCode));
            versionUpdateResult.setMessageType(VersionUpdateResult.MessageType.FORCE_UPDATE);
        } else {
            versionUpdateResult.setMessageType(VersionUpdateResult.MessageType.OK);
        }

        versionUpdateResult.setLinkInPlayStore(android.getLinkInStore());

        return versionUpdateResult;
    }

    public static String getVersionName() {
        String version = BuildConfig.VERSION_NAME.trim();
        if (BuildConfig.DEBUG) {
            version = HPStringUtils.getStringBeforeRegex(version, DEBUG_VERSION_SPLIT_REGEX);
        }
        return version;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

}