package com.hp.printosmobile.data.remote.services;

import com.hp.printosmobile.data.remote.ApiConstants;
import com.hp.printosmobile.data.remote.models.VersionsData;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.GET;

/**
 * Created by Anwar Asbah 9/21/2016
 */
public interface SupportedVersionsService {

    @GET(ApiConstants.SUPPORTED_VERSIONS_URL)
    Call<VersionsData> getVersionsData();
}
