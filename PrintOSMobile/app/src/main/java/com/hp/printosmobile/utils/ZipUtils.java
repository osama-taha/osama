package com.hp.printosmobile.utils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by osama on 6/15/17.
 */

public class ZipUtils {

    public interface ZipIterator<A, B> {
        boolean each(A t, B u);
    }

    public static <A, B> List<? extends Pair<A, B>> zip(List<A> as, List<B> bs, ZipIterator<A, B> each) {
        Iterator<A> it1 = as.iterator();
        Iterator<B> it2 = bs.iterator();
        List<Pair<A, B>> result = new ArrayList<>();
        while (it1.hasNext() && it2.hasNext()) {
            A a = it1.next();
            B b = it2.next();
            if (each.each(a, b)) {
                result.add(new Pair<A, B>(a, b));
            }
        }
        return result;
    }

    public static class Pair<A, B> {
        private final A key;
        private final B value;

        public Pair(A key, B value) {
            this.key = key;
            this.value = value;
        }

        public A getKey() {
            return key;
        }

        public B getValue() {
            return value;
        }

        public String toString() {
            return "{" + key + "," + value + "}";
        }
    }
}
