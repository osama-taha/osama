
package com.hp.printosmobile.data.remote.models.ExtendedStatistics;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class GeneralData {

    @JsonProperty("headers")
    private Headers headers;
    @JsonProperty("printer_data")
    private PrinterData printerData;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * @return The headers
     */
    @JsonProperty("headers")
    public Headers getHeaders() {
        return headers;
    }

    /**
     * @param headers The headers
     */
    @JsonProperty("headers")
    public void setHeaders(Headers headers) {
        this.headers = headers;
    }

    /**
     * @return The printerData
     */
    @JsonProperty("printer_data")
    public PrinterData getPrinterData() {
        return printerData;
    }

    /**
     * @param printerData The printer_data
     */
    @JsonProperty("printer_data")
    public void setPrinterData(PrinterData printerData) {
        this.printerData = printerData;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class Headers {

        @JsonProperty("AccountId")
        private String accountId;
        @JsonProperty("PrinterSerialNumber")
        private String printerSerialNumber;
        @JsonProperty("PrinterProductNumber")
        private String printerProductNumber;
        @JsonProperty("PrinterProductName")
        private String printerProductName;
        @JsonProperty("PrinterFirmwareVersion")
        private String printerFirmwareVersion;
        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<String, Object>();

        /**
         * @return The accountId
         */
        @JsonProperty("AccountId")
        public String getAccountId() {
            return accountId;
        }

        /**
         * @param accountId The AccountId
         */
        @JsonProperty("AccountId")
        public void setAccountId(String accountId) {
            this.accountId = accountId;
        }

        /**
         * @return The printerSerialNumber
         */
        @JsonProperty("PrinterSerialNumber")
        public String getPrinterSerialNumber() {
            return printerSerialNumber;
        }

        /**
         * @param printerSerialNumber The PrinterSerialNumber
         */
        @JsonProperty("PrinterSerialNumber")
        public void setPrinterSerialNumber(String printerSerialNumber) {
            this.printerSerialNumber = printerSerialNumber;
        }

        /**
         * @return The printerProductNumber
         */
        @JsonProperty("PrinterProductNumber")
        public String getPrinterProductNumber() {
            return printerProductNumber;
        }

        /**
         * @param printerProductNumber The PrinterProductNumber
         */
        @JsonProperty("PrinterProductNumber")
        public void setPrinterProductNumber(String printerProductNumber) {
            this.printerProductNumber = printerProductNumber;
        }

        /**
         * @return The printerProductName
         */
        @JsonProperty("PrinterProductName")
        public String getPrinterProductName() {
            return printerProductName;
        }

        /**
         * @param printerProductName The PrinterProductName
         */
        @JsonProperty("PrinterProductName")
        public void setPrinterProductName(String printerProductName) {
            this.printerProductName = printerProductName;
        }

        /**
         * @return The printerFirmwareVersion
         */
        @JsonProperty("PrinterFirmwareVersion")
        public String getPrinterFirmwareVersion() {
            return printerFirmwareVersion;
        }

        /**
         * @param printerFirmwareVersion The PrinterFirmwareVersion
         */
        @JsonProperty("PrinterFirmwareVersion")
        public void setPrinterFirmwareVersion(String printerFirmwareVersion) {
            this.printerFirmwareVersion = printerFirmwareVersion;
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }

    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class PrinterData {

        @JsonProperty("id")
        private Integer id;
        @JsonProperty("data")
        private com.hp.printosmobile.data.remote.models.ExtendedStatistics.PrinterData data;
        @JsonProperty("service_id")
        private Object serviceId;
        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<String, Object>();

        /**
         * @return The id
         */
        @JsonProperty("id")
        public Integer getId() {
            return id;
        }

        /**
         * @param id The id
         */
        @JsonProperty("id")
        public void setId(Integer id) {
            this.id = id;
        }

        /**
         * @return The data
         */
        @JsonProperty("data")
        public com.hp.printosmobile.data.remote.models.ExtendedStatistics.PrinterData getData() {
            return data;
        }

        /**
         * @param data The data
         */
        @JsonProperty("data")
        public void setData(com.hp.printosmobile.data.remote.models.ExtendedStatistics.PrinterData data) {
            this.data = data;
        }

        /**
         * @return The serviceId
         */
        @JsonProperty("service_id")
        public Object getServiceId() {
            return serviceId;
        }

        /**
         * @param serviceId The service_id
         */
        @JsonProperty("service_id")
        public void setServiceId(Object serviceId) {
            this.serviceId = serviceId;
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }

    }
}
