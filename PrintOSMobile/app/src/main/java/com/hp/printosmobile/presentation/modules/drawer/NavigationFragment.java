package com.hp.printosmobile.presentation.modules.drawer;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.ViewCompat;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.hp.printosmobile.BuildConfig;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitViewModel;
import com.hp.printosmobile.presentation.modules.main.IMainFragment;
import com.hp.printosmobile.presentation.modules.shared.UserViewModel;
import com.hp.printosmobile.utils.AppUtils;
import com.hp.printosmobile.utils.CustomTypefaceSpan;
import com.hp.printosmobile.utils.EmailUtils;
import com.hp.printosmobile.utils.HPLocaleUtils;
import com.hp.printosmobile.utils.HPUIUtils;
import com.hp.printosmobilelib.core.communications.remote.APIException;
import com.hp.printosmobilelib.core.logging.HPLogger;
import com.hp.printosmobilelib.ui.common.HPFragment;
import com.hp.printosmobilelib.ui.widgets.TypefaceManager;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * A fragment to manage the NavigationDrawer.
 * Activities that contain this fragment must implement the {@link NavigationFragmentCallbacks} interface
 * to handle interaction events.
 *
 * @author Osama Taha
 */
public class NavigationFragment extends HPFragment implements NavView, IMainFragment, BusinessUnitsAdapter.BusinessUnitsAdapterCallbacks {

    private static final String TAG = NavigationFragment.class.getSimpleName();

    @Bind(R.id.navigation_drawer)
    NavigationView navigationView;
    @Bind(R.id.navigation_drawer_bottom)
    NavigationView navigationViewFooter;
    @Bind(R.id.refer_a_friend_button)
    View referAFriendView;
    @Bind(R.id.header)
    View header;

    TextView userNameTextView;
    View expandArrow;
    TextView emailTextView;

    NavigationPresenter presenter;

    private NavigationFragmentCallbacks callbacks;
    private OrganizationViewModel selectedOrganization;
    private TextView serverNameTextView;
    private View serverInfoLayout;

    public NavigationFragment() {
        // Required empty public constructor
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
    }

    private void initView() {

        presenter = new NavigationPresenter();
        presenter.attachView(this);

        //Set the tint color of the menu icons to be null in order to show the original menu icons.
        navigationView.setItemIconTintList(null);
        navigationViewFooter.setItemIconTintList(null);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            navigationView.setElevation(0);
        } else {
            ViewCompat.setElevation(navigationView, 0);
        }

        Menu m = navigationView.getMenu();
        for (int i=0;i<m.size();i++) {
            MenuItem mi = m.getItem(i);
            applyFontToMenuItem(mi);
        }
        m.findItem(R.id.drawer_send_beta_test_review).setVisible(BuildConfig.isBetaVersion);

        m = navigationViewFooter.getMenu();
        for (int i=0;i<m.size();i++) {
            MenuItem mi = m.getItem(i);
            applyFontToMenuItem(mi);
        }

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {

                callbacks.onMenuItemSelected();

                switch (menuItem.getItemId()) {
                    case R.id.drawer_notification_item:
                        callbacks.onNotificationButtonClicked();
                        break;
                    case R.id.drawer_about_item:
                        callbacks.onAboutMenuItemClicked();
                        break;
                    case R.id.drawer_ask_question_item:
                    case R.id.drawer_send_feedback_item:
                    case R.id.drawer_report_problem_item:
                        callbacks.onContactHPClicked(menuItem.getItemId());
                        break;
                    case R.id.drawer_privacy_item:
                        callbacks.onPrivacyMenuItemClick();
                        break;
                    case R.id.drawer_settings_item:
                        callbacks.onSettingMenuItemClicked();
                        break;
                    case R.id.drawer_send_beta_test_review:
                        sendFeedbackWithAttachmentThroughEmail();
                        break;
                }

                HPLogger.d(TAG,"Click menu item " + menuItem.getTitle());

                callbacks.onMenuItemClicked(menuItem.getItemId());
                return false;
            }
        });

        navigationViewFooter.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {

                callbacks.onMenuItemSelected();

                switch (menuItem.getItemId()) {
                    case R.id.drawer_switch_division_item:
                        callbacks.onDivisionSwitchMenuItemClick();
                        break;
                    case R.id.drawer_signout_item:
                        callbacks.onLogoutMenuItemClicked();
                        break;
                }

                HPLogger.d(TAG,"Click menu item " + menuItem.getTitle());

                callbacks.onMenuItemClicked(menuItem.getItemId());
                return false;
            }
        });

        boolean isReferFeatureEnabled = PrintOSPreferences.getInstance(getActivity()).isReferToAFriendEnabled();
        String referUrl = PrintOSPreferences.getInstance(getActivity()).getReferToAFriendUrl();
        referAFriendView.setVisibility(isReferFeatureEnabled && referUrl != null ? View.VISIBLE : View.GONE);

        setUserAndServerInfo();

        TextView organizationTextView = (TextView) header.findViewById(R.id.user_accounts_text_view);
        OrganizationViewModel model = PrintOSPreferences.getInstance(getActivity()).getSelectedOrganization();
        if(model != null && model.getOrganizationName() != null) {
            organizationTextView.setText(model.getOrganizationName());
        }
    }

    @OnClick(R.id.refer_a_friend_button)
    public void onReferAFriendClicked () {
        String referUrl = PrintOSPreferences.getInstance(getActivity()).getReferToAFriendUrl();
        if (referUrl != null) {
            AppUtils.startApplication(getActivity(), Uri.parse(referUrl));
        }
    }

    public void onValidationCompleted() {
        initAccountSpinner();
    }

    public void setNumberOfNotifications (int numberOfNotifications) {
        Menu menu = navigationView.getMenu();
        MenuItem notificationMenuItem = menu.findItem(R.id.drawer_notification_item);
        notificationMenuItem.setIcon(numberOfNotifications > 0 ?
                HPUIUtils.getNotificationIconBadgeDrawable(getActivity(), String.valueOf(numberOfNotifications))
                : ResourcesCompat.getDrawable(getResources(), R.drawable.notifications, null));
    }

    private void applyFontToMenuItem(MenuItem menuItem) {
        Typeface font = TypefaceManager.getTypeface(PrintOSApplication.getAppContext(), TypefaceManager.HPTypeface.HP_REGULAR);
        SpannableString mNewTitle = new SpannableString(menuItem.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("" , font), 0 , mNewTitle.length(),  Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        menuItem.setTitle(mNewTitle);
    }

    private void initAccountSpinner() {
        userNameTextView = (TextView) header.findViewById(R.id.user_accounts_text_view);
        expandArrow = header.findViewById(R.id.expand_arrow);
        presenter.getOrganizations();
    }

    private void setUserAndServerInfo() {

        userNameTextView = (TextView) header.findViewById(R.id.drawer_header_user_name);
        emailTextView = (TextView) header.findViewById(R.id.drawer_header_email);
        serverInfoLayout = header.findViewById(R.id.drawer_header_server_info_layout);
        serverNameTextView = (TextView) header.findViewById(R.id.drawer_header_server_name);

        UserViewModel userViewModel = PrintOSPreferences.getInstance(getContext()).getUserInfo();

        userNameTextView.setText(userViewModel.getDisplayName());
        emailTextView.setText(userViewModel.getEmail());

        PrintOSPreferences userPreferences = PrintOSPreferences.getInstance(PrintOSApplication.getAppContext());

        if (userPreferences.showServerName()) {
            String serverName = userPreferences.getServerName();
            serverInfoLayout.setVisibility(View.VISIBLE);
            serverNameTextView.setText(serverName);
        } else {
            serverInfoLayout.setVisibility(View.GONE);
        }

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_navigation;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof NavigationFragmentCallbacks) {
            callbacks = (NavigationFragmentCallbacks) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement NavigationFragmentCallbacks");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();

        callbacks = null;
        presenter.detachView();
    }

    @Override
    public void onBusinessUnitSelected(BusinessUnitViewModel businessUnitViewModel, boolean resetFilters) {
    }

    @Override
    public void onBusinessUnitSelected(BusinessUnitViewModel businessUnitViewModel) {
    }

    @Override
    public void onFiltersChanged(BusinessUnitViewModel businessUnitViewModel) {
        //FOR LATER USE.
    }

    @Override
    public void didFinishGettingBusinessUnits(Map<BusinessUnitEnum, BusinessUnitViewModel> businessUnitsMap) {
        enableMenuItem(R.id.drawer_switch_division_item, businessUnitsMap.size() > 0);
    }

    @Override
    public void onMainError(String msg, boolean triggerSignOut) {
        didFinishGettingBusinessUnits(new HashMap<BusinessUnitEnum, BusinessUnitViewModel>());
    }

    @Override
    public boolean respondToValidationPeriodExceedance() {
        return false;
    }

    private void enableMenuItem(int id, boolean isEnabled) {
        MenuItem menuItem = navigationView.getMenu().findItem(id);
        if (menuItem != null) {
            menuItem.setEnabled(isEnabled);
        } else {
            menuItem = navigationViewFooter.getMenu().findItem(id);
            if (menuItem != null) {
                menuItem.setEnabled(isEnabled);
            }
        }
    }

    @Override
    public void onError(APIException exception, String TAG) {

    }

    @Override
    public boolean isFragmentNameToBeDisplayed() {
        return false;
    }

    @Override
    public boolean isFilteringAvailable() {
        return false;
    }

    @Override
    public int getToolbarDisplayName() {
        return R.string.unknown_value;
    }

    @Override
    public void onOrganizationsRetrieved(List<OrganizationViewModel> organizations) {

        if (!isAdded() || getActivity() == null) {
            return;
        }

        String selectedOrganizationID = "";
        if(organizations != null) {
            Collections.sort(organizations, OrganizationViewModel.OrganizationComparator);

            if (organizations.size() <= 1) {
                header.findViewById(R.id.account_container).setOnClickListener(null);
                userNameTextView.setText(organizations.size() != 0 ? organizations.get(0).getOrganizationName() : "");
                expandArrow.setVisibility(View.GONE);

            } else {

                expandArrow.setVisibility(View.VISIBLE);
                header.findViewById(R.id.account_container).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        callbacks.onMenuItemSelected();
                        callbacks.onChangeOrganizationClicked();
                    }
                });

                for (OrganizationViewModel organization : organizations) {
                    if (organization.getOrganizationId().equals(PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).getSavedOrganizationId())) {
                        userNameTextView.setText(organization.getOrganizationName());
                        selectedOrganizationID = organization.getOrganizationId();
                        break;
                    }
                }
            }
        }

        if(callbacks != null) {
            callbacks.onOrganizationsRetrieved(organizations, selectedOrganizationID);
        }
    }

    public void onOrganizationSelected(OrganizationViewModel organizationViewModel){
        //Prevent the selected organization from being selected again.
        if (!PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).getSavedOrganizationId().equals(organizationViewModel.getOrganizationId())) {
            if (presenter != null) {
                presenter.changeContext(organizationViewModel);
                callbacks.onOrganizationSelected();
            }
        }
    }

    @Override
    public void onContextChanged(boolean isSuccessful, OrganizationViewModel organizationViewModel,
                                 APIException e) {
        if(isSuccessful) {
            if(organizationViewModel != null) {
                selectedOrganization = organizationViewModel;
                PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).saveOrganization(selectedOrganization);
                ((TextView) header.findViewById(R.id.user_accounts_text_view)).setText(organizationViewModel.getOrganizationName());
            }
        } else {
            HPUIUtils.displayToast(getActivity(), HPLocaleUtils.getSimpleErrorMsg(PrintOSApplication.getAppContext(), e), false);
        }
        callbacks.onContextChanged(isSuccessful, organizationViewModel == null ? null : organizationViewModel.getOrganizationId());
    }

    public void reload() {
        initAccountSpinner();
    }

    private void sendFeedbackWithAttachmentThroughEmail() {

        String to = getString(R.string.reviewer_email);
        String subject = getString(R.string.beta_tester_review_email_title);
        String body = getDeviceInfo();
        File logsFile = new File(HPLogger.getInstance(getActivity()).getFilePath());

        HPLogger.d(TAG, "Sending beta testing review");

        if (logsFile != null) {

            ArrayList<Uri> attachments = new ArrayList<>();
            attachments.add(Uri.fromFile(logsFile));
            EmailUtils.sendEmail(getActivity(), new String[]{to}, subject, body, attachments);

        } else {
            HPLogger.e(TAG, "Error occurred while sending Beta tester review");
            HPUIUtils.displayToast(getActivity(), getActivity().getString(R.string.unable_to_attach_log_file), false);
        }
    }

    private String getDeviceInfo(){
        String info = getString(R.string.beta_tester_review_email_body, Build.MODEL, Build.VERSION.SDK_INT);

        return info;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment.
     *
     * @author Osama Taha
     */
    public interface NavigationFragmentCallbacks {

        void onOrganizationSelected();

        void onContextChanged(boolean isSuccessful, String selectedOrganizationID);

        void onMenuItemSelected();

        void onContactHPClicked(int id);

        void onDivisionSwitchMenuItemClick();

        void onLogoutMenuItemClicked();

        void onNotificationButtonClicked();

        void onAboutMenuItemClicked();

        void onPrivacyMenuItemClick();

        void onMenuItemClicked(int menuItemId);

        void onOrganizationsRetrieved(List<OrganizationViewModel> organizationViewModels, String selectOrganizationID);

        void onChangeOrganizationClicked();

        void onSettingMenuItemClicked();

    }
}