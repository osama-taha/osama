package com.hp.printosmobile.presentation.modules.production;


import android.os.Bundle;
import com.hp.printosmobilelib.core.logging.HPLogger;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitViewModel;
import com.hp.printosmobile.presentation.modules.main.IMainFragment;
import com.hp.printosmobilelib.ui.common.HPFragment;

import java.util.Map;

import butterknife.Bind;

public class ProductionFragment extends HPFragment implements IProductionView, IMainFragment {

    public final static String TAG = ProductionFragment.class.getSimpleName();

    @Bind(R.id.number_of_printed_jobs)
    TextView mNumberOfPrintedJobsText;

    @Bind(R.id.refresh_production)
    Button refreshProductionButton;


    ProductionFragmentPresenter presenter;

    public ProductionFragment() {
        // Required empty public constructor

    }

    public static ProductionFragment newInstance() {
        ProductionFragment homeFragment = new ProductionFragment();

        return homeFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new ProductionFragmentPresenter(getContext());
        presenter.attachView(this);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View ret = super.onCreateView(inflater, container, savedInstanceState);
        presenter=new ProductionFragmentPresenter(getContext());
        presenter.attachView(this);

        refreshProductionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.refresh();
            }
        });
        return ret;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_production;
    }


    @Override
    public void onBusinessUnitSelected(BusinessUnitViewModel businessUnitViewModel) {
        HPLogger.d(TAG, "Update " + TAG);
    }

    @Override
    public void onFiltersChanged(BusinessUnitViewModel businessUnitViewModel) {

    }

    @Override
    public void didFinishGettingBusinessUnits(Map<BusinessUnitEnum, BusinessUnitViewModel> businessUnitsMap) {

    }

    @Override
    public void onMainError(String msg, boolean triggerSignOut) {

    }

    @Override
    public boolean respondToValidationPeriodExceedance() {
        return false;
    }

    public void setNumberOfJobs(String numberOfJobs) {
        HPLogger.d(TAG, "setNumberOfJobs:" + numberOfJobs);
        mNumberOfPrintedJobsText.setText(numberOfJobs);
    }

    @Override
    public void onDestroyView() {
        presenter.detachView();
        super.onDestroyView();
    }

    @Override
    public boolean isFragmentNameToBeDisplayed() {
        return false;
    }

    @Override
    public boolean isFilteringAvailable() {
        return false;
    }

    @Override
    public int getToolbarDisplayName() {
        return R.string.unknown_value;
    }
}
