package com.hp.printosmobile.presentation.modules.beatcoins;

import android.app.Dialog;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.text.style.UnderlineSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.AppseeSdk;
import com.hp.printosmobile.R;
import com.hp.printosmobile.utils.AppUtils;
import com.hp.printosmobilelib.core.logging.HPLogger;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by anwar asbah on 7/17/2017.
 */
public class BeatCoinsPopup extends DialogFragment {

    public static final String TAG = BeatCoinsPopup.class.getSimpleName();

    private static final String VIEW_MODEL_ARG = "viewModelAgr";

    @Bind(R.id.title)
    TextView titleTextView;
    @Bind(R.id.cancel_button)
    TextView cancelButton;
    @Bind(R.id.expiry_text_view)
    TextView expiryTextView;

    BeatCoinsViewModel beatCoinsViewModel;
    BeatCoinPopupCallback listener;

    public static BeatCoinsPopup getInstance(BeatCoinPopupCallback popupCallback, BeatCoinsViewModel viewModel) {
        BeatCoinsPopup dialog = new BeatCoinsPopup();

        dialog.listener = popupCallback;
        Bundle bundle = new Bundle();
        bundle.putSerializable(VIEW_MODEL_ARG, viewModel);
        dialog.setArguments(bundle);

        return dialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE,
                R.style.NPSDialogStyle);

        Bundle bundle = getArguments();
        if (bundle != null && bundle.containsKey(VIEW_MODEL_ARG)) {
            beatCoinsViewModel = (BeatCoinsViewModel) bundle.getSerializable(VIEW_MODEL_ARG);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.beat_coin_popup, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        initView();
        getDialog().setCanceledOnTouchOutside(false);
        AppseeSdk.getInstance(getActivity()).startScreen(AppseeSdk.SCREEN_BEAT_COINS);
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        }
    }

    private void initView() {

        String cancelText = getString(R.string.beat_coins_cancel_button);
        SpannableString notSureSpannable = new SpannableString(cancelText);
        notSureSpannable.setSpan(new UnderlineSpan(), 0, cancelText.length(), 0);
        cancelButton.setText(notSureSpannable);

        String title = getString(R.string.nps_title, beatCoinsViewModel.getNumberOfBeatCoins());
        String bitCoinsString = String.valueOf(beatCoinsViewModel.getNumberOfBeatCoins());
        int start = title.indexOf(bitCoinsString);
        int end = start + bitCoinsString.length();
        SpannableString titleSpannable = new SpannableString(title);
        titleSpannable.setSpan(new RelativeSizeSpan(2f), start, end, 0);
        titleTextView.setText(titleSpannable);

        expiryTextView.setText(getString(R.string.beat_coins_link_expiration_text,
                beatCoinsViewModel.getNumberOfDaysTillExpiry()));
    }

    @Override
    public void dismiss() {
        super.dismiss();
        if (listener != null) {
            listener.onDialogDismissed();
        }
    }

    @OnClick(R.id.cancel_button)
    public void notSureClicked() {
        dismiss();
    }

    @OnClick(R.id.collect_button)
    public void collectClicked() {

        if (getActivity() == null) {
            return;
        }

        HPLogger.d(TAG, "collect coins clicked.");

        Analytics.sendEvent(Analytics.COLLECT_COINS_CLICKED_ACTION);
        AppseeSdk.getInstance(getActivity()).sendEvent(AppseeSdk.EVENT_COLLECT_COINS_CLICKED);

        if (beatCoinsViewModel.getUrl() != null) {
            AppUtils.startApplication(getActivity(), Uri.parse(beatCoinsViewModel.getUrl()));
        }
        dismiss();
    }

    public interface BeatCoinPopupCallback {
        void onDialogDismissed();
    }
}
