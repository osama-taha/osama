package com.hp.printosmobile.presentation.modules.filters;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobilelib.ui.common.HPFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;

/**
 * Created by Osama Taha on 10/9/16.
 */
public class FiltersListFragment extends HPFragment implements FilterItemsAdapter.FilterItemsAdapterCallbacks {

    private static final String ARG_FILTER_ITEMS = "FILTER_ITEMS";

    @Bind(R.id.items_recycler_view)
    RecyclerView itemsRecyclerView;
    @Bind(R.id.list_empty)
    TextView emptyListTextView;

    private List<FilterItem> mItems;

    private FiltersListFragmentCallbacks mListener;
    private FilterItemsAdapter adapter;

    public static FiltersListFragment newInstance(List<FilterItem> items) {
        FiltersListFragment fragment = new FiltersListFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_FILTER_ITEMS, new ArrayList<>(items));
        fragment.setArguments(args);
        return fragment;
    }

    public FiltersListFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mItems = (ArrayList<FilterItem>) getArguments().getSerializable(ARG_FILTER_ITEMS);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initView();
    }

    private void initView() {

        RecyclerView.LayoutManager manager = new LinearLayoutManager(getContext());
        itemsRecyclerView.setLayoutManager(manager);

        adapter = new FilterItemsAdapter(getActivity(), mItems, this);

        itemsRecyclerView.setAdapter(adapter);
        itemsRecyclerView.setHasFixedSize(true);

        itemsRecyclerView.setVisibility(mItems.size() > 0 ? View.VISIBLE : View.INVISIBLE);
        emptyListTextView.setVisibility(mItems.size() > 0 ? View.INVISIBLE : View.VISIBLE);

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_filters_list;
    }

    @Override
    public boolean isFragmentNameToBeDisplayed() {
        return false;
    }

    @Override
    public boolean isFilteringAvailable() {
        return false;
    }

    @Override
    public int getToolbarDisplayName() {
        return 0;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (getParentFragment() instanceof FiltersListFragmentCallbacks) {
            mListener = (FiltersListFragmentCallbacks) getParentFragment();
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement FiltersFragmentCallbacks");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onItemSelected(boolean selected, FilterItem filterItem) {

        mListener.onItemSelected(selected, filterItem);
    }

    public void updateView(List<FilterItem> items) {

        if (adapter != null) {

            this.mItems = items;
            adapter.addItems(items);

            RecyclerView.LayoutManager manager = new LinearLayoutManager(getContext());
            itemsRecyclerView.setLayoutManager(manager);

            itemsRecyclerView.setVisibility(mItems.size() > 0 ? View.VISIBLE : View.INVISIBLE);
            emptyListTextView.setVisibility(mItems.size() > 0 ? View.INVISIBLE : View.VISIBLE);

            emptyListTextView.setText(PrintOSApplication.getAppContext().getString(R.string.filter_no_groups));
        }
    }

    public void manageSelection(FilterItem filterItem, boolean select) {
        if (adapter != null) {
            if (filterItem instanceof SiteViewModel) {
                adapter.clearSelections();
                adapter.selectItem(filterItem, true);
            } else if (filterItem instanceof GroupViewModel) {
                adapter.clearSelections();
                adapter.selectItem(filterItem, select);
            } else {
                adapter.selectItem(filterItem, select);
            }
        }
    }

    public List<FilterItem> getItems() {
        return mItems;
    }

    public boolean isEmpty() {
        return mItems == null || mItems.size() == 0;
    }

    public List<FilterItem> getSelectedItems() {
        if (adapter == null) {
            return new ArrayList<>();
        }
        return adapter.getSelectedItems();
    }

    public void selectAll() {
        adapter.selectAll();
    }

    public void selectItems(List<FilterItem> items) {
        if (adapter != null) {
            adapter.selectRange(items);
        }
    }

    public void clearSelection() {
        adapter.clearSelections();
    }

    public interface FiltersListFragmentCallbacks {
        void onItemSelected(boolean selected, FilterItem filterItem);
    }

}
