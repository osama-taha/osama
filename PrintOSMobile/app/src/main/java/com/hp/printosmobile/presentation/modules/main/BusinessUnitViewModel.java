package com.hp.printosmobile.presentation.modules.main;

import com.hp.printosmobile.data.remote.models.DeviceData;
import com.hp.printosmobile.data.remote.models.PreferencesData;
import com.hp.printosmobile.presentation.modules.filters.FiltersViewModel;
import com.hp.printosmobile.presentation.modules.filters.SiteViewModel;
import com.hp.printosmobile.presentation.modules.home.Panel;

import java.io.Serializable;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

/**
 * A model represents a single business unit with its connected devices,sites and groups
 * <p/>
 * Created by Osama Taha on 5/31/16.
 */
public class BusinessUnitViewModel implements Serializable, Comparable<BusinessUnitViewModel> {

    private BusinessUnitEnum businessUnit;
    private List<Panel> panels;
    private Map<String, DeviceData> devices;
    private List<BusinessUnitKpi> businessUnitKpi;
    private FiltersViewModel filtersViewModel;

    public static Comparator<BusinessUnitViewModel> businessUnitViewModelNameComparator = new Comparator<BusinessUnitViewModel>() {
        @Override
        public int compare(BusinessUnitViewModel lhs, BusinessUnitViewModel rhs) {
            if (lhs == null && rhs == null) return 0;
            if (lhs == null && rhs != null) return 1;
            if (lhs != null && rhs == null) return -1;
            return lhs.compareTo(rhs);
        }
    };
    private SiteViewModel selectSite;
    private PreferencesData.UnitSystem unitSystem;

    public BusinessUnitEnum getBusinessUnit() {
        return businessUnit;
    }

    public void setBusinessUnit(BusinessUnitEnum businessUnit) {
        this.businessUnit = businessUnit;
    }

    public List<Panel> getPanels() {
        return panels;
    }

    public void setPanels(List<Panel> panels) {
        this.panels = panels;
    }

    public Map<String, DeviceData> getDevices() {
        return devices;
    }

    public void setDevices(Map<String, DeviceData> devices) {
        this.devices = devices;
    }

    public String getDeviceName(String serialNumber) {

        if (serialNumber == null) {
            return "";
        }
        for (String key : devices.keySet()) {
            DeviceData deviceData = devices.get(key);
            if (serialNumber.equals(deviceData.getSerialNumber())) {
                return deviceData.getDeviceName();
            }
        }

        return serialNumber;
    }

    public List<BusinessUnitKpi> getBusinessUnitKpi() {
        return businessUnitKpi;
    }

    public void setBusinessUnitKpi(List<BusinessUnitKpi> businessUnitKpi) {
        this.businessUnitKpi = businessUnitKpi;
    }

    public void setFiltersViewModel(FiltersViewModel filtersViewModel) {
        this.filtersViewModel = filtersViewModel;
    }

    public FiltersViewModel getFiltersViewModel() {
        return filtersViewModel;
    }

    @Override
    public String toString() {
        return "BusinessUnitViewModel{" +
                "businessUnit=" + businessUnit +
                ", panels=" + panels +
                ", devices=" + devices +
                ", businessUnitKpi=" + businessUnitKpi +
                ", filtersViewModel=" + filtersViewModel +
                '}';
    }

    @Override
    public int compareTo(BusinessUnitViewModel another) {
        if (another == null || another.getBusinessUnit().getBusinessUnitDisplayName() == null) {
            return -1;
        }
        return getBusinessUnit().getBusinessUnitDisplayName().compareTo(another.getBusinessUnit().getBusinessUnitDisplayName());
    }

    public static class BusinessUnitKpi implements Serializable {

        private String kpiKeyName;
        private ReportTypeEnum reportTypeEnum;
        private ReportChartTypeEnum reportChartTypeEnum;

        public String getkpiKeyName() {
            return kpiKeyName;
        }

        public void setReportKpiEnum(String kpiKeyName) {
            this.kpiKeyName = kpiKeyName;
        }

        public ReportTypeEnum getReportTypeEnum() {
            return reportTypeEnum;
        }

        public void setReportTypeEnum(ReportTypeEnum reportTypeEnum) {
            this.reportTypeEnum = reportTypeEnum;
        }

        public ReportChartTypeEnum getReportChartTypeEnum() {
            return reportChartTypeEnum;
        }

        public void setReportChartTypeEnum(ReportChartTypeEnum reportChartTypeEnum) {
            this.reportChartTypeEnum = reportChartTypeEnum;
        }
    }

}
