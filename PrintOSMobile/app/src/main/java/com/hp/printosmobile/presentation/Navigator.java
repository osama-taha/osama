package com.hp.printosmobile.presentation;

import android.content.Context;
import android.content.Intent;

import com.hp.printosmobile.Constants;
import com.hp.printosmobile.presentation.modules.login.LoginActivity;
import com.hp.printosmobile.presentation.modules.main.MainActivity;
import com.hp.printosmobilelib.core.logging.HPLogger;

/**
 * A class responsible to manage the navigation between the activities.
 * <p/>
 * Created by Osama Taha on 4/10/16.
 */
public class Navigator {

    private static final String TAG = Navigator.class.getName();

    public static void openMainActivity(Context context, String universalLinkScreen) {

        Intent intent = new Intent(context, MainActivity.class);
        if (universalLinkScreen != null) {
            HPLogger.d(TAG, "Screen name " + universalLinkScreen);
            intent.putExtra(Constants.IntentExtras.UNIVERSAL_LINK_SCREEN_KEY, universalLinkScreen);
        }
        context.startActivity(intent);

    }

    public static void openLoginActivity(Context context) {

        Intent intent = new Intent(context, LoginActivity.class);
        context.startActivity(intent);

    }
}
