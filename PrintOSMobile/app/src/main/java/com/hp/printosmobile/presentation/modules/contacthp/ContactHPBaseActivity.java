package com.hp.printosmobile.presentation.modules.contacthp;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.BaseActivity;
import com.hp.printosmobile.presentation.modules.contacthp.shared.AttachImageView;
import com.hp.printosmobile.presentation.modules.contacthp.shared.ImageFetchListener;
import com.hp.printosmobile.utils.HPUIUtils;
import com.hp.printosmobilelib.core.communications.remote.APIException;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created be Anwar Asbah 10/11/2016
 */
public abstract class ContactHPBaseActivity extends BaseActivity implements ContactHpView , AttachImageView.AttachImageClickListener {

    public static final String TAG = ContactHPBaseActivity.class.getSimpleName();
    private static final int PICK_PHOTO_PERMISSION_REQUEST_CODE = 0;
    private static final int PICK_PHOTO_INTENT_REQUEST_CODE = 2;
    private static final String IMAGE_INTENT_EXTRA = "image/png";

    @Bind(R.id.case_layout)
    View caseLayout;
    @Bind(R.id.progress_arc)
    View progressArc;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.toolbar_display_title)
    TextView toolbarDisplayName;

    private boolean isEnabled;
    ContactHpPresenter presenter;
    ImageFetchListener imageFetchListener;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        caseLayout.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus){
                    hideSoftKeyboard();
                }
            }
        });

        isEnabled = true;

        init();
        intiPresenter();
        initToolbar();
        hideSoftKeyboard();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_PHOTO_INTENT_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            if(this.imageFetchListener != null){
                imageFetchListener.onImageFetched(data.getData());
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void intiPresenter() {
        presenter = new ContactHpPresenter();
        presenter.attachView(this);
        presenter.hasPhone();
    }

    private void initToolbar () {
        toolbarDisplayName.setText(getString(getTitleResourceID()));
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(ResourcesCompat.getDrawable(getResources(), R.drawable.back_button, null));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void onAttachImageClicked(ImageFetchListener imageFetchListener) {
        this.imageFetchListener = imageFetchListener;

        Intent intent;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            intent = new Intent(Intent.ACTION_PICK);
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, false);
            intent.addFlags(Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {

                    if(ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)){
                        HPUIUtils.displayToast(this, getString(R.string.contact_hp_permission_not_granted_msg), false);
                    }else {
                        ActivityCompat.requestPermissions(this,
                                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                PICK_PHOTO_PERMISSION_REQUEST_CODE);
                    }
                    return;
                }
            }

        } else {
            intent = new Intent(Intent.ACTION_GET_CONTENT);
        }

        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        intent.setType(IMAGE_INTENT_EXTRA);

        startActivityForResult(intent, PICK_PHOTO_INTENT_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PICK_PHOTO_PERMISSION_REQUEST_CODE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Intent intent = new Intent(Intent.ACTION_PICK);
                    intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, false);
                    intent.addFlags(Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION);
                    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    intent.setType(IMAGE_INTENT_EXTRA);
                    startActivityForResult(intent, PICK_PHOTO_INTENT_REQUEST_CODE);

                }
                return;
            }
        }
    }

    @OnClick(R.id.cancel_button)
    public void onCancelButtonClicked() {
        onBackPressed();
    }

    @OnClick(R.id.send_button)
    public void onSendButtonClicked() {
        if (isFormFilled() && isEnabled) {
            sendRequest();
            setEnabled(false);
            setLoading(true);
        }
    }

    public void setLoading(boolean isLoading) {
        progressArc.setVisibility(isLoading ? View.VISIBLE : View.GONE);
    }

    public void setEnabled(boolean isEnabled) {
        this.isEnabled = isEnabled;
        setFormEnable(isEnabled);
    }

    public boolean displayWarningMessage(String input, int stringID) {
        if (input == null || input.isEmpty()) {
            HPUIUtils.displayToast(this, getString(stringID), false);
            return false;
        }
        return true;
    }

    @Override
    public void onSuccess() {
        HPUIUtils.displayToast(PrintOSApplication.getAppContext(),
                PrintOSApplication.getAppContext().getString(R.string.contact_hp_request_success), false);
        onBackPressed();
    }

    @Override
    public void onError(APIException exception, String tag) {
        setLoading(false);
        setEnabled(true);

        HPUIUtils.displayToastException(this, exception, tag, false);
    }

    @Override
    public void onBackPressed() {
        hideSoftKeyboard();
        super.onBackPressed();
    }

    @Override
    public boolean isBackwardCompatible() {
        return true;
    }

    public abstract boolean isFormFilled();

    public abstract void sendRequest();

    public abstract void setHasPhone(boolean hasPhone);

    public abstract void setFormEnable(boolean isEnabled);

    protected abstract void init();

    protected abstract void hideSoftKeyboard();

    public abstract int getTitleResourceID ();
}
