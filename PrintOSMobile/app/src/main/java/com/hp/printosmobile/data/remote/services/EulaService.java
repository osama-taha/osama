package com.hp.printosmobile.data.remote.services;

import com.hp.printosmobile.data.remote.ApiConstants;

import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Path;
import rx.Observable;

/**
 * Retrofit REST interface definition for EULA
 *
 * @author Anwar Asbah
 */
public interface EulaService {

    /**
     * get EULA content
     */
    @GET(ApiConstants.EULA_URL)
    Observable<Response<ResponseBody>> getEULA(@Path("language") String language);

}
