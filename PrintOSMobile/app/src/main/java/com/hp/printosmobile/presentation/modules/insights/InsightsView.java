package com.hp.printosmobile.presentation.modules.insights;

import com.hp.printosmobile.presentation.MVPView;

/**
 * Created by Anwar Asbah on 6/20/17.
 */
public interface InsightsView extends MVPView {

    void onInsightDataRetrieved(InsightsViewModel viewModel, InsightsViewModel.InsightKpiEnum kpiEnum,
                                boolean isDataSelected);
}
