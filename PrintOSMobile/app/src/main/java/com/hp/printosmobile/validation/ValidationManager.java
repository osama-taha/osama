package com.hp.printosmobile.validation;

import android.content.Context;

import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.data.remote.ApiServicesProvider;
import com.hp.printosmobilelib.core.communications.remote.Preferences;
import com.hp.printosmobilelib.core.communications.remote.SessionManager;
import com.hp.printosmobilelib.core.communications.remote.models.OrganizationJsonBody;
import com.hp.printosmobilelib.core.communications.remote.models.UserData;
import com.hp.printosmobilelib.core.communications.remote.services.LoginService;

import okhttp3.ResponseBody;
import retrofit2.Response;
import rx.Observable;

/**
 * Created by Anwar Asbah on 6/7/2017.
 */

public class ValidationManager {

    public static Observable<ResponseBody> validateToken() {
        ApiServicesProvider apiServicesProvider = PrintOSApplication.getApiServicesProviderWithAuthenticator();
        LoginService loginService = apiServicesProvider.getLoginService();

        return loginService.checkCookieAsync().asObservable();
    }

    public static Observable<Response<UserData>> performAutoLogin(Context context) {
        ApiServicesProvider apiServicesProvider = PrintOSApplication.getApiServicesProviderWithAuthenticator();
        LoginService loginService = apiServicesProvider.getLoginService();

        return loginService.login("", SessionManager.getInstance(context).getUserCredentials()).asObservable();
    }

    public static Observable<ResponseBody> changeContext(Context context) {
        ApiServicesProvider apiServicesProvider = PrintOSApplication.getApiServicesProviderWithAuthenticator();
        LoginService loginService = apiServicesProvider.getLoginService();

        return loginService.changeOrganizationContextAsync(getOrganizationJsonBody(context)).asObservable();
    }

    public static OrganizationJsonBody getOrganizationJsonBody(Context context) {
        UserData.Context organizationContext = Preferences.getInstance(context).getSelectedOrganization();
        OrganizationJsonBody body = new OrganizationJsonBody();
        body.setId(organizationContext.getId());
        body.setName(organizationContext.getName());
        body.setType(organizationContext.getType());
        return body;
    }

}
