package com.hp.printosmobile.presentation.modules.notificationslist;

import android.app.NotificationManager;
import android.content.Context;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.daimajia.swipe.SimpleSwipeListener;
import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.daimajia.swipe.implments.SwipeItemRecyclerMangerImpl;
import com.daimajia.swipe.util.Attributes;
import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.AppseeSdk;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.presentation.modules.settings.SubscriptionEvent;
import com.hp.printosmobile.utils.HPLocaleUtils;
import com.hp.printosmobile.utils.HPUIUtils;
import com.hp.printosmobilelib.core.logging.HPLogger;
import com.hp.printosmobilelib.core.utils.HPDateUtils;
import com.hp.printosmobilelib.core.utils.StringUtils;

import java.util.Date;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Anwar Asbah on 1/23/2017.
 */
public class NotificationsListAdapter extends RecyclerSwipeAdapter<NotificationsListAdapter.NotificationsViewHolder> {

    private static final String TAG = NotificationsListAdapter.class.getSimpleName();
    private static final String BOX_URL = "%s/api/portal/v1/go/box-folder/%s";
    private static final int BOX_FOLDER_NAME_MAX_LENGTH = 10;
    private Context context;
    private List<NotificationViewModel> models;
    NotificationAdapterCallback callback;
    private int viewWidth = 0;
    protected SwipeItemRecyclerMangerImpl mItemManger = new SwipeItemRecyclerMangerImpl(this);

    public NotificationsListAdapter(Context context, List<NotificationViewModel> notificationViewModels,
                                    NotificationAdapterCallback callback, RecyclerView parentView) {
        this.context = context;
        this.callback = callback;
        this.models = notificationViewModels;

        viewWidth = parentView.getMeasuredWidth();
        setMode(Attributes.Mode.Single);
    }

    @Override
    public NotificationsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        View view = inflater.inflate(R.layout.notifications_list_item, parent, false);
        return new NotificationsViewHolder(view);
    }

    @Override
    public synchronized void onBindViewHolder(final NotificationsViewHolder viewHolder, int position) {

        NotificationViewModel model = models.get(position);

        viewHolder.swipeRevealLayout.setShowMode(SwipeLayout.ShowMode.LayDown);

        viewHolder.swipeRevealLayout.addSwipeListener(new SimpleSwipeListener() {
            @Override
            public void onOpen(SwipeLayout layout) {
                mItemManger.closeAllExcept(layout);
            }
        });

        viewHolder.position = position;

        String timeAgo = HPLocaleUtils.getTimeAgo(context, model.getSentDate(), false);
        viewHolder.sendDateTextView.setText(timeAgo);

        if (model.getNotificationEventEnum() == SubscriptionEvent.NotificationEventEnum.BOX_NEW_FOLDER) {

            String goLink = getGoLink(model);
            String folderName = model.getLinkLabel();
            //Limit folder name to @{link BOX_FOLDER_NAME_MAX_LENGTH} at max.
            folderName = folderName == null ? "" : (folderName.length() > BOX_FOLDER_NAME_MAX_LENGTH
                    ? folderName.substring(0, BOX_FOLDER_NAME_MAX_LENGTH) + StringUtils.ELLIPSIS : folderName);

            String body = context.getString(R.string.notifications_list_link, folderName,
                    model.getNotificationBody() == null ? context.getString(R.string.unknown_value) : model.getNotificationBody(),
                    getDateString(model.getSentDate()),
                    goLink);

            int start = body.indexOf(goLink);
            int end = body.length();

            Spannable spannableBody = new SpannableString(body);
            spannableBody.setSpan(new ForegroundColorSpan(ResourcesCompat.getColor(context.getResources(),
                    R.color.c62, null)), start, end, Spannable.SPAN_INCLUSIVE_INCLUSIVE);

            viewHolder.bodyTextView.setText(spannableBody);

        } else {
            viewHolder.bodyTextView.setText(model.getNotificationBody());
        }

        if (model.getNotificationEventEnum() != SubscriptionEvent.NotificationEventEnum.UNKNOWN) {
            viewHolder.eventTypeTextView.setBackground(ResourcesCompat.getDrawable(context.getResources(),
                    model.getNotificationEventEnum().getBgDrawableID(), null));
            viewHolder.eventTypeTextView.setText(context.getString(model.getNotificationEventEnum().getNameID()));
        }

        boolean isRead = model.isRead();
        viewHolder.notificationLayout.setBackgroundColor(ResourcesCompat.getColor(context.getResources(),
                isRead ? R.color.c92b : R.color.c92a, null));
        HPUIUtils.setVisibility(!isRead, viewHolder.markAsReadLayout);

        viewHolder.notificationLayout.measure(View.MeasureSpec.makeMeasureSpec(viewWidth, View.MeasureSpec.EXACTLY), View.MeasureSpec.UNSPECIFIED);
        int measuredHeight = viewHolder.notificationLayout.getMeasuredHeight();
        setViewHeight(viewHolder.notificationLayout, measuredHeight);
        setViewHeight(viewHolder.swipeRevealLayout, measuredHeight);
        setViewHeight(viewHolder.actionLayout, measuredHeight);

        mItemManger.bindView(viewHolder.swipeRevealLayout, position);

    }

    private String getDateString(Date date) {
        return date == null ? null : HPDateUtils.getDateTimeString(context, date, context.getString(
                R.string.date_range_different_year), null);
    }

    private void setViewHeight(View view, int height) {
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        layoutParams.height = height;
        view.setLayoutParams(layoutParams);
    }

    @Override
    public int getItemCount() {
        return models == null ? 0 : models.size();
    }

    public List<NotificationViewModel> getModels() {
        return models;
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe_reveal_layout;
    }

    public class NotificationsViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.swipe_reveal_layout)
        SwipeLayout swipeRevealLayout;
        @Bind(R.id.notification_layout)
        View notificationLayout;
        @Bind(R.id.action_layout)
        View actionLayout;
        @Bind(R.id.notification_event_type_text_view)
        TextView eventTypeTextView;
        @Bind(R.id.notification_send_date_txt_view)
        TextView sendDateTextView;
        @Bind(R.id.notification_body_text_view)
        TextView bodyTextView;
        @Bind(R.id.mark_as_read_layout)
        LinearLayout markAsReadLayout;

        int position;

        public NotificationsViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.mark_as_read_layout)
        public void markAsReadClicked() {
            notificationLayout.setBackgroundColor(ResourcesCompat.getColor(context.getResources(),
                    R.color.c92b, null));
            NotificationViewModel notificationViewModel = models.get(position);
            notificationViewModel.setRead(true);

            mItemManger.closeAllItems();

            notifyDataSetChanged();

            if (callback != null) {
                if (notificationViewModel.getNotificationEventEnum() != null) {
                    Analytics.sendEvent(Analytics.MOBILE_PRINT_BEAT_CATEGORY, Analytics.NOTIFICATION_MARKED_READ_ACTION, notificationViewModel.getNotificationEventEnum().getKey());
                }
                callback.markNotificationAsRead(notificationViewModel.getNotificationID(),
                        notificationViewModel.getUserEventID());
            }

            HPLogger.d(TAG, "mark as read clicked for notification with id: "
                    + notificationViewModel.getNotificationID());
        }

        @OnClick(R.id.delete_layout)
        public void deleteClicked() {
            NotificationViewModel notificationViewModel = models.get(position);
            models.remove(position);

            mItemManger.closeAllItems();

            notifyDataSetChanged();

            if (callback != null) {
                if (notificationViewModel.getNotificationEventEnum() != null) {
                    Analytics.sendEvent(Analytics.MOBILE_PRINT_BEAT_CATEGORY, Analytics.NOTIFICATION_DELETED_ACTION, notificationViewModel.getNotificationEventEnum().getKey());
                    AppseeSdk.getInstance(context).sendEvent(AppseeSdk.EVENT_DELETE_NOTIFICATION,
                            notificationViewModel.getNotificationEventEnum().getKey());
                }
                callback.deleteNotification(notificationViewModel.getNotificationID(),
                        notificationViewModel.getUserEventID());
            }

            dismissPushNotification(notificationViewModel);

            HPLogger.d(TAG, "delete clicked for notification with id: "
                    + notificationViewModel.getNotificationID());
        }

        @OnClick(R.id.notification_layout)
        public void onNotificationActionClicked() {
            NotificationViewModel notificationViewModel = models.get(position);

            dismissPushNotification(notificationViewModel);

            if (callback != null) {
                //mark as read
                callback.markNotificationAsRead(notificationViewModel.getNotificationID(),
                        notificationViewModel.getUserEventID());
                notificationViewModel.setRead(true);
                notifyItemChanged(position);
                //execute notification command

                if (notificationViewModel.getNotificationEventEnum() != null) {
                    AppseeSdk.getInstance(context).sendEvent(AppseeSdk.EVENT_CLICK_NOTIFICATION,
                            notificationViewModel.getNotificationEventEnum().getKey());
                    Analytics.sendEvent(Analytics.MOBILE_PRINT_BEAT_CATEGORY, Analytics.CLICK_NOTIFICATION_ACTION,
                            notificationViewModel.getNotificationEventEnum().getKey());

                }

                callback.onNotificationActionClicked(notificationViewModel.getNotificationEventEnum()
                                == SubscriptionEvent.NotificationEventEnum.BOX_NEW_FOLDER ?
                                getGoLink(notificationViewModel) : notificationViewModel.getEntity(),
                        notificationViewModel.getNotificationEventEnum().getKey());
            }

            HPLogger.d(TAG, "Clicked notification with id: "
                    + notificationViewModel.getNotificationID());
        }
    }

    private String getGoLink(NotificationViewModel notificationViewModel) {
        return notificationViewModel.getGoLink() == null ?
                notificationViewModel.getEntity() == null ? context.getString(R.string.unknown_value)
                        : String.format(BOX_URL,
                        PrintOSPreferences.getInstance(context).getServerUrl(context),
                        notificationViewModel.getEntity())
                : notificationViewModel.getGoLink();
    }

    private void dismissPushNotification(NotificationViewModel notificationViewModel) {
        if (!notificationViewModel.isRead()) {
            NotificationManager notificationManager =
                    (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.cancel(notificationViewModel.getNotificationID());
        }

    }

    public interface NotificationAdapterCallback {
        void deleteNotification(int notificationID, int UserID);

        void markNotificationAsRead(int notificationID, int userID);

        void onNotificationActionClicked(String extra, String notificationKey);
    }
}
