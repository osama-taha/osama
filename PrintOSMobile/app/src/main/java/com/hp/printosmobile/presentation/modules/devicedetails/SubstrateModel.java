package com.hp.printosmobile.presentation.modules.devicedetails;

import com.hp.printosmobile.R;

import java.io.Serializable;

/**
 * Created by Anwar Asbah on 8/17/16.
 */
public class SubstrateModel implements Serializable {

    public enum SubstrateUnit {
        METER("meter", R.string.meter_unit),
        METERS("meters", R.string.meter_unit),
        MILLIMETERS("millimeters", R.string.millimeter_unit),
        UNKNOWN("unknown", R.string.unknown_unit);

        private String unitName;
        private int localizedNameId;

        SubstrateUnit(String unitName, int localizedNameId) {
            this.unitName = unitName;
            this.localizedNameId = localizedNameId;
        }

        public String getUnitName() {
            return unitName;
        }

        public void setUnitName(String unitName) {
            this.unitName = unitName;
        }

        public int getLocalizedNameId() {
            return localizedNameId;
        }

        public void setLocalizedNameId(int localizedNameId) {
            this.localizedNameId = localizedNameId;
        }

        public static SubstrateUnit from(String state) {
            for (SubstrateUnit unit : SubstrateUnit.values()) {
                if (unit.getUnitName().toLowerCase().equals(state.toLowerCase()))
                    return unit;
            }
            return SubstrateUnit.UNKNOWN;
        }
    }

    private String mediaName;
    private float mediaWidth;
    private float mediaCounter;
    private String remainingLength;
    private SubstrateUnit unit;

    public String getMediaName() {
        return mediaName;
    }

    public void setMediaName(String mediaName) {
        this.mediaName = mediaName;
    }

    public float getMediaWidth() {
        return mediaWidth;
    }

    public void setMediaWidth(float mediaWidth) {
        this.mediaWidth = mediaWidth;
    }

    public float getMediaCounter() {
        return mediaCounter;
    }

    public void setMediaCounter(float mediaCounter) {
        this.mediaCounter = mediaCounter;
    }

    public SubstrateUnit getUnit() {
        return unit;
    }

    public void setUnit(SubstrateUnit unit) {
        this.unit = unit;
    }

    public String getRemainingLength() {
        return remainingLength;
    }

    public void setRemainingLength(String remainingLength) {
        this.remainingLength = remainingLength;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("SubstrateModel{");
        sb.append("mediaName='").append(mediaName).append('\'');
        sb.append(", mediaWidth=").append(mediaWidth);
        sb.append(", mediaCounter=").append(mediaCounter);
        sb.append(", remainingLength=").append(remainingLength);
        sb.append(", unit='").append(unit).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
