package com.hp.printosmobile.data.remote.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by Anwar Asbah 3/9/2017
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ServiceCallData {

    @JsonProperty("total")
    private int total;
    @JsonProperty("count")
    private int count;
    @JsonProperty("members")
    List<CallsData> callsDataList;

    @JsonProperty("total")
    public int getTotal() {
        return total;
    }

    @JsonProperty("total")
    public void setTotal(int total) {
        this.total = total;
    }

    @JsonProperty("count")
    public int getCount() {
        return count;
    }

    @JsonProperty("count")
    public void setCount(int count) {
        this.count = count;
    }

    @JsonProperty("members")
    public List<CallsData> getCallsDataList() {
        return callsDataList;
    }

    @JsonProperty("members")
    public void setCallsDataList(List<CallsData> callsDataList) {
        this.callsDataList = callsDataList;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ServiceCallData{");
        sb.append("total=").append(total);
        sb.append(", count=").append(count);
        sb.append(", callsDataList=").append(callsDataList);
        sb.append('}');
        return sb.toString();
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class CallsData {

        @JsonProperty("callerName")
        private String callerName;
        @JsonProperty("callId")
        private String callId;
        @JsonProperty("category")
        private String category;
        @JsonProperty("closeDate")
        private String closeDate;
        @JsonProperty("description")
        private String description;
        @JsonProperty("domain")
        private String domain;
        @JsonProperty("endUserId")
        private String endUserId;
        @JsonProperty("issueType")
        private String issueType;
        @JsonProperty("openDate")
        private String openDate;
        @JsonProperty("phone")
        private String phone;
        @JsonProperty("problem")
        private String problem;
        @JsonProperty("serialNumber")
        private String serialNumber;
        @JsonProperty("status")
        private String status;

        @JsonProperty("callerName")
        public String getCallerName() {
            return callerName;
        }

        @JsonProperty("callerName")
        public void setCallerName(String callerName) {
            this.callerName = callerName;
        }

        @JsonProperty("callId")
        public String getCallId() {
            return callId;
        }

        @JsonProperty("callId")
        public void setCallId(String callId) {
            this.callId = callId;
        }

        @JsonProperty("category")
        public String getCategory() {
            return category;
        }

        @JsonProperty("category")
        public void setCategory(String category) {
            this.category = category;
        }

        @JsonProperty("closeDate")
        public String getCloseDate() {
            return closeDate;
        }

        @JsonProperty("closeDate")
        public void setCloseDate(String closeDate) {
            this.closeDate = closeDate;
        }

        @JsonProperty("description")
        public String getDescription() {
            return description;
        }

        @JsonProperty("description")
        public void setDescription(String description) {
            this.description = description;
        }

        @JsonProperty("domain")
        public String getDomain() {
            return domain;
        }

        @JsonProperty("domain")
        public void setDomain(String domain) {
            this.domain = domain;
        }

        @JsonProperty("endUserId")
        public String getEndUserId() {
            return endUserId;
        }

        @JsonProperty("endUserId")
        public void setEndUserId(String endUserId) {
            this.endUserId = endUserId;
        }

        @JsonProperty("issueType")
        public String getIssueType() {
            return issueType;
        }

        @JsonProperty("issueType")
        public void setIssueType(String issueType) {
            this.issueType = issueType;
        }

        @JsonProperty("openDate")
        public String getOpenDate() {
            return openDate;
        }

        @JsonProperty("openDate")
        public void setOpenDate(String openDate) {
            this.openDate = openDate;
        }

        @JsonProperty("phone")
        public String getPhone() {
            return phone;
        }

        @JsonProperty("phone")
        public void setPhone(String phone) {
            this.phone = phone;
        }

        @JsonProperty("problem")
        public String getProblem() {
            return problem;
        }

        @JsonProperty("problem")
        public void setProblem(String problem) {
            this.problem = problem;
        }

        @JsonProperty("serialNumber")
        public String getSerialNumber() {
            return serialNumber;
        }

        @JsonProperty("serialNumber")
        public void setSerialNumber(String serialNumber) {
            this.serialNumber = serialNumber;
        }

        @JsonProperty("status")
        public String getStatus() {
            return status;
        }

        @JsonProperty("status")
        public void setStatus(String status) {
            this.status = status;
        }

        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder("CallsData{");
            sb.append("callerName='").append(callerName).append('\'');
            sb.append(", callId='").append(callId).append('\'');
            sb.append(", category='").append(category).append('\'');
            sb.append(", closeDate='").append(closeDate).append('\'');
            sb.append(", description='").append(description).append('\'');
            sb.append(", domain='").append(domain).append('\'');
            sb.append(", endUserId='").append(endUserId).append('\'');
            sb.append(", issueType='").append(issueType).append('\'');
            sb.append(", openDate='").append(openDate).append('\'');
            sb.append(", phone='").append(phone).append('\'');
            sb.append(", problem='").append(problem).append('\'');
            sb.append(", serialNumber='").append(serialNumber).append('\'');
            sb.append(", status='").append(status).append('\'');
            sb.append('}');
            return sb.toString();
        }
    }
}