package com.hp.printosmobile.presentation.modules.devicedetails;

import com.hp.printosmobile.R;

import java.io.Serializable;
import java.util.Comparator;

/**
 * Created by Anwar Asbah on 8/16/16.
 */
public class InkModel implements Comparable<InkModel>, Serializable {

    public static Comparator<InkModel> COMPARATOR = new Comparator<InkModel>() {
        @Override
        public int compare(InkModel lhs, InkModel rhs) {
            if (lhs == null && rhs == null) return 0;
            if (lhs == null && rhs != null) return 1;
            if (lhs != null && rhs == null) return -1;
            return lhs.compareTo(rhs);
        }
    };

    public enum InkEnum {

        M("M", R.string.m_color_code, R.string.m_color, R.color.m_color, 0),
        LM("LM", R.string.lm_color_code, R.string.lm_color, R.color.lm_color, 1),
        LC("LC", R.string.lc_color_code, R.string.lc_color, R.color.lc_color, 2),
        C("C", R.string.c_color_code, R.string.c_color, R.color.c_color, 3),
        OP("OP", R.string.op_color_code, R.string.op_color, R.color.op_color, 4),
        Y("Y", R.string.y_color_code, R.string.y_color, R.color.y_color, 5),
        K("K", R.string.k_color_code, R.string.k_color, R.color.k_color, 6),
        UNKNOWN("UNKNOWN", -1, R.string.unknown_color, R.color.unknown_color, 7);

        private String name;
        private int localizedColorCodeResId;
        private int colorID;
        private int localizedNameId;
        private int sortOrder;

        InkEnum(String colorCode, int localizedColorCodeResId, int localizedNameId, int colorID, int sortOrder) {
            this.name = colorCode;
            this.localizedColorCodeResId = localizedColorCodeResId;
            this.localizedNameId = localizedNameId;
            this.colorID = colorID;
            this.sortOrder = sortOrder;
        }

        public String getName() {
            return name;
        }

        public int getColor() {
            return colorID;
        }

        public int getSortOrder() {
            return sortOrder;
        }

        public int getLocalizedNameId() {
            return localizedNameId;
        }

        public int getLocalizedColorCodeResId() {
            return localizedColorCodeResId;
        }

        public static InkEnum from(String state) {
            for (InkEnum ink : InkEnum.values()) {
                if (ink.getName().toLowerCase().equals(state.toLowerCase()))
                    return ink;
            }
            return InkEnum.UNKNOWN;
        }
    }

    public enum InkState {

        LOW("Low", R.string.ink_state_low),
        OK("OK", R.string.ink_state_ok),
        EXPIRED("Expired", R.string.ink_state_expired),
        UNKNOWN("UNKNOWN", R.string.ink_state_unknown);

        private String name;
        private int localizedNameId;

        InkState(String name, int localizedNameId) {
            this.name = name;
            this.localizedNameId = localizedNameId;
        }

        public String getName() {
            return name;
        }

        public int getLocalizedNameId() {
            return localizedNameId;
        }

        public static InkState from(String state) {
            if (state == null) {
                return InkState.UNKNOWN;
            }
            for (InkState ink : InkState.values()) {
                if (ink.getName().toLowerCase().equals(state.toLowerCase()))
                    return ink;
            }
            return InkState.UNKNOWN;
        }
    }

    private InkEnum inkEnum;
    private int level;
    private int capacity;
    private InkState inkState;
    private String expiryDate;

    public InkModel() {
        level = 0;
        capacity = 1;
        inkEnum = InkEnum.UNKNOWN;
    }

    public InkEnum getInkEnum() {
        return inkEnum;
    }

    public void setInkEnum(InkEnum inkEnum) {
        this.inkEnum = inkEnum;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public InkState getInkState() {
        return inkState;
    }

    public void setInkState(InkState inkState) {
        this.inkState = inkState;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("InkModel{");
        sb.append("inkEnum=").append(inkEnum);
        sb.append(", level=").append(level);
        sb.append(", capacity=").append(capacity);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public int compareTo(InkModel another) {
        if (another == null) return 1;
        return inkEnum.getSortOrder() - another.getInkEnum().getSortOrder();
    }
}
