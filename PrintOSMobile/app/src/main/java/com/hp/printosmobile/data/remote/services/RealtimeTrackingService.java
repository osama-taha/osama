package com.hp.printosmobile.data.remote.services;

import com.hp.printosmobile.data.remote.ApiConstants;
import com.hp.printosmobile.data.remote.models.ActiveShiftsData;
import com.hp.printosmobile.data.remote.models.ActualVsTargetData;
import com.hp.printosmobile.data.remote.models.PrintVolumeData;
import com.hp.printosmobile.data.remote.models.RealtimeActualVsTargetDataV2;
import com.hp.printosmobile.data.remote.models.RealtimeManyDataV2;
import com.hp.printosmobile.data.remote.models.RealtimeTargetManyDataV2;
import com.hp.printosmobile.data.remote.models.TargetData;

import java.util.List;

import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Retrofit REST interface definition for Realtime tracking Endpoints
 *
 * @author Osama Taha
 */
public interface RealtimeTrackingService {

    @GET(ApiConstants.ACTIVE_SHIFTS)
    Observable<Response<ActiveShiftsData>> getActiveShifts(@Query("bu") String businessUnit, @Query("devices") List<String> devices);

    @GET(ApiConstants.REALTIME_TRACKING_MANY)
    Observable<Response<List<PrintVolumeData>>> getPrintVolumeData(@Query("bu") String businessUnit, @Query("devices") List<String> devices, @Query("item") String item, @Query("unit") String unit, @Query("isShiftSupport") boolean isShiftSupport);

    @GET(ApiConstants.REALTIME_TRACKING_MANY_V2)
    Observable<Response<RealtimeManyDataV2>> getPrintVolumeDataV2(@Query("bu") String businessUnit, @Query("devices") List<String> devices, @Query("item") String item, @Query("unit") String unit, @Query("isShiftSupport") boolean isShiftSupport, @Query("unitSystem") String unitSystem);

    @GET(ApiConstants.REALTIME_TRACKING_TARGET)
    Observable<Response<List<TargetData>>> getTargetData(@Query("bu") String businessUnit, @Query("devices") List<String> devices, @Query("item") String item, @Query("unit") String unit, @Query("isShiftSupport") boolean isShiftSupport);

    @GET(ApiConstants.REALTIME_TRACKING_TARGET_V2)
    Observable<Response<RealtimeTargetManyDataV2>> getTargetDataV2(@Query("bu") String businessUnit, @Query("devices") List<String> devices, @Query("item") String item, @Query("unit") String unit, @Query("isShiftSupport") boolean isShiftSupport, @Query("unitSystem") String unitSystem);

    @GET(ApiConstants.REALTIME_TRACKING_ACTUAL_VS_TARGET)
    Observable<Response<ActualVsTargetData>> getActualVsTargetData(@Query("bu") String businessUnit, @Query("devices") List<String> devices, @Query("item") String item, @Query("unit") String unit, @Query("days") String days, @Query("isShiftSupport") boolean isShiftSupport);

    @GET(ApiConstants.REALTIME_TRACKING_ACTUAL_VS_TARGET_V2)
    Observable<Response<RealtimeActualVsTargetDataV2>> getActualVsTargetDataV2(@Query("bu") String businessUnit, @Query("devices") List<String> devices, @Query("item") String item, @Query("unit") String unit, @Query("days") String days, @Query("isShiftSupport") boolean isShiftSupport, @Query("unitSystem") String unitSystem);

}
