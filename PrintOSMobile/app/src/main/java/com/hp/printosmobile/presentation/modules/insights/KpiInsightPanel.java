package com.hp.printosmobile.presentation.modules.insights;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.util.AttributeSet;
import android.view.View;

import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.modules.home.Panel;
import com.hp.printosmobile.utils.FileUtils;
import com.hp.printosmobilelib.ui.widgets.panel.PanelView;

import java.util.Calendar;
import java.util.Date;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Anwar Asbah on 6/15/2017.
 */
public class KpiInsightPanel extends PanelView<InsightsViewModel> implements InsightsChartView.InsightsChartViewCallback {

    public final static String TAG = KpiInsightPanel.class.getSimpleName();
    private static final String SCREEN_SHOT_FILE_NAME = "PrintOS";
    private static final int TITLE_MAX_NUMBER_OF_LINES = 3;

    @Bind(R.id.large_loading_view)
    View largeLoadingView;
    @Bind(R.id.insights_chart)
    InsightsChartView insightsChartView;

    private KpiInsightPanelCallback callback;

    public KpiInsightPanel(Context context) {
        super(context);
        bindViews();
    }

    public KpiInsightPanel(Context context, AttributeSet attrs) {
        super(context, attrs);
        bindViews();
    }

    public KpiInsightPanel(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        bindViews();
    }

    private void bindViews() {
        ButterKnife.bind(this, getView());
        setTitleNumberOfLines(TITLE_MAX_NUMBER_OF_LINES);
        insightsChartView.attachCallback(this);
    }

    public void addCallback(KpiInsightPanelCallback callback) {
        this.callback = callback;
    }

    @Override
    public Spannable getTitleSpannable() {
        if (getViewModel() == null) {
            return new SpannableStringBuilder(getContext().getString(R.string.insights_tab_name));
        }

        return new SpannableStringBuilder(InsightsUtils.getPanelTitle(getContext(), getViewModel()));
    }

    @Override
    public int getContentView() {
        return R.layout.kpi_insight_panel;
    }

    @Override
    public void updateViewModel(InsightsViewModel viewModel) {
        hideLoading();
        setViewModel(viewModel);

        if (showEmptyCard()) {
            return;
        }

        insightsChartView.setViewModel(viewModel);
        setTitle(getTitleSpannable());
    }

    @Override
    protected boolean isValidViewModel() {
        return !(getViewModel() == null ||
                getViewModel().getInsightModels() == null
                || getViewModel().getInsightModels().size() == 0);
    }

    protected String getEmptyCardText() {
        return getContext().getString(R.string.no_information_to_display);
    }

    @Override
    public void onRefresh() {
        super.onRefresh();
        if (insightsChartView != null) {
            insightsChartView.onRefresh();
        }
    }

    @Override
    public void onDateSelected(Date from, Date to) {
        if (callback != null && getViewModel() != null) {
            callback.onDateSelected(from, to, getViewModel().getInsightKpiEnum());
        }
    }

    @Override
    public void onShareClicked() {
        if (callback != null) {
            callback.onShareButtonClicked(getViewModel().getInsightKpiEnum() == InsightsViewModel.InsightKpiEnum.JAM ?
                    Panel.JAM_CHART : Panel.FAILURE_CHART);
        }
    }

    public void sharePanel() {
        if (callback != null) {

            lockShareButton(true);

            Bitmap weekPanelBitmap = FileUtils.getScreenShot(this);
            Uri storageUri = FileUtils.store(weekPanelBitmap, SCREEN_SHOT_FILE_NAME);

            callback.onScreenshotCreated(Panel.PERFORMANCE, storageUri);
        }
    }

    public Date getDateFrom() {
        if (insightsChartView != null) {
            return insightsChartView.getDateFrom();
        }
        return Calendar.getInstance().getTime();
    }

    public Date getDateTo() {
        if (insightsChartView != null) {
            return insightsChartView.getDateTo();
        }
        return Calendar.getInstance().getTime();
    }

    public void resetDateFilter() {
        if (insightsChartView != null) {
            insightsChartView.resetDateFilter();
        }
    }

    @Override
    public void hideLoading() {
        super.hideLoading();
        largeLoadingView.setVisibility(GONE);
    }

    @Override
    public void showLoading() {
        super.showLoading();
        largeLoadingView.setVisibility(VISIBLE);
    }

    public interface KpiInsightPanelCallback {
        void onDateSelected(Date from, Date to, InsightsViewModel.InsightKpiEnum kpiEnum);

        void onShareButtonClicked(Panel panel);

        void onScreenshotCreated(Panel panel, Uri uri);
    }
}
