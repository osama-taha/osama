package com.hp.printosmobile.presentation.modules.login;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.AppseeSdk;
import com.hp.printosmobile.Constants;
import com.hp.printosmobile.IntercomSdk;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.presentation.BaseActivity;
import com.hp.printosmobile.presentation.Navigator;
import com.hp.printosmobile.presentation.indigowidget.PrintOSAppWidgetProvider;
import com.hp.printosmobile.presentation.modules.eula.EULAViewModel;
import com.hp.printosmobile.presentation.modules.eula.EulaActivity;
import com.hp.printosmobile.presentation.modules.hiddensettings.HiddenSettingsActivity;
import com.hp.printosmobile.presentation.modules.resetpassword.ResetPasswordActivity;
import com.hp.printosmobile.utils.AppUtils;
import com.hp.printosmobile.utils.HPUIUtils;
import com.hp.printosmobilelib.core.communications.remote.APIException;
import com.hp.printosmobilelib.core.communications.remote.SessionManager;
import com.hp.printosmobilelib.core.communications.remote.models.AAAError;
import com.hp.printosmobilelib.core.communications.remote.models.UserCredentials;
import com.hp.printosmobilelib.core.communications.remote.models.UserData;
import com.hp.printosmobilelib.core.logging.HPLogQueue;
import com.hp.printosmobilelib.core.logging.HPLogger;

import butterknife.Bind;
import butterknife.OnClick;


/**
 * Login Screen activity
 *
 * @author Osama Taha
 */
public class LoginActivity extends BaseActivity implements LoginView {

    public static final String TAG = LoginActivity.class.getSimpleName();
    private static final String SETTINGS = "settings";

    @Bind(R.id.login_layout)
    View loginLayout;
    @Bind(R.id.edit_text_username)
    EditText usernameEditText;
    @Bind(R.id.edit_text_password)
    EditText passwordEditText;
    @Bind(R.id.button_login)
    TextView loginButton;
    @Bind(R.id.progress_loading)
    ProgressBar loadingView;
    @Bind(R.id.server_info_layout)
    View serverInfoLayout;
    @Bind(R.id.server_name_text)
    TextView serverNameTextView;
    @Bind(R.id.register_to_printos_button)
    TextView registerButton;

    private LoginPresenter presenter;
    private EULAViewModel eulaModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        presenter = new LoginPresenter();
        presenter.attachView(this);

        IntercomSdk.getInstance(this).logout();
        IntercomSdk.getInstance(this).logEvent(IntercomSdk.PBM_VIEW_LOGIN_EVENT);

        init();

        loginLayout.getViewTreeObserver()
                .addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {

                        loginLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);

                        requestNeededPermissions();
                    }
                });

        PrintOSPreferences.getInstance(this).setVersionCheckingTime(0);
    }

    private void init() {

        String serverUrl = PrintOSPreferences.getInstance(this).getServerUrl(this);
        String dailyUrl = getString(R.string.daily_server);
        String pressQAUrl = getString(R.string.pressqa_server);

        UserCredentials userCredentials = SessionManager.getInstance(this).getTempCredentials();
        SessionManager.getInstance(this).clearTempCredentials();

        if (TextUtils.isEmpty(userCredentials.getPassword()) ||
                TextUtils.isEmpty(userCredentials.getLogin())) {
            if (serverUrl != null) {
                if (serverUrl.equals(dailyUrl)) {
                    usernameEditText.setText(getString(R.string.daily_user));
                    passwordEditText.setText(getString(R.string.daily_password));
                } else if (serverUrl.equals(pressQAUrl)) {
                    usernameEditText.setText(getString(R.string.pressqa_user));
                    passwordEditText.setText(getString(R.string.pressqa_password));
                }
            }
        } else {
            usernameEditText.setText(userCredentials.getLogin());
            passwordEditText.setText(userCredentials.getPassword());
            onLoginButtonClicked();
        }

        setServerInfo();

        loginLayout.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    hideSoftKeyboard();
                }
            }
        });
        passwordEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    onLoginButtonClicked();
                    return true;
                }
                return false;
            }
        });
    }

    private void setServerInfo() {

        PrintOSPreferences userPreferences = PrintOSPreferences.getInstance(this);

        if (userPreferences.showServerName()) {
            String serverName = userPreferences.getServerName();
            serverInfoLayout.setVisibility(View.VISIBLE);
            serverNameTextView.setText(serverName);
        } else {
            serverInfoLayout.setVisibility(View.GONE);
        }

    }

    private void hideSoftKeyboard() {
        HPUIUtils.hideSoftKeyboard(this, usernameEditText, passwordEditText);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_login;
    }

    @Override
    public void showLoading() {
        loadingView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        loadingView.setVisibility(View.GONE);
    }

    @Override
    public void setLoginInButtonEnabled(boolean enabled) {
        loginButton.setEnabled(enabled);
    }

    @Override
    public void onLoginSuccessful(UserData userData) {

        PrintOSPreferences.getInstance(this).saveUserInfo(userData);

        PrintOSPreferences.getInstance(this).setNotificationEnabled(PrintOSPreferences.NOTIFICATION_MODE_ON);
        PrintOSPreferences.getInstance(this).setLoggedIn(true);

        AppseeSdk.getInstance(this).setUserId();
        AppseeSdk.getInstance(this).sendEvent(AppseeSdk.EVENT_LOGIN_SUCCESS);

        if (userData != null && userData.getContext() != null) {
            PrintOSPreferences.getInstance(this).saveOrganization(userData.getContext());
            Analytics.sendEvent(Analytics.MOBILE_PRINT_BEAT_CATEGORY, Analytics.LOGIN_EVENT_LOGIN_ACTION,
                    (userData.getUser() != null ? userData.getUser().getUserId() : "") + "|" + userData.getContext().getId());
        } else {
            Analytics.sendEvent(Analytics.MOBILE_PRINT_BEAT_CATEGORY, Analytics.LOGIN_EVENT_LOGIN_ACTION);
        }

        HPLogger.i(TAG, String.format("user logged in successfully. locale used: %s",
                PrintOSPreferences.getInstance(this).getLanguageCode()));

        IntercomSdk.getInstance(getApplicationContext()).login();

        startRegistrationService(Constants.IntentExtras.NOTIFICATION_REGISTER_INTENT_ACTION);

        PrintOSAppWidgetProvider.updateAllWidgets(this);

        HPLogQueue.getInstance(this).flushPreLoginLogs();

        Navigator.openMainActivity(this, null);

        startVersionCheckingService();

        finish();

    }

    @Override
    public void showGeneralLoginError(APIException error) {

        AppseeSdk.getInstance(this).sendEvent(AppseeSdk.EVENT_LOGIN_FAIL);

        AppUtils.sendLoginErrorEvent(error, Analytics.LOGIN_FAILED_ACTION);

        if (error.getAAAError() != null) {

            if (error.getKind() == APIException.Kind.UNAUTHORIZED) {

                AAAError.SmsError smsError = error.getAAAError().getSmsError();
                AAAError.ErrorSubCode errorSubCode = smsError.getSubCode();

                if (errorSubCode == AAAError.ErrorSubCode.INVALID_USER_NAME_PASSWORD) {
                    HPUIUtils.displaySnackbar(this, loginLayout, getString(R.string.error_login_unauthorized), Snackbar.LENGTH_SHORT);
                } else if (errorSubCode == AAAError.ErrorSubCode.ACCOUNT_LOCKED) {
                    HPUIUtils.displaySnackbar(this, loginLayout, getString(R.string.error_login_account_is_locked), Snackbar.LENGTH_SHORT);
                } else if (errorSubCode == AAAError.ErrorSubCode.EULA_NOT_ACCEPTED) {
                    HPUIUtils.displaySnackbar(this, loginLayout, getString(R.string.error_login_eula_not_accepted), Snackbar.LENGTH_SHORT);
                    openEULAActivity();
                } else if (errorSubCode == AAAError.ErrorSubCode.PASSWORD_EXPIRED) {
                    HPUIUtils.displaySnackbar(this, loginLayout, getString(R.string.error_login_password_has_expired), Snackbar.LENGTH_SHORT);
                } else {
                    HPUIUtils.displaySnackbar(this, loginLayout, smsError.getMessage(), Snackbar.LENGTH_SHORT);
                }
            }

        } else {

            HPUIUtils.displayToastException(this, error, LoginActivity.TAG, false);
        }
    }

    @Override
    public void showErrorMsg(String msg) {
        HPUIUtils.displaySnackbar(this, loginLayout, msg, Snackbar.LENGTH_SHORT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        HPLogger.d(TAG, "Activity result " + resultCode + " and code " + requestCode);

        if (requestCode == Constants.IntentExtras.EULA_ACTIVITY_REQUEST_CODE
                && resultCode == Constants.IntentExtras.EULA_ACTIVITY_RESULT_CODE) {

            boolean didAcceptEula = data.getBooleanExtra(Constants.IntentExtras.EULA_ACTIVITY_ACCEPTED_RESULT, false);

            if (didAcceptEula) {

                this.eulaModel = (EULAViewModel) data.getSerializableExtra(Constants.IntentExtras.EULA_ACTIVITY_RESULT_OBJECT);
                onLoginButtonClicked();

            } else {
                HPUIUtils.displaySnackbar(this, loginLayout, getString(R.string.error_login_eula_not_accepted), Snackbar.LENGTH_SHORT);
            }
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        setServerInfo();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

    @OnClick(R.id.button_login)
    public void onLoginButtonClicked() {
        HPUIUtils.hideSoftKeyboard(this, usernameEditText, passwordEditText);

        String userName = usernameEditText.getText().toString().trim();
        String password = passwordEditText.getText().toString().trim();

        if (!userName.isEmpty() && !password.isEmpty()) {

            AppseeSdk.getInstance(this).sendEvent(AppseeSdk.EVENT_CLICK_LOGIN);

            if (!hiddenSettingShouldAppear(userName)) {
                presenter.login(userName, password, eulaModel == null ? null : eulaModel.getEulaVersion());
            }
        }
    }

    @OnClick(R.id.register_to_printos_button)
    public void onRegisterButtonClicked() {
        AppUtils.startApplication(this, Uri.parse(Constants.REGISTRATION_LINK));
    }

    public boolean hiddenSettingShouldAppear(String userName) {
        if (userName.equalsIgnoreCase(SETTINGS)) {
            LoginActivity.this.finish();
            Intent intent = new Intent(this, HiddenSettingsActivity.class);
            startActivity(intent);
            return true;
        }
        return false;
    }

    @OnClick(R.id.forgot_password_button)
    public void onForgotPasswordClicked() {
        Analytics.sendEvent(Analytics.MOBILE_PRINT_BEAT_CATEGORY, Analytics.FORGOT_PASSWORD_CLICKED_ACTION);

        Intent intent = new Intent(this, ResetPasswordActivity.class);
        startActivity(intent);
    }

    private void openEULAActivity() {

        Intent intent = new Intent(this, EulaActivity.class);
        intent.putExtra(Constants.IntentExtras.EULA_ACTIVITY_TO_ACCEPT_TERMS, true);
        startActivityForResult(intent, Constants.IntentExtras.EULA_ACTIVITY_REQUEST_CODE);

    }

    @Override
    public void onError(APIException exception, String tag) {
        //TODO
    }
}
