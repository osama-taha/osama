package com.hp.printosmobile.presentation.indigowidget;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.widget.RemoteViewsService;

import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobilelib.core.logging.HPLogger;

/**
 * Created by Osama Taha on 2/10/17.
 */
@SuppressLint("NewApi")
public abstract class PrintOSWidgetService extends RemoteViewsService implements Runnable {

    private static final String TAG = PrintOSWidgetService.class.getSimpleName();

    private static final long WIDGET_UPDATE_PERIOD = 10 * 60 * 1000;

    @Override
    public RemoteViewsFactory onGetViewFactory(Intent intent) {
        return new ListRemoteViewsFactory(this.getApplicationContext(), intent, getWidgetType());
    }

    @Override
    public void onCreate() {
        super.onCreate();

        HPLogger.d(TAG, "Create service for widget " + getWidgetType().name());

        new Thread(this).start();
    }

    @SuppressLint("SimpleDateFormat")
    @Override
    public void run() {

        final long lastRefreshTime = PrintOSPreferences.getInstance(this).getWidgetLastRefreshTime(getWidgetType());
        final long now = System.currentTimeMillis();

        HPLogger.d(TAG, "check to update widget " + getWidgetType().name());

        if (now - lastRefreshTime >= WIDGET_UPDATE_PERIOD) {

            HPLogger.d(TAG, "refresh widget " + getWidgetType().name());

            PrintOSPreferences.getInstance(this).saveWidgetLastRefreshTime(getWidgetType(), now);

            Intent refreshNowIntent = new Intent(instance(), getWidgetType().getProviderClass());
            refreshNowIntent.setAction(PrintOSAppWidgetProvider.ACTION_REFRESH_AUTO);
            sendBroadcast(refreshNowIntent);
        }

        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(instance());
        int[] appWidgetIds = appWidgetManager.getAppWidgetIds(new ComponentName(instance(),
                getWidgetType().getProviderClass()));
        if (appWidgetIds.length > 0) {
            setAlarm(instance(), WIDGET_UPDATE_PERIOD);
        } else {
            setAlarm(instance(), -1);
        }


        stopSelf();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public void setAlarm(Context context, long updateRate) {

        Intent autoRefreshIntent = new Intent(this, getWidgetType().getProviderClass());
        autoRefreshIntent.setAction(PrintOSAppWidgetProvider.ACTION_REFRESH_AUTO);
        PendingIntent pending = PendingIntent.getBroadcast(instance(), getWidgetType().ordinal(), autoRefreshIntent, 0);

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        if (updateRate >= 0) {
            HPLogger.d(TAG, "set alarm for widget " + getWidgetType().name());
            alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME, SystemClock.elapsedRealtime(), updateRate, pending);
        } else {
            HPLogger.d(TAG, "cancel alarm for widget " + getWidgetType().name());
            alarmManager.cancel(pending);
        }

    }


    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

    @Override
    public void onRebind(Intent intent) {
        super.onRebind(intent);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }


    abstract WidgetType getWidgetType();

    abstract PrintOSWidgetService instance();

}