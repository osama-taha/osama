package com.hp.printosmobile;

import android.content.Context;
import android.text.TextUtils;

import com.appsee.Appsee;
import com.appsee.AppseeListener;
import com.appsee.AppseeScreenDetectedInfo;
import com.appsee.AppseeSessionEndedInfo;
import com.appsee.AppseeSessionEndingInfo;
import com.appsee.AppseeSessionStartedInfo;
import com.appsee.AppseeSessionStartingInfo;
import com.crashlytics.android.Crashlytics;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.presentation.modules.shared.UserViewModel;
import com.hp.printosmobilelib.core.logging.HPLogger;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by OsamaTaha on 3/7/17.
 */

public class AppseeSdk implements AppseeListener {

    private static final String TAG = AppseeSdk.class.getSimpleName();

    public static final String SCREEN_LANGUAGES = "Languages";
    public static final String SCREEN_UNIT_SYSTEM = "Unit System";
    public static final String SCREEN_NPS = "NPS";
    public static final String SCREEN_BEAT_COINS = "Beat coins";
    public static final String SCREEN_NOTIFICATION_SETTINGS = "NotificationSettings";
    public static final String SCREEN_SETTINGS = "Settings";
    public static final String SCREEN_NOTIFICATIONS_LIST = "NotificationsList";
    public static final String SCREEN_HOME = "Home";
    public static final String SCREEN_DEVICES = "Devices";
    public static final String SCREEN_INSIGHTS = "Insights";
    public static final String SCREEN_INDIGO_DEVICE_DETAILS = "IndigoDeviceDetails";
    public static final String SCREEN_LATEX_DEVICE_DETAILS = "LatexDeviceDetails";
    public static final String SCREEN_REPORTS = "Reports";
    public static final String JOBS = "Jobs";
    public static final String SCREEN_CHANGE_ORGANIZATION = "ChangeOrganization";
    public static final String SCREEN_SERVICE_CALLS = "ServiceCalls";
    public static final String SCREEN_NAME_PERSONAL_ADVISOR_POPUP = "PersonalAdvisorPopup";
    public static final String SCREEN_TIME_ZONE_DIALOG = "TimeZonePopup";

    public static final String EVENT_REGISTER_NOTIFICATION = "REGISTER_NOTIFICATION";
    public static final String EVENT_UNREGISTER_NOTIFICATION = "UNREGISTER_NOTIFICATION";
    public static final String EVENT_OPEN_MENU = "OPEN_MENU";
    public static final String EVENT_CLOSE_MENU = "CLOSE_MENU";
    public static final String EVENT_SEND_QUESTION = "CLICK_SEND_A_QUESTION";
    public static final String EVENT_SEND_PROBLEM = "CLICK_REPORT_A_PROBLEM";

    public static final String EVENT_CLICK_LOGIN = "CLICK LOGIN";
    public static final String EVENT_LOGIN_FAIL = "LOGIN FAIL";
    public static final String EVENT_LOGIN_SUCCESS = "LOGIN SUCCESS";

    public static final String EVENT_RATE_RATE_DIALOG_SHOWN = "RATE_DIALOG_SHOWN";
    public static final String EVENT_RATE_DIALOG_NO_THANKS_CLICKED = "RATE_DIALOG_NO_THANKS_CLICKED";
    public static final String EVENT_RATE_DIALOG_REMIND_ME_LATER_CLICKED = "RATE_DIALOG_REMIND_ME_LATER_CLICKED";
    public static final String EVENT_RATE_DIALOG_SEND_FEEDBACK_CLICKED = "RATE_DIALOG_SEND_FEEDBACK_CLICKED";

    public static final String EVENT_VIEW_BUSINESS_UNIT = "VIEW_BU";

    public final static String EVENT_CLICK_NOTIFICATION = "EVENT_CLICK_NOTIFICATION";
    public final static String EVENT_DELETE_NOTIFICATION = "EVENT_DELETE_NOTIFICATION";

    public final static String EVENT_OPEN_APP_FROM_ICON = "OPEN_APP_FROM_ICON";
    public final static String EVENT_OPEN_FROM_NOTIFICATION = "OPEN_APP_FROM_NOTIFICATION";
    public final static String EVENT_OPEN_FROM_WIDGET = "OPEN_APP_FROM_WIDGET";
    public final static String EVENT_SHOW_NOTIFICATION = "SHOW_NOTIFICATION";

    private static final List<String> IGNORED_COMPONENTS = Arrays.asList("Dialog");

    public static final String EVENT_SHOW_NPS = "NPS_POPUP_SHOWN";
    public static final String EVENT_SUBMIT_NPS = "SUBMIT_NPS";
    public static final String EVENT_SHOW_BEAT_COINS = "BEAT_COINS_POPUP_SHOWN";
    public static final String EVENT_COLLECT_COINS_CLICKED = "COLLECT_COINS_CLICKED";

    public static final String EVENT_TIME_ZONE_DIALOG_SHOWN = "TIME_ZONE_DIALOG_SHOWN";
    public static final String EVENT_TIME_ZONE_NO_CLICKED = "TIME_ZONE_NO_CLICKED";
    public static final String EVENT_TIME_ZONE_YES_CLICKED = "TIME_ZONE_YES_CLICKED";

    //Others
    private static final String EVENT_VALUE_PARAMETER = "value";
    public static final String DENY_LOCATION_SERVICES = "DENY_LOCATION_SERVICES";
    public static final String ACCEPT_LOCATION_SERVICES = "ACCEPT_LOCATION_SERVICES";
    public static final String OPEN_APP_SETTINGS = "CLICK_GO_TO_SETTINGS";
    public static final String EVENT_KPI_EXPLANATION_SHOWN = "EVENT_KPI_EXPLANATION_SHOWN";

    private static AppseeSdk sharedInstance;
    private final Context context;

    public static AppseeSdk getInstance(Context context) {
        if (sharedInstance == null) {

            HPLogger.d(TAG, "Init Appsee sdk.");

            sharedInstance = new AppseeSdk(context);
            Appsee.addAppseeListener(sharedInstance);
        }
        return sharedInstance;
    }

    private AppseeSdk(Context context) {
        this.context = context;
    }

    public void init() {
        Appsee.start(BuildConfig.DEBUG ? context.getString(R.string.appsee_test_account_api_key)
                : context.getString(R.string.appsee_production_account_api_key));
        setUserId();
    }

    public void setUserId() {

        UserViewModel userViewModel = PrintOSPreferences.getInstance(context).getUserInfo();
        if (!TextUtils.isEmpty(userViewModel.getUserId())) {
            Appsee.setUserId(PrintOSPreferences.getInstance(context).getUserInfo().getUserId());
        } else {
            Appsee.setUserId("");
        }
    }

    public void startScreen(String screenName) {
        Appsee.startScreen(screenName);
    }

    @Override
    public void onAppseeSessionStarting(AppseeSessionStartingInfo sessionStartingInfo) {

    }

    @Override
    public void onAppseeSessionStarted(AppseeSessionStartedInfo sessionStartedInfo) {
        // Crashlytics (session-level integration)
        String crashlyticsAppseeId = Appsee.generate3rdPartyId("Crashlytics", false);
        Crashlytics.getInstance().core.setString("AppseeSessionUrl",
                "https://dashboard.appsee.com/3rdparty/crashlytics/" + crashlyticsAppseeId);
    }

    @Override
    public void onAppseeSessionEnding(AppseeSessionEndingInfo sessionEndingInfo) {

    }

    @Override
    public void onAppseeSessionEnded(AppseeSessionEndedInfo sessionEndedInfo) {

    }

    @Override
    public void onAppseeScreenDetected(AppseeScreenDetectedInfo screenDetectedInfo) {

        // Ignore some screen elements like dialog from being detected automatically.
        if (IGNORED_COMPONENTS.contains(screenDetectedInfo.getScreenName())) {
            screenDetectedInfo.setScreenName(null);
        }

    }

    public void sendEvent(String eventName, String value) {
        String event = String.format("%s_%s", eventName, value);
        sendEvent(event);
    }

    public void sendEvent(String eventName) {
        Appsee.addEvent(eventName);
    }

    public void sendEventWithValueParameter(String eventName, String value) {

        Map<String, Object> parameters = new HashMap<>();
        parameters.put(EVENT_VALUE_PARAMETER, value);

        Appsee.addEvent(eventName, parameters);

    }
}
