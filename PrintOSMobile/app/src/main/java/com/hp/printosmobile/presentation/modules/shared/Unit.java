package com.hp.printosmobile.presentation.modules.shared;

import android.content.Context;

import com.hp.printosmobile.R;
import com.hp.printosmobile.data.remote.models.PreferencesData;


/**
 * Created by Osama Taha on 5/19/17.
 */
public enum Unit {

    LITERS(R.string.liters_consumed, R.string.liters_consumed, R.string.liters_consumed_value
            , R.string.gallons_consumed, R.string.gallons_consumed, R.string.gallons_consumed_value),
    SQM(R.string.square_meters, R.string.today_sqm, R.string.printed_value_square_meters,
            R.string.foot_square, R.string.today_foot_square, R.string.printed_value_foot_square),
    METER(R.string.meters, R.string.meters, R.string.printed_value_meters,
            R.string.foot, R.string.foot, R.string.printed_value_foot),
    MILLIMETER(-1, -1, R.string.millimeter_unit, -1, -1, R.string.inch_unit);

    private final int metricUnitDefaultStyle;
    private final int metricUnitDescriptionStyle;
    private final int metricUnitValueStyle;
    private final int imperialUnitDefaultStyle;
    private final int imperialUnitDescriptionStyle;
    private final int imperialUnitValueStyle;

    Unit(int metricUnitDefaultStyle, int metricUnitDescriptionStyle, int metricUnitValueStyle, int imperialUnitDefaultStyle, int imperialUnitDescriptionStyle, int imperialUnitValueStyle) {
        this.metricUnitDefaultStyle = metricUnitDefaultStyle;
        this.metricUnitDescriptionStyle = metricUnitDescriptionStyle;
        this.metricUnitValueStyle = metricUnitValueStyle;
        this.imperialUnitDefaultStyle = imperialUnitDefaultStyle;
        this.imperialUnitDescriptionStyle = imperialUnitDescriptionStyle;
        this.imperialUnitValueStyle = imperialUnitValueStyle;
    }

    public int getMetricUnitDefaultStyle() {
        return metricUnitDefaultStyle;
    }

    public int getMetricUnitDescriptionStyle() {
        return metricUnitDescriptionStyle;
    }

    public int getMetricUnitValueStyle() {
        return metricUnitValueStyle;
    }

    public int getImperialUnitDefaultStyle() {
        return imperialUnitDefaultStyle;
    }

    public int getImperialUnitDescriptionStyle() {
        return imperialUnitDescriptionStyle;
    }

    public int getImperialUnitValueStyle() {
        return imperialUnitValueStyle;
    }

    public int getUnitTextResourceId(PreferencesData.UnitSystem unitSystem, UnitStyle style) {
        switch (style) {
            case DEFAULT:
                return unitSystem == PreferencesData.UnitSystem.Metric ? getMetricUnitDefaultStyle() : getImperialUnitDefaultStyle();
            case DESCRIPTION:
                return unitSystem == PreferencesData.UnitSystem.Metric ? getMetricUnitDescriptionStyle() : getImperialUnitDescriptionStyle();
            case VALUE:
                return unitSystem == PreferencesData.UnitSystem.Metric ? getMetricUnitValueStyle() : getImperialUnitValueStyle();
        }
        return -1;
    }

    public String getUnitTextWithValue(Context context, PreferencesData.UnitSystem unitSystem, UnitStyle style, String value) {
        int stringResourceId = getUnitTextResourceId(unitSystem, style);
        return context.getString(stringResourceId, value);
    }

    public String getUnitText(Context context, PreferencesData.UnitSystem unitSystem, UnitStyle style) {
        int stringResourceId = getUnitTextResourceId(unitSystem, style);
        return context.getString(stringResourceId);
    }

    public enum UnitStyle {
        DEFAULT,
        DESCRIPTION,
        VALUE
    }

}
