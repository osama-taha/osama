package com.hp.printosmobile.data.local;

import android.content.Context;
import android.text.TextUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.remote.models.PreferencesData;
import com.hp.printosmobile.data.remote.models.VersionsData;
import com.hp.printosmobile.presentation.indigowidget.WidgetType;
import com.hp.printosmobile.presentation.modules.drawer.OrganizationViewModel;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;
import com.hp.printosmobile.presentation.modules.shared.UserViewModel;
import com.hp.printosmobile.utils.HPStringUtils;
import com.hp.printosmobilelib.core.communications.remote.Preferences;
import com.hp.printosmobilelib.core.communications.remote.models.UserData;
import com.hp.printosmobilelib.core.logging.HPLogger;

/**
 * Created by Osama Taha on 6/14/16.
 */
public class PrintOSPreferences {

    private static final String TAG = PrintOSPreferences.class.getSimpleName();

    private static final String WIDGET_LAST_REFRESH_TIME = "appwidget_refresh_time_";
    private static final String GOOGLE_PLAY_SERVICES_STATUS = "google_play_services_status";
    private static final String UNIT_SYSTEM_PREF = "unit_system";
    private static final String USE_V2_PREF = "use_v2";
    private static final String KEY_IS_COMPLETED_ENABLED = "key_is_completed_enabled";
    private static final String LANGUAGE_SPLIT_REGEX = "-";
    private static final String NPS_NOTIFICATION = "key_nps_notification";
    private static final String NPS_DISPLAY_TIMING = "key_nps_display_timing";
    private static final String ORGANIZATION_INTERNAL_ID = "key_organization_internal_id";
    private static final String HAS_INDIGO_DIVISION = "key_has_indigo_division";
    private static final String NPS_FEATURE_ENABLED = "nps_feature_enabled";
    private static final String BEAT_COIN_FEATURE_ENABLED = "beat_coin_feature_enabled";
    private static final String TIME_ZONE_POPUP_ENABLED = "time_zone_popup_enable";
    private static final String WEEK_INCREMENTAL_ENABLED = "week_incremental_enabled";
    private static final String REFER_TO_FRIEND_ENABLED = "refer_to_friend_enabled";
    private static final String REFER_TO_FRIEND_URL = "refer_to_friend_url";
    private static final String LOCATION_BASED_NOTIFICATION_ON = "location_based_notification_on";
    private static final String LOCATION_BASED_FEATURE_ENABLED = "location_based_feature_enabled";
    private static final String CLOSED_SERVICE_CALLS_DAYS = "closed_service_calls_days";


    private static String SELECTED_BUSINESS_UNIT = "selected_business_unit";
    private static final String KEY_LOGIN_USER_FIRST_NAME = "LoggedInUserFirstName";
    private static final String KEY_LOGIN_USER_LAST_NAME = "LoggedInUserLastName";
    private static final String KEY_LOGIN_USER_DISPLAY_NAME = "LoggedInUserDisplayName";
    private static final String KEY_LOGIN_USER_ID = "LoggedInUserID";
    public static final String KEY_LAST_VERSION_CHECKING_TIME = "lastVersionCheckinTime";
    public static final String KEY_SUPPORTED_VERSIONS = "supportedVersions";
    public static final String KEY_DID_SHOW_WARNING_DIALOG = "didShowWarningDialog";
    private static final String KEY_IS_LOGGED_IN = "isLoggedIn";
    public static final String LANGUAGE_PREFERENCE = "Language";
    public static final String USER_ROLES = "LoggedInUserRoles";
    private static final String SELF_PROVISION_ORGANIZATION = "selfProvisionOrganization";
    private static final String IS_FIRST_LAUNCH_KEY = "isFirstLaunchKey";
    private static final String BETA_TESTER_NOTIFICATION = "beta_testers_notification";

    private static final String CHECK_COOKE_EXPIRY_TIME_KEY = "checkCookieExpiryHrs";
    private static final String MOVE_TO_HOME_TIME_KEY = "moveToHomeMins";
    private static final String SHOW_VALIDATION_TOAST_KEY = "showToastMessage";

    //Keys for GCM.
    private static final String KEY_SENT_TOKEN_TO_SERVER = "sentTokenToServer";
    private static final String KEY_DEVICE_GCM_TOKEN = "deviceGCMToken";
    private static final String KEY_NOTIFICATION_MODE = "notificationMode";
    public static final boolean NOTIFICATION_MODE_ON = true;
    public static final boolean NOTIFICATION_MODE_OFF = false;

    //rating keys
    private static final String VERSION_NAME_PREF = "version_name_pref";
    private static final String RATE_STATUS_PREF = "rate_status_pref";
    private static final String RATE_DIALOG_NUMBER_OF_SESSIONS_PREF = "number_of_sessions_pref";
    private static final String TIME_ZONE_NUMBER_OF_SESSIONS_PREF = "time_zone_number_of_sessions_pref";
    private static final String TIME_ZONE_SHOWN = "time_zone_popup_shown";

    private static final String INSIGHTS_NEW_TAB = "Insights_NewTab";
    private static final String INSIGHTS_TAB_VERSION = "insights_tabVersion";
    private static final String INSIGHTS_JAMS_ENABLED = "insights_jamsEnabled";
    private static final String INSIGHTS_FAILURES_ENABLED = "insights_failuresEnabled";

    private static final String KPI_EXPLANATION_ENABLED = "kpi_explanation_enabled";


    private static PrintOSPreferences sSharedPrintOSPreferences;
    private static Preferences sPreferences;

    private PrintOSPreferences(Context context) {
        sPreferences = Preferences.getInstance(context);
    }

    public static PrintOSPreferences getInstance(Context context) {

        if (sSharedPrintOSPreferences == null) {
            sSharedPrintOSPreferences = new PrintOSPreferences(context.getApplicationContext());
        }

        return sSharedPrintOSPreferences;
    }


    public void saveSelectedBusinessUnit(BusinessUnitEnum selectedBusinessUnit) {

        sPreferences.put(SELECTED_BUSINESS_UNIT, selectedBusinessUnit.getName());
        sPreferences.commit();
    }

    public BusinessUnitEnum getSelectedBusinessUnit() {

        return BusinessUnitEnum.from(sPreferences.getString(SELECTED_BUSINESS_UNIT, null));

    }

    /**
     * Saves the information pertaining to the currently logged in user.
     *
     * @param userData - An object contains the user details.
     */
    public void saveUserInfo(UserData userData) {

        sPreferences.put(KEY_LOGIN_USER_ID, userData.getUser().getUserId());
        sPreferences.put(KEY_LOGIN_USER_FIRST_NAME, userData.getUser().getFirstName());
        sPreferences.put(KEY_LOGIN_USER_LAST_NAME, userData.getUser().getLastName());
        sPreferences.put(KEY_LOGIN_USER_DISPLAY_NAME, userData.getUser().getDisplayName());

        //Commit the changes.
        sPreferences.commit();

        saveUserInfo();


    }

    /**
     * Retrieves the information pertaining to the currently logged in user.
     *
     * @return user object.
     */
    public UserViewModel getUserInfo() {

        UserViewModel user = new UserViewModel();

        user.setUserId(sPreferences.getString(KEY_LOGIN_USER_ID, ""));
        user.setDisplayName(sPreferences.getString(KEY_LOGIN_USER_DISPLAY_NAME, ""));
        user.setFirstName(sPreferences.getString(KEY_LOGIN_USER_FIRST_NAME, ""));
        user.setLastName(sPreferences.getString(KEY_LOGIN_USER_LAST_NAME, ""));
        user.setUserRoles(sPreferences.getString(USER_ROLES, ""));
        user.setSelfProvisionOrg(sPreferences.getString(SELF_PROVISION_ORGANIZATION, ""));
        user.setUserOrganization(getSelectedOrganization());

        return user;
    }

    public void saveOrganization(UserData.Context organization) {
        sPreferences.saveOrganization(organization);
    }

    public void saveUserInfo() {
        UserViewModel userViewModel = getUserInfo();
        sPreferences.saveUserId(userViewModel.getUserId());
    }

    public void saveOrganization(OrganizationViewModel organization) {

        UserData.Context organizationContext = new UserData.Context();
        organizationContext.setId(organization.getOrganizationId());
        organizationContext.setType(organization.getOrganizationTypeDisplayName());
        organizationContext.setName(organization.getOrganizationName());

        sPreferences.saveOrganization(organizationContext);
    }

    public void saveUserType(String userType) {
        sPreferences.saveUserType(userType);
    }

    public OrganizationViewModel.OrganizationType getUserType() {
        return OrganizationViewModel.OrganizationType.getType(sPreferences.getUserType());
    }

    public void clearOrganization() {
        sPreferences.clearOrganization();
    }


    public OrganizationViewModel getSelectedOrganization() {

        UserData.Context organizationContext = Preferences.getInstance(PrintOSApplication.getAppContext()).getSelectedOrganization();

        OrganizationViewModel organizationViewModel = new OrganizationViewModel();
        organizationViewModel.setOrganizationId(organizationContext.getId());
        organizationViewModel.setOrganizationName(organizationContext.getName());
        organizationViewModel.setOrganizationTypeDisplayName(organizationContext.getType());
        organizationViewModel.setOrganizationType(OrganizationViewModel.OrganizationType.getType(organizationContext.getType()));

        return organizationViewModel;
    }

    public VersionsData getVersionsData() {

        String versionsDataJson = sPreferences.getString(KEY_SUPPORTED_VERSIONS, null);

        if (!TextUtils.isEmpty(versionsDataJson)) {

            ObjectMapper mapper = new ObjectMapper();
            try {
                VersionsData versionsData = mapper.readValue(versionsDataJson, VersionsData.class);
                return versionsData;
            } catch (Exception e) {
                HPLogger.e(TAG, "Error in parsing the versions data", e);
            }
        }

        return null;
    }

    public void setSupportedVersions(String versions) {
        sPreferences.put(KEY_SUPPORTED_VERSIONS, versions);
        sPreferences.commit();
    }

    public long getVersionCheckingTime() {
        return sPreferences.getLong(KEY_LAST_VERSION_CHECKING_TIME, -1);
    }

    public void setVersionCheckingTime(long time) {
        sPreferences.put(KEY_LAST_VERSION_CHECKING_TIME, time);
        sPreferences.commit();
    }

    public boolean isWarningUpdateDialogEnabled() {
        return sPreferences.getBoolean(KEY_DID_SHOW_WARNING_DIALOG, true);
    }

    public void enableWarningUpdateDialog(Boolean enable) {
        sPreferences.put(KEY_DID_SHOW_WARNING_DIALOG, enable);
        sPreferences.commit();
    }


    public void enableCompletedJobsFeature(boolean enable) {
        sPreferences.put(KEY_IS_COMPLETED_ENABLED, enable);
        sPreferences.commit();
    }

    public boolean isCompletedJobsFeatureEnabled() {
        return sPreferences.getBoolean(KEY_IS_COMPLETED_ENABLED, false);
    }


    public String getSavedOrganizationId() {
        return sPreferences.getSavedOrganizationId();
    }

    public void setSentNotificationTokenToServer(boolean sent) {
        sPreferences.put(KEY_SENT_TOKEN_TO_SERVER, sent);
        sPreferences.commit();
    }

    public boolean sentNotificationTokenToServer() {
        return sPreferences.getBoolean(KEY_SENT_TOKEN_TO_SERVER, false);
    }

    public void setNotificationToken(String token) {
        sPreferences.put(KEY_DEVICE_GCM_TOKEN, token);
        sPreferences.commit();
    }

    public String getNotificationToken() {
        return sPreferences.getString(KEY_DEVICE_GCM_TOKEN, "");
    }

    public void setNotificationEnabled(boolean isEnabled) {
        sPreferences.put(KEY_NOTIFICATION_MODE, isEnabled);
        sPreferences.commit();
    }

    public boolean isNotificationEnabled() {
        return sPreferences.getBoolean(KEY_NOTIFICATION_MODE, NOTIFICATION_MODE_ON);
    }

    public void setLoggedIn(boolean isLoggedIn) {
        sPreferences.put(KEY_IS_LOGGED_IN, isLoggedIn);
        sPreferences.commit();
    }


    public boolean isLoggedIn() {
        return sPreferences.getBoolean(KEY_IS_LOGGED_IN, false);
    }

    public String getServerUrl(Context context) {
        return sPreferences.getString(context.getString(R.string.pref_server_choose),
                context.getString(R.string.default_server));
    }

    public String getServerName() {

        String prefValue = getServerUrl(PrintOSApplication.getAppContext());
        CharSequence[] keys = PrintOSApplication.getAppContext().getResources().getTextArray(R.array.pref_servers_titles);
        CharSequence[] values = PrintOSApplication.getAppContext().getResources().getTextArray(R.array.pref_servers_values);
        int length = values.length;
        for (int i = 0; i < length; i++) {
            if (values[i].equals(prefValue)) {
                return (String) keys[i];
            }
        }

        return "";
    }

    public boolean showServerName() {

        String selectedServerName = getServerName();
        if (selectedServerName.equals(PrintOSApplication.getAppContext().getString(R.string.default_server_item))) {
            return false;
        }

        return true;
    }

    public void setLanguage(String language) {
        sPreferences.put(LANGUAGE_PREFERENCE, language);
        sPreferences.commit();
    }

    public String getLanguage(String defaultValue) {
        return sPreferences.getString(LANGUAGE_PREFERENCE, defaultValue);
    }

    public String getLanguage() {
        return sPreferences.getString(LANGUAGE_PREFERENCE, PrintOSApplication.getAppContext().getString(R.string.default_language));
    }

    public String getLanguageCode() {
        String language = getLanguage(PrintOSApplication.getAppContext().getString(R.string.default_language));
        return HPStringUtils.getStringBeforeRegex(language, LANGUAGE_SPLIT_REGEX);
    }

    public String getPreferenceValue(String key) {
        return sPreferences.getString(key, "");
    }

    public void saveUserRoles(String roles) {
        sPreferences.put(USER_ROLES, roles);
        sPreferences.commit();
    }

    public void saveSelfProvisionOrganization(String provision) {
        sPreferences.put(SELF_PROVISION_ORGANIZATION, provision);
        sPreferences.commit();
    }

    public void clearUserInfo() {
        sPreferences.put(KEY_LOGIN_USER_ID, "");
        sPreferences.put(KEY_LOGIN_USER_DISPLAY_NAME, "");
        sPreferences.put(KEY_LOGIN_USER_FIRST_NAME, "");
        sPreferences.put(KEY_LOGIN_USER_LAST_NAME, "");
        sPreferences.put(USER_ROLES, "");
        sPreferences.put(SELF_PROVISION_ORGANIZATION, "");

        sPreferences.commit();

        sPreferences.saveUserType("");
        sPreferences.clearOrganization();
    }

    public void saveWidgetLastRefreshTime(WidgetType widgetType, long lastRefreshTime) {
        sPreferences.put(String.valueOf(WIDGET_LAST_REFRESH_TIME + widgetType.name()), lastRefreshTime);
        sPreferences.commit();
    }

    public long getWidgetLastRefreshTime(WidgetType widgetType) {
        return sPreferences.getLong(String.valueOf(WIDGET_LAST_REFRESH_TIME + widgetType.name()), 0);
    }

    public boolean isFirstLaunch() {
        return sPreferences.getBoolean(IS_FIRST_LAUNCH_KEY, true);
    }

    public void setIsFirstLaunch(boolean isFirstLaunch) {
        sPreferences.put(IS_FIRST_LAUNCH_KEY, isFirstLaunch);
        sPreferences.commit();
    }

    public void setGooglePlayServicesStatus(String status) {
        sPreferences.put(GOOGLE_PLAY_SERVICES_STATUS, status);
        sPreferences.commit();
    }

    public String getGooglePlayServiceStatus() {
        return sPreferences.getString(GOOGLE_PLAY_SERVICES_STATUS, "");
    }

    public boolean wasBetaTestersNotified() {
        return sPreferences.getBoolean(BETA_TESTER_NOTIFICATION, false);
    }

    public void setBetaTestersNotified() {
        sPreferences.put(BETA_TESTER_NOTIFICATION, true);
        sPreferences.commit();
    }

    public String getAppVersion() {
        return sPreferences.getString(VERSION_NAME_PREF, "");
    }

    public void setAppVersion(String versionName) {
        sPreferences.put(VERSION_NAME_PREF, versionName == null ? "" : versionName);
        sPreferences.commit();
    }

    public void setRateStatus(String rateStatus) {
        sPreferences.put(RATE_STATUS_PREF, rateStatus == null ? "" : rateStatus);
        sPreferences.commit();
    }

    public String getRateStatus() {
        return sPreferences.getString(RATE_STATUS_PREF, "");
    }

    public synchronized void setNumberOfAuthenticatedSessionsForRateDialog(int numberOfSessions) {
        sPreferences.put(RATE_DIALOG_NUMBER_OF_SESSIONS_PREF, numberOfSessions);
        sPreferences.commit();
    }


    public synchronized int getNumberOfAuthenticatedSessionsForRateDialog() {
        return sPreferences.getInt(RATE_DIALOG_NUMBER_OF_SESSIONS_PREF, 0);
    }

    public synchronized void setNumberOfAuthenticatedSessionsForTimeZoneDialog(int numberOfSessions) {
        sPreferences.put(TIME_ZONE_NUMBER_OF_SESSIONS_PREF, numberOfSessions);
        sPreferences.commit();
    }

    public synchronized int getNumberOfAuthenticatedSessionsForTimeZoneDialog() {
        return sPreferences.getInt(TIME_ZONE_NUMBER_OF_SESSIONS_PREF, 0);
    }

    public static void saveUnitSystem(PreferencesData.UnitSystem unitSystem) {

        if (unitSystem == null) {
            return;
        }

        HPLogger.d(TAG, "save unit " + unitSystem.name());

        sPreferences.put(UNIT_SYSTEM_PREF, unitSystem.name());
        sPreferences.commit();
    }

    public static PreferencesData.UnitSystem getUnitSystem() {

        String unitSystemString = sPreferences.getString(UNIT_SYSTEM_PREF, PreferencesData.UnitSystem.Metric.name());
        PreferencesData.UnitSystem unitSystem = PreferencesData.UnitSystem.from(unitSystemString);

        return unitSystem;
    }

    public void setV2Support(boolean v2Support) {
        sPreferences.put(USE_V2_PREF, v2Support);
        sPreferences.commit();
    }

    public void saveValidationParams(int checkCookieExpiryHrs, int moveToHomeMins, boolean showToastMessage) {
        sPreferences.put(CHECK_COOKE_EXPIRY_TIME_KEY, checkCookieExpiryHrs);
        sPreferences.put(MOVE_TO_HOME_TIME_KEY, moveToHomeMins);
        sPreferences.put(SHOW_VALIDATION_TOAST_KEY, showToastMessage);
        sPreferences.commit();
    }

    public int getCookieExpiryTimeFloor(int defValue) {
        return sPreferences.getInt(CHECK_COOKE_EXPIRY_TIME_KEY, defValue);
    }

    public int getMoveToHomeTimePeriod(int defValue) {
        return sPreferences.getInt(MOVE_TO_HOME_TIME_KEY, defValue);
    }

    public boolean showValidationToastMsg(boolean defValue) {
        return sPreferences.getBoolean(SHOW_VALIDATION_TOAST_KEY, defValue);
    }

    public boolean shouldShowInsightsNewBadge(boolean defValue) {
        return sPreferences.getBoolean(INSIGHTS_NEW_TAB, defValue);
    }

    public void setShowInsightsNewBadge(boolean show) {
        sPreferences.put(INSIGHTS_NEW_TAB, show);
    }

    public boolean supportV2() {
        return true;
    }

    public boolean isInsightsJamsEnabled(boolean defValue) {
        return sPreferences.getBoolean(INSIGHTS_JAMS_ENABLED, defValue);
    }

    public void setInsightsJamsEnabled(boolean enabled) {
        sPreferences.put(INSIGHTS_JAMS_ENABLED, enabled);
        sPreferences.commit();
    }

    public boolean isInsightsFailuresEnabled(boolean defValue) {
        return sPreferences.getBoolean(INSIGHTS_FAILURES_ENABLED, defValue);
    }

    public void setInsightsFailureEnabled(boolean enabled) {
        sPreferences.put(INSIGHTS_FAILURES_ENABLED, enabled);
        sPreferences.commit();
    }

    public String getInsightsFirstOccuringVersion(String defValue) {
        return sPreferences.getString(INSIGHTS_TAB_VERSION, defValue);
    }

    public void setInsightsFirstOccuringVersion(String version) {
        sPreferences.put(INSIGHTS_TAB_VERSION, version);
        sPreferences.commit();
    }

    public boolean isNPSNotificationsEnabled() {
        return sPreferences.getBoolean(NPS_NOTIFICATION, true);
    }

    public void setNPSNotificationsEnabled(boolean enabled) {
        sPreferences.put(NPS_NOTIFICATION, enabled);
        sPreferences.commit();
    }

    public boolean hasNPSNotificationPreference() {
        return sPreferences.containsKey(NPS_NOTIFICATION);
    }

    public void setNPSLastDisplayTiming(long milliSeconds) {
        sPreferences.put(NPS_DISPLAY_TIMING, milliSeconds);
        sPreferences.commit();
    }

    public long getNPSLastDisplayTiming() {
        return sPreferences.getLong(NPS_DISPLAY_TIMING, Long.MAX_VALUE);
    }

    public void setInternalID(String id) {
        sPreferences.put(ORGANIZATION_INTERNAL_ID, id);
        sPreferences.commit();
    }

    public String getInternalID() {
        return sPreferences.getString(ORGANIZATION_INTERNAL_ID, "");
    }

    public void setHasIndigoDivision(boolean hasIndigoDivision) {
        sPreferences.put(HAS_INDIGO_DIVISION, hasIndigoDivision);
        sPreferences.commit();
    }

    public boolean getHasIndigoDivision() {
        return sPreferences.getBoolean(HAS_INDIGO_DIVISION, false);
    }

    public boolean isNPSFeatureEnabled() {
        return sPreferences.getBoolean(NPS_FEATURE_ENABLED, true);
    }

    public void setNPSFeatureEnabled(boolean enabled) {
        sPreferences.put(NPS_FEATURE_ENABLED, enabled);
        sPreferences.commit();
    }

    public boolean isBeatCoinFeatureEnabled() {
        return sPreferences.getBoolean(BEAT_COIN_FEATURE_ENABLED, true);
    }

    public void setBeatCoinFeatureEnabled(boolean enabled) {
        sPreferences.put(BEAT_COIN_FEATURE_ENABLED, enabled);
        sPreferences.commit();
    }

    public boolean isWeekIncrementalFeatureEnabled() {
        return sPreferences.getBoolean(WEEK_INCREMENTAL_ENABLED, false);
    }

    public void setWeekIncrementalFeatureEnabled(boolean enabled) {
        sPreferences.put(WEEK_INCREMENTAL_ENABLED, enabled);
        sPreferences.commit();
    }

    public boolean isReferToAFriendEnabled() {
        return sPreferences.getBoolean(REFER_TO_FRIEND_ENABLED, false);
    }

    public void setReferToAFriendEnabled(boolean enabled) {
        sPreferences.put(REFER_TO_FRIEND_ENABLED, enabled);
        sPreferences.commit();
    }

    public String getReferToAFriendUrl() {
        return sPreferences.getString(REFER_TO_FRIEND_URL, null);
    }

    public void setReferToAFriendUrl(String url) {
        sPreferences.put(REFER_TO_FRIEND_URL, url);
        sPreferences.commit();
    }

    public boolean isTimeZonePopupEnabled() {
        return sPreferences.getBoolean(TIME_ZONE_POPUP_ENABLED, false);
    }

    public void setTimeZonePopupEnabled(boolean enabled) {
        sPreferences.put(TIME_ZONE_POPUP_ENABLED, enabled);
        sPreferences.commit();
    }

    public boolean isTimeZonePopupShown() {
        return sPreferences.getBoolean(TIME_ZONE_SHOWN, false);
    }

    public void setTimeZonePopupShown(boolean shown) {
        sPreferences.put(TIME_ZONE_SHOWN, shown);
        sPreferences.commit();
    }

    public boolean isKpiExplanationEnabled() {
        return sPreferences.getBoolean(KPI_EXPLANATION_ENABLED, false);
    }

    public void setKpiExplanationEnabled(boolean enabled) {
        sPreferences.put(KPI_EXPLANATION_ENABLED, enabled);
        sPreferences.commit();
    }

    public void setLocationBasedNotificationsOn(boolean on) {
        sPreferences.put(LOCATION_BASED_NOTIFICATION_ON, on);
        sPreferences.commit();
    }

    public boolean isLocationBasedNotificationsOn() {
        return sPreferences.getBoolean(LOCATION_BASED_NOTIFICATION_ON, true);
    }

    public boolean isLocationBasedNotificationsFeatureEnabled() {
        return sPreferences.getBoolean(LOCATION_BASED_FEATURE_ENABLED, false);
    }

    public void setLocationBasedNotificationsFeatureEnabled(boolean enabled) {
        sPreferences.put(LOCATION_BASED_FEATURE_ENABLED, enabled);
        sPreferences.commit();
    }

    public void firstTimeAskingPermission(String permission, boolean isFirstTime) {
        sPreferences.put(permission, isFirstTime);
        sPreferences.commit();
    }

    public boolean isFirstTimeAskingPermission(String permission) {
        return sPreferences.getBoolean(permission, true);
    }

    public int closedServiceCallsDays(int closedServiceCallsDaysDefault) {

        if (sPreferences.containsKey(CLOSED_SERVICE_CALLS_DAYS) && sPreferences.getInt(CLOSED_SERVICE_CALLS_DAYS) > 0) {
            return sPreferences.getInt(CLOSED_SERVICE_CALLS_DAYS);
        }

        return closedServiceCallsDaysDefault;
    }

    public void setClosedServiceCallsDays(int closedServiceCallsDays) {
        sPreferences.put(CLOSED_SERVICE_CALLS_DAYS, closedServiceCallsDays);
        sPreferences.commit();
    }
}
