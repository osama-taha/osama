package com.hp.printosmobile.presentation.modules.reports;

/**
 * Holds properties to customize the bar chart.
 *
 * @Author : Osama Taha
 */
public class BarChartConfiguration extends ChartConfiguration {

    private int maxVisibleValueCount;

    public int getMaxVisibleValueCount() {
        return maxVisibleValueCount;
    }

    public void setMaxVisibleValueCount(int maxVisibleValueCount) {
        this.maxVisibleValueCount = maxVisibleValueCount;
    }
}
