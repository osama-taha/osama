package com.hp.printosmobile.presentation.modules.contacthp;

import com.hp.printosmobile.presentation.MVPView;

/**
 * Created by anwar asbah on 10/12/2016.
 */
public interface ContactHpView extends MVPView {
    void onSuccess();
    void setHasPhone (boolean hasPhone);
}
