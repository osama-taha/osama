package com.hp.printosmobile;

/**
 * A class holds constant values.
 *
 * @author Osama Taha
 */
public final class Constants {

    public static final String GOOGLE_PLAY_URL = "market://details?id=";
    public static final String REGISTRATION_LINK = "https://h71044.www7.hp.com/ga/us/en/contactus.php?PRINTOS";
    public static final String NOTIFICATION_BODY_KEY = "body";
    public static final String NOTIFICATION_ENTITY_ID_KEY = "entityId";
    public static final String NOTIFICATION_EVENT_KEY = "event";
    public static final String NOTIFICATION_USER_EVENT_ID_KEY = "userEventId";
    public static final String NOTIFICATION_ID_KEY = "notificationId";
    public static final String NOTIFICATION_LINK_KEY = "link";

    public final class IntentExtras {

        public static final int FIRST_INSTALL_PERMISSIONS_REQUEST_CODE = 1076;
        public static final int ACCESS_LOCATION_PERMISSION_REQUEST_CODE = 1077;

        public static final String NOTIFICATION_REGISTRATION_COMPLETED_INTENT_ACTION = "gcm_registration_completed";
        public static final String NOTIFICATION_UNREGISTER_COMPLETED_INTENT_ACTION = "gcm_un_register_completed";

        public static final String NOTIFICATION_UNREGISTER_INTENT_ACTION = "UNREGISTER";
        public static final String NOTIFICATION_REGISTER_INTENT_ACTION = "REGISTER";
        public static final String NOTIFICATION_UNREGISTER_AND_CLEAR_CACHE_INTENT_ACTION = "UNREGISTER_BEFORE_LOGOUT";
        public static final int EULA_ACTIVITY_REQUEST_CODE = 1001;
        public static final int EULA_ACTIVITY_RESULT_CODE = 1002;
        public static final int APPS_PICKER_INTENT_REQUEST_CODE = 1009;
        public static final String EULA_ACTIVITY_TO_ACCEPT_TERMS = "ACCEPT_EULA_TERMS";
        public static final String EULA_ACTIVITY_ACCEPTED_RESULT = "IS_EULA_ACCEPTED";
        public static final String EULA_ACTIVITY_RESULT_OBJECT = "EULA_RESULT";

        public static final String MAIN_ACTIVITY_ORGANIZATION_EXTRA = "organization_extra";
        public static final String MAIN_ACTIVITY_DIVISION_EXTRA = "division_extra";
        public static final String MAIN_ACTIVITY_FILTER_EXTRA = "filter_extra";
        public static final String MAIN_ACTIVITY_TODAY_EXTRA = "today_extra";
        public static final String MAIN_ACTIVITY_NOTIFICATION_EXTRA = "notification_extra";
        public static final String MAIN_ACTIVITY_NOTIFICATION_ID_EXTRA = "notification_id_extra" ;
        public static final String MAIN_ACTIVITY_USER_ID_EXTRA = "user_id_extra" ;
        public static final String MAIN_ACTIVITY_LINK_EXTRA = "link_extra";
        public static final String MAIN_ACTIVITY_BEAT_COIN_EXTRA = "mainActivityBeatCoinExtra";
        public static final String MAIN_ACTIVITY_IS_PUSH_NOTIFICATION = "is_push_notification";

        public static final String VERSION_UPDATE_INTENT_ACTION = "com.hp.printosforpsp.broadcast.version_update";
        public static final String VERSION_UPDATE_INTENT_EXTRA_KEY = "version_update_extra";
        public static final String NOTIFICATION_RECEIVED_ACTION = "NOTIFICATION_RECEIVED_ACTION";
        public static final String DEVICE_ID = "DEVICE_ID";

        public static final String NPS_EXTRA = "NPS";
        public static final String UNIVERSAL_LINK_SCREEN_KEY = "screen";
        public static final String UNIVERSAL_LINK_SCREEN_HOME_TAB = "HOME";
        public static final String UNIVERSAL_LINK_SCREEN_INSIGHTS_TAB = "INSIGHTS";
        public static final String UNIVERSAL_LINK_SCREEN_NOTIFICATIONS = "NOTIFICATIONS";
        public static final String UNIVERSAL_LINK_SCREEN_SERVICE_CALLS_TAB = "SERVICE_CALLS";
    }
}
