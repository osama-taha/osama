package com.hp.printosmobile.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.AppCompatTextView;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.modules.contacthp.ContactHPBaseActivity;
import com.hp.printosmobile.presentation.modules.eula.EulaActivity;
import com.hp.printosmobile.presentation.modules.login.LoginActivity;
import com.hp.printosmobile.presentation.modules.personaladvisor.PersonalAdvisorPanel;
import com.hp.printosmobile.presentation.modules.personaladvisor.presenter.PersonalAdvisorFactory;
import com.hp.printosmobile.presentation.modules.printersnapshot.PrinterSnapshotPanel;
import com.hp.printosmobile.presentation.modules.productionsnapshot.indigo.IndigoProductionPanel;
import com.hp.printosmobile.presentation.modules.productionsnapshot.latex.LatexProductionPanel;
import com.hp.printosmobile.presentation.modules.reports.ReportsFragment;
import com.hp.printosmobile.presentation.modules.resetpassword.ResetPasswordActivity;
import com.hp.printosmobile.presentation.modules.servicecallpanel.ServiceCallPanel;
import com.hp.printosmobile.presentation.modules.today.TodayPanel;
import com.hp.printosmobile.presentation.modules.week.WeekPanel;
import com.hp.printosmobilelib.core.communications.remote.APIException;
import com.hp.printosmobilelib.core.logging.HPLogger;
import com.hp.printosmobilelib.ui.widgets.TypefaceManager;

import java.util.Random;

/**
 * Created by Anwar Asbah on 5/18/2016.
 */
public class HPUIUtils {

    private static final int COLOR_COMPONENT_MAX = 255;
    private static final String TAG = HPUIUtils.class.getSimpleName();

    private static Random random = new Random();

    public static int[] randomColors = new int[]{
            Color.parseColor("#75bc73"),
            Color.parseColor("#eb4e47"),
            Color.parseColor("#f4b51e"),
            Color.parseColor("#63b4f5"),
            Color.parseColor("#c937c0"),
            Color.parseColor("#3a5e0c"),
            Color.parseColor("#3A9C79"),
            Color.parseColor("#280f1a"),
            Color.parseColor("#2c16f2"),
            Color.parseColor("#9C35E5"),
            Color.parseColor("#fade22")
    };

    public static int[] insightsColor = new int[]{
            Color.parseColor("#d1d54d"),
            Color.parseColor("#f19c2d"),
            Color.parseColor("#911e5a"),
            Color.parseColor("#f26e80"),
            Color.parseColor("#0da5c1")
    };

    public static int[] insightsDrawableID = new int[]{
            R.drawable.cursor_green,
            R.drawable.cursor_orange,
            R.drawable.cursor_purple,
            R.drawable.cursor_pink,
            R.drawable.cursor_blue
    };

    public static int randomColorIndex = 0;

    public static int getTextWidth(TextView textView, String string) {
        Rect bounds = new Rect();
        Paint textPaint = textView.getPaint();
        textPaint.getTextBounds(string, 0, string.length(), bounds);
        return bounds.width();
    }

    public static int dpToPx(Context context, int dp) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    public static int getRandomColor() {
        if (randomColorIndex < randomColors.length) {
            int color = randomColors[randomColorIndex];
            randomColorIndex = (randomColorIndex + 1) % randomColors.length;
            return color;
        }
        return Color.rgb(random.nextInt() % 255, random.nextInt() % 255, random.nextInt() % 255);
    }

    public static int getInsightColor(int i) {
        if (i < insightsColor.length) {
            return insightsColor[i];
        }
        return getRandomColor();
    }

    public static int getInsightCursourDrawabel(int i) {
        if (i < insightsDrawableID.length) {
            return insightsDrawableID[i];
        }
        return R.drawable.cursor_gray;
    }

    public static void hideSoftKeyboard(Context context, EditText... editTexts) {
        for (EditText editText : editTexts) {
            if (editText != null) {
                InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(editText.getWindowToken(), 0);
            }
        }
    }

    public static void displayToastException(Context context, APIException error, String tag, boolean customView) {
        if (tag == null || context == null) {
            return;
        }

        String msg = "";

        if (tag.equals(IndigoProductionPanel.TAG)) {
            msg = context.getString(R.string.error_failed_to_load_production_panel);
        } else if (tag.equals(LatexProductionPanel.TAG)) {
            msg = context.getString(R.string.error_failed_to_load_production_panel);
        } else if (tag.equals(TodayPanel.TAG)) {
            msg = context.getString(R.string.error_failed_to_load_today_panel);
        } else if (tag.equals(PrinterSnapshotPanel.TAG)) {
            msg = context.getString(R.string.error_failed_to_load_printer_panel);
        } else if (tag.equals(WeekPanel.TAG)) {
            msg = context.getString(R.string.error_failed_to_load_week_panel);
        } else if (tag.equals(PersonalAdvisorPanel.TAG)) {
            msg = context.getString(R.string.error_failed_to_load_advisor_panel);
        } else if (tag.equals(PersonalAdvisorFactory.TAG_LIKE)) {
            msg = context.getString(R.string.error_failed_to_like_advice);
        } else if (tag.equals(PersonalAdvisorFactory.TAG_SUPPRESS)) {
            msg = context.getString(R.string.error_failed_to_suppress_advice);
        } else if (tag.equals(ReportsFragment.TAG)) {
            msg = context.getString(R.string.error_failed_to_load_reports);
        } else if (tag.equals(EulaActivity.TAG)) {
            msg = context.getString(R.string.error_failed_to_eula);
        } else if (tag.equals(ResetPasswordActivity.TAG)) {
            msg = context.getString(R.string.error_failed_to_reset_password);
        } else if (tag.equals(TodayPanel.TAG_ACTUAL_VS_TARGET)) {
            msg = context.getString(R.string.error_failed_to_reset_password);
        } else if (tag.equals(ContactHPBaseActivity.TAG)) {
            msg = context.getString(R.string.error_failed_to_send_hp_contact_request);
        } else if (tag.equals(ServiceCallPanel.TAG)) {
            msg = context.getString(R.string.error_failed_to_get_service_call_data);
        }

        if (error != null && error.getKind() != null) {
            switch (error.getKind()) {
                case NETWORK:
                    msg += "\n" + context.getString(R.string.error_network_error);
                    break;
                case SERVICE_TIME_OUT:
                    msg = context.getString(R.string.error_service_time_out);
                    break;
                case HTTP:
                    msg += "\n" + context.getString(R.string.error_internal_server_error);
                    break;
                case UNAUTHORIZED:
                    if (tag.equals(LoginActivity.TAG)) {
                        msg = context.getString(R.string.error_login_unauthorized);
                    } else {
                        msg = msg + "\nUnauthorized!";
                    }
                    break;
                case URL_NOT_FOUND_ERROR:
                    msg += "\n" + context.getString(R.string.error_message_service_not_found);
                    break;
                case INTERNAL_SERVER_ERROR:
                    msg += "\n" + context.getString(R.string.error_internal_server_error);
                    break;
                case SERVICE_UNAVAILABLE:
                    msg += "\n" + context.getString(R.string.error_message_service_unavailable);
                    break;
                case FILE_NOT_FOUND:
                    msg += "\n" + context.getString(R.string.error_message_file_not_found);
                    break;
                case UNKNOWN_USER:
                    msg = context.getString(R.string.error_invalid_user_name_or_email_address);
                    break;
                default:
                    msg += "\n" + context.getString(R.string.error_unknown);
                    break;
            }
        }


        if (!msg.isEmpty()) {
            displayToast(context, msg, customView);
        }

    }

    /**
     * Display toast message.
     *
     * @param context - the context in which to display the toast message.
     * @param content - the message to be displayed.
     */
    public static void displayToast(Context context, String content, boolean customView) {
        HPLogger.d(TAG, "show toast message " + content);
        Toast toast = Toast.makeText(context, content, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    /**
     * Display toast message.
     *
     * @param context - the context in which to display the toast message.
     * @param Gravity - the toast location.
     * @param content - the message to be displayed.
     */
    public static void displayToast(Context context, int Gravity, String content, boolean customView) {
        HPLogger.d(TAG, "show toast message " + content);
        Toast toast = Toast.makeText(context, content, Toast.LENGTH_LONG);
        toast.setGravity(Gravity, 0, 0);
        toast.show();
    }

    /**
     * Display toast message.
     *
     * @param context - the context in which to display the toast message.
     * @param Gravity - the toast location.
     * @param content - the message to be displayed.
     */
    public static void displayToast(Context context, int Gravity, int duration, String content, boolean customView) {
        HPLogger.d(TAG, "show toast message " + content);
        Toast toast = Toast.makeText(context, content, duration);
        toast.setGravity(Gravity, 0, 0);
        toast.show();
    }

    public static void displaySnackbar(Context context, View view, String msg, int length) {

        HPLogger.d(TAG, "show toast message " + msg);

        Snackbar snackbar = Snackbar.make(view, msg, length);

        View sbView = snackbar.getView();

        AppCompatTextView textView = (AppCompatTextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        textView.setTextColor(ResourcesCompat.getColor(context.getResources(), R.color.c55, null));
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, context.getResources().getInteger(R.integer.toast_text_size_in_sp));
        textView.setTypeface(TypefaceManager.getTypeface(context, TypefaceManager.HPTypeface.HP_REGULAR));
        textView.setMaxLines(context.getResources().getInteger(R.integer.snack_bar_max_lines));
        textView.setGravity(Gravity.CENTER_HORIZONTAL);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            textView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        }

        sbView.setBackgroundColor(ResourcesCompat.getColor(context.getResources(), R.color.toast_background_color, null));

        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) sbView.getLayoutParams();
        params.gravity = Gravity.TOP;
        sbView.setLayoutParams(params);

        snackbar.show();
    }

    public static void setVisibility(boolean isVisible, View... views) {
        for (View view : views) {
            view.setVisibility(isVisible ? View.VISIBLE : View.GONE);
        }
    }

    public static void setVisibilityForViews(boolean isVisible, View... views) {
        for (View view : views) {
            view.setVisibility(isVisible ? View.VISIBLE : View.INVISIBLE);
        }

    }

    public static void resetRandomColorIndex() {
        randomColorIndex = 0;
    }

    public static int getDrawableResourceId(String name) {
        Context context = PrintOSApplication.getAppContext();
        int resourceId = context.getResources().getIdentifier(name, "drawable", context.getPackageName());
        return resourceId;
    }

    public static BitmapDrawable getNotificationIconBadgeDrawable(Context context, String numberOfNotification) {

        Bitmap textBgBitmap = Bitmap.createBitmap(convertToPixels(context, 13),
                convertToPixels(context, 13), Bitmap.Config.ARGB_8888);

        Typeface typeface = TypefaceManager.getTypeface(context, TypefaceManager.HPTypeface.HP_REGULAR);

        Canvas textCanvas = new Canvas(textBgBitmap);

        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);

        paint.setColor(ResourcesCompat.getColor(context.getResources(), R.color.c92c, null));
        textCanvas.drawCircle(textBgBitmap.getWidth() / 2, textBgBitmap.getHeight() / 2,
                textBgBitmap.getWidth() / 2, paint);

        paint.setColor(Color.WHITE);
        paint.setTypeface(typeface);
        paint.setTextAlign(Paint.Align.CENTER);
        paint.setTextSize(convertToPixels(context, 9));

        Rect textRect = new Rect();
        paint.getTextBounds(numberOfNotification, 0, numberOfNotification.length(), textRect);

        int xPos = (textCanvas.getWidth() / 2);
        int yPos = (int) ((textCanvas.getHeight() / 2) - ((paint.descent() + paint.ascent()) / 2));
        textCanvas.drawText(numberOfNotification, xPos, yPos, paint);

        Bitmap notificationBitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.notifications)
                .copy(Bitmap.Config.ARGB_8888, true);

        int leftOffset = notificationBitmap.getWidth() - textBgBitmap.getWidth();

        Canvas canvas = new Canvas(notificationBitmap);
        canvas.drawBitmap(textBgBitmap, leftOffset, 0, null);

        return new BitmapDrawable(context.getResources(), notificationBitmap);
    }

    public static int convertToPixels(Context context, int nDP) {
        final float conversionScale = context.getResources().getDisplayMetrics().density;
        return (int) ((nDP * conversionScale) + 0.5f);
    }

    public static int getColorWithAlpha(int color, double alpha) {
        return Color.argb((int) (alpha * COLOR_COMPONENT_MAX), Color.red(color),
                Color.green(color),
                Color.blue(color)
        );
    }

    public static void statusBarColor(Activity activity, int color) {
        if (activity != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            activity.getWindow().setStatusBarColor(ContextCompat.getColor(activity, color));
        }
    }
}
