package com.hp.printosmobile.presentation.modules.drawer;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.hp.printosmobile.IntercomSdk;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitViewModel;
import com.hp.printosmobilelib.ui.common.HPActivity;

import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by Anwar Asbah on 6/5/2016.
 */
public class DivisionSwitchActivity extends HPActivity implements View.OnClickListener {

    public static final String SELECTION_KEY = "selection";
    public static final String DIVISIONS_KEY = "division_key";

    @Bind(R.id.business_units_container)
    RecyclerView businessUnits;
    @Bind(R.id.cancel_button)
    TextView buttonCancel;
    @Bind(R.id.switch_button)
    TextView buttonSwitch;

    private DivisionSwitchAdapter adapter;
    private List<BusinessUnitViewModel> divisions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        IntercomSdk.getInstance(this).logEvent(IntercomSdk.PBM_VIEW_SWITCH_EVENT);

        readExtra();
        init();
    }

    public void readExtra() {
        Bundle bundle = getIntent().getExtras();
        if (bundle.containsKey(DIVISIONS_KEY)) {
            divisions = (List<BusinessUnitViewModel>) bundle.getSerializable(DIVISIONS_KEY);
        }
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_business_units_switch;
    }

    private void init() {

        if (divisions != null) {

            RecyclerView.LayoutManager manager = new LinearLayoutManager(this);

            adapter = new DivisionSwitchAdapter(divisions, this);
            businessUnits.setLayoutManager(manager);
            businessUnits.setAdapter(adapter);
            businessUnits.setHasFixedSize(true);

        }

        buttonCancel.setOnClickListener(this);
        buttonSwitch.setOnClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View v) {
        Intent returnIntent;

        switch (v.getId()) {
            case R.id.cancel_button:
                close();
                break;
            case R.id.switch_button:
                IntercomSdk.getInstance(this).logEvent(IntercomSdk.PBM_CHANGE_BU_EVENT);

                BusinessUnitViewModel model = adapter == null ? null : adapter.getSelection();
                if (model == null || model.getBusinessUnit() == null) {
                    close();
                    break;
                }

                String selection = model.getBusinessUnit().getName();
                if (selection != null) {

                    //Save the selected unit.
                    PrintOSPreferences.getInstance(this).saveSelectedBusinessUnit(BusinessUnitEnum.from(selection));

                    returnIntent = new Intent();
                    returnIntent.putExtra(SELECTION_KEY, selection);
                    setResult(Activity.RESULT_OK, returnIntent);
                    finish();
                }
                break;
        }
    }

    private void close() {
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_CANCELED, returnIntent);
        finish();
    }

    @OnClick(R.id.dialog_layout)
    public void onClickOutside() {
        close();
    }

    @Override
    protected void onPause() {
        super.onPause();
        overridePendingTransition(0, 0);
    }
}
