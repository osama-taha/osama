package com.hp.printosmobile.presentation.modules.week.kpiexplanationdialog;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.data.remote.models.PreferencesData;
import com.hp.printosmobile.presentation.modules.kpiview.KPIScoreStateEnum;
import com.hp.printosmobile.presentation.modules.kpiview.KPIViewModel;
import com.hp.printosmobile.utils.HPLocaleUtils;
import com.hp.printosmobilelib.ui.common.HPView;
import com.hp.printosmobilelib.ui.utils.DividerItemDecoration;
import com.hp.printosmobilelib.ui.widgets.HPDrawableBasedProgressBar;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Anwar Asbah on 8/7/2017.
 */
public class KPIExplanationView extends FrameLayout implements HPView<KPIViewModel> {

    private static final String DENOMINATOR_STRING = "/%s";

    @Bind(R.id.header_kpi_image_view)
    ImageView headerKpiImage;
    @Bind(R.id.kpi_name_text_view)
    TextView headerKpiName;
    @Bind(R.id.score_nominator_text_view)
    TextView headerScoreNominator;
    @Bind(R.id.score_denominator_text_view)
    TextView headerScoreDenominator;
    @Bind(R.id.description_text_view)
    TextView descriptionTextView;
    @Bind(R.id.properties_recycler_view)
    RecyclerView propertiesList;
    @Bind(R.id.kpi_progress_bar)
    HPDrawableBasedProgressBar kpiProgressBar;
    @Bind(R.id.sub_kpi_list_separator)
    View subKpiListSeparator;
    @Bind(R.id.sub_kpi_list)
    RecyclerView subKpiList;
    @Bind(R.id.more_less_button_container)
    View moreLessButtonContainer;
    @Bind(R.id.more_less_details_text_view)
    TextView moreLessTextView;
    @Bind(R.id.more_less_arrow)
    ImageView moreLessArrow;
    @Bind(R.id.no_info_to_display_view)
    View noInfoToDisplayView;
    @Bind(R.id.data_view)
    View dataView;

    public KPIExplanationView(Context context) {
        super(context);
        init();
    }

    public KPIExplanationView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public KPIExplanationView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.view_kpi_explanation, this);
        ButterKnife.bind(this, this);
    }

    @Override
    public void setViewModel(KPIViewModel viewModel) {
        if (viewModel == null || viewModel.getKpiExplanationViewModels() == null ||
                viewModel.getKpiExplanationViewModels().size() == 0) {
            if(viewModel != null) {
                KpiExplanationEnum kpiExplanationEnum = viewModel.getKpiEnum();
                headerKpiImage.setImageDrawable(ResourcesCompat.getDrawable(getResources(),
                        kpiExplanationEnum.getKpiExplanationHeaderDrawable(), null));
                headerKpiName.setText(getContext().getString(kpiExplanationEnum.getLocalizedNameStringID()));
            }
            dataView.setVisibility(GONE);
            noInfoToDisplayView.setVisibility(VISIBLE);
            return;
        }

        List<KpiExplanationViewModel> viewModels = viewModel.getKpiExplanationViewModels();
        boolean hasSubKpi = viewModels.size() > 1;

        if (hasSubKpi) {
            initHeader(
                    ResourcesCompat.getDrawable(getResources(), viewModel.getKpiEnum().getKpiExplanationHeaderDrawable(), null),
                    getContext().getString(viewModel.getKpiEnum().getLocalizedNameStringID()),
                    viewModel.getScore(), viewModel.getMaxScore());

            setKpiProgressBarColor(kpiProgressBar, headerScoreNominator, viewModel.getScoreState())
                    .setScoreAndLimit(viewModel.getScore(), viewModel.getMaxScore())
                    .setIndicatorDrawable(ResourcesCompat.getDrawable(getResources(),
                            viewModel.getKpiEnum().getKpiExplanationIndicatorDrawable(), null))
                    .setValue(HPLocaleUtils.getDecimalString(viewModel.getValue(), true));

            descriptionTextView.setText(getContext().getString(R.string.kpi_explanation_header_more_details));
            descriptionTextView.setVisibility(VISIBLE);

            addSubKpis(viewModels);
        } else {
            setExplanationViewModel(viewModels.get(0));
        }

        moreLessButtonContainer.setVisibility(hasSubKpi ? VISIBLE : GONE);
        moreLessButtonContainer.setOnClickListener(new OnClickListener() {

            private boolean isCollapsed = true;

            @Override
            public void onClick(View view) {
                isCollapsed = !isCollapsed;
                moreLessArrow.setImageDrawable(ResourcesCompat.getDrawable(getResources(),
                        isCollapsed ? R.drawable.arrow_down_blue : R.drawable.arrow_up_blue, null));
                moreLessTextView.setText(getResources().getString(isCollapsed ?
                        R.string.kpi_explanation_more_details : R.string.kpi_explanation_less_details));
                subKpiList.setVisibility(isCollapsed ? GONE : VISIBLE);
                subKpiListSeparator.setVisibility(isCollapsed ? GONE : VISIBLE);
                descriptionTextView.setVisibility(isCollapsed ? VISIBLE : GONE);
            }
        });

        kpiProgressBar.setVisibility(viewModel.getKpiEnum().isShowBar() ? VISIBLE : GONE);
    }

    public void addSubKpis(List<KpiExplanationViewModel> viewModels) {
        SubKpiAdapter subKpiAdapter = new SubKpiAdapter(viewModels);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        subKpiList.setAdapter(subKpiAdapter);
        subKpiList.setLayoutManager(layoutManager);
        subKpiList.setHasFixedSize(true);
        subKpiList.addItemDecoration(new DividerItemDecoration(getContext(), 1, viewModels.size(), false));

    }

    public void setExplanationViewModel(KpiExplanationViewModel kpiViewModel) {
        initHeader(ResourcesCompat.getDrawable(getResources(), kpiViewModel.getKpiExplanationEnum().getKpiExplanationHeaderDrawable(), null),
                getContext().getString(kpiViewModel.getKpiExplanationEnum().getLocalizedNameStringID()),
                kpiViewModel.getScore(), kpiViewModel.getLimit());

        PropertiesAdapter propertiesAdapter = new PropertiesAdapter(kpiViewModel.getProperties());
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        propertiesList.setAdapter(propertiesAdapter);
        propertiesList.setLayoutManager(layoutManager);
        propertiesList.setHasFixedSize(true);

        if (kpiViewModel.getDescriptionText() != null) {
            descriptionTextView.setText(kpiViewModel.getDescriptionText());
            descriptionTextView.setVisibility(VISIBLE);
        }

        int barTagID = PrintOSPreferences.getInstance(getContext()).getUnitSystem() == PreferencesData.UnitSystem.Metric ?
                kpiViewModel.getKpiExplanationEnum().getBarDescriptiveTagStringIDMetric() :
                kpiViewModel.getKpiExplanationEnum().getBarDescriptiveTagStringIDImperial();

        setKpiProgressBarColor(kpiProgressBar, headerScoreNominator, kpiViewModel.getStateEnum())
                .setScoreAndLimit(kpiViewModel.getScore(), kpiViewModel.getLimit())
                .setIndicatorDrawable(ResourcesCompat.getDrawable(getResources(),
                        kpiViewModel.getKpiExplanationEnum().getKpiExplanationIndicatorDrawable(), null))
                .setValue(HPLocaleUtils.getDecimalString(kpiViewModel.getValue(), true))
                .setMinValue(kpiViewModel.getMin() < 0 ? "" : HPLocaleUtils.getDecimalString(kpiViewModel.getMin(), true))
                .setMaxValue(HPLocaleUtils.getDecimalString(kpiViewModel.getMax(), true))
                .setBarTag(getContext().getString(barTagID));
    }

    public void initHeader(Drawable drawable, String name, int score, int limit) {
        headerKpiImage.setImageDrawable(drawable);
        headerKpiName.setText(name);
        headerScoreNominator.setText(String.valueOf(score));
        headerScoreDenominator.setText(String.format(DENOMINATOR_STRING, limit));
    }

    @Override
    public void showLoading() {
    }

    @Override
    public void hideLoading() {
    }

    private HPDrawableBasedProgressBar setKpiProgressBarColor(HPDrawableBasedProgressBar bar, TextView textView, KPIScoreStateEnum state) {
        int progressColor;

        switch (state) {
            case GREAT:
                progressColor = ResourcesCompat.getColor(getResources(), R.color.kpi_score_state_great_progress_color, null);
                break;
            case GOOD:
                progressColor = ResourcesCompat.getColor(getResources(), R.color.kpi_score_state_great_progress_color, null);
                break;
            case AVERAGE:
                progressColor = ResourcesCompat.getColor(getResources(), R.color.kpi_score_state_good_progress_color, null);
                break;
            default:
                progressColor = ResourcesCompat.getColor(getResources(), R.color.kpi_score_state_below_progress_color, null);
                break;
        }
        textView.setTextColor(progressColor);

        return bar.setColors(progressColor);
    }

    public class PropertiesAdapter extends RecyclerView.Adapter<PropertiesAdapter.PropertyViewHolder> {

        private List<KpiExplanationViewModel.Property> properties;

        public PropertiesAdapter(List<KpiExplanationViewModel.Property> properties) {
            this.properties = properties;
        }

        @Override
        public PropertyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(getContext());

            return new PropertyViewHolder(inflater.inflate(
                    R.layout.kpi_explanation_property_label_view, parent, false));
        }

        @Override
        public void onBindViewHolder(PropertyViewHolder holder, int position) {
            holder.labelText.setText(KpiExplanationUtils.getPropertySpannable(getContext(),
                    properties.get(position)));
        }

        @Override
        public int getItemCount() {
            return properties == null ? 0 : properties.size();
        }

        public class PropertyViewHolder extends RecyclerView.ViewHolder {

            @Bind(R.id.label_text)
            TextView labelText;

            public PropertyViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }
        }
    }

    public class SubKpiAdapter extends RecyclerView.Adapter<SubKpiAdapter.SubKpiViewHolder> {

        private List<KpiExplanationViewModel> kpiList;

        public SubKpiAdapter(List<KpiExplanationViewModel> kpiList) {
            this.kpiList = kpiList;
        }

        @Override
        public SubKpiViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(getContext());

            return new SubKpiViewHolder(inflater.inflate(
                    R.layout.view_kpi_explanation_page, parent, false));
        }

        @Override
        public void onBindViewHolder(SubKpiViewHolder holder, int position) {
            holder.kpiExplanationView.setExplanationViewModel(kpiList.get(position));
        }

        @Override
        public int getItemCount() {
            return kpiList == null ? 0 : kpiList.size();
        }

        public class SubKpiViewHolder extends RecyclerView.ViewHolder {

            @Bind(R.id.score_bar)
            KPIExplanationView kpiExplanationView;

            public SubKpiViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }
        }
    }

}