package com.hp.printosmobile.presentation.indigowidget;

/**
 * Created by Osama Taha on 2/10/17.
 */
public enum WidgetType {

    INDIGO_4X2(2, Indigo4X2WidgetService.class, Indigo4X2WidgetProvider.class),
    INDIGO_4X3(3, Indigo4X3WidgetService.class, Indigo4X3WidgetProvider.class);

    private final int mMaxDevices;
    private final Class mServiceClass;
    private final Class mProviderClass;

    WidgetType(int maxDevices, Class serviceClass, Class providerClass) {
        this.mMaxDevices = maxDevices;
        this.mServiceClass = serviceClass;
        this.mProviderClass = providerClass;
    }

    public int getMaxDevices() {
        return mMaxDevices;
    }

    public Class getServiceClass() {
        return mServiceClass;
    }

    public Class getProviderClass() {
        return mProviderClass;
    }
}