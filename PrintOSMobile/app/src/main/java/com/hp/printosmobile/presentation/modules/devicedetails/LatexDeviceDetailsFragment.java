package com.hp.printosmobile.presentation.modules.devicedetails;

import android.os.Bundle;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.AppseeSdk;
import com.hp.printosmobile.IntercomSdk;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.data.remote.models.PreferencesData;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;
import com.hp.printosmobile.presentation.modules.shared.DeviceExtraData;
import com.hp.printosmobile.presentation.modules.shared.DeviceLastUpdateType;
import com.hp.printosmobile.presentation.modules.shared.DeviceState;
import com.hp.printosmobile.presentation.modules.shared.DeviceViewModel;
import com.hp.printosmobile.presentation.modules.shared.DevicesUtils;
import com.hp.printosmobile.presentation.modules.shared.PrinterUtils;
import com.hp.printosmobile.presentation.modules.shared.Unit;
import com.hp.printosmobile.utils.HPLocaleUtils;
import com.hp.printosmobile.utils.HPScoreUtils;
import com.hp.printosmobilelib.core.utils.HPDateUtils;
import com.hp.printosmobilelib.ui.common.HPFragment;
import com.hp.printosmobilelib.ui.widgets.HPProgressBar;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.Bind;

/**
 * create by anwar asbah 10/8/2016
 */
public class LatexDeviceDetailsFragment extends HPFragment implements InkAdapter.InkAdapterListener {

    private static final String ARG_VIEW_MODEL = "param1";
    private static final String SERVER_EXPIRY_DATE_FORMAT = "yyyy-MM-dd";

    private static final float BAR_CORNER_RADIUS = 0;
    private static final float TARGET_INDICATOR_RADIUS = 3;
    private static final float CURRENT_JOB_PROGRESS_BAR_MAX_VALUE = 100.0f;

    @Bind(R.id.press_name_text_view)
    TextView pressNameTextView;
    @Bind(R.id.updated_time_text_view)
    TextView updateTimeTextView;
    @Bind(R.id.press_image_view)
    ImageView pressImageView;
    @Bind(R.id.press_state_image)
    ImageView pressStateImage;
    @Bind(R.id.press_message_text_view)
    TextView pressMsgTextView;
    @Bind(R.id.front_panel_msg_layout)
    View frontPanelMsg;
    @Bind(R.id.progress_bar)
    HPProgressBar progressBar;
    @Bind(R.id.print_volume_unit_text_view)
    TextView printVolumeUnitTextView;
    @Bind(R.id.impression_nominator_text_view)
    TextView nominatorPrintVolumeTextView;
    @Bind(R.id.impression_denominator_text_view)
    TextView denominatorPrintVolumeTextView;
    @Bind(R.id.time_in_state_text_view)
    TextView timeInStateTextView;

    @Bind(R.id.press_state_text_view)
    TextView pressStateTextView;

    @Bind(R.id.print_queue_jobs_in_queue_text_view)
    TextView jobsInQueueValueTextView;

    @Bind(R.id.current_job_data_na_text_view)
    View currentJobDataNotAvailable;
    @Bind(R.id.current_job_content_layout)
    View currentJobContentLayout;
    @Bind(R.id.current_job_image)
    ImageView currentJobImage;
    @Bind(R.id.current_job_name_text_view)
    TextView currentJobNameTextView;
    @Bind(R.id.current_job_status_value_text_view)
    TextView currentJobStatusTextView;
    @Bind(R.id.current_job_time_to_complete_text_view)
    TextView currentJobTimeToComplete;
    @Bind(R.id.current_job_progress_bar)
    HPProgressBar currentJobProgressBar;

    @Bind(R.id.substrate_title_text_view)
    View substrateTitle;
    @Bind(R.id.substrate_recycler_view)
    RecyclerView substrateRecyclerView;
    @Bind(R.id.substrate_data_na_text_view)
    View substrateDataUnavailable;

    @Bind(R.id.ink_data_na_text_view)
    View inkNotAvailableView;
    @Bind(R.id.ink_list)
    View inkList;
    @Bind(R.id.ink_name_text_view)
    TextView inkNameTextView;
    @Bind(R.id.ink_level_description_status_text_view)
    TextView inkDescriptionStatusTextView;
    @Bind(R.id.ink_level_description_expiry_date_text_view)
    TextView inkDescriptionExpirationTextView;
    @Bind(R.id.ink_description_view)
    View inkDescriptionView;
    @Bind(R.id.ink_recycler_view)
    RecyclerView inkRecyclerView;

    @Bind(R.id.printer_serial_number_value_text_view)
    TextView printerSerialNumberTextView;
    @Bind(R.id.printer_firmware_value_text_view)
    TextView printerFirmwareVersionTextView;

    @Bind(R.id.printed_data_na_text_view)
    View printedDataNotAvailable;
    @Bind(R.id.printed_layout)
    View printedLayout;
    @Bind(R.id.printed_jobs_count_text_view)
    TextView printedJobsCountTextView;
    @Bind(R.id.liters_consumed_text_view)
    TextView litersConsumedTextView;

    private DeviceViewModel deviceViewModel;
    private DeviceExtraData deviceExtraData;
    private boolean hasExtraData;
    private PreferencesData.UnitSystem unitSystem;

    public static LatexDeviceDetailsFragment newInstance(DeviceViewModel deviceViewModel) {
        LatexDeviceDetailsFragment fragment = new LatexDeviceDetailsFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_VIEW_MODEL, deviceViewModel);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null) {
            deviceViewModel = (DeviceViewModel) bundle.getSerializable(ARG_VIEW_MODEL);
            deviceExtraData = deviceViewModel == null ? null : deviceViewModel.getExtraData();
            hasExtraData = deviceExtraData != null;
        }

        unitSystem = PrintOSPreferences.getInstance(getActivity()).getUnitSystem();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        displayModel();
        IntercomSdk.getInstance(PrintOSApplication.getAppContext()).logEvent(
                IntercomSdk.PBM_VIEW_DEVICELIST_EVENT, BusinessUnitEnum.LATEX_PRINTER);
        Analytics.sendEvent(Analytics.MOBILE_PRINT_BEAT_CATEGORY, Analytics.VIEW_DEVICE_DETAILS_ACTION, deviceViewModel.getName());
        AppseeSdk.getInstance(getActivity()).startScreen(AppseeSdk.SCREEN_LATEX_DEVICE_DETAILS);
    }

    private void displayModel() {

        displayPrinterInfo();
        displayJobsInfo();
        displayPrinterSerialInfo();
        displaySubstrateInfo();
        displayInkInfo();
    }

    private void displayPrinterInfo() {

        if (deviceViewModel.getModel() != null) {
            pressNameTextView.setText(getString(R.string.printer_name_with_model,
                    deviceViewModel.getName(), deviceViewModel.getModel()));
        } else {
            pressNameTextView.setText(deviceViewModel.getName());
        }

        if (deviceViewModel.getLastUpdateTime() != null) {
            String lastUpdateDate = HPDateUtils.getDateTimeString(getActivity(), deviceViewModel.getLastUpdateTime(),
                    getString(R.string.date_range_different_year), null);
            updateTimeTextView.setText(getString(R.string.device_detail_last_update, lastUpdateDate));
        } else {
            updateTimeTextView.setText(getString(R.string.device_detail_last_update_unknown));
        }

        Picasso.with(getActivity())
                .load(deviceViewModel.getDeviceImageResource())
                .fit().into(pressImageView);

        boolean alpha = deviceViewModel.getDeviceLastUpdateType() == DeviceLastUpdateType.LAST_UPDATE_IN_24HRS_TODAY
                || deviceViewModel.getDeviceLastUpdateType() == DeviceLastUpdateType.LAST_UPDATE_IN_24HRS_YESTERDAY;

        DevicesUtils.updateProgressBar(getActivity(), progressBar,
                deviceViewModel.getPrintVolumeShiftTargetValue(),
                deviceViewModel.getPrintVolumeIntraDailyTargetValue(),
                deviceViewModel.getPrintVolumeValue(), alpha);

        frontPanelMsg.setVisibility(View.GONE);

        if (hasExtraData && deviceExtraData.getFrontPanelMessage() != null && !deviceExtraData.getFrontPanelMessage().isEmpty()) {
            pressMsgTextView.setText(deviceExtraData.getFrontPanelMessage());
            frontPanelMsg.setVisibility(View.VISIBLE);
        }

        Picasso.with(getActivity())
                .load(PrinterUtils.getPrinterStateImage(deviceViewModel.getState()))
                .fit().into(pressStateImage);

        DevicesUtils.updateTimeInState(getActivity(), deviceViewModel, BusinessUnitEnum.LATEX_PRINTER, timeInStateTextView, pressStateTextView);

        if (deviceViewModel.getState() == DeviceState.NULL) {
            timeInStateTextView.setVisibility(View.GONE);
        }

        printVolumeUnitTextView.setText(Unit.SQM.getUnitText(getContext(), unitSystem, Unit.UnitStyle.DEFAULT));
        nominatorPrintVolumeTextView.setText(HPLocaleUtils.getLocalizedValue((int) deviceViewModel.getPrintVolumeValue()));
        nominatorPrintVolumeTextView.setTextColor(HPScoreUtils.getProgressColor(getActivity(), deviceViewModel.getPrintVolumeValue(), deviceViewModel.getPrintVolumeIntraDailyTargetValue(), false));
        denominatorPrintVolumeTextView.setText(String.format(getString(R.string.press_progress_impression_v2), HPLocaleUtils.getLocalizedValue((int) deviceViewModel.getPrintVolumeShiftTargetValue())));
    }

    private void displayInkInfo() {

        if (!hasExtraData || deviceExtraData.getInkList() == null || deviceExtraData.getInkList().size() == 0) {

            inkNotAvailableView.setVisibility(View.VISIBLE);
            inkList.setVisibility(View.GONE);

        } else {

            InkAdapter inkAdapter = new InkAdapter(getContext(), deviceExtraData.getInkList(), this);
            RecyclerView.LayoutManager inkLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
            inkRecyclerView.setLayoutManager(inkLayoutManager);
            inkRecyclerView.setAdapter(inkAdapter);
            inkNotAvailableView.setVisibility(View.GONE);
            inkList.setVisibility(View.VISIBLE);
        }
    }

    private void displaySubstrateInfo() {

        if (!hasExtraData || deviceExtraData.getSubstrateList() == null || deviceExtraData.getSubstrateList().size() == 0) {
            substrateRecyclerView.setVisibility(View.GONE);
            substrateTitle.setVisibility(View.GONE);
            substrateDataUnavailable.setVisibility(View.VISIBLE);
        } else {
            SubstrateAdapter substrateAdapter = new SubstrateAdapter(getContext(), deviceExtraData.getSubstrateList(), unitSystem);
            RecyclerView.LayoutManager substrateLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
            substrateRecyclerView.setLayoutManager(substrateLayoutManager);
            substrateRecyclerView.setAdapter(substrateAdapter);
            substrateRecyclerView.setVisibility(View.VISIBLE);
            substrateDataUnavailable.setVisibility(View.GONE);
            substrateTitle.setVisibility(View.VISIBLE);
        }
    }

    private void displayJobsInfo() {


        String jobName = deviceViewModel.getCurrentJob();
        float progressValue = deviceViewModel.getCurrentJobProgress();
        int timeToDone = deviceViewModel.getCurrentJobTimeToDone();

        //Job extra data
        List<JobModel> printingJobs = hasExtraData ? deviceExtraData.getPrintingJobs() : null;
        JobModel jobModel = printingJobs != null && printingJobs.size() > 0 ? deviceExtraData.getPrintingJobs().get(0) : null;

        if (jobName != null) {

            currentJobNameTextView.setText(jobName);

            int progressColor = ResourcesCompat.getColor(getActivity().getResources(), R.color.c62, null);
            int trackColor = ResourcesCompat.getColor(getActivity().getResources(), R.color.bar_bg_gray, null);

            DevicesUtils.updateProgressBar(currentJobProgressBar, progressColor, trackColor,
                    CURRENT_JOB_PROGRESS_BAR_MAX_VALUE, 0, progressValue, 0, BAR_CORNER_RADIUS);

            if (timeToDone > -1 && deviceViewModel.getCurrentJobTimeToDoneUnit() != null) {
                currentJobTimeToComplete.setVisibility(View.VISIBLE);
                currentJobTimeToComplete.setText(HPLocaleUtils.getLocalizedTimeString(getActivity(), timeToDone,
                        deviceViewModel.getCurrentJobTimeToDoneUnit()));
            } else {
                currentJobTimeToComplete.setVisibility(View.GONE);
            }

            //Get job thumbnail and status from DeviceStats if possible.
            if (jobModel != null) {

                Picasso.with(getActivity())
                        .load(jobModel.getJobImageUrl())
                        .placeholder(R.drawable.job)
                        .error(R.drawable.job)
                        .fit()
                        .into(currentJobImage);

                if (jobModel.getEndState() != null) {
                    currentJobStatusTextView.setText(getString(jobModel.getEndState().getLocalizedNameID()));
                    int jobStateColor = ResourcesCompat.getColor(getActivity().getResources(),
                            jobModel.getEndState().getStateTextColorResId(), null);
                    currentJobStatusTextView.setTextColor(jobStateColor);
                }
            }

            currentJobDataNotAvailable.setVisibility(View.GONE);
            currentJobContentLayout.setVisibility(View.VISIBLE);

        } else {

            currentJobDataNotAvailable.setVisibility(View.VISIBLE);
            currentJobContentLayout.setVisibility(View.GONE);
        }

        if (jobModel != null) {

            jobsInQueueValueTextView.setText(DevicesUtils.getJobsInQueueString(getActivity(),
                    printingJobs.size()));
        } else {
            jobsInQueueValueTextView.setText(getString(R.string.latex_no_jobs_in_queue));
        }


        if (deviceViewModel.getPrintedJobs() > -1) {
            printedJobsCountTextView.setText(deviceViewModel.getPrintedJobs() == 1 ?
                    getString(R.string.device_detail_completed_jobs_value_one_job) :
                    String.format(getString(R.string.device_detail_completed_jobs_value_jobs),
                            HPLocaleUtils.getLocalizedValue(deviceViewModel.getPrintedJobs())));
            printedJobsCountTextView.setVisibility(View.VISIBLE);
        } else {
            printedJobsCountTextView.setVisibility(View.GONE);
        }

        if (deviceViewModel.getLitersConsumed() > -1) {
            litersConsumedTextView.setText(DevicesUtils.getLitersConsumed(getActivity(), unitSystem, deviceViewModel.getLitersConsumed()));
            litersConsumedTextView.setVisibility(View.VISIBLE);
        } else {
            litersConsumedTextView.setVisibility(View.GONE);
        }

        boolean showPrinted = deviceViewModel.getPrintedJobs() > -1 && deviceViewModel.getLitersConsumed() > -1;

        printedLayout.setVisibility(showPrinted ? View.VISIBLE : View.GONE);
        printedDataNotAvailable.setVisibility(!showPrinted ? View.VISIBLE : View.GONE);

    }

    private void displayPrinterSerialInfo() {

        String printerSerialNumber = deviceViewModel.getSerialNumberDisplay();
        String printerFirmwareVersion = hasExtraData ? deviceExtraData.getFirmwareVersion() : null;

        printerSerialNumberTextView.setText(printerSerialNumber == null ? getString(R.string.unknown_value) : printerSerialNumber);
        printerFirmwareVersionTextView.setText(printerFirmwareVersion == null ? getString(R.string.unknown_value) : printerFirmwareVersion);

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_latex_device_details;
    }

    @Override
    public void onInkSelected(InkModel model) {

        int percentageRemaining = (int) DevicesUtils.getPercentageRemaining(model.getLevel(), model.getCapacity());
        inkNameTextView.setText(String.format(
                getActivity().getString(R.string.device_detail_ink_descriptive_name),
                getActivity().getString(model.getInkEnum().getLocalizedNameId()),
                percentageRemaining));


        String formattedDate = model.getExpiryDate() == null ? "-" :
                HPDateUtils.formatDate(
                        HPDateUtils.parseDate(model.getExpiryDate(), SERVER_EXPIRY_DATE_FORMAT),
                        PrintOSApplication.getAppContext().getString(R.string.date_range_different_year));

        inkDescriptionExpirationTextView.setText(formattedDate);
        inkDescriptionStatusTextView.setText(getActivity().getString(model.getInkState().getLocalizedNameId()));
        inkDescriptionView.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean isFragmentNameToBeDisplayed() {
        return true;
    }

    @Override
    public boolean isFilteringAvailable() {
        return false;
    }

    @Override
    public int getToolbarDisplayName() {
        return R.string.printer_detail;
    }

    @Override
    public boolean isIntercomAccessible() {
        return true;
    }
}
