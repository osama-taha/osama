package com.hp.printosmobile.presentation.modules.personaladvisor;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.TextView;

import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.modules.home.Panel;
import com.hp.printosmobile.presentation.modules.personaladvisor.presenter.PersonalAdvisorFactory;
import com.hp.printosmobile.presentation.modules.shared.DeviceViewModel;
import com.hp.printosmobile.presentation.modules.shared.PanelShareCallbacks;
import com.hp.printosmobile.utils.FileUtils;
import com.hp.printosmobile.utils.HPUIUtils;
import com.hp.printosmobilelib.core.communications.remote.APIException;
import com.hp.printosmobilelib.core.logging.HPLogger;
import com.hp.printosmobilelib.ui.widgets.CustomSlidingAnimation;
import com.hp.printosmobilelib.ui.widgets.HPViewPager;
import com.hp.printosmobilelib.ui.widgets.panel.PanelView;
import com.viewpagerindicator.CirclePageIndicator;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Anwar Asbah on 4/12/2016.
 */
public class PersonalAdvisorPanel extends PanelView<PersonalAdvisorViewModel> implements PersonalAdvisorAdapter.PersonalAdviseCallback, View.OnClickListener, PersonalAdvisorFactory.PersonalAdvisorObserver {


    public static final String TAG = PersonalAdvisorPanel.class.getSimpleName();
    private static final String ADVICE_SCREEN_SHOT_FILE_NAME = "advice_screenshot";
    private static final long CAPTURE_SCREENSHOT_DELAY_MILLI_SECONDS = 200;

    @Bind(R.id.advise_pager)
    HPViewPager advicesPager;
    @Bind(R.id.advice_pager_indicator)
    CirclePageIndicator advicesPagerIndicator;
    @Bind(R.id.button_unhide_all)
    TextView unSuppressAllTextView;
    @Bind(R.id.separator_view)
    View separatorView;

    private ImageView likeButton;
    private ImageView suppressButton;
    private ImageView shareButton;

    PersonalAdvicePanelCallback callback;
    PersonalAdvisorAdapter advisorAdapter;

    int currentAdviceIndex = -1;
    boolean wasPagerScrolled = false;

    public PersonalAdvisorPanel(Context context) {
        super(context);
        bindViews();
    }

    public PersonalAdvisorPanel(Context context, AttributeSet attrs) {
        super(context, attrs);
        bindViews();
    }

    public PersonalAdvisorPanel(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        bindViews();
    }

    private void bindViews() {

        ButterKnife.bind(this, getView());

        View headerLeftView = inflate(getContext(), R.layout.personal_advisor_panel_header_left_view, getHeaderLeftView());
        likeButton = (ImageView) headerLeftView.findViewById(R.id.personal_advisor_like_button);
        suppressButton = (ImageView) headerLeftView.findViewById(R.id.personal_advisor_suppress_button);
        shareButton = (ImageView) headerLeftView.findViewById(R.id.personal_advisor_share_button);

        likeButton.setOnClickListener(this);
        suppressButton.setOnClickListener(this);
        shareButton.setOnClickListener(this);

        final Handler handler = new Handler();

        advicesPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(final int position, float positionOffset, int positionOffsetPixels) {
                if (positionOffset == 0) {
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            advicesPager.measureCurrentView(advicesPager.findViewWithTag(position));
                        }
                    }, 100);
                }
            }

            @Override
            public void onPageSelected(final int position) {
                currentAdviceIndex = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    @Override
    public Spannable getTitleSpannable() {
        SpannableStringBuilder builder = new SpannableStringBuilder(getContext().getString(R.string.personal_advisor_panel_title));
        return builder;
    }

    @Override
    public int getContentView() {
        return R.layout.personal_advisor_content;
    }

    @Override
    public void updateViewModel(final PersonalAdvisorViewModel viewModel) {

        hideLoading();

        setViewModel(viewModel);

        boolean reset = getViewModel() == null;

        if (showEmptyCard()) {
            return;
        }

        if (!reset) {
            if (advisorAdapter != null) {
                advisorAdapter.setViewModel(viewModel);
                advisorAdapter.notifyDataSetChanged();
                setHeaderButtonsAndPagerIndicatorVisibility();
                setUnHideAdvicesButtonVisibility(getViewModel().getAllAdvices().size() != getViewModel().getUnsuppressedAdvices().size());

                if (advisorAdapter != null && advisorAdapter.getCount() > 0 &&
                        (currentAdviceIndex >= advisorAdapter.getCount() || currentAdviceIndex < 0)) {
                    currentAdviceIndex = 0;
                }

                if (!getViewModel().getUnsuppressedAdvices().isEmpty()) {
                    if (currentAdviceIndex >= 0 && currentAdviceIndex < getViewModel().getUnsuppressedAdvices().size()) {
                        updateLikeButton(getViewModel().getUnsuppressedAdvices().get(currentAdviceIndex));
                    }
                }
                return;
            }
        }

        wasPagerScrolled = false;

        advisorAdapter = new PersonalAdvisorAdapter(getContext(), viewModel, this, true);
        advicesPager.setAdapter(advisorAdapter);
        advicesPagerIndicator.setViewPager(advicesPager);
        if (advisorAdapter.getCount() == 0) {
            advicesPagerIndicator.setCurrentItem(-1);
        }

        advisorAdapter.notifyDataSetChanged();

        setHeaderButtonsAndPagerIndicatorVisibility();
        setUnHideAdvicesButtonVisibility(getViewModel().getAllAdvices().size() != getViewModel().getUnsuppressedAdvices().size());

        advicesPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            int prevPosition = 0;

            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                wasPagerScrolled = position != 0 || (position == 0 && wasPagerScrolled);
            }

            @Override
            public void onPageSelected(int position) {

                currentAdviceIndex = position;

                if (callback != null) {
                    callback.onReadMoreClicked();
                }

                if (position != prevPosition) {
                    Analytics.sendEvent(Analytics.MOBILE_PRINT_BEAT_CATEGORY, Analytics.PERSONAL_ADVISOR_ACTION,
                            position > prevPosition ? Analytics.PERSONAL_ADVISOR_SWIPE_LEFT_LABEL : Analytics.PERSONAL_ADVISOR_SWIPE_RIGHT_LABEL);
                    prevPosition = position;
                }

                if (position > -1 && position < getViewModel().getUnsuppressedAdvices().size()) {
                    updateLikeButton(getViewModel().getUnsuppressedAdvices().get(position));
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        advicesPager.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {

                advicesPager.getViewTreeObserver().removeOnGlobalLayoutListener(this);

                if (advicesPager.getAdapter() != null && advicesPager.getAdapter().getCount() > 0) {
                    advicesPager.measureCurrentView(advicesPager.findViewWithTag(0));
                    currentAdviceIndex = 0;
                }

                if (callback != null) {
                    advicesPager.setMaxHeight(getAvailableHeight());
                }

                advicesPager.addCustomAnimationCallback(new CustomSlidingAnimation.CustomAnimationCallback() {
                    @Override
                    public void onAnimationFrame() {
                        if (callback != null && wasPagerScrolled) {
                            callback.onReadMoreClicked();
                        }
                    }
                });
            }
        });

    }

    @Override
    protected boolean isValidViewModel() {
        return !(getViewModel() == null || getViewModel().getAllAdvices() == null || getViewModel().getAllAdvices().size() == 0);
    }

    protected String getEmptyCardText() {
        return getContext().getString(R.string.no_information_to_display);
    }

    @Override
    public void sharePanel() {

        if (callback != null && advisorAdapter != null && advisorAdapter.getCount() > 0) {

            final View adviceView = advicesPager.findViewWithTag(advicesPager.getCurrentItem());

            if (adviceView == null) {
                return;
            }

            lockShareButton(true);

            //In order to share without the "Read more" option , we need first to expand the advice text if it is not expanded.
            AdviceViewModel currentAdvice = getViewModel().getUnsuppressedAdvices()
                    .get(advicesPager.getCurrentItem());

            if (!currentAdvice.getIsReadMoreClicked()) {
                advicesPager.findViewWithTag(advicesPager.getCurrentItem())
                        .findViewById(R.id.read_more_button).performClick();
            }

            //Execute after few milliseconds to make sure that current advice's text is expanded.
            new Handler().postDelayed(new Runnable() {
                                          @Override
                                          public void run() {

                                              boolean pagerIndicatorVisibility = advicesPagerIndicator.isShown();
                                              boolean unSuppressAllVisibility = unSuppressAllTextView.isShown();
                                              boolean headerButtonsVisibility = likeButton.isShown();

                                              //Keep only the advice cell and the panel header before taking the required screenshot.
                                              HPUIUtils.setVisibility(false, advicesPagerIndicator,
                                                      unSuppressAllTextView, separatorView, suppressButton, shareButton, likeButton);

                                              Bitmap adviceViewBitmap = FileUtils.getScreenShot(PersonalAdvisorPanel.this);
                                              Uri storageUri = FileUtils.store(adviceViewBitmap, ADVICE_SCREEN_SHOT_FILE_NAME);

                                              //Return panel to its original state.
                                              HPUIUtils.setVisibilityForViews(pagerIndicatorVisibility, advicesPagerIndicator);
                                              HPUIUtils.setVisibility(unSuppressAllVisibility, unSuppressAllTextView, separatorView);
                                              HPUIUtils.setVisibility(headerButtonsVisibility, likeButton, shareButton, suppressButton);

                                              callback.onScreenshotCreated(Panel.PERSONAL_ADVISOR, storageUri);
                                              lockShareButton(false);

                                          }

                                      }
                    , CAPTURE_SCREENSHOT_DELAY_MILLI_SECONDS);


        }


    }

    public void lockShareButton(boolean lock) {
        shareButton.setEnabled(!lock);
        shareButton.setClickable(!lock);
    }

    private void updateLikeButton(AdviceViewModel adviceViewModel) {
        if (adviceViewModel == null) {
            return;
        }
        likeButton.setImageResource(adviceViewModel.getLiked() ? R.drawable.like_selected : R.drawable.like);
    }

    private void setHeaderButtonsAndPagerIndicatorVisibility() {
        int count = advisorAdapter.getCount();
        HPUIUtils.setVisibilityForViews(count > 1, advicesPagerIndicator);
        HPUIUtils.setVisibility(advisorAdapter.getCount() > 0, advicesPager, likeButton, suppressButton, shareButton);
    }

    private void setUnHideAdvicesButtonVisibility(final boolean visibility) {

        HPUIUtils.setVisibility(visibility, unSuppressAllTextView, separatorView);

        unSuppressAllTextView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {

                unSuppressAllTextView.getViewTreeObserver().removeOnGlobalLayoutListener(this);

                if (callback != null) {
                    advicesPager.setMaxHeight(getAvailableHeight());
                }
            }
        });
    }

    private int getAvailableHeight() {
        if (callback != null) {
            int available = callback.getFragmentHeight()
                    - getContext().getResources().getDimensionPixelSize(R.dimen.panel_view_header_height)
                    - advicesPagerIndicator.getHeight()
                    - (unSuppressAllTextView.getVisibility() == GONE ? 0 : getContext().getResources().getDimensionPixelSize(R.dimen.personal_advisor_unhide_all_button_hight))
                    - 3 * getContext().getResources().getDimensionPixelSize(R.dimen.activity_vertical_margin);
            return available;
        }
        return 0;
    }

    public void addPersonalAdvisorPanelCallback(PersonalAdvicePanelCallback callback) {
        this.callback = callback;
    }

    @Override
    public void onSuppressClicked(AdviceViewModel adviceViewModel) {
        if (adviceViewModel == null) {
            return;
        }
        PersonalAdvisorFactory.getInstance().suppressAdvice(adviceViewModel);
    }

    @Override
    public void onLikeClicked(AdviceViewModel adviceViewModel) {
        if (adviceViewModel == null) {
            return;
        }
        PersonalAdvisorFactory.getInstance().likeAdvice(adviceViewModel,
                !adviceViewModel.getLiked());
    }

    @OnClick(R.id.button_unhide_all)
    void onUnhideAllAdvicesClicked() {
        PersonalAdvisorFactory.getInstance().unhideAllAdvices();
    }

    @Override
    public void onReadMoreClicked(View view) {
        advicesPager.measureCurrentView(view);
        if (callback != null) {
            callback.onReadMoreClicked();
        }
    }

    @Override
    public void onClick(View view) {

        if (advicesPager == null || advicesPager.getCurrentItem() < 0 ||
                advicesPager.getCurrentItem() > getViewModel().getUnsuppressedAdvices().size() - 1) {
            return;
        }

        AdviceViewModel currentAdvice = getViewModel().getUnsuppressedAdvices().get(advicesPager.getCurrentItem());

        switch (view.getId()) {
            case R.id.personal_advisor_like_button:
                onLikeClicked(currentAdvice);
                break;
            case R.id.personal_advisor_suppress_button:
                onSuppressClicked(currentAdvice);
                break;
            case R.id.personal_advisor_share_button:
                onShareClicked();
                break;
        }
    }

    private void onShareClicked() {

        callback.onShareButtonClicked(Panel.PERSONAL_ADVISOR);

    }

    public void scrollToAdvice(DeviceViewModel model, AdviceViewModel targetAdviceViewModel) {

        int adviceIndex = -1;

        if (getViewModel() == null || getViewModel().getUnsuppressedAdvices() == null) {
            return;
        }

        if (targetAdviceViewModel == null) {
            if (model != null) {
                String device = model.getSerialNumber();
                for (AdviceViewModel adviceViewModel : getViewModel().getUnsuppressedAdvices()) {
                    if (adviceViewModel.getDeviceSerialNumber().equals(device)) {
                        adviceIndex = getViewModel().getUnsuppressedAdvices().indexOf(adviceViewModel);
                        break;
                    }
                }
            }
        } else {
            for (AdviceViewModel adviceViewModel : getViewModel().getUnsuppressedAdvices()) {
                if (adviceViewModel.getAdviseId() == targetAdviceViewModel.getAdviseId() &&
                        adviceViewModel.getDeviceSerialNumber().equals(targetAdviceViewModel.getDeviceSerialNumber())) {
                    adviceIndex = getViewModel().getUnsuppressedAdvices().indexOf(adviceViewModel);
                    break;
                }
            }
        }

        if (adviceIndex > -1) {
            advicesPager.setCurrentItem(adviceIndex, true);
        }
    }

    public void enableAdviceScrolling(boolean enableScrolling) {
        if (currentAdviceIndex >= 0) {
            View adviceView = advicesPager.findViewWithTag(
                    currentAdviceIndex);
            if (adviceView != null) {
                NestedScrollView contentTextViewScrollView = (NestedScrollView) adviceView.findViewById(R.id.scroll_view);
                contentTextViewScrollView.setNestedScrollingEnabled(enableScrolling);
            }
        }
    }

    public void scrollToNewestAdvice() {

        int adviceIndex = -1;

        if (getViewModel() == null || getViewModel().getUnsuppressedAdvices() == null) {
            return;
        }

        for (AdviceViewModel adviceViewModel : getViewModel().getUnsuppressedAdvices()) {
            if (adviceViewModel.isNew()) {
                adviceIndex = getViewModel().getUnsuppressedAdvices().indexOf(adviceViewModel);
                break;
            }
        }

        if (adviceIndex > -1) {
            advicesPager.setCurrentItem(adviceIndex, true);
        }
    }

    @Override
    public void updatePersonalAdvisorObserver() {
        HPLogger.d(TAG, "updating Personal Advisor Observer");
        PersonalAdvisorViewModel personalAdvisorViewModel = PersonalAdvisorFactory.getInstance().getFilteredAdvises();
        if (personalAdvisorViewModel != null) {
            updateViewModel(personalAdvisorViewModel);
        }
        if (callback != null) {
            callback.updatePersonalAdvisorPanel(personalAdvisorViewModel);
        }
    }

    @Override
    public void onPersonalAdvisorError(APIException e, String tag) {
        if (callback != null) {
            callback.onPersonalAdvisorError(e, TAG);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        PersonalAdvisorFactory.getInstance().removeObserver(this);
    }

    public interface PersonalAdvicePanelCallback extends PanelShareCallbacks {

        void onReadMoreClicked();

        int getFragmentHeight();

        void updatePersonalAdvisorPanel(PersonalAdvisorViewModel viewModel);

        void onPersonalAdvisorError(APIException e, String tag);

    }
}
