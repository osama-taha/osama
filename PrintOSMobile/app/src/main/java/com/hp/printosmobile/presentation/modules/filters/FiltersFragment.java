package com.hp.printosmobile.presentation.modules.filters;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.hp.printosmobile.IntercomSdk;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;
import com.hp.printosmobile.utils.HPUIUtils;
import com.hp.printosmobilelib.ui.common.HPFragment;
import com.hp.printosmobilelib.ui.widgets.HPViewPager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.OnClick;


/**
 * Created by Osama Taha on 10/9/16.
 */
public class FiltersFragment extends HPFragment implements TabLayout.OnTabSelectedListener, FiltersListFragment.FiltersListFragmentCallbacks {


    private static final String TAG = FiltersFragment.class.getSimpleName();

    private static final String ARG_FILTERS_VIEW_MODEL = "filters_view_model_param";
    private static final String ARG_BUSINESS_UNIT = "arg_business_unit";
    private static final int INDEX_NOT_FOUND = -1;
    private static final long SET_SELECTED_SITE_DELAY_IN_MILLS = 1000;

    @Bind(R.id.sliding_tabs)
    TabLayout tabLayout;
    @Bind(R.id.viewpager)
    HPViewPager viewPager;

    private FiltersFragmentCallbacks mListener;
    private FiltersViewModel mFiltersViewModel;
    private BusinessUnitEnum businessUnitEnum;
    private MainPagerAdapter mAdapter;

    private int sitesFragmentIndex = INDEX_NOT_FOUND;
    private int groupsFragmentIndex = INDEX_NOT_FOUND;
    private int devicesFragmentIndex = INDEX_NOT_FOUND;
    private boolean isFilterUpdated;

    private List<FilterItem> prevSelectedSites;
    private List<FilterItem> prevSelectedGroups;
    private List<FilterItem> prevSelectedDevices;

    public FiltersFragment() {
        // Required empty public constructor
    }

    public static FiltersFragment newInstance(FiltersViewModel filtersViewModel, BusinessUnitEnum businessUnitEnum) {
        FiltersFragment fragment = new FiltersFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_FILTERS_VIEW_MODEL, filtersViewModel);
        args.putString(ARG_BUSINESS_UNIT, businessUnitEnum.getName());
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (getArguments() != null) {
            mFiltersViewModel = (FiltersViewModel) getArguments().getSerializable(ARG_FILTERS_VIEW_MODEL);
            businessUnitEnum = BusinessUnitEnum.from(getArguments().getString(ARG_BUSINESS_UNIT));
        }

        initView();

    }

    private void initView() {
        isFilterUpdated = false;
        setupViewPager();
    }


    private void setupViewPager() {

        mAdapter = new MainPagerAdapter(getChildFragmentManager());


        //If user has one site, the Site section will not be displayed.
        if (mFiltersViewModel.getSites().size() > 1) {
            sitesFragmentIndex = mAdapter.addFragment(FiltersListType.SITES, FiltersListFragment.newInstance(mFiltersViewModel.getSites()));
        }

        //If user has not groups, the groups section will not be displayed.
        if (mFiltersViewModel.getGroups().size() > 0) {
            groupsFragmentIndex = mAdapter.addFragment(FiltersListType.GROUPS, FiltersListFragment.newInstance(mFiltersViewModel.getGroups()));
        }

        final FiltersListFragment devicesListFragment = FiltersListFragment.newInstance(addAllDevicesItemToDevicesList(mFiltersViewModel.getDevices()));
        devicesFragmentIndex = mAdapter.addFragment(FiltersListType.DEVICES, devicesListFragment);

        //If user has only one site and didn't define groups,
        // the filter panel will automatically display all of the presses on site.
        if (devicesFragmentIndex == 0) {
            tabLayout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 0));
        }

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        viewPager.setScrollingEnabled(false);
        viewPager.setAdapter(mAdapter);
        viewPager.setOffscreenPageLimit(mAdapter.getCount());

        tabLayout.setupWithViewPager(viewPager);
        tabLayout.addOnTabSelectedListener(this);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                FiltersListFragment fragment = getFilterFragment(sitesFragmentIndex);
                if (fragment != null) {
                    FilterItem filterItem = mFiltersViewModel.getSelectedSite();
                    fragment.selectItems(Collections.singletonList(filterItem));
                    onItemSelected(true, filterItem);
                } else {
                    //set all as selected
                    List<FilterItem> selectedItems = devicesListFragment.getItems();
                    if (!isListEmpty(selectedItems)) {
                        DeviceFilterViewModel allDevicesItem = (DeviceFilterViewModel) selectedItems.get(0);
                        if (allDevicesItem.isAllDevices()) {
                            devicesListFragment.manageSelection(allDevicesItem, true);
                        }
                    }
                }
            }
        }, SET_SELECTED_SITE_DELAY_IN_MILLS);

    }

    public List<FilterItem> addAllDevicesItemToDevicesList(List<FilterItem> devices) {

        if (devices == null) {

            return null;

        } else if (devices.size() <= 1) {

            return devices;

        } else {

            List<FilterItem> devicesWithAllItem = new ArrayList<>();

            DeviceFilterViewModel allDevicesItem = new DeviceFilterViewModel();
            if (getActivity() != null) {
                allDevicesItem.setDeviceName(getActivity().getString(R.string.filters_all_site));
            }

            allDevicesItem.setSerialNumber("");
            allDevicesItem.setAllDevices(true);

            devicesWithAllItem.add(allDevicesItem);
            devicesWithAllItem.addAll(devices);

            return devicesWithAllItem;

        }

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_filters;
    }

    @Override
    public boolean isFragmentNameToBeDisplayed() {
        return false;
    }

    @Override
    public boolean isFilteringAvailable() {
        return false;
    }

    @Override
    public int getToolbarDisplayName() {
        return 0;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FiltersFragmentCallbacks) {
            mListener = (FiltersFragmentCallbacks) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement FiltersFragmentCallbacks");

        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @OnClick(R.id.filters_cancel_button)
    public void onCancelFiltersClicked() {

        HPUIUtils.statusBarColor(getActivity(), R.color.colorPrimary);

        isFilterUpdated = false;
        mListener.onCancelFiltersSelected();
        resetFilters();
    }

    public void resetFilters() {
        setFilterFragmentSelection(sitesFragmentIndex, prevSelectedSites, true);
        setFilterFragmentSelection(groupsFragmentIndex, prevSelectedGroups, false);
        setFilterFragmentSelection(devicesFragmentIndex, prevSelectedDevices, false);

        //set all as selected
        FiltersListFragment devicesListFragment = getFilterFragment(devicesFragmentIndex);

        if (devicesListFragment == null || devicesListFragment.isEmpty()) {
            return;
        }

        FilterItem allDevicesItem = devicesListFragment.getItems().get(0);
        if (allDevicesItem instanceof DeviceFilterViewModel) {
            if (((DeviceFilterViewModel) allDevicesItem).isAllDevices() && (sitesFragmentIndex != INDEX_NOT_FOUND || groupsFragmentIndex != INDEX_NOT_FOUND)) {
                List<FilterItem> selectedFilterItems = devicesListFragment.getSelectedItems();
                devicesListFragment.manageSelection(allDevicesItem, selectedFilterItems == null || selectedFilterItems.size() == 0);
            }
        }
    }

    private void setFilterFragmentSelection(int index, List<FilterItem> filterItems, boolean notifySelection) {
        FiltersListFragment filtersListFragment = getFilterFragment(index);
        if (filtersListFragment != null) {
            filtersListFragment.selectItems(filterItems);
            if (notifySelection && !isListEmpty(filterItems)) {
                onItemSelected(true, filterItems.get(0));
            }
        }
    }

    @OnClick(R.id.filters_apply_button)
    void onApplyFiltersClicked() {

        if (mFiltersViewModel == null || !isFilterUpdated || mAdapter == null) {
            mListener.onApplyFiltersSelected(null);
            return;
        }

        IntercomSdk.getInstance(PrintOSApplication.getAppContext()).logEvent(IntercomSdk.PBM_FILTER_APPLY_EVENT);

        isFilterUpdated = false;

        FiltersListFragment sitesListFragment = getFilterFragment(sitesFragmentIndex);
        FiltersListFragment groupsListFragment = getFilterFragment(groupsFragmentIndex);
        FiltersListFragment devicesListFragment = getFilterFragment(devicesFragmentIndex);

        SiteViewModel selectedSite = null;
        if (sitesListFragment != null) {
            if (!isListEmpty(sitesListFragment.getSelectedItems())) {
                selectedSite = (SiteViewModel) sitesListFragment.getSelectedItems().get(0);
            }
        } else {
            if (mFiltersViewModel != null && !isListEmpty(mFiltersViewModel.getSites())) {
                selectedSite = (SiteViewModel) mFiltersViewModel.getSites().get(0);
            }
        }

        //should not happen unless not filter data
        if (selectedSite == null) {
            if (mListener != null) {
                mListener.onApplyFiltersSelected(null);
            }
            return;
        }

        mFiltersViewModel.setSelectedSite(selectedSite);

        if (groupsListFragment != null) {
            //If user selects a group then, then the take all presses for this group.
            List<FilterItem> selectedGroups = groupsListFragment.getSelectedItems();
            if (!isListEmpty(selectedGroups)) {
                mListener.onApplyFiltersSelected(selectedGroups.get(0).getItems());
                return;
            }
        }

        //If user select single or multiple presses then take all these selected presses regardless the site or group.

        if (devicesListFragment == null) {
            mListener.onApplyFiltersSelected(null);
            return;
        }

        List<FilterItem> selectedDevices = devicesListFragment.getSelectedItems();

        if (devicesListFragment.getSelectedItems().size() > 0) {

            if (selectedDevices.size() == 1 && ((DeviceFilterViewModel) selectedDevices.get(0)).isAllDevices()) {

                if (sitesListFragment == null) {
                    mListener.onApplyFiltersSelected(mFiltersViewModel.getDevices());
                } else {
                    SiteViewModel site = (SiteViewModel) sitesListFragment.getSelectedItems().get(0);
                    mListener.onApplyFiltersSelected(site.getItems());
                }

            } else {
                mListener.onApplyFiltersSelected(devicesListFragment.getSelectedItems());
            }

        } else if (sitesListFragment != null && !isListEmpty(sitesListFragment.getSelectedItems())) {
            //Otherwise, return all devices for the selected site.
            mListener.onApplyFiltersSelected(sitesListFragment.getSelectedItems().get(0).getItems());
        } else {
            mListener.onApplyFiltersSelected(mFiltersViewModel.getDevices());
        }

    }

    private FiltersListFragment getFilterFragment(int index) {
        return mAdapter == null || index == INDEX_NOT_FOUND || index < 0 || index >= mAdapter.getCount() ? null
                : (FiltersListFragment) mAdapter.getItem(index);

    }

    private boolean isListEmpty(List list) {
        return list == null || list.size() == 0;
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewPager.setCurrentItem(tab.getPosition());
        viewPager.requestLayout();
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {
    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    @Override
    public void onItemSelected(boolean selected, FilterItem filterItem) {

        isFilterUpdated = true;

        FiltersListFragment sitesListFragment = getFilterFragment(sitesFragmentIndex);
        FiltersListFragment groupsListFragment = getFilterFragment(groupsFragmentIndex);
        FiltersListFragment devicesListFragment = getFilterFragment(devicesFragmentIndex);

        if (filterItem instanceof SiteViewModel) {

            SiteViewModel site = (SiteViewModel) filterItem;

            if (groupsListFragment != null) {
                groupsListFragment.updateView(site.getGroups());
            }

            if (devicesListFragment != null) {
                devicesListFragment.updateView(addAllDevicesItemToDevicesList(site.getDevices()));

                //set all as selected
                List<FilterItem> filterItems = devicesListFragment.getItems();
                if (!isListEmpty(filterItems)) {
                    DeviceFilterViewModel allDevicesItem = (DeviceFilterViewModel) filterItems.get(0);
                    if (allDevicesItem.isAllDevices()) {
                        devicesListFragment.manageSelection(allDevicesItem, true);
                    }
                }
            }

            if (sitesListFragment != null) {
                sitesListFragment.manageSelection(filterItem, selected);
            }

        } else if (filterItem instanceof GroupViewModel) {

            GroupViewModel group = (GroupViewModel) filterItem;

            if (selected) {
                //if user selects a group, then all devices associated with this group are selected.
                if (devicesListFragment != null) {
                    devicesListFragment.selectItems(group.getDevices());
                }

                if (groupsFragmentIndex > 0 && groupsListFragment != null) {
                    groupsListFragment.manageSelection(filterItem, true);
                }

            } else {

                if (groupsFragmentIndex > 0 && groupsListFragment != null) {
                    groupsListFragment.clearSelection();
                }

                if (devicesListFragment != null) {
                    devicesListFragment.clearSelection();
                }
            }

        } else if (filterItem instanceof DeviceFilterViewModel && devicesListFragment != null) {

            DeviceFilterViewModel deviceViewModel = (DeviceFilterViewModel) filterItem;

            if (deviceViewModel.isAllDevices()) {
                // Selecting 'All' items should deselect all other selections.
                devicesListFragment.clearSelection();
            } else {
                // Selecting any device should reset the selection of 'All' item.
                if (!isListEmpty(devicesListFragment.getItems())) {
                    DeviceFilterViewModel allDevicesItem = (DeviceFilterViewModel) devicesListFragment.getItems().get(0);

                    List<FilterItem> selectedItems = devicesListFragment.getSelectedItems();
                    if (allDevicesItem.isAllDevices()) {
                        devicesListFragment.manageSelection(allDevicesItem,
                                !selected && (selectedItems == null || selectedItems.size() <= 1));
                    }
                }
            }

            //if user added or removed any press in the Presses section, then these presses become individual multi-selected presses.
            devicesListFragment.manageSelection(filterItem, selected);
            //The group(previously selected) is automatically not selected anymore.
            if (groupsFragmentIndex > 0 && groupsListFragment != null) {
                groupsListFragment.clearSelection();
            }
        }

    }

    private class MainPagerAdapter extends FragmentStatePagerAdapter {

        private final Map<FiltersListType, Fragment> fragmentHashMap = new HashMap<>();
        private final List<FiltersListType> fragmentTypesList = new ArrayList<>();

        MainPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        int addFragment(FiltersListType type, Fragment fragment) {
            fragmentHashMap.put(type, fragment);
            fragmentTypesList.add(type);
            return fragmentTypesList.indexOf(type);
        }

        @Override
        public Fragment getItem(int position) {
            return fragmentHashMap.get(fragmentTypesList.get(position));
        }

        @Override
        public boolean isViewFromObject(View view, Object fragment) {
            return ((Fragment) fragment).getView() == view;
        }

        /**
         * Here we can safely save a reference to the created
         * Fragment, no matter where it came from (either getItem() or
         * FragmentManager).
         */
        @Override
        public Object instantiateItem(ViewGroup container, int position) {

            Fragment fragment = (Fragment) super.instantiateItem(container, position);

            if (fragment instanceof FiltersListFragment) {

                switch (fragmentTypesList.get(position)) {
                    case SITES:
                        sitesFragmentIndex = position;
                        break;
                    case GROUPS:
                        groupsFragmentIndex = position;
                        break;
                    case DEVICES:
                        devicesFragmentIndex = position;
                        break;
                }

            }

            return fragment;
        }

        @Override
        public int getCount() {
            return fragmentTypesList.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return getString((businessUnitEnum == BusinessUnitEnum.LATEX_PRINTER) ?
                    fragmentTypesList.get(position).getAlternativeResourceId()
                    : fragmentTypesList.get(position).getTitleResourceId());
        }

    }

    public void onHideFilter() {

        HPUIUtils.statusBarColor(getActivity(), R.color.colorPrimary);

        if (mListener != null) {
            mListener.onFilterHideDone();
        }
    }

    public void onShowFilter() {

        HPUIUtils.statusBarColor(getActivity(), R.color.semi_transparent);

        FiltersListFragment sitesListFragment = getFilterFragment(sitesFragmentIndex);
        FiltersListFragment groupsListFragment = getFilterFragment(groupsFragmentIndex);
        FiltersListFragment devicesListFragment = getFilterFragment(devicesFragmentIndex);

        prevSelectedSites = sitesListFragment != null ? sitesListFragment.getSelectedItems() : null;
        prevSelectedGroups = groupsListFragment != null ? groupsListFragment.getSelectedItems() : null;
        prevSelectedDevices = devicesListFragment != null ? devicesListFragment.getSelectedItems() : null;
    }

    @OnClick(R.id.touch_outside)
    public void onOutsideFilterClicked() {

        HPUIUtils.statusBarColor(getActivity(), R.color.colorPrimary);

        if (mListener != null) {
            mListener.requestFilterHiding();
        }
        resetFilters();
    }

    public interface FiltersFragmentCallbacks {

        void onCancelFiltersSelected();

        void onApplyFiltersSelected(List<FilterItem> selectedItems);

        void requestFilterHiding();

        void onFilterHideDone();
    }

    public enum FiltersListType {

        SITES(R.string.filters_sites_section_title, R.string.filters_sites_section_title),
        GROUPS(R.string.filters_groups_section_title, R.string.filters_groups_section_title),
        DEVICES(R.string.filters_presses_section_title, R.string.printers);

        private final int titleResourceId;
        private final int alternativeResourceId;

        FiltersListType(int titleResourceId, int alternativeResourceId) {
            this.titleResourceId = titleResourceId;
            this.alternativeResourceId = alternativeResourceId;
        }

        public int getTitleResourceId() {
            return titleResourceId;
        }

        public int getAlternativeResourceId() {
            return alternativeResourceId;
        }
    }
}
