package com.hp.printosmobile.presentation.modules.reports;


/**
 * The report's container activity or fragment should implement this interface in order manage the report functionalities.
 *
 * @Author : Osama Taha
 */
public interface PerformanceChartBuilderCallbacks {

    void onChartInitialized(ReportKpiViewModel kpiToDisplay);

    void onKpiSelected(ReportKpiViewModel kpi);

    void onLegendViewLongClicked(ReportKpiViewModel kpi);

    void onPeriodChanged(int lowestVisibleWeek, int highestVisibleWeek);
}
