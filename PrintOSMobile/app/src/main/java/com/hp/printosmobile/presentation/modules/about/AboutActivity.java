package com.hp.printosmobile.presentation.modules.about;

import android.content.ClipboardManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.BuildConfig;
import com.hp.printosmobile.Constants;
import com.hp.printosmobile.IntercomSdk;
import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.BaseActivity;
import com.hp.printosmobile.presentation.modules.eula.EulaActivity;
import com.hp.printosmobile.utils.HPUIUtils;
import com.hp.printosmobilelib.core.logging.HPLogger;
import com.hp.printosmobilelib.core.utils.DeviceUtils;

import butterknife.Bind;
import butterknife.OnClick;

public class AboutActivity extends BaseActivity {

    private static final CharSequence DEVICE_TOKEN = "Device Token";
    private static final String TAG = AboutActivity.class.getSimpleName();

    @Bind(R.id.tvAboutAppVersion)
    TextView appVersionNameTextView;
    @Bind(R.id.tvAboutAppName)
    TextView appNameTextView;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.toolbar_display_title)
    TextView toolbarDisplayName;
    @Bind(R.id.device_token_text_view)
    TextView deviceTokenTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        IntercomSdk.getInstance(this).logEvent(IntercomSdk.PBM_VIEW_ABOUT_EVENT);

        initView();

    }

    private void initView() {

        appNameTextView.setText(R.string.app_name);
        appVersionNameTextView.setText(getString(R.string.about_version, BuildConfig.VERSION_NAME));

        String token = DeviceUtils.getDeviceIdMD5(this);
        if (token != null) {
            deviceTokenTextView.setText(getString(R.string.about_device_token, DeviceUtils.getDeviceIdMD5(this)));
        }
        deviceTokenTextView.setVisibility(token == null ? View.GONE : View.VISIBLE);


        toolbarDisplayName.setText(getString(R.string.about_title));
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(ResourcesCompat.getDrawable(getResources(), R.drawable.back_button, null));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_about;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @OnClick(R.id.device_token_text_view)
    public void onDeviceTokenTextViewClicked() {

        String token = DeviceUtils.getDeviceIdMD5(this);
        if (token == null) {
            return;
        }

        HPLogger.d(TAG, "user clicked token - <"+ token +">");

        ClipboardManager clipboardManager = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
        final android.content.ClipData clipData = android.content.ClipData
                .newPlainText(DEVICE_TOKEN, token);
        clipboardManager.setPrimaryClip(clipData);

        HPUIUtils.displayToast(this, getString(R.string.about_screen_device_token_copy_msg), false);
    }

    @OnClick(R.id.privacy_button)
    public void openPrivacy() {
        Analytics.sendEvent(Analytics.MOBILE_PRINT_BEAT_CATEGORY, Analytics.OPEN_SOURCE_CLICKED_ACTION);

        Intent intent = new Intent(this, EulaActivity.class);
        Bundle bundle = new Bundle();
        bundle.putBoolean(Constants.IntentExtras.EULA_ACTIVITY_TO_ACCEPT_TERMS, false);
        intent.putExtras(bundle);
        startActivity(intent);
    }

    @Override
    public boolean isBackwardCompatible() {
        return true;
    }
}
