package com.hp.printosmobile.presentation.modules.main;

/**
 * Created by Anwar Asbah on 10/6/2016.
 */
public enum ReportTypeEnum {
    COUNT("count"),
    VALUE("value"),
    VALUES("values"),
    RATE("rate"),
    WEIGHT("weight"),
    GUIDELINES_PERCENT("guidelinesPercent"),
    UNKNOWN("");

    private String key;

    ReportTypeEnum(String key) {
        this.key = key;
    }

    public static ReportTypeEnum from(String key) {
        for (ReportTypeEnum type : ReportTypeEnum.values()) {
            if (key.toLowerCase().equals(type.key.toLowerCase()))
                return type;
        }
        return ReportTypeEnum.UNKNOWN;
    }
}
