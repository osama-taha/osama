package com.hp.printosmobile.presentation.modules.beatcoins;

import java.io.Serializable;

/**
 * Created by Anwar on 7/18/2017.
 */

public class BeatCoinsViewModel implements Serializable {
    private String url;
    private int numberOfBeatCoins;
    private int numberOfDaysTillExpiry;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getNumberOfBeatCoins() {
        return numberOfBeatCoins;
    }

    public void setNumberOfBeatCoins(int numberOfBeatCoins) {
        this.numberOfBeatCoins = numberOfBeatCoins;
    }

    public int getNumberOfDaysTillExpiry() {
        return numberOfDaysTillExpiry;
    }

    public void setNumberOfDaysTillExpiry(int numberOfDaysTillExpiry) {
        this.numberOfDaysTillExpiry = numberOfDaysTillExpiry;
    }
}
