package com.hp.printosmobile.data.remote.services;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.hp.printosmobile.data.remote.models.PreferencesData;
import com.hp.printosmobile.data.remote.models.TargetData;
import com.hp.printosmobile.data.remote.models.WeekData;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by osama on 5/20/17.
 */
public class WeeklyDataV2 {

    @JsonProperty("data")
    private List<WeekData> data;
    @JsonProperty("unitSystem")
    private PreferencesData.UnitSystem unitSystem;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("data")
    public List<WeekData> getData() {
        return data;
    }

    @JsonProperty("data")
    public void setData(List<WeekData> data) {
        this.data = data;
    }

    @JsonProperty("unitSystem")
    public PreferencesData.UnitSystem getUnitSystem() {
        return unitSystem;
    }

    @JsonProperty("unitSystem")
    public void setUnitSystem(PreferencesData.UnitSystem unitSystem) {
        this.unitSystem = unitSystem;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
}
