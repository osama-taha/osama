package com.hp.printosmobile.presentation.modules.insights;

import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.data.remote.ApiServicesProvider;
import com.hp.printosmobile.data.remote.models.InsightData;
import com.hp.printosmobile.data.remote.services.InsightsService;
import com.hp.printosmobile.presentation.modules.insights.InsightsViewModel.InsightKpiEnum;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitViewModel;
import com.hp.printosmobile.utils.HPUIUtils;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;
import rx.Observable;
import rx.functions.Func1;

/**
 * Created by Anwar Asbah on 6/20/17.
 */
public class InsightsDataManager {

    private static final int TOP = 5;
    private static final String TO_BE_REPLACE_REGEX = "_";
    private static final String REPLACING_REGEX = " ";

    public static Observable<InsightsViewModel> getInsightsData
            (final BusinessUnitViewModel businessUnitViewModel, final InsightKpiEnum kpi, String fromDate, String toDate) {

        ApiServicesProvider serviceProvider = PrintOSApplication.getApiServicesProvider();
        InsightsService insightsService = serviceProvider.getInsightsService();
        List<String> serialNumbers = businessUnitViewModel.getFiltersViewModel().getSerialNumbers();

        return insightsService.getInsightData(serialNumbers, fromDate, toDate, kpi.getTAG(), TOP).map(new Func1<Response<InsightData>, InsightsViewModel>() {
            @Override
            public InsightsViewModel call(Response<InsightData> insightData) {
                if (insightData.isSuccessful() && insightData.body() != null && insightData.body().getInsightList() != null) {
                    return mapInsightsData(businessUnitViewModel, kpi, insightData.body().getInsightList());
                }
                return null;
            }
        });
    }

    private static InsightsViewModel mapInsightsData(BusinessUnitViewModel bu, InsightKpiEnum kpi, List<InsightData.Insight> insightDataList) {

        InsightsViewModel insightsViewModel = new InsightsViewModel();
        List<InsightsViewModel.InsightModel> insightModels = new ArrayList<>();

        for (int i = 0; i < insightDataList.size(); i++) {
            InsightData.Insight data = insightDataList.get(i);
            if (data != null) {
                InsightsViewModel.InsightModel model = new InsightsViewModel.InsightModel();

                String label = data.getEventName();
                label = label == null ? "" : label;
                label = label.replace(TO_BE_REPLACE_REGEX, REPLACING_REGEX);
                model.setLabel(label);

                model.setPercentage(data.getPercentage());
                model.setOccurrences(data.getOccurrences());
                model.setColor(HPUIUtils.getInsightColor(i));
                model.setInsightCursorDrawableID(HPUIUtils.getInsightCursourDrawabel(i));

                insightModels.add(model);
            }
        }

        insightsViewModel.setInsightModels(insightModels);
        insightsViewModel.setInsightKpiEnum(kpi);
        insightsViewModel.setSelectedDeviceCount(bu.getFiltersViewModel().getSelectedDevices().size());
        insightsViewModel.setTotalDeviceCount(bu.getFiltersViewModel().getSiteDevicesIds().size());

        return insightsViewModel;
    }
}