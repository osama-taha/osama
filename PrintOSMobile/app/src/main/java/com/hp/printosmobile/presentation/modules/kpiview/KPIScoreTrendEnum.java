package com.hp.printosmobile.presentation.modules.kpiview;

/**
 * An enum represents the kpi score trend.
 * <p/>
 * Created by Osama Taha on 1/17/16.
 */
public enum KPIScoreTrendEnum {

    EMPTY(""), INCREASE("INCREASE"), DECREASE("DECREASE"), EQUAL("EQUAL");

    private String state;

    KPIScoreTrendEnum(String state) {
        this.state = state;
    }

    public String getState() {
        return state;
    }

    public static KPIScoreTrendEnum from(String state) {
        for (KPIScoreTrendEnum kpiState : KPIScoreTrendEnum.values()) {
            if (kpiState.getState().equals(state))
                return kpiState;
        }
        return KPIScoreTrendEnum.EMPTY;
    }

}
