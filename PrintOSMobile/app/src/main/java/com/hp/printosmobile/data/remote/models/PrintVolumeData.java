package com.hp.printosmobile.data.remote.models;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class PrintVolumeData {

    @JsonProperty("value")
    private Double value;
    @JsonProperty("day")
    private Integer day;
    @JsonProperty("hour")
    private Integer hour;
    @JsonProperty("lastStateChangedTimeStamp")
    private String lastStateChangedTimeStamp;
    @JsonProperty("timeInStateMin")
    private int timeInStateMin;
    @JsonProperty("lastSeenTimeStamp")
    private String lastSeenTimeStamp;
    @JsonProperty("lastSeenMin")
    private Integer lastSeenMin;
    @JsonProperty("state")
    private String state;
    @JsonProperty("maintenanceState")
    private String maintenanceState;
    @JsonProperty("device")
    private String device;
    @JsonProperty("sheets")
    private Integer sheets;
    @JsonProperty("timeToDone")
    private TimeToDone timeToDone;
    @JsonProperty("meters")
    private Double meters;
    @JsonProperty("squareMeters")
    private Double squareMeters;
    @JsonProperty("printedJobs")
    private Integer printedJobs;
    @JsonProperty("jobsInQueue")
    private Integer jobsInQueue;
    @JsonProperty("currentJob")
    private String currentJob;
    @JsonProperty("currentJobTimeToDone")
    private TimeToDone currentJobTimeToDone;
    @JsonProperty("currentJobProgress")
    private float currentJobProgress;
    @JsonProperty("currentSheet")
    private Integer currentSheet;
    @JsonProperty("totalSheet")
    private Integer totalSheet;
    @JsonProperty("date")
    private String date;
    @JsonProperty("lastUpdateLfp")
    private String lastUpdateLfp;
    @JsonProperty("impressionType")
    private String impressionType;
    @JsonProperty("underWarranty")
    private Boolean underWarranty;
    @JsonProperty("values")
    private List<ImpressionValue> values = new ArrayList<>();
    @JsonProperty("shiftDataRespond")
    private ShiftData shiftData;
    @JsonProperty("nextTenJobs")
    private List<job> nextTenJobs;
    @JsonProperty("lastPrintedJobs")
    private List<job> lastPrintedJobs;
    @JsonProperty("litersConsumed")
    private Double litersConsumed;
    @JsonProperty("pressState")
    private PressState pressState;
    @JsonProperty("isTimeSynced")
    private Boolean isTimeSynced;

    @JsonProperty("isTimeSynced")
    public Boolean getTimeSynced() {
        return isTimeSynced;
    }

    @JsonProperty("isTimeSynced")
    public void setTimeSynced(Boolean isTimeSynced) {
        this.isTimeSynced = isTimeSynced;
    }

    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<>();

    @JsonProperty("nextTenJobs")
    public List<job> getNextTenJobs() {
        return nextTenJobs;
    }

    @JsonProperty("nextTenJobs")
    public void setNextTenJobs(List<job> nextTenJobs) {
        this.nextTenJobs = nextTenJobs;
    }

    /**
     * @return The value
     */
    @JsonProperty("value")
    public Double getValue() {
        return value;
    }

    /**
     * @param value The value
     */
    @JsonProperty("value")
    public void setValue(Double value) {
        this.value = value;
    }

    /**
     * @return The day
     */
    @JsonProperty("day")
    public Integer getDay() {
        return day;
    }

    /**
     * @param day The day
     */
    @JsonProperty("day")
    public void setDay(Integer day) {
        this.day = day;
    }

    /**
     * @return The hour
     */
    @JsonProperty("hour")
    public Integer getHour() {
        return hour;
    }

    /**
     * @param hour The hour
     */
    @JsonProperty("hour")
    public void setHour(Integer hour) {
        this.hour = hour;
    }

    /**
     * @return The lastStateChangedTimeStamp
     */
    @JsonProperty("lastStateChangedTimeStamp")
    public Object getLastStateChangedTimeStamp() {
        return lastStateChangedTimeStamp;
    }

    /**
     * @param lastStateChangedTimeStamp The lastStateChangedTimeStamp
     */
    @JsonProperty("lastStateChangedTimeStamp")
    public void setLastStateChangedTimeStamp(String lastStateChangedTimeStamp) {
        this.lastStateChangedTimeStamp = lastStateChangedTimeStamp;
    }

    /**
     * @return The timeInStateMin
     */
    @JsonProperty("timeInStateMin")
    public int getTimeInStateMin() {
        return timeInStateMin;
    }

    /**
     * @param timeInStateMin The timeInStateMin
     */
    @JsonProperty("timeInStateMin")
    public void setTimeInStateMin(int timeInStateMin) {
        this.timeInStateMin = timeInStateMin;
    }

    /**
     * @return The lastSeenTimeStamp
     */
    @JsonProperty("lastSeenTimeStamp")
    public Object getLastSeenTimeStamp() {
        return lastSeenTimeStamp;
    }

    /**
     * @param lastSeenTimeStamp The lastSeenTimeStamp
     */
    @JsonProperty("lastSeenTimeStamp")
    public void setLastSeenTimeStamp(String lastSeenTimeStamp) {
        this.lastSeenTimeStamp = lastSeenTimeStamp;
    }

    /**
     * @return The lastSeenMin
     */
    @JsonProperty("lastSeenMin")
    public Integer getLastSeenMin() {
        return lastSeenMin;
    }

    /**
     * @param lastSeenMin The lastSeenMin
     */
    @JsonProperty("lastSeenMin")
    public void setLastSeenMin(Integer lastSeenMin) {
        this.lastSeenMin = lastSeenMin;
    }

    /**
     * @return The state
     */
    @JsonProperty("state")
    public String getState() {
        return state;
    }

    /**
     * @param state The state
     */
    @JsonProperty("state")
    public void setState(String state) {
        this.state = state;
    }

    /**
     * @return The maintenanceState
     */
    @JsonProperty("maintenanceState")
    public String getMaintenanceState() {
        return maintenanceState;
    }

    /**
     * @param maintenanceState The maintenanceState
     */
    @JsonProperty("maintenanceState")
    public void setMaintenanceState(String maintenanceState) {
        this.maintenanceState = maintenanceState;
    }

    /**
     * @return The device
     */
    @JsonProperty("device")
    public String getDevice() {
        return device;
    }

    /**
     * @param device The device
     */
    @JsonProperty("device")
    public void setDevice(String device) {
        this.device = device;
    }

    /**
     * @return The sheets
     */
    @JsonProperty("sheets")
    public Integer getSheets() {
        return sheets;
    }

    /**
     * @param sheets The sheets
     */
    @JsonProperty("sheets")
    public void setSheets(Integer sheets) {
        this.sheets = sheets;
    }

    /**
     * @return The timeToDone
     */
    @JsonProperty("timeToDone")
    public TimeToDone getTimeToDone() {
        return timeToDone;
    }

    /**
     * @param timeToDone The timeToDone
     */
    @JsonProperty("timeToDone")
    public void setTimeToDone(TimeToDone timeToDone) {
        this.timeToDone = timeToDone;
    }

    /**
     * @return The meters
     */
    @JsonProperty("meters")
    public Double getMeters() {
        return meters;
    }

    /**
     * @param meters The meters
     */
    @JsonProperty("meters")
    public void setMeters(Double meters) {
        this.meters = meters;
    }

    /**
     * @return The squareMeters
     */
    @JsonProperty("squareMeters")
    public Double getSquareMeters() {
        return squareMeters;
    }

    /**
     * @param squareMeters The squareMeters
     */
    @JsonProperty("squareMeters")
    public void setSquareMeters(Double squareMeters) {
        this.squareMeters = squareMeters;
    }

    /**
     * @return The printedJobs
     */
    @JsonProperty("printedJobs")
    public Integer getPrintedJobs() {
        return printedJobs;
    }

    /**
     * @param printedJobs The printedJobs
     */
    @JsonProperty("printedJobs")
    public void setPrintedJobs(Integer printedJobs) {
        this.printedJobs = printedJobs;
    }

    /**
     * @return The jobsInQueue
     */
    @JsonProperty("jobsInQueue")
    public Integer getJobsInQueue() {
        return jobsInQueue;
    }

    /**
     * @param jobsInQueue The jobsInQueue
     */
    @JsonProperty("jobsInQueue")
    public void setJobsInQueue(Integer jobsInQueue) {
        this.jobsInQueue = jobsInQueue;
    }

    @JsonProperty("currentJobProgress")
    public float getCurrentJobProgress() {
        return currentJobProgress;
    }

    @JsonProperty("currentJobProgress")
    public void setCurrentJobProgress(float currentJobProgress) {
        this.currentJobProgress = currentJobProgress;
    }

    /**
     * @return The currentJob
     */
    @JsonProperty("currentJob")
    public String getCurrentJob() {
        return currentJob;
    }

    /**
     * @param currentJob The currentJob
     */
    @JsonProperty("currentJob")
    public void setCurrentJob(String currentJob) {
        this.currentJob = currentJob;
    }

    /**
     * @return The currentJobTimeToDone
     */
    @JsonProperty("currentJobTimeToDone")
    public TimeToDone getCurrentJobTimeToDone() {
        return currentJobTimeToDone;
    }

    /**
     * @param currentJobTimeToDone The currentJobTimeToDone
     */
    @JsonProperty("currentJobTimeToDone")
    public void setCurrentJobTimeToDone(TimeToDone currentJobTimeToDone) {
        this.currentJobTimeToDone = currentJobTimeToDone;
    }

    /**
     * @return The currentSheet
     */
    @JsonProperty("currentSheet")
    public Object getCurrentSheet() {
        return currentSheet;
    }

    /**
     * @param currentSheet The currentSheet
     */
    @JsonProperty("currentSheet")
    public void setCurrentSheet(Integer currentSheet) {
        this.currentSheet = currentSheet;
    }

    /**
     * @return The totalSheet
     */
    @JsonProperty("totalSheet")
    public Object getTotalSheet() {
        return totalSheet;
    }

    /**
     * @param totalSheet The totalSheet
     */
    @JsonProperty("totalSheet")
    public void setTotalSheet(Integer totalSheet) {
        this.totalSheet = totalSheet;
    }

    /**
     * @return The date
     */
    @JsonProperty("date")
    public Object getDate() {
        return date;
    }

    /**
     * @param date The date
     */
    @JsonProperty("date")
    public void setDate(String date) {
        this.date = date;
    }

    @JsonProperty("lastUpdateLfp")
    public void setLastUpdateLfp(String lastUpdateLfp) {
        this.lastUpdateLfp = lastUpdateLfp;
    }

    @JsonProperty("lastUpdateLfp")
    public String getLastUpdateLfp() {
        return lastUpdateLfp;
    }

    /**
     * @return The impressionType
     */
    @JsonProperty("impressionType")
    public String getImpressionType() {
        return impressionType;
    }

    /**
     * @param impressionType The impressionType
     */
    @JsonProperty("impressionType")
    public void setImpressionType(String impressionType) {
        this.impressionType = impressionType;
    }

    /**
     * @return The values
     */
    @JsonProperty("values")
    public List<ImpressionValue> getValues() {
        return values;
    }

    /**
     * @param values The values
     */
    @JsonProperty("values")
    public void setValues(List<ImpressionValue> values) {
        this.values = values;
    }

    @JsonProperty("shiftDataRespond")
    public ShiftData getShiftData() {
        return shiftData;
    }

    @JsonProperty("shiftDataRespond")
    public void setShiftData(ShiftData shiftData) {
        this.shiftData = shiftData;
    }

    @JsonProperty("litersConsumed")
    public void setLitersConsumed(Double litersConsumed) {
        this.litersConsumed = litersConsumed;
    }

    @JsonProperty("litersConsumed")
    public Double getLitersConsumed() {
        return litersConsumed;
    }

    @JsonProperty("pressState")
    public void setPressState(PressState pressState) {
        this.pressState = pressState;
    }

    @JsonProperty("pressState")
    public PressState getPressState() {
        return pressState;
    }

    @JsonProperty("underWarranty")
    public void setUnderWarranty(Boolean underWarranty) {
        this.underWarranty = underWarranty;
    }

    @JsonProperty("underWarranty")
    public Boolean isUnderWarranty() {
        return underWarranty;
    }

    @JsonProperty("lastPrintedJobs")
    public List<job> getLastPrintedJobs() {
        return lastPrintedJobs;
    }

    @JsonProperty("lastPrintedJobs")
    public void setLastPrintedJobs(List<job> lastPrintedJobs) {
        this.lastPrintedJobs = lastPrintedJobs;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class ImpressionValue {

        @JsonProperty("name")
        private String name;
        @JsonProperty("value")
        private Double value;
        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<>();

        /**
         * @return The name
         */
        @JsonProperty("name")
        public String getName() {
            return name;
        }

        /**
         * }        * @param name
         * The name
         */
        @JsonProperty("name")
        public void setName(String name) {
            this.name = name;
        }

        /**
         * @return The value
         */
        @JsonProperty("value")
        public Double getValue() {
            return value;
        }

        /**
         * @param value The value
         */
        @JsonProperty("value")
        public void setValue(Double value) {
            this.value = value;
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }

    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public class TimeToDone {

        @JsonProperty("duration")
        private int duration;
        @JsonProperty("unit")
        private String unit;

        public int getDuration() {
            return duration;
        }

        public void setDuration(int duration) {
            this.duration = duration;
        }

        public String getUnit() {
            return unit;
        }

        public void setUnit(String unit) {
            this.unit = unit;
        }
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class job {

        @JsonProperty("jobName")
        private String jobName;
        @JsonProperty("index")
        private int index;
        @JsonProperty("timeToComplete")
        private TimeToComplete timeToComplete;
        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<>();

        @JsonProperty("jobName")
        public String getJobName() {
            return jobName;
        }

        @JsonProperty("jobName")
        public void setJobName(String jobName) {
            this.jobName = jobName;
        }

        @JsonProperty("index")
        public int getIndex() {
            return index;
        }

        @JsonProperty("index")
        public void setIndex(int index) {
            this.index = index;
        }

        @JsonProperty("timeToComplete")
        public TimeToComplete getTimeToComplete() {
            return timeToComplete;
        }

        @JsonProperty("timeToComplete")
        public void setTimeToComplete(TimeToComplete timeToComplete) {
            this.timeToComplete = timeToComplete;
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }

        @JsonInclude(JsonInclude.Include.NON_NULL)
        public static class TimeToComplete {

            @JsonProperty("duration")
            private int duration;
            @JsonProperty("unit")
            private String unit;
            @JsonIgnore
            private Map<String, Object> additionalProperties = new HashMap<>();

            @JsonProperty("duration")
            public int getDuration() {
                return duration;
            }

            @JsonProperty("duration")
            public void setDuration(int duration) {
                this.duration = duration;
            }

            @JsonProperty("unit")
            public String getUnit() {
                return unit;
            }

            @JsonProperty("unit")
            public void setUnit(String unit) {
                this.unit = unit;
            }

            @JsonAnyGetter
            public Map<String, Object> getAdditionalProperties() {
                return this.additionalProperties;
            }

            @JsonAnySetter
            public void setAdditionalProperty(String name, Object value) {
                this.additionalProperties.put(name, value);
            }

        }
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class PressState {

        @JsonProperty("state")
        private String state;
        @JsonProperty("color")
        private String color;

        @JsonProperty("state")
        public void setState(String state) {
            this.state = state;
        }

        @JsonProperty("state")
        public String getState() {
            return state;
        }

        @JsonProperty("color")
        public void setColor(String color) {
            this.color = color;
        }

        @JsonProperty("color")
        public String getColor() {
            return color;
        }
    }
}