package com.hp.printosmobile.data.remote.services;

import android.content.Context;
import com.hp.printosmobilelib.core.logging.HPLogger;

import com.hp.printosmobile.data.remote.ApiServicesProvider;
import com.hp.printosmobile.data.remote.models.ProductionData;
import com.hp.printosmobile.data.remote.models.ProductionViewModel;
import com.hp.printosmobilelib.core.communications.remote.AsyncDataFetcherBaseClass;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by leviasaf on 25/04/2016.
 */
public class ProductionFetchDataHelper extends AsyncDataFetcherBaseClass<ProductionData> {
    private static String TAG = ProductionFetchDataHelper.class.getSimpleName();
    private static ProductionFetchDataHelper myInstance;

    private Context mAppContext;
    ProductionViewModel mModel;
    ApiServicesProvider mProvider;

    private ProductionFetchDataHelper(Context appContext, ApiServicesProvider provider)
    {
        mAppContext=appContext;
        mModel=new ProductionViewModel(mAppContext);
        mProvider=provider;
        init();

    }
    public static ProductionFetchDataHelper getInstance(Context appContext, ApiServicesProvider provider) {
        if (myInstance == null)
            myInstance = new ProductionFetchDataHelper(appContext,provider);

        return myInstance;
    }


    @Override
    protected ProductionData getDataFromModel() {
        return mModel.loadFeed();
    }

    protected ProductionData getDataFromNetwork() throws IOException {
        ProductionData retValue;
        Call<ProductionData> dataCall=null;
        Response<ProductionData> dataResponse=null;
        dataCall= mProvider.getProductionService().getProductionData();
        dataResponse=dataCall.execute();

        if (dataResponse.isSuccessful()) {
            retValue=dataResponse.body();
            try {
                mModel.save(retValue);
            }
            catch (Exception e)
            {
                HPLogger.d(TAG,"error");
            }
            HPLogger.d(TAG,"data from network");

        } else {
            throw new IOException();
        }
        return retValue;
    }



}



