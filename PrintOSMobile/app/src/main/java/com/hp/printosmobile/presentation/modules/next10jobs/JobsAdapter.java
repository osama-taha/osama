package com.hp.printosmobile.presentation.modules.next10jobs;

import android.content.Context;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.modules.shared.DeviceViewModel;
import com.hp.printosmobile.utils.HPLocaleUtils;
import com.hp.printosmobile.utils.HPUIUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Anwar Asbah on 5/15/2016.
 */
class JobsAdapter extends RecyclerView.Adapter<JobsAdapter.JobsViewHolder> {

    private static final int MAX_NUMBER_OF_DISPLAYED_JOBS = 10;
    private static final long ANIMATION_DURATION = 300;

    private Context context;
    private List<ExpandableModel> models;
    private JobsInQueueAdapterCallback callback;
    private int currentExpandedPosition;
    private DeviceViewModel.JobType jobType;
    private int firstExpandableItemIndex = -1;

    JobsAdapter(Context context, List<DeviceViewModel> deviceViewModels, DeviceViewModel.JobType jobType,
                JobsInQueueAdapterCallback callback) {
        this.context = context;
        this.callback = callback;
        this.jobType = jobType;
        models = null;

        boolean nonExpanded = true;
        if (deviceViewModels != null) {
            int count = deviceViewModels.size();
            models = new ArrayList<>();
            for (int i = 0; i < deviceViewModels.size(); i++) {

                DeviceViewModel viewModel = deviceViewModels.get(i);
                List<DeviceViewModel.Job> jobs = viewModel.getJobs(jobType);

                ExpandableModel expandableModel = new ExpandableModel();
                expandableModel.deviceViewModel = viewModel;
                expandableModel.childCount = jobs == null ? 0 : jobs.size();
                expandableModel.isClickable = count > 1 && expandableModel.childCount > 0;
                expandableModel.isExpanded = nonExpanded && expandableModel.childCount > 0;
                if (nonExpanded && expandableModel.childCount > 0) {
                    firstExpandableItemIndex = i;
                    currentExpandedPosition = i;
                    nonExpanded = false;
                }

                models.add(expandableModel);
            }
        }
    }

    public int getFirstExpandableItemIndex() {
        return firstExpandableItemIndex;
    }

    @Override
    public JobsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        ViewGroup view = (ViewGroup) inflater.inflate(R.layout.fragment_next_10_jobs_item, parent, false);
        return new JobsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(JobsViewHolder viewHolder, int position) {
        ExpandableModel expandableModel = models.get(position);
        viewHolder.position = position;

        buildHeaderView(viewHolder, expandableModel);
        buildChildView(viewHolder, expandableModel);

        viewHolder.rootView.setBackground(ResourcesCompat.getDrawable(context.getResources(),
                getItemCount() == 1 ? android.R.color.transparent : R.drawable.jobs_in_queue_cell, null));

        int horizontalPadding = (int) context.getResources().getDimension(getItemCount() == 1 ?
                R.dimen.next_10_jobs_single_press_margin
                : R.dimen.next_10_jobs_multiple_press_margin);
        viewHolder.childSeparator.setPadding(horizontalPadding, 0, horizontalPadding, 0);
    }

    private void buildHeaderView(JobsViewHolder jobsViewHolder, ExpandableModel expandableModel) {

        DeviceViewModel indigoModel = expandableModel.deviceViewModel;

        String pressName = indigoModel.getImpressionType() != null ?
                context.getString(R.string.printer_name_with_model, indigoModel.getName(), indigoModel.getImpressionType())
                : indigoModel.getName();
        jobsViewHolder.pressNameTextView.setText(pressName);

        List<DeviceViewModel.Job> deviceJobs = indigoModel.getJobs(jobType);
        int totalNumberOfJobs = indigoModel.getTotalJobCount(jobType);
        int displayedNumberOfJobs = deviceJobs == null ? 0 : deviceJobs.size();
        displayedNumberOfJobs = Math.min(MAX_NUMBER_OF_DISPLAYED_JOBS, displayedNumberOfJobs);

        String headerDescriptiveText = context.getString(R.string.next_10_jobs_list_header_empty);
        if (displayedNumberOfJobs != 0) {
            switch (jobType) {
                case COMPLETED:
                    headerDescriptiveText = displayedNumberOfJobs == 1 ? context.getString(R.string.completed_job_list_header_display_statement)
                            : context.getString(R.string.completed_jobs_list_header_display_statement, displayedNumberOfJobs);
                    break;
                case QUEUED:
                    if (totalNumberOfJobs != 0) {
                        headerDescriptiveText = context.getString(
                                R.string.jobs_in_queue_list_header_display_statement,
                                displayedNumberOfJobs,
                                totalNumberOfJobs);
                    }
                    break;
            }
        }
        jobsViewHolder.displayingTextView.setText(headerDescriptiveText);


        jobsViewHolder.expandingIndicator.setVisibility(expandableModel.isClickable ?
                View.VISIBLE : View.INVISIBLE);
        jobsViewHolder.expandingIndicator.setImageResource(expandableModel.isExpanded ?
                R.drawable.expand_arrow_up : R.drawable.expand_arrow_down);
    }

    private void buildChildView(JobsViewHolder jobsViewHolder, ExpandableModel expandableModel) {
        DeviceViewModel childModel = expandableModel.deviceViewModel;
        jobsViewHolder.containerLayout.setVisibility(expandableModel.isExpanded ? View.VISIBLE : View.GONE);

        if (expandableModel.childCount == 0) {
            return;
        }

        List<DeviceViewModel.Job> jobs = childModel.getJobs(jobType)
                .subList(0, Math.min(MAX_NUMBER_OF_DISPLAYED_JOBS, expandableModel.childCount));

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context,
                LinearLayoutManager.VERTICAL, false);
        JobsChildListAdapter adapter = new JobsChildListAdapter(context, jobs, jobType);
        jobsViewHolder.jobsList.setLayoutManager(layoutManager);
        jobsViewHolder.jobsList.setAdapter(adapter);
        jobsViewHolder.jobsList.setHasFixedSize(true);

        if (jobType.isHasTimeParameters()) {
            jobsViewHolder.totalTimeRemaining.setText(HPLocaleUtils.getLocalizedTimeStringV2(context,
                    adapter.getTotalTimeInSeconds(), TimeUnit.SECONDS));
        }

        HPUIUtils.setVisibility(jobType.isHasTimeParameters(), jobsViewHolder.totalTimeRemainingLayout,
                jobsViewHolder.estimatedTimeTextView);
    }

    @Override
    public int getItemCount() {
        return models == null ? 0 : models.size();
    }

    class JobsViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.root_layout)
        View rootView;
        @Bind(R.id.header_layout)
        View headerLayout;
        @Bind(R.id.press_name_text_view)
        TextView pressNameTextView;
        @Bind(R.id.displaying_text_view)
        TextView displayingTextView;
        @Bind(R.id.expanding_indicator)
        ImageView expandingIndicator;
        @Bind(R.id.container_layout)
        View containerLayout;
        @Bind(R.id.child_separator)
        View childSeparator;
        @Bind(R.id.jobs_list)
        RecyclerView jobsList;
        @Bind(R.id.total_time_remaining)
        TextView totalTimeRemaining;
        @Bind(R.id.total_time_remaining_layout)
        View totalTimeRemainingLayout;
        @Bind(R.id.estimated_time_text_view)
        View estimatedTimeTextView;

        int position;

        private JobsViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            headerLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    expandChild();
                }
            });
        }

        private void expandChild() {
            if (models.get(position).isClickable) {

                ExpandableModel model = models.get(position);
                model.isExpanded = !model.isExpanded;

                if (model.isExpanded) {
                    JobsAdapter.expand(containerLayout, position, callback);
                    if (currentExpandedPosition > -1) {
                        models.get(currentExpandedPosition).isExpanded = false;
                        notifyItemChanged(currentExpandedPosition);
                    }
                    currentExpandedPosition = position;
                } else {
                    JobsAdapter.collapse(containerLayout);
                    currentExpandedPosition = -1;
                }

                expandingIndicator.setImageResource(model.isExpanded ?
                        R.drawable.expand_arrow_up : R.drawable.expand_arrow_down);
            }
        }
    }

    private static void expand(final View v, final int position, final JobsInQueueAdapterCallback callback) {

        v.measure(LinearLayout.LayoutParams.MATCH_PARENT, View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        final int targetHeight = v.getMeasuredHeight();

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        v.getLayoutParams().height = 1;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1
                        ? LinearLayout.LayoutParams.WRAP_CONTENT
                        : (int) (targetHeight * interpolatedTime);
                v.requestLayout();
                callback.onJobExpanded(position);
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        a.setDuration(ANIMATION_DURATION);
        v.startAnimation(a);
    }

    private static void collapse(final View v) {
        final int initialHeight = v.getMeasuredHeight();

        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {
                    v.setVisibility(View.GONE);
                } else {
                    v.getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        a.setDuration(ANIMATION_DURATION);
        v.startAnimation(a);
    }


    private class ExpandableModel {
        DeviceViewModel deviceViewModel;
        boolean isExpanded;
        boolean isClickable;
        int childCount;
    }

    interface JobsInQueueAdapterCallback {
        void onJobExpanded(int viewPosition);
    }
}
