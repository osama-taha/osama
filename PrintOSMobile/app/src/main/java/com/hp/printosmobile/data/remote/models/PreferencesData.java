package com.hp.printosmobile.data.remote.models;

import android.content.Context;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.hp.printosmobile.R;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Osama Taha on 5/16/17.
 */
public class PreferencesData {

    @JsonProperty("general")
    private General general;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("general")
    public General getGeneral() {
        return general;
    }

    @JsonProperty("general")
    public void setGeneral(General general) {
        this.general = general;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public static class General {

        @JsonProperty("locale")
        private String locale;
        @JsonProperty("timeZone")
        private String timeZone;
        @JsonProperty("unitSystem")
        private UnitSystem unitSystem;

        @JsonProperty("locale")
        public String getLocale() {
            return locale;
        }

        @JsonProperty("locale")
        public void setLocale(String locale) {
            this.locale = locale;
        }

        @JsonProperty("timeZone")
        public String getTimeZone() {
            return timeZone;
        }

        @JsonProperty("timeZone")
        public void setTimeZone(String timeZone) {
            this.timeZone = timeZone;
        }

        @JsonProperty("unitSystem")
        public UnitSystem getUnitSystem() {
            return unitSystem;
        }

        @JsonProperty("unitSystem")
        public void setUnitSystem(UnitSystem unitSystem) {
            this.unitSystem = unitSystem;
        }

        @Override
        public String toString() {
            return "General{" +
                    "locale='" + locale + '\'' +
                    ", timeZone='" + timeZone + '\'' +
                    ", unitSystem=" + unitSystem +
                    '}';
        }
    }


    @Override
    public String toString() {
        return "PreferencesData{" +
                "general=" + general +
                ", additionalProperties=" + additionalProperties +
                '}';
    }

    public enum UnitSystem {

        Metric, Imperial;

        public static UnitSystem from(String unitSystemString) {

            if (unitSystemString == null) {
                return UnitSystem.Metric;
            }

            for (UnitSystem unitSystem : UnitSystem.values()) {
                if (unitSystem.name().equalsIgnoreCase(unitSystemString)) {
                    return unitSystem;
                }
            }

            return UnitSystem.Metric;

        }

        public String getLocalizedName(Context context) {
            switch (this) {
                case Metric:
                    return context.getString(R.string.unit_metric);
                case Imperial:
                    return context.getString(R.string.unit_imperial);
            }
            return null;
        }
    }
}
