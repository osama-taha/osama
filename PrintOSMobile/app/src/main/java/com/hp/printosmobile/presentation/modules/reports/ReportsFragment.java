package com.hp.printosmobile.presentation.modules.reports;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.hp.printosmobile.R;
import com.hp.printosmobile.data.remote.models.ReportData;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitViewModel;
import com.hp.printosmobile.presentation.modules.main.IMainFragment;
import com.hp.printosmobile.presentation.modules.reports.presenter.ReportDataManager;
import com.hp.printosmobile.presentation.modules.reports.presenter.ReportPresenter;
import com.hp.printosmobile.presentation.modules.reports.presenter.ReportView;
import com.hp.printosmobile.presentation.modules.reports.presenter.ReportsDataParser;
import com.hp.printosmobile.utils.HPLocaleUtils;
import com.hp.printosmobile.utils.HPUIUtils;
import com.hp.printosmobilelib.core.communications.remote.APIException;
import com.hp.printosmobilelib.core.utils.HPDateUtils;
import com.hp.printosmobilelib.ui.common.HPFragment;
import com.hp.printosmobilelib.ui.widgets.TypefaceManager;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import butterknife.Bind;

/**
 * Created by Anwar Asbah 6/13/2016
 */
public class ReportsFragment extends HPFragment implements ReportView, IMainFragment, PerformanceChartBuilderCallbacks {

    public static final String TAG = ReportsFragment.class.getSimpleName();
    private static final String ERROR_MSG_FORMAT = "%1$s\n%2$s";

    public static final int OVERALL_POINTS = 100;
    public static final int KPI_POINTS = 20;
    private static final int WEEK_OFFSET = -52;
    @Bind(R.id.report_fragment_layout)
    View reportFragmentLayout;
    @Bind(R.id.all_values_description_label)
    TextView allValuesDescriptionLabel;
    @Bind(R.id.text_report_title)
    TextView reportTitleText;
    @Bind(R.id.text_report_date_range)
    TextView reportDateRangeText;
    @Bind(R.id.y_axsis_text_label)
    TextView yAxisLabel;
    @Bind(R.id.loading_view)
    ProgressBar loadingView;
    @Bind(R.id.error_msg_text_view)
    TextView errorMsg;

    private ReportPresenter reportPresenter;
    private ReportFilter reportFilter;
    private PerformanceChartBuilder builder;
    private ReportKpiViewModel rootKpi = null;
    private ReportFragmentCallback listener;
    private boolean isLoaded = false;
    private boolean stayOnKpi;

    public ReportsFragment() {
    }

    public static ReportsFragment newInstance() {
        return new ReportsFragment();
    }

    public static ReportFilter getReportFilter() {
        ReportFilter reportFilter = new ReportFilter();
        reportFilter.setOffset(WEEK_OFFSET);
        reportFilter.setDateFormat(ReportDataManager.DATE_FORMAT);
        reportFilter.setUnit(ReportFilter.Unit.SHEETS);
        reportFilter.setResolution(ReportFilter.Resolution.WEEK);

        return reportFilter;
    }

    /**
     * Prepare an initial configurations for the performance chart.
     */
    public static LineChartConfiguration getChartConfiguration(Context context) {
        LineChartConfiguration configuration = new LineChartConfiguration();

        configuration.setDragEnabled(true);
        configuration.setTouchEnabled(true);
        configuration.setScaleXEnabled(true);
        configuration.setScaleYEnabled(false);
        configuration.setDoubleTapToZoomEnabled(false);
        configuration.setHighlightPerDragEnabled(true);
        configuration.setPinchZoomEnabled(true);

        configuration.setxAxisTypeFace(TypefaceManager.getTypeface(context, TypefaceManager.HPTypeface.HP_LIGHT));
        configuration.setxAxisTextColor(Color.BLACK);

        configuration.setyAxisTypeFace(TypefaceManager.getTypeface(context, TypefaceManager.HPTypeface.HP_LIGHT));
        configuration.setyAxisTextColor(Color.BLACK);

        return configuration;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_reports;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            listener = (ReportFragmentCallback) getActivity();
        } catch (ClassCastException e) {
            throw new RuntimeException("parent activity must implement ReportFragmentCallback");
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initPresenter();
        reportFragmentLayout.setVisibility(View.INVISIBLE);
    }

    @Override
    public boolean isIntercomAccessible() {
        return true;
    }

    private void initPresenter() {
        reportPresenter = new ReportPresenter();
        reportPresenter.attachView(this);

        this.reportFilter = getReportFilter();
    }

    @Override
    public void onBusinessUnitSelected(BusinessUnitViewModel businessUnitModel) {
        //resetting chart
        reportFilter.setBusinessUnit(businessUnitModel);
        isLoaded = false;

        if(listener != null) {
            listener.onReportBusinessUnitSet();
        }
    }

    public void loadScreen () {
        if(isLoaded && !stayOnKpi) {
            openReportFragment(null);
            return;
        }

        if(stayOnKpi && reportFilter.getKpi() != null) {
            getKpiData(reportFilter.getKpi());
        } else {
            getOverallData();
        }

        stayOnKpi = false;
        isLoaded = true;
    }

    @Override
    public void onFiltersChanged(BusinessUnitViewModel businessUnitViewModel) {

        onBusinessUnitSelected(businessUnitViewModel);

    }

    private void getOverallData() {
        if (reportFilter == null || reportFilter.getBusinessUnit() == null) {
            return;
        }
        showLoading();
        reportPresenter.getOverAllData(getContext(), reportFilter);
    }

    private void getKpiData(ReportKpiViewModel kpi) {
        showLoading();
        reportFilter.setKpi(kpi);
        reportPresenter.getKpiData(getContext(), reportFilter, false);
    }

    @Override
    public void onRequestCompleted() {
        hideLoading();
    }

    @Override
    public void onOverallError() {
        errorMsg.setVisibility(View.VISIBLE);
        rootKpi = null;
        if(listener != null) {
            listener.moveToHomeFromTab(getStringResource(R.string.fragment_reports_name_key));
        }
    }

    private void hideLoading() {
        loadingView.setVisibility(View.INVISIBLE);
    }

    private void showLoading() {
        loadingView.setVisibility(View.VISIBLE);
    }

    @Override
    public void displayKpiReport(ReportKpiViewModel kpi, boolean isRoot, int pointTotal) {
        rootKpi = isRoot ? kpi : rootKpi;

        if (builder == null) {
            initPerformanceChartBuilder();
        }

        ReportKpiViewModel selectedKpi = builder.getSelectedKpi();
        if (kpi == null || (kpi.getParentKpi() != null && (selectedKpi == null || !kpi.getName().equals(selectedKpi.getName())))) {
            return;
        }

        reportFragmentLayout.setVisibility(View.VISIBLE);
        errorMsg.setVisibility(View.GONE);

        builder.setReportTotalPoints(pointTotal);
        builder.animateX();
        builder.displayKpi(kpi);
    }

    private void initPerformanceChartBuilder() {
        builder = PerformanceChartBuilder.create(getActivity(), getView(), allValuesDescriptionLabel)
                .setConfigurations(getChartConfiguration(getContext()))
                .setCallbacks(this);
        builder.buildChart();
    }

    public void openReportFragment(String kpiName) {
        HPUIUtils.getRandomColor();
        if (kpiName == null) {
            if (rootKpi == null) {
                return;
            } else {
                displayKpiReport(rootKpi, false, OVERALL_POINTS);
            }
        } else if (rootKpi != null) {
            ReportKpiViewModel kpi = searchForKpi(rootKpi, kpiName);
            if (kpi != null) {
                builder.setSelectedKpi(kpi);
                if (kpi.getKpiValues() != null || kpi.hasSubKpis() || !kpi.isReportPerPressListEmpty()) {
                    displayKpiReport(kpi, false, KPI_POINTS);
                } else {
                    getKpiData(kpi);
                }
            }
        } else {
            getKpiData(ReportsDataParser.initKPI(getContext(), kpiName, reportFilter.getBusinessUnit().getBusinessUnit()));
        }
    }

    private ReportKpiViewModel searchForKpi(ReportKpiViewModel kpi, String targetKpiName) {
        if (kpi == null) return null;

        Stack<ReportKpiViewModel> model = new Stack<>();
        model.add(kpi);

        while (model.size() > 0) {
            ReportKpiViewModel currentKpi = model.pop();
            if (targetKpiName.equals(currentKpi.getName())) {
                return currentKpi;
            } else {
                if (currentKpi.getSubKPIs() != null) {
                    for (ReportKpiViewModel subKpi : currentKpi.getSubKPIs()) {
                        model.add(subKpi);
                    }
                }
            }
        }

        return null;
    }

    @Override
    public void onChartInitialized(ReportKpiViewModel kpi) {
        if (kpi == null) return;

        yAxisLabel.setText(kpi.getYAxisLabel());
        reportTitleText.setText(kpi.getLocalizedName());
    }

    @Override
    public void onKpiSelected(ReportKpiViewModel kpi) {
        getKpiData(kpi);
    }

    @Override
    public void onLegendViewLongClicked(ReportKpiViewModel kpi) {
    }

    @Override
    public void onPeriodChanged(int lowestVisibleWeek, int highestVisibleWeek) {
        List<ReportData.UnitEvent.Event> values = builder.getSelectedKpi().getUnitEvents();
        setDatePeriod(values, lowestVisibleWeek, highestVisibleWeek);
    }

    private void setDatePeriod(List<ReportData.UnitEvent.Event> values, int firstIndex, int lastIndex) {
        if (values == null) return;
        int size = values.size();
        if (firstIndex >= 0 && firstIndex < size && lastIndex >= 0 && lastIndex < size) {
            Date dateFrom = HPDateUtils.parseDate(values.get(firstIndex).getHappenedOn(), reportFilter.getDateFormat());
            Date dateTo = HPDateUtils.parseDate(values.get(lastIndex).getHappenedOn(), reportFilter.getDateFormat());
            reportDateRangeText.setText(HPLocaleUtils.getDateRangeString(getActivity(), dateFrom, dateTo));
        }
    }

    @Override
    public boolean onBackPressed() {
        //All null move to home
        if (builder == null || builder.getSelectedKpi() == null || builder.getSelectedKpi().getParentKpi() == null) {
            listener.moveToHome();
        } else {
            ReportKpiViewModel parent = builder.getSelectedKpi().getParentKpi();
            //Parent kpi is aggregate display parent
            if (parent.isAggregate()) {
                builder.setSelectedKpi(parent);
                displayKpiReport(parent, false, parent.getParentKpi() == null ? OVERALL_POINTS : KPI_POINTS);
                //Parent kpi not aggregate
            } else {
                parent = parent.getParentKpi();
                //parent of parent null move to home
                if (parent == null) {
                    listener.moveToHome();
                    //move to parent of parent
                } else {
                    builder.setSelectedKpi(parent);
                    displayKpiReport(parent, false, KPI_POINTS);
                }
            }
        }
        return true;
    }

    @Override
    public void didFinishGettingBusinessUnits(Map<BusinessUnitEnum, BusinessUnitViewModel> businessUnitsMap) {

    }

    @Override
    public void onError(APIException exception, String tag) {
        if (ReportPresenter.TAG_KPI.equals(tag)) {
            HPUIUtils.displayToastException(getContext(), exception, tag, false);
        } else {
            errorMsg.setText(String.format(ERROR_MSG_FORMAT, getStringResource(R.string.error_no_data_msg),
                    HPLocaleUtils.getSimpleErrorMsg(getActivity(), exception)));
        }
    }

    @Override
    public void onMainError(String msg, boolean triggerSignOut){
        reportFragmentLayout.setVisibility(View.INVISIBLE);
        errorMsg.setVisibility(View.VISIBLE);
        rootKpi = null;
    }

    @Override
    public boolean respondToValidationPeriodExceedance() {
        return false;
    }

    @Override
    public boolean isFragmentNameToBeDisplayed() {
        return true;
    }

    @Override
    public boolean isFilteringAvailable() {
        return true;
    }

    @Override
    public int getToolbarDisplayName() {
        return R.string.reports;
    }

    public void onRefresh() {
        reportPresenter.onRefresh();
    }

    public void onPostValidation(){
        stayOnKpi = true;
    }

    public interface ReportFragmentCallback {
        void moveToHome();

        void onReportBusinessUnitSet ();

        void moveToHomeFromTab(String string);
    }
}
