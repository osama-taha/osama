package com.hp.printosmobile.notification;

import android.Manifest;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.res.ResourcesCompat;

import com.foursquare.pilgrim.Confidence;
import com.foursquare.pilgrim.CurrentPlace;
import com.foursquare.pilgrim.PilgrimNotificationHandler;
import com.foursquare.pilgrim.PilgrimSdk;
import com.foursquare.pilgrim.PilgrimSdkPlaceNotification;
import com.foursquare.pilgrim.RegionType;
import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.AppseeSdk;
import com.hp.printosmobile.BuildConfig;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.presentation.modules.settings.SubscriptionEvent;
import com.hp.printosmobilelib.core.logging.HPLogger;

/**
 * Created by Osama Taha on 8/10/17.
 */
public class LocationBasedNotificationsManager {

    private static final String TAG = LocationBasedNotificationsManager.class.getSimpleName();

    private static LocationBasedNotificationsManager instance;
    private final Context context;

    public static LocationBasedNotificationsManager getInstance(Context context) {
        if (instance == null) {
            instance = new LocationBasedNotificationsManager(context);
        }
        return instance;
    }

    private LocationBasedNotificationsManager(Context context) {
        this.context = context;
    }

    public void init() {

        PilgrimSdk.Builder builder = new PilgrimSdk.Builder(context)
                .consumer(BuildConfig.PILGRIM_CLIENT_ID, BuildConfig.PILGRIM_CLIENT_SECRET)
                .logLevel(PilgrimSdk.LogLevel.DEBUG)
                .enablePersistentLogs()
                .notificationHandler(new PilgrimNotificationHandler() {
                    @Override
                    public void handlePlaceNotification(Context context, PilgrimSdkPlaceNotification pilgrimSdkPlaceNotification) {
                        LocationBasedNotificationsManager.getInstance(context).displayNotification(pilgrimSdkPlaceNotification);
                    }
                });

        PilgrimSdk.with(builder);

        if (PrintOSPreferences.getInstance(context).isLocationBasedNotificationsFeatureEnabled() && locationBasedNotificationToggleOn()) {
            startMonitoring();
        }

    }

    public boolean isLocationPermissionGranted() {
        int result = context.checkCallingOrSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    public boolean locationBasedNotificationToggleOn() {
        return isLocationPermissionGranted()
                && PrintOSPreferences.getInstance(context).isLocationBasedNotificationsOn();
    }

    public void enableNotifications(boolean isSubscribed) {
        if (isSubscribed) {
            startMonitoring();
        } else {
            stopMonitoring();
        }
    }

    private void startMonitoring() {

        HPLogger.d(TAG, "Pilgrim start monitoring...");

        PrintOSPreferences.getInstance(context).setLocationBasedNotificationsOn(true);
        PilgrimSdk.start(context);
    }

    private void stopMonitoring() {

        HPLogger.d(TAG, "Pilgrim stop monitoring...");

        PrintOSPreferences.getInstance(context).setLocationBasedNotificationsOn(false);
        PilgrimSdk.stop(context);
    }

    public void displayNotification(PilgrimSdkPlaceNotification pilgrimSdkPlaceNotification) {

        if (PrintOSPreferences.getInstance(context).isLocationBasedNotificationsFeatureEnabled() && locationBasedNotificationToggleOn()) {
            fireNotification(pilgrimSdkPlaceNotification);
        }
    }

    private void fireNotification(PilgrimSdkPlaceNotification pilgrimSdkPlaceNotification) {

        if (pilgrimSdkPlaceNotification.getCurrentPlace() == null) {
            return;
        }

        HPLogger.d(TAG, "handlePlaceNotification " + pilgrimSdkPlaceNotification.getCurrentPlace());
        HPLogger.d(TAG, "notification confidence level: " + pilgrimSdkPlaceNotification.getCurrentPlace().getConfidence());

        CurrentPlace currentPlace = pilgrimSdkPlaceNotification.getCurrentPlace();
        boolean isExit = currentPlace.hasExited();
        RegionType type = currentPlace.getType();

        if (type.isHomeOrWork() && !isExit && (currentPlace.getConfidence() == Confidence.HIGH)) {

            // Instantiate a Builder object.
            NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
            builder.setSmallIcon(R.drawable.ic_notification);

            int requestCode = NotificationUtils.getUniqueRequestCode();

            SubscriptionEvent.NotificationEventEnum notificationEventEnum = type == RegionType.WORK ?
                    SubscriptionEvent.NotificationEventEnum.LOCATION_BASED_NOTIFICATIONS_ARRIVE_WORK
                    : SubscriptionEvent.NotificationEventEnum.LOCATION_BASED_NOTIFICATIONS_ARRIVE_HOME;

            Intent intent = NotificationUtils.getNotificationIntent(context, null, notificationEventEnum.getKey(), 0, 0, null, true);
            PendingIntent pendingIntent = PendingIntent.getActivity(context, requestCode, intent, PendingIntent.FLAG_ONE_SHOT);
            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            String title = context.getString(R.string.app_name);
            String message = type == RegionType.WORK ? context.getString(R.string.location_based_notification_arrive_work)
                    : context.getString(R.string.location_based_notification_arrive_home);

            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context)
                    .setSmallIcon(R.drawable.ic_notification)
                    .setColor(ResourcesCompat.getColor(context.getResources(), R.color.c62, null))
                    .setContentTitle(title)
                    .setContentText(message)
                    .setStyle(new NotificationCompat.BigTextStyle()
                            .bigText(message).setBigContentTitle(title))
                    .setAutoCancel(true)
                    .setSound(defaultSoundUri)
                    .setContentIntent(pendingIntent);

            NotificationManager notificationManager =
                    (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

            int id = (int) System.currentTimeMillis();
            notificationManager.notify(id, notificationBuilder.build());

            HPLogger.d(TAG, "show location based notification " + notificationEventEnum.getKey());

            AppseeSdk.getInstance(context).sendEvent(AppseeSdk.EVENT_SHOW_NOTIFICATION, notificationEventEnum.getKey());
            Analytics.sendNotificationArriveFirebaseEvent(notificationEventEnum.getKey());
            Analytics.sendEvent(Analytics.MOBILE_PRINT_BEAT_CATEGORY, Analytics.SHOW_NOTIFICATION_ACTION, notificationEventEnum.getKey());

        }

    }

}
