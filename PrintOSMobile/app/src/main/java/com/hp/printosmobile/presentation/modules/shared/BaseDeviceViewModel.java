package com.hp.printosmobile.presentation.modules.shared;

import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;

import java.util.Comparator;

/**
 * Base device view model for All Printbeat divisions.
 * <p/>
 * Created by Osama Taha on 6/2/16.
 */
public abstract class BaseDeviceViewModel {

    public static Comparator<BaseDeviceViewModel> NAME_COMPARATOR = new Comparator<BaseDeviceViewModel>() {
        @Override
        public int compare(BaseDeviceViewModel lhs, BaseDeviceViewModel rhs) {

            if (lhs == null && rhs == null) return 0;
            if (lhs == null && rhs != null) return 1;
            if (lhs != null && rhs == null) return -1;

            String lhsName = lhs.getName();
            String rhsName = rhs.getName();

            if (lhsName == null && rhsName == null) return 0;
            if (lhsName == null && rhsName != null) return 1;
            if (lhsName != null && rhsName == null) return -1;

            return lhsName.compareTo(rhsName);
        }
    };

    public static Comparator<BaseDeviceViewModel> STATUS_COMPARATOR = new Comparator<BaseDeviceViewModel>() {
        @Override
        public int compare(BaseDeviceViewModel lhs, BaseDeviceViewModel rhs) {

            if (lhs == null && rhs == null) return 0;
            if (lhs == null && rhs != null) return 1;
            if (lhs != null && rhs == null) return -1;

            DeviceState lhsState = lhs.getState();
            DeviceState rhsState = rhs.getState();

            if (lhsState == null && rhsState == null) return 0;
            if (lhsState == null && rhsState != null) return 1;
            if (lhsState != null && rhsState == null) return -1;

            int lhsStateOrdinal = lhs.getBusinessUnitEnum() == BusinessUnitEnum.LATEX_PRINTER ?
                    lhs.getState().getLatexSortOrder() : lhsState.ordinal();
            int rhsStateOrdinal = rhs.getBusinessUnitEnum() == BusinessUnitEnum.LATEX_PRINTER ?
                    rhs.getState().getLatexSortOrder() : rhsState.ordinal();

            int sortOrderDiff = lhsStateOrdinal - rhsStateOrdinal;

            if (sortOrderDiff == 0 && lhs.getName() != null) {
                return lhs.getName().compareTo(rhs.getName());
            }

            return sortOrderDiff;
        }
    };

    public abstract String getName();

    public abstract DeviceState getState();

    public abstract BusinessUnitEnum getBusinessUnitEnum();

}
