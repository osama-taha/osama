package com.hp.printosmobile.presentation.modules.drawer;

import com.hp.printosmobile.presentation.MVPView;
import com.hp.printosmobilelib.core.communications.remote.APIException;

import java.util.List;

/**
 * Created by Osama Taha on 5/20/16.
 */
public interface NavView extends MVPView {
    void onOrganizationsRetrieved(List<OrganizationViewModel> organizations);

    void onContextChanged(boolean isSuccessful, OrganizationViewModel organizationViewModel, APIException e);
}
