package com.hp.printosmobile.data.remote.models;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Osama Taha on 4/24/16.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ScorableConfigData {

    @JsonProperty("units")
    private List<String> units = new ArrayList<String>();
    @JsonProperty("resolutions")
    private List<String> resolutions = new ArrayList<String>();
    @JsonProperty("aggregations")
    private List<String> aggregations = new ArrayList<String>();
    @JsonProperty("orderValue")
    private Integer orderValue;
    @JsonProperty("calType")
    private String calType;
    @JsonProperty("presentationHint")
    private String presentationHint;
    @JsonProperty("name")
    private String name;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * @return The units
     */
    @JsonProperty("units")
    public List<String> getUnits() {
        return units;
    }

    /**
     * @param units The units
     */
    @JsonProperty("units")
    public void setUnits(List<String> units) {
        this.units = units;
    }

    /**
     * @return The resolutions
     */
    @JsonProperty("resolutions")
    public List<String> getResolutions() {
        return resolutions;
    }

    /**
     * @param resolutions The resolutions
     */
    @JsonProperty("resolutions")
    public void setResolutions(List<String> resolutions) {
        this.resolutions = resolutions;
    }

    /**
     * @return The aggregations
     */
    @JsonProperty("aggregations")
    public List<String> getAggregations() {
        return aggregations;
    }

    /**
     * @param aggregations The aggregations
     */
    @JsonProperty("aggregations")
    public void setAggregations(List<String> aggregations) {
        this.aggregations = aggregations;
    }

    /**
     * @return The orderValue
     */
    @JsonProperty("orderValue")
    public Integer getOrderValue() {
        return orderValue;
    }

    /**
     * @param orderValue The orderValue
     */
    @JsonProperty("orderValue")
    public void setOrderValue(Integer orderValue) {
        this.orderValue = orderValue;
    }

    /**
     * @return The calType
     */
    @JsonProperty("calType")
    public String getCalType() {
        return calType;
    }

    /**
     * @param calType The calType
     */
    @JsonProperty("calType")
    public void setCalType(String calType) {
        this.calType = calType;
    }

    /**
     * @return The presentationHint
     */
    @JsonProperty("presentationHint")
    public String getPresentationHint() {
        return presentationHint;
    }

    /**
     * @param presentationHint The presentationHint
     */
    @JsonProperty("presentationHint")
    public void setPresentationHint(String presentationHint) {
        this.presentationHint = presentationHint;
    }

    /**
     * @return The name
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("ScorableConfigData{");
        sb.append("units=").append(units);
        sb.append(", resolutions=").append(resolutions);
        sb.append(", aggregations=").append(aggregations);
        sb.append(", orderValue=").append(orderValue);
        sb.append(", calType='").append(calType).append('\'');
        sb.append(", presentationHint='").append(presentationHint).append('\'');
        sb.append(", name='").append(name).append('\'');
        sb.append(", additionalProperties=").append(additionalProperties);
        sb.append('}');
        return sb.toString();
    }
}