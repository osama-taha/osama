package com.hp.printosmobile.data.remote.models;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Anwar Asbah
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
public class NotificationApplicationEventData {

    @JsonProperty("eventDescriptionList")
    private List<EventDescriptionData> eventDescriptionList;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("applicationList")
    public List<EventDescriptionData> getEventDescriptionList() {
        return eventDescriptionList;
    }

    @JsonProperty("applicationList")
    public void setEventDescriptionList(List<EventDescriptionData> eventDescriptionList) {
        this.eventDescriptionList = eventDescriptionList;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class EventDescriptionData {
        @JsonProperty("eventDescriptionName")
        private String eventDescriptionName;
        @JsonProperty("mobile")
        private boolean mobile;
        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<String, Object>();

        @JsonProperty("eventDescriptionName")
        public String getEventDescriptionName() {
            return eventDescriptionName;
        }

        @JsonProperty("eventDescriptionName")
        public void setEventDescriptionName(String eventDescriptionName) {
            this.eventDescriptionName = eventDescriptionName;
        }

        @JsonProperty("mobile")
        public boolean isMobile() {
            return mobile;
        }

        @JsonProperty("mobile")
        public void setMobile(boolean mobile) {
            this.mobile = mobile;
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }
    }
}
