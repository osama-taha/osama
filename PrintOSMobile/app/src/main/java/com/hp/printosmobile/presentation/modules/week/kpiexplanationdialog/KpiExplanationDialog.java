package com.hp.printosmobile.presentation.modules.week.kpiexplanationdialog;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.widget.TextView;

import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.modules.kpiview.KPIViewModel;
import com.hp.printosmobilelib.ui.widgets.HPViewPager;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Anwar Asbah on 7/8/2017.
 */

public class KpiExplanationDialog extends DialogFragment implements ViewPager.OnPageChangeListener {

    private static final String TAG = KpiExplanationDialog.class.getSimpleName();
    private static final String KPI_MODELS_ARG = "kpi_models_args";
    private static final String KPI_INDEX_ARG = "kpi_index_args";
    private static final long VISIBILITY_DELAY = 100;

    @Bind(R.id.dialog_layout)
    View dialogLayout;
    @Bind(R.id.header_layout)
    View headerLayout;
    @Bind(R.id.kpi_view_pager)
    HPViewPager kpiViewPager;
    @Bind(R.id.prev_kpi_name)
    TextView prevKpiName;
    @Bind(R.id.next_kpi_name)
    TextView nextKpiName;
    @Bind(R.id.buttons_layout)
    View buttonsLayout;

    List<KPIViewModel> kpiList;
    int selectedKpiIndex;
    KpiExplanationDialogCallback callback;
    KpiExplanationAdapter kpiExplanationAdapter;

    public static KpiExplanationDialog newInstance(ArrayList<KPIViewModel> kpiList, int selectedKpiIndex,
                                                   KpiExplanationDialogCallback callback) {
        KpiExplanationDialog dialog = new KpiExplanationDialog();
        dialog.callback = callback;
        Bundle bundle = new Bundle();
        bundle.putSerializable(KPI_MODELS_ARG, kpiList);
        bundle.putInt(KPI_INDEX_ARG, selectedKpiIndex);
        dialog.setArguments(bundle);
        return dialog;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle bundle = getArguments();
        if (bundle != null) {
            if (bundle.containsKey(KPI_MODELS_ARG)) {
                kpiList = (List<KPIViewModel>) bundle.getSerializable(KPI_MODELS_ARG);
            }
            if (bundle.containsKey(KPI_INDEX_ARG)) {
                selectedKpiIndex = bundle.getInt(KPI_INDEX_ARG);
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.dialog_kpi_explanation, container, false);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        init();
    }

    private void init() {

        kpiViewPager.setVisibility(View.INVISIBLE);

        kpiExplanationAdapter = new KpiExplanationAdapter(getActivity(), kpiList);
        kpiViewPager.setAdapter(kpiExplanationAdapter);
        kpiViewPager.setCurrentItem(selectedKpiIndex, false);
        updateNextPrevButtons();

        kpiExplanationAdapter.notifyDataSetChanged();

        kpiViewPager.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                kpiViewPager.getViewTreeObserver().removeOnGlobalLayoutListener(this);

                int available = dialogLayout.getMeasuredHeight()
                        - dialogLayout.getPaddingTop()
                        - dialogLayout.getPaddingBottom()
                        - headerLayout.getMeasuredHeight()
                        - buttonsLayout.getMeasuredHeight();
                kpiViewPager.setMaxHeight(available);

                kpiViewPager.addOnPageChangeListener(KpiExplanationDialog.this);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        final View firstKpiView = kpiViewPager.findViewWithTag(selectedKpiIndex);
                        kpiViewPager.measureCurrentView(firstKpiView);
                        kpiViewPager.setVisibility(View.VISIBLE);

                    }
                }, VISIBILITY_DELAY);
            }
        });

        dialogLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                close();
            }
        });
    }

    private void updateNextPrevButtons() {

        if (kpiList == null) {
            return;
        }

        int nextIndex = (selectedKpiIndex + 1) % kpiList.size();
        int prevIndex = (selectedKpiIndex - 1);
        prevIndex = prevIndex < 0 ? kpiList.size() - 1 : prevIndex;

        KPIViewModel prevKpi = kpiList.get(prevIndex);
        KPIViewModel nextKpi = kpiList.get(nextIndex);

        prevKpiName.setText(prevKpi == null || prevKpi.getLocalizedName() == null ? "" :
                prevKpi.getLocalizedName());
        nextKpiName.setText(nextKpi == null || nextKpi.getLocalizedName() == null ? "" :
                nextKpi.getLocalizedName());
    }

    @OnClick(R.id.prev_kpi_button)
    public void onPrevKpiClicked() {
        if (kpiViewPager == null || kpiViewPager.getChildCount() == 0) {
            return;
        }

        selectedKpiIndex = (selectedKpiIndex + -1);
        selectedKpiIndex = selectedKpiIndex < 0 ? kpiList.size() - 1 : selectedKpiIndex;
        kpiViewPager.setCurrentItem(selectedKpiIndex, true);
        //updateNextPrevButtons();
    }

    @OnClick(R.id.next_kpi_button)
    public void onNextKpiClicked() {
        if (kpiViewPager == null || kpiViewPager.getChildCount() == 0) {
            return;
        }

        selectedKpiIndex = (selectedKpiIndex + 1) % kpiList.size();
        kpiViewPager.setCurrentItem(selectedKpiIndex, true);
        //updateNextPrevButtons();
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        }
    }

    @OnClick(R.id.close_button)
    public void close() {
        dismiss();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (callback != null) {
            callback.onDialogDismiss();
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        if (positionOffset == 0) {
            kpiViewPager.measureCurrentView(kpiViewPager.findViewWithTag(position));
        }
    }

    @Override
    public void onPageSelected(int position) {
        selectedKpiIndex = position;
        updateNextPrevButtons();
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    public interface KpiExplanationDialogCallback {
        void onDialogDismiss();
    }
}
