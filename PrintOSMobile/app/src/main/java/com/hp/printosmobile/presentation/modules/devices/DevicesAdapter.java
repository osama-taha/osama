package com.hp.printosmobile.presentation.modules.devices;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.data.remote.models.PreferencesData;
import com.hp.printosmobile.presentation.modules.devicedetails.JobModel;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;
import com.hp.printosmobile.presentation.modules.shared.BaseDeviceViewModel;
import com.hp.printosmobile.presentation.modules.shared.DeviceExtraData;
import com.hp.printosmobile.presentation.modules.shared.DeviceState;
import com.hp.printosmobile.presentation.modules.shared.DeviceViewModel;
import com.hp.printosmobile.presentation.modules.shared.DevicesUtils;
import com.hp.printosmobile.presentation.modules.shared.PrinterUtils;
import com.hp.printosmobile.presentation.modules.shared.Unit;
import com.hp.printosmobile.utils.HPLocaleUtils;
import com.hp.printosmobile.utils.HPScoreUtils;
import com.hp.printosmobilelib.ui.widgets.HPProgressBar;
import com.hp.printosmobilelib.ui.widgets.HPSegmentedRecyclerView;
import com.hp.printosmobilelib.ui.widgets.TypefaceManager;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Osama Taha on 5/15/2016.
 * TODO: This class needs to be merged with the TodayDevicesAdapter class.
 */
public class DevicesAdapter extends HPSegmentedRecyclerView.SegmentedRecyclerViewAdapter<
        DevicesAdapter.DeviceViewHolder> {

    private static final int TYPE_INDIGO_NON_RT_DEVICE = 0;
    private static final int TYPE_INDIGO_RT_DEVICE = 1;
    private static final int TYPE_PWP_DEFAULT = 2;
    private static final int TYPE_SCITEX_DEFAULT = 3;
    private static final int TYPE_LATEX_DEFAULT = 4;
    private static final int TYPE_LATEX_DATA_NOT_AVAILABLE = 5;
    private static final int TYPE_LATEX_DATA_IS_NOT_UP_TO_DATE = 6;
    private static final int TYPE_LATEX_WRONG_TIME_ZONE = 7;
    private static final int TYPE_INDIGO_LOCKED_DEVICE = 8;

    private static final int JOB_NAME_MAX_CHAR = 15;

    private static Map<Integer, Comparator<BaseDeviceViewModel>> comparatorList = new HashMap<>();
    private static List<Integer> orderedComparatorNamesIndigo = new ArrayList<>();
    private static List<Integer> orderedComparatorNamesLatex = new ArrayList<>();
    private static List<Integer> orderedComparatorNamesPWP = new ArrayList<>();

    static {

        comparatorList.put(R.string.fragment_devices_name_comparator, BaseDeviceViewModel.NAME_COMPARATOR);
        comparatorList.put(R.string.fragment_devices_status_comparator, BaseDeviceViewModel.STATUS_COMPARATOR);

        orderedComparatorNamesIndigo.add(R.string.fragment_devices_name_comparator);
        orderedComparatorNamesLatex.add(R.string.fragment_devices_status_comparator);
        orderedComparatorNamesLatex.add(R.string.fragment_devices_name_comparator);
        orderedComparatorNamesPWP.add(R.string.fragment_devices_name_comparator);
    }

    private final PreferencesData.UnitSystem unitSystem;

    public Context context;
    public List<? extends BaseDeviceViewModel> selectedDeviceViewModels;
    private DevicesAdapterListener listener;
    private BusinessUnitEnum businessUnitEnum;

    public DevicesAdapter(Context context, List<? extends BaseDeviceViewModel> deviceViewModels, BusinessUnitEnum businessUnit, DevicesAdapterListener listener) {

        super(businessUnit == BusinessUnitEnum.INDIGO_PRESS ? orderedComparatorNamesIndigo
                        : businessUnit == BusinessUnitEnum.LATEX_PRINTER ? orderedComparatorNamesLatex
                        : orderedComparatorNamesPWP,
                context, getSegmentsStyle(context));

        this.context = context;
        this.listener = listener;
        this.businessUnitEnum = businessUnit;

        Collections.sort(deviceViewModels, comparatorList.get((businessUnit == BusinessUnitEnum.INDIGO_PRESS ? orderedComparatorNamesIndigo
                : businessUnit == BusinessUnitEnum.LATEX_PRINTER ? orderedComparatorNamesLatex
                : orderedComparatorNamesPWP).get(0)));
        selectedDeviceViewModels = deviceViewModels;
        unitSystem = PrintOSPreferences.getInstance(context).getUnitSystem();
    }

    private static HPSegmentedRecyclerView.SegmentStyle getSegmentsStyle(Context context) {

        Typeface tabTextTypeFace = TypefaceManager.getTypeface(context, TypefaceManager.HPTypeface.HP_REGULAR);
        float tabTextFontSize = context.getResources().getDimension(R.dimen.app_default_font_size);
        HPSegmentedRecyclerView.SegmentStyle segmentStyle = new HPSegmentedRecyclerView
                .SegmentStyle(R.color.hp_default, R.color.c62, tabTextTypeFace, tabTextTypeFace, tabTextFontSize);
        return segmentStyle;
    }


    @Override
    public int getItemViewType(int position) {

        DeviceViewModel deviceViewModel = (DeviceViewModel) selectedDeviceViewModels.get(position);

        switch (businessUnitEnum) {
            case INDIGO_PRESS:
                return deviceViewModel.getMessage() != null ? TYPE_INDIGO_LOCKED_DEVICE :
                        deviceViewModel.isRTSupported() ? TYPE_INDIGO_RT_DEVICE : TYPE_INDIGO_NON_RT_DEVICE;
            case SCITEX_PRESS:
                return TYPE_SCITEX_DEFAULT;
            case LATEX_PRINTER:
                return getLatexItemViewType(deviceViewModel);
            case IHPS_PRESS:
                return TYPE_PWP_DEFAULT;
        }

        return -1;

    }

    private int getLatexItemViewType(DeviceViewModel deviceViewModel) {

        switch (deviceViewModel.getDeviceLastUpdateType()) {
            case DATA_UP_TO_DATE:
                return TYPE_LATEX_DEFAULT;
            case LAST_UPDATE_IN_24HRS_TODAY:
            case LAST_UPDATE_IN_24HRS_YESTERDAY:
                return TYPE_LATEX_DATA_IS_NOT_UP_TO_DATE;
            case LAST_UPDATE_YESTERDAY:
            case LAST_UPDATE_DAYS_AGO:
            case DATA_UNAVAILABLE:
                return TYPE_LATEX_DATA_NOT_AVAILABLE;
            case WRONG_TIME_ZONE:
                return TYPE_LATEX_WRONG_TIME_ZONE;
            default:
                return TYPE_LATEX_DEFAULT;
        }
    }

    @Override
    public DeviceViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(context);

        switch (viewType) {

            case TYPE_INDIGO_RT_DEVICE:
                return new IndigoDeviceRTViewHolder(inflater
                        .inflate(R.layout.devices_indigo_rt_device_layout, parent, false));
            case TYPE_INDIGO_LOCKED_DEVICE:
                return new IndigoLockedDeviceViewHolder(inflater
                        .inflate(R.layout.device_indigo_locked_layout, parent, false));
            case TYPE_INDIGO_NON_RT_DEVICE:
                return new IndigoDeviceViewHolder(inflater
                        .inflate(R.layout.devices_indigo_non_rt_device_layout, parent, false));
            case TYPE_SCITEX_DEFAULT:
                return new ScitexDeviceRTViewHolder(inflater
                        .inflate(R.layout.devices_scitex_device_layout, parent, false));
            case TYPE_LATEX_DEFAULT:
                return new LatexDefaultDeviceViewHolder(inflater
                        .inflate(R.layout.devices_latex_device_default_layout, parent, false));
            case TYPE_LATEX_DATA_NOT_AVAILABLE:
                return new LatexDeviceDataUnavailableViewHolder(inflater
                        .inflate(R.layout.devices_latex_device_data_not_unavailable_layout, parent, false));
            case TYPE_LATEX_DATA_IS_NOT_UP_TO_DATE:
                return new LatexDeviceDataUpToDateViewHolder(inflater
                        .inflate(R.layout.latex_device_data_no_up_to_date, parent, false));
            case TYPE_LATEX_WRONG_TIME_ZONE:
                return new LatexDeviceWrongTimeZoneViewHolder(inflater
                        .inflate(R.layout.latex_device_wrong_time_zone, parent, false));
            case TYPE_PWP_DEFAULT:
                return new PWPDeviceViewHolder(inflater
                        .inflate(R.layout.devices_pwp_device_layout, parent, false));
        }

        return null;
    }

    @Override
    public void onBindViewHolder(DeviceViewHolder viewHolder, int position) {

        DeviceViewModel deviceViewModel = (DeviceViewModel) selectedDeviceViewModels.get(position);

        viewHolder.model = deviceViewModel;

        switch (viewHolder.getItemViewType()) {

            case TYPE_INDIGO_RT_DEVICE: {

                IndigoDeviceRTViewHolder indigoDeviceRTViewHolder = (IndigoDeviceRTViewHolder) viewHolder;

                Picasso.with(context).load(deviceViewModel.getDeviceImageResource()).into(indigoDeviceRTViewHolder.deviceImageView);

                String impressionType = deviceViewModel.getImpressionType();
                String deviceName = deviceViewModel.getName();
                String printerName = impressionType == null ? deviceName : String.format("%s (%s)", deviceViewModel.getName(), impressionType);

                indigoDeviceRTViewHolder.deviceNameTextView.setText(printerName);

                if (deviceViewModel.isPressDisconnected()) {
                    indigoDeviceRTViewHolder.deviceStateTextView.setText(context.getString(R.string.press_disconnected));
                    indigoDeviceRTViewHolder.deviceStateTextView.setTextColor(ResourcesCompat.getColor(context.getResources(), R.color.c1, null));
                    indigoDeviceRTViewHolder.timeInStateView.setVisibility(View.GONE);
                } else {
                    indigoDeviceRTViewHolder.deviceStateTextView.setText(DevicesUtils.getDeviceStateString(context, deviceViewModel.getDeviceState()));
                    indigoDeviceRTViewHolder.deviceStateTextView.setTextColor(ResourcesCompat.getColor(context.getResources(), deviceViewModel.getDeviceState().getColor(), null));

                    int timeInState = deviceViewModel.getTimeInStateMin();
                    if (timeInState == 0) {
                        indigoDeviceRTViewHolder.timeInState.setText(context.getString(R.string.press_entered_state));
                        indigoDeviceRTViewHolder.timeInState.setTextColor(ResourcesCompat.getColor(context.getResources(),
                                deviceViewModel.getDeviceState().getColor(), null));
                    } else {
                        indigoDeviceRTViewHolder.timeInState.setText(HPLocaleUtils.getLocalizedTimeString(context, timeInState, TimeUnit.MINUTES));
                        indigoDeviceRTViewHolder.timeInState.setTextColor(ResourcesCompat.getColor(context.getResources(),
                                R.color.hp_default, null));
                    }

                }


                updateImp(indigoDeviceRTViewHolder.printingVolumeNominatorTextView, indigoDeviceRTViewHolder.printingVolumeDenominatorTextView, deviceViewModel.getPrintVolumeValue(), deviceViewModel.getPrintVolumeIntraDailyTargetValue(), deviceViewModel.getPrintVolumeShiftTargetValue());

                DevicesUtils.updateProgressBar(context, indigoDeviceRTViewHolder.progressBar,
                        deviceViewModel.getPrintVolumeShiftTargetValue(),
                        deviceViewModel.getPrintVolumeIntraDailyTargetValue(),
                        deviceViewModel.getPrintVolumeValue());

                int jobsInQueue = deviceViewModel.getJobsInQueue();
                indigoDeviceRTViewHolder.jobsInQueueTextView.setText(DevicesUtils.getJobsInQueueString(context, jobsInQueue));

                int durationToDone = deviceViewModel.getDurationToDone();
                if (durationToDone > 0) {
                    indigoDeviceRTViewHolder.printingTimeTextView.setText(HPLocaleUtils.getLocalizedTimeString(context, deviceViewModel.getDurationToDone(), deviceViewModel.getDurationToDoneUnit()));
                }

                indigoDeviceRTViewHolder.timeView.setVisibility(jobsInQueue == 0 || durationToDone <= 0 ? View.GONE : View.VISIBLE);

                indigoDeviceRTViewHolder.hasAdviceIcon.setVisibility(deviceViewModel.hasUnsuppressedAdvices() ? View.VISIBLE : View.GONE);
            }
            break;

            case TYPE_SCITEX_DEFAULT: {

                ScitexDeviceRTViewHolder scitexDeviceRTViewHolder = (ScitexDeviceRTViewHolder) viewHolder;

                Picasso.with(context).load(deviceViewModel.getDeviceImageResource()).into(scitexDeviceRTViewHolder.deviceImageView);

                scitexDeviceRTViewHolder.deviceNameTextView.setText(deviceViewModel.getName());

                if (deviceViewModel.isPressDisconnected() || deviceViewModel.getState() == DeviceState.UNKNOWN) {

                    scitexDeviceRTViewHolder.deviceStateTextView.setText(context.getString(R.string.press_disconnected));
                    scitexDeviceRTViewHolder.deviceStateTextView.setTextColor(ResourcesCompat.getColor(context.getResources(), R.color.c1, null));
                    scitexDeviceRTViewHolder.timeInStateContainer.setVisibility(View.GONE);

                } else {

                    scitexDeviceRTViewHolder.deviceStateTextView.setText(DevicesUtils.getDeviceStateString(context, deviceViewModel.getDeviceState()));
                    scitexDeviceRTViewHolder.deviceStateTextView.setTextColor(ResourcesCompat.getColor(context.getResources(), deviceViewModel.getDeviceState().getColor(), null));

                    int timeInState = deviceViewModel.getTimeInStateMin();

                    if (timeInState == 0) {
                        scitexDeviceRTViewHolder.printingTimeTextView.setText(context.getString(R.string.press_entered_state));
                        scitexDeviceRTViewHolder.printingTimeTextView.setTextColor(ResourcesCompat.getColor(context.getResources(),
                                deviceViewModel.getDeviceState().getColor(), null));
                    } else {
                        scitexDeviceRTViewHolder.printingTimeTextView.setText(HPLocaleUtils.getLocalizedTimeString(context, timeInState, TimeUnit.MINUTES));
                        scitexDeviceRTViewHolder.printingTimeTextView.setTextColor(ResourcesCompat.getColor(context.getResources(),
                                R.color.hp_default, null));
                    }
                    scitexDeviceRTViewHolder.timeInStateContainer.setVisibility(View.VISIBLE);

                }

                String printVolume = Unit.SQM.getUnitTextWithValue(context, unitSystem, Unit.UnitStyle.VALUE, HPLocaleUtils.getLocalizedValue((int) deviceViewModel.getPrintVolumeValue()));
                scitexDeviceRTViewHolder.printVolumeTextView.setText(printVolume);

                if (deviceViewModel.getSheets() > 0) {
                    scitexDeviceRTViewHolder.sheetsPrintedTextView.setText(DevicesUtils.getSheetsPrinted(context, deviceViewModel.getSheets()));
                } else {
                    scitexDeviceRTViewHolder.sheetsPrintedTextView.setVisibility(View.GONE);
                }

                if (deviceViewModel.getLitersConsumed() > 0) {
                    scitexDeviceRTViewHolder.litersConsumedTextView.setText(DevicesUtils.getLitersConsumed(context, unitSystem, deviceViewModel.getLitersConsumed()));
                } else {
                    scitexDeviceRTViewHolder.litersConsumedTextView.setVisibility(View.GONE);
                }

                DevicesUtils.updateProgressBar(context, scitexDeviceRTViewHolder.progressBar,
                        deviceViewModel.getPrintVolumeShiftTargetValue(),
                        deviceViewModel.getPrintVolumeIntraDailyTargetValue(),
                        deviceViewModel.getPrintVolumeValue());

            }
            break;

            case TYPE_INDIGO_NON_RT_DEVICE: {

                IndigoDeviceViewHolder indigoDeviceViewHolder = (IndigoDeviceViewHolder) viewHolder;

                Picasso.with(context).load(deviceViewModel.getDeviceImageResource()).into(indigoDeviceViewHolder.deviceImageView);

                String impressionType = deviceViewModel.getImpressionType();
                String printerName = impressionType == null ? deviceViewModel.getName()
                        : String.format("%s (%s)", deviceViewModel.getName(), impressionType);

                indigoDeviceViewHolder.deviceNameTextView.setText(printerName);
                indigoDeviceViewHolder.hasAdviceIcon.setVisibility(deviceViewModel.hasUnsuppressedAdvices() ? View.VISIBLE : View.GONE);

            }
            break;

            case TYPE_LATEX_DATA_NOT_AVAILABLE: {

                LatexDeviceDataUnavailableViewHolder latexDeviceDataUnavailableViewHolder = (LatexDeviceDataUnavailableViewHolder) viewHolder;

                latexDeviceDataUnavailableViewHolder.printerNameTextView.setText(getPrinterName(deviceViewModel));

                latexDeviceDataUnavailableViewHolder.dataUnavailableTextView.setText(deviceViewModel.getDeviceLastUpdateType()
                        .getDisplayMessage(context, deviceViewModel.getLastUpdateTime()));

                Picasso.with(context)
                        .load(deviceViewModel.getDeviceImageResource())
                        .into(latexDeviceDataUnavailableViewHolder.printerImageView);
            }

            break;

            case TYPE_LATEX_WRONG_TIME_ZONE: {

                LatexDeviceWrongTimeZoneViewHolder latexDeviceWrongTimeZoneViewHolder = (LatexDeviceWrongTimeZoneViewHolder) viewHolder;

                latexDeviceWrongTimeZoneViewHolder.printerNameTextView.setText(getPrinterName(deviceViewModel));

                latexDeviceWrongTimeZoneViewHolder.msgTextView.setText(deviceViewModel.getDeviceLastUpdateType()
                        .getDisplayMessage(context, deviceViewModel.getLastUpdateTime()));

                Picasso.with(context)
                        .load(deviceViewModel.getDeviceImageResource())
                        .into(latexDeviceWrongTimeZoneViewHolder.printerImageView);
            }

            break;

            case TYPE_LATEX_DATA_IS_NOT_UP_TO_DATE: {

                LatexDeviceDataUpToDateViewHolder latexDeviceDataUpToDateViewHolder = (LatexDeviceDataUpToDateViewHolder) viewHolder;
                latexDeviceDataUpToDateViewHolder.printerNameTextView.setText(getPrinterName(deviceViewModel));

                latexDeviceDataUpToDateViewHolder.printerNameTextView.setText(getPrinterName(deviceViewModel));

                latexDeviceDataUpToDateViewHolder.dataUnavailableTextView.setText(deviceViewModel.getDeviceLastUpdateType()
                        .getDisplayMessage(context, deviceViewModel.getLastUpdateTime()));

                Picasso.with(context)
                        .load(deviceViewModel.getDeviceImageResource())
                        .into(latexDeviceDataUpToDateViewHolder.printerImageView);

                double shiftTarget = deviceViewModel.getPrintVolumeShiftTargetValue();
                double dailyTarget = deviceViewModel.getPrintVolumeIntraDailyTargetValue();

                latexDeviceDataUpToDateViewHolder.printVolumeUnitTextView.setText(Unit.SQM.getUnitText(context, unitSystem, Unit.UnitStyle.DESCRIPTION));

                latexDeviceDataUpToDateViewHolder.printingVolumeNominatorTextView
                        .setText(HPLocaleUtils.getLocalizedValue((int) deviceViewModel.getPrintVolumeValue()));

                latexDeviceDataUpToDateViewHolder.printingVolumeNominatorTextView
                        .setTextColor(HPScoreUtils.getProgressColor(context, deviceViewModel.getPrintVolumeValue(), dailyTarget, false));

                latexDeviceDataUpToDateViewHolder.printingVolumeDenominatorTextView
                        .setText(context.getString(R.string.today_imp_denominator, HPLocaleUtils.getLocalizedValue((int) shiftTarget)));

                DevicesUtils.updateProgressBar(context, latexDeviceDataUpToDateViewHolder.progressBar,
                        deviceViewModel.getPrintVolumeShiftTargetValue(),
                        deviceViewModel.getPrintVolumeIntraDailyTargetValue(),
                        deviceViewModel.getPrintVolumeValue(), true);

            }

            break;

            case TYPE_LATEX_DEFAULT: {

                LatexDefaultDeviceViewHolder latexDefaultDeviceViewHolder = (LatexDefaultDeviceViewHolder) viewHolder;

                DeviceExtraData deviceExtraData = deviceViewModel.getExtraData();

                latexDefaultDeviceViewHolder.printerNameTextView.setText(getPrinterName(deviceViewModel));

                Picasso.with(context)
                        .load(deviceViewModel.getDeviceImageResource())
                        .into(latexDefaultDeviceViewHolder.printerImageView);

                Picasso.with(context)
                        .load(PrinterUtils.getPrinterStateImage(deviceViewModel.getState()))
                        .into(latexDefaultDeviceViewHolder.imagePrinterStatus);

                latexDefaultDeviceViewHolder.printVolumeUnitTextView.setText(Unit.SQM.getUnitText(context, unitSystem, Unit.UnitStyle.DESCRIPTION));

                latexDefaultDeviceViewHolder.hasAdviceImage.setVisibility(deviceViewModel.hasUnsuppressedAdvices() ? View.VISIBLE : View.INVISIBLE);

                int timeInState = deviceViewModel.getTimeInStateMin();

                if (timeInState == 0) {
                    latexDefaultDeviceViewHolder.printingTimeTextView.setText(context.getString(R.string.press_entered_state));
                    latexDefaultDeviceViewHolder.printingTimeTextView.setTextColor(ResourcesCompat.getColor(context.getResources(),
                            deviceViewModel.getDeviceState().getColor(), null));
                } else {
                    latexDefaultDeviceViewHolder.printingTimeTextView.setText(HPLocaleUtils.getLocalizedTimeString(context, timeInState, TimeUnit.MINUTES));
                    latexDefaultDeviceViewHolder.printingTimeTextView.setTextColor(ResourcesCompat.getColor(context.getResources(),
                            R.color.hp_default, null));
                }

                double shiftTarget = deviceViewModel.getPrintVolumeShiftTargetValue();
                double dailyTarget = deviceViewModel.getPrintVolumeIntraDailyTargetValue();

                latexDefaultDeviceViewHolder.printingVolumeNominatorTextView
                        .setText(HPLocaleUtils.getLocalizedValue((int)deviceViewModel.getPrintVolumeValue()));

                latexDefaultDeviceViewHolder.printingVolumeNominatorTextView
                        .setTextColor(HPScoreUtils.getProgressColor(context, deviceViewModel.getPrintVolumeValue(), dailyTarget, false));

                latexDefaultDeviceViewHolder.printingVolumeDenominatorTextView
                        .setText(context.getString(R.string.today_imp_denominator, HPLocaleUtils.getLocalizedValue((int) shiftTarget)));

                if (deviceViewModel.hasExtraData() && deviceExtraData.getFrontPanelMessage() != null) {
                    latexDefaultDeviceViewHolder.printerStatusMessage.setVisibility(View.VISIBLE);
                    latexDefaultDeviceViewHolder.printerStatusMessage.setText(deviceViewModel.getExtraData().getFrontPanelMessage());
                } else {
                    latexDefaultDeviceViewHolder.printerStatusMessage.setVisibility(View.GONE);
                }

                DevicesUtils.updateTimeInState(context, deviceViewModel, BusinessUnitEnum.LATEX_PRINTER, latexDefaultDeviceViewHolder.printingTimeTextView
                        , latexDefaultDeviceViewHolder.deviceStateTextView);

                SpannableStringBuilder jobsInQueueLabel;

                if (!TextUtils.isEmpty(deviceViewModel.getCurrentJob())) {

                    latexDefaultDeviceViewHolder.currentJobContainer.setVisibility(View.VISIBLE);
                    latexDefaultDeviceViewHolder.currentJobName.setText(getTruncatedJobName(deviceViewModel.getCurrentJob()));

                    if (deviceViewModel.getCurrentJobTimeToDone() > -1) {
                        latexDefaultDeviceViewHolder.currentJobTimeContainer.setVisibility(View.VISIBLE);
                        String currentJobTimeToDoneString = HPLocaleUtils.getLocalizedTimeString(context, deviceViewModel.getCurrentJobTimeToDone(),
                                deviceViewModel.getCurrentJobTimeToDoneUnit());
                        latexDefaultDeviceViewHolder.currentJobTimeToCompleteTextView.setText(currentJobTimeToDoneString);

                    } else {
                        latexDefaultDeviceViewHolder.currentJobTimeContainer.setVisibility(View.GONE);
                    }

                } else {

                    int jobsInQueue = 0;
                    List<JobModel> futureJobs;
                    if (deviceViewModel.hasExtraData()) {
                        futureJobs = deviceExtraData.getPrintingJobs();
                        if (futureJobs != null) {
                            jobsInQueue = futureJobs.size();
                        }
                    }

                    jobsInQueueLabel = DevicesUtils.getJobsInQueueString(context, jobsInQueue);

                    latexDefaultDeviceViewHolder.currentJobName.setText(jobsInQueueLabel);
                    latexDefaultDeviceViewHolder.currentJobTimeContainer.setVisibility(View.GONE);

                }

                if (deviceViewModel.getState() == DeviceState.NULL) {
                    latexDefaultDeviceViewHolder.timeInStateContainer.setVisibility(View.GONE);
                } else {
                    latexDefaultDeviceViewHolder.timeInStateContainer.setVisibility(View.VISIBLE);
                }

                DevicesUtils.updateProgressBar(context, latexDefaultDeviceViewHolder.progressBar,
                        deviceViewModel.getPrintVolumeShiftTargetValue(),
                        deviceViewModel.getPrintVolumeIntraDailyTargetValue(),
                        deviceViewModel.getPrintVolumeValue());

            }

            break;

            case TYPE_PWP_DEFAULT: {

                PWPDeviceViewHolder PWPDeviceViewHolder = (PWPDeviceViewHolder) viewHolder;

                Picasso.with(context).load(deviceViewModel.getDeviceImageResource())
                        .error(R.drawable.press_default)
                        .into(PWPDeviceViewHolder.printerImageView);

                PWPDeviceViewHolder.printerNameTextView.setText(deviceViewModel.getName());
            }

            break;

            case TYPE_INDIGO_LOCKED_DEVICE: {

                IndigoLockedDeviceViewHolder indigoLockedDeviceViewHolder = (IndigoLockedDeviceViewHolder) viewHolder;

                Picasso.with(context).load(deviceViewModel.getDeviceImageResource()).into(indigoLockedDeviceViewHolder.deviceImageView);

                String impressionType = deviceViewModel.getImpressionType();
                String deviceName = deviceViewModel.getName();
                String printerName = impressionType == null ? deviceName : String.format("%s (%s)", deviceViewModel.getName(), impressionType);

                indigoLockedDeviceViewHolder.deviceNameTextView.setText(printerName);

                String msg = deviceViewModel.getMessage() != null && deviceViewModel.getMessage().getMsg() != null ?
                        deviceViewModel.getMessage().getMsg() : "";
                indigoLockedDeviceViewHolder.messageTextView.setText(msg);

            }
            break;
        }

    }

    private String getPrinterName(DeviceViewModel deviceViewModel) {

        if (deviceViewModel.getModel() != null) {
            return context.getString(R.string.printer_name_with_model,
                    deviceViewModel.getName(), deviceViewModel.getModel());
        } else {
            return deviceViewModel.getName();
        }

    }

    private String getTruncatedJobName(String currentJob) {

        String jobName = currentJob;
        String jobNameTruncated = jobName.substring(0, Math.min(JOB_NAME_MAX_CHAR, jobName.length()));
        if (!jobName.equals(jobNameTruncated)) {
            jobNameTruncated += "...";
        }

        return jobNameTruncated;
    }

    private void updateImp(TextView nTextView, TextView dTextView, double value, double target, double total) {
        nTextView.setText(HPLocaleUtils.getLocalizedValue((int) value));
        nTextView.setTextColor(HPScoreUtils.getProgressColor(context, value, target, false));
        dTextView.setText(context.getString(R.string.press_progress_impression_v2, HPLocaleUtils.getLocalizedValue((int) total)));
    }

    @Override
    public int getItemCount() {
        return selectedDeviceViewModels == null ? 0 : selectedDeviceViewModels.size();
    }

    @Override
    public void onSegmentTabClicked(int segmentNameId) {
        Collections.sort(selectedDeviceViewModels, comparatorList.get(segmentNameId));
        notifyDataSetChanged();
        listener.onFilterSelected();

    }

    public void setDeviceData (List<? extends BaseDeviceViewModel> deviceViewModels) {

        Collections.sort(deviceViewModels, comparatorList.get((businessUnitEnum == BusinessUnitEnum.INDIGO_PRESS ? orderedComparatorNamesIndigo
                : businessUnitEnum == BusinessUnitEnum.LATEX_PRINTER ? orderedComparatorNamesLatex
                : orderedComparatorNamesPWP).get(0)));
        selectedDeviceViewModels = deviceViewModels;

        notifyDataSetChanged();
    }

    public class DeviceViewHolder extends RecyclerView.ViewHolder {

        DeviceViewModel model;

        public DeviceViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public class IndigoDeviceBaseViewHolder extends DeviceViewHolder {

        @Bind(R.id.image_printer)
        ImageView deviceImageView;
        @Bind(R.id.text_printer_name)
        TextView deviceNameTextView;

        public IndigoDeviceBaseViewHolder(View itemView) {
            super(itemView);
        }

    }

    class IndigoLockedDeviceViewHolder extends IndigoDeviceBaseViewHolder{

        @Bind(R.id.message_text_view)
        TextView messageTextView;

        DeviceViewModel model;

        public IndigoLockedDeviceViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public class IndigoDeviceViewHolder extends IndigoDeviceBaseViewHolder {

        @Bind(R.id.has_advice_icon)
        View hasAdviceIcon;

        public IndigoDeviceViewHolder(View itemView) {
            super(itemView);
            hasAdviceIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.onAdvicesIconClicked(model);
                    }
                }
            });
        }

    }

    public class IndigoDeviceRTViewHolder extends IndigoDeviceViewHolder {

        @Bind(R.id.image_printer)
        ImageView deviceImageView;
        @Bind(R.id.text_printer_name)
        TextView deviceNameTextView;
        @Bind(R.id.progress_bar)
        HPProgressBar progressBar;
        @Bind(R.id.text_printing_time)
        TextView printingTimeTextView;
        @Bind(R.id.text_printing_state)
        TextView deviceStateTextView;
        @Bind(R.id.impression_nominator_text_view)
        TextView printingVolumeNominatorTextView;
        @Bind(R.id.impression_denominator_text_view)
        TextView printingVolumeDenominatorTextView;
        @Bind(R.id.print_queue_jobs_in_queue_text_view)
        TextView jobsInQueueTextView;
        @Bind(R.id.time_container)
        View timeView;
        @Bind(R.id.text_time_in_state)
        TextView timeInState;
        @Bind(R.id.time_in_state_layout)
        View timeInStateView;

        public IndigoDeviceRTViewHolder(View itemView) {
            super(itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (model != null) {
                        listener.onDeviceClicked(model, true);
                    }
                }
            });
        }
    }

    public class ScitexDeviceRTViewHolder extends DeviceViewHolder {

        @Bind(R.id.image_printer)
        ImageView deviceImageView;
        @Bind(R.id.text_printer_name)
        TextView deviceNameTextView;
        @Bind(R.id.progress_bar)
        HPProgressBar progressBar;
        @Bind(R.id.text_time_in)
        TextView printingTimeTextView;
        @Bind(R.id.text_printing_state)
        TextView deviceStateTextView;
        @Bind(R.id.text_printing_volume)
        TextView printVolumeTextView;
        @Bind(R.id.text_liters_consumed)
        TextView litersConsumedTextView;
        @Bind(R.id.text_sheets_printed)
        TextView sheetsPrintedTextView;
        @Bind(R.id.time_in_state_container)
        View timeInStateContainer;

        public ScitexDeviceRTViewHolder(View itemView) {
            super(itemView);
        }
    }


    public class PWPDeviceViewHolder extends DeviceViewHolder {

        @Bind(R.id.text_printer_name)
        TextView printerNameTextView;
        @Bind(R.id.image_printer)
        ImageView printerImageView;

        public PWPDeviceViewHolder(View itemView) {
            super(itemView);
        }
    }

    public class LatexDeviceViewHolder extends DeviceViewHolder {

        @Bind(R.id.image_printer)
        ImageView printerImageView;
        @Bind(R.id.text_printer_name)
        TextView printerNameTextView;

        public LatexDeviceViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (model != null && !model.isWrongTZ()) {
                        listener.onDeviceClicked(model, true);
                    }
                }
            });
        }
    }

    public class LatexDefaultDeviceViewHolder extends LatexDeviceViewHolder {

        @Bind(R.id.image_has_advice)
        ImageView hasAdviceImage;
        @Bind(R.id.image_printer_state)
        ImageView imagePrinterStatus;
        @Bind(R.id.progress_bar)
        HPProgressBar progressBar;
        @Bind(R.id.text_time_in)
        TextView printingTimeTextView;
        @Bind(R.id.text_printing_state)
        TextView deviceStateTextView;
        @Bind(R.id.text_printing_volume_nominator)
        TextView printingVolumeNominatorTextView;
        @Bind(R.id.text_printing_volume_dinominator)
        TextView printingVolumeDenominatorTextView;
        @Bind(R.id.print_volume_unit_text_view)
        TextView printVolumeUnitTextView;
        @Bind(R.id.text_printer_status_message)
        TextView printerStatusMessage;
        @Bind(R.id.time_in_state_container)
        View timeInStateContainer;
        @Bind(R.id.current_job_container_layout)
        View currentJobContainer;
        @Bind(R.id.current_job_name_text_view)
        TextView currentJobName;
        @Bind(R.id.current_job_time_container)
        View currentJobTimeContainer;
        @Bind(R.id.current_job_time_to_complete_text_view)
        TextView currentJobTimeToCompleteTextView;

        public LatexDefaultDeviceViewHolder(View itemView) {
            super(itemView);
        }

    }

    public class LatexDeviceDataUnavailableViewHolder extends LatexDeviceViewHolder {

        @Bind(R.id.text_data_unavailable)
        TextView dataUnavailableTextView;

        public LatexDeviceDataUnavailableViewHolder(View itemView) {
            super(itemView);
        }

    }

    public class LatexDeviceWrongTimeZoneViewHolder extends LatexDeviceViewHolder {

        @Bind(R.id.msg_text_view)
        TextView msgTextView;

        public LatexDeviceWrongTimeZoneViewHolder(View itemView) {
            super(itemView);
        }

    }

    public class LatexDeviceDataUpToDateViewHolder extends LatexDeviceViewHolder {

        @Bind(R.id.text_data_unavailable)
        TextView dataUnavailableTextView;
        @Bind(R.id.progress_bar)
        HPProgressBar progressBar;
        @Bind(R.id.print_volume_unit_text_view)
        TextView printVolumeUnitTextView;
        @Bind(R.id.text_printing_volume_nominator)
        TextView printingVolumeNominatorTextView;
        @Bind(R.id.text_printing_volume_dinominator)
        TextView printingVolumeDenominatorTextView;

        public LatexDeviceDataUpToDateViewHolder(View itemView) {
            super(itemView);
        }

    }

    public interface DevicesAdapterListener {

        void onDeviceClicked(DeviceViewModel model, boolean fromDevices);

        void onAdvicesIconClicked(DeviceViewModel model);

        void onFilterSelected();
    }
}
