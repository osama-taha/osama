package com.hp.printosmobile.presentation.modules.production;

/**
 * Created by leviasaf on 26/04/2016.
 */
public interface IProductionView {
    void setNumberOfJobs(String numberOfJobs);
}
