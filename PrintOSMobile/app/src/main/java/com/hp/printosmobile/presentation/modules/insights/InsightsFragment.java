package com.hp.printosmobile.presentation.modules.insights;

import android.Manifest;
import android.animation.ObjectAnimator;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.presentation.BaseFragment;
import com.hp.printosmobile.presentation.modules.home.Panel;
import com.hp.printosmobile.presentation.modules.insights.InsightsViewModel.InsightKpiEnum;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitViewModel;
import com.hp.printosmobile.presentation.modules.main.IMainFragment;
import com.hp.printosmobile.presentation.modules.personaladvisor.AdviceViewModel;
import com.hp.printosmobile.presentation.modules.personaladvisor.PersonalAdvisorPanel;
import com.hp.printosmobile.presentation.modules.personaladvisor.PersonalAdvisorViewModel;
import com.hp.printosmobile.presentation.modules.personaladvisor.presenter.PersonalAdvisorFactory;
import com.hp.printosmobile.presentation.modules.shared.DeviceViewModel;
import com.hp.printosmobile.utils.HPUIUtils;
import com.hp.printosmobilelib.core.communications.remote.APIException;
import com.hp.printosmobilelib.core.utils.HPDateUtils;
import com.hp.printosmobilelib.ui.widgets.HPScrollView;
import com.hp.printosmobilelib.ui.widgets.panel.PanelView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;

/**
 * Created by admin on 6/13/2017.
 */

public class InsightsFragment extends BaseFragment implements IMainFragment, InsightsView, KpiInsightPanel.KpiInsightPanelCallback, PersonalAdvisorPanel.PersonalAdvicePanelCallback {

    private static final String API_DATE_FORMAT = "yyyy-MM-dd";
    private static final String ANIMATION_AXIS = "scrollY";
    private static final long SCROLL_TO_ADVICE_DELAY = 600;
    private static final long SCROLL_TO_PANEL_DELAY = 200;
    private static final long SCROLL_ANIMATION_DURATION = 400;
    private static final int READ_WRITE_EXTERNAL_STORAGE_PERMISSION = 1;

    private static final List<Panel> panels = new ArrayList<>();

    static {
        panels.add(Panel.FAILURE_CHART);
        panels.add(Panel.JAM_CHART);
        panels.add(Panel.PERSONAL_ADVISOR);
    }

    @Bind(R.id.insights_fragment_layout)
    View insightsFragmentLayout;
    @Bind(R.id.panels_container)
    LinearLayout panelContainer;
    @Bind(R.id.scroll_view)
    HPScrollView scrollView;
    @Bind(R.id.no_date_text_view)
    TextView noDataTextView;

    private BusinessUnitViewModel businessUnitModel;
    private HashMap<Panel, PanelView> panelMap;
    private HashMap<Panel, Boolean> hasDataMap;
    private InsightsPresenter presenter;
    private Panel panelRequestedSharing;
    private Panel focusablePanel;
    private boolean reloadPanels;
    private boolean wasDataLoaded;
    private InsightsFragmentCallback callback;

    public static InsightsFragment newInstance() {
        return new InsightsFragment();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
    }

    private void init() {
        panelMap = new HashMap<>();
        hasDataMap = new HashMap<>();
        wasDataLoaded = false;
        presenter = new InsightsPresenter();
        presenter.attachView(this);

        scrollView.setOnScrollStoppedListener(new HPScrollView.OnScrollStopListener() {
            @Override
            public void onScrollStopped(int y) {
            }

            @Override
            public void onScrollDown() {
            }

            @Override
            public void onScrollUp() {
                enableAdviceVerticalScrolling(false);
            }

            @Override
            public void onTopReached() {
            }

            @Override
            public void onBottomReached() {
                enableAdviceVerticalScrolling(true);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            callback = (InsightsFragmentCallback) getActivity();
        } catch (ClassCastException e) {
            throw new RuntimeException("parent activity must implement InsightsFragmentCallback");
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_insights;
    }

    @Override
    public boolean isFragmentNameToBeDisplayed() {
        return true;
    }

    @Override
    public boolean isFilteringAvailable() {
        return true;
    }

    @Override
    public int getToolbarDisplayName() {
        return R.string.insights_tab_name;
    }

    @Override
    public boolean isIntercomAccessible() {
        return true;
    }

    @Override
    public void onBusinessUnitSelected(BusinessUnitViewModel businessUnitModel) {
        if (businessUnitModel == null || businessUnitModel.getBusinessUnit() != BusinessUnitEnum.INDIGO_PRESS) {
            return;
        }

        reloadPanels = this.businessUnitModel == null || this.businessUnitModel.getBusinessUnit() != businessUnitModel.getBusinessUnit();
        this.businessUnitModel = businessUnitModel;
        wasDataLoaded = false;

        if(callback != null) {
            callback.onInsightsBusinessUnitSet();
        }
    }

    public void loadScreen () {
        if (wasDataLoaded || businessUnitModel == null || businessUnitModel.getBusinessUnit() != BusinessUnitEnum.INDIGO_PRESS) {
            return;
        }

        wasDataLoaded = true;

        if (reloadPanels) {
            panelContainer.removeAllViews();
            panelMap.clear();
        }

        for (Panel panel : panels) {
            if((panel == Panel.JAM_CHART && !PrintOSPreferences.getInstance(getActivity()).isInsightsJamsEnabled(true)) ||
                    (panel == Panel.FAILURE_CHART && !PrintOSPreferences.getInstance(getActivity()).isInsightsFailuresEnabled(true))){
                continue;
            }

            PanelView panelView = panelMap.get(panel);
            if(panelView instanceof KpiInsightPanel) {
                ((KpiInsightPanel) panelView).resetDateFilter();
            }

            initPanel(panel, reloadPanels);
            getData(panel);
        }

        panelContainer.setVisibility(View.VISIBLE);
    }

    @Override
    public void onFiltersChanged(BusinessUnitViewModel businessUnitViewModel) {

        onBusinessUnitSelected(businessUnitViewModel);
    }

    @Override
    public void didFinishGettingBusinessUnits(Map<BusinessUnitEnum, BusinessUnitViewModel> businessUnitsMap) {
        //do nothing
    }

    @Override
    public void onMainError(String msg, boolean triggerSignOut) {
        panelContainer.setVisibility(View.INVISIBLE);
        noDataTextView.setText(R.string.error_no_data_msg);
        noDataTextView.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean respondToValidationPeriodExceedance() {
        return false;
    }

    private void initPanel(Panel panel, boolean reload) {
        PanelView panelView = null;
        if (reload || !panelMap.containsKey(panel)) {
            LinearLayout.LayoutParams layoutParams = getPanelLayoutParams();
            switch (panel) {
                case JAM_CHART:
                    panelView = new KpiInsightPanel(getActivity());
                    ((KpiInsightPanel) panelView).addCallback(this);
                    panelView.showLoading();
                    break;
                case FAILURE_CHART:
                    panelView = new KpiInsightPanel(getActivity());
                    ((KpiInsightPanel) panelView).addCallback(this);
                    panelView.showLoading();
                    break;
                case PERSONAL_ADVISOR:
                    panelView = new PersonalAdvisorPanel(getActivity());
                    ((PersonalAdvisorPanel) panelView).addPersonalAdvisorPanelCallback(this);
                    PersonalAdvisorFactory.getInstance().addObserver((PersonalAdvisorPanel) panelView);
                    panelView.showLoading();
                    break;
            }
            if (panelView != null) {
                panelMap.put(panel, panelView);
                panelContainer.addView(panelView, layoutParams);
            }
        } else {
            panelView = panelMap.get(panel);
            panelView.showLoading();
        }
    }

    private void getData(Panel panel) {
        if (panelMap == null || !panelMap.containsKey(panel)) {
            return;
        }

        if (hasDataMap.containsKey(panel)) {
            hasDataMap.remove(panel);
        }
        hasDataMap.put(panel, true);

        String from = HPDateUtils.formatDate(Calendar.getInstance().getTime(), API_DATE_FORMAT);
        String to = HPDateUtils.formatDate(Calendar.getInstance().getTime(), API_DATE_FORMAT);

        PanelView panelView = panelMap.get(panel);
        if(panelView instanceof KpiInsightPanel) {
            from = HPDateUtils.formatDate(((KpiInsightPanel) panelView).getDateFrom(), API_DATE_FORMAT);
            to = HPDateUtils.formatDate(((KpiInsightPanel) panelView).getDateTo(), API_DATE_FORMAT);
        }

        switch (panel) {
            case JAM_CHART:
                presenter.getInsightsData(businessUnitModel, InsightKpiEnum.JAM, from, to, false);
                break;
            case FAILURE_CHART:
                presenter.getInsightsData(businessUnitModel, InsightKpiEnum.FAILURE, from, to, false);
                break;
            case PERSONAL_ADVISOR:
                PersonalAdvisorFactory.getInstance().loadData(businessUnitModel);
                break;
        }
    }

    private LinearLayout.LayoutParams getPanelLayoutParams() {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        int verticalMargin = (int) getContext().getResources().getDimension(R.dimen.home_fragment_panels_divider_half_height);
        layoutParams.setMargins(0, 0, 0, 2 * verticalMargin);

        return layoutParams;
    }

    @Override
    public void onInsightDataRetrieved(InsightsViewModel viewModel, InsightKpiEnum kpiEnum, boolean onDateSelected) {
        Panel panelEnum = kpiEnum == InsightKpiEnum.JAM ? Panel.JAM_CHART : Panel.FAILURE_CHART;
        KpiInsightPanel panel = (KpiInsightPanel) panelMap.get(panelEnum);

        boolean hasData = viewModel != null && viewModel.getInsightModels() != null
                && !viewModel.getInsightModels().isEmpty();
        panel.setVisibility(hasData || onDateSelected ? View.VISIBLE : View.GONE);

        if (!onDateSelected) {
            if (hasDataMap.containsKey(panel)) {
                hasDataMap.remove(panel);
            }
            hasDataMap.put(panelEnum, hasData);
        }
        isDataEmpty();

        panel.updateViewModel(viewModel);
    }

    private synchronized void isDataEmpty() {
        boolean isEmpty = true;
        for (Panel key : hasDataMap.keySet()) {
            isEmpty = isEmpty && !hasDataMap.get(key);
        }

        if (isEmpty) {
            boolean isNull = businessUnitModel == null || businessUnitModel.getFiltersViewModel() == null;
            int totalNumberOfDevices = isNull || businessUnitModel.getFiltersViewModel().getSiteDevicesIds() == null ?
                    0 : businessUnitModel.getFiltersViewModel().getSiteDevicesIds().size();
            int selectedNumberOfDevices = isNull || businessUnitModel.getFiltersViewModel().getSelectedDevices() == null ?
                    0 : businessUnitModel.getFiltersViewModel().getSelectedDevices().size();

            noDataTextView.setText(totalNumberOfDevices != selectedNumberOfDevices ?
                    R.string.insights_no_insight_change_filter
                    : R.string.insights_no_insight);

        }

        noDataTextView.setVisibility(isEmpty ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onError(APIException exception, String tag) {
        onInsightDataRetrieved(null, InsightKpiEnum.JAM.getTAG().equals(tag) ? InsightKpiEnum.JAM
                : InsightKpiEnum.FAILURE, false);
        if(callback != null) {
            callback.moveToHomeFromTab(getString(R.string.fragment_insights_name_key));
        }
    }

    @Override
    public void onDateSelected(Date from, Date to, InsightKpiEnum insightKpiEnum) {
        Panel panel = insightKpiEnum == InsightKpiEnum.JAM ? Panel.JAM_CHART : Panel.FAILURE_CHART;
        if (panelMap.containsKey(panel)) {
            panelMap.get(panel).showLoading();
        }

        presenter.getInsightsData(businessUnitModel, insightKpiEnum,
                HPDateUtils.formatDate(from, API_DATE_FORMAT),
                HPDateUtils.formatDate(to, API_DATE_FORMAT), true);
    }

    @Override
    public void onShareButtonClicked(Panel panel) {

        this.panelRequestedSharing = panel;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            int writePermissionGrantedCode = ContextCompat.checkSelfPermission(PrintOSApplication.getAppContext(),
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE);

            int readPermissionGrantedCode = ContextCompat.checkSelfPermission(PrintOSApplication.getAppContext(),
                    Manifest.permission.READ_EXTERNAL_STORAGE);

            if (readPermissionGrantedCode != PackageManager.PERMISSION_GRANTED ||
                    writePermissionGrantedCode != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE},
                        READ_WRITE_EXTERNAL_STORAGE_PERMISSION);

            } else {
                performPanelSharing(panel);
            }
        } else {
            performPanelSharing(panel);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case READ_WRITE_EXTERNAL_STORAGE_PERMISSION: {
                if (grantResults.length > 0) {
                    boolean allGranted = true;
                    for (int i = 0; i < grantResults.length; i++) {
                        allGranted = allGranted && grantResults[i] == PackageManager.PERMISSION_GRANTED;
                    }
                    if (allGranted) {
                        performPanelSharing(panelRequestedSharing);
                    }
                }
                return;
            }
        }
    }

    public void performPanelSharing(Panel panel) {
        if (panelMap.containsKey(panel)) {
            PanelView panelView = panelMap.get(panel);
            panelView.sharePanel();
        }
    }

    @Override
    public void onScreenshotCreated(Panel panel, Uri uri) {
        shareImage(uri);
    }

    @Override
    public void onReadMoreClicked() {
        scrollView.fullScroll(ScrollView.FOCUS_DOWN);
    }

    @Override
    public int getFragmentHeight() {
        return insightsFragmentLayout.getHeight();
    }

    @Override
    public void updatePersonalAdvisorPanel(PersonalAdvisorViewModel personalAdvisorViewModel) {

        if (!panelMap.containsKey(Panel.PERSONAL_ADVISOR)) {
            return;
        }

        PersonalAdvisorPanel personalAdvisorPanel = (PersonalAdvisorPanel) panelMap.get(Panel.PERSONAL_ADVISOR);

        boolean showAdvisorPanel = personalAdvisorViewModel != null && personalAdvisorViewModel.getAllAdvices() != null
                && personalAdvisorViewModel.getAllAdvices().size() > 0;
        HPUIUtils.setVisibility(showAdvisorPanel, personalAdvisorPanel);

        if (hasDataMap.containsKey(Panel.PERSONAL_ADVISOR)) {
            hasDataMap.remove(Panel.PERSONAL_ADVISOR);
        }
        hasDataMap.put(Panel.PERSONAL_ADVISOR, showAdvisorPanel);

        isDataEmpty();

        scrollFragmentToPanel(focusablePanel);

    }

    @Override
    public void onPersonalAdvisorError(APIException e, String tag) {
        if (hasDataMap.containsKey(Panel.PERSONAL_ADVISOR)) {
            hasDataMap.remove(Panel.PERSONAL_ADVISOR);
        }
        hasDataMap.put(Panel.PERSONAL_ADVISOR, false);

        isDataEmpty();
        updatePersonalAdvisorPanel(null);
    }

    public void scrollToNewestAdvice() {
        scrollView.postDelayed(new Runnable() {
            @Override
            public void run() {

                if (panelMap.containsKey(Panel.PERSONAL_ADVISOR)) {
                    PersonalAdvisorPanel personalAdvisorPanel = (PersonalAdvisorPanel) panelMap.get(Panel.PERSONAL_ADVISOR);
                    personalAdvisorPanel.scrollToNewestAdvice();
                }

            }
        }, SCROLL_TO_ADVICE_DELAY);
    }

    private void scrollFragmentToPanel(final Panel panel) {
        if (focusablePanel == null || panelMap == null || !panelMap.containsKey(panel)) {
            return;
        }

        final PanelView panelView = panelMap.get(panel);

        scrollView.postDelayed(new Runnable() {
            @Override
            public void run() {
                scrollView.scrollTo(0, panelView.getTop());
                scrollToNewestAdvice();
                focusablePanel = null;
            }
        }, SCROLL_TO_PANEL_DELAY);

    }

    private void enableAdviceVerticalScrolling(boolean enableScrolling) {
        if (panelMap != null && panelMap.containsKey(Panel.PERSONAL_ADVISOR)) {
            PersonalAdvisorPanel panel = (PersonalAdvisorPanel) panelMap.get(Panel.PERSONAL_ADVISOR);
            panel.enableAdviceScrolling(enableScrolling);
        }
    }

    public void scrollToAdvice(final AdviceViewModel model) {
        onHasAdviceIconClicked(null, model);
    }

    private void onHasAdviceIconClicked(final DeviceViewModel model, final AdviceViewModel adviceViewModel) {
        scrollToBottom();

        scrollView.postDelayed(new Runnable() {
            @Override
            public void run() {

                if (panelMap.containsKey(Panel.PERSONAL_ADVISOR)) {
                    PersonalAdvisorPanel personalAdvisorPanel = (PersonalAdvisorPanel) panelMap.get(Panel.PERSONAL_ADVISOR);
                    personalAdvisorPanel.scrollToAdvice(model, adviceViewModel);
                }

            }
        }, SCROLL_TO_ADVICE_DELAY);
    }

    private void scrollToBottom() {

        View lastChild = scrollView.getChildAt(scrollView.getChildCount() - 1);
        int bottom = lastChild.getBottom() + scrollView.getPaddingBottom();
        int sy = 0;
        int sh = scrollView.getHeight();
        final int delta = bottom - (sy + sh);

        ObjectAnimator objectAnimator = ObjectAnimator.ofInt(scrollView, ANIMATION_AXIS, 0, delta).setDuration(SCROLL_ANIMATION_DURATION);
        objectAnimator.start();
    }

    public void setFocusablePanel(Panel panel) {
        this.focusablePanel = panel;
    }

    @Override
    public boolean onBackPressed() {
        if (callback != null) {
            callback.onInsightsFragmentBackPress();
        }
        return true;
    }

    public interface InsightsFragmentCallback {
        void onInsightsFragmentBackPress();

        void onInsightsBusinessUnitSet();

        void moveToHomeFromTab(String string);
    }
}
