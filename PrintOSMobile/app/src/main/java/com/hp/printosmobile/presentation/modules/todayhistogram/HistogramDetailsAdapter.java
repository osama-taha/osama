package com.hp.printosmobile.presentation.modules.todayhistogram;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hp.printosmobile.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Anwar Asbah on 10/9/2016.
 */
public class HistogramDetailsAdapter extends PagerAdapter {

    public final static String TAG = HistogramDetailsAdapter.class.getSimpleName();
    private static final int MAX_NUMBER_OF_DISPLAYED_DAYS = 7;

    private Context context;
    private TodayHistogramViewModel histogramViewModel;
    private HistogramViewBuilder histogramViewBuilder;
    private int numberOfDaysToDisplay;

    public HistogramDetailsAdapter(Context context, TodayHistogramViewModel histogramViewModel, int numberOfDaysToDisplay) {
        this.context = context;
        this.histogramViewModel = histogramViewModel;
        this.histogramViewBuilder = new HistogramViewBuilder();

        this.numberOfDaysToDisplay = 0;
        if (this.histogramViewModel != null && this.histogramViewModel.getData() != null) {
            this.numberOfDaysToDisplay = Math.min(numberOfDaysToDisplay, histogramViewModel.getData().size());
        }
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View layout = inflater.inflate(R.layout.histogram_details_item, container, false);
        histogramViewBuilder.build(layout, position);
        container.addView(layout);
        return layout;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return (numberOfDaysToDisplay / MAX_NUMBER_OF_DISPLAYED_DAYS)
                + (numberOfDaysToDisplay % MAX_NUMBER_OF_DISPLAYED_DAYS != 0 ? 1 : 0);
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    class HistogramViewBuilder {

        @Bind(R.id.histogram_view)
        TodayHistogramView histogram;

        private void build(View view, int position) {
            ButterKnife.bind(this, view);
            int reversedPosition = getCount() - position - 1;
            int daysSpan = Math.min(MAX_NUMBER_OF_DISPLAYED_DAYS, numberOfDaysToDisplay - MAX_NUMBER_OF_DISPLAYED_DAYS * reversedPosition);
            if (daysSpan > 0) {
                histogram.setDaysSpan(daysSpan - 1);
                histogram.setPaging(true);
                histogram.setPagingSpan(numberOfDaysToDisplay);
                histogram.setOffset(reversedPosition * MAX_NUMBER_OF_DISPLAYED_DAYS);
                histogram.setViewModel(histogramViewModel);
            }
        }
    }
}
