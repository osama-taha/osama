package com.hp.printosmobile.presentation.modules.drawer;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitViewModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Osama Taha on 5/20/16.
 */
public class BusinessUnitsAdapter extends RecyclerView.Adapter<BusinessUnitsAdapter.ViewHolder> {

    private Context mContext;
    private BusinessUnitsAdapterCallbacks mCallbacks;
    private List<BusinessUnitViewModel> mBusinessUnits = new ArrayList<>();

    public BusinessUnitsAdapter(Context context, BusinessUnitsAdapterCallbacks callbacks) {

        this.mCallbacks = callbacks;
        this.mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_business_unit, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        BusinessUnitViewModel businessUnitViewModel = mBusinessUnits.get(position);

        holder.businessUnitName.setText(businessUnitViewModel.getBusinessUnit().getName());

    }

    @Override
    public int getItemCount() {
        return mBusinessUnits.size();
    }

    public void bind(List<BusinessUnitViewModel> businessUnits) {

        this.mBusinessUnits = businessUnits;
        notifyDataSetChanged();

    }

    /**
     * This interface must be implemented by activities/fragments that contain this
     * adapter to allow an interaction.
     */
    public interface BusinessUnitsAdapterCallbacks {
        void onBusinessUnitSelected(BusinessUnitViewModel businessUnitViewModel, boolean resetFilters);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.image_view_business_unit_image)
        ImageView businessUnitImage;
        @Bind(R.id.text_view_business_unit_name)
        TextView businessUnitName;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.text_view_business_unit_name)
        void listItemClicked() {
            if (mCallbacks != null) {
                mCallbacks.onBusinessUnitSelected(mBusinessUnits.get(getAdapterPosition()), true);
            }
        }
    }

}
