package com.hp.printosmobile.presentation.modules.splash;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;

import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.AppseeSdk;
import com.hp.printosmobile.BuildConfig;
import com.hp.printosmobile.Constants;
import com.hp.printosmobile.IntercomSdk;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.presentation.BaseActivity;
import com.hp.printosmobile.presentation.Navigator;
import com.hp.printosmobilelib.core.communications.remote.SessionManager;
import com.hp.printosmobilelib.core.logging.HPLogConfig;
import com.hp.printosmobilelib.core.logging.HPLogger;

import butterknife.Bind;
import butterknife.ButterKnife;
import io.branch.indexing.BranchUniversalObject;
import io.branch.referral.Branch;
import io.branch.referral.BranchError;
import io.branch.referral.util.LinkProperties;

public class SplashActivity extends BaseActivity {

    private static final long SPLASH_DURATION = 2000L;

    private static final String TAG = SplashActivity.class.getName();

    @Bind(R.id.beta_msg_text_view)
    View betaMsgTextView;

    private String universalLinkScreen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AppseeSdk.getInstance(this).sendEvent(AppseeSdk.EVENT_OPEN_APP_FROM_ICON);
        Analytics.sendEvent(Analytics.APP_OPENED_ACTION);

        ButterKnife.bind(this);

        betaMsgTextView.setVisibility(BuildConfig.isBetaVersion ? View.VISIBLE : View.GONE);

        HPLogConfig.getInstance(this).setDispatchingEnabled(false);
    }

    @Override
    protected void onStart() {
        super.onStart();

        Branch branch = Branch.getInstance();
        branch.initSession(new Branch.BranchUniversalReferralInitListener() {
            @Override
            public void onInitFinished(BranchUniversalObject branchUniversalObject, LinkProperties linkProperties, BranchError error) {
                if (error == null && branchUniversalObject != null) {

                    HPLogger.i(TAG, "Referring Branch Universal Object: " + branchUniversalObject.toString());

                    // check if the item is contained in the metadata
                    if (branchUniversalObject.getMetadata().containsKey(Constants.IntentExtras.UNIVERSAL_LINK_SCREEN_KEY)) {
                        universalLinkScreen = branchUniversalObject.getMetadata().get(Constants.IntentExtras.UNIVERSAL_LINK_SCREEN_KEY);
                    }
                }
            }
        }, this.getIntent().getData(), this);

    }

    @Override
    public void onNewIntent(Intent intent) {
        this.setIntent(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();

        AppseeSdk.getInstance(getApplicationContext()).init();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                dismissSplash(isLogInNeeded());
            }
        }, SPLASH_DURATION);
    }

    boolean isLogInNeeded() {
        //TODO: Add presenter, then move the below logic..
        SessionManager sessionManager = SessionManager.getInstance(PrintOSApplication.getAppContext());
        return !sessionManager.hasCookie();
    }

    private void dismissSplash(boolean loginNeeded) {

        if (!isForceUpdating) {

            IntercomSdk.getInstance(getApplicationContext()).login();

            if (loginNeeded) {
                Navigator.openLoginActivity(this);
            } else {
                // We’re logged in, we can register the user with Intercom
                Navigator.openMainActivity(this, universalLinkScreen);

                PrintOSPreferences.getInstance(this).setNumberOfAuthenticatedSessionsForRateDialog(PrintOSPreferences.getInstance(this)
                        .getNumberOfAuthenticatedSessionsForRateDialog() + 1);
                PrintOSPreferences.getInstance(this).setNumberOfAuthenticatedSessionsForTimeZoneDialog(PrintOSPreferences.getInstance(this)
                        .getNumberOfAuthenticatedSessionsForTimeZoneDialog() + 1);
            }

            finish();

        }

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_splash;
    }

    @Override
    protected void onPause() {
        super.onPause();
        //to mimic returning from background
        returningFromBackground = true;
    }
}
