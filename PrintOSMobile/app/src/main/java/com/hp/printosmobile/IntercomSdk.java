package com.hp.printosmobile;

import android.app.Application;
import android.content.Context;

import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.presentation.modules.drawer.OrganizationViewModel;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;
import com.hp.printosmobile.presentation.modules.shared.UserViewModel;
import com.hp.printosmobilelib.core.communications.remote.SessionManager;
import com.hp.printosmobilelib.core.logging.HPLogger;

import io.intercom.android.sdk.Company;
import io.intercom.android.sdk.Intercom;
import io.intercom.android.sdk.UnreadConversationCountListener;
import io.intercom.android.sdk.UserAttributes;
import io.intercom.android.sdk.identity.Registration;

/**
 * Created by Osama Taha on 10/25/16.
 */
public class IntercomSdk {

    private static final String TAG = IntercomSdk.class.getSimpleName();

    private final static String EVENT_WITH_EXTRA = "%s_%s";

    private final static String VIEW_PRINTBEAT_MOBILE_EVENT = "view_printbeat_mobile";
    private final static String CLICK_SHARE_ICON_MOBILE_EVENT = "share_pb_score_mobile";
    private final static String REGISTER_NOTIFICATION_TYPE_EVENT = "mobile_pns_register_%s";
    private final static String UNREGISTER_NOTIFICATION_TYPE_EVENT = "mobile_pns_unregister_%s";
    private final static String CLICK_NOTIFICATION_TYPE_EVENT = "mobile_pns_click_%s";
    private final static String USER_INSTALL_WIDGET_EVENT = "mobile_install_widget";
    private final static String USER_OPEN_APP_BY_WIDGET_EVENT = "mobile_open_app_by_widget";
    private final static String CHANGE_EQUIPMENT_EVENT = "mobile_view_bu_%s";
    private static final String USER_CLICK_DEVICE_IN_WIDGET = "PBM_CLICK_WID_DEVICE";
    private static final String USER_CLICK_GAUGE_IN_WIDGET = "PBM_CLICK_WID_GAUGE";

    public final static String PBM_VIEW_LOGIN_EVENT = "PBM_VIEW_LOGIN";
    public final static String PBM_VIEW_FORGOT_EVENT = "PBM_VIEW_FORGOT";
    public final static String PBM_VIEW_HOME_EVENT = "PBM_VIEW_HOME";
    public final static String PBM_VIEW_7DAY_EVENT = "PBM_VIEW_7DAY";
    public final static String PBM_VIEW_7DAY_DETAILED_EVENT = "PBM_VIEW_7DAY_DETAILED";
    public final static String PBM_VIEW_QUEUED_EVENT = "PBM_VIEW_QUEUED";
    public final static String PBM_VIEW_DEVICELIST_EVENT = "PBM_VIEW_DEVICELIST";
    public final static String PBM_VIEW_DEVICE_EVENT = "PBM_VIEW_DEVICE";
    public final static String PBM_VIEW_REPORTS_EVENT = "PBM_VIEW_REPORTS";
    public final static String PBM_VIEW_NOTIFICATIONS_EVENT = "PBM_VIEW_NOTIFICATIONS";
    public final static String PBM_VIEW_ASK_EVENT = "PBM_VIEW_ASK";
    public final static String PBM_VIEW_SETTINGS_EVENT = "PBM_VIEW_SETTINGS";
    public final static String PBM_VIEW_LANGUAGE_EVENT = "PBM_VIEW_LANGUAGE";
    public final static String PBM_VIEW_NOTIFICATIONS_SETTINGS_EVENT = "PBM_VIEW_NOTIFICATIONS_SETTINGS";
    public final static String PBM_VIEW_PROBLEM_EVENT = "PBM_VIEW_PROBLEM";
    public final static String PBM_VIEW_ABOUT_EVENT = "PBM_VIEW_ABOUT";
    public final static String PBM_VIEW_PRIVACY_EVENT = "PBM_VIEW_PRIVACY";
    public final static String PBM_VIEW_SWITCH_EVENT = "PBM_VIEW_SWITCH";
    public final static String PBM_VIEW_ACCOUNT_EVENT = "PBM_VIEW_ACCOUNT";
    public final static String PBM_VIEW_FILTER_EVENT = "PBM_VIEW_FILTER";

    public final static String PBM_SEND_QUESTION_EVENT = "PBM_SEND_QUESTION";
    public final static String PBM_SEND_PROBLEM_EVENT = "PBM_SEND_PROBLEM";
    public final static String PBM_FILTER_APPLY_EVENT = "PBM_FILTER_APPLY";
    public final static String PBM_CHANGE_BU_EVENT = "PBM_CHANGE_BU";
    public final static String PBM_PA_SUPPRESS_EVENT = "PBM_PA_SUPPRESS";
    public final static String PBM_PA_LIKE_EVENT = "PBM_PA_LIKE";

    private static final String ORGANIZATION_TYPE_CUSTOM_ATTR_KEY = "organization_type";

    private static IntercomSdk sharedInstance;
    private final Context context;

    public static IntercomSdk getInstance(Context context) {
        if (sharedInstance == null) {
            sharedInstance = new IntercomSdk(context);
        }
        return sharedInstance;
    }

    private IntercomSdk(Context context) {
        this.context = context;
    }

    public void init(IntercomProject project) {

        //Init the Intercom.
        HPLogger.d(TAG, "Init intercom project: " + project.name());

        Intercom.initialize((Application) context.getApplicationContext(),
                context.getString(project.getApiKey()),
                context.getString(project.getAppId()));
    }

    public void login() {

        boolean isAuthenticated = SessionManager.getInstance(context).hasCookie();

        if (isAuthenticated) {
            // Registering with Intercom.
            Intercom.client()
                    .registerIdentifiedUser(new Registration()
                            .withUserId(PrintOSPreferences.getInstance(context.getApplicationContext())
                                    .getUserInfo().getUserId()));
            Intercom.client().logEvent(VIEW_PRINTBEAT_MOBILE_EVENT);

            UserViewModel userViewModel = PrintOSPreferences.getInstance(context).getUserInfo();

            if (userViewModel != null && userViewModel.getUserOrganization() != null) {

                OrganizationViewModel organization = userViewModel.getUserOrganization();

                Company company = new Company.Builder()
                        .withCompanyId(organization.getOrganizationId())
                        .withName(organization.getOrganizationName())
                        .withCustomAttribute(ORGANIZATION_TYPE_CUSTOM_ATTR_KEY, organization.getOrganizationTypeDisplayName())
                        .build();
                UserAttributes userAttributes = new UserAttributes.Builder()
                        .withLanguageOverride(PrintOSPreferences.getInstance(context).getLanguageCode())
                        .withCompany(company).build();
                Intercom.client().updateUser(userAttributes);
            }

        }else {

            Intercom.client().reset();
        }

    }

    public void logout() {
        // This resets the Intercom for Android cache of your users’ identities
        // and wipes the slate clean.
        Intercom.client().reset();
    }

    public void sendClickShareIconEvent() {
        logEvent(CLICK_SHARE_ICON_MOBILE_EVENT);
    }

    public void sendUserInstallWidgetEvent() {
        logEvent(USER_INSTALL_WIDGET_EVENT);
    }

    public void sendUserOpenAppByWidgetEvent() {
        logEvent(USER_OPEN_APP_BY_WIDGET_EVENT);
    }

    public void sendUserRegisterNotificationTypeEvent(boolean register, String notificationType) {
        logEvent(String.format(register ? REGISTER_NOTIFICATION_TYPE_EVENT
                : UNREGISTER_NOTIFICATION_TYPE_EVENT, notificationType));
    }

    public void sendUserClickNotificationTypeEvent(String notificationType) {
        logEvent(String.format(CLICK_NOTIFICATION_TYPE_EVENT, notificationType));
    }

    public void sendUserChangeEquipmentEvent(String equipment) {
        logEvent(String.format(CHANGE_EQUIPMENT_EVENT, equipment));
    }

    public void sendUserClickDeviceEvent() {
        logEvent(USER_CLICK_DEVICE_IN_WIDGET);
    }

    public void sendUserClickGaugeEvent() {
        logEvent(USER_CLICK_GAUGE_IN_WIDGET);
    }

    public void logEvent(String event) {
        //TEMP
        if (event != null && !event.contains("PBM")) {
            Intercom.client().logEvent(event);
            HPLogger.d(TAG, "send intercom event " + event);
        }
    }

    public int getNumberOfUnreadMsgs() {
        return Intercom.client().getUnreadConversationCount();
    }

    public void addUnreadConversationCountListener(UnreadConversationCountListener listener) {
        Intercom.client().addUnreadConversationCountListener(listener);
    }

    public void removeUnreadConversationCountListener(UnreadConversationCountListener listener) {
        Intercom.client().removeUnreadConversationCountListener(listener);
    }

    public void logEvent(String event, BusinessUnitEnum businessUnitEnum) {
        String eventWithExtra = businessUnitEnum == null ? event : String.format(EVENT_WITH_EXTRA,
                event, businessUnitEnum.getShortName());
        logEvent(eventWithExtra);
    }

    public void displayConversationsList() {
        Intercom.client().displayConversationsList();
    }

    public enum IntercomProject {

        TESTING(R.string.intercomsdk_apikey_test, R.string.intercomsdk_appid_test),
        PRODUCTION(R.string.intercomsdk_apikey_production, R.string.intercomsdk_appid_production);

        private final int apiKey;
        private final int appId;

        IntercomProject(int apiKey, int appId) {
            this.apiKey = apiKey;
            this.appId = appId;
        }

        public int getApiKey() {
            return apiKey;
        }

        public int getAppId() {
            return appId;
        }
    }
}
