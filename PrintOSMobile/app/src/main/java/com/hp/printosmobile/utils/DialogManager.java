package com.hp.printosmobile.utils;

import android.os.Bundle;

import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.AppseeSdk;
import com.hp.printosmobile.Constants;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.presentation.BaseActivity;
import com.hp.printosmobile.presentation.modules.drawer.OrganizationViewModel;
import com.hp.printosmobile.presentation.modules.npspopup.NPSPopup;
import com.hp.printosmobile.presentation.modules.timezonedialog.TimeZoneManager;
import com.hp.printosmobilelib.core.logging.HPLogger;

/**
 * Created by Anwar Asbah on 7/16/2017.
 */

public class DialogManager {

    public static final long NPS_MIN_DISPLAY_TIME_INTERVAL = 7 * 24 * 60 * 60 * 1000; //week
    private static final String TAG = DialogManager.class.getSimpleName();

    public static boolean displayDialogs(BaseActivity activity) {
        if (activity.getIntent() != null && activity.getIntent().hasExtra(Constants.IntentExtras.MAIN_ACTIVITY_BEAT_COIN_EXTRA)) {
            return false;
        }
        if (!shouldDisplayNPSDialog(activity)) {
            if (!RateUtils.displayRatingDialog(activity)) {
                return TimeZoneManager.getInstance().showTimeZoneDialog(activity);
            }
        }
        return true;
    }

    private static boolean shouldDisplayNPSDialog(BaseActivity activity) {

        OrganizationViewModel.OrganizationType userType = PrintOSPreferences.getInstance(activity).getUserType();
        boolean isRegularUser = userType == OrganizationViewModel.OrganizationType.PSP;

        if (!PrintOSPreferences.getInstance(activity).getHasIndigoDivision() ||
                !PrintOSPreferences.getInstance(activity).isNPSFeatureEnabled()
                || !isRegularUser) {
            return false;
        }

        Bundle bundle = activity.getIntent().getExtras();
        if (bundle != null && bundle.containsKey(Constants.IntentExtras.NPS_EXTRA)) {
            HPLogger.v(TAG, "has NPS extra");
            activity.getIntent().removeExtra(Constants.IntentExtras.NPS_EXTRA);
            displayNPSPopup(activity);
            return true;
        } else if (!PrintOSPreferences.getInstance(activity).isNPSNotificationsEnabled()) {
            HPLogger.v(TAG, "local notifications off");
            long timeDiff = System.currentTimeMillis() - PrintOSPreferences.getInstance(activity).getNPSLastDisplayTiming();
            HPLogger.v(TAG, "timeDiff: " + timeDiff);
            if (timeDiff >= NPS_MIN_DISPLAY_TIME_INTERVAL) {
                displayNPSPopup(activity);
                return true;
            }
        }
        return false;
    }

    private static void displayNPSPopup(BaseActivity activity) {

        HPLogger.d(TAG, "show NPS popup.");

        Analytics.sendEvent(Analytics.SHOW_NPS_ACTION);
        AppseeSdk.getInstance(activity).sendEvent(AppseeSdk.EVENT_SHOW_NPS);

        PrintOSPreferences.getInstance(activity).setNPSLastDisplayTiming(System.currentTimeMillis());
        NPSPopup.getInstance(activity instanceof NPSPopup.NPSPopupCallback ?
                (NPSPopup.NPSPopupCallback) activity : null)
                .show(activity.getSupportFragmentManager(), NPSPopup.TAG);

    }

}
