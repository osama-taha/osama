package com.hp.printosmobile.presentation.modules.personaladvisor.presenter;

import com.hp.printosmobile.presentation.modules.personaladvisor.PersonalAdvisorViewModel;
import com.hp.printosmobilelib.core.communications.remote.APIException;

/**
 * Created by Anwar Asbah 5/10/17.
 */
public interface PersonalAdvisorView {

    void onPersonalAdvisorDataRetrieved(PersonalAdvisorViewModel personalAdvisorViewModel);

    void onError(APIException exception, String tag);

}
