package com.hp.printosmobile.presentation.modules.npspopup;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.res.ResourcesCompat;

import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.AppseeSdk;
import com.hp.printosmobile.Constants;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.notification.NotificationUtils;
import com.hp.printosmobile.presentation.modules.drawer.OrganizationViewModel;
import com.hp.printosmobile.presentation.modules.main.MainActivity;
import com.hp.printosmobile.presentation.modules.settings.SubscriptionEvent;
import com.hp.printosmobile.utils.DialogManager;
import com.hp.printosmobilelib.core.logging.HPLogger;

import java.util.Calendar;

/**
 * Created by Anwar Asbah on 7/16/2017.
 */

public class NPSNotificationManager {

    private static final String TAG = NPSNotificationManager.class.getSimpleName();

    public static void displayNotification(Context context) {

        OrganizationViewModel.OrganizationType userType = PrintOSPreferences.getInstance(context).getUserType();
        boolean isRegularUser = userType == OrganizationViewModel.OrganizationType.PSP;

        if (PrintOSPreferences.getInstance(context).getHasIndigoDivision() &&
                PrintOSPreferences.getInstance(context).isNPSFeatureEnabled() && isRegularUser) {
            String title = context.getString(R.string.app_name);
            String message = context.getString(R.string.nps_notification_msg);

            fireNotification(context, title, message);
        }
    }

    private static void fireNotification(Context context, String title, String message) {
        HPLogger.v(TAG, "fireNotification");
        int requestCode = NotificationUtils.getUniqueRequestCode();

        Intent intent = getNotificationIntent(context);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, requestCode, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.ic_notification)
                .setColor(ResourcesCompat.getColor(context.getResources(), R.color.c62, null))
                .setContentTitle(title)
                .setContentText(message)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(message).setBigContentTitle(title))
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        int id = (int) System.currentTimeMillis();
        notificationManager.notify(id, notificationBuilder.build());

        HPLogger.d(TAG, "show nps notification");

        String notificationKey = SubscriptionEvent.NotificationEventEnum.NPS_NOTIFICATIONS.getKey();

        AppseeSdk.getInstance(context).sendEvent(AppseeSdk.EVENT_SHOW_NOTIFICATION, notificationKey);
        Analytics.sendNotificationArriveFirebaseEvent(notificationKey);
        Analytics.sendEvent(Analytics.MOBILE_PRINT_BEAT_CATEGORY, Analytics.SHOW_NOTIFICATION_ACTION, notificationKey);
    }

    private static Intent getNotificationIntent(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        Bundle bundle = new Bundle();
        bundle.putBoolean(Constants.IntentExtras.NPS_EXTRA, true);
        intent.putExtras(bundle);

        return intent;
    }

    public static void enableNotifications(boolean enabled) {
        HPLogger.v(TAG, "enableNotifications(" + enabled + ")");
        PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).setNPSNotificationsEnabled(enabled);
        if (enabled) {
            setRecurringNSPAlarm();
        } else {
            removeRecurringNPSAlarm();
        }
    }

    private static void setRecurringNSPAlarm() {
        HPLogger.v(TAG, "setRecurringNSPAlarm");

        Calendar updateTime = Calendar.getInstance();
        updateTime.set(Calendar.HOUR_OF_DAY, 12);
        updateTime.set(Calendar.MINUTE, 0);

        PendingIntent recurringNPSNotification = getNPSAlarmIntent();
        AlarmManager alarms = (AlarmManager) PrintOSApplication.getAppContext().getSystemService(
                Context.ALARM_SERVICE);
        alarms.setRepeating(AlarmManager.ELAPSED_REALTIME,
                SystemClock.elapsedRealtime() + DialogManager.NPS_MIN_DISPLAY_TIME_INTERVAL,
                DialogManager.NPS_MIN_DISPLAY_TIME_INTERVAL, recurringNPSNotification);
    }

    private static void removeRecurringNPSAlarm() {
        AlarmManager alarms = (AlarmManager) PrintOSApplication.getAppContext().getSystemService(
                Context.ALARM_SERVICE);
        alarms.cancel(getNPSAlarmIntent());
    }

    private static PendingIntent getNPSAlarmIntent() {
        Intent nspIntent = new Intent(PrintOSApplication.getAppContext(), NPSBroadcastReceiver.class);
        nspIntent.setFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        PendingIntent recurringNPSNotification = PendingIntent.getBroadcast(PrintOSApplication.getAppContext(),
                0, nspIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        return recurringNPSNotification;
    }

    public static void setNotificationsEnabledByDefault() {
        if (!PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).hasNPSNotificationPreference()
                && PrintOSPreferences.getInstance(PrintOSApplication.getAppContext()).isNPSFeatureEnabled()) {
            enableNotifications(true);
        }
    }

}
