package com.hp.printosmobile.presentation;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.PersistableBundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;

import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.Constants;
import com.hp.printosmobile.PrintOSApplication;
import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.data.remote.models.VersionUpdateResult;
import com.hp.printosmobile.data.remote.models.VersionsData;
import com.hp.printosmobile.data.remote.services.VersionCheckService;
import com.hp.printosmobile.notification.LocationBasedNotificationsManager;
import com.hp.printosmobile.notification.RegistrationIntentService;
import com.hp.printosmobile.presentation.indigowidget.PrintOSAppWidgetProvider;
import com.hp.printosmobile.presentation.modules.main.MainActivity;
import com.hp.printosmobile.utils.AppUtils;
import com.hp.printosmobile.utils.HPLocaleUtils;
import com.hp.printosmobile.utils.HPUIUtils;
import com.hp.printosmobile.validation.ValidationPresenter;
import com.hp.printosmobile.validation.ValidationView;
import com.hp.printosmobilelib.core.communications.remote.APIException;
import com.hp.printosmobilelib.core.communications.remote.CookieAuthenticator;
import com.hp.printosmobilelib.core.communications.remote.SessionManager;
import com.hp.printosmobilelib.core.logging.HPLogConfig;
import com.hp.printosmobilelib.core.logging.HPLogger;
import com.hp.printosmobilelib.core.utils.DeviceUtils;
import com.hp.printosmobilelib.core.utils.HPDateUtils;
import com.hp.printosmobilelib.ui.common.HPActivity;

import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by Osama Taha on 8/28/16.
 */
public abstract class BaseActivity extends HPActivity implements ValidationView {

    private static final String TAG = BaseActivity.class.getSimpleName();

    private static final int TIME_PERIOD_FOR_VERSION_CHECKING = 60 * 60000;
    private static final String COOKIE_ITEM_SEPARATOR = ";";
    private static final String COOKIE_DATE_TIME_SEPARATOR = ",";
    private static final int COOKIE_DATE_INDEX = 5;
    private static final String COOKIE_DATE_FORMAT = "dd-MMM-yyyy hh:mm:ss z";
    private static final int MIN_IN_MILLI = 60 * 1000;
    private static final long HOUR_IN_MILLI = 60 * MIN_IN_MILLI;
    private static final int COOKIE_TIME_DIFFERENCE_FLOOR_DEF_VALUE = 1;
    private static final int VALIDATION_PERIOD_PASSED_DEF_VALUE = 3;
    private static final long VALIDATION_DELAY = 300;

    public static boolean isServiceStarted = false;
    public boolean isForceUpdating;
    public boolean isShowing;
    private BroadcastReceiver mVersionUpdateReceiver;
    private AuthenticationBroadcastReceiver authenticationBroadcastReceiver;
    private long timeCreated;

    private boolean isFirstCreation;
    public static boolean returningFromBackground = false;
    private ValidationPresenter validationPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        HPLogger.d(this.getClass().getSimpleName(), "onCreate");
        validationPresenter = new ValidationPresenter();
        validationPresenter.attachView(this);
        timeCreated = System.currentTimeMillis();
        this.isFirstCreation = true;
    }

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
        isShowing = false;

    }

    protected void requestNeededPermissions() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            int writePermissionGrantedCode = ContextCompat.checkSelfPermission(PrintOSApplication.getAppContext(),
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE);

            int readPermissionGrantedCode = ContextCompat.checkSelfPermission(PrintOSApplication.getAppContext(),
                    Manifest.permission.READ_EXTERNAL_STORAGE);

            if (readPermissionGrantedCode != PackageManager.PERMISSION_GRANTED ||
                    writePermissionGrantedCode != PackageManager.PERMISSION_GRANTED) {

                String[] permissions = null;
                if (PrintOSPreferences.getInstance(this).isFirstTimeAskingPermission(Manifest.permission.ACCESS_FINE_LOCATION)) {
                    //Ask for Access fine location once.
                    permissions = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION};

                    PrintOSPreferences.getInstance(this).firstTimeAskingPermission(Manifest.permission.ACCESS_FINE_LOCATION, false);
                } else {
                    permissions = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_EXTERNAL_STORAGE};
                }

                requestPermissions(permissions, Constants.IntentExtras.FIRST_INSTALL_PERMISSIONS_REQUEST_CODE);

            }


        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            if (requestCode == Constants.IntentExtras.FIRST_INSTALL_PERMISSIONS_REQUEST_CODE) {
                DeviceUtils.cacheDeviceId(this);
            }

            if (LocationBasedNotificationsManager.getInstance(this).locationBasedNotificationToggleOn()) {
                LocationBasedNotificationsManager.getInstance(this).enableNotifications(true);
            }
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        HPLogger.d(this.getClass().getSimpleName(), "onResume");

        if (returningFromBackground) {
            HPLogger.i(TAG, String.format("app is now open. locale used: %s",
                    PrintOSPreferences.getInstance(this).getLanguageCode()));
            Analytics.sendEvent(Analytics.APP_OPENED_ACTION);
        }

        long validationPeriod = PrintOSPreferences.getInstance(this).getMoveToHomeTimePeriod(
                VALIDATION_PERIOD_PASSED_DEF_VALUE) * MIN_IN_MILLI;
        long timePassed = System.currentTimeMillis() - timeCreated;
        if (timePassed > validationPeriod && returningFromBackground) {
            if (isBackwardCompatible()) {
                Intent mainActivityIntent = new Intent(this, MainActivity.class);
                mainActivityIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(mainActivityIntent);
                return;
            } else {
                onValidationPeriodPassed();
            }
        }

        if (shouldValidateSession()) {
            boolean isCookieAboutToExpire = false;
            String cookie = SessionManager.getInstance(this).getCookie();
            if (cookie != null) {
                String[] cookieItems = cookie.split(COOKIE_ITEM_SEPARATOR);
                if (COOKIE_DATE_INDEX < cookieItems.length) {
                    try {
                        Date expiryDate = HPDateUtils.parseDate(cookieItems[COOKIE_DATE_INDEX].split(COOKIE_DATE_TIME_SEPARATOR)[1],
                                COOKIE_DATE_FORMAT);
                        int diffHours = (int) ((expiryDate.getTime() - System.currentTimeMillis()) / HOUR_IN_MILLI);

                        HPLogger.d(this.getClass().getSimpleName(), "Hours till cookie expires: " + diffHours);

                        isCookieAboutToExpire = diffHours <= PrintOSPreferences.getInstance(this)
                                .getCookieExpiryTimeFloor(COOKIE_TIME_DIFFERENCE_FLOOR_DEF_VALUE);
                    } catch (Exception e) {
                        //do nothing
                    }
                }
            }

            final boolean cookieWillExpire = isCookieAboutToExpire;
            if (returningFromBackground || isCookieAboutToExpire) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        validate(cookieWillExpire, isFirstCreation);
                        isFirstCreation = false;
                    }
                }, VALIDATION_DELAY);
            }
        }

        registerCheckVersionUpdateBroadcastReceiver();
        registerAuthenticatorBroadcastReceiver();
        checkVersioning();
    }

    public boolean shouldValidateSession() {
        return false;
    }

    public boolean isBackwardCompatible() {
        return false;
    }

    public void validate(boolean isCookieAboutToExpire, boolean isFirstCreation) {
        HPLogConfig.getInstance(this).setDispatchingEnabled(false);
        HPLogger.d(this.getClass().getSimpleName(), "validating");
        onPreValidation();
        if (isCookieAboutToExpire) {
            //directly go to login
            //validation of cookie is miss leading
            validationPresenter.login(this, isFirstCreation);
        } else {
            validationPresenter.authenticate(this, isFirstCreation);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        HPLogger.d(this.getClass().getSimpleName(), "onPause");
        returningFromBackground = false;
    }

    @Override
    protected void onStop() {
        super.onStop();

        unregisterCheckVersionUpdateBroadcastReceiver();
        unregisterAuthenticatorBroadcastReceiver();
        returningFromBackground = true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        validationPresenter.detachView();
    }

    @Override
    public void onPreValidation() {
        HPLogger.d(this.getClass().getSimpleName(), "onPreValidation");
    }

    @Override
    public void onPreAutoLogin(boolean isFirstCreation) {
        HPLogger.d(this.getClass().getSimpleName(), "onPreAutoLogin");

        if (PrintOSPreferences.getInstance(this).showValidationToastMsg(false) && !isFirstCreation) {
            HPUIUtils.displayToast(this, getString(R.string.just_a_second), false);
        }
    }

    @Override
    public void onValidationCompleted() {
        HPLogConfig.getInstance(this).setDispatchingEnabled(true);
        HPLogger.d(this.getClass().getSimpleName(), "onValidationCompleted");
    }

    @Override
    public void onValidationError() {
        HPLogger.d(this.getClass().getSimpleName(), "onValidationError");
    }

    @Override
    public void onValidationUnauthorized() {
        HPLogger.d(this.getClass().getSimpleName(), "onValidationUnauthorized");
        SessionManager.getInstance(this).saveTempCredentials();
        SessionManager.getInstance(this).clearUserCredentials();
        PrintOSPreferences.getInstance(this).clearUserInfo();
    }

    @Override
    public void onValidationCompletedWithoutContextChange() {
        HPLogConfig.getInstance(this).setDispatchingEnabled(true);
        HPLogger.d(this.getClass().getSimpleName(), "onValidationCompletedWithoutContextChange");
    }

    public void onValidationPeriodPassed() {
        timeCreated = System.currentTimeMillis();
    }

    @Override
    public void onError(APIException exception, String tag) {
        //do nothing
    }

    private void registerCheckVersionUpdateBroadcastReceiver() {
        IntentFilter intentFilter = new IntentFilter(Constants.IntentExtras.VERSION_UPDATE_INTENT_ACTION);

        mVersionUpdateReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {

                isServiceStarted = false;
                VersionUpdateResult versionUpdateResult = (VersionUpdateResult) intent.getSerializableExtra(Constants.IntentExtras.VERSION_UPDATE_INTENT_EXTRA_KEY);
                HPLogger.d(TAG, "Version needs update " + versionUpdateResult);

                showUpdateDialog(versionUpdateResult);

            }
        };

        //registering receiver
        registerReceiver(mVersionUpdateReceiver, intentFilter);

    }

    private void registerAuthenticatorBroadcastReceiver() {
        IntentFilter intentFilter = new IntentFilter(CookieAuthenticator.AUTHENTICATOR_INTENT_ACTION);
        authenticationBroadcastReceiver = new AuthenticationBroadcastReceiver();
        registerReceiver(authenticationBroadcastReceiver, intentFilter);
    }

    public void startRegistrationService(String action) {
        // Start IntentService to register this application with GCM.
        Intent intent = new Intent(this, RegistrationIntentService.class);
        intent.setAction(action);
        startService(intent);
    }

    private void unregisterCheckVersionUpdateBroadcastReceiver() {
        //Try catch was add to avoid crash when attempting to unregister the receiver that might have
        //been unregistered by the system (ex: when battery is low)
        try {
            unregisterReceiver(mVersionUpdateReceiver);
        } catch (Exception e) {
            //unregister versionUpdateReceiver exception
        }
    }

    private void unregisterAuthenticatorBroadcastReceiver() {
        try {
            unregisterReceiver(authenticationBroadcastReceiver);
        } catch (Exception e) {
            //unregister authenticationBroadcastReceiver exception
        }
    }

    private void checkVersioning() {

        VersionsData versionsData = PrintOSPreferences.getInstance(this).getVersionsData();
        long lastCheckingTime = PrintOSPreferences.getInstance(this).getVersionCheckingTime();
        long timeDiff = System.currentTimeMillis() - lastCheckingTime;

        if (versionsData == null || timeDiff > TIME_PERIOD_FOR_VERSION_CHECKING) {
            startVersionCheckingService();
        } else {
            VersionUpdateResult versionUpdateResult = VersionCheckService.getVersionUpdateResult(versionsData, this);
            handleLanguageChange(versionsData);
            showUpdateDialog(versionUpdateResult);
        }
    }

    public void handleLanguageChange(VersionsData versionsData) {

        String languageCode = PrintOSPreferences.getInstance(this).getLanguageCode();

        if (versionsData != null && versionsData.getConfiguration() != null) {
            List<String> supportedLanguages = versionsData.getConfiguration().getSupportedLanguages();
            if (supportedLanguages != null && !supportedLanguages.contains(languageCode)) {
                PrintOSPreferences.getInstance(this).setLanguage(getString(R.string.default_language));
                HPLocaleUtils.updateLanguage(this, getString(R.string.default_language));
                HPLocaleUtils.configureAppLanguage(PrintOSApplication.getInstance(), Locale.getDefault());
                startRegistrationService(Constants.IntentExtras.NOTIFICATION_REGISTER_INTENT_ACTION);
                PrintOSAppWidgetProvider.updateAllWidgets(this);

                Intent returnIntent = new Intent();
                setResult(Activity.RESULT_OK, returnIntent);
                this.finish();
            }
        }
    }

    protected void startVersionCheckingService() {
        if (!isServiceStarted) {
            isServiceStarted = true;
            Intent intent = new Intent(this, VersionCheckService.class);
            startService(intent);
        }
    }

    private void showUpdateDialog(VersionUpdateResult versionUpdateResult) {
        if (isShowing) {
            return;
        }

        HPLogger.d(TAG, "show update dialog");

        isForceUpdating = versionUpdateResult != null && versionUpdateResult.getMessageType() == VersionUpdateResult.MessageType.FORCE_UPDATE;

        VersionUpdateResult.MessageType messageType = versionUpdateResult == null ? VersionUpdateResult.MessageType.OK : versionUpdateResult.getMessageType();
        boolean dontShow = messageType == VersionUpdateResult.MessageType.OK ||
                (messageType == VersionUpdateResult.MessageType.WARNING && !PrintOSPreferences.getInstance(this).isWarningUpdateDialogEnabled());
        if (dontShow) {
            return;
        }

        isShowing = true;
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle(getString(R.string.outdated_version_update_title));
        builder.setMessage(versionUpdateResult.getUpdateMessage());
        builder.setCancelable(false);
        builder.setPositiveButton(getString(R.string.outdated_version_update_string), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                HPLogger.d(TAG, "update app");
                isShowing = false;
                AppUtils.startApplication(BaseActivity.this, Uri.parse
                        (Constants.GOOGLE_PLAY_URL + getPackageName()));
                PrintOSPreferences.getInstance(BaseActivity.this).enableWarningUpdateDialog(false);
                dialog.dismiss();
            }
        });

        if (versionUpdateResult.getMessageType() == VersionUpdateResult.MessageType.WARNING) {
            builder.setNegativeButton(getString(R.string.outdated_version_cancel_string), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    HPLogger.d(TAG, "Cancel app update");
                    isShowing = false;
                    PrintOSPreferences.getInstance(BaseActivity.this).enableWarningUpdateDialog(false);
                    dialog.cancel();
                }
            });
        }

        builder.setCancelable(false);
        builder.show();

    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode, Bundle options) {
        super.startActivityForResult(intent, requestCode, options);
        unregisterCheckVersionUpdateBroadcastReceiver();
    }

    public class AuthenticationBroadcastReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            if (bundle != null && bundle.containsKey(CookieAuthenticator.EXCEPTION_EXTRA)) {
                APIException apiException = (APIException) bundle.getSerializable(CookieAuthenticator.EXCEPTION_EXTRA);
                AppUtils.sendLoginErrorEvent(apiException, Analytics.LOGIN_EVENT_AUTO_LOGIN_FAILED_ACTION);
            } else {
                Analytics.sendEvent(Analytics.MOBILE_PRINT_BEAT_CATEGORY, Analytics.LOGIN_EVENT_AUTO_LOGIN_ACTION);
            }
        }
    }
}
