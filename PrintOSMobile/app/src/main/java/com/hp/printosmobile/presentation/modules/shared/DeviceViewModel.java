package com.hp.printosmobile.presentation.modules.shared;

import com.hp.printosmobile.data.remote.models.PreferencesData;
import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;
import com.hp.printosmobile.presentation.modules.personaladvisor.PersonalAdvisorViewModel;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * A model for PrintBeat devices.
 * <p/>
 * Created by Osama Taha on 5/15/2016.
 */
public class DeviceViewModel extends BaseDeviceViewModel implements Serializable {

    public enum JobType implements Serializable{
        COMPLETED (R.string.jobs_completed_tab_title, false),
        QUEUED (R.string.jobs_queued_tab_title, true);

        int nameID;
        boolean hasTimeParameters;

        JobType (int nameID, boolean hasTimeParameters) {
            this.nameID = nameID;
            this.hasTimeParameters = hasTimeParameters;
        }

        public int getNameID() {
            return nameID;
        }

        public boolean isHasTimeParameters() {
            return hasTimeParameters;
        }
    }

    private static final int DISCONNECTED_MIN = 15;

    private String name;
    private String deviceImageUrl;
    private MaintenanceState maintenanceState;
    private DeviceState deviceState;
    private double printVolumeShiftTargetValue;
    private double printVolumeIntraDailyTargetValue;
    private double printVolumeValue;
    private int durationToDone;
    private TimeUnit durationToDoneUnit;
    private int jobsInQueue;
    private int meters;
    private int printedJobs;
    private int sheets;
    private int metersSquare;
    private double litersConsumed;
    private String currentJob;
    private int timeInStateMin;
    private String impressionType;
    private int lastSeenMin;
    private boolean isRTSupported;
    private Map<JobType, List<Job>> jobs;
    private int deviceImageResource;
    private String serialNumber;
    private String serialNumberDisplay;
    private PersonalAdvisorViewModel personalAdvisorViewModel;
    private DeviceExtraData extraData;
    private boolean underWarranty;
    private int currentJobTimeToDone;
    private TimeUnit currentJobTimeToDoneUnit;
    private boolean hasExtraData;
    private BusinessUnitEnum businessUnitEnum;
    private float currentJobProgress;
    private Date lastUpdateTime;
    private boolean isWrongTZ;
    private DeviceLastUpdateType deviceLastUpdateType;
    private String model;
    private PreferencesData.UnitSystem unitSystem;
    private Message message;

    public PersonalAdvisorViewModel getPersonalAdvisorViewModel() {
        return this.personalAdvisorViewModel;
    }

    public void setPersonalAdvisorViewModel(PersonalAdvisorViewModel personalAdvisorViewModel) {
        this.personalAdvisorViewModel = personalAdvisorViewModel;
    }

    public boolean isWrongTZ() {
        return isWrongTZ;
    }

    public void setWrongTZ(boolean isWrongTZ) {
        this.isWrongTZ = isWrongTZ;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public DeviceState getState() {
        return deviceState;
    }

    public String getDeviceImageUrl() {
        return deviceImageUrl;
    }

    public void setDeviceImageUrl(String deviceImageUrl) {
        this.deviceImageUrl = deviceImageUrl;
    }

    public MaintenanceState getMaintenanceState() {
        return maintenanceState;
    }

    public void setMaintenanceState(MaintenanceState maintenanceState) {
        this.maintenanceState = maintenanceState;
    }

    public DeviceState getDeviceState() {
        return deviceState;
    }

    public void setDeviceState(DeviceState deviceState) {
        this.deviceState = deviceState;
    }

    public double getPrintVolumeShiftTargetValue() {
        return printVolumeShiftTargetValue;
    }

    public void setPrintVolumeShiftTargetValue(double printVolumeShiftTargetValue) {
        this.printVolumeShiftTargetValue = printVolumeShiftTargetValue;
    }

    public double getPrintVolumeIntraDailyTargetValue() {
        return printVolumeIntraDailyTargetValue;
    }

    public void setPrintVolumeIntraDailyTargetValue(double printVolumeIntraDailyTargetValue) {
        this.printVolumeIntraDailyTargetValue = printVolumeIntraDailyTargetValue;
    }

    public double getPrintVolumeValue() {
        return printVolumeValue;
    }

    public void setPrintVolumeValue(double printVolumeValue) {
        this.printVolumeValue = printVolumeValue;
    }

    public int getDurationToDone() {
        return durationToDone;
    }

    public void setDurationToDone(int durationToDone) {
        this.durationToDone = durationToDone;
    }

    public TimeUnit getDurationToDoneUnit() {
        return durationToDoneUnit;
    }

    public void setDurationToDoneUnit(TimeUnit durationToDoneUnit) {
        this.durationToDoneUnit = durationToDoneUnit;
    }

    public int getJobsInQueue() {
        return jobsInQueue;
    }

    public void setJobsInQueue(int jobsInQueue) {
        this.jobsInQueue = jobsInQueue;
    }

    public int getMeters() {
        return meters;
    }

    public void setMeters(int meters) {
        this.meters = meters;
    }

    public int getPrintedJobs() {
        return printedJobs;
    }

    public void setPrintedJobs(int printedJobs) {
        this.printedJobs = printedJobs;
    }

    public int getSheets() {
        return sheets;
    }

    public void setSheets(int sheets) {
        this.sheets = sheets;
    }

    public String getCurrentJob() {
        return currentJob;
    }

    public void setCurrentJob(String currentJob) {
        this.currentJob = currentJob;
    }

    public int getTimeInStateMin() {
        return timeInStateMin;
    }

    public void setTimeInStateMin(int timeInStateMin) {
        this.timeInStateMin = timeInStateMin;
    }

    public String getImpressionType() {
        return impressionType;
    }

    public void setImpressionType(String impressionType) {
        this.impressionType = impressionType;
    }

    public int getLastSeenMin() {
        return lastSeenMin;
    }

    public void setLastSeenMin(int lastSeenMin) {
        this.lastSeenMin = lastSeenMin;
    }

    public boolean isPressDisconnected() {
        //If the machine is not synchronized more than x minutes
        return lastSeenMin > DISCONNECTED_MIN;
    }

    public int getMetersSquare() {
        return metersSquare;
    }

    public void setMetersSquare(int metersSquare) {
        this.metersSquare = metersSquare;
    }

    public boolean isRTSupported() {
        return isRTSupported;
    }

    public void setRTSupported(boolean RTSupported) {
        isRTSupported = RTSupported;
    }

    public List<Job> getJobs(JobType type) {
        return jobs != null && jobs.containsKey(type)? jobs.get(type) : null;
    }

    public int getTotalJobCount(JobType type) {
        switch (type) {
            case QUEUED:
                return getJobsInQueue();
            case COMPLETED:
                return getPrintedJobs();
        }
        return 0;
    }

    public void setJobs(Map<JobType, List<Job>> jobs) {
        this.jobs = jobs;
    }

    public int getDeviceImageResource() {
        return deviceImageResource;
    }

    public void setDeviceImageResource(int deviceImageResource) {
        this.deviceImageResource = deviceImageResource;
    }

    public double getLitersConsumed() {
        return litersConsumed;
    }

    public void setLitersConsumed(double litersConsumed) {
        this.litersConsumed = litersConsumed;
    }

    public boolean hasUnsuppressedAdvices() {
        return personalAdvisorViewModel != null && personalAdvisorViewModel.getUnsuppressedAdvices() != null && personalAdvisorViewModel.getUnsuppressedAdvices().size() > 0;
    }

    public boolean hasAdvices() {
        return personalAdvisorViewModel != null && personalAdvisorViewModel.getAllAdvices() != null && personalAdvisorViewModel.getAllAdvices().size() > 0;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public BusinessUnitEnum getBusinessUnitEnum() {
        return businessUnitEnum;
    }

    public void setBusinessUnitEnum(BusinessUnitEnum businessUnitEnum) {
        this.businessUnitEnum = businessUnitEnum;
    }

    public void setDeviceLastUpdateType(DeviceLastUpdateType deviceLastUpdateType) {
        this.deviceLastUpdateType = deviceLastUpdateType;
    }

    public DeviceLastUpdateType getDeviceLastUpdateType() {
        return deviceLastUpdateType;
    }

    public void setExtraData(DeviceExtraData extraData) {
        this.extraData = extraData;
    }

    public DeviceExtraData getExtraData() {
        return extraData;
    }

    public void setUnderWarranty(boolean underWarranty) {
        this.underWarranty = underWarranty;
    }

    public boolean isUnderWarranty() {
        return underWarranty;
    }

    public void setCurrentJobTimeToDone(int currentJobTimeToDone) {
        this.currentJobTimeToDone = currentJobTimeToDone;
    }

    public int getCurrentJobTimeToDone() {
        return currentJobTimeToDone;
    }

    public void setCurrentJobTimeToDoneUnit(TimeUnit currentJobTimeToDoneUnit) {
        this.currentJobTimeToDoneUnit = currentJobTimeToDoneUnit;
    }

    public TimeUnit getCurrentJobTimeToDoneUnit() {
        return currentJobTimeToDoneUnit;
    }

    public void setHasExtraData(boolean hasExtraData) {
        this.hasExtraData = hasExtraData;
    }

    public boolean hasExtraData() {
        return hasExtraData;
    }

    public void setCurrentJobProgress(float currentJobProgress) {
        this.currentJobProgress = currentJobProgress;
    }

    public float getCurrentJobProgress() {
        return currentJobProgress;
    }

    public void setLastUpdateTime(Date lastUpdateTime) {
        this.lastUpdateTime = lastUpdateTime;
    }

    public Date getLastUpdateTime() {
        return lastUpdateTime;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getModel() {
        return model;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    public String getSerialNumberDisplay() {
        return serialNumberDisplay;
    }

    public void setSerialNumberDisplay(String serialNumberDisplay) {
        this.serialNumberDisplay = serialNumberDisplay;
    }

    public static class Job implements Serializable {

        String name;
        int duration;
        TimeUnit timeUnit;
        int index;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getDuration() {
            return duration;
        }

        public void setDuration(int duration) {
            this.duration = duration;
        }

        public int getIndex() {
            return index;
        }

        public void setIndex(int index) {
            this.index = index;
        }

        public TimeUnit getTimeUnit() {
            return timeUnit;
        }

        public void setTimeUnit(TimeUnit timeUnit) {
            this.timeUnit = timeUnit;
        }

        @Override
        public String toString() {
            return "Job{" +
                    "name='" + name + '\'' +
                    ", duration=" + duration +
                    ", timeUnit=" + timeUnit +
                    ", index=" + index +
                    '}';
        }
    }

    public static class Message implements Serializable {

        String msg;
        int msgID;

        public String getMsg() {
            return msg;
        }

        public void setMsg(String msg) {
            this.msg = msg;
        }

        public int getMsgID() {
            return msgID;
        }

        public void setMsgID(int msgID) {
            this.msgID = msgID;
        }

        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder("Message{");
            sb.append("msg='").append(msg).append('\'');
            sb.append(", msgID=").append(msgID);
            sb.append('}');
            return sb.toString();
        }
    }

    @Override
    public String toString() {
        return "DeviceViewModel{" +
                "name='" + name + '\'' +
                ", deviceImageUrl='" + deviceImageUrl + '\'' +
                ", maintenanceState=" + maintenanceState +
                ", deviceState=" + deviceState +
                ", printVolumeShiftTargetValue=" + printVolumeShiftTargetValue +
                ", printVolumeIntraDailyTargetValue=" + printVolumeIntraDailyTargetValue +
                ", printVolumeValue=" + printVolumeValue +
                ", durationToDone=" + durationToDone +
                ", durationToDoneUnit=" + durationToDoneUnit +
                ", jobsInQueue=" + jobsInQueue +
                ", meters=" + meters +
                ", printedJobs=" + printedJobs +
                ", sheets=" + sheets +
                ", metersSquare=" + metersSquare +
                ", litersConsumed=" + litersConsumed +
                ", currentJob='" + currentJob + '\'' +
                ", timeInStateMin=" + timeInStateMin +
                ", impressionType='" + impressionType + '\'' +
                ", lastSeenMin=" + lastSeenMin +
                ", isRTSupported=" + isRTSupported +
                ", deviceImageResource=" + deviceImageResource +
                ", serialNumber='" + serialNumber + '\'' +
                ", personalAdvisorViewModel=" + personalAdvisorViewModel +
                ", extraData=" + extraData +
                ", underWarranty=" + underWarranty +
                ", currentJobTimeToDone=" + currentJobTimeToDone +
                ", currentJobTimeToDoneUnit=" + currentJobTimeToDoneUnit +
                ", hasExtraData=" + hasExtraData +
                ", businessUnitEnum=" + businessUnitEnum +
                ", currentJobProgress=" + currentJobProgress +
                ", lastUpdateTime=" + lastUpdateTime +
                ", deviceLastUpdateType=" + deviceLastUpdateType +
                ", model='" + model + '\'' +
                '}';
    }

    /**
     * A comparator to sort the devices according to their RT support property then according to their names.
     */
    public static Comparator<DeviceViewModel> RT_DEVICES_FIRST_COMPARATOR = new Comparator<DeviceViewModel>() {
        @Override
        public int compare(DeviceViewModel lhs, DeviceViewModel rhs) {

            if (lhs == null && rhs == null) return 0;
            if (lhs == null && rhs != null) return 1;
            if (lhs != null && rhs == null) return -1;

            int sortOrderDiff = Boolean.valueOf(rhs.isRTSupported).compareTo(Boolean.valueOf(lhs.isRTSupported));

            if (sortOrderDiff == 0 && lhs.getName() != null) {
                return lhs.getName().compareTo(lhs.getName());
            }

            return sortOrderDiff;
        }
    };

}
