package com.hp.printosmobile.presentation.indigowidget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.res.ResourcesCompat;
import android.view.View;
import android.widget.RemoteViews;

import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.modules.home.PrintBeatDashboardViewModel;
import com.hp.printosmobile.presentation.modules.shared.DeviceViewModel;
import com.hp.printosmobile.presentation.modules.today.TodayViewModel;
import com.hp.printosmobile.presentation.modules.tooltip.TodayInfoTooltipViewModel;
import com.hp.printosmobile.utils.HPLocaleUtils;
import com.hp.printosmobile.utils.HPScoreUtils;
import com.hp.printosmobilelib.core.logging.HPLogger;
import com.hp.printosmobilelib.ui.widgets.TypefaceManager;

import java.util.List;

/**
 * Created by Osama Taha on 2/10/17.
 */
public class IndigoWidgetRemoteViews extends RemoteViews {

    private static final String TAG = IndigoWidgetRemoteViews.class.getSimpleName();

    private static int WIDGET_LARGE_FONT_SIZE_SP = 18;
    private static int WIDGET_DEFAULT_FONT_SIZE_SP = 16;
    private static int WIDGET_SMALL_FONT_SIZE_SP = 14;

    private Context mContext;

    private AppWidgetManager mAppWidgetManager;

    private int[] mAppWidgetIds;
    private WidgetType mWidgetType;
    private int mDefaultTextColor;
    private int mImpTypeTextColor;

    public IndigoWidgetRemoteViews(Context context, WidgetType widgetType) {
        super(context.getPackageName(), R.layout.widget_layout);
        init(context, widgetType);
    }

    private void init(Context context, WidgetType widgetType) {
        this.mContext = context;
        this.mAppWidgetManager = AppWidgetManager.getInstance(mContext);
        this.mWidgetType = widgetType;
        this.mAppWidgetIds = getAppWidgetIds();
        this.mDefaultTextColor = ResourcesCompat.getColor(mContext.getResources(), R.color.hp_default, null);
        this.mImpTypeTextColor = ResourcesCompat.getColor(mContext.getResources(), android.R.color.white, null);
    }

    private Class<? extends PrintOSAppWidgetProvider> getAppWidgetProvider() {
        return mWidgetType.getProviderClass();
    }

    private Intent getProviderIntent() {
        return new Intent(mContext, getAppWidgetProvider());
    }

    public int[] getAppWidgetIds() {
        ComponentName provider = new ComponentName(mContext, getAppWidgetProvider());
        return mAppWidgetManager.getAppWidgetIds(provider);
    }

    public void loading() {

        HPLogger.d(TAG, "Start loading");

        final int widgetLoading = R.id.widget_loading;
        setViewVisibility(widgetLoading, View.VISIBLE);
        setViewVisibility(R.id.widget_empty_layout, View.GONE);
        mAppWidgetManager.updateAppWidget(mAppWidgetIds, this);
    }

    public void loadComplete() {

        HPLogger.d(TAG, "Load complete");

        final int widgetLoading = R.id.widget_loading;
        setViewVisibility(widgetLoading, View.GONE);
        mAppWidgetManager.updateAppWidget(mAppWidgetIds, this);
    }

    public void setOnOpenAppClickPendingIntent() {
        Intent intent = getProviderIntent();
        intent.setAction(PrintOSAppWidgetProvider.ACTION_OPEN_APP);
        PendingIntent logoPendingIntent = PendingIntent.getBroadcast(mContext, 0, intent, 0);
        setOnClickPendingIntent(R.id.widget_open_app_button, logoPendingIntent);
    }

    public void setOnPreviousSiteClickPendingIntent() {
        Intent intent = getProviderIntent();
        intent.setAction(PrintOSAppWidgetProvider.ACTION_SELECT_PREVIOUS_SITE);
        PendingIntent logoPendingIntent = PendingIntent.getBroadcast(mContext, 0, intent, 0);
        setOnClickPendingIntent(R.id.widget_previous_site_button, logoPendingIntent);
    }


    public void setOnNextSiteClickPendingIntent() {
        Intent intent = getProviderIntent();
        intent.setAction(PrintOSAppWidgetProvider.ACTION_SELECT_NEXT_SITE);
        PendingIntent refreshPendingIntent = PendingIntent.getBroadcast(mContext, 0, intent, 0);
        setOnClickPendingIntent(R.id.widget_next_site_button, refreshPendingIntent);
    }

    public void setShowNextPrevButton(boolean show) {
        int visibility = show ? View.VISIBLE : View.GONE;
        setViewVisibility(R.id.widget_next_site_button, visibility);
        setViewVisibility(R.id.widget_previous_site_button, visibility);
        mAppWidgetManager.updateAppWidget(mAppWidgetIds, this);
    }

    public void showNext() {
        showNext(R.id.sites_view_flipper);
        mAppWidgetManager.updateAppWidget(mAppWidgetIds, this);
    }

    public void showPrevious() {
        showPrevious(R.id.sites_view_flipper);
        mAppWidgetManager.updateAppWidget(mAppWidgetIds, this);
    }

    public void setDisplayedChild(int position) {
        setDisplayedChild(R.id.sites_view_flipper, position);
        mAppWidgetManager.updateAppWidget(mAppWidgetIds, this);
    }

    public void showErrorView(String errorMessage) {

        setViewVisibility(R.id.sites_view_flipper, View.GONE);
        setViewVisibility(R.id.widget_empty_layout, View.VISIBLE);
        setViewVisibility(R.id.widget_loading, View.GONE);
        setViewVisibility(R.id.widget_open_app_button, View.INVISIBLE);
        setTextViewText(R.id.widget_error_message_text_view, errorMessage);

        mAppWidgetManager.updateAppWidget(mAppWidgetIds, this);

    }

    public void showUserNotLoggedInView() {

        setViewVisibility(R.id.sites_view_flipper, View.GONE);
        setViewVisibility(R.id.widget_empty_layout, View.VISIBLE);
        setViewVisibility(R.id.widget_open_app_button, View.VISIBLE);
        setViewVisibility(R.id.widget_loading, View.GONE);
        setTextViewText(R.id.widget_error_message_text_view,
                mContext.getString(R.string.widget_empty_data_description_text));
        setTextViewText(R.id.widget_open_app_button, mContext.getString(R.string.widget_open_app_button_text));

        mAppWidgetManager.updateAppWidget(mAppWidgetIds, this);

    }

    public void setAppName() {

        Typeface hpLightTypeface = getWidgetTypeFace(TypefaceManager.HPTypeface.HP_LIGHT);

        Bitmap appNameBitmap = WidgetUtils.getTextBitmap(mContext,
                mContext.getString(R.string.app_name), hpLightTypeface
                , mDefaultTextColor, WIDGET_LARGE_FONT_SIZE_SP);

        setImageViewBitmap(R.id.image_app_name, appNameBitmap);
    }

    public void bindListViewAdapter() {

        int listViewResId = R.id.sites_view_flipper;
        Intent serviceIntent = new Intent(mContext, mWidgetType.getServiceClass());
        serviceIntent.setData(Uri.parse(serviceIntent.toUri(Intent.URI_INTENT_SCHEME)));
        //setEmptyView(listViewResId, R.id.widget_empty_layout);
        setRemoteAdapter(listViewResId, serviceIntent);

        Intent listItemIntent = getProviderIntent();
        listItemIntent.setAction(PrintOSAppWidgetProvider.ACTION_CLICK_GAUGE);
        listItemIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, listViewResId);
        PendingIntent pendingIntentTemplate = PendingIntent.getBroadcast(mContext, 0, listItemIntent, 0);
        setPendingIntentTemplate(listViewResId, pendingIntentTemplate);

    }

    public void notifyAppWidgetViewDataChanged() {
        int[] appIds = getAppWidgetIds();
        // notify data changed for view flipper.
        HPLogger.d(TAG, "notify app widget data changed");
        mAppWidgetManager.notifyAppWidgetViewDataChanged(appIds, R.id.sites_view_flipper);
    }

    /**
     * @param printBeatDashboardViewModel
     * @return
     */
    public RemoteViews applyViewModel(final PrintBeatDashboardViewModel printBeatDashboardViewModel) {

        if (printBeatDashboardViewModel == null) {
            return null;
        }

        RemoteViews views = new RemoteViews(mContext.getPackageName(), R.layout.app_widget_page);

        updatePage(views, printBeatDashboardViewModel);

        return views;
    }

    private void updatePage(RemoteViews remoteViews, PrintBeatDashboardViewModel printBeatDashboardViewModel) {

        TodayViewModel todayViewModel = printBeatDashboardViewModel.getTodayViewModel();

        Bitmap siteNameBitmap = WidgetUtils.getTextBitmap(mContext, todayViewModel.getSiteViewModel().getName(),
                getWidgetDefaultTypeFace(), mDefaultTextColor, WIDGET_DEFAULT_FONT_SIZE_SP);

        remoteViews.setImageViewBitmap(R.id.image_site_name, siteNameBitmap);

        remoteViews.setImageViewBitmap(R.id.progress_arc_image, WidgetUtils.getTodayArcBitmap(todayViewModel, mContext));

        Bundle extras = new Bundle();
        Intent fillInIntent = new Intent();
        fillInIntent.putExtras(extras);
        remoteViews.setOnClickFillInIntent(R.id.progress_arc_container, fillInIntent);

        int color = HPScoreUtils.getProgressColor(mContext, todayViewModel.getPrintVolumeValue(),
                todayViewModel.getPrintVolumeIntraDailyTargetValue(), false);

        Typeface hpLightTypeface = getWidgetTypeFace(TypefaceManager.HPTypeface.HP_LIGHT);

        Bitmap printVolumeBitmap = WidgetUtils.getTextBitmap(mContext,
                HPLocaleUtils.getLocalizedValue((int) todayViewModel.getPrintVolumeValue()),
                getWidgetDefaultTypeFace(), color, WIDGET_LARGE_FONT_SIZE_SP);

        Bitmap shiftTargetBitmap = WidgetUtils.getTextBitmap(mContext,
                HPLocaleUtils.getLocalizedValue((int) todayViewModel.getPrintVolumeShiftTargetValue()),
                getWidgetDefaultTypeFace(), mDefaultTextColor, WIDGET_LARGE_FONT_SIZE_SP);

        Bitmap impressionsBitmap = WidgetUtils.getTextBitmap(mContext,
                mContext.getString(R.string.impressions),
                hpLightTypeface, mDefaultTextColor, WIDGET_LARGE_FONT_SIZE_SP);

        Bitmap goalBitmap = WidgetUtils.getTextBitmap(mContext,
                mContext.getString(R.string.today_panel_goal),
                hpLightTypeface, mDefaultTextColor, WIDGET_LARGE_FONT_SIZE_SP);

        remoteViews.setImageViewBitmap(R.id.image_total_print_volume_value, printVolumeBitmap);
        remoteViews.setImageViewBitmap(R.id.image_total_shift_value, shiftTargetBitmap);
        remoteViews.setImageViewBitmap(R.id.image_impressions, impressionsBitmap);
        remoteViews.setImageViewBitmap(R.id.image_goal, goalBitmap);

        List<DeviceViewModel> deviceViewModelList = todayViewModel.getDeviceViewModels();

        int maxDevices = Math.min(mWidgetType.getMaxDevices(), deviceViewModelList.size());

        int listViewResId = R.id.listView;

        remoteViews.removeAllViews(listViewResId);

        for (int index = 0; index < maxDevices; index++) {

            DeviceViewModel deviceViewModel = deviceViewModelList.get(index);

            RemoteViews deviceRowItem = new RemoteViews(mContext.getPackageName(), deviceViewModel.isRTSupported() ? R.layout.widget_listview_row : R.layout.widget_listview_row_non_rt);

            Intent intent = getProviderIntent();
            intent.setAction(PrintOSAppWidgetProvider.ACTION_JUMP_DEVICE_ITEM);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(mContext, 0, intent, 0);
            deviceRowItem.setOnClickPendingIntent(R.id.item_container, pendingIntent);

            Bitmap pressNameBitmap = WidgetUtils.getTextBitmap(mContext, deviceViewModel.getName(), getWidgetDefaultTypeFace(), mDefaultTextColor, WIDGET_DEFAULT_FONT_SIZE_SP);
            deviceRowItem.setImageViewBitmap(R.id.image_press_name, pressNameBitmap);

            if (deviceViewModel.isRTSupported()) {

                Bitmap pressStateBitmap;
                int textColor;
                if (deviceViewModel.isPressDisconnected()) {

                    textColor = ResourcesCompat.getColor(mContext.getResources(), R.color.hp_default, null);
                    pressStateBitmap = WidgetUtils.getTextBitmap(mContext, mContext.getString(R.string.press_disconnected), getWidgetDefaultTypeFace(), textColor, WIDGET_DEFAULT_FONT_SIZE_SP);

                } else {

                    textColor = ResourcesCompat.getColor(mContext.getResources(), deviceViewModel.getDeviceState().getColor(), null);
                    pressStateBitmap = WidgetUtils.getTextBitmap(mContext, mContext.getString(deviceViewModel.getDeviceState().getDisplayNameResourceId()),
                            getWidgetDefaultTypeFace(), textColor, WIDGET_DEFAULT_FONT_SIZE_SP);

                }

                deviceRowItem.setImageViewBitmap(R.id.image_press_state, pressStateBitmap);

                int jobsInQueue = deviceViewModel.getJobsInQueue();
                String jobsInQueueText = jobsInQueue == 0 ? mContext.getString(R.string.latex_no_jobs_in_queue)
                        : jobsInQueue == 1 ? mContext.getString(R.string.widget_one_job_in_queue)
                        : String.format(mContext.getString(R.string.widget_jobs_in_queue), jobsInQueue);

                Bitmap jobsInQueueBitmap = WidgetUtils.getTextBitmap(mContext, jobsInQueueText, getWidgetDefaultTypeFace(), mDefaultTextColor, WIDGET_SMALL_FONT_SIZE_SP);

                deviceRowItem.setImageViewBitmap(R.id.image_jobs_in_queue_value, jobsInQueueBitmap);

                int durationToDone = deviceViewModel.getDurationToDone();

                if (durationToDone > 0) {

                    String timeToComplete =
                            HPLocaleUtils.getLocalizedTimeString(mContext, deviceViewModel.getDurationToDone(), deviceViewModel.getDurationToDoneUnit());
                    Bitmap timeToCompleteBitmap = WidgetUtils.getTextBitmap(mContext, timeToComplete, getWidgetDefaultTypeFace(), mDefaultTextColor, WIDGET_SMALL_FONT_SIZE_SP);

                    deviceRowItem.setImageViewBitmap(R.id.image_jobs_time_to_complete, timeToCompleteBitmap);
                }

                deviceRowItem.setViewVisibility(R.id.press_jobs_time_complete_layout, jobsInQueue == 0
                        || durationToDone <= 0 ? View.GONE : View.VISIBLE);

            } else {

                deviceRowItem.setViewVisibility(R.id.image_press_state, View.INVISIBLE);

            }

            if (index == maxDevices - 1) {
                deviceRowItem.setViewVisibility(R.id.separator_view, View.GONE);
            }

            remoteViews.addView(R.id.listView, deviceRowItem);

        }

        if (mWidgetType == WidgetType.INDIGO_4X3) {

            TodayInfoTooltipViewModel infoTooltipViewModel = todayViewModel.getInfoTooltipViewModel();

            if (infoTooltipViewModel != null && infoTooltipViewModel.getImpressionsList() != null
                    && infoTooltipViewModel.getImpressionsList().size() > 0) {

                remoteViews.setViewVisibility(R.id.today_info_list, View.VISIBLE);

                remoteViews.removeAllViews(R.id.today_info_list);

                for (TodayInfoTooltipViewModel.InfoItemViewModel infoItemViewModel : infoTooltipViewModel.getImpressionsList()) {

                    RemoteViews infoRow = new RemoteViews(mContext.getPackageName(), R.layout.widget_info_item_layout);

                    Bitmap nameBitmap = WidgetUtils.getTextBitmap(mContext, infoItemViewModel.getInfoItemName(),
                            getWidgetDefaultTypeFace(), mImpTypeTextColor, WIDGET_DEFAULT_FONT_SIZE_SP);

                    Bitmap valueBitmap = WidgetUtils.getTextBitmap(mContext, infoItemViewModel.getInfoItemValue(),
                            getWidgetDefaultTypeFace(), color, WIDGET_DEFAULT_FONT_SIZE_SP);

                    infoRow.setImageViewBitmap(R.id.image_imp_type, nameBitmap);
                    infoRow.setImageViewBitmap(R.id.image_imp_value, valueBitmap);
                    remoteViews.addView(R.id.today_info_list, infoRow);
                }

            } else {

                remoteViews.setViewVisibility(R.id.today_info_list, View.GONE);
            }

        } else {

            remoteViews.setViewVisibility(R.id.today_info_list, View.GONE);
        }

    }

    private Typeface getWidgetDefaultTypeFace() {
        return getWidgetTypeFace(TypefaceManager.HPTypeface.HP_REGULAR);
    }

    private Typeface getWidgetTypeFace(int typefaceValue) {
        return TypefaceManager.getTypeface(mContext, typefaceValue);
    }

    public void showWidgetFlipper() {

        setViewVisibility(R.id.sites_view_flipper, View.VISIBLE);
        setViewVisibility(R.id.widget_empty_layout, View.GONE);
        setViewVisibility(R.id.widget_loading, View.GONE);

        mAppWidgetManager.updateAppWidget(mAppWidgetIds, this);

    }
}
