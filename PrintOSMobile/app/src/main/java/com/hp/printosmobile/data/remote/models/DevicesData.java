package com.hp.printosmobile.data.remote.models;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Osama Taha on 5/31/16.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class DevicesData {

    @JsonProperty("total")
    private int total;
    @JsonProperty("limit")
    private int limit;
    @JsonProperty("offset")
    private int offset;
    @JsonProperty("devices")
    List<DeviceData> devices;

    public DevicesData() {
    }

    @JsonProperty("total")
    public int getTotal() {
        return total;
    }

    @JsonProperty("total")
    public void setTotal(int total) {
        this.total = total;
    }

    @JsonProperty("limit")
    public int getLimit() {
        return limit;
    }

    @JsonProperty("limit")
    public void setLimit(int limit) {
        this.limit = limit;
    }

    @JsonProperty("offset")
    public int getOffset() {
        return offset;
    }

    @JsonProperty("offset")
    public void setOffset(int offset) {
        this.offset = offset;
    }

    @JsonProperty("devices")
    public List<DeviceData> getDevices() {
        return devices;
    }

    @JsonProperty("devices")
    public void setDevices(List<DeviceData> devices) {
        this.devices = devices;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("DevicesData{");
        sb.append("total=").append(total);
        sb.append(", limit=").append(limit);
        sb.append(", offset=").append(offset);
        sb.append(", devices=").append(devices);
        sb.append('}');
        return sb.toString();
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class DeviceData extends BaseDeviceData {

        @JsonProperty("organizationId")
        private String organizationId;
        @JsonProperty("deviceId")
        private String deviceId;
        @JsonProperty("description")
        private String description;
        @JsonProperty("SerialNumber")
        private String SerialNumber;
        @JsonProperty("serialNumberDisplay")
        private String serialNumberDisplay;
        @JsonProperty("model")
        private String model;
        @JsonProperty("name")
        private String name;
        @JsonProperty("type")
        private String type;
        @JsonProperty("active")
        private boolean active;
        @JsonProperty("locked")
        private boolean locked;
        @JsonProperty("anonymous")
        private boolean anonymous;

        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<String, Object>();

        public DeviceData() {
        }

        public String getOrganizationId() {
            return organizationId;
        }

        public void setOrganizationId(String organizationId) {
            this.organizationId = organizationId;
        }

        public String getDeviceId() {
            return deviceId;
        }

        public void setDeviceId(String deviceId) {
            this.deviceId = deviceId;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getSerialNumber() {
            return SerialNumber;
        }

        public void setSerialNumber(String serialNumber) {
            SerialNumber = serialNumber;
        }

        public String getModel() {
            return model;
        }

        public void setModel(String model) {
            this.model = model;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        @JsonProperty("active")
        public boolean isActive() {
            return active;
        }

        @JsonProperty("active")
        public void setActive(boolean active) {
            this.active = active;
        }

        @JsonProperty("locked")
        public boolean isLocked() {
            return locked;
        }

        @JsonProperty("locked")
        public void setLocked(boolean locked) {
            this.locked = locked;
        }

        @JsonProperty("anonymous")
        public boolean isAnonymous() {
            return anonymous;
        }

        @JsonProperty("anonymous")
        public void setAnonymous(boolean anonymous) {
            this.anonymous = anonymous;
        }

        @JsonProperty("serialNumberDisplay")
        public String getSerialNumberDisplay() {
            return serialNumberDisplay;
        }

        @JsonProperty("serialNumberDisplay")
        public void setSerialNumberDisplay(String serialNumberDisplay) {
            this.serialNumberDisplay = serialNumberDisplay;
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperties(Map<String, Object> additionalProperties) {
            this.additionalProperties = additionalProperties;
        }

        @Override
        public String toString() {
            final StringBuffer sb = new StringBuffer("DeviceData{");
            sb.append("organizationId='").append(organizationId).append('\'');
            sb.append(", deviceId='").append(deviceId).append('\'');
            sb.append(", description='").append(description).append('\'');
            sb.append(", SerialNumber='").append(SerialNumber).append('\'');
            sb.append(", model='").append(model).append('\'');
            sb.append(", name='").append(name).append('\'');
            sb.append(", type='").append(type).append('\'');
            sb.append(", additionalProperties=").append(additionalProperties);
            sb.append('}');
            return sb.toString();
        }
    }
}