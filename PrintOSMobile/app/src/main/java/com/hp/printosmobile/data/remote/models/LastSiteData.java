package com.hp.printosmobile.data.remote.models;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Osama Taha on 10/10/16.
 */

public class LastSiteData {

    @JsonProperty("additionalData")
    private LastSite additionalData;

    @JsonProperty("additionalData")
    public LastSite getAdditionalData() {
        return additionalData;
    }

    @JsonProperty("additionalData")
    public void setAdditionalData(LastSite additionalData) {
        this.additionalData = additionalData;
    }

    public static class LastSite {

        @JsonProperty("lastSiteId")
        private String lastSiteId;
        @JsonProperty("type")
        private String type;
        @JsonProperty("version")
        private String version;

        public LastSite(String lastSiteId, String type, String version) {
            this.lastSiteId = lastSiteId;
            this.type = type;
            this.version = version;
        }

        @JsonProperty("lastSiteId")
        public String getLastSiteId() {
            return lastSiteId;
        }

        @JsonProperty("lastSiteId")
        public void setLastSiteId(String lastSiteId) {
            this.lastSiteId = lastSiteId;
        }

        @JsonProperty("type")
        public String getType() {
            return type;
        }

        @JsonProperty("type")
        public void setType(String type) {
            this.type = type;
        }

        @JsonProperty("version")
        public String getVersion() {
            return version;
        }

        @JsonProperty("version")
        public void setVersion(String version) {
            this.version = version;
        }

        @Override
        public String toString() {
            final StringBuffer sb = new StringBuffer("LastSite{");
            sb.append("lastSiteId=").append(lastSiteId);
            sb.append(", type=").append(type);
            sb.append(", version=").append(version);
            sb.append('}');
            return sb.toString();
        }
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("LastSiteData{");
        sb.append("additionalData=").append(additionalData);
        sb.append('}');
        return sb.toString();
    }
}
