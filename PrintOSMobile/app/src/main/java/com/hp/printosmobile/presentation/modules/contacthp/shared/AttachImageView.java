package com.hp.printosmobile.presentation.modules.contacthp.shared;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.FrameLayout;

import com.hp.printosmobile.R;
import com.kyleduo.switchbutton.SwitchButton;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * @author Anwar Asbah Create on 10/10/2016
 */
public class AttachImageView extends FrameLayout implements AttachImageAdapter.AttachImageAdapterCallback, ImageFetchListener {

    @Bind(R.id.enable_toggle)
    SwitchButton enableToggle;
    @Bind(R.id.image_attachment_list)
    RecyclerView imageList;

    private AttachImageAdapter attachImageAdapter;
    private AttachImageClickListener listener;
    private boolean isEnabled;


    public AttachImageView(Context context) {
        this(context, null);
        init();
    }

    public AttachImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public AttachImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public void init() {
        isEnabled = true;

        View view = inflate(getContext(), R.layout.contact_hp_attach_image_view, this);
        ButterKnife.bind(this, view);

        attachImageAdapter = new AttachImageAdapter(getContext(), this);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);

        imageList.setAdapter(attachImageAdapter);
        imageList.setLayoutManager(manager);
        imageList.setHasFixedSize(true);

        enableToggle.setBackgroundResource(R.drawable.gray_rounded_rect_frame);
        enableToggle.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                enableToggle.setBackgroundResource(isChecked? R.drawable.blue_rounded_rect_frame : R.drawable.gray_rounded_rect_frame);
                imageList.setVisibility(isChecked ? VISIBLE : GONE);
            }
        });
    }

    public void addAttachImageClickListener(AttachImageClickListener listener) {
        this.listener = listener;
    }

    @Override
    public void onAddImageClicked() {
        if (listener != null && isEnabled) {
            listener.onAttachImageClicked(this);
        }
    }

    @Override
    public void onImageFetched(Uri uri) {
        if (uri != null) {
            attachImageAdapter.addImage(uri);
        }
    }

    public List<Uri> getAttachedImagesUri() {
        if (enableToggle.isChecked()) {
            return attachImageAdapter.getImageUri();
        }
        return null;
    }

    public void setEnabled(boolean isEnabled) {
        this.isEnabled = isEnabled;
        enableToggle.setEnabled(isEnabled);
        attachImageAdapter.setEnabled(isEnabled);
    }

    public interface AttachImageClickListener {
        void onAttachImageClicked(ImageFetchListener imageFetchListener);
    }
}