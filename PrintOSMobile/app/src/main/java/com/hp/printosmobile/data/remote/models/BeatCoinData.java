package com.hp.printosmobile.data.remote.models;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Anwar Asbah 7/18/2017
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BeatCoinData {

    @JsonProperty("coins")
    int coins;
    @JsonProperty("coinsURL")
    String coinsURL;
    @JsonProperty("coinsURLExpirationDate")
    String coinsURLExpirationDate;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("coins")
    public int getCoins() {
        return coins;
    }

    @JsonProperty("coins")
    public void setCoins(int coins) {
        this.coins = coins;
    }

    @JsonProperty("coinsURL")
    public String getCoinsURL() {
        return coinsURL;
    }

    @JsonProperty("coinsURL")
    public void setCoinsURL(String coinsURL) {
        this.coinsURL = coinsURL;
    }

    @JsonProperty("coinsURLExpirationDate")
    public String getCoinsURLExpirationDate() {
        return coinsURLExpirationDate;
    }

    @JsonProperty("coinsURLExpirationDate")
    public void setCoinsURLExpirationDate(String coinsURLExpirationDate) {
        this.coinsURLExpirationDate = coinsURLExpirationDate;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
}