package com.hp.printosmobile.data.remote.services;

import com.hp.printosmobile.data.remote.ApiConstants;
import com.hp.printosmobile.data.remote.models.NotificationApplicationData;
import com.hp.printosmobile.data.remote.models.NotificationApplicationEventData;
import com.hp.printosmobile.data.remote.models.NotificationSubscription;
import com.hp.printosmobile.data.remote.models.NotificationsData;
import com.hp.printosmobile.data.remote.models.NotificationsStringData;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by Osama Taha on 6/29/16.
 */
public interface NotificationService {

    public static final String APP_NAME_PARAMETER_VALUE = "printbeat";

    @POST(ApiConstants.NOTIFICATION_SERVICE_REGISTRATION_API)
    @Headers("Content-Type: application/json")
    Call<Void> registerDeviceForPushNotification(@Query("deviceToken") String token, @Query("locale") String locale, @Query("appname") String appName);

    @DELETE(ApiConstants.NOTIFICATION_SERVICE_REGISTRATION_API)
    @Headers("Content-Type: application/json")
    Call<Void> unRegisterDeviceForPushNotification(@Query("deviceToken") String token, @Query("appname") String appName);

    @DELETE(ApiConstants.NOTIFICATION_SERVICE_REGISTRATION_API)
    @Headers("Content-Type: application/json")
    Observable<Response<Void>> unRegisterDeviceForPushNotificationAsync(@Query("deviceToken") String token);

    @GET(ApiConstants.NOTIFICATION_SUBSCRIPTION_API)
    Observable<Response<NotificationSubscription>> getUserSubscriptions();

    @POST(ApiConstants.NOTIFICATION_SUBSCRIBE_API)
    Observable<Response<NotificationSubscription.Subscription>> subscribe(@Query("eventdescname") String eventDescName);

    @DELETE(ApiConstants.NOTIFICATIONS_REMOVAL_API)
    Observable<ResponseBody> unsubscribe(@Path("eventDescID") String eventDescID);

    @GET(ApiConstants.NOTIFICATIONS_STRINGS_API)
    Observable<Response<NotificationsStringData>> getNotificationLocalizedStrings(@Path("locale") String locale);

    @GET(ApiConstants.NOTIFICATIONS_ITEMS_API)
    Observable<Response<NotificationsData>> getUserNotifications(@Query("limit") int limit, @Query("offset") int offset);

    @PUT(ApiConstants.NOTIFICATIONS_ACTION_API)
    Observable<ResponseBody> markNotificationAsRead(@Path("id") int id, @Query("ueid") int userId);

    @DELETE(ApiConstants.NOTIFICATIONS_ACTION_API)
    Observable<ResponseBody> deleteNotification(@Path("id") int id, @Query("ueid") int userId);

    @PUT(ApiConstants.NOTIFICATION_SUBSCRIPTION_DESTINATION)
    Observable<ResponseBody> setSubscriptionDestination(@Path("subscription_id") String subscriptionID);

    @GET(ApiConstants.NOTIFICATION_APPLICATION_API)
    Observable<Response<NotificationApplicationData>> getNotificationApplications();

    @GET(ApiConstants.NOTIFICATION_EVENT_DESCRIPTION_API)
    Call<NotificationApplicationEventData> getNotificationApplicationEvents(@Path("application_id") String application_id);
}
