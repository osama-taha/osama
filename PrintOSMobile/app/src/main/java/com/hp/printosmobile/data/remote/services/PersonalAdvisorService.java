package com.hp.printosmobile.data.remote.services;

import com.hp.printosmobile.data.remote.ApiConstants;
import com.hp.printosmobile.data.remote.models.AdviceData;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Retrofit REST interface definition for Personal advisor
 *
 * @author Anwar Asbah
 */
public interface PersonalAdvisorService {

    /**
     * @return list of Advises
     */
    @GET(ApiConstants.PERSONAL_ADVISOR_API)
    Observable<Response<List<AdviceData>>> getAdvices(@Query("language") String language, @Query("device") List<String> device, @Query("formatted") boolean formatted);

    /**
     * Suppress an advise
     */
    @POST(ApiConstants.PERSONAL_ADVISOR_SUPPRESS_API)
    Observable<ResponseBody> suppressAdvice(@Path("adviceId") String adviceId, @Query("device") String device, @Query("suppressed") boolean isSuppressed);

    /**
     * @return list of Advises
     */
    @POST(ApiConstants.PERSONAL_ADVISOR_LIKE_API)
    Observable<ResponseBody> likeAdvice(@Path("adviceId") String adviceId, @Query("device") String device, @Query("liked") boolean isLiked);

    @POST(ApiConstants.PERSONAL_ADVISOR_UNHIDE_ALL_API)
    Observable<ResponseBody> unhideAllAdvices(@Query("devices") List<String> devices, @Query("suppressed") boolean suppress);
}
