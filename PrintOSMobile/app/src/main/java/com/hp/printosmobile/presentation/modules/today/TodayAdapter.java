package com.hp.printosmobile.presentation.modules.today;

import android.content.Context;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hp.printosmobile.R;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.data.remote.models.PreferencesData;
import com.hp.printosmobile.presentation.modules.main.BusinessUnitEnum;
import com.hp.printosmobile.presentation.modules.shared.DeviceViewModel;
import com.hp.printosmobile.presentation.modules.shared.Unit;
import com.hp.printosmobile.presentation.modules.todayhistogram.TodayHistogramView;
import com.hp.printosmobile.presentation.modules.todayhistogram.TodayHistogramViewModel;
import com.hp.printosmobile.presentation.modules.tooltip.IntraDailyTooltip;
import com.hp.printosmobile.presentation.modules.tooltip.MaintenanceFanTooltip;
import com.hp.printosmobile.presentation.modules.tooltip.TodayInfoTooltip;
import com.hp.printosmobile.utils.HPLocaleUtils;
import com.hp.printosmobile.utils.HPScoreUtils;
import com.hp.printosmobile.utils.HPStringUtils;
import com.hp.printosmobilelib.core.utils.HPDateUtils;
import com.hp.printosmobilelib.ui.utils.DividerItemDecoration;
import com.hp.printosmobilelib.ui.widgets.HPProgressArc;
import com.hp.printosmobilelib.ui.widgets.HPViewPager;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Anwar Asbah on 9/7/2016.
 */
public class TodayAdapter extends PagerAdapter {

    public final static String TAG = TodayAdapter.class.getSimpleName();

    private static final int NUMBER_OF_VIEWS = 2;
    public static final int TODAY_VIEW_POSITION = 1;
    public static final int HISTOGRAM_VIEW_POSITION = 0;
    private final PreferencesData.UnitSystem unitSystem;

    private Context context;
    private TodayViewModel todayViewModel;
    private TodayHistogramViewModel todayHistogramViewModel;
    private TodayViewBuilder todayViewBuilder;
    private HistogramViewBuilder histogramViewBuilder;
    private TodayAdapterCallbacks callback;
    private TodayDevicesAdapter todayDevicesAdapter;
    private boolean histogramDataArrived;

    public TodayAdapter(Context context, TodayAdapterCallbacks callback) {
        this.context = context;
        this.todayViewBuilder = new TodayViewBuilder();
        this.histogramViewBuilder = new HistogramViewBuilder();
        this.callback = callback;
        this.unitSystem = PrintOSPreferences.getInstance(context).getUnitSystem();
    }

    public void setTodayViewModel(TodayViewModel todayViewModel) {
        this.todayViewModel = todayViewModel;
        todayViewBuilder.build();
    }

    public void setTodayHistogramViewModel(TodayHistogramViewModel todayHistogramViewModel) {
        this.todayHistogramViewModel = todayHistogramViewModel;
        this.histogramDataArrived = true;
        histogramViewBuilder.build();
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View layout;

        switch (position) {
            case TODAY_VIEW_POSITION:
                layout = inflater.inflate(R.layout.today_panel_press_view, container, false);
                todayViewBuilder.attachView(layout);
                todayViewBuilder.build();
                break;
            default:
                layout = inflater.inflate(R.layout.today_panel_histogram_view, container, false);
                histogramViewBuilder.attachView(layout);
                histogramViewBuilder.build();
                break;
        }

        container.addView(layout);
        return layout;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }


    @Override
    public void setPrimaryItem(ViewGroup container, int position, Object object) {
        super.setPrimaryItem(container, position, object);

        View view = (View) object;
        HPViewPager pager = (HPViewPager) container;
        if (view != null) {
            pager.measureCurrentView(view);
        }

    }

    @Override
    public int getCount() {
        return NUMBER_OF_VIEWS;
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    public void notifyDevicesList() {
        if (todayDevicesAdapter != null) {
            todayDevicesAdapter.notifyDataSetChanged();
        }
    }

    class TodayViewBuilder implements TodayDevicesAdapter.TodayDeviceAdapterCallback {

        @Bind(R.id.recycler_view_printers)
        RecyclerView printersRecyclerView;
        @Bind(R.id.info_tooltip_button)
        ImageButton infoTooltipButton;
        @Bind(R.id.progress_arc)
        HPProgressArc progressArc;
        @Bind(R.id.imp_value_text)
        TextView impressionValueText;
        @Bind(R.id.goal_value_text)
        TextView goalValueText;
        @Bind(R.id.impressions_list)
        LinearLayout impressionsList;
        @Bind(R.id.impressions_title_text_view)
        TextView impressionTitleTextView;
        @Bind(R.id.progress_arc_container)
        View progressArcContainer;
        @Bind(R.id.last_update_time_text)
        TextView lastUpdateTimeText;
        @Bind(R.id.last_update_time_value_text)
        TextView lastUpdateTimeValueText;

        IntraDailyTooltip tooltip;
        ViewGroup parentView;

        View view;

        private void attachView(View view) {
            this.view = view;
        }

        private void build() {

            if (todayViewModel == null || view == null) {
                return;
            }

            ButterKnife.bind(this, view);

            parentView = (ViewGroup) view;

            printersRecyclerView.setHasFixedSize(true);
            printersRecyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
            printersRecyclerView.setNestedScrollingEnabled(false);

            if (todayViewModel.getInfoTooltipViewModel() != null && todayViewModel.getInfoTooltipViewModel().hasInfo()) {
                infoTooltipButton.setVisibility(View.VISIBLE);
            } else {
                infoTooltipButton.setVisibility(View.GONE);
            }

            lastUpdateTimeText.setText(context.getString(R.string.device_detail_last_update, ""));
            lastUpdateTimeValueText.setText(HPDateUtils.getCurrentTime(context));

            buildPrintersList();
            buildTodayScoreView(todayViewModel.getPrintVolumeValue(),
                    todayViewModel.getPrintVolumeIntraDailyTargetValue(),
                    todayViewModel.getPrintVolumeShiftTargetValue());

            infoTooltipButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showInfoTooltip();
                }
            });

            progressArc.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    openIntraDailyTargetToolTip(event.getX(), event.getY());
                    return false;
                }
            });

            printersRecyclerView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    printersRecyclerView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    if (callback != null) {
                        callback.onTodayViewInitialized();
                    }
                }
            });
        }

        private void openIntraDailyTargetToolTip(float x, float y) {

            if (tooltip == null) {
                tooltip = new IntraDailyTooltip(context);
            }

            HPProgressArc.ProgressArcClickEvent event = progressArc.isThumbClicked(x, y);
            if (event != null) {
                String intraDailyTargetValue = "";
                if (todayViewModel.getBusinessUnitViewModel().getBusinessUnit() == BusinessUnitEnum.INDIGO_PRESS) {
                    intraDailyTargetValue = context.getString(R.string.imp_value, HPLocaleUtils.getLocalizedValue(
                            (int) todayViewModel.getPrintVolumeIntraDailyTargetValue()
                    ));
                } else {

                    intraDailyTargetValue = context.getString(Unit.SQM.getUnitTextResourceId(unitSystem, Unit.UnitStyle.VALUE),
                            HPLocaleUtils.getLocalizedValue((int) todayViewModel.getPrintVolumeIntraDailyTargetValue()));
                }

                String tooltipMsg = context.getString(R.string.tooltip_intra_daily_target, intraDailyTargetValue);

                tooltip.updateView(HPStringUtils.setSpannableRegexBold(tooltipMsg, intraDailyTargetValue));
                tooltip.showToolTip(progressArc, event.locationX, event.locationY, event.gravity);
            }
        }

        private void buildPrintersList() {

            if (todayViewModel.getDeviceViewModels() == null) {
                return;
            }

            BusinessUnitEnum businessUnitEnum = todayViewModel.getBusinessUnitViewModel().getBusinessUnit();
            List<DeviceViewModel> devices = todayViewModel.getNameSortedDeviceViewModels();
            int numOfPrinters = devices.size();
            int maxNumOfPrintersToShow = businessUnitEnum == businessUnitEnum.LATEX_PRINTER ? numOfPrinters
                    : TodayViewModel.NUMBER_OF_DEVICES;
            int printersSize = numOfPrinters > maxNumOfPrintersToShow ? maxNumOfPrintersToShow : numOfPrinters;

            List<DeviceViewModel> deviceViewModels = devices.subList(0, printersSize);

            todayDevicesAdapter = new TodayDevicesAdapter(context, businessUnitEnum, deviceViewModels, this);
            printersRecyclerView.setAdapter(todayDevicesAdapter);
            printersRecyclerView.addItemDecoration(new DividerItemDecoration(context, 1, printersSize, false));

            todayDevicesAdapter.notifyDataSetChanged();
        }

        private void buildTodayScoreView(double score, double target, double max) {

            int progressColor = HPScoreUtils.getProgressColor(context, score, target, false);
            int trackColor = HPScoreUtils.getProgressColor(context, score, target, true);

            BusinessUnitEnum businessUnitEnum = todayViewModel.getBusinessUnitViewModel().getBusinessUnit();

            if (businessUnitEnum == BusinessUnitEnum.INDIGO_PRESS) {

                impressionTitleTextView.setText(context.getString(R.string.impressions));
                impressionValueText.setText(HPLocaleUtils.getLocalizedValue((int) score));
                goalValueText.setText(HPLocaleUtils.getLocalizedValue((int) max));

            } else {

                String squareMeters = context.getString(Unit.SQM.getUnitTextResourceId(unitSystem, Unit.UnitStyle.DEFAULT));

                impressionTitleTextView.setText(squareMeters);
                impressionValueText.setText(HPLocaleUtils.getLocalizedValue((int) score));
                goalValueText.setText(HPLocaleUtils.getLocalizedValue((int) max));

            }

            impressionValueText.setTextColor(progressColor);

            final int[] sweepColors = {progressColor, progressColor};
            final float[] sweepPosition = {0.5f, 1f};
            progressArc.setProgressSweepGradient(sweepColors, sweepPosition);
            progressArc.setRoundedEdges(false);
            progressArc.setTrackColor(trackColor);

            double percentageFill = max == 0 ? (score == 0 ? 0 : 1) : score / max;
            percentageFill = Math.max(0, percentageFill);

            double percentageTarget = max == 0 ? 0 : target / max;
            percentageTarget = Math.max(0, percentageTarget);

            progressArc.setThumb((int) (percentageTarget * 100) == 0 ? null : ResourcesCompat.getDrawable(context.getResources(), R.drawable.circle, null));
            progressArc.setTarget((int) (percentageTarget * 100));
            progressArc.setProgress((int) (percentageFill * 100));

            progressArcContainer.setVisibility(View.VISIBLE);

        }

        @Override
        public void onDeviceClicked(DeviceViewModel model) {
            if (callback != null && model != null) {
                callback.onDeviceClicked(model);
            }
        }

        @Override
        public void onHasAdviceIconClicked(DeviceViewModel model) {
            if (callback != null && model != null) {
                callback.onHasAdviceIconClicked(model);
            }
        }

        @Override
        public void onMaintenanceIconClicked(View view, DeviceViewModel model) {

            MaintenanceFanTooltip maintenanceFanTooltip = new MaintenanceFanTooltip(context);
            maintenanceFanTooltip.updateView(model.getMaintenanceState());
            maintenanceFanTooltip.showToolTip(view);
        }

        private void showInfoTooltip() {

            TodayInfoTooltip tooltip = new TodayInfoTooltip(context);
            tooltip.updateView(todayViewModel.getInfoTooltipViewModel());
            tooltip.showToolTipToLeftSide(infoTooltipButton, 2 * context.getResources().getDimension(R.dimen.panel_side_margin), -infoTooltipButton.getPaddingBottom() / 2);

        }
    }

    class HistogramViewBuilder {

        @Bind(R.id.histogram_view)
        TodayHistogramView histogram;
        @Bind(R.id.empty_data_set_text_view)
        TextView emptyDatasetTextView;

        View view;

        private void attachView(View view) {
            this.view = view;
        }

        private void build() {

            if (view == null || !histogramDataArrived) {
                return;
            }

            ButterKnife.bind(this, view);

            if (todayHistogramViewModel == null) {
                emptyDatasetTextView.setVisibility(View.VISIBLE);
                histogram.setVisibility(View.INVISIBLE);
                return;
            }

            emptyDatasetTextView.setVisibility(View.INVISIBLE);
            histogram.setVisibility(View.VISIBLE);

            int daysSpan = context.getResources().getInteger(R.integer.today_panel_histogram_span);
            histogram.setDaysSpan(daysSpan);
            histogram.setPaging(false);
            histogram.setOffset(0);

            histogram.setViewModel(todayHistogramViewModel);
        }
    }

    public interface TodayAdapterCallbacks {

        void onDeviceClicked(DeviceViewModel model);

        void onHasAdviceIconClicked(DeviceViewModel model);

        void onTodayViewInitialized();
    }
}
