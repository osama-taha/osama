package com.hp.printosmobile.presentation;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import rx.Subscription;

/**
 * @author Osama Taha
 */
public abstract class Presenter<T> {

    protected T mView;
    List<Subscription> subscriptions = new ArrayList<>();

    public void attachView(@NonNull T view) {
        this.mView = view;
    }

    public abstract void detachView();

    public void addSubscriber(Subscription subscription){
        subscriptions.add(subscription);
    }

    public void stopSubscribers(){
        for(Subscription subscription : subscriptions){
            if(!subscription.isUnsubscribed()) {
                subscription.unsubscribe();
            }
        }
        subscriptions.clear();
    }

    public T getView() {
        return mView;
    }
}