package com.hp.printosmobile.presentation.modules.week;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.support.v4.content.res.ResourcesCompat;

import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.modules.kpiview.KPIScoreStateEnum;
import com.hp.printosmobile.presentation.modules.kpiview.KPIViewModel;
import com.hp.printosmobile.utils.HPLocaleUtils;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

/**
 * Created by Anwar Asbah on 4/12/2016.
 */
public class WeekViewModel implements Comparable<WeekViewModel> {

    List<KPIViewModel> kpis;
    Date fromDate;
    Date toDate;
    int relativeIndexToLastWeek;
    int weekScore;
    int weekMaxScore;
    KPIScoreStateEnum performance;
    boolean hasRankingData;
    RankingViewModel rankingViewModel;
    List<WeekPressStatus> weekPressStatuses;

    public static Comparator<WeekViewModel> DATE_COMPARATOR = new Comparator<WeekViewModel>() {
        @Override
        public int compare(WeekViewModel model1, WeekViewModel model2) {
            return model1.compareTo(model2);
        }
    };
    private int weekNumber;

    public Date getFromDate() {
        return fromDate;
    }

    public int getWeekScore() {
        return weekScore;
    }

    public void setWeekScore(int weekScore) {
        this.weekScore = weekScore;
    }

    public int getWeekMaxScore() {
        return weekMaxScore;
    }

    public void setWeekMaxScore(int weekMaxScore) {
        this.weekMaxScore = weekMaxScore;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public int getRelativeIndexToLastWeek() {
        return relativeIndexToLastWeek;
    }

    public void setRelativeIndexToLastWeek(int relativeIndexToLastWeek) {
        this.relativeIndexToLastWeek = relativeIndexToLastWeek;
    }

    public KPIScoreStateEnum getPerformance() {
        return performance;
    }

    public void setPerformance(KPIScoreStateEnum performance) {
        this.performance = performance;
    }

    public List<KPIViewModel> getKpis() {
        return kpis;
    }

    public void setKpis(List<KPIViewModel> kpis) {
        this.kpis = kpis;
    }

    public boolean hasRankingData() {
        return hasRankingData;
    }

    public void setHasRankingData(boolean hasRankingData) {
        this.hasRankingData = hasRankingData;
    }

    public RankingViewModel getRankingViewModel() {
        return rankingViewModel;
    }

    public void setRankingViewModel(RankingViewModel rankingViewModel) {
        this.rankingViewModel = rankingViewModel;
    }

    public List<WeekPressStatus> getWeekPressStatuses() {
        return weekPressStatuses;
    }

    public void setWeekPressStatuses(List<WeekPressStatus> weekPressStatuses) {
        this.weekPressStatuses = weekPressStatuses;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("WeekViewModel{");
        sb.append("kpis=").append(kpis);
        sb.append(", fromDate=").append(fromDate);
        sb.append(", toDate=").append(toDate);
        sb.append(", relativeIndexToLastWeek=").append(relativeIndexToLastWeek);
        sb.append(", weekScore=").append(weekScore);
        sb.append(", weekMaxScore=").append(weekMaxScore);
        sb.append(", performance=").append(performance);
        sb.append('}');
        return sb.toString();
    }

    @Override
    public int compareTo(WeekViewModel another) {
        if (another == null) {
            return 1;
        }
        return this.getToDate().compareTo(another.toDate);
    }

    public void setWeekNumber(int weekNumber) {
        this.weekNumber = weekNumber;
    }

    public int getWeekNumber() {
        return weekNumber;
    }

    public static class WeekPressStatus implements Serializable {

        private static final float[] CORNER_RADII = new float[]{100, 100, 100, 100, 100, 100, 100, 100};

        public enum WeeklyPressStatusEnum {
            READY("READY", R.string.week_press_status_msg_ready, R.color.week_status_ready),
            NOT_READY("NOT_READY", R.string.week_press_status_msg_not_ready, R.color.week_status_not_ready),
            MISSING("MISSING", R.string.week_press_status_msg_missing, R.color.week_status_missing);

            String status;
            int msgID;
            int colorID;

            WeeklyPressStatusEnum(String status, int msgID, int colorID) {
                this.status = status;
                this.msgID = msgID;
                this.colorID = colorID;
            }

            public static WeeklyPressStatusEnum from(String status) {
                if (status != null) {
                    for (WeeklyPressStatusEnum statusEnum : WeeklyPressStatusEnum.values()) {
                        if (statusEnum.status.equals(status)) {
                            return statusEnum;
                        }
                    }
                }
                return READY;
            }

            public static String getMsg(Context context, WeeklyPressStatusEnum weekPressStatus, Date date) {
                if (weekPressStatus == null) {
                    return context.getString(READY.msgID);
                }
                switch (weekPressStatus) {
                    case MISSING:
                        return context.getString(MISSING.msgID);
                    case NOT_READY:
                        return context.getString(NOT_READY.msgID, HPLocaleUtils.getTimeAgo(context, date, true));
                    default:
                        return context.getString(READY.msgID, HPLocaleUtils.getTimeAgo(context, date, true));
                }
            }

            public static Drawable getColorDrawable(Context context, WeeklyPressStatusEnum weekPressStatus) {
                GradientDrawable shape = new GradientDrawable();
                shape.setShape(GradientDrawable.RECTANGLE);
                shape.setCornerRadii(CORNER_RADII);
                shape.setColor(ResourcesCompat.getColor(context.getResources(), weekPressStatus.colorID,  null));
                return shape;
            }
        }

        String serialNumber;
        String pressName;
        String pressType;
        WeeklyPressStatusEnum weeklyPressStatusEnum;
        Date dateReady;

        public String getSerialNumber() {
            return serialNumber;
        }

        public void setSerialNumber(String serialNumber) {
            this.serialNumber = serialNumber;
        }

        public String getPressName() {
            return pressName;
        }

        public void setPressName(String pressName) {
            this.pressName = pressName;
        }

        public String getPressType() {
            return pressType;
        }

        public void setPressType(String pressType) {
            this.pressType = pressType;
        }

        public WeeklyPressStatusEnum getWeeklyPressStatusEnum() {
            return weeklyPressStatusEnum;
        }

        public void setWeeklyPressStatusEnum(WeeklyPressStatusEnum weeklyPressStatusEnum) {
            this.weeklyPressStatusEnum = weeklyPressStatusEnum;
        }

        public Date getDateReady() {
            return dateReady;
        }

        public void setDateReady(Date dateReady) {
            this.dateReady = dateReady;
        }
    }
}