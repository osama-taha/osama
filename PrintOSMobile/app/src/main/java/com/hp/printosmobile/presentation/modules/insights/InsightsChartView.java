package com.hp.printosmobile.presentation.modules.insights;

import android.app.DatePickerDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.os.Handler;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.hp.printosmobile.R;
import com.hp.printosmobile.utils.HPLocaleUtils;
import com.hp.printosmobile.utils.HPUIUtils;
import com.hp.printosmobilelib.ui.widgets.TypefaceManager;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * @author Anwar Asbah Create on 20/06/2017
 */
public class InsightsChartView extends FrameLayout {

    public static final int MONTH_HISTORY_LIMIT = 3;

    private static final int TOTAL_PERCENTAGE = 100;
    private static final long HIGH_LIGHT_DELAY = 200;
    private static final int DEGREE_360_SPIN_DURATION = 1000;
    private static final int DEGREE_360 = 360;
    private static final int DEGREE_270 = 270;
    private static final int DEGREE_180 = 180;
    private static final int DEGREE_90 = 90;
    private static final String LABEL_WITH_OCCURRENCE_FORMAT = "%1$s%% %2$s (%3$s)";
    private static final String LABEL_WITHOUT_OCCURRENCE_FORMAT = "%1$s%% %2$s";

    private InsightsViewModel insightsViewModel;
    private List<InsightsViewModel.InsightModel> insightModelsWithOthers;
    private InsightChartBuilder insightChartBuilder;
    private InsightsChartViewCallback callback;
    private Date dateFrom;
    private Date dateTo;

    public InsightsChartView(Context context) {
        super(context);
        setupView();
    }

    public InsightsChartView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setupView();
    }

    public InsightsChartView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setupView();
    }

    private void setupView() {
        inflate(getContext(), R.layout.insights_chart_view, this);
        insightChartBuilder = new InsightChartBuilder(this);
    }

    public void attachCallback(InsightsChartViewCallback callback) {
        this.callback = callback;
    }

    public void setViewModel(InsightsViewModel insightsViewModel) {
        if (insightsViewModel == null || insightsViewModel.getInsightModels() == null) {
            return;
        }
        this.insightsViewModel = insightsViewModel;

        insightModelsWithOthers = new ArrayList<>();
        int percentageSum = 0;
        for (int i = 0; i < insightsViewModel.getInsightModels().size(); i++) {
            InsightsViewModel.InsightModel model = insightsViewModel.getInsightModels().get(i);
            if (model != null) {
                insightModelsWithOthers.add(model);
                percentageSum += model.getPercentage();
            }
        }

        if (percentageSum < TOTAL_PERCENTAGE && percentageSum != 0) {
            InsightsViewModel.InsightModel othersModel = new InsightsViewModel.InsightModel();
            othersModel.setPercentage(TOTAL_PERCENTAGE - percentageSum);
            othersModel.setLabel(getContext().getString(R.string.insights_others_label));
            othersModel.setOccurrences(-1);
            othersModel.setColor(ResourcesCompat.getColor(getResources(), R.color.c1b, null));
            othersModel.setInsightCursorDrawableID(R.drawable.cursor_gray);
            insightModelsWithOthers.add(othersModel);
        }

        insightChartBuilder.update();
    }

    public void onRefresh() {
        if (insightChartBuilder != null) {
            insightChartBuilder.resetDatePicker();
        }
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void resetDateFilter() {
        if(insightChartBuilder != null) {
            insightChartBuilder.resetDateFilter();
        }
    }

    public class InsightChartBuilder implements OnChartValueSelectedListener, RadioGroup.OnCheckedChangeListener {

        @Bind(R.id.chart)
        PieChart pieChart;
        @Bind(R.id.chart_layout)
        View layoutChart;
        @Bind(R.id.labels_list)
        RecyclerView labelsList;
        @Bind(R.id.date_label_text_view)
        TextView dateLabelTextView;
        @Bind(R.id.date_segmented_control)
        RadioGroup dateSegmentedControl;
        @Bind(R.id.panel_label)
        TextView panelLabelTextView;
        @Bind(R.id.kpi_image_view)
        ImageView kpiImageView;
        @Bind(R.id.cursor_image)
        ImageView cursorImage;

        InsightLabelsAdapter insightLabelsAdapter;
        Date tempDateFrom;

        public InsightChartBuilder(View view) {
            ButterKnife.bind(this, view);
            dateSegmentedControl.setOnCheckedChangeListener(this);

            Calendar monthsAgo = Calendar.getInstance();
            monthsAgo.add(Calendar.MONTH, -1 * MONTH_HISTORY_LIMIT);
            dateFrom = monthsAgo.getTime();
            dateTo = Calendar.getInstance().getTime();
        }

        private void resetDateFilter () {
            Calendar fromCalendar = Calendar.getInstance();
            fromCalendar.add(Calendar.MONTH, -1 * MONTH_HISTORY_LIMIT);
            dateFrom = fromCalendar.getTime();
            dateTo = Calendar.getInstance().getTime();

            dateSegmentedControl.setOnCheckedChangeListener(null);
            dateSegmentedControl.check(R.id.button_3_months);
            dateSegmentedControl.setOnCheckedChangeListener(this);
        }

        public void update() {
            String pressExpression = insightsViewModel.getSelectedDeviceCount() == insightsViewModel.getTotalDeviceCount() ?
                    String.valueOf(insightsViewModel.getSelectedDeviceCount()) :
                    (insightsViewModel.getSelectedDeviceCount() + "/" + insightsViewModel.getTotalDeviceCount());

            panelLabelTextView.setText(getContext().getString(insightsViewModel.getInsightKpiEnum().titleStringID, pressExpression));
            kpiImageView.setImageDrawable(ResourcesCompat.getDrawable(getResources(), insightsViewModel.getInsightKpiEnum().getDrawableID(), null));

            updateDateLabel();

            if (insightsViewModel.getInsightModels().size() > 0) {
                pieChart.setData(getDataSet());
                pieChart.setDescription("");
                pieChart.setDrawSliceText(false);
                pieChart.getLegend().setEnabled(false);
                pieChart.setRotationEnabled(false);
                pieChart.setOnChartValueSelectedListener(this);

                insightLabelsAdapter = new InsightLabelsAdapter(insightModelsWithOthers);
                RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
                labelsList.setLayoutManager(layoutManager);
                labelsList.setAdapter(insightLabelsAdapter);
                labelsList.setHasFixedSize(true);
                labelsList.setNestedScrollingEnabled(false);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Highlight h = new Highlight(0, 0);
                        pieChart.highlightValues(new Highlight[]{h});
                        spinPie(0);
                        insightLabelsAdapter.setSelectedIndex(0);
                    }
                }, HIGH_LIGHT_DELAY);
            }

            layoutChart.setVisibility(insightsViewModel.getInsightModels().size() > 0 ? VISIBLE : INVISIBLE);
        }

        public void resetDatePicker() {
            dateSegmentedControl.check(R.id.button_3_months);
        }

        private PieData getDataSet() {
            List<InsightsViewModel.InsightModel> modelList = insightModelsWithOthers;

            ArrayList entries = new ArrayList<>();
            ArrayList labels = new ArrayList();
            int[] colors = new int[modelList.size() == 1 ? 2 : modelList.size()];

            for (int i = 0; i < modelList.size(); i++) {
                InsightsViewModel.InsightModel insightModel = modelList.get(i);
                entries.add(new Entry(insightModel.getPercentage(), i));
                labels.add("");
                colors[i] = insightModel.getColor();
            }

            PieDataSet dataSet = new PieDataSet(entries, "");
            dataSet.setSliceSpace(1f);
            dataSet.setColors(colors);
            PieData data = new PieData(labels, dataSet);
            data.setDrawValues(false);

            return data;
        }

        @Override
        public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {
            spinPie(e.getXIndex());
        }

        private void spinPie(int index) {

            float start = pieChart.getRotationAngle(); //get current angle
            start = start >= DEGREE_180 ? start - DEGREE_360 : start;
            float offset = pieChart.getDrawAngles()[index] / 2; //calculate center of slice
            float offsetAngle = (pieChart.getAbsoluteAngles()[index] - offset);

            float end;
            if (offsetAngle <= DEGREE_180) {
                end = DEGREE_90 - offsetAngle;
            } else {
                offsetAngle = offsetAngle - DEGREE_360;
                end = -1 * (offsetAngle + DEGREE_270);
            }
            int animationDuration = (int) (Math.abs(end - start) / DEGREE_360 * DEGREE_360_SPIN_DURATION);

            pieChart.spin(animationDuration, start, end, Easing.EasingOption.EaseInOutQuad);//rotate to slice center
            pieChart.highlightValue(index, 0);
            insightLabelsAdapter.setSelectedIndex(index);

            InsightsViewModel.InsightModel insightModel = insightModelsWithOthers.get(index);
            cursorImage.setImageDrawable(ResourcesCompat.getDrawable(getResources(),
                    insightModel.getInsightCursorDrawableID(), null));
        }

        @Override
        public void onNothingSelected() {
            if(insightLabelsAdapter != null && insightLabelsAdapter.selectedIndex >= 0) {
                spinPie(insightLabelsAdapter.selectedIndex);
            }
        }

        @OnClick(R.id.button_date_picker)
        public void onDatePickerClicked() {
            Calendar minDate = Calendar.getInstance();
            minDate.add(Calendar.MONTH, -1 * MONTH_HISTORY_LIMIT);
            Calendar maxDate = Calendar.getInstance();

            showDatePicker(minDate, maxDate, true);
        }

        @OnClick(R.id.share_button)
        public void onShareClicked() {
            if (callback != null) {
                callback.onShareClicked();
            }
        }

        public void showDatePicker(Calendar from, final Calendar to, final boolean isFrom) {
            try {
                Calendar defaultSelected = Calendar.getInstance();
                defaultSelected.setTime(isFrom ? dateFrom : dateTo);

                final DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(), R.style.date_picker_theme, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int dayOfMonth) {
                        Calendar selectedDate = Calendar.getInstance();
                        selectedDate.set(year, month, dayOfMonth);
                        if (isFrom) {
                            tempDateFrom = selectedDate.getTime();
                            showDatePicker(selectedDate, to, false);
                            return;
                        }

                        dateFrom = tempDateFrom;
                        dateTo = selectedDate.getTime();
                        onDateSelected();
                    }
                }, defaultSelected.get(Calendar.YEAR), defaultSelected.get(Calendar.MONTH), defaultSelected.get(Calendar.DATE));

                final DatePicker datePicker = datePickerDialog.getDatePicker();

                //to fix android 4.0 crash
                from.set(Calendar.HOUR, 0);
                to.set(Calendar.HOUR, 12);

                datePicker.setMinDate(from.getTime().getTime());
                datePicker.setMaxDate(to.getTime().getTime());
                datePickerDialog.setTitle(isFrom ? R.string.insights_date_from : R.string.insights_date_to);
                datePickerDialog.setCancelable(false);
                datePickerDialog.show();

                if (!isFrom) {
                    Button button = datePickerDialog.getButton(DatePickerDialog.BUTTON_NEUTRAL);
                    button.setText(getContext().getString(R.string.today));
                    button.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dateFrom = tempDateFrom;
                            dateTo = Calendar.getInstance().getTime();
                            onDateSelected();
                            datePickerDialog.dismiss();
                        }
                    });
                    button.setVisibility(View.VISIBLE);
                    button.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.rounded_blue_bg, null));
                    button.setTextColor(Color.WHITE);
                }
            } catch (Exception e) {
            }
        }

        private void updateDateLabel() {
            dateLabelTextView.setText(HPLocaleUtils.getDateRangeString(getContext(), dateFrom, dateTo));
        }

        @Override
        public void onCheckedChanged(RadioGroup radioGroup, int checkedID) {

            dateTo = Calendar.getInstance().getTime();
            Calendar minDate;
            switch (checkedID) {
                case R.id.button_3_months:
                    minDate = Calendar.getInstance();
                    minDate.add(Calendar.MONTH, -1 * MONTH_HISTORY_LIMIT);
                    dateFrom = minDate.getTime();
                    onDateSelected();
                    break;
                case R.id.button_month:
                    minDate = Calendar.getInstance();
                    minDate.add(Calendar.MONTH, -1);
                    dateFrom = minDate.getTime();
                    onDateSelected();
                    break;
                case R.id.button_week:
                    minDate = Calendar.getInstance();
                    minDate.add(Calendar.WEEK_OF_MONTH, -1);
                    dateFrom = minDate.getTime();
                    onDateSelected();
                    break;
            }
        }

        void onDateSelected() {
            updateDateLabel();
            if (callback != null) {
                callback.onDateSelected(dateFrom, dateTo);
            }
        }
    }

    public class InsightLabelsAdapter extends RecyclerView.Adapter<InsightLabelsAdapter.InsightViewHolder> {

        private List<InsightsViewModel.InsightModel> models;
        private int selectedIndex;
        private final float[] CORNER_RADII = new float[]{100, 100, 100, 100, 100, 100, 100, 100};

        public InsightLabelsAdapter(List<InsightsViewModel.InsightModel> models) {
            this.models = models;
            this.selectedIndex = 0;
        }

        @Override
        public InsightViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(getContext());

            return new InsightViewHolder(inflater.inflate(R.layout.insights_label_view, parent, false));
        }

        private void setSelectedIndex(int selectedIndex) {
            this.selectedIndex = selectedIndex;
            notifyDataSetChanged();
        }

        @Override
        public void onBindViewHolder(InsightViewHolder holder, int position) {
            InsightsViewModel.InsightModel model = models.get(position);

            holder.index = position;

            if (model.getOccurrences() > 0) {
                holder.labelText.setText(String.format(LABEL_WITH_OCCURRENCE_FORMAT,
                        HPLocaleUtils.getLocalizedValue(model.getPercentage()),
                        model.getLabel(),
                        HPLocaleUtils.getLocalizedValue(model.getOccurrences())));
            } else {
                holder.labelText.setText(String.format(LABEL_WITHOUT_OCCURRENCE_FORMAT,
                        HPLocaleUtils.getLocalizedValue(model.getPercentage()),
                        model.getLabel()));
            }

            holder.labelText.setTypeface(TypefaceManager.getTypeface(getContext(),
                    position == selectedIndex ? TypefaceManager.HPTypeface.HP_BOLD
                            : TypefaceManager.HPTypeface.HP_REGULAR));

            holder.colorIndicator.setImageDrawable(getLegendColorDrawable(model.getColor()));
        }

        private Drawable getLegendColorDrawable(int color) {
            GradientDrawable shape = new GradientDrawable();
            shape.setShape(GradientDrawable.RECTANGLE);
            shape.setCornerRadii(CORNER_RADII);
            shape.setColor(color);
            return shape;
        }

        @Override
        public int getItemCount() {
            return models == null ? 0 : models.size();
        }

        public class InsightViewHolder extends RecyclerView.ViewHolder {

            @Bind(R.id.label_text)
            TextView labelText;
            @Bind(R.id.color_indicator)
            ImageView colorIndicator;

            int index;

            public InsightViewHolder(View itemView) {
                super(itemView);

                ButterKnife.bind(this, itemView);

                itemView.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        insightChartBuilder.spinPie(index);
                    }
                });
            }
        }
    }

    public interface InsightsChartViewCallback {
        void onDateSelected(Date from, Date to);

        void onShareClicked();
    }
}
