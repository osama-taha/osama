package com.hp.printosmobile.presentation.modules.contacthp;

import android.view.View;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.hp.printosmobile.Analytics;
import com.hp.printosmobile.R;
import com.hp.printosmobile.presentation.modules.contacthp.shared.ContactHpContactThroughView;
import com.hp.printosmobile.presentation.modules.contacthp.shared.ContactHpTextField;
import com.hp.printosmobile.utils.HPUIUtils;

import butterknife.Bind;

/**
 * Created be Anwar Asbah 10/11/2016
 */
public class FeedbackActivity extends ContactHPBaseActivity implements CompoundButton.OnCheckedChangeListener {

    private static final String TAG = FeedbackActivity.class.getSimpleName();

    public enum FeedbackEnum {
        NEGATIVE("-1"),
        NEUTRAL("0"),
        POSITIVE("1");

        String descriptionValue;

        FeedbackEnum(String descriptionValue) {
            this.descriptionValue = descriptionValue;
        }

        public String getDescriptiveValue() {
            return descriptionValue;
        }
    }

    @Bind(R.id.subject_text_view)
    ContactHpTextField subjectTextView;
    @Bind(R.id.description_text_view)
    ContactHpTextField descriptionTextView;
    @Bind(R.id.contact_through_view)
    ContactHpContactThroughView contactThroughView;
    @Bind(R.id.positive_radio_button)
    RadioButton positiveRadioButton;
    @Bind(R.id.negative_radio_button)
    RadioButton negativeRadioButton;
    @Bind(R.id.neutral_radio_button)
    RadioButton neutralRadioButton;

    @Override
    protected int getLayoutResource() {
        return R.layout.contact_hp_feedback;
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

        if(isChecked) {
            switch (buttonView.getId()) {
                case R.id.neutral_radio_button:
                    negativeRadioButton.setChecked(false);
                    positiveRadioButton.setChecked(false);
                    break;
                case R.id.negative_radio_button:
                    positiveRadioButton.setChecked(false);
                    neutralRadioButton.setChecked(false);
                    break;
                case R.id.positive_radio_button:
                    negativeRadioButton.setChecked(false);
                    neutralRadioButton.setChecked(false);
                    break;
            }
        }
    }

    @Override
    protected void init() {
        positiveRadioButton.setOnCheckedChangeListener(this);
        negativeRadioButton.setOnCheckedChangeListener(this);
        neutralRadioButton.setOnCheckedChangeListener(this);

        descriptionTextView.setTextFieldRightDrawable(null);
    }

    @Override
    protected void hideSoftKeyboard() {
        HPUIUtils.hideSoftKeyboard(this,
                subjectTextView.getAutoCompleteTextView(),
                descriptionTextView.getAutoCompleteTextView());
    }

    @Override
    public int getTitleResourceID() {
        return R.string.hp_contact_Feedback_title;
    }

    @Override
    public boolean isFormFilled() {
        String subject = subjectTextView.getText();
        String description = descriptionTextView.getText();

        return displayWarningMessage(subject, R.string.hp_contact_fill_subject_warning)
                && displayWarningMessage(description, R.string.hp_contact_fill_description_warning);
    }

    @Override
    public void sendRequest() {
        ContactHpContactThroughView.ContactMethod contactMethod = contactThroughView.getContactOption();

        Analytics.sendEvent(Analytics.MOBILE_PRINT_BEAT_CATEGORY, Analytics.FEEDBACK_TO_HP_SEND_ACTION);

        presenter.sendFeedback(this,
                subjectTextView.getText(),
                descriptionTextView.getText(),
                null,
                contactMethod,
                contactMethod != ContactHpContactThroughView.ContactMethod.DO_NOT_CONTACT,
                getFeedback());
    }

    public FeedbackEnum getFeedback() {
        if (positiveRadioButton.isChecked()) {
            return FeedbackEnum.POSITIVE;
        } else if (neutralRadioButton.isChecked()) {
            return FeedbackEnum.NEUTRAL;
        } else {
            return FeedbackEnum.NEGATIVE;
        }
    }

    @Override
    public void setFormEnable(boolean isEnabled) {
        subjectTextView.setEnabled(isEnabled);
        descriptionTextView.setEnabled(isEnabled);
        contactThroughView.setEnabled(isEnabled);
        positiveRadioButton.setEnabled(isEnabled);
        negativeRadioButton.setEnabled(isEnabled);
        neutralRadioButton.setEnabled(isEnabled);
    }

    @Override
    public void setHasPhone(boolean hasPhone) {
        contactThroughView.setHasPhone(hasPhone);
    }
}
