package com.hp.printosmobile.data.remote.models;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.hp.printosmobile.data.remote.services.PersonalAdvisorService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A model representing the response of {@link PersonalAdvisorService#getAdvices(String, List)} endpoint.
 *
 * @author Anwar Asbah
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
public class AdviceData {

    @JsonProperty("adviceName")
    private String adviceName;
    @JsonProperty("adviceID")
    private Integer adviceID;
    @JsonProperty("adviceText")
    private String adviceText;
    @JsonProperty("adviceTitle")
    private String adviceTitle;
    @JsonProperty("suppressed")
    private Boolean suppressed;
    @JsonProperty("suppressionTime")
    private String suppressionTime;
    @JsonProperty("priority")
    private Integer priority;
    @JsonProperty("device")
    private String device;
    @JsonProperty("category")
    private String category;
    @JsonProperty("liked")
    private Boolean liked;
    @JsonProperty("iconName")
    private String iconName;
    @JsonProperty("isNew")
    private boolean isNew;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * @return The adviceName
     */
    @JsonProperty("adviceName")
    public String getAdviceName() {
        return adviceName;
    }

    /**
     * @param adviceName The Advice name
     */
    @JsonProperty("adviceName")
    public void setAdviceName(String adviceName) {
        this.adviceName = adviceName;
    }


    /**
     * @return The adviceID
     */
    @JsonProperty("adviceID")
    public Integer getAdviceID() {
        return adviceID;
    }

    /**
     * @param adviceID The adviceID
     */
    @JsonProperty("adviceID")
    public void setAdviceID(Integer adviceID) {
        this.adviceID = adviceID;
    }

    /**
     * @return The adviceTitle
     */
    @JsonProperty("adviceTitle")
    public String getAdviceTitle() {
        return adviceTitle;
    }

    /**
     * @param adviceTitle The Advice title
     */
    @JsonProperty("adviceTitle")
    public void setAdviceTitle(String adviceTitle) {
        this.adviceTitle = adviceTitle;
    }

    /**
     * @return The adviceText
     */
    @JsonProperty("adviceText")
    public String getAdviceText() {
        return adviceText;
    }

    /**
     * @param adviceText The adviceText
     */
    @JsonProperty("adviceText")
    public void setAdviceText(String adviceText) {
        this.adviceText = adviceText;
    }

    /**
     * @return The suppressed
     */
    @JsonProperty("suppressed")
    public Boolean getSuppressed() {
        return suppressed;
    }

    /**
     * @param suppressed The suppressed
     */
    @JsonProperty("suppressed")
    public void setSuppressed(Boolean suppressed) {
        this.suppressed = suppressed;
    }

    /**
     * @return The suppressionTime
     */
    @JsonProperty("suppressionTime")
    public String getSuppressionTime() {
        return suppressionTime;
    }

    /**
     * @param suppressionTime The suppressionTime
     */
    @JsonProperty("suppressionTime")
    public void setSuppressionTime(String suppressionTime) {
        this.suppressionTime = suppressionTime;
    }

    /**
     * @return The priority
     */
    @JsonProperty("priority")
    public Integer getPriority() {
        return priority;
    }

    /**
     * @param priority The priority
     */
    @JsonProperty("priority")
    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    /**
     * @return The device
     */
    @JsonProperty("device")
    public String getDevice() {
        return device;
    }

    /**
     * @param device The device
     */
    @JsonProperty("device")
    public void setDevice(String device) {
        this.device = device;
    }

    /**
     * @return The category
     */
    @JsonProperty("category")
    public String getCategory() {
        return category;
    }

    /**
     * @param category The category
     */
    @JsonProperty("category")
    public void setCategory(String category) {
        this.category = category;
    }

    /**
     * @return The liked
     */
    @JsonProperty("liked")
    public Boolean getLiked() {
        return liked;
    }

    /**
     * @param liked The liked
     */
    @JsonProperty("liked")
    public void setLiked(Boolean liked) {
        this.liked = liked;
    }

    /**
     * @return The iconName
     */
    @JsonProperty("iconName")
    public String getIconName() {
        return iconName;
    }

    /**
     * @param iconName The iconName
     */
    @JsonProperty("iconName")
    public void setIconName(String iconName) {
        this.iconName = iconName;
    }

    @JsonProperty("isNew")
    public boolean getIsNew() {
        return isNew;
    }

    @JsonProperty("isNew")
    public void setIsNew(boolean isNew) {
        this.isNew = isNew;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("AdviceData{");
        sb.append("adviceID=").append(adviceID);
        sb.append(", adviceText='").append(adviceText).append('\'');
        sb.append(", suppressed=").append(suppressed);
        sb.append(", suppressionTime=").append(suppressionTime);
        sb.append(", priority=").append(priority);
        sb.append(", device='").append(device).append('\'');
        sb.append(", category=").append(category);
        sb.append(", liked=").append(liked);
        sb.append(", iconName='").append(iconName).append('\'');
        sb.append(", additionalProperties=").append(additionalProperties);
        sb.append('}');
        return sb.toString();
    }
}
