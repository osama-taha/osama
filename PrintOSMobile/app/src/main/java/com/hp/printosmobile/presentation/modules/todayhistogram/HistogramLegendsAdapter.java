package com.hp.printosmobile.presentation.modules.todayhistogram;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hp.printosmobile.R;

import java.util.List;

/**
 * Created by Osama Taha on 10/12/16.
 */
public class HistogramLegendsAdapter extends RecyclerView.Adapter<HistogramLegendsAdapter.ViewHolder> {

    private final Context context;
    private final List<TodayHistogramViewModel.HistogramShiftLegend> mLegends;

    public HistogramLegendsAdapter(Context context, List<TodayHistogramViewModel.HistogramShiftLegend> shiftLegends) {
        this.context = context;
        this.mLegends = shiftLegends;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.histogram_legend_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.mLegendItem = mLegends.get(position);

        holder.mLegendColorView.setBackgroundResource(holder.mLegendItem.getLegendColor());
        holder.mLegendNameTextView.setText(holder.mLegendItem.getLegendName());

    }

    @Override
    public int getItemCount() {
        return mLegends == null ? 0 : mLegends.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        public final View mView;
        public final View mLegendColorView;
        public final TextView mLegendNameTextView;
        public TodayHistogramViewModel.HistogramShiftLegend mLegendItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mLegendColorView = view.findViewById(R.id.legend_color_view);
            mLegendNameTextView = (TextView) view.findViewById(R.id.legend_name_text_view);
        }

    }
}
