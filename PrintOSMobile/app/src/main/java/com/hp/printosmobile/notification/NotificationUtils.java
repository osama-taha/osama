package com.hp.printosmobile.notification;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;

import com.hp.printosmobile.Constants;
import com.hp.printosmobile.data.local.PrintOSPreferences;
import com.hp.printosmobile.presentation.modules.main.MainActivity;
import com.hp.printosmobile.presentation.modules.settings.SubscriptionEvent;

/**
 * Created by anwar asbah on 2/1/2017.
 */

public class NotificationUtils {
    private static final String TAG = NotificationUtils.class.getSimpleName();
    private static final String ENTITY_SPLIT_REGEX = "/";

    public static final int ERROR_INT_EXTRA = -1;

    public static Intent getNotificationIntent(Context context, String entity, String notificationKey,
                                               int notificationID, int userID, String link, boolean isPushNotification) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        Bundle bundle = new Bundle();
        if (entity != null) {
            String[] entities = entity.split(ENTITY_SPLIT_REGEX);

            for (int i = 0; i < entities.length; i++) {
                if (i > 3 && !PrintOSPreferences.getInstance(context).isBeatCoinFeatureEnabled()) {
                    continue;
                }
                bundle.putString(i == 0 ? Constants.IntentExtras.MAIN_ACTIVITY_ORGANIZATION_EXTRA
                        : i == 1 ? Constants.IntentExtras.MAIN_ACTIVITY_DIVISION_EXTRA
                        : i == 2 ? Constants.IntentExtras.MAIN_ACTIVITY_FILTER_EXTRA
                        : i == 3 ? Constants.IntentExtras.MAIN_ACTIVITY_TODAY_EXTRA
                        : Constants.IntentExtras.MAIN_ACTIVITY_BEAT_COIN_EXTRA, entities[i]);
            }
        }

        SubscriptionEvent.NotificationEventEnum eventEnum = SubscriptionEvent.NotificationEventEnum
                .from(notificationKey);

        if (eventEnum == SubscriptionEvent.NotificationEventEnum.BOX_NEW_FOLDER) {
            //The entity id of the box-new folder doesn't contain the org id.
            bundle.remove(Constants.IntentExtras.MAIN_ACTIVITY_ORGANIZATION_EXTRA);
        }

        if (notificationKey != null) {
            bundle.putString(Constants.IntentExtras.MAIN_ACTIVITY_NOTIFICATION_EXTRA, notificationKey);
        }

        if (notificationID != ERROR_INT_EXTRA && userID != ERROR_INT_EXTRA) {
            bundle.putInt(Constants.IntentExtras.MAIN_ACTIVITY_NOTIFICATION_ID_EXTRA, notificationID);
            bundle.putInt(Constants.IntentExtras.MAIN_ACTIVITY_USER_ID_EXTRA, userID);
        }

        if (!TextUtils.isEmpty(link)) {
            bundle.putString(Constants.IntentExtras.MAIN_ACTIVITY_LINK_EXTRA, link);
        }

        bundle.putBoolean(Constants.IntentExtras.MAIN_ACTIVITY_IS_PUSH_NOTIFICATION, isPushNotification);

        intent.putExtras(bundle);

        return intent;
    }

    public static int getUniqueRequestCode() {
        return (int) (System.currentTimeMillis() % Integer.MAX_VALUE);
    }
}
