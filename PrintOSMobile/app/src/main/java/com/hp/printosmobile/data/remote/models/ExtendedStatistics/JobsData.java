
package com.hp.printosmobile.data.remote.models.ExtendedStatistics;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class JobsData {

    @JsonProperty("future")
    private List<FutureJob> future = new ArrayList<FutureJob>();
    @JsonProperty("printing_jobs")
    private List<FutureJob> printingJobs = new ArrayList<FutureJob>();
    @JsonProperty("history")
    private List<HistoryJob> history = new ArrayList<HistoryJob>();
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * @return The future
     */
    @JsonProperty("future")
    public List<FutureJob> getFuture() {
        return future;
    }

    /**
     * @param future The future
     */
    @JsonProperty("future")
    public void setFuture(List<FutureJob> future) {
        this.future = future;
    }

    /**
     * @return The printingJobs
     */
    @JsonProperty("printing_jobs")
    public List<FutureJob> getPrintingJobs() {
        return printingJobs;
    }

    /**
     * @param printingJobs The printing_jobs
     */
    @JsonProperty("printing_jobs")
    public void setPrintingJobs(List<FutureJob> printingJobs) {
        this.printingJobs = printingJobs;
    }

    /**
     * @return The history
     */
    @JsonProperty("history")
    public List<HistoryJob> getHistory() {
        return history;
    }

    /**
     * @param history The history
     */
    @JsonProperty("history")
    public void setHistory(List<HistoryJob> history) {
        this.history = history;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class FutureJob {

        @JsonProperty("job_state")
        private String jobState;
        @JsonProperty("file_name")
        private String fileName;
        @JsonProperty("print_mode")
        private String printMode;
        @JsonProperty("uuid")
        private String uuid;
        @JsonProperty("print_queue")
        private PrintQueue printQueue;
        @JsonProperty("thumbnail_url")
        private String thumbnailUrl;
        @JsonProperty("progress")
        private float progress;
        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<String, Object>();

        /**
         * @return The jobState
         */
        @JsonProperty("job_state")
        public String getJobState() {
            return jobState;
        }

        /**
         * @param jobState The job_state
         */
        @JsonProperty("job_state")
        public void setJobState(String jobState) {
            this.jobState = jobState;
        }

        /**
         * @return The fileName
         */
        @JsonProperty("file_name")
        public String getFileName() {
            return fileName;
        }

        /**
         * @param fileName The file_name
         */
        @JsonProperty("file_name")
        public void setFileName(String fileName) {
            this.fileName = fileName;
        }

        /**
         * @return The printMode
         */
        @JsonProperty("print_mode")
        public String getPrintMode() {
            return printMode;
        }

        /**
         * @param printMode The print_mode
         */
        @JsonProperty("print_mode")
        public void setPrintMode(String printMode) {
            this.printMode = printMode;
        }

        /**
         * @return The uuid
         */
        @JsonProperty("uuid")
        public String getUuid() {
            return uuid;
        }

        /**
         * @param uuid The uuid
         */
        @JsonProperty("uuid")
        public void setUuid(String uuid) {
            this.uuid = uuid;
        }

        /**
         * @return The printQueue
         */
        @JsonProperty("print_queue")
        public PrintQueue getPrintQueue() {
            return printQueue;
        }

        /**
         * @param printQueue The print_queue
         */
        @JsonProperty("print_queue")
        public void setPrintQueue(PrintQueue printQueue) {
            this.printQueue = printQueue;
        }

        /**
         * @return The thumbnailUrl
         */
        @JsonProperty("thumbnail_url")
        public String getThumbnailUrl() {
            return thumbnailUrl;
        }

        /**
         * @param thumbnailUrl The thumbnail_url
         */
        @JsonProperty("thumbnail_url")
        public void setThumbnailUrl(String thumbnailUrl) {
            this.thumbnailUrl = thumbnailUrl;
        }

        @JsonProperty("progress")
        public void setProgress(float progress) {
            this.progress = progress;
        }

        @JsonProperty("progress")
        public float getProgress() {
            return progress;
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }

    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class HistoryJob {

        @JsonProperty("file_name")
        private String fileName;
        @JsonProperty("end_state")
        private String endState;
        @JsonProperty("creation_date_time")
        private Object creationDateTime;
        @JsonProperty("start_time")
        private String startTime;
        @JsonProperty("completion_time")
        private String completionTime;
        @JsonProperty("printing_time")
        private String printingTime;
        @JsonProperty("print_mode")
        private String printMode;
        @JsonProperty("copies")
        private String copies;
        @JsonProperty("pages")
        private Object pages;
        @JsonProperty("sequence")
        private String sequence;
        @JsonProperty("uuid")
        private String uuid;
        @JsonProperty("ink")
        private Ink ink;
        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<String, Object>();

        /**
         * @return The fileName
         */
        @JsonProperty("file_name")
        public String getFileName() {
            return fileName;
        }

        /**
         * @param fileName The file_name
         */
        @JsonProperty("file_name")
        public void setFileName(String fileName) {
            this.fileName = fileName;
        }

        /**
         * @return The endState
         */
        @JsonProperty("end_state")
        public String getEndState() {
            return endState;
        }

        /**
         * @param endState The end_state
         */
        @JsonProperty("end_state")
        public void setEndState(String endState) {
            this.endState = endState;
        }

        /**
         * @return The creationDateTime
         */
        @JsonProperty("creation_date_time")
        public Object getCreationDateTime() {
            return creationDateTime;
        }

        /**
         * @param creationDateTime The creation_date_time
         */
        @JsonProperty("creation_date_time")
        public void setCreationDateTime(Object creationDateTime) {
            this.creationDateTime = creationDateTime;
        }

        /**
         * @return The startTime
         */
        @JsonProperty("start_time")
        public String getStartTime() {
            return startTime;
        }

        /**
         * @param startTime The start_time
         */
        @JsonProperty("start_time")
        public void setStartTime(String startTime) {
            this.startTime = startTime;
        }

        /**
         * @return The completionTime
         */
        @JsonProperty("completion_time")
        public String getCompletionTime() {
            return completionTime;
        }

        /**
         * @param completionTime The completion_time
         */
        @JsonProperty("completion_time")
        public void setCompletionTime(String completionTime) {
            this.completionTime = completionTime;
        }

        /**
         * @return The printingTime
         */
        @JsonProperty("printing_time")
        public String getPrintingTime() {
            return printingTime;
        }

        /**
         * @param printingTime The printing_time
         */
        @JsonProperty("printing_time")
        public void setPrintingTime(String printingTime) {
            this.printingTime = printingTime;
        }

        /**
         * @return The printMode
         */
        @JsonProperty("print_mode")
        public String getPrintMode() {
            return printMode;
        }

        /**
         * @param printMode The print_mode
         */
        @JsonProperty("print_mode")
        public void setPrintMode(String printMode) {
            this.printMode = printMode;
        }

        /**
         * @return The copies
         */
        @JsonProperty("copies")
        public String getCopies() {
            return copies;
        }

        /**
         * @param copies The copies
         */
        @JsonProperty("copies")
        public void setCopies(String copies) {
            this.copies = copies;
        }

        /**
         * @return The pages
         */
        @JsonProperty("pages")
        public Object getPages() {
            return pages;
        }

        /**
         * @param pages The pages
         */
        @JsonProperty("pages")
        public void setPages(Object pages) {
            this.pages = pages;
        }

        /**
         * @return The sequence
         */
        @JsonProperty("sequence")
        public String getSequence() {
            return sequence;
        }

        /**
         * @param sequence The sequence
         */
        @JsonProperty("sequence")
        public void setSequence(String sequence) {
            this.sequence = sequence;
        }

        /**
         * @return The uuid
         */
        @JsonProperty("uuid")
        public String getUuid() {
            return uuid;
        }

        /**
         * @param uuid The uuid
         */
        @JsonProperty("uuid")
        public void setUuid(String uuid) {
            this.uuid = uuid;
        }

        /**
         * @return The ink
         */
        @JsonProperty("ink")
        public Ink getInk() {
            return ink;
        }

        /**
         * @param ink The ink
         */
        @JsonProperty("ink")
        public void setInk(Ink ink) {
            this.ink = ink;
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }

        @JsonInclude(JsonInclude.Include.NON_NULL)
        public static class Ink {

            @JsonProperty("inks_info")
            private InksInfo inksInfo;
            @JsonIgnore
            private Map<String, Object> additionalProperties = new HashMap<String, Object>();

            /**
             * @return The inksInfo
             */
            @JsonProperty("inks_info")
            public InksInfo getInksInfo() {
                return inksInfo;
            }

            /**
             * @param inksInfo The inks_info
             */
            @JsonProperty("inks_info")
            public void setInksInfo(InksInfo inksInfo) {
                this.inksInfo = inksInfo;
            }

            @JsonAnyGetter
            public Map<String, Object> getAdditionalProperties() {
                return this.additionalProperties;
            }

            @JsonAnySetter
            public void setAdditionalProperty(String name, Object value) {
                this.additionalProperties.put(name, value);
            }

            @JsonInclude(JsonInclude.Include.NON_NULL)
            public static class InksInfo {

                @JsonProperty("K")
                private Integer k;
                @JsonProperty("C")
                private Integer c;
                @JsonProperty("M")
                private Integer m;
                @JsonProperty("Y")
                private Integer y;
                @JsonProperty("LC")
                private Integer lC;
                @JsonProperty("LM")
                private Integer lM;
                @JsonProperty("OP")
                private Integer oP;
                @JsonIgnore
                private Map<String, Object> additionalProperties = new HashMap<String, Object>();

                /**
                 * @return The k
                 */
                @JsonProperty("K")
                public Integer getK() {
                    return k;
                }

                /**
                 * @param k The K
                 */
                @JsonProperty("K")
                public void setK(Integer k) {
                    this.k = k;
                }

                /**
                 * @return The c
                 */
                @JsonProperty("C")
                public Integer getC() {
                    return c;
                }

                /**
                 * @param c The C
                 */
                @JsonProperty("C")
                public void setC(Integer c) {
                    this.c = c;
                }

                /**
                 * @return The m
                 */
                @JsonProperty("M")
                public Integer getM() {
                    return m;
                }

                /**
                 * @param m The M
                 */
                @JsonProperty("M")
                public void setM(Integer m) {
                    this.m = m;
                }

                /**
                 * @return The y
                 */
                @JsonProperty("Y")
                public Integer getY() {
                    return y;
                }

                /**
                 * @param y The Y
                 */
                @JsonProperty("Y")
                public void setY(Integer y) {
                    this.y = y;
                }

                /**
                 * @return The lC
                 */
                @JsonProperty("LC")
                public Integer getLC() {
                    return lC;
                }

                /**
                 * @param lC The LC
                 */
                @JsonProperty("LC")
                public void setLC(Integer lC) {
                    this.lC = lC;
                }

                /**
                 * @return The lM
                 */
                @JsonProperty("LM")
                public Integer getLM() {
                    return lM;
                }

                /**
                 * @param lM The LM
                 */
                @JsonProperty("LM")
                public void setLM(Integer lM) {
                    this.lM = lM;
                }

                /**
                 * @return The oP
                 */
                @JsonProperty("OP")
                public Integer getOP() {
                    return oP;
                }

                /**
                 * @param oP The OP
                 */
                @JsonProperty("OP")
                public void setOP(Integer oP) {
                    this.oP = oP;
                }

                @JsonAnyGetter
                public Map<String, Object> getAdditionalProperties() {
                    return this.additionalProperties;
                }

                @JsonAnySetter
                public void setAdditionalProperty(String name, Object value) {
                    this.additionalProperties.put(name, value);
                }

            }
        }
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class PrintQueue {

        @JsonProperty("id")
        private String id;
        @JsonProperty("name")
        private String name;
        @JsonProperty("job_order")
        private String jobOrder;
        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<String, Object>();

        /**
         * @return The id
         */
        @JsonProperty("id")
        public String getId() {
            return id;
        }

        /**
         * @param id The id
         */
        @JsonProperty("id")
        public void setId(String id) {
            this.id = id;
        }

        /**
         * @return The name
         */
        @JsonProperty("name")
        public String getName() {
            return name;
        }

        /**
         * @param name The name
         */
        @JsonProperty("name")
        public void setName(String name) {
            this.name = name;
        }

        /**
         * @return The jobOrder
         */
        @JsonProperty("job_order")
        public String getJobOrder() {
            return jobOrder;
        }

        /**
         * @param jobOrder The job_order
         */
        @JsonProperty("job_order")
        public void setJobOrder(String jobOrder) {
            this.jobOrder = jobOrder;
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }

    }
}
