package com.hp.printosmobile.presentation.modules.personaladvisor;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v4.view.PagerAdapter;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.TextView;

import com.hp.printosmobile.R;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Anwar Asbah on 4/25/2016.
 */
public class PersonalAdvisorAdapter extends PagerAdapter {

    public final static String TAG = PersonalAdvisorAdapter.class.getSimpleName();
    private static final int MIN_NUMBER_OF_LINES = 6;

    private Context context;
    private PersonalAdvisorViewModel viewModel;
    private AdviseViewBuilder viewBuilder;
    private PersonalAdviseCallback callback;
    private Map<Integer, View> viewsList;

    public PersonalAdvisorAdapter(Context context, PersonalAdvisorViewModel viewModel,
                                  PersonalAdviseCallback callback, boolean isPanel) {
        this.context = context;
        this.viewModel = viewModel;
        this.viewBuilder = isPanel ? new AdvisePanelViewBuilder() : new AdviserDialogViewBuilder();
        this.callback = callback;
        this.viewsList = new HashMap<>();
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        if (viewsList.containsKey(position)) {
            return viewsList.get(position);
        }

        LayoutInflater inflater = LayoutInflater.from(context);
        View layout = inflater.inflate(viewBuilder.getLayout(), container, false);
        viewBuilder.buildView(layout, viewModel.getUnsuppressedAdvices().get(position), position);
        container.addView(layout);

        viewsList.put(position, layout);

        layout.setTag(position);
        return layout;

    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        viewsList.remove(position);
        container.removeView((View) object);
    }


    @Override
    public int getCount() {
        if (viewModel != null && viewModel.getUnsuppressedAdvices() != null) {
            return viewModel.getUnsuppressedAdvices().size();
        }
        return 0;
    }

    public String getAdviceTitle(int position) {

        if (viewModel != null) {
            List<AdviceViewModel> advices = viewModel.getUnsuppressedAdvices();
            if (advices != null && advices.size() > position) {
                AdviceViewModel adviceViewModel = advices.get(position);
                return adviceViewModel.getTitle();
            }
        }

        return "";
    }

    public Drawable getAdviceIcon(int position) {

        if (viewModel != null) {

            List<AdviceViewModel> advices = viewModel.getUnsuppressedAdvices();
            if (advices != null && advices.size() > position) {
                AdviceViewModel adviceViewModel = advices.get(position);
                if (adviceViewModel.getAdviceType() != AdviceType.EMPTY) {
                    return ResourcesCompat.getDrawable(context.getResources(), adviceViewModel.getAdviceType().getAdviceIcon(), null);
                } else {
                    return ResourcesCompat.getDrawable(context.getResources(), R.drawable.personal_advisor, null);
                }
            }
        }

        return null;
    }

    public boolean isLiked(int position) {
        if (viewModel != null) {
            List<AdviceViewModel> advices = viewModel.getUnsuppressedAdvices();
            if (advices != null && position < advices.size()) {
                AdviceViewModel adviceViewModel = advices.get(position);
                return adviceViewModel.getLiked();
            }
        }
        return false;
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    public AdviceViewModel getAdvice(int currentAdvicePosition) {
        if (viewModel != null && currentAdvicePosition >= 0) {
            List<AdviceViewModel> adviceViewModels = viewModel.getUnsuppressedAdvices();
            if (adviceViewModels != null && currentAdvicePosition < adviceViewModels.size()) {
                return viewModel.getUnsuppressedAdvices().get(currentAdvicePosition);
            }
        }
        return null;
    }

    public void setViewModel(PersonalAdvisorViewModel viewModel) {
        this.viewModel = viewModel;
    }

    abstract class AdviseViewBuilder {
        abstract void buildView(View view, final AdviceViewModel viewModel, int position);

        abstract int getLayout();
    }

    class AdviserDialogViewBuilder extends AdviseViewBuilder {

        @Bind(R.id.text_advice_content)
        TextView contentTextView;

        @Override
        void buildView(View view, AdviceViewModel viewModel, int position) {
            ButterKnife.bind(this, view);
            setAdviceContentText(viewModel, contentTextView);
        }

        @Override
        int getLayout() {
            return R.layout.dialog_advices_item;
        }
    }

    class AdvisePanelViewBuilder extends AdviseViewBuilder {

        @Bind(R.id.image_advice_background)
        ImageView adviceImageView;
        @Bind(R.id.image_advice_icon)
        ImageView iconImageView;
        @Bind(R.id.text_advice_title)
        TextView titleTextView;
        @Bind(R.id.text_advice_device)
        TextView deviceNameTextView;
        @Bind(R.id.text_advice_content)
        TextView contentTextView;
        @Bind(R.id.read_more_button)
        View readMoreButton;

        @Override
        void buildView(final View view, final AdviceViewModel viewModel, int position) {

            ButterKnife.bind(this, view);
            if (viewModel.getAdviceType() != AdviceType.EMPTY) {
                iconImageView.setImageDrawable(ResourcesCompat.getDrawable(context.getResources(), viewModel.getAdviceType().getAdviceIcon(), null));
            }

            titleTextView.setText(viewModel.getTitle());

            setAdviceContentText(viewModel, contentTextView);

            deviceNameTextView.setText(viewModel.getDeviceName());

            Picasso.with(context).load(viewModel.getImageResourceId()).into(adviceImageView);

            contentTextView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {

                    TextView contextTextView = ((TextView) view.findViewById(R.id.text_advice_content));
                    View readMoreView = view.findViewById(R.id.read_more_button);

                    if (contextTextView.getLineCount() > MIN_NUMBER_OF_LINES && !viewModel.getIsReadMoreClicked()) {
                        readMoreView.setVisibility(View.VISIBLE);
                        contextTextView.setMaxLines(MIN_NUMBER_OF_LINES - 1);
                    } else {
                        readMoreView.setVisibility(View.GONE);
                        contextTextView.setMaxLines(Integer.MAX_VALUE);
                    }
                }
            });

            readMoreButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    TextView contextTextView = ((TextView) view.findViewById(R.id.text_advice_content));
                    View readMoreView = view.findViewById(R.id.read_more_button);

                    viewModel.setIsReadMoreClicked(true);

                    contextTextView.setMaxLines(Integer.MAX_VALUE);
                    readMoreView.setVisibility(View.GONE);

                    if (callback != null) {
                        callback.onReadMoreClicked(view);
                    }
                }
            });
        }

        @Override
        int getLayout() {
            return R.layout.view_advice;
        }
    }

    private void setAdviceContentText(AdviceViewModel viewModel, TextView contentTextView) {

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            contentTextView.setText(Html.fromHtml(viewModel.getContent(), Html.FROM_HTML_MODE_LEGACY));
        } else {
            contentTextView.setText(Html.fromHtml(viewModel.getContent()));
        }
    }

    public interface PersonalAdviseCallback {

        void onSuppressClicked(AdviceViewModel adviceViewModel);

        void onLikeClicked(AdviceViewModel adviceViewModel);

        void onReadMoreClicked(View view);

    }
}
