package com.hp.printosmobile.data.remote.models;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Anwar Asbah 6/20/2017
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class InsightData {

    @JsonProperty("topOccurrencesChart")
    List<Insight> insightList;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("topOccurrencesChart")
    public List<Insight> getInsightList() {
        return insightList;
    }

    @JsonProperty("topOccurrencesChart")
    public void setInsightList(List<Insight> insightList) {
        this.insightList = insightList;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class Insight {

        @JsonProperty("eventName")
        String eventName;
        @JsonProperty("occurrences")
        int occurrences;
        @JsonProperty("percentage")
        int percentage;
        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<String, Object>();

        @JsonProperty("eventName")
        public String getEventName() {
            return eventName;
        }

        @JsonProperty("eventName")
        public void setEventName(String eventName) {
            this.eventName = eventName;
        }

        @JsonProperty("occurrences")
        public int getOccurrences() {
            return occurrences;
        }

        @JsonProperty("occurrences")
        public void setOccurrences(int occurrences) {
            this.occurrences = occurrences;
        }

        @JsonProperty("percentage")
        public int getPercentage() {
            return percentage;
        }

        @JsonProperty("percentage")
        public void setPercentage(int percentage) {
            this.percentage = percentage;
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }
    }
}