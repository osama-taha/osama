package com.hp.printosmobile.presentation.modules.insights;

import com.hp.printosmobile.R;

import java.util.Comparator;
import java.util.List;

/**
 * Created by Anwar Asbah on 6/14/2017.
 */

public class InsightsViewModel {

    public static Comparator<InsightModel> COMPARATOR = new Comparator<InsightModel>() {
        @Override
        public int compare(InsightModel lhs, InsightModel rhs) {
            if (lhs == null && rhs == null) return 0;
            if (lhs == null && rhs != null) return 1;
            if (lhs != null && rhs == null) return -1;
            return lhs.compareTo(rhs);
        }
    };

    public enum InsightKpiEnum {
        JAM("Jams", R.string.insights_top_jams, R.drawable.insights_jams),
        FAILURE("Failures", R.string.insights_top_failure, R.drawable.insights_failures);

        String TAG;
        int titleStringID;
        int drawableID;

        InsightKpiEnum(String tag, int titleStringID, int drawableID) {
            this.TAG = tag;
            this.titleStringID = titleStringID;
            this.drawableID = drawableID;
        }

        public String getTAG() {
            return TAG;
        }

        public void setTAG(String TAG) {
            this.TAG = TAG;
        }

        public int getDrawableID() {
            return drawableID;
        }

        public int getTitleStringID() {
            return titleStringID;
        }
    }

    InsightKpiEnum insightKpiEnum;
    List<InsightModel> insightModels;
    int totalDeviceCount;
    int selectedDeviceCount;

    public List<InsightModel> getInsightModels() {
        return insightModels;
    }

    public void setInsightModels(List<InsightModel> insightModels) {
        this.insightModels = insightModels;
    }

    public InsightKpiEnum getInsightKpiEnum() {
        return insightKpiEnum;
    }

    public void setInsightKpiEnum(InsightKpiEnum insightKpiEnum) {
        this.insightKpiEnum = insightKpiEnum;
    }

    public int getTotalDeviceCount() {
        return totalDeviceCount;
    }

    public void setTotalDeviceCount(int totalDeviceCount) {
        this.totalDeviceCount = totalDeviceCount;
    }

    public int getSelectedDeviceCount() {
        return selectedDeviceCount;
    }

    public void setSelectedDeviceCount(int selectedDeviceCount) {
        this.selectedDeviceCount = selectedDeviceCount;
    }

    public static class InsightModel implements Comparable<InsightModel> {
        private int percentage;
        private String label;
        private int occurrences;
        private int color;
        private int insightCursorDrawableID;

        public int getPercentage() {
            return percentage;
        }

        public void setPercentage(int percentage) {
            this.percentage = percentage;
        }

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }

        public int getColor() {
            return color;
        }

        public void setColor(int colorResourceId) {
            this.color = colorResourceId;
        }

        public int getOccurrences() {
            return occurrences;
        }

        public void setOccurrences(int occurrences) {
            this.occurrences = occurrences;
        }

        public int getInsightCursorDrawableID() {
            return insightCursorDrawableID;
        }

        public void setInsightCursorDrawableID(int insightCursorDrawableID) {
            this.insightCursorDrawableID = insightCursorDrawableID;
        }

        @Override
        public int compareTo(InsightModel insightModel) {
            if (insightModel == null) {
                return 1;
            }
            return insightModel.getPercentage() == getPercentage() ? 0 :
                    insightModel.getPercentage() < getPercentage() ? -1 : 1;
        }
    }
}
