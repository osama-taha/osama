package com.hp.printosmobilelib.ui.widgets;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.LinearInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.hp.printosmobilelib.ui.R;

/**
 * @author Anwar Asbah Create on 8/16/2016
 */
public class HPVerticalProgressBar extends FrameLayout implements ViewTreeObserver.OnGlobalLayoutListener {

    private static final long ANIMATION_DURATION = 500;
    private static final String ANIMATION_TYPE = "progress";
    private static final int ANIMATION_INITIAL_VALUE = 0;
    private TextView labelText;
    private TextView descriptionText;
    private ImageView statusImage;
    private TextView valueText;
    private HPProgressBar progressBar;
    private View border;
    private View targetBorder;
    private TriangleView pointerArrow;
    private float targetBorderHeightRatio;

    public HPVerticalProgressBar(Context context) {
        super(context);
        init();
    }

    public HPVerticalProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs, android.R.attr.progressBarStyle);
        init();
    }

    public HPVerticalProgressBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, android.R.attr.progressBarStyle);
        init();
    }

    private void init() {

        View view = inflate(getContext(), R.layout.verticle_progress_bar, this);
        labelText = (TextView) view.findViewById(R.id.label_text);
        descriptionText = (TextView) view.findViewById(R.id.description_text);
        statusImage = (ImageView) view.findViewById(R.id.status_image);
        valueText = (TextView) view.findViewById(R.id.value_text);
        progressBar = (HPProgressBar) view.findViewById(R.id.progress_bar);
        border = view.findViewById(R.id.border_image);
        targetBorder = view.findViewById(R.id.target_border_image);
        pointerArrow = (TriangleView) view.findViewById(R.id.pointer_arrow);
        progressBar.setVertical(true);

        ViewTreeObserver vto = this.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(this);
    }

    public HPVerticalProgressBar setColors(int progressValueColor, int trackColor) {
        progressBar.setColors(progressValueColor, trackColor);
        return this;
    }

    public HPVerticalProgressBar setValues(int progressValue, int maxValue) {
        progressBar.setMax(maxValue);

        ObjectAnimator progressAnimator = ObjectAnimator.ofInt(progressBar, ANIMATION_TYPE, ANIMATION_INITIAL_VALUE, progressValue);
        progressAnimator.setDuration(ANIMATION_DURATION);
        progressAnimator.setInterpolator(new LinearInterpolator());
        progressAnimator.start();

        return this;
    }

    public HPVerticalProgressBar displayDescriptionText(boolean show) {
        descriptionText.setVisibility(show ? VISIBLE : GONE);
        return this;
    }

    public HPVerticalProgressBar displayLabelText(boolean show) {
        labelText.setVisibility(show ? VISIBLE : GONE);
        return this;
    }

    public HPVerticalProgressBar displayTargetBorder(boolean show) {
        targetBorder.setVisibility(show ? VISIBLE : GONE);
        return this;
    }

    public HPVerticalProgressBar setTargetBorder(float target, float max) {
        if (target < 0 || max <= 0) {
            targetBorderHeightRatio = 1;
            return this;
        }
        targetBorderHeightRatio = target / max;
        return this;
    }

    public HPVerticalProgressBar hideBorder(boolean hide) {
        border.setVisibility(hide ? GONE : VISIBLE);
        return this;
    }

    public HPVerticalProgressBar setValueText(int value) {
        valueText.setText(String.valueOf(value));
        valueText.setVisibility(VISIBLE);
        return this;
    }

    public HPVerticalProgressBar setValueTypeFace(Typeface typeFace) {
        valueText.setTypeface(typeFace);
        return this;
    }

    public HPVerticalProgressBar setValueFontSize(float textSize) {
        valueText.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
        return this;
    }

    public HPVerticalProgressBar setLabel(String label) {
        labelText.setText(label);
        return this;
    }


    public HPVerticalProgressBar setLabelTextColor(int colorResourceId) {
        labelText.setTextColor(colorResourceId);
        return this;
    }

    public HPVerticalProgressBar setDescription(String description, int textColorResourceId) {
        descriptionText.setText(description);
        descriptionText.setTextColor(textColorResourceId);
        return this;
    }

    public HPVerticalProgressBar setLabelTypeFace(Typeface typeFace) {
        labelText.setTypeface(typeFace);
        return this;
    }

    public HPVerticalProgressBar setLabelFontSize(float textSize) {
        labelText.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
        return this;
    }

    public HPVerticalProgressBar setStatusImage(Drawable image) {
        statusImage.setImageDrawable(image);
        statusImage.setVisibility(VISIBLE);
        return this;
    }

    public HPVerticalProgressBar setArrowVisibility(int visibility) {
        pointerArrow.setVisibility(visibility);
        return this;
    }

    public HPVerticalProgressBar setArrowColor(int color) {
        pointerArrow.setColor(color);
        return this;
    }


    public void setStatusImageGone() {
        statusImage.setVisibility(INVISIBLE);
    }

    @Override
    public void onGlobalLayout() {
        getViewTreeObserver().removeGlobalOnLayoutListener(this);
        ViewGroup.LayoutParams layoutParams = targetBorder.getLayoutParams();
        layoutParams.height = (int) (progressBar.getHeight() * targetBorderHeightRatio);
        targetBorder.setLayoutParams(layoutParams);
    }

    public void setLableTextSize(float labelTextSize) {
        labelText.setTextSize(TypedValue.COMPLEX_UNIT_PX, labelTextSize);
    }
}
