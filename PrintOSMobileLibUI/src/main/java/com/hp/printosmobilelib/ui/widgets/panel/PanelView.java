package com.hp.printosmobilelib.ui.widgets.panel;

import android.content.Context;
import android.text.Spannable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.hp.printosmobilelib.ui.R;
import com.hp.printosmobilelib.ui.common.HPView;

import butterknife.ButterKnife;

/**
 * Created by Anwar Asbah on 4/12/2016.
 */
public abstract class PanelView<T> extends FrameLayout implements HPView<T> {

    private TextView headerTitle;
    private LinearLayout contentLayout;
    private ProgressBar loadingView;
    private FrameLayout headerLeftViewContainer;
    private View contentView;
    private TextView noInformationTextView;

    private T viewModel;

    public PanelView(Context context) {
        super(context);
        init();
    }

    public PanelView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public PanelView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.view_panel, this);
        contentLayout = (LinearLayout) findViewById(R.id.layout_content);
        loadingView = (ProgressBar) findViewById(R.id.loading_view);
        headerTitle = (TextView) findViewById(R.id.text_title);
        headerLeftViewContainer = (FrameLayout) findViewById(R.id.left_view_container);
        noInformationTextView = (TextView) findViewById(R.id.text_view_no_information);
        setTitle(getTitleSpannable());
        contentView = inflate(getContext(), getContentView(), contentLayout);
        ButterKnife.bind(this, this);
    }

    public void setTitleNumberOfLines(int numberOfLines) {
        headerTitle.setMaxLines(numberOfLines);
    }

    public void onRefresh() {
        //do nothing
    }

    public abstract Spannable getTitleSpannable();

    public abstract int getContentView();

    public abstract void updateViewModel(T viewModel);

    public FrameLayout getHeaderLeftView() {
        return headerLeftViewContainer;
    }

    @Override
    public void showLoading() {
        loadingView.setVisibility(VISIBLE);
        noInformationTextView.setVisibility(INVISIBLE);
    }

    @Override
    public void hideLoading() {
        loadingView.setVisibility(GONE);
    }

    public boolean isLoading() {
        return loadingView.getVisibility() == VISIBLE;
    }

    @Override
    public void setViewModel(T viewModel) {
        this.viewModel = viewModel;
    }

    public T getViewModel() {
        return viewModel;
    }

    public View getView() {
        return contentView;
    }

    public void setTitle(Spannable title) {
        headerTitle.setText(title, TextView.BufferType.SPANNABLE);
    }

    public void sharePanel() {
        //Overriding this method is not required..
        //Concrete classes may hide the implementation of it with their custom implementation.
    }

    public void lockShareButton(boolean lock) {
        //Concrete classes may hide the implementation of it with their custom implementation.
    }

    public void onDestroy() {

    }

    public boolean showEmptyCard() {

        boolean isValid = isValidViewModel();

        if (isValid) {
            noInformationTextView.setVisibility(GONE);
            contentLayout.setVisibility(VISIBLE);
            headerLeftViewContainer.setVisibility(VISIBLE);
        } else {
            noInformationTextView.setText(getEmptyCardText());
            contentLayout.setVisibility(GONE);
            noInformationTextView.setVisibility(VISIBLE);
            headerLeftViewContainer.setVisibility(GONE);
        }

        hideLoading();

        return !isValid;
    }

    protected boolean isValidViewModel() {
        return true;
    }

    protected abstract String getEmptyCardText();
}
