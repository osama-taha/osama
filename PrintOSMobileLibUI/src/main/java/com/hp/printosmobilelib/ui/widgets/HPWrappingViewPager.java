package com.hp.printosmobilelib.ui.widgets;

import android.content.Context;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;

/**
 * created by anwar asbah
 */
public class HPWrappingViewPager extends HPViewPager {

    private static int DEFAULT_MAX_HEIGHT = 200;

    private int maxHeight;

    public HPWrappingViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    void init() {
        super.init();
        maxHeight = DEFAULT_MAX_HEIGHT;
    }

    @Override
    protected int getNewHeight(int widthMeasureSpec) {
        int height = 0;
        FragmentStatePagerAdapter adapter = (FragmentStatePagerAdapter) getAdapter();
        if(adapter != null){
            for (int i = 0; i < adapter.getCount(); i++) {
                View child = adapter.getItem(i).getView();
                height = Math.max(height, getItemHeight(child, widthMeasureSpec));
            }
        }
        return height;
    }

    private int getItemHeight (View child, int widthMeasureSpec){
        if (child != null) {
            child.measure(widthMeasureSpec, MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED));
            return Math.min(maxHeight, child.getMeasuredHeight());
        }
        return 0;
    }

    @Override
    boolean itemsInstantiated() {
        return getAdapter() != null;
    }

    public void setMaxHeight(int maxHeight) {
        this.maxHeight = maxHeight;
    }

    public void setAnimating(boolean animating) {
        this.mAnimStarted = animating;
    }
}
