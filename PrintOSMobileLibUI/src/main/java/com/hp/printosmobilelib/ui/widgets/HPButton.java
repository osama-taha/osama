package com.hp.printosmobilelib.ui.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;

/**
 * Subclass from {@link android.widget.Button} to support different font types.
 *
 * @author Osama Taha
 *         Create on 30/6/2015
 */
public class HPButton extends Button {

    /**
     * Constructor to use when creating the button programmatically.
     *
     * @param context The Context where the button is running in.
     */
    public HPButton(Context context) {
        this(context, null);
    }

    /**
     * Constructor that is called when inflating a widget from XML
     *
     * @param context The Context where the text view is running in.
     * @param attrs   The attributes of the XML definition.
     */
    public HPButton(Context context, AttributeSet attrs) {
        super(context, attrs);

        //Indicates whether this View is currently in edit mode
        // A View is usually in edit mode when displayed within a developer tool
        if (!isInEditMode()) {
            TypefaceManager.setTypeface(this, context, attrs);
        }
    }

    /**
     * Constructor that is called when inflating the button from XML
     * and apply a default style to this view.
     *
     * @param context  The Context where the text view is running in.
     * @param attrs    The attributes of the XML definition.
     * @param defStyle The default style to apply to this view.
     */
    public HPButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        //Indicates whether this View is currently in edit mode
        // A View is usually in edit mode when displayed within a developer tool
        if (!isInEditMode()) {
            TypefaceManager.setTypeface(this, context, attrs);
        }
    }
}