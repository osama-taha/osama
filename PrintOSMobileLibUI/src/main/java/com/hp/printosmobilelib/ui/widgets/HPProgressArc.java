package com.hp.printosmobilelib.ui.widgets;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.SweepGradient;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;

import com.hp.printosmobilelib.ui.R;


public class HPProgressArc extends View {

    private static final String TAG = HPProgressArc.class.getSimpleName();
    private static final int BOUNDARY_ANGLE = 450;
    private static int INVALID_PROGRESS_VALUE = -1;
    // The initial rotational offset -90 means we start at 12 o'clock
    private final int mAngleOffset = -90;
    private int mHeight = 0;
    private int mWidth = 0;

    private Drawable mThumb;
    private RectF mThumbRect;
    private int thumbAngle;
    /**
     * The Maximum value that this SeekArc can be set to
     */
    private int mMax = 100;

    /**
     * The Current value that the SeekArc is set to
     */
    private int mProgress = 0;

    /**
     * The width of the progress line for this SeekArc
     */
    private int mProgressWidth = 4;

    /**
     * The Width of the background arc for the SeekArc
     */
    private int mArcWidth = 2;


    /**
     * The Width of the background arc for the SeekArc
     */
    private int mBackroundWidth = 0;

    /**
     * The Angle to start drawing this Arc from
     */
    private int mStartAngle = 0;

    /**
     * The Angle through which to draw the arc (Max is 360)
     */
    private int mSweepAngle = 300;

    /**
     * The rotation of the SeekArc- 0 is twelve o'clock
     */
    private int mRotation = 210;

    /**
     * Give the SeekArc rounded edges
     */
    private boolean mRoundedEdges = false;


    /**
     * Will the progress increase clockwise or anti-clockwise
     */
    private boolean mClockwise = true;

    // Internal variables
    private int mArcRadius = 0;
    private float mProgressSweep = 0;
    private RectF mArcRectAndBackGround = new RectF();
    private RectF mArcRect = new RectF();
    private Paint mArcPaint;
    private Paint mProgressPaint;
    private int mTranslateX;
    private int mTranslateY;
    private int mThumbXPos = -1;
    private int mThumbYPos = -1;
    private Paint mBackgroundPaint;
    private int[] mProgressSweepColor;
    private float[] mProgressSweepPosition;
    private float mTarget;
    private SweepGradient mShader;
    private int progressWidth;


    public HPProgressArc(Context context) {
        super(context);
        init(context, null, 0);
    }

    public HPProgressArc(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs, 0);
    }

    public HPProgressArc(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs, defStyle);
    }

    public HPProgressArc(Context context, int width, int height) {
        super(context);
        mWidth = width;
        mHeight = height;
        init(context, null, 0);

    }

    private void init(Context context, AttributeSet attrs, int defStyle) {

        final Resources res = getResources();
        float density = context.getResources().getDisplayMetrics().density;

        // Defaults, may need to link this into theme settings
        int arcColor = Color.BLUE;
        int backgroundColor = res.getColor(android.R.color.transparent);
        int progressColor = res.getColor(android.R.color.holo_blue_light);
        int thumbHalfHeight = 0;
        int thumbHalfWidth = 0;
        // Convert progress width to pixels for current density
        mProgressWidth = (int) (mProgressWidth * density);

        if (attrs != null) {
            // Attribute initialization
            final TypedArray a = context.obtainStyledAttributes(attrs,
                    R.styleable.ProgressArc, defStyle, 0);


            mMax = a.getInteger(R.styleable.ProgressArc_max, mMax);
            mProgress = a.getInteger(R.styleable.ProgressArc_progress, mProgress);
            mProgressWidth = (int) a.getDimension(
                    R.styleable.ProgressArc_progressWidth, mProgressWidth);
            mArcWidth = (int) a.getDimension(R.styleable.ProgressArc_arcWidth,
                    mArcWidth);

            mBackroundWidth = (int) a.getDimension(R.styleable.ProgressArc_backroundWidth,
                    mArcWidth);

            mBackroundWidth += mArcWidth / 2;


            mStartAngle = a.getInt(R.styleable.ProgressArc_startAngle, mStartAngle);
            mSweepAngle = a.getInt(R.styleable.ProgressArc_sweepAngle, mSweepAngle);
            mRotation = a.getInt(R.styleable.ProgressArc_rotation, mRotation);
            mRoundedEdges = a.getBoolean(R.styleable.ProgressArc_roundEdges,
                    mRoundedEdges);
            mClockwise = a.getBoolean(R.styleable.ProgressArc_clockwise,
                    mClockwise);
            mThumb = a.getDrawable(R.styleable.ProgressArc_thumb);

            if (mThumb != null) {
                thumbHalfHeight = (int) mThumb.getIntrinsicHeight() / 2;
                thumbHalfWidth = (int) mThumb.getIntrinsicWidth() / 2;
                mThumb.setBounds(-thumbHalfWidth, -thumbHalfHeight, thumbHalfWidth,
                        thumbHalfHeight);
            }
            arcColor = a.getColor(R.styleable.ProgressArc_arcColor, arcColor);
            progressColor = a.getColor(R.styleable.ProgressArc_progressColor,
                    progressColor);


            backgroundColor = a.getColor(R.styleable.ProgressArc_backroundColor,
                    backgroundColor);

            a.recycle();
        }

        mProgress = (mProgress > mMax) ? mMax : mProgress;
        mProgress = (mProgress < 0) ? 0 : mProgress;

        mSweepAngle = (mSweepAngle > 360) ? 360 : mSweepAngle;
        mSweepAngle = (mSweepAngle < 0) ? 0 : mSweepAngle;

        mProgressSweep = (float) mProgress / mMax * mSweepAngle;

        mStartAngle = (mStartAngle > 360) ? 0 : mStartAngle;
        mStartAngle = (mStartAngle < 0) ? 0 : mStartAngle;

        mArcPaint = new Paint();
        mArcPaint.setColor(arcColor);
        mArcPaint.setAntiAlias(true);
        mArcPaint.setStyle(Paint.Style.STROKE);
        mArcPaint.setStrokeWidth(mArcWidth);
        //mArcPaint.setAlpha(45);

        mProgressPaint = new Paint();
        mProgressPaint.setColor(progressColor);
        mProgressPaint.setAntiAlias(true);
        mProgressPaint.setStyle(Paint.Style.STROKE);
        mProgressPaint.setStrokeWidth(mProgressWidth);

        mBackgroundPaint = new Paint();
        mBackgroundPaint.setColor(backgroundColor);
        mBackgroundPaint.setAntiAlias(true);
        mBackgroundPaint.setStrokeCap(Paint.Cap.ROUND);


        if (mRoundedEdges) {
            mArcPaint.setStrokeCap(Paint.Cap.ROUND);
            mProgressPaint.setStrokeCap(Paint.Cap.ROUND);
        }
    }

    public void setTrackColor(int trackColor) {
        mArcPaint.setColor(trackColor);
    }

    public void setThumb(Drawable mThumb) {
        this.mThumb = mThumb;
        if (mThumb != null) {
            int thumbHalfheight = mThumb.getIntrinsicHeight() / 2;
            int thumbHalfWidth = mThumb.getIntrinsicWidth() / 2;
            mThumb.setBounds(-thumbHalfWidth, -thumbHalfheight, thumbHalfWidth,
                    thumbHalfheight);
        }
    }

    public void setProgressColor(int progressColor) {
        mProgressPaint.setColor(progressColor);
    }

    public void setProgressSweepGradient(int[] progressSweepColor, float[] progressSweepPosition) {
        mProgressSweepColor = progressSweepColor;
        mProgressSweepPosition = progressSweepPosition;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if (!mClockwise) {
            canvas.scale(-1, 1, mArcRectAndBackGround.centerX(), mArcRectAndBackGround.centerY());
        }

        // Draw the arcs
        final int arcStart = mStartAngle + mAngleOffset + mRotation;
        final int arcSweep = mSweepAngle;

        canvas.drawCircle(mArcRectAndBackGround.centerX(), mArcRectAndBackGround.centerY(), mArcRadius, mBackgroundPaint);

        canvas.drawArc(mArcRect, arcStart, arcSweep, false, mArcPaint);
        canvas.drawArc(mArcRect, arcStart, Math.max(mProgressSweep, 0.00001f), false, mProgressPaint);

        // Draw the thumb nail
        if (mThumb != null && mThumbXPos != -1) {
            canvas.translate(mTranslateX - mThumbXPos, mTranslateY - mThumbYPos);
            mThumb.draw(canvas);
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        int viewWidth = mWidth == 0 ? getSuggestedMinimumWidth() : mWidth;
        int viewHeight = mHeight == 0 ? getSuggestedMinimumHeight() : mHeight;

        final int height = getDefaultSize(viewHeight,
                heightMeasureSpec);
        final int width = getDefaultSize(viewWidth,
                widthMeasureSpec);

        final int min = Math.min(width, height);
        float top = 0;
        float left = 0;
        int arcDiameter = 0;

        mTranslateX = (int) (width * 0.5f);
        mTranslateY = (int) (height * 0.5f);

        int offset = 0;
        if (mThumb != null) {
            offset = Math.max(mThumb.getIntrinsicWidth(), mThumb.getIntrinsicHeight());
            offset -= mArcWidth;
        }

        arcDiameter = min - offset;
        mArcRadius = arcDiameter / 2;
        top = (height - min) / 2;
        left = (width - min) / 2;
        mArcRectAndBackGround.set(left, top, left + min, top + min);
        mArcRect.set(mArcRectAndBackGround.left + mBackroundWidth + offset / 2,
                mArcRectAndBackGround.top + mBackroundWidth + offset / 2,
                mArcRectAndBackGround.right - mBackroundWidth - offset / 2,
                mArcRectAndBackGround.bottom - mBackroundWidth - offset / 2);
        if (mProgressSweepColor != null) {
            mShader = new SweepGradient(mArcRectAndBackGround.centerX(), mArcRectAndBackGround.centerY(), mProgressSweepColor, mProgressSweepPosition);
            mProgressPaint.setColor(Color.WHITE);
            mShader.setLocalMatrix(new Matrix());
            mProgressPaint.setShader(mShader);
        }
        float targetPositionAngle = Math.min((int) mTarget, mMax);
        targetPositionAngle = targetPositionAngle / mMax * mSweepAngle;
        thumbAngle = (int) (mStartAngle + targetPositionAngle + mRotation + 90);
        mThumbXPos = (int) ((mArcRadius - mBackroundWidth) * Math.cos(Math.toRadians(thumbAngle)));
        mThumbYPos = (int) ((mArcRadius - mBackroundWidth) * Math.sin(Math.toRadians(thumbAngle)));

        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    public ProgressArcClickEvent isThumbClicked(float x, float y) {
        int thumbW = 0, thumbH = 0;
        if (mThumb != null) {
            thumbW = mThumb.getIntrinsicWidth();
            thumbH = mThumb.getIntrinsicHeight();
        }

        int[] locations = new int[2];
        getLocationOnScreen(locations);

        int thumbX = mTranslateX - mThumbXPos;
        int thumbY = mTranslateY - mThumbYPos;

        int left = thumbX - thumbW / 2;
        int top = thumbY - thumbH / 2;

        if (left <= x && x <= left + thumbW && top <= y && y <= top + thumbH) {
            ProgressArcClickEvent event = new ProgressArcClickEvent();
            event.locationX = locations[0] + thumbX;
            event.locationY = locations[1] + thumbY + thumbW / 2;
            event.gravity = thumbAngle < BOUNDARY_ANGLE ? Gravity.RIGHT : Gravity.LEFT;
            return event;
        }

        return null;
    }

    @Override
    protected void drawableStateChanged() {
        super.drawableStateChanged();
        if (mThumb != null && mThumb.isStateful()) {
            int[] state = getDrawableState();
            mThumb.setState(state);
        }
        invalidate();
    }


    private double getTouchDegrees(float xPos, float yPos) {
        float x = xPos - mTranslateX;
        float y = yPos - mTranslateY;
        //invert the x-coord if we are rotating anti-clockwise
        x = (mClockwise) ? x : -x;
        // convert to arc Angle
        double angle = Math.toDegrees(Math.atan2(y, x) + (Math.PI / 2)
                - Math.toRadians(mRotation));
        if (angle < 0) {
            angle = 360 + angle;
        }
        angle -= mStartAngle;
        return angle;
    }

    private int getProgressForAngle(double angle) {
        int touchProgress = (int) Math.round(valuePerDegree() * angle);

        touchProgress = (touchProgress < 0) ? INVALID_PROGRESS_VALUE
                : touchProgress;
        touchProgress = (touchProgress > mMax) ? INVALID_PROGRESS_VALUE
                : touchProgress;
        return touchProgress;
    }

    private float valuePerDegree() {
        return (float) mMax / mSweepAngle;
    }

    private void onProgressRefresh(int progress, boolean fromUser) {
        updateProgress(progress, fromUser);
    }

    private void updateThumbPosition(float target) {
        mTarget = target;
    }

    private void updateProgress(int progress, boolean fromUser) {

        if (progress == INVALID_PROGRESS_VALUE) {
            return;
        }

        progress = (progress > mMax) ? mMax : progress;
        progress = (mProgress < 0) ? 0 : progress;

        mProgress = progress;
        mProgressSweep = (float) progress / mMax * mSweepAngle;

        invalidate();
    }


    public void setProgress(int progress) {
        updateProgress(progress, false);
    }

    public void setTarget(int progress) {
        updateThumbPosition(progress);
    }


    public int getArcWidth() {
        return mArcWidth;
    }

    public void setArcWidth(int mArcWidth) {
        this.mArcWidth = mArcWidth;
        this.mBackroundWidth += mArcWidth / 2;
        mArcPaint.setStrokeWidth(mArcWidth);
    }

    public int getArcRotation() {
        return mRotation;
    }

    public void setRoundedEdges(boolean isEnabled) {
        mRoundedEdges = isEnabled;
        if (mRoundedEdges) {
            mArcPaint.setStrokeCap(Paint.Cap.ROUND);
            mProgressPaint.setStrokeCap(Paint.Cap.ROUND);
        } else {
            mArcPaint.setStrokeCap(Paint.Cap.SQUARE);
            mProgressPaint.setStrokeCap(Paint.Cap.SQUARE);
        }
    }


    public void setClockwise(boolean isClockwise) {
        mClockwise = isClockwise;
    }

    public void setBackgroundWidth(int backgroundWidth) {
        this.mBackroundWidth = backgroundWidth;
    }

    public void setProgressWidth(int progressWidth) {
        this.mProgressWidth = progressWidth;
        mProgressPaint.setStrokeWidth(mProgressWidth);
    }

    public int getProgressWidth() {
        return progressWidth;
    }

    public class ProgressArcClickEvent {
        public int locationX;
        public int locationY;
        public int gravity;
    }
}
