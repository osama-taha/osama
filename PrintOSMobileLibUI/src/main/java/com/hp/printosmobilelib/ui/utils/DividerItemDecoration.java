package com.hp.printosmobilelib.ui.utils;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.View;

import com.hp.printosmobilelib.ui.R;

/**
 * Created by Anwar Asbah on 5/9/2016.
 */
public class DividerItemDecoration extends RecyclerView.ItemDecoration {
    private static final int DEFAULT_VERTICAL_MARGIN = 5;
    private Drawable dividerDrawable;
    private Context context;
    private boolean isVertical;
    private int columnNumber;
    private int numberOfItems;
    private int margin;

    public DividerItemDecoration(Context context, int numberOfColumns, int numberOfItems, boolean isVertical) {
        this.context = context;
        this.isVertical = isVertical;
        this.columnNumber = numberOfColumns;
        this.numberOfItems = numberOfItems;
        this.dividerDrawable = ResourcesCompat.getDrawable(this.context.getResources(), R.drawable.line_divider, null);
        this.margin = isVertical ? DEFAULT_VERTICAL_MARGIN : 0;
    }

    public DividerItemDecoration(Context context, int numberOfColumns, int numberOfItems, int margin, boolean isVertical) {
        this.context = context;
        this.isVertical = isVertical;
        this.columnNumber = numberOfColumns;
        this.numberOfItems = numberOfItems;
        this.dividerDrawable = ResourcesCompat.getDrawable(this.context.getResources(), R.drawable.line_divider, null);
        this.margin = margin;
    }

    @Override
    public void onDrawOver(Canvas c, RecyclerView parent, RecyclerView.State state) {
        int margin = this.margin;

        margin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, margin, Resources.getSystem().getDisplayMetrics());

        int childCount = parent.getChildCount();

        for (int i = 0; i < childCount; i++) {
            if ((i + 1) % numberOfItems == 0) {
                continue;
            }

            View child = parent.getChildAt(i);
            int top, bottom, right, left;

            if(isVertical){
                top = child.getTop() + margin;
                bottom = child.getBottom() - margin;
                right = child.getRight();
                left = right - dividerDrawable.getIntrinsicWidth();
            }else{
                top = child.getBottom();
                bottom = top + dividerDrawable.getIntrinsicHeight();
                right = parent.getRight() - margin;
                left = child.getLeft() + margin;
            }

            dividerDrawable.setBounds(left, top, right, bottom);
            dividerDrawable.draw(c);
        }
    }
}