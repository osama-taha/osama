package com.hp.printosmobilelib.ui.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;

/**
 * Created by Anwar Asbah on 5/23/2016.
 */
public class RotatedLayout extends FrameLayout {
    public RotatedLayout(Context context) {
        super(context);
        init();
    }

    public RotatedLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public RotatedLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        setPivotX(0);
        setPivotY(0);
        setRotation(90f);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(heightMeasureSpec, widthMeasureSpec);
        setTranslationX(getMeasuredHeight());
    }
}
