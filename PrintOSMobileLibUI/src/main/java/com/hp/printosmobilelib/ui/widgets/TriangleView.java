package com.hp.printosmobilelib.ui.widgets;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by Anwar Asbah on 10/20/2016.
 */
public class TriangleView extends View {

    private int color = Color.RED;
    private Paint paint = new Paint();

    public TriangleView(Context context) {
        super(context);
    }

    public TriangleView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TriangleView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setColor(int color){
        this.color = color;
        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        int w = getWidth();
        int h = getHeight();

        Path path = new Path();
        path.moveTo( 0, 0);
        path.lineTo( w, 0);
        path.lineTo( w/2 , h);
        path.lineTo( 0 , 0);
        path.close();

        paint.setColor(color);
        canvas.drawPath(path, paint);
    }
}
