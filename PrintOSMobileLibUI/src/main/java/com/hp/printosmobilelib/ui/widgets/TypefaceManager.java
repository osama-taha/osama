package com.hp.printosmobilelib.ui.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.widget.TextView;

import com.hp.printosmobilelib.ui.R;

/**
 * Responsible for managing the font typefaces for different views.
 *
 * @author Osama Taha
 *         Created on 30/6/2015
 */
public class TypefaceManager {

    /**
     * For now we have 4 type faces {@link HPTypeface}
     */
    private static final int TYPE_FACE_COUNT = 4;

    /**
     * An array to cache the type faces, we don't need to load the font from assets every time.
     */
    private final static SparseArray<Typeface> Cache = new SparseArray<Typeface>(TYPE_FACE_COUNT);

    /**
     * Initialize the typeface using the attributes of the XML definition.
     *
     * @param view    The custom text view (Note that: button,edit text are subclasses from textView)
     * @param context The activity or fragment where the view is added.
     * @param attrs   The attributes of the XML definition.
     */
    public static void setTypeface(TextView view, Context context, AttributeSet attrs) {

        Typeface typeface = null;

        if (attrs != null) {

            //Obtain the HPTextView style in order to get the typeface attribute from.
            TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.HPTextView);

            //Check if the view has the typeface attribute. if not do nothing.
            if (typedArray.hasValue(R.styleable.HPTextView_typeface)) {
                int typefaceValue = typedArray.getInt(R.styleable.HPTextView_typeface, HPTypeface.HP_REGULAR);
                typeface = TypefaceManager.getTypeface(context, typefaceValue);
            } else if (typedArray.hasValue(R.styleable.ResizeTextView_typeface)) {
                int typefaceValue = typedArray.getInt(R.styleable.ResizeTextView_typeface, HPTypeface.HP_REGULAR);
                typeface = TypefaceManager.getTypeface(context, typefaceValue);
            } else if (typedArray.hasValue(R.styleable.HPEditText_typeface)) {
                int typefaceValue = typedArray.getInt(R.styleable.HPEditText_typeface, HPTypeface.HP_REGULAR);
                typeface = TypefaceManager.getTypeface(context, typefaceValue);
            }

            typedArray.recycle();

        } else {

            //Obtain default typeface in case the AttributeSet is null.
            typeface = TypefaceManager.getTypeface(context, HPTypeface.HP_REGULAR);

        }

        //Setup typeface for view.
        view.setPaintFlags(view.getPaintFlags() | Paint.SUBPIXEL_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);
        view.setTypeface(typeface);
    }

    /**
     * Check the typefaces cache to obtain the typeface by its value if not get the font from assets folder.
     *
     * @param context       The Context where the text view is running in.
     * @param typefaceValue The value of "typeface" attribute mapped with the typeface name. {@see attr.xml file}
     * @return {@link Typeface}
     */
    public static Typeface getTypeface(Context context, int typefaceValue) {

        Typeface typeface = Cache.get(typefaceValue);
        if (typeface == null) {
            typeface = Typeface.createFromAsset(context.getAssets(), getFontPath(context, typefaceValue));
            Cache.put(typefaceValue, typeface);
        }
        return typeface;
    }

    /**
     * Create typeface from assets.
     *
     * @param context The Context the widget is running in, through which it can
     *                access the current theme, resources, etc.
     * @param value   The value of "typeface" attribute mapped with the typeface name. {@see attr.xml file}
     * @return {@link Typeface}
     */
    public static String getFontPath(Context context, int value) {

        String assetsPath;
        switch (value) {
            case HPTypeface.ROBOTO_REGULAR:
                assetsPath = "fonts/Roboto-Regular.ttf";
                break;
            case HPTypeface.ROBOTO_MEDIUM:
                assetsPath = "fonts/Roboto-Medium.ttf";
                break;
            case HPTypeface.ROBOTO_BOLD:
                assetsPath = "fonts/Roboto-Bold.ttf";
                break;
            case HPTypeface.ROBOTO_LIGHT:
                assetsPath = "fonts/Roboto-Light.ttf";
                break;
            case HPTypeface.HP_BOLD:
                assetsPath = "fonts/HPSimplifiedW01-Bold.ttf";
                break;
            case HPTypeface.HP_BOLD_ITALIC:
                assetsPath = "fonts/HPSimplifiedW01-BoldItalic.ttf";
                break;
            case HPTypeface.HP_ITALIC:
                assetsPath = "fonts/HPSimplifiedW01-Italic.ttf";
                break;
            case HPTypeface.HP_LIGHT:
                assetsPath = "fonts/HPSimplifiedW01-Light.ttf";
                break;
            case HPTypeface.HP_LIGHT_ITALIC:
                assetsPath = "fonts/HPSimplifiedW01-LightItalic.ttf";
                break;
            case HPTypeface.HP_REGULAR:
                assetsPath = "fonts/HPSimplifiedW01-Regular.ttf";
                break;
            default:
                assetsPath = "fonts/HPSimplifiedW01-Regular.ttf";
                break;
        }
        return assetsPath;
    }

    /**
     * Values for the typeface attributes.
     * {@see attr.xml file}
     */
    public class HPTypeface {

        public final static int ROBOTO_REGULAR = 0;
        public final static int ROBOTO_MEDIUM = 1;
        public final static int ROBOTO_BOLD = 2;
        public final static int ROBOTO_LIGHT = 3;
        public final static int HP_BOLD = 4;
        public final static int HP_BOLD_ITALIC = 5;
        public final static int HP_ITALIC = 6;
        public final static int HP_LIGHT = 7;
        public final static int HP_LIGHT_ITALIC = 8;
        public final static int HP_REGULAR = 9;
    }

}
