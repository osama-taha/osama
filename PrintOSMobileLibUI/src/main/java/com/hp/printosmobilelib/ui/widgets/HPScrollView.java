package com.hp.printosmobilelib.ui.widgets;

import android.content.Context;
import android.support.v4.widget.NestedScrollView;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ScrollView;

/**
 * Created by osama on 12/14/16.
 */
public class HPScrollView extends NestedScrollView {

    OnScrollStopListener listener;

    public interface OnScrollStopListener {

        void onScrollStopped(int y);

        void onScrollDown();

        void onScrollUp();

        void onTopReached();

        void onBottomReached();
    }

    public HPScrollView(Context context) {
        super(context);
    }

    public HPScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onScrollChanged(int scrollX, int scrollY, int oldScrollX, int oldScrollY) {

        if (listener == null) {
            return;
        }

        if (scrollY > oldScrollY) {
            //Scroll down.
            listener.onScrollDown();
        }

        if (scrollY < oldScrollY) {
            //Scroll UP.
            listener.onScrollUp();
        }

        if (scrollY == 0) {
            listener.onTopReached();
        }

        if (scrollY == (getChildAt(0).getMeasuredHeight() - getMeasuredHeight())) {
            listener.onBottomReached();
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        switch (ev.getAction()) {
            case MotionEvent.ACTION_UP:
                //checkIfScrollStopped();
        }

        return super.onTouchEvent(ev);
    }

    int initialY = 0;

    private void checkIfScrollStopped() {
        initialY = getScrollY();
        this.postDelayed(new Runnable() {
            @Override
            public void run() {
                int updatedY = getScrollY();
                if (updatedY == initialY) {
                    if (listener != null) {
                        listener.onScrollStopped(getScrollY());
                    }
                } else {
                    initialY = updatedY;
                    checkIfScrollStopped();
                }
            }
        }, 50);
    }

    public void setOnScrollStoppedListener(OnScrollStopListener yListener) {
        listener = yListener;
    }

}