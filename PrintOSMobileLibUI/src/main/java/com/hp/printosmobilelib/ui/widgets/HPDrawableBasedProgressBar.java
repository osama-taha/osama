package com.hp.printosmobilelib.ui.widgets;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.ClipDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hp.printosmobilelib.ui.R;

/**
 * @author Anwar Asbah Create on 8/6/2017
 */
public class HPDrawableBasedProgressBar extends RelativeLayout {

    private static final float[] CORNER_RADII = new float[]{100, 100, 100, 100, 100, 100, 100, 100};
    private int trackColor = Color.parseColor("#ededed");
    private int progressColor = Color.parseColor("#295260");

    private int progressValue;
    private int maxValue;
    private float[] barDrawableCornerRadii;

    TextView maxScoreView;
    TextView minScoreView;
    ProgressBar progressBar;
    View progressReflectionForIndicator;
    View progressIndicatorView;
    ImageView progressIndicatorImageView;
    View progressBarLayout;
    TextView actualScoreTextView;
    View progressReflectionForScore;
    TextView minValueTextView;
    TextView maxValueTextView;
    TextView barTagTextView;

    public HPDrawableBasedProgressBar(Context context) {
        super(context);
        init();
    }

    public HPDrawableBasedProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs, android.R.attr.progressBarStyleHorizontal);
        init();
    }

    public HPDrawableBasedProgressBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, android.R.attr.progressBarStyleHorizontal);
        init();
    }

    private void init() {

        barDrawableCornerRadii = new float[]{0, 0, 0, 0, 0, 0, 0, 0};

        View view = inflate(getContext(), R.layout.drawable_based_progress_bar, this);

        maxScoreView = (TextView) view.findViewById(R.id.max_score_text_view);
        minScoreView = (TextView) view.findViewById(R.id.min_score_text_view);
        progressBar = (ProgressBar) view.findViewById(R.id.progress_bar);
        progressIndicatorView = view.findViewById(R.id.progress_indicator);
        progressIndicatorImageView = (ImageView) view.findViewById(R.id.progress_indicator_image_view);
        progressReflectionForIndicator = view.findViewById(R.id.progress_reflection_for_indicator);
        progressBarLayout = view.findViewById(R.id.progress_bar_layout);
        actualScoreTextView = (TextView) view.findViewById(R.id.actual_score_text_view);
        progressReflectionForScore = view.findViewById(R.id.progress_reflection_for_text);
        minValueTextView = (TextView) view.findViewById(R.id.min_value_text_view);
        maxValueTextView = (TextView) view.findViewById(R.id.max_value_text_view);
        barTagTextView = (TextView) view.findViewById(R.id.bar_unit_tag_text_view);

        maxScoreView.setBackground(getRoundedDrawable(Color.WHITE, trackColor));
        minScoreView.setBackground(getRoundedDrawable(Color.WHITE, progressColor));
        progressIndicatorView.setBackground(getRoundedDrawable(progressColor, Color.WHITE));

        progressValue = 0;
        maxValue = 1;

        progressBar.setMax(1);
        progressBar.setProgress(0);
        progressBar.setProgressDrawable(getProgressDrawable());
    }

    private Drawable getRoundedDrawable(int color, int strokeColor) {
        GradientDrawable shape = new GradientDrawable();
        shape.setShape(GradientDrawable.RECTANGLE);
        shape.setCornerRadii(CORNER_RADII);
        shape.setStroke((int) dpToPixel(3), strokeColor);
        shape.setColor(color);
        return shape;
    }

    public HPDrawableBasedProgressBar setColors(int progressValueColor) {
        this.progressColor = progressValueColor;
        minScoreView.setBackground(getRoundedDrawable(Color.WHITE, progressColor));
        progressIndicatorView.setBackground(getRoundedDrawable(progressColor, Color.WHITE));
        progressBar.setProgressDrawable(getProgressDrawable());
        return this;
    }

    public HPDrawableBasedProgressBar setScoreAndLimit(int score, int limit) {
        maxValue = Math.max(limit, 0);
        progressValue = Math.min(this.maxValue, score);
        progressBar.setMax(this.maxValue);
        progressBar.setProgress(this.progressValue);

        progressReflectionForIndicator.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                progressReflectionForIndicator.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                int totalWidth = progressBar.getWidth();

                float progressPercentage = maxValue == 0 ? 0 : (float) progressValue / (float) maxValue;

                ViewGroup.LayoutParams layoutParams = progressReflectionForIndicator.getLayoutParams();
                layoutParams.width = (int) (totalWidth * progressPercentage);
                progressReflectionForIndicator.setLayoutParams(layoutParams);

                layoutParams = progressReflectionForScore.getLayoutParams();
                layoutParams.width = (int) (totalWidth * progressPercentage);
                progressReflectionForScore.setLayoutParams(layoutParams);
            }
        });

        maxScoreView.setText(String.valueOf(maxValue));

        return this;
    }

    public HPDrawableBasedProgressBar setValue(String actualScore) {
        actualScoreTextView.setText(actualScore);
        return this;
    }

    public HPDrawableBasedProgressBar setMinValue(String minValue) {
        minValueTextView.setText(minValue);
        return this;
    }

    public HPDrawableBasedProgressBar setMaxValue(String maxValue) {
        maxValueTextView.setText(maxValue);
        return this;
    }

    public HPDrawableBasedProgressBar setBarTag(String tag) {
        barTagTextView.setText(tag);
        return this;
    }

    public HPDrawableBasedProgressBar setIndicatorDrawable(Drawable indicatorDrawable) {
        progressIndicatorImageView.setImageDrawable(indicatorDrawable);
        return this;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }

    public Drawable getProgressDrawable() {
        RoundRectShape progressRect = new RoundRectShape(barDrawableCornerRadii, null, null);
        ShapeDrawable progressDrawable = new ShapeDrawable(progressRect);
        progressDrawable.getPaint().setColor(progressColor);

        RoundRectShape backgroundRect = new RoundRectShape(barDrawableCornerRadii, null, null);
        ShapeDrawable backgroundDrawable = new ShapeDrawable(backgroundRect);
        backgroundDrawable.getPaint().setColor(trackColor);

        ClipDrawable progressClip = new ClipDrawable(progressDrawable, Gravity.LEFT | Gravity.BOTTOM, ClipDrawable.HORIZONTAL);

        Drawable[] drawables = {backgroundDrawable, progressClip};
        LayerDrawable layerdrawable = new LayerDrawable(drawables);

        layerdrawable.setId(0, android.R.id.background);
        layerdrawable.setId(1, android.R.id.progress);

        return layerdrawable;
    }

    private float dpToPixel(float value) {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, value, getResources().getDisplayMetrics());
    }
}
