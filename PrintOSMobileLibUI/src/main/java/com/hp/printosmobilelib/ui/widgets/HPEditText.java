package com.hp.printosmobilelib.ui.widgets;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.res.ResourcesCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.hp.printosmobilelib.ui.R;

public class HPEditText extends EditText implements View.OnTouchListener, View.OnFocusChangeListener {

    private Drawable drawableRight;
    private OnTouchListener onTouchListener;
    private OnFocusChangeListener onFocusChangeListener;
    private boolean forceHide;

    public HPEditText(Context context) {
        this(context, null);
        init();
    }

    public HPEditText(Context context, AttributeSet attrs) {
        super(context, attrs);

        //Indicates whether this View is currently in edit mode
        // A View is usually in edit mode when displayed within a developer tool
        if (!isInEditMode()) {
            TypefaceManager.setTypeface(this, context, attrs);
        }
        init();
    }

    public HPEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        //Indicates whether this View is currently in edit mode
        // A View is usually in edit mode when displayed within a developer tool
        if (!isInEditMode()) {
            TypefaceManager.setTypeface(this, context, attrs);
        }
        init();
    }

    @Override
    public void setOnTouchListener(OnTouchListener onTouchListener) {
        this.onTouchListener = onTouchListener;
    }

    @Override
    public void setOnFocusChangeListener(OnFocusChangeListener onFocusChangeListener) {
        this.onFocusChangeListener = onFocusChangeListener;
    }


    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if (getCompoundDrawables()[2] != null) {
            //Check if the clicked drawable is the right side one in order to clear the text inside.
            boolean tappedX = event.getX() > (getWidth() - getPaddingRight() - drawableRight.getIntrinsicWidth());
            if (tappedX) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    setText("");
                }
                return true;
            }
        }
        //forward the touch listener in case the user has its own listener.
        if (onTouchListener != null) {
            return onTouchListener.onTouch(v, event);
        }
        return false;
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (hasFocus) {
            //clear/show the cross icon based on the text side in case the edit text is focused.
            setClearShowDrawables(isNotEmpty(getText()));
        } else {
            //else hide the x icon.
            setClearShowDrawables(false);
        }
        //forward the focus change listener in case the user has its own listener.
        if (onFocusChangeListener != null) {
            onFocusChangeListener.onFocusChange(v, hasFocus);
        }
    }

    private void init() {

        drawableRight = getCompoundDrawables()[2];
        if (drawableRight == null) {
            drawableRight = ResourcesCompat.getDrawable(getResources(), R.drawable.cancel, null);
        }
        drawableRight.setBounds(0, 0, drawableRight.getIntrinsicWidth(), drawableRight.getIntrinsicHeight());
        //hide the button initially (to be shown when the edit text is focused and text inside is not empty )
        setClearShowDrawables(false);
        super.setOnFocusChangeListener(this);
        super.setOnTouchListener(this);
        this.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (isFocused()) {
                    setClearShowDrawables(isNotEmpty(charSequence.toString()));
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        forceHide = false;
    }

    protected void setClearShowDrawables(boolean isVisible) {
        if(forceHide){
            setCompoundDrawables(getCompoundDrawables()[0], getCompoundDrawables()[1], null, getCompoundDrawables()[3]);
            return;
        }

        boolean wasVisible = (getCompoundDrawables()[2] != null);
        if (isVisible != wasVisible) {
            Drawable drawableRight = isVisible ? this.drawableRight : null;
            //the following line will take care of showing the left,right,bottom,top drawables of the edit text.
            setCompoundDrawables(getCompoundDrawables()[0], getCompoundDrawables()[1], drawableRight, getCompoundDrawables()[3]);
        }
    }

    public void forceHideClearButton(boolean force){
        this.forceHide = force;
    }

    /**
     * This is a Fix of buggy behavior of android, where after setting text using setText(...), selection remain on
     * first character and not at last character.
     */
    @Override
    public void setText(CharSequence text, BufferType type) {
        super.setText(text, type);
        if (text != null && !(text.toString().trim().length() == 0)) {
            final int textLength = text.length();
            if (getSelectionEnd() != textLength) {
                setSelection(textLength);
            }
        }
    }

    public static boolean isNotEmpty(CharSequence str) {
        return !(str == null || str.length() == 0);
    }
}
