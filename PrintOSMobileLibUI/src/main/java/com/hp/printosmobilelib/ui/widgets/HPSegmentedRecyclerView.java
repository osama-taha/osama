package com.hp.printosmobilelib.ui.widgets;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.hp.printosmobilelib.ui.R;
import com.hp.printosmobilelib.ui.utils.DividerItemDecoration;

import java.util.List;

public class HPSegmentedRecyclerView extends FrameLayout {

    private RecyclerView content;
    private RecyclerView header;

    public HPSegmentedRecyclerView(Context context) {
        this(context, null);
        init();
    }

    public HPSegmentedRecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public HPSegmentedRecyclerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        setVisibility(INVISIBLE);

        View view = inflate(getContext(), R.layout.segmented_recyclerview, this);
        header = (RecyclerView) view.findViewById(R.id.recycler_view_header);
        content = (RecyclerView) view.findViewById(R.id.recycler_view_content);

        RecyclerView.LayoutManager headerManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        header.setLayoutManager(headerManager);
    }

    public void setSegmentsVisible(boolean visible) {
        header.setVisibility(visible ? VISIBLE : GONE);
    }

    public void setAdapter(SegmentedRecyclerViewAdapter adapter) {
        content.setAdapter(adapter);
        header.setAdapter(adapter.getHeaderAdapter());
        setVisibility(VISIBLE);
    }

    public void setContentLayoutManager(RecyclerView.LayoutManager layoutManager) {
        content.setLayoutManager(layoutManager);
    }

    public void setHasFixedSize(boolean hasFixedSize) {
        this.content.setHasFixedSize(hasFixedSize);
    }

    public void addItemDecoration(DividerItemDecoration dividerItemDecoration) {
        this.content.addItemDecoration(dividerItemDecoration);
    }

    public RecyclerView getContentRecyclerView() {
        return content;
    }

    static class HeaderAdapter extends RecyclerView.Adapter<HeaderAdapter.HeaderHolder> {

        private final SegmentStyle segmentStyle;
        private List<Integer> segmentsNameIds;
        private int selectedNameId;
        private SegmentedAdapterCallback callback;
        private Context context;

        public HeaderAdapter(Context context, List<Integer> segmentsName, SegmentedAdapterCallback callback, SegmentStyle segmentStyle) {
            this.segmentsNameIds = segmentsName;
            this.segmentStyle = segmentStyle;
            this.callback = callback;
            this.context = context;
            if (segmentsName != null && segmentsName.size() > 0) {
                this.selectedNameId = segmentsName.get(0);
            }
        }

        @Override
        public HeaderHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View tabView = LayoutInflater.from(context).inflate(R.layout.segmented_recycler_view_tab, null);
            HeaderHolder viewHolder = new HeaderHolder(tabView);

            return viewHolder;
        }

        @Override
        public int getItemCount() {
            return segmentsNameIds == null ? 0 : segmentsNameIds.size();
        }

        @Override
        public void onBindViewHolder(HeaderHolder holder, int position) {
            int segmentNameId = segmentsNameIds.get(position);
            String tabName = context.getString(segmentNameId);
            holder.tabName.setText(tabName);
            holder.id = segmentNameId;
            if (segmentNameId == selectedNameId) {
                holder.tabName.setTextColor(ResourcesCompat.getColor(context.getResources(), segmentStyle.getSelectedTabTextColor(), null));
                holder.tabName.setTextSize(TypedValue.COMPLEX_UNIT_PX, segmentStyle.getTabTextFontSize());
                holder.tabName.setTypeface(segmentStyle.getSelectedTabTextTypeFace());
                holder.selectionIndicator.setVisibility(VISIBLE);
            } else {
                holder.tabName.setTextColor(ResourcesCompat.getColor(context.getResources(), segmentStyle.getTabTextColor(), null));
                holder.tabName.setTextSize(TypedValue.COMPLEX_UNIT_PX, segmentStyle.getTabTextFontSize());
                holder.tabName.setTypeface(segmentStyle.getTabTextTypeFace());
                holder.selectionIndicator.setVisibility(GONE);
            }
        }

        class HeaderHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            TextView tabName;
            View selectionIndicator;
            int id;

            public HeaderHolder(View itemView) {
                super(itemView);
                tabName = (TextView) itemView.findViewById(R.id.tab_name);
                selectionIndicator = itemView.findViewById(R.id.selection_underline);
                itemView.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
                selectedNameId = id;
                notifyDataSetChanged();
                callback.onSegmentTabClicked(selectedNameId);
            }
        }
    }

    public static class SegmentStyle {

        private int tabTextColor;
        private int selectedTabTextColor;
        private Typeface tabTextTypeFace;
        private Typeface selectedTabTextTypeFace;
        private float tabTextFontSize;

        public SegmentStyle(int tabTextColor, int selectedTabTextColor, Typeface tabTextTypeFace, Typeface selectedTabTextTypeFace, float tabTextFontSize) {
            this.tabTextColor = tabTextColor;
            this.selectedTabTextColor = selectedTabTextColor;
            this.tabTextTypeFace = tabTextTypeFace;
            this.selectedTabTextTypeFace = selectedTabTextTypeFace;
            this.tabTextFontSize = tabTextFontSize;
        }

        public int getTabTextColor() {
            return tabTextColor;
        }

        public int getSelectedTabTextColor() {
            return selectedTabTextColor;
        }

        public Typeface getTabTextTypeFace() {
            return tabTextTypeFace;
        }

        public Typeface getSelectedTabTextTypeFace() {
            return selectedTabTextTypeFace;
        }

        public float getTabTextFontSize() {
            return tabTextFontSize;
        }
    }

    public interface SegmentedAdapterCallback {
        void onSegmentTabClicked(int segmentNameId);
    }

    static public abstract class SegmentedRecyclerViewAdapter<T extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<T>
            implements SegmentedAdapterCallback {

        HeaderAdapter headerAdapter;
        Context context;

        public SegmentedRecyclerViewAdapter(List<Integer> segmentsName, Context context, SegmentStyle segmentStyle) {
            this.context = context;
            headerAdapter = new HeaderAdapter(context, segmentsName, this, segmentStyle);
        }

        private HeaderAdapter getHeaderAdapter() {
            return headerAdapter;
        }
    }

}
