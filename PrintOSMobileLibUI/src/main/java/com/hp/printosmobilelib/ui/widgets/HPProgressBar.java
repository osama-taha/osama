package com.hp.printosmobilelib.ui.widgets;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ClipDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.ProgressBar;

/**
 * @author Anwar Asbah Create on 5/9/2015
 */
public class HPProgressBar extends ProgressBar {

    private static final int BAR_CORNERS_RADIUS = 1;
    private static final int TARGET_INDICATOR_STROKE_WIDTH = 1;
    private static final int TARGET_INDICATOR_RADIUS = 6;

    private final Paint paintStroke = new Paint(Paint.ANTI_ALIAS_FLAG);
    private final Paint paintFill = new Paint(Paint.ANTI_ALIAS_FLAG);

    private int targetValue;
    private int progressValue;
    private int maxValue;

    private int trackColor = Color.parseColor("#8aa0a8");
    private int progressColor = Color.parseColor("#295260");
    private int targetIndicatorStrokeColor = Color.parseColor("#d1d1d1");
    private int targetIndicatorFillColor = Color.parseColor("#ffffff");

    private float targetIndicatorRadius;
    private float[] barDrawableCornerRadii;

    boolean isVertical;

    public HPProgressBar(Context context) {
        super(context);
        init();
    }

    public HPProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs, android.R.attr.progressBarStyleHorizontal);
        init();
    }

    public HPProgressBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, android.R.attr.progressBarStyleHorizontal);
        init();
    }

    private void init() {
        isVertical = false;

        targetIndicatorRadius = dpToPixel(TARGET_INDICATOR_RADIUS);

        float cornerRadius = dpToPixel(BAR_CORNERS_RADIUS);
        barDrawableCornerRadii = new float[]{cornerRadius, cornerRadius, cornerRadius, cornerRadius,
                cornerRadius, cornerRadius, cornerRadius, cornerRadius};

        paintFill.setStyle(Paint.Style.FILL);
        paintStroke.setStyle(Paint.Style.STROKE);
        paintStroke.setStrokeWidth(dpToPixel(TARGET_INDICATOR_STROKE_WIDTH));

        //Setting default max and progress values
        setMax(1);
        setProgress(0);
        setProgressDrawable(getProgressDrawable());
    }

    public HPProgressBar setTargetIndicatorRadius(float radius) {
        this.targetIndicatorRadius = radius * getContext().getResources().getDisplayMetrics().density;
        return this;
    }

    public HPProgressBar setProgressBarCorner(float radius) {
        radius *= getContext().getResources().getDisplayMetrics().density;
        this.barDrawableCornerRadii = new float[]{
                radius, radius, radius, radius, radius, radius, radius, radius
        };
        setProgressDrawable(getProgressDrawable());
        return this;
    }

    public HPProgressBar setColors(int progressValueColor, int trackColor) {
        this.progressColor = progressValueColor;
        this.trackColor = trackColor;
        setProgressDrawable(getProgressDrawable());
        return this;
    }

    public HPProgressBar setVertical(boolean isVerticale){
        this.isVertical = isVerticale;
        setProgressDrawable(getProgressDrawable());
        return this;
    }

    public HPProgressBar setColors(int progressValueColor, int trackColor, int targetIndicatorFillColor, int targetIndicatorStrokeColor) {
        this.progressColor = progressValueColor;
        this.trackColor = trackColor;
        this.targetIndicatorFillColor = targetIndicatorFillColor;
        this.targetIndicatorStrokeColor = targetIndicatorStrokeColor;
        setProgressDrawable(getProgressDrawable());
        return this;
    }

    public HPProgressBar setValues(int progressValue, int maxValue) {
        this.maxValue = Math.max(maxValue, 0);
        this.progressValue = Math.min(this.maxValue, progressValue);

        //setting target value to -1 so it won't be considered
        this.targetValue = -1;

        setMax(Math.max(1, this.maxValue));
        setProgress(this.progressValue);

        return this;
    }

    public HPProgressBar setValues(int progressValue, int maxValue, int targetValue) {
        this.maxValue = Math.max(maxValue, 1);
        this.progressValue = Math.min(this.maxValue, progressValue);
        this.targetValue = Math.min(this.maxValue, targetValue);

        setMax(this.maxValue);
        setProgress(this.progressValue);

        return this;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        int width = getWidth();
        int height = getHeight();

        if (maxValue <= 0)
            return;

        if (targetValue > 0) {
            float centerY = height / 2f;
            float centerX = (width * (float) targetValue / (float) maxValue);
            centerX = Math.min(width - targetIndicatorRadius, Math.max(centerX, targetIndicatorRadius));

            paintFill.setColor(targetIndicatorFillColor);
            paintStroke.setColor(targetIndicatorStrokeColor);

            canvas.drawCircle(centerX, centerY, targetIndicatorRadius, paintFill);
            canvas.drawCircle(centerX, centerY, targetIndicatorRadius, paintStroke);
        }
    }

    public Drawable getProgressDrawable() {
        RoundRectShape progressRect = new RoundRectShape(barDrawableCornerRadii, null, null);
        ShapeDrawable progressDrawable = new ShapeDrawable(progressRect);
        progressDrawable.getPaint().setColor(progressColor);

        RoundRectShape backgroundRect = new RoundRectShape(barDrawableCornerRadii, null, null);
        ShapeDrawable backgroundDrawable = new ShapeDrawable(backgroundRect);
        backgroundDrawable.getPaint().setColor(trackColor);

        ClipDrawable progressClip = new ClipDrawable(progressDrawable, Gravity.LEFT|Gravity.BOTTOM, isVertical ? ClipDrawable.VERTICAL : ClipDrawable.HORIZONTAL);

        Drawable[] drawables = {backgroundDrawable, progressClip};
        LayerDrawable layerdrawable = new LayerDrawable(drawables);

        layerdrawable.setId(0, android.R.id.background);
        layerdrawable.setId(1, android.R.id.progress);

        return layerdrawable;
    }

    private float dpToPixel(float value) {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, value, getResources().getDisplayMetrics());
    }
}
