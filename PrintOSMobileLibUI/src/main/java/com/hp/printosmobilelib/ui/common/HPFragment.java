package com.hp.printosmobilelib.ui.common;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;

/**
 * A Base fragment for HP fragments.
 *
 * @author Osama Taha
 */
public abstract class HPFragment extends Fragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View ret= inflater.inflate(getLayoutResource(), container, false);
        ButterKnife.bind(this, ret);
        return ret;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    public String getStringResource (int id) {
        if(getActivity() != null && isAdded()) {
            getString(id);
        }
        return "";
    }

    public String getToolbarDisplayNameExtra(){
        return "";
    }

    protected abstract int getLayoutResource();

    public boolean onBackPressed(){
        return false;
    }

    public abstract boolean isFragmentNameToBeDisplayed();
    public abstract boolean isFilteringAvailable();
    public abstract int getToolbarDisplayName();

    public boolean isIntercomAccessible() {
        return false;
    }
}
