package com.hp.printosmobilelib.core.utils;

import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Environment;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

import com.hp.printosmobilelib.core.logging.HPLogger;

import java.io.File;
import java.net.NetworkInterface;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

/**
 * Created by Osama Taha
 */
public class DeviceUtils {

    private static final String TAG = DeviceUtils.class.getSimpleName();

    private static String uniqueID = null;
    private static final String PREF_UNIQUE_ID = "PREF_UNIQUE_ID";
    private static final String UNKNOWN_DEVICE_ID = "Unknown";
    private static final String DEVICE_ID_CACHE_NAME = "printos.txt";

    public synchronized static String getDeviceIdMD5(Context context) {

        if (uniqueID == null) {

            SharedPreferences sharedPrefs = context.getSharedPreferences(PREF_UNIQUE_ID, Context.MODE_PRIVATE);
            uniqueID = sharedPrefs.getString(PREF_UNIQUE_ID, null);

            if (uniqueID == null) {

                String cachedDeviceId = getCachedDeviceId();
                if (cachedDeviceId == null || TextUtils.isEmpty(cachedDeviceId)) {
                    String deviceId = getDeviceId(context);
                    uniqueID = deviceId.equals(UNKNOWN_DEVICE_ID) ? UNKNOWN_DEVICE_ID
                            : getEncryptedDeviceId(deviceId);
                    HPLogger.logD(TAG, "Generate a new device token " + uniqueID);
                } else {
                    HPLogger.logD(TAG, "Read the device token from the cache " + cachedDeviceId);
                    uniqueID = cachedDeviceId;
                }

                cacheDeviceId(uniqueID);

                Editor editor = sharedPrefs.edit();
                editor.putString(PREF_UNIQUE_ID, uniqueID);
                editor.commit();

            }
        }

        return uniqueID;
    }

    public static void cacheDeviceId(Context context) {
        HPLogger.logD(TAG, "cache the device token.");
        cacheDeviceId(getDeviceIdMD5(context));
    }

    private static void cacheDeviceId(String deviceId) {
        boolean result = FileUtils.writeFile(getCacheFilePath(), deviceId, false);
        if (result) {
            HPLogger.logD(TAG, "Device id has been successfully cached.");
        } else {
            HPLogger.logD(TAG, "Unable to cache the device id.");
        }
    }

    private static String getEncryptedDeviceId(String deviceId) {
        String encryptedId = EncryptionUtils.md5(deviceId);
        return (encryptedId == null || TextUtils.isEmpty(encryptedId)) ? UNKNOWN_DEVICE_ID : encryptedId;
    }

    /**
     * Get unique device id derived from the Wi-Fi MAC address
     * (if null, use the IMEI, if IMEI is null, use unique Random ID,
     * if unique Id is null use “Unknown”.
     */
    public synchronized static String getDeviceId(Context context) {

        try {

            String wifiMac = getWifiMacAddress(context);
            if (!TextUtils.isEmpty(wifiMac)) {
                HPLogger.logD(TAG, "DeviceId(WifiMac): " + wifiMac);
                return wifiMac;
            }

            String androidId = getAndroidId(context);
            if (!TextUtils.isEmpty(androidId)) {
                HPLogger.logD(TAG, "DeviceId(AndroidId): " + androidId);
                return androidId;
            }

            // Random UUID
            String uuid = getUniqueId();
            if (!TextUtils.isEmpty(uuid)) {
                HPLogger.logD(TAG, "DeviceId(UUID): " + uuid);
                return uuid;
            }

        } catch (Exception e) {
            return UNKNOWN_DEVICE_ID;
        }

        return UNKNOWN_DEVICE_ID;

    }

    public static String getUniqueId() {
        UUID uuid = UUID.randomUUID();
        return uuid == null ? null : uuid.toString();
    }

    private static String getCachedDeviceId() {

        StringBuilder stringBuilder = FileUtils.readFile(getCacheFilePath());
        return stringBuilder == null ? null : stringBuilder.toString();
    }

    private static String getCacheFilePath() {
        return Environment.getExternalStorageDirectory().toString()
                + File.separator + DEVICE_ID_CACHE_NAME;
    }

    public static String getIMEI(Context context) {

        boolean hasPermission = context.checkCallingOrSelfPermission(Manifest.permission.READ_PHONE_STATE)
                == PackageManager.PERMISSION_GRANTED;

        String deviceId = null;
        if (hasPermission) {
            TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            deviceId = telephonyManager == null ? null : telephonyManager.getDeviceId();
        }

        return deviceId;
    }

    public static String getWifiMacAddress(Context context) {

        try {

            boolean hasPermission = context.checkCallingOrSelfPermission(Manifest.permission.ACCESS_WIFI_STATE)
                    == PackageManager.PERMISSION_GRANTED;

            String macAddress = null;

            if (hasPermission) {
                WifiManager wifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
                WifiInfo info = wifi == null ? null : wifi.getConnectionInfo();
                macAddress = info == null ? null : info.getMacAddress();
            }

            if (macAddress == null || macAddress.equals("02:00:00:00:00:00")) {

                List<NetworkInterface> networkInterfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
                for (NetworkInterface networkInterface : networkInterfaces) {

                    if (!networkInterface.getName().equalsIgnoreCase("wlan0")) {
                        continue;
                    }

                    byte[] hardwareAddress = networkInterface.getHardwareAddress();
                    if (hardwareAddress == null) {
                        return "";
                    }

                    StringBuilder stringBuilder = new StringBuilder();
                    for (byte b : hardwareAddress) {
                        stringBuilder.append(String.format("%02X:", b));
                    }

                    if (stringBuilder.length() > 0) {
                        stringBuilder.deleteCharAt(stringBuilder.length() - 1);
                    }

                    return stringBuilder.toString();
                }
            }

        } catch (Exception e) {
            HPLogger.e(TAG, "Error reading the mac address", e);
        }

        return "";
    }

    public static String getIPAddress(Context context) {

        boolean hasPermission = context.checkCallingOrSelfPermission(Manifest.permission.ACCESS_WIFI_STATE)
                == PackageManager.PERMISSION_GRANTED;

        if (hasPermission) {
            WifiManager wifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
            WifiInfo info = wifi == null ? null : wifi.getConnectionInfo();
            int ipAddress = info == null ? 0 : info.getIpAddress();

            return ipAddress == 0 ? "" : String.format("%d.%d.%d.%d",
                    (ipAddress & 0xff),
                    (ipAddress >> 8 & 0xff),
                    (ipAddress >> 16 & 0xff),
                    (ipAddress >> 24 & 0xff));

        }

        return null;
    }

    public static String getAndroidId(Context context) {
        try {
            return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
        } catch (Exception e) {
            return "";
        }
    }
}