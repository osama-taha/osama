package com.hp.printosmobilelib.core.communications.remote.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.io.IOException;
import java.io.Serializable;

/**
 * Created by Osama Taha on 9/8/16.
 */
public class AAAError implements Serializable{

    @JsonProperty("smsError")
    private SmsError smsError;

    /**
     * @return The smsError
     */
    @JsonProperty("smsError")
    public SmsError getSmsError() {
        return smsError;
    }

    /**
     * @param smsError The smsError
     */
    @JsonProperty("smsError")
    public void setSmsError(SmsError smsError) {
        this.smsError = smsError;
    }


    public class SmsError implements Serializable{

        @JsonProperty("statusCode")
        private Integer statusCode;
        @JsonProperty("message")
        private String message;
        @JsonProperty("moreInfoUrl")
        private Object moreInfoUrl;
        @JsonProperty("developerInfo")
        private Object developerInfo;
        @JsonProperty("subCode")
        @JsonDeserialize(using = SubCodeDeserializer.class)
        private ErrorSubCode subCode;
        @JsonProperty("serviceName")
        private String serviceName;

        /**
         * @return The statusCode
         */
        @JsonProperty("statusCode")

        public Integer getStatusCode() {
            return statusCode;
        }

        /**
         * @param statusCode The statusCode
         */
        @JsonProperty("statusCode")
        public void setStatusCode(Integer statusCode) {
            this.statusCode = statusCode;
        }

        /**
         * @return The message
         */
        @JsonProperty("message")
        public String getMessage() {
            return message;
        }

        /**
         * @param message The message
         */
        @JsonProperty("message")
        public void setMessage(String message) {
            this.message = message;
        }

        /**
         * @return The moreInfoUrl
         */
        @JsonProperty("moreInfoUrl")
        public Object getMoreInfoUrl() {
            return moreInfoUrl;
        }

        /**
         * @param moreInfoUrl The moreInfoUrl
         */
        @JsonProperty("moreInfoUrl")
        public void setMoreInfoUrl(Object moreInfoUrl) {
            this.moreInfoUrl = moreInfoUrl;
        }

        /**
         * @return The developerInfo
         */
        @JsonProperty("developerInfo")
        public Object getDeveloperInfo() {
            return developerInfo;
        }

        /**
         * @param developerInfo The developerInfo
         */
        @JsonProperty("developerInfo")
        public void setDeveloperInfo(Object developerInfo) {
            this.developerInfo = developerInfo;
        }

        /**
         * @return The subCode
         */
        @JsonProperty("subCode")
        @JsonDeserialize(using = SubCodeDeserializer.class)
        public ErrorSubCode getSubCode() {
            return subCode;
        }

        /**
         * @param subCode The subCode
         */
        @JsonProperty("subCode")
        @JsonDeserialize(using = SubCodeDeserializer.class)
        public void setSubCode(ErrorSubCode subCode) {
            this.subCode = subCode;
        }

        /**
         * @return The serviceName
         */
        @JsonProperty("serviceName")
        public String getServiceName() {
            return serviceName;
        }

        /**
         * @param serviceName The serviceName
         */
        @JsonProperty("serviceName")
        public void setServiceName(String serviceName) {
            this.serviceName = serviceName;
        }

        @Override
        public String toString() {
            return "SmsError{" +
                    "statusCode=" + statusCode +
                    ", message='" + message + '\'' +
                    ", moreInfoUrl=" + moreInfoUrl +
                    ", developerInfo=" + developerInfo +
                    ", subCode=" + subCode +
                    ", serviceName='" + serviceName + '\'' +
                    '}';
        }
    }


    @Override
    public String toString() {
        return "APIError{" +
                "smsError=" + smsError +
                '}';
    }

    public enum ErrorSubCode {

        EULA_NOT_ACCEPTED,
        PASSWORD_EXPIRED,
        ACCOUNT_LOCKED,
        INVALID_USER_NAME_PASSWORD,
        ACCESS_DENIED,
        UNKNOWN;

        ErrorSubCode() {
        }

        public static ErrorSubCode fromTypeCode(final int typeCode) {

            switch (typeCode) {
                case 1021:
                case 1000:
                    return EULA_NOT_ACCEPTED;
                case 1001:
                    return PASSWORD_EXPIRED;
                case 1016:
                case 1003:
                    return ACCOUNT_LOCKED;
                case 1008:
                    return INVALID_USER_NAME_PASSWORD;
                case 0:
                    return ACCESS_DENIED;
            }

            return UNKNOWN;
        }
    }

    public static class SubCodeDeserializer extends JsonDeserializer<ErrorSubCode> {
        @Override
        public ErrorSubCode deserialize(final JsonParser parser, final DeserializationContext context) throws IOException {
            return ErrorSubCode.fromTypeCode(parser.getValueAsInt());
        }
    }

}
