package com.hp.printosmobilelib.core.logging;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.hp.printosmobilelib.core.utils.HPDateUtils;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by Osama Taha
 */
public class HPLogEvent implements Serializable {

    @JsonProperty("time")
    private String time;
    @JsonProperty("level")
    private String level;
    @JsonProperty("loggerName")
    private String loggerName;
    @JsonProperty("msg")
    private String message;
    @JsonIgnore
    private Date date;
    @JsonIgnore
    private HPLogQueue.LogType type;

    public HPLogEvent() {
    }

    public HPLogEvent(HPLogQueue.LogType type, final String level, final String loggerName, final String message) {
        this.type = type;
        this.date = new Date();
        this.time = HPDateUtils.getCurrentUTCDateTimeAsString(date);
        this.level = level;
        this.loggerName = loggerName;
        this.message = message;
    }

    @JsonProperty("level")
    public void setLevel(String level) {
        this.level = level;
    }

    @JsonProperty("loggerName")
    public void setLoggerName(String loggerName) {
        this.loggerName = loggerName;
    }

    @JsonProperty("msg")
    public void setMessage(String message) {
        this.message = message;
    }

    @JsonProperty("time")
    public void setTime(String time) {
        this.time = time;
    }

    @JsonProperty("time")
    public String getTime() {
        return time;
    }

    @JsonProperty("level")
    public String getLevel() {
        return level;
    }

    @JsonProperty("loggerName")
    public String getLoggerName() {
        return loggerName;
    }

    @JsonProperty("msg")
    public String getMessage() {
        return message;
    }

    public void setType(HPLogQueue.LogType type) {
        this.type = type;
    }

    public HPLogQueue.LogType getType() {
        return type;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getDate() {
        return date;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("HPLogEvent{");
        sb.append("time='").append(time).append('\'');
        sb.append(", level='").append(level).append('\'');
        sb.append(", loggerName='").append(loggerName).append('\'');
        sb.append(", message='").append(message).append('\'');
        sb.append(", type=").append(type);
        sb.append('}');
        return sb.toString();
    }
}