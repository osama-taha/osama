package com.hp.printosmobilelib.core.utils;

import android.content.Context;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.util.Log;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

/**
 * HPDateUtils contains a lot of methods considering manipulations of Dates or Calendars.
 *
 * @Author: Osama Taha
 * Created on 6/20/2015.
 */
public class HPDateUtils {

    private static final String TAG = HPDateUtils.class.getSimpleName();

    public static final String DATE_TIME_STRING_FORMAT = "%s  %s";
    public static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    public static final String TIME_ZONE_UTC = "UTC";
    public static final int DAYS_IN_WEEK = 7;
    public static final int WEEKS_IN_MONTH = 4;
    public static final int MONTHS_IN_YEAR = 12;

    /**
     * convert the date from one format to another date object in another format without.
     *
     * @param dateString     - date string.
     * @param originalFormat - date format.
     * @param targetFormat   - target format.
     * @return - date string.
     */
    public static String formatDate(String dateString, String originalFormat, String targetFormat) {

        Date date = parseDate(dateString, originalFormat);
        SimpleDateFormat formatter = new SimpleDateFormat(targetFormat);
        String formattedDate = formatter.format(date);
        return formattedDate;
    }

    /**
     * format a date object to string with a specific format.
     */
    public static String formatDate(Date date, String targetFormat, TimeZone timeZone) {

        SimpleDateFormat formatter = new SimpleDateFormat(targetFormat);
        if (timeZone != null) {
            formatter.setTimeZone(timeZone);
        }
        String formattedDate = formatter.format(date);
        return formattedDate;
    }

    public static int getDateOfMonth(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return calendar.get(Calendar.DAY_OF_MONTH);
    }

    /**
     * Returns a date range string given two dates ( start and end ).
     * This method was added in order to manage the different cases of Data ranges between the different Locales.
     * Each language has a different rule based on the requirement received.
     *
     * @param startDate - date from.
     * @param endDate   - date to.
     */
    public static String getDateRangeString(String dateFormat, String separator, Date startDate, Date endDate) {

        String startDateFormat = StringUtils.getSubStringBeforeSeparator(dateFormat, separator);
        String endDateFormat = StringUtils.getSubStringAfterSeparator(dateFormat, separator);

        SimpleDateFormat dateFormatter = new SimpleDateFormat();
        dateFormatter.applyLocalizedPattern(startDateFormat);
        String dateFrom = dateFormatter.format(startDate);

        dateFormatter.applyLocalizedPattern(endDateFormat);
        String dateTo = dateFormatter.format(endDate);
        String dateRangeString = dateFrom + separator + dateTo;

        return dateRangeString;
    }

    public static String getCurrentTime(Context context) {

        SimpleDateFormat timeFormatter = (SimpleDateFormat) android.text.format.DateFormat.getTimeFormat(context);

        return timeFormatter.format(Calendar.getInstance().getTime());

    }

    public static String getTime(Context context, Date date, TimeZone timeZone) {

        SimpleDateFormat timeFormatter = (SimpleDateFormat) android.text.format.DateFormat.getTimeFormat(context);
        if (timeZone != null) {
            timeFormatter.setTimeZone(timeZone);
        }
        return timeFormatter.format(date);

    }

    public static String getDateTimeString(Context context, Date date, String dateFormat, TimeZone timeZone) {
        if (context == null || date == null) {
            return "";
        }

        String timeString = getTime(context, date, timeZone);
        String dateString = HPDateUtils.formatDate(date, dateFormat, timeZone);

        return String.format(DATE_TIME_STRING_FORMAT, dateString, timeString);
    }

    public static String getCurrentUTCDateTimeAsString(Date date) {

        final SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
        dateFormat.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_UTC));
        final String utcDateTime = dateFormat.format(date);

        return utcDateTime;
    }


    /**
     * <p>Checks if two dates are on the same day ignoring time.</p>
     *
     * @param date1 the first date, not altered, not null
     * @param date2 the second date, not altered, not null
     * @return true if they represent the same day
     * @throws IllegalArgumentException if either date is <code>null</code>
     */
    public static boolean isSameDay(Date date1, Date date2) {
        if (date1 == null || date2 == null) {
            return false;
            //throw new IllegalArgumentException("The dates must not be null");
        }
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);
        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);

        return (cal1.get(Calendar.ERA) == cal2.get(Calendar.ERA) &&
                cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
                cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR));
    }


    /**
     * <p>Checks if a date is today.</p>
     *
     * @param date the date, not altered, not null.
     * @return true if the date is today.
     * @throws IllegalArgumentException if the date is <code>null</code>
     */
    public static boolean isToday(Date date) {
        return isSameDay(date, Calendar.getInstance().getTime());
    }

    public static long getDifferenceDays(Date d1, Date d2) {
        long diff = d2.getTime() - d1.getTime();
        return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
    }

    /**
     * Returns true if date is yesterday.
     *
     * @param date
     * @return true if date is yesterday
     */
    public static boolean isYesterday(Date date, TimeZone timezone) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.DATE, -1);
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
        if (timezone != null) {
            df.setTimeZone(timezone);
        }
        return df.format(cal.getTime()).equals(df.format(date));
    }


    /**
     * Get the week number for an input date.
     *
     * @param date       - date string.
     * @param dateFormat - date format.
     */
    public static int getWeekOfYear(String date, String dateFormat) {

        Calendar calendar = Calendar.getInstance();
        //this is how print-beat works
        calendar.setMinimalDaysInFirstWeek(4);

        DateFormat formatter = new SimpleDateFormat(dateFormat);
        int week = 0;
        try {
            calendar.setTime(formatter.parse(date));
            week = calendar.get(Calendar.WEEK_OF_YEAR);
        } catch (ParseException e) {
        }
        return week;
    }

    public static Date parseDate(String dateString, String dateFormat) {

        return parseDate(dateString, dateFormat, null);
    }

    /**
     * Turn a date string to a Date object.
     *
     * @param dateString - date in string.
     * @param dateFormat - date format.
     * @param timeZone
     * @return - Date object.
     */
    public static Date parseDate(String dateString, String dateFormat, TimeZone timeZone) {

        if (TextUtils.isEmpty(dateFormat)) {
            return null;
        }

        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
        if (timeZone != null) {
            formatter.setTimeZone(timeZone);
        }

        Date date = null;
        try {
            date = formatter.parse(dateString);
            return date;
        } catch (Exception e) {
        }
        return null;
    }

    /**
     * format a date object to string with a specific format.
     */
    public static String formatDate(Date date, String targetFormat) {

        SimpleDateFormat formatter = new SimpleDateFormat(targetFormat);
        String formattedDate = formatter.format(date);
        return formattedDate;
    }

    /**
     * Get a new date object by adding a month/s,year/s,day/s, or week/s value to an input date.
     *
     * @param date  - date to get new date from.
     * @param unit  - Represent a conversion unit: @Calendar.WEEK,Calendar.Date,etc..
     * @param value - Value to be added the current date based on its unit.
     * @return - new date.
     */
    public static Date getDate(Date date, int unit, int value) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(unit, value);
        return calendar.getTime();
    }

    public static int monthDifference(Calendar c1, Calendar c2) {

        int diffYear = c2.get(Calendar.YEAR) - c1.get(Calendar.YEAR);
        return diffYear * MONTHS_IN_YEAR + c2.get(Calendar.MONTH) - c1.get(Calendar.MONTH);
    }

    public static int getDaysDiff(Calendar c1, Calendar c2) {
        long diff = c2.getTimeInMillis() - c1.getTimeInMillis();
        return (int) (diff / (DateUtils.DAY_IN_MILLIS));
    }

    public static float getDaysDiff(long time1, long time2) {
        return (float) (time2 - time1) / DateUtils.DAY_IN_MILLIS;
    }

    public static Date getDateFromTimeStamp(long timestamp) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(timestamp);
        return calendar.getTime();
    }


    public static int getNumberOfDaysFromToday(Date expiryDate) {
        Calendar today = Calendar.getInstance();
        Calendar expiry = Calendar.getInstance();
        expiry.setTime(expiryDate);

        long msDiff = expiry.getTimeInMillis() - today.getTimeInMillis();
        return (int) TimeUnit.MILLISECONDS.toDays(msDiff);
    }

    public static int getWeek() {
        Calendar calendar = Calendar.getInstance();
        calendar.setMinimalDaysInFirstWeek(4);
        return calendar.get(Calendar.WEEK_OF_YEAR);
    }

    public static int getYear() {
        return Calendar.getInstance().get(Calendar.YEAR);
    }

    public static boolean isExpired(Date expiryDate) {
        Calendar today = Calendar.getInstance();
        return expiryDate.getTime() < today.getTime().getTime();
    }
}
