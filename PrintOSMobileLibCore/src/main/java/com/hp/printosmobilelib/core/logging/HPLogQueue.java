package com.hp.printosmobilelib.core.logging;

import android.content.Context;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import ch.qos.logback.classic.Level;

/**
 * Created by Osama Taha
 */

public final class HPLogQueue {

    private static final String TAG = HPLogQueue.class.getSimpleName();

    private final HashMap<LogType, BlockingQueue<HPLogEvent>> queuedEventsMap = new HashMap<>();

    private final Context context;

    private HPLogQueue(Context context) {
        this.context = context;

        for (LogType type : LogType.values()) {

            List<HPLogEvent> cachedEvents = LogCache.getCachedLogs(context, type);

            if (cachedEvents != null) {

                HPLogger.logD(TAG, "number of " + type.getLogCacheName() + " cached items " + cachedEvents.size());

                BlockingQueue<HPLogEvent> logEventBlockingQueue = new LinkedBlockingQueue<>();
                logEventBlockingQueue.addAll(cachedEvents);
                queuedEventsMap.put(type, logEventBlockingQueue);
            }
        }
    }

    private static HPLogQueue INSTANCE = null;

    public static synchronized HPLogQueue getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = new HPLogQueue(context);
        }
        return INSTANCE;
    }

    public void enqueueLog(LogType logType, final Level level, final String tag,
                           final String message) {

        final int batchSize = HPLogConfig.getInstance(context).getBatchSize();
        final int preloginMaxSize = HPLogConfig.getInstance(context).getPreloginMaxSize();

        HPLogEvent logEvent = null;

        BlockingQueue<HPLogEvent> queuedEvents = queuedEventsMap.get(logType);

        if (logType == LogType.AUTHENTICATED_LOG) {

            logEvent = new HPLogEvent(LogType.AUTHENTICATED_LOG, level.levelStr, tag, message);
            HPLogConfig.getInstance(context).getLogEventFormatter().format(logEvent, LogType.AUTHENTICATED_LOG);

            queuedEvents.add(logEvent);

            if (queuedEvents.size() >= batchSize) {

                flushQueue(LogType.AUTHENTICATED_LOG, batchSize);
                LogCache.clearCache(context, LogType.AUTHENTICATED_LOG);

            } else {

                List<HPLogEvent> logEventBatch = getLogs(LogType.AUTHENTICATED_LOG);
                LogCache.cacheLogs(context, Collections.unmodifiableList(logEventBatch)
                        , LogType.AUTHENTICATED_LOG);
            }

        } else if (logType == LogType.PRE_LOGIN) {

            logEvent = new HPLogEvent(LogType.PRE_LOGIN, level.levelStr, tag, message);
            HPLogConfig.getInstance(context).getLogEventFormatter().format(logEvent, LogType.PRE_LOGIN);

            queuedEvents.add(logEvent);

            if (queuedEvents.size() >= preloginMaxSize) {
                queuedEvents.remove();
            }

            List<HPLogEvent> logEventBatch = getLogs(LogType.PRE_LOGIN);

            List<HPLogEvent> hpLogEvents = Collections.unmodifiableList(logEventBatch);
            LogCache.cacheLogs(context, hpLogEvents, LogType.PRE_LOGIN);
        }
    }

    private void flushQueue(LogType type, final int batchSize) {

        HPLogger.logD(TAG, "flush logs " + type.name());

        List<HPLogEvent> logEventBatch = new ArrayList<>();

        BlockingQueue<HPLogEvent> queuedEvents = queuedEventsMap.get(type);

        queuedEvents.drainTo(logEventBatch, batchSize);

        if (!logEventBatch.isEmpty()) {

            HPLogger.logD(TAG, "Dispatch " + logEventBatch.size() + " log events.");

            List<HPLogEvent> hpLogEvents = Collections.unmodifiableList(logEventBatch);

            HPLogConfig.getInstance(context).getLogDispatcher().dispatchLogs(type, hpLogEvents);
        }
    }

    public void flushPreLoginLogs() {

        BlockingQueue<HPLogEvent> queuedEvents = queuedEventsMap.get(LogType.PRE_LOGIN);

        if (queuedEvents.size() > 0) {
            flushQueue(LogType.PRE_LOGIN, queuedEvents.size());
        }
    }

    public void flushLogs() {
        BlockingQueue<HPLogEvent> queuedEvents = queuedEventsMap.get(LogType.AUTHENTICATED_LOG);
        if (queuedEvents.size() > 0) {
            flushQueue(LogType.AUTHENTICATED_LOG, queuedEvents.size());
        }
    }


    public void enqueueFailedLogs(List<HPLogEvent> logEvents) {

        BlockingQueue<HPLogEvent> queuedEvents = queuedEventsMap.get(LogType.FAILED);

        queuedEvents.addAll(logEvents);

        while (queuedEvents.size() > HPLogConfig.getInstance(context).getFailedLogsMaxSize()) {
            queuedEvents.remove();
        }

        List<HPLogEvent> allLogEvents = getLogs(LogType.FAILED);
        LogCache.cacheLogs(context, allLogEvents, LogType.FAILED);

    }

    public List<HPLogEvent> getLogs(LogType logType) {

        BlockingQueue<HPLogEvent> queuedEvents = queuedEventsMap.get(logType);
        List<HPLogEvent> allQueuedEvents = new ArrayList<>();
        for (HPLogEvent log : queuedEvents) {
            allQueuedEvents.add(log);
        }

        return allQueuedEvents;
    }

    public void clearLogCache(LogType type) {

        HPLogger.logD(TAG, "clear cache for " + type.getLogCacheName());

        BlockingQueue<HPLogEvent> queuedEvents = queuedEventsMap.get(type);
        queuedEvents.clear();
        LogCache.clearCache(context, type);
    }

    public enum LogType {

        PRE_LOGIN("Prelogin", "PreLoginCache"), AUTHENTICATED_LOG("", "AfterLoginCache"), FAILED("", "FailedLogCache");

        private final String logMsgPrefix;
        private final String logCacheName;

        LogType(String logMsgPrefix, String cacheName) {
            this.logMsgPrefix = logMsgPrefix;
            this.logCacheName = cacheName;
        }

        public String getLogMsgPrefix() {
            return logMsgPrefix;
        }

        public String getLogCacheName() {
            return logCacheName;
        }

    }
}