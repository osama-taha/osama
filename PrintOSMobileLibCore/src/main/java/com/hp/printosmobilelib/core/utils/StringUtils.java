package com.hp.printosmobilelib.core.utils;

/**
 * Created by user on 5/15/2016.
 */
public class StringUtils {

    public static final String PERCENT_SYMBOL = "%";
    public static final String ELLIPSIS = "...";

    /**
     * Returns a substring until the index of a given separator.
     */
    public static String getSubStringBeforeSeparator(String originalString, String separator) {

        if (originalString.indexOf(separator) > 0) {
            return originalString.substring(0, originalString.indexOf(separator));
        }

        return originalString;
    }

    /**
     * Returns a string containing a suffix of {@code originalString} starting from the end of a given "separator".
     */
    public static String getSubStringAfterSeparator(String originalString, String separator) {

        if (originalString.indexOf(separator) > 0) {
            return originalString.substring(originalString.lastIndexOf(separator) + separator.length());
        }

        return originalString;
    }
}
