package com.hp.printosmobilelib.core.logging;

import android.content.Context;

import com.hp.printosmobilelib.core.communications.remote.SessionManager;
import com.hp.printosmobilelib.core.communications.remote.services.LogDispatchService;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Osama Taha
 */
public class HPLogDispatcher {

    private static final String TAG = HPLogDispatcher.class.getSimpleName();

    private Context context;
    private LogDispatchService logDispatchService;

    public HPLogDispatcher(Context context, LogDispatchService logDispatchService) {
        this.context = context;
        this.logDispatchService = logDispatchService;
    }

    public synchronized void dispatchLogs(final HPLogQueue.LogType type, final List<HPLogEvent> logEventList) {

        HPLogger.logD(TAG, String.format("Sending %d - %s logs to server", logEventList.size(), type.getLogMsgPrefix()));

        Runnable sendLogs = new Runnable() {
            @Override
            public void run() {

                try {

                    if (!SessionManager.getInstance(context).hasCookie() ||
                            !HPLogConfig.getInstance(context).isDispatchingEnabled()){
                        HPLogQueue.getInstance(context).enqueueFailedLogs(logEventList);
                        return;
                    }

                    Call<ResponseBody> call = logDispatchService.sendLogsToServer(logEventList);

                    Response<ResponseBody> response = call.execute();

                    if (response.isSuccessful()) {

                        HPLogger.logD(TAG, "Sending logs to server succeeded.");

                    } else {

                        HPLogger.logD(TAG, "Try again sending logs.");

                        call = logDispatchService.sendLogsToServer(logEventList);
                        response = call.execute();

                        if (response.isSuccessful()) {
                            HPLogger.logD(TAG, "Sending logs to server succeeded after the second try.");
                        } else {
                            HPLogQueue.getInstance(context).enqueueFailedLogs(logEventList);
                            HPLogger.logD(TAG, "Failed to send logs again " + response.errorBody().string());
                        }

                    }

                    if (response.isSuccessful()) {

                        if (type == HPLogQueue.LogType.PRE_LOGIN) {
                            //Clear pre login cache.
                            HPLogger.logD(TAG, "Clear cache for prelogin logs.");
                            HPLogQueue.getInstance(context).clearLogCache(type);

                        } else {

                            //Try to send all cached logs if any.
                            sendLogs(HPLogQueue.LogType.PRE_LOGIN);
                            sendLogs(HPLogQueue.LogType.FAILED);
                        }
                    }

                    HPLogConfig.getInstance(context).getLogListener().flushLogs(logEventList);

                } catch (Exception e) {

                    HPLogger.logE(TAG, "Error sending logs to server " + type.getLogMsgPrefix(), e);

                    if (type == HPLogQueue.LogType.AUTHENTICATED_LOG) {
                        HPLogQueue.getInstance(context).enqueueFailedLogs(logEventList);
                    }

                }
            }
        };

        new Thread(sendLogs).start();

    }

    private void sendLogs(HPLogQueue.LogType logType) {

        try {

            List<HPLogEvent> logEvents = HPLogQueue.getInstance(context).getLogs(logType);

            if (logEvents.size() > 0) {

                HPLogger.logD(TAG, "Sending all " + logType.name() + " logs.");

                Call<ResponseBody> call = logDispatchService.sendLogsToServer(logEvents);
                Response<ResponseBody> response = call.execute();
                if (response.isSuccessful()) {
                    HPLogQueue.getInstance(context).clearLogCache(logType);
                }
            }

        } catch (Exception e) {
            HPLogger.logD(TAG, "Error sending " + logType.name() + " logs to server " + e);
        }


    }
}
