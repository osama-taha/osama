package com.hp.printosmobilelib.core.communications.remote.models;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.hp.printosmobilelib.core.communications.remote.services.LoginService;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * A model representing the Response of the {@link LoginService#login(String, UserCredentials)} endpoint.
 *
 * @author Osama Taha
 */
public class UserData {

    @JsonProperty("context")
    private Context context;
    @JsonProperty("type")
    private String type;
    @JsonProperty("user")
    private User user;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * @return The context
     */
    @JsonProperty("context")
    public Context getContext() {
        return context;
    }

    /**
     * @param context The context
     */
    @JsonProperty("context")
    public void setContext(Context context) {
        this.context = context;
    }

    /**
     * @return The type
     */
    @JsonProperty("type")
    public String getType() {
        return type;
    }

    /**
     * @param type The type
     */
    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return The user
     */
    @JsonProperty("user")
    public User getUser() {
        return user;
    }

    /**
     * @param user The user
     */
    @JsonProperty("user")
    public void setUser(User user) {
        this.user = user;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("UserData{");
        sb.append("context=").append(context);
        sb.append(", type='").append(type).append('\'');
        sb.append(", user=").append(user);
        sb.append(", additionalProperties=").append(additionalProperties);
        sb.append('}');
        return sb.toString();
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class User {

        @JsonProperty("expireTime")
        private String expireTime;
        @JsonProperty(value = "primaryPhone")
        private String primaryPhone;
        @JsonProperty("lastName")
        private String lastName;
        @JsonProperty("invalidAttempts")
        private Integer invalidAttempts;
        @JsonProperty("createdTime")
        private Long createdTime;
        @JsonProperty("userId")
        private String userId;
        @JsonProperty("locked")
        private Boolean locked;
        @JsonProperty("displayName")
        private String displayName;
        @JsonProperty("firstName")
        private String firstName;
        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<String, Object>();

        /**
         * @return The primaryPhone
         */
        @JsonProperty(value = "primaryPhone", required = false)
        public String getPrimaryPhone() {
            return primaryPhone;
        }

        /**
         * @param primaryPhone The expireTime
         */
        @JsonProperty(value = "primaryPhone", required = false)
        public void setPrimaryPhone(String primaryPhone) {
            this.primaryPhone = primaryPhone;
        }

        /**
         * @return The expireTime
         */
        @JsonProperty("expireTime")
        public String getExpireTime() {
            return expireTime;
        }

        /**
         * @param expireTime The expireTime
         */
        @JsonProperty("expireTime")
        public void setExpireTime(String expireTime) {
            this.expireTime = expireTime;
        }

        /**
         * @return The lastName
         */
        @JsonProperty("lastName")
        public String getLastName() {
            return lastName;
        }

        /**
         * @param lastName The lastName
         */
        @JsonProperty("lastName")
        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        /**
         * @return The invalidAttempts
         */
        @JsonProperty("invalidAttempts")
        public Integer getInvalidAttempts() {
            return invalidAttempts;
        }

        /**
         * @param invalidAttempts The invalidAttempts
         */
        @JsonProperty("invalidAttempts")
        public void setInvalidAttempts(Integer invalidAttempts) {
            this.invalidAttempts = invalidAttempts;
        }

        /**
         * @return The createdTime
         */
        @JsonProperty("createdTime")
        public Long getCreatedTime() {
            return createdTime;
        }

        /**
         * @param createdTime The createdTime
         */
        @JsonProperty("createdTime")
        public void setCreatedTime(Long createdTime) {
            this.createdTime = createdTime;
        }

        /**
         * @return The userId
         */
        @JsonProperty("userId")
        public String getUserId() {
            return userId;
        }

        /**
         * @param userId The userId
         */
        @JsonProperty("userId")
        public void setUserId(String userId) {
            this.userId = userId;
        }

        /**
         * @return The locked
         */
        @JsonProperty("locked")
        public Boolean getLocked() {
            return locked;
        }

        /**
         * @param locked The locked
         */
        @JsonProperty("locked")
        public void setLocked(Boolean locked) {
            this.locked = locked;
        }

        /**
         * @return The displayName
         */
        @JsonProperty("displayName")
        public String getDisplayName() {
            return displayName;
        }

        /**
         * @param displayName The displayName
         */
        @JsonProperty("displayName")
        public void setDisplayName(String displayName) {
            this.displayName = displayName;
        }

        /**
         * @return The firstName
         */
        @JsonProperty("firstName")
        public String getFirstName() {
            return firstName;
        }

        /**
         * @param firstName The firstName
         */
        @JsonProperty("firstName")
        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }

        @Override
        public String toString() {
            final StringBuffer sb = new StringBuffer("User{");
            sb.append("expireTime='").append(expireTime).append('\'');
            sb.append(", lastName='").append(lastName).append('\'');
            sb.append(", invalidAttempts=").append(invalidAttempts);
            sb.append(", createdTime=").append(createdTime);
            sb.append(", userId='").append(userId).append('\'');
            sb.append(", locked=").append(locked);
            sb.append(", displayName='").append(displayName).append('\'');
            sb.append(", firstName='").append(firstName).append('\'');
            sb.append(", additionalProperties=").append(additionalProperties);
            sb.append('}');
            return sb.toString();
        }
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonPropertyOrder({
            "type",
            "id",
            "name"
    })
    public static class Context implements Serializable {

        @JsonProperty("type")
        private String type;
        @JsonProperty("id")
        private String id;
        @JsonProperty("name")
        private String name;
        @JsonIgnore
        private Map<String, Object> additionalProperties = new HashMap<String, Object>();

        /**
         * @return The type
         */
        @JsonProperty("type")
        public String getType() {
            return type;
        }

        /**
         * @param type The type
         */
        @JsonProperty("type")
        public void setType(String type) {
            this.type = type;
        }

        /**
         * @return The id
         */
        @JsonProperty("id")
        public String getId() {
            return id;
        }

        /**
         * @param id The id
         */
        @JsonProperty("id")
        public void setId(String id) {
            this.id = id;
        }

        /**
         * @return The name
         */
        @JsonProperty("name")
        public String getName() {
            return name;
        }

        /**
         * @param name The name
         */
        @JsonProperty("name")
        public void setName(String name) {
            this.name = name;
        }

        @JsonAnyGetter
        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        @JsonAnySetter
        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }

        @Override
        public String toString() {
            final StringBuffer sb = new StringBuffer("Context{");
            sb.append("type='").append(type).append('\'');
            sb.append(", id='").append(id).append('\'');
            sb.append(", name='").append(name).append('\'');
            sb.append(", additionalProperties=").append(additionalProperties);
            sb.append('}');
            return sb.toString();
        }

    }
}