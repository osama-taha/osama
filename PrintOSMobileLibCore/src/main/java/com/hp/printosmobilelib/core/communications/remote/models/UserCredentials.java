package com.hp.printosmobilelib.core.communications.remote.models;

import com.hp.printosmobilelib.core.communications.remote.services.LoginService;

/**
 * A model representing the request body of the {@link LoginService#login(String, UserCredentials)} endpoint.
 *
 * @author Osama Taha
 */
public class UserCredentials {

    private String login;
    private String password;
    private String eulaVersion;

    public UserCredentials(String login, String password, String eulaVersion) {
        this.login = login;
        this.password = password;
        this.eulaVersion = eulaVersion;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public String getEulaVersion() {
        return eulaVersion;
    }

    @Override
    public String toString() {
        return "UserCredentials{" +
                "login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", eulaVersion='" + eulaVersion + '\'' +
                '}';
    }

}
