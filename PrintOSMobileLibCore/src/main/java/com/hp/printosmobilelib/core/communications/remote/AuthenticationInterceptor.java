package com.hp.printosmobilelib.core.communications.remote;

import com.hp.printosmobilelib.core.logging.HPLogger;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * This interceptor put all the required headers/cookies in the request and manage renewing the cookie.
 *
 * @author Osama Taha
 */
public class AuthenticationInterceptor implements Interceptor {

    private static final String TAG = AuthenticationInterceptor.class.getSimpleName();

    private final String COOKIE_REQUEST_HEADER = "Cookie";
    private final String SET_COOKIE_RESPONSE_HEADER = "Set-Cookie";

    private final SessionManager sessionManager;

    public AuthenticationInterceptor(SessionManager sessionManager) {
        this.sessionManager = sessionManager;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {

        Request request = chain.request();
        Request modifiedRequest = request;

        HPLogger.d(TAG, "start request (" + getRequestUrl(request) + ")");

        if (sessionManager.hasCookie()) {

            modifiedRequest = request.newBuilder()
                    .addHeader(COOKIE_REQUEST_HEADER, sessionManager.getCookie())
                    .build();
        }

        Response response = chain.proceed(modifiedRequest);

        if (response.isSuccessful()) {

            Map<String, List<String>> listMap = response.headers().toMultimap();
            for (String key : listMap.keySet()) {
                for (String value : listMap.get(key)) {
                    if (value.startsWith("Indigo-SmS-Auth-Token")) {
                        sessionManager.saveCookie(value);
                        HPLogger.d(TAG, "Cookie saved.");
                        break;
                    }
                }
            }
        }

        HPLogger.d(TAG, "Request Completed " + getRequestUrl(request) + " (" + (response == null ? 0 : response.code()) + ")");

        return response;
    }

    private String getRequestUrl(Request request) {
        try {
            return request.url().toString();
        } catch (Exception e) {
            return "";
        }
    }

}