package com.hp.printosmobilelib.core.communications.remote;


import android.content.Context;

import com.hp.printosmobilelib.core.communications.remote.services.LoginService;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.jackson.JacksonConverterFactory;

/**
 * A class to build the restful adapter and allow access to the API endpoints.
 * <p>
 * This class can be extended in the application level to provide additional endpoints.
 *
 * @author Osama Taha
 */
public class ApiClientFactory {

    private static final long DEFAULT_TIME_OUT = 60;
    final ConcurrentHashMap<Class, Object> services;
    final Retrofit apiAdapter;

    private OkHttpClient authenticatingHttpClient;
    private Context context;
    private ApiConfig apiConfig;

    public ApiClientFactory(Context context, ApiConfig apiConfig, boolean attachTokenAuthentication) {

        this.context = context;
        this.apiConfig = apiConfig;

        services = new ConcurrentHashMap<>();

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(apiConfig.getBaseHostUrl())
                .addCallAdapterFactory(ApiCallAdapterFactory.create())
                .addConverterFactory(JacksonConverterFactory.create());

        SessionManager sessionManager = SessionManager.getInstance(context);

        OkHttpClient httpClient = getAuthenticatingHttpClient(sessionManager, attachTokenAuthentication);

        apiAdapter = builder.client(httpClient).build();
    }

    /**
     * Creates a single instance from OkHttpClient to use in all requests.
     */
    public OkHttpClient getAuthenticatingHttpClient(SessionManager sessionManager, boolean attachTokenAuthentication) {

        if (authenticatingHttpClient == null) {

            OkHttpClient.Builder builder = new OkHttpClient.Builder();

            //Add authentication interceptor.
            builder.addInterceptor(new AuthenticationInterceptor(sessionManager));
            //Add authenticator
            if (attachTokenAuthentication) {
                builder.authenticator(new CookieAuthenticator(context, apiConfig, sessionManager));
            }

            builder.connectTimeout(DEFAULT_TIME_OUT, TimeUnit.SECONDS);
            builder.readTimeout(DEFAULT_TIME_OUT, TimeUnit.SECONDS);
            builder.writeTimeout(DEFAULT_TIME_OUT, TimeUnit.SECONDS);

            //Add logging interceptor
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

            builder.addInterceptor(interceptor);

            authenticatingHttpClient = builder.build();

        }

        return authenticatingHttpClient;
    }


    /**
     * @return {@link LoginService} to access the login API.
     */
    public final LoginService getLoginService() {
        return getService(LoginService.class);
    }

    /**
     * Converts Retrofit Endpoints interface to service instance.
     *
     * @param serviceClass Retrofit endpoints interface
     * @return instance of serviceClass
     */
    public <T> T getService(Class<T> serviceClass) {
        return getAdapterService(apiAdapter, serviceClass);
    }

    /**
     * Converts the Retrofit Endpoints interface into an instance using the given Retrofit adapter.
     *
     * @param adapter      A retrofit adapter to use to generate service instance.
     * @param serviceClass Retrofit interface for a given endpoint.
     * @return instance of serviceClass
     */
    protected <T> T getAdapterService(Retrofit adapter, Class<T> serviceClass) {

        if (!services.contains(serviceClass)) {
            services.putIfAbsent(serviceClass, adapter.create(serviceClass));
        }

        return (T) services.get(serviceClass);
    }

}
