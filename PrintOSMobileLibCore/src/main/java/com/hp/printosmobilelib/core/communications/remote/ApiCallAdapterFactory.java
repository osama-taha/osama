package com.hp.printosmobilelib.core.communications.remote;

import com.fasterxml.jackson.databind.JsonMappingException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import javax.net.ssl.SSLException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.CallAdapter;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.HttpException;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import rx.Observable;
import rx.functions.Action1;
import rx.functions.Func1;

public class ApiCallAdapterFactory extends CallAdapter.Factory {
    private final RxJavaCallAdapterFactory original;

    private ApiCallAdapterFactory() {
        original = RxJavaCallAdapterFactory.create();
    }

    public static CallAdapter.Factory create() {
        return new ApiCallAdapterFactory();
    }

    @Override
    public CallAdapter get(Type returnType, Annotation[] annotations, Retrofit retrofit) {
        if (original.get(returnType, annotations, retrofit) == null) {
            return original.get(returnType, annotations, retrofit);
        }
        return new RxCallAdapterWrapper(original.get(returnType, annotations, retrofit));
    }

    public static class RxCallAdapterWrapper<R> implements CallAdapter<R, Object> {

        private static final int URL_NOT_FOUND_ERROR_CODE = 404;
        private static final int UNAUTHORIZED_ERROR_CODE = 401;
        private static final int INTERNAL_SERVER_ERROR_CODE = 500;
        private static final int SERVER_BAD_GATEWAY_ERROR_CODE = 502;
        private static final int SERVICE_UNAVAILABLE_ERROR_CODE = 503;
        private static final int SERVICE_TIME_OUT_ERROR_CODE = 504;
        private final CallAdapter<R, Object> wrapped;

        public RxCallAdapterWrapper(CallAdapter<R, Object> wrapper) {

            RxJavaCallAdapterFactory.create();
            this.wrapped = wrapper;
        }

        @Override
        public Type responseType() {
            return wrapped.responseType();
        }

        @SuppressWarnings("unchecked")
        @Override
        public Object adapt(Call<R> call) {
            return ((Observable) wrapped.adapt(call)).doOnNext(new Action1() {
                @Override
                public void call(Object object) {
                    if (object instanceof Response) {
                        throwApiExceptionIfAny((Response) object);
                    } else if (object instanceof ResponseBody) {
                        ResponseBody responseBody = (ResponseBody) object;
                        try {
                            responseBody.string();
                        } catch (IOException e) {
                            throw asRetrofitException(APIException.Kind.UNAUTHORIZED, responseBody);
                        }
                    }
                }
            }).onErrorResumeNext(new Func1<Throwable, Observable>() {
                @Override
                public Observable call(Throwable throwable) {

                    return Observable.error(asRetrofitException(throwable, null));
                }
            });
        }

        public static void throwApiExceptionIfAny(Response response) {
            if (!response.isSuccessful()) {
                if (response.code() == URL_NOT_FOUND_ERROR_CODE) {
                    throw asRetrofitException(APIException.Kind.URL_NOT_FOUND_ERROR, response.errorBody());
                } else if (response.code() == UNAUTHORIZED_ERROR_CODE) {
                    throw asRetrofitException(APIException.Kind.UNAUTHORIZED, response.errorBody());
                } else if (response.code() == INTERNAL_SERVER_ERROR_CODE || response.code() == SERVER_BAD_GATEWAY_ERROR_CODE) {
                    throw asRetrofitException(APIException.Kind.INTERNAL_SERVER_ERROR, response.errorBody());
                } else if (response.code() == SERVICE_UNAVAILABLE_ERROR_CODE) {
                    throw asRetrofitException(APIException.Kind.SERVICE_UNAVAILABLE, response.errorBody());
                } else if (response.code() == SERVICE_TIME_OUT_ERROR_CODE) {
                    throw asRetrofitException(APIException.Kind.SERVICE_TIME_OUT, response.errorBody());
                }
            }
        }

        private static APIException asRetrofitException(Throwable throwable, ResponseBody responseBody) {

            if (throwable instanceof HttpException) {
                return asRetrofitException(APIException.Kind.HTTP, responseBody);
            } else if (throwable instanceof SocketTimeoutException) {
                return asRetrofitException(APIException.Kind.SERVICE_TIME_OUT, responseBody);
            } else if (throwable instanceof SSLException) {
                return asRetrofitException(APIException.Kind.INTERNAL_SERVER_ERROR, responseBody);
            } else if (throwable instanceof JsonMappingException) {
                return asRetrofitException(APIException.Kind.INTERNAL_SERVER_ERROR, responseBody);
            } else if (throwable instanceof UnknownHostException) {
                return asRetrofitException(APIException.Kind.NETWORK, responseBody);
            } else if (throwable instanceof FileNotFoundException) {
                return asRetrofitException(APIException.Kind.FILE_NOT_FOUND, responseBody);
            } else if (throwable instanceof APIException) {
                return (APIException) throwable;
            }
            return APIException.create(APIException.Kind.UNEXPECTED);
        }

        private static APIException asRetrofitException(APIException.Kind kind, ResponseBody responseBody) {
            return APIException.create(kind, responseBody);
        }

    }
}