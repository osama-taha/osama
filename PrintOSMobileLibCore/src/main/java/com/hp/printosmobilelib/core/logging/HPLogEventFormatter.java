package com.hp.printosmobilelib.core.logging;

import android.content.Context;
import android.text.TextUtils;

import com.hp.printosmobilelib.core.communications.remote.Preferences;
import com.hp.printosmobilelib.core.utils.DeviceUtils;

/**
 * Created by Osama Taha
 */

public class HPLogEventFormatter {

    private static final String TAG = HPLogEventFormatter.class.getSimpleName();

    private static final String LOG_PLATFORM_NOT_AUTHENTICATED_FORMAT = "Android - %1$s - account type: PRELOGIN";
    private static final String LOG_PLATFORM_AUTHENTICATED_FORMAT = "Android - %1$s - account type: %2$s";
    private static final String LOG_MESSAGE_SEPARATOR = " | ";
    private static final String DEVICE_ID_AUTHENTICATED_FORMAT = "deviceUDID %1$s user_id %2$s";
    private static final String DEVICE_ID_NOT_AUTHENTICATED_FORMAT = "deviceUDID %1$s user_id PRELOGIN";

    private final Context context;

    public HPLogEventFormatter(Context context) {
        this.context = context;
    }

    public void format(HPLogEvent logEntry, HPLogQueue.LogType logType) {

        String deviceId = DeviceUtils.getDeviceIdMD5(context);
        deviceId = TextUtils.isEmpty(deviceId) ? "" : deviceId;

        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(logEntry.getDate());
        stringBuilder.append(LOG_MESSAGE_SEPARATOR);

        String userId = Preferences.getInstance(context).getUserId();

        String deviceIdString = logType == HPLogQueue.LogType.AUTHENTICATED_LOG ?
                String.format(DEVICE_ID_AUTHENTICATED_FORMAT, deviceId, userId) :
                String.format(DEVICE_ID_NOT_AUTHENTICATED_FORMAT, deviceId);

        stringBuilder.append(deviceIdString);
        stringBuilder.append(LOG_MESSAGE_SEPARATOR);

        String appVersion = HPLogConfig.getInstance(context).getAppVersion();
        String userType = Preferences.getInstance(context).getUserType();
        userType = !TextUtils.isEmpty(userType) ? userType : "-";

        String logPlatform = logType == HPLogQueue.LogType.AUTHENTICATED_LOG ?
                String.format(LOG_PLATFORM_AUTHENTICATED_FORMAT, appVersion, userType)
                : String.format(LOG_PLATFORM_NOT_AUTHENTICATED_FORMAT, appVersion);

        stringBuilder.append(logPlatform);
        stringBuilder.append(LOG_MESSAGE_SEPARATOR);

        String logMsgPrefix = logEntry.getType().getLogMsgPrefix();
        stringBuilder.append(logMsgPrefix);
        if (!TextUtils.isEmpty(logMsgPrefix)) {
            stringBuilder.append(" - ");
        }

        stringBuilder.append(logEntry.getMessage());

        logEntry.setMessage(stringBuilder.toString());

    }

}
