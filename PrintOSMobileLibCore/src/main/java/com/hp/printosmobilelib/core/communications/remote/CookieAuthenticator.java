package com.hp.printosmobilelib.core.communications.remote;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;

import com.hp.printosmobilelib.core.communications.remote.models.OrganizationJsonBody;
import com.hp.printosmobilelib.core.communications.remote.models.UserCredentials;
import com.hp.printosmobilelib.core.communications.remote.models.UserData;
import com.hp.printosmobilelib.core.communications.remote.services.LoginService;
import com.hp.printosmobilelib.core.logging.HPLogger;
import com.hp.printosmobilelib.core.utils.StringUtils;

import java.io.IOException;

import okhttp3.Authenticator;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.Route;

/**
 * @author Anwar Asbah
 */
public class CookieAuthenticator implements Authenticator {

    private static final String TAG = CookieAuthenticator.class.getSimpleName();

    public static final String AUTHENTICATOR_INTENT_ACTION = "com.hp.printosforpsp.broadcast.cookie_authenticator";
    private static final String COOKIE_REQUEST_HEADER = "Cookie";
    public static final String COOKIE_REFRESH_ACTION = "cookieRefreshAction";

    public static final String ORGANIZATION_ID_EXTRA = "organizationExtra";
    public static final String ORGANIZATION_NAME_EXTRA = "organizationNameExtra";
    public static final String ORGANIZATION_EXTRA = "organizationTypeExtra";
    public static final String EXCEPTION_EXTRA = "exception_arg";
    private static final String DUMMY_COOKIE = "DUMMY";

    private SessionManager sessionManager;
    private ApiClientFactory apiClientFactory;
    private Context context;

    public CookieAuthenticator(Context context, ApiConfig apiConfig, SessionManager sessionManager) {
        this.sessionManager = sessionManager;
        apiClientFactory = new ApiClientFactory(context, apiConfig, false);
        this.context = context;
    }

    @Override
    public Request authenticate(Route route, Response response) throws IOException {

        String token = SessionManager.getInstance(context).getCookie();

        synchronized (SessionManager.getInstance(context)) {
            UserCredentials userCredentials = SessionManager.getInstance(context).getUserCredentials();
            if(userCredentials == null || TextUtils.isEmpty(userCredentials.getLogin())||
                    TextUtils.isEmpty(userCredentials.getPassword())){
                HPLogger.d(TAG, "failed authentication for request " + getRequestUrl(response));
                return null;
            }

            String currentToken = SessionManager.getInstance(context).getCookie();
            //comparing current token with token that was stored before,
            // if it was not updated - do auto login
            if (currentToken != null && currentToken.equals(token)) {
                HPLogger.d(TAG, "Start validate for request " + getRequestUrl(response));

                boolean isValid = validateToken(response);
                if (isValid) {
                    return null;
                }

                retrofit2.Response<UserData> autoLoginResponse = performAutoLogin(response);

                Intent intent = new Intent();
                intent.setAction(AUTHENTICATOR_INTENT_ACTION);

                try {
                    ApiCallAdapterFactory.RxCallAdapterWrapper.throwApiExceptionIfAny(autoLoginResponse);
                } catch (APIException e){
                    Bundle bundle = new Bundle();
                    bundle.putSerializable(EXCEPTION_EXTRA, e);
                    intent.putExtras(bundle);
                }

                context.sendBroadcast(intent);

                if (!autoLoginResponse.isSuccessful()) {
                    SessionManager.getInstance(context).saveCookie(DUMMY_COOKIE); //to prevent looping
                    sessionManager.saveTempCredentials();
                    sessionManager.clearUserCredentials();
                    return null;
                }

                changeContext(autoLoginResponse);
            }

            HPLogger.d(TAG, "retrying unauthorized request " + getRequestUrl(response));
            currentToken = SessionManager.getInstance(context).getCookie();
            if (currentToken != null) { //retry requires new auth token,
                return response.request().newBuilder()
                        .header(COOKIE_REQUEST_HEADER, sessionManager.getCookie())
                        .build();
            }
        }
        return null;
    }

    private boolean validateToken(Response response) throws IOException {
        LoginService loginService = apiClientFactory.getService(LoginService.class);
        retrofit2.Response<ResponseBody> isCookieValidResponse = loginService.checkCookie().execute();

        HPLogger.d(TAG, "Validate completed for request " + getRequestUrl(response) + " ("
                + getResponseCode(isCookieValidResponse) + ")");

        if (isCookieValidResponse.isSuccessful()) {
            try {
                HPLogger.d(TAG, isCookieValidResponse.body().string());
            } catch (Exception e) {
                //do nothing
            }
            return true;
        }
        return false;
    }

    private retrofit2.Response<UserData> performAutoLogin(Response response) throws IOException {
        LoginService loginService = apiClientFactory.getService(LoginService.class);
        retrofit2.Response<UserData> responseBody = loginService.loginSync("",
                this.sessionManager.getUserCredentials()).execute();
        HPLogger.d(TAG, "Auto login complete for request " + getRequestUrl(response) +
                " (" + getResponseCode(responseBody) + ")");

        return responseBody;
    }

    private void changeContext(retrofit2.Response<UserData> AutoLoginResponse)
            throws IOException {
        LoginService loginService = apiClientFactory.getService(LoginService.class);
        if (Preferences.getInstance(context).getSavedOrganizationId() != null) {
            HPLogger.d(TAG, "Start Change context.");

            retrofit2.Response<ResponseBody> organizationChangeResponse = loginService
                    .changeOrganizationContext(getOrganizationJsonBody()).execute();
            HPLogger.d(TAG, "Change context completed (" + organizationChangeResponse.code() + ").");
        } else if (AutoLoginResponse.body() != null) {
            UserData.Context organization = AutoLoginResponse.body().getContext();
            Preferences.getInstance(context).saveOrganization(organization);
        }
    }

    private int getResponseCode(retrofit2.Response response) {
        try {
            return response.code();
        } catch (Exception e) {
            HPLogger.e(TAG, "Error getting response code.", e);
            return 0;
        }
    }

    private String getRequestUrl(Response response) {
        try {
            return response.request().url().toString();
        } catch (Exception e) {
            HPLogger.e(TAG, "Error getting request url.", e);
            return "";
        }
    }

    public OrganizationJsonBody getOrganizationJsonBody() {
        UserData.Context organizationContext = Preferences.getInstance(context).getSelectedOrganization();
        OrganizationJsonBody body = new OrganizationJsonBody();
        body.setId(organizationContext.getId());
        body.setName(organizationContext.getName());
        body.setType(organizationContext.getType());
        return body;
    }
}