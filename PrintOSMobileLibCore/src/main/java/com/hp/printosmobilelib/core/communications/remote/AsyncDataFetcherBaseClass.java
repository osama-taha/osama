package com.hp.printosmobilelib.core.communications.remote;

import com.hp.printosmobilelib.core.logging.HPLogger;

import java.io.IOException;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subjects.BehaviorSubject;

/**
 * Created by leviasaf on 16/05/2016.
 */
public abstract class AsyncDataFetcherBaseClass<T> {

    private final BehaviorSubject<T> fetchDataRxSubject = BehaviorSubject.create();

    public BehaviorSubject<T> getFetchDataRxSubject() {
        return fetchDataRxSubject;
    }

    public Observable<T> getForceRefreshRxObservable() {
        return refreshData;
    }

    public Observable<T> getReadFromDataBaseRxObservable() {
        return readFromDataBase;
    }

    private final Observable<T> refreshData = Observable.create(new Observable.OnSubscribe<T>() {
        @Override
        public void call(Subscriber<? super T> subscriber) {
            try {
                HPLogger.d("", "refreshData thread=" + Thread.currentThread().getName());
                T data = getDataFromNetwork();
                getFetchDataRxSubject().onNext(data);
                subscriber.onNext(data);
                subscriber.onCompleted();
            } catch (Exception e) {
                subscriber.onError(e); // In case there are network errors
            }


        }
    });

    private final Observable<T> readFromDataBase = Observable.create(new Observable.OnSubscribe<T>() {
        @Override
        public void call(Subscriber<? super T> subscriber) {
            try {
                HPLogger.i("", "readFromDataBase thread=" + Thread.currentThread().getName());
                T data = getDataFromModel();
                subscriber.onNext(data);
                subscriber.onCompleted();

            } catch (Exception e) {
                subscriber.onError(e);
            }

        }
    });

    private void readFromDatabase() {
        getReadFromDataBaseRxObservable().observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<T>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                    }

                    @Override
                    public void onNext(T respond) {
                        HPLogger.i("", "readFromDataBase onNext thread=" + Thread.currentThread().getName());
                        if (respond != null) {
                            getFetchDataRxSubject().onNext(respond);
                        }
                    }
                });
    }

    private void refreshFromNetwork() {
        getForceRefreshRxObservable().observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<T>() {
                    @Override
                    public void onCompleted() {
                        HPLogger.d("", "Refresh complete");
                    }

                    @Override
                    public void onError(Throwable e) {
                        HPLogger.e("", "Network error");
                    }

                    @Override
                    public void onNext(T productionDataResponse) {
                        HPLogger.d("", "Refresh onNext thread=" + Thread.currentThread().getName());
                    }
                });
    }

    protected abstract T getDataFromModel();

    protected abstract T getDataFromNetwork() throws IOException;

    protected void init() {
        //todo: we need to decide if we want to load from database or to fetch the network.
        // maybe we need to read from database and than to decide if we need to refresh from network.
        readFromDatabase();
        refreshFromNetwork();
    }
}
