package com.hp.printosmobilelib.core.logging;

import android.content.Context;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.hp.printosmobilelib.core.communications.remote.Preferences;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Osama Taha
 */

public final class LogCache {

    private static final String TAG = LogCache.class.getSimpleName();

    private LogCache() {
    }

    public static void cacheLogs(Context context, List<HPLogEvent> logEvents, HPLogQueue.LogType type) {

        try {

            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
            String arrayToJson = objectMapper.writeValueAsString(logEvents);

            Preferences preferences = Preferences.getInstance(context);
            preferences.put(type.getLogCacheName(), arrayToJson);
            preferences.commit();

        } catch (Exception e) {
            HPLogger.logE(TAG, "Error in caching the logs", e);
        }

    }

    public static List<HPLogEvent> getCachedLogs(Context context, HPLogQueue.LogType type) {

        Preferences preferences = Preferences.getInstance(context);
        String cachedLogs = preferences.getString(type.getLogCacheName(), null);

        try {

            if (cachedLogs != null) {

                ObjectMapper objectMapper = new ObjectMapper();
                objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
                TypeReference<List<HPLogEvent>> mapType = new TypeReference<List<HPLogEvent>>() {
                };

                List<HPLogEvent> logEvents = objectMapper.readValue(cachedLogs, mapType);

                return logEvents;
            }

        } catch (Exception e) {
            HPLogger.logE(TAG, "Error getting logs from cache", e);
        }

        return new ArrayList<>();
    }

    public static void clearCache(Context context, HPLogQueue.LogType type) {

        try {
            Preferences preferences = Preferences.getInstance(context);
            preferences.remove(type.getLogCacheName());
            preferences.commit();
        } catch (Exception e) {
            HPLogger.logE(TAG, "Error while clearing the cache.", e);
        }
    }
}