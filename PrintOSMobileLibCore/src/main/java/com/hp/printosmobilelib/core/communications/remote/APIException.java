package com.hp.printosmobilelib.core.communications.remote;

import com.hp.printosmobilelib.core.logging.HPLogger;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hp.printosmobilelib.core.communications.remote.models.AAAError;

import java.io.Serializable;

import okhttp3.ResponseBody;

public class APIException extends RuntimeException implements Serializable {

    private static final String TAG = APIException.class.getSimpleName();

    private final Kind kind;
    private AAAError aaaError;

    public static APIException create(Kind type) {
        return new APIException(type);
    }

    public static APIException create(Kind type, ResponseBody responseBody) {

        if(type == null) {
            type = Kind.UNEXPECTED;
        }

        if(responseBody == null){
            return new APIException(type);
        }
        return new APIException(type, responseBody);
    }

    /**
     * Identifies the event kind which triggered a {@link APIException}.
     */
    public enum Kind {
        NETWORK,
        HTTP,
        UNAUTHORIZED,
        INTERNAL_SERVER_ERROR,
        URL_NOT_FOUND_ERROR,
        SERVICE_UNAVAILABLE,
        FILE_NOT_FOUND,
        UNEXPECTED, SERVICE_TIME_OUT, UNKNOWN_USER;

        public static String getName(Kind type) {
            switch (type) {
                case NETWORK:
                    return "Network";
                case HTTP:
                    return "http";
                case UNAUTHORIZED:
                    return "unauthorized";
                case INTERNAL_SERVER_ERROR:
                    return "internal server error";
                case URL_NOT_FOUND_ERROR:
                    return "url not found";
                case SERVICE_UNAVAILABLE:
                    return "service_unavailable";
                case SERVICE_TIME_OUT:
                    return "service_time_out";
                case FILE_NOT_FOUND:
                    return "file_not_found";
                case UNKNOWN_USER:
                    return "unknown_user";
                default:
                    return "unknown";
            }
        }
    }

    APIException(Kind kind) {
        super(Kind.getName(kind), new RuntimeException(Kind.getName(kind)));
        this.kind = kind;
    }

    APIException(Kind kind, ResponseBody responseBody) {

        this(kind);
        this.aaaError = readErrorResponse(responseBody);

    }

    private AAAError readErrorResponse(ResponseBody responseBody) {

        ObjectMapper mapper = new ObjectMapper();
        AAAError error = null;
        try {
            error = mapper.readValue(responseBody.bytes(), AAAError.class);
        } catch (Exception e) {
            HPLogger.e(TAG, "Unable to parse the error response", e);
        }

        return error;
    }

    /**
     * The event kind which triggered this error.
     */
    public Kind getKind() {
        return kind;
    }

    public AAAError getAAAError() {
        return aaaError;
    }

}