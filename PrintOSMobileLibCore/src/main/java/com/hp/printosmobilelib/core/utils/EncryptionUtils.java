package com.hp.printosmobilelib.core.utils;

import android.util.Base64;

import com.hp.printosmobilelib.core.logging.HPLogger;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.InvalidParameterSpecException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

/**
 * Utility class that provides a mean for encrypting and decrypting text
 * Created by Anwar on 2/7/2016.
 */
public class EncryptionUtils {

    private static final String TAG = EncryptionUtils.class.getSimpleName();
    private static String ENCRYPTION_KEY = "B3AF76D75ACCEA5AC6C232F30736C82E";
    private static String ENCRYPTION_MODE = "AES";

    /**
     * @return Secret key to be used for encryption
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    public static SecretKey generateKey() throws NoSuchAlgorithmException, InvalidKeySpecException {
        return new SecretKeySpec(ENCRYPTION_KEY.getBytes(), ENCRYPTION_MODE);
    }

    /**
     * @param message a string to be encrypted
     * @return the encrypted string
     */
    public static String encrypt(String message) {

        try {
            SecretKey key = generateKey();
            return encryptMsg(message, key);
        } catch (Exception e) {
            HPLogger.e(TAG, "failed to encrypt string");
        }
        return "";
    }

    /**
     * @param message an encrypted string to be decrypted
     * @return the decrypted string
     */
    public static String decrypt(String message) {

        SecretKey key;
        try {
            key = generateKey();
            return decryptMsg(message, key);
        } catch (Exception e) {
            HPLogger.e(TAG, "failed to decrypt string");
        }
        return "";
    }

    private static String encryptMsg(String message, SecretKey secret) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidParameterSpecException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {

        //Encrypt the message.
        Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE, secret);
        byte[] cipherText = cipher.doFinal(message.getBytes("UTF-8"));

        return Base64.encodeToString(cipherText, Base64.DEFAULT);
    }

    private static String decryptMsg(String cipherText, SecretKey secret) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidParameterSpecException, InvalidAlgorithmParameterException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException, UnsupportedEncodingException {

        //Decrypt the message, given derived encContentValues and initialization vector.
        byte[] cipherArray = Base64.decode(cipherText, Base64.DEFAULT);

        Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
        cipher.init(Cipher.DECRYPT_MODE, secret);
        String decryptString = new String(cipher.doFinal(cipherArray), "UTF-8");

        return decryptString;
    }

    public static String md5(String data) {
        try {
            MessageDigest digester = MessageDigest.getInstance("MD5");
            digester.update(data.getBytes());
            byte[] messageDigest = digester.digest();
            return byteArrayToHexString(messageDigest);
        } catch (Exception e) {
            return null;
        }
    }

    public static String byteArrayToHexString(byte[] array) {
        StringBuffer hexString = new StringBuffer();
        for (byte b : array) {
            int intVal = b & 0xff;
            if (intVal < 0x10)
                hexString.append("0");
            hexString.append(Integer.toHexString(intVal));
        }
        return hexString.toString();
    }

}
