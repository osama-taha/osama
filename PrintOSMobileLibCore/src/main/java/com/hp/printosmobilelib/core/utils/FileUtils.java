package com.hp.printosmobilelib.core.utils;

import android.annotation.SuppressLint;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.text.TextUtils;

import com.hp.printosmobilelib.core.logging.HPLogger;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;

/**
 * Created by Osama Taha on 12/20/15.
 */
public class FileUtils {

    private static final String TAG = FileUtils.class.getSimpleName();

    private static final String EXTERNAL_STORAGE_DOC_AUTHORITY = "com.android.externalstorage.documents";
    private static final String MEDIA_DOC_AUTHORITY = "com.android.providers.media.documents";
    private static final String DOWNLOADS_DOC_AUTHORITY = "com.android.providers.downloads.documents";
    private static final String DOWNLOADS_CONTENT_URI = "content://downloads/public_downloads";

    public static String getDynamicFilePath(Context context, Uri uri) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            return getFilePathKitkat(context, uri);
        } else {
            return getFilePathPreKitkat(context, uri);
        }
    }

    @SuppressLint("NewApi")
    private static String getFilePathKitkat(Context context, Uri uri) {

        if (DocumentsContract.isDocumentUri(context, uri)) {

            //Check the selected document type in order to select a proper way to fetch the file path.

            if (EXTERNAL_STORAGE_DOC_AUTHORITY.equals(uri.getAuthority())) {

                HPLogger.d(TAG, "Selected document type: external storage document");

                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

            } else if (DOWNLOADS_DOC_AUTHORITY.equals(uri.getAuthority())) {

                HPLogger.d(TAG, "Selected document type: downloads document");

                //Get the document id and build a content uri for it to get the original path for the selected file.
                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(Uri.parse(DOWNLOADS_CONTENT_URI), Long.valueOf(id));

                return getFilePath(context, contentUri, null, null);

            } else if (MEDIA_DOC_AUTHORITY.equals(uri.getAuthority())) {

                HPLogger.d(TAG, "Selected document type: media document");

                //Parse the document ID in order to check its type and define the content uri.
                String docId = DocumentsContract.getDocumentId(uri);
                String[] split = docId.split(":");
                String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                }

                String selection = "_id=?";
                String[] selectionArgs = new String[]{split[1]};

                return getFilePath(context, contentUri, selection, selectionArgs);
            }
        } else if ("content".equalsIgnoreCase(uri.getScheme())) {
            return getFilePath(context, uri, null, null);
        }

        return null;
    }


    /**
     * Get the file path using a given uri for versions less than kitkat.
     */
    private static String getFilePathPreKitkat(Context context, Uri uri) {

        if (uri != null && !uri.toString().isEmpty()) {

            String[] projectionList = {MediaStore.Images.Media.DATA};
            Cursor cursor = context.getContentResolver().query(uri, projectionList, null, null, null);
            if (cursor != null) {
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToFirst();
                return cursor.getString(column_index);
            }

            return uri.getPath();
        }

        return null;
    }

    /**
     * Get the value of the data column for this Uri where it should represent the file path.
     */
    private static String getFilePath(Context context, Uri uri, String selection,
                                      String[] selectionArgs) {
        final String _data = "_data";
        final String[] projectionList = {_data};

        Cursor cursor = null;

        try {
            cursor = context.getContentResolver().query(uri, projectionList, selection, selectionArgs, null);
            if (cursor != null && cursor.moveToFirst()) {
                int column_index = cursor.getColumnIndexOrThrow(_data);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return null;
    }

    public static StringBuilder readFile(String filePath) {

        File file = new File(filePath);
        StringBuilder fileContent = new StringBuilder("");
        if (!file.isFile()) {
            return null;
        }

        BufferedReader reader = null;
        try {
            InputStreamReader is = new InputStreamReader(new FileInputStream(
                    file));
            reader = new BufferedReader(is);
            String line;
            while ((line = reader.readLine()) != null) {
                if (!fileContent.toString().equals("")) {
                    fileContent.append("\r\n");
                }
                fileContent.append(line);
            }
            reader.close();
            return fileContent;
        } catch (Exception e) {
            return fileContent;
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (Exception e) {
                    return fileContent;
                }
            }
        }
    }

    public static boolean writeFile(String filePath, String content,
                                    boolean append) {
        if (TextUtils.isEmpty(content)) {
            return false;
        }

        FileWriter fileWriter = null;
        try {
            makeDirs(filePath);
            fileWriter = new FileWriter(filePath, append);
            fileWriter.write(content);
            fileWriter.close();
            return true;
        } catch (Exception e) {
            return false;
        } finally {
            if (fileWriter != null) {
                try {
                    fileWriter.close();
                } catch (Exception e) {
                    return false;
                }
            }
        }
    }

    public static String getFolderName(String filePath) {

        if (TextUtils.isEmpty(filePath)) {
            return filePath;
        }

        int filePosition = filePath.lastIndexOf(File.separator);
        return (filePosition == -1) ? "" : filePath.substring(0, filePosition);
    }

    public static boolean makeDirs(String filePath) {
        String folderName = getFolderName(filePath);
        if (TextUtils.isEmpty(folderName)) {
            return false;
        }

        File folder = new File(folderName);
        return (folder.exists() && folder.isDirectory()) || folder
                .mkdirs();
    }

}
