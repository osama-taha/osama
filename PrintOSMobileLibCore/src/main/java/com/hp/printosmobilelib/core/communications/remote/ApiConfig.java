package com.hp.printosmobilelib.core.communications.remote;

import android.net.Uri;

/**
 * A configuration class for API client.
 *
 * @author Osama Taha
 */
public class ApiConfig {

    /*
   * The base host URL.
   */
    private final String baseHostUrl;

    public ApiConfig(String baseHostUrl) {
        if (baseHostUrl == null) {
            throw new IllegalArgumentException(this.getClass().getName() + ", baseHostUrl == null");
        }
        this.baseHostUrl = baseHostUrl;
    }

    public String getBaseHostUrl() {
        return baseHostUrl;
    }

    /**
     * Build URL given paths to append to the base host url.
     *
     * @param paths the paths to append
     */
    public Uri.Builder buildUponBaseHostUrl(String... paths) {

        final Uri.Builder builder = Uri.parse(getBaseHostUrl()).buildUpon();
        if (paths != null) {
            for (String path : paths) {
                builder.appendPath(path);
            }
        }
        return builder;
    }

}
