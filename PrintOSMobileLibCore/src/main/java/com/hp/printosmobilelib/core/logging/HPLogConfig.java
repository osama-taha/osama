package com.hp.printosmobilelib.core.logging;

import android.content.Context;

import com.hp.printosmobilelib.core.communications.remote.services.LogDispatchService;

import java.util.List;

/**
 * Created by Osama Taha
 */
public final class HPLogConfig {

    private final Context context;
    private boolean isEnabled;
    private boolean logToFileEnabled;
    private int batchSize;
    private HPLogEventFormatter logEventFormatter;
    private String appVersion;

    private HPLogListener logListener;
    private HPLogDispatcher logDispatcher;
    private int preloginMaxSize;
    private int failedLogsMaxSize;
    private boolean dispatchingEnabled;

    private HPLogConfig(Context context) {
        this.context = context;
        this.isEnabled = false;
        this.logListener = new DefaultListener();
        this.batchSize = 20;
        this.preloginMaxSize = 200;
        this.failedLogsMaxSize = 25;
        this.logEventFormatter = new HPLogEventFormatter(context);
        this.dispatchingEnabled = true;
    }

    private static volatile HPLogConfig INSTANCE = null;

    public static synchronized HPLogConfig getInstance(Context context) {
        if (INSTANCE == null)
            INSTANCE = new HPLogConfig(context);
        return INSTANCE;
    }

    public boolean isEnabled() {
        return isEnabled;
    }

    public HPLogConfig enabled(boolean isEnabled) {
        this.isEnabled = isEnabled;
        return this;
    }

    public boolean isDispatchingEnabled (){
        return dispatchingEnabled;
    }

    public void setDispatchingEnabled (boolean dispatchingEnabled) {
        this.dispatchingEnabled = dispatchingEnabled;
    }

    public boolean isLogToFileEnabled() {
        return logToFileEnabled;
    }

    public HPLogConfig setLogToFileEnabled(boolean isEnabled) {
        this.logToFileEnabled = isEnabled;
        return this;
    }

    /**
     * Configure the listener for flushing logs externally.
     */
    public HPLogListener getLogListener() {
        return logListener;
    }

    public HPLogConfig logListener(final HPLogListener logListener) {
        this.logListener = logListener;
        return this;
    }

    public HPLogEventFormatter getLogEventFormatter() {
        return logEventFormatter;
    }

    public void setLogEventFormatter(HPLogEventFormatter logEventFormatter) {
        this.logEventFormatter = logEventFormatter;
    }

    /**
     * Set batch size for log events before flushing.
     */
    public int getBatchSize() {
        return batchSize;
    }

    public HPLogConfig batchSize(final int batchSize) {
        this.batchSize = batchSize;
        return this;
    }

    public HPLogConfig setAppVersion(String appVersion) {
        this.appVersion = appVersion;
        return this;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public int getFailedLogsMaxSize() {
        return failedLogsMaxSize;
    }

    public HPLogConfig setFailedLogsMaxSize(int failedLogsMaxSize) {
        this.failedLogsMaxSize = failedLogsMaxSize;
        return this;
    }

    public HPLogConfig setDispatchService(LogDispatchService dispatchService) {
        this.logDispatcher = new HPLogDispatcher(context, dispatchService);
        return this;
    }

    public HPLogDispatcher getLogDispatcher() {
        return logDispatcher;
    }

    public int getPreloginMaxSize() {
        return preloginMaxSize;
    }

    public HPLogConfig setPreloginMaxSize(int preloginMaxSize) {
        this.preloginMaxSize = preloginMaxSize;
        return this;
    }

    private static class DefaultListener implements HPLogListener {

        private static final String TAG = DefaultListener.class.getSimpleName();

        @Override
        public void flushLogs(final List<HPLogEvent> logEvents) {
            if (logEvents != null) {
                HPLogger.logD(TAG, "Flush logs " + logEvents.size());
            }
        }
    }
}